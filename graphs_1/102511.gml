graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.1517857142857144
  density 0.009649263292761051
  graphCliqueNumber 3
  node [
    id 0
    label "poniewa&#380;"
    origin "text"
  ]
  node [
    id 1
    label "zesz&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "tydzie&#324;"
    origin "text"
  ]
  node [
    id 3
    label "jarek"
    origin "text"
  ]
  node [
    id 4
    label "rozbi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "obydwa"
    origin "text"
  ]
  node [
    id 6
    label "swoje"
    origin "text"
  ]
  node [
    id 7
    label "model"
    origin "text"
  ]
  node [
    id 8
    label "tym"
    origin "text"
  ]
  node [
    id 9
    label "razem"
    origin "text"
  ]
  node [
    id 10
    label "musia&#322;"
    origin "text"
  ]
  node [
    id 11
    label "po&#380;ycza&#263;"
    origin "text"
  ]
  node [
    id 12
    label "hubert"
    origin "text"
  ]
  node [
    id 13
    label "niestety"
    origin "text"
  ]
  node [
    id 14
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 15
    label "ani"
    origin "text"
  ]
  node [
    id 16
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 17
    label "szcz&#281;scia"
    origin "text"
  ]
  node [
    id 18
    label "bardzo"
    origin "text"
  ]
  node [
    id 19
    label "umiej&#281;tno&#347;&#263;"
    origin "text"
  ]
  node [
    id 20
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 21
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 22
    label "spotkanie"
    origin "text"
  ]
  node [
    id 23
    label "niefortunny"
    origin "text"
  ]
  node [
    id 24
    label "skok"
    origin "text"
  ]
  node [
    id 25
    label "po&#380;yczy&#263;"
    origin "text"
  ]
  node [
    id 26
    label "rustlerem"
    origin "text"
  ]
  node [
    id 27
    label "zako&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 28
    label "si&#281;"
    origin "text"
  ]
  node [
    id 29
    label "z&#322;amanie"
    origin "text"
  ]
  node [
    id 30
    label "wahacz"
    origin "text"
  ]
  node [
    id 31
    label "szcz&#281;&#347;cie"
    origin "text"
  ]
  node [
    id 32
    label "monta&#380;"
    origin "text"
  ]
  node [
    id 33
    label "czeka&#263;"
    origin "text"
  ]
  node [
    id 34
    label "tuningowe"
    origin "text"
  ]
  node [
    id 35
    label "oferta"
    origin "text"
  ]
  node [
    id 36
    label "rpm"
    origin "text"
  ]
  node [
    id 37
    label "rustler"
    origin "text"
  ]
  node [
    id 38
    label "szybko"
    origin "text"
  ]
  node [
    id 39
    label "powr&#243;ci&#263;"
    origin "text"
  ]
  node [
    id 40
    label "tora"
    origin "text"
  ]
  node [
    id 41
    label "ostatni"
  ]
  node [
    id 42
    label "&#380;&#243;&#322;ty_tydzie&#324;"
  ]
  node [
    id 43
    label "doba"
  ]
  node [
    id 44
    label "czas"
  ]
  node [
    id 45
    label "Wielki_Tydzie&#324;"
  ]
  node [
    id 46
    label "weekend"
  ]
  node [
    id 47
    label "miesi&#261;c"
  ]
  node [
    id 48
    label "wyrobi&#263;"
  ]
  node [
    id 49
    label "unieszkodliwi&#263;"
  ]
  node [
    id 50
    label "pomiesza&#263;"
  ]
  node [
    id 51
    label "blast"
  ]
  node [
    id 52
    label "przybi&#263;"
  ]
  node [
    id 53
    label "roz&#322;o&#380;y&#263;"
  ]
  node [
    id 54
    label "rozpi&#261;&#263;"
  ]
  node [
    id 55
    label "rozdrobni&#263;"
  ]
  node [
    id 56
    label "zdezorganizowa&#263;"
  ]
  node [
    id 57
    label "poturbowa&#263;"
  ]
  node [
    id 58
    label "pokona&#263;"
  ]
  node [
    id 59
    label "podzieli&#263;"
  ]
  node [
    id 60
    label "zniszczy&#263;"
  ]
  node [
    id 61
    label "rozepcha&#263;"
  ]
  node [
    id 62
    label "rozszyfrowa&#263;"
  ]
  node [
    id 63
    label "zgarn&#261;&#263;"
  ]
  node [
    id 64
    label "ubi&#263;"
  ]
  node [
    id 65
    label "bankrupt"
  ]
  node [
    id 66
    label "obrabowa&#263;"
  ]
  node [
    id 67
    label "zbudowa&#263;"
  ]
  node [
    id 68
    label "st&#322;uc"
  ]
  node [
    id 69
    label "divide"
  ]
  node [
    id 70
    label "rozproszy&#263;"
  ]
  node [
    id 71
    label "peddle"
  ]
  node [
    id 72
    label "pitch"
  ]
  node [
    id 73
    label "odbi&#263;"
  ]
  node [
    id 74
    label "naruszy&#263;"
  ]
  node [
    id 75
    label "typ"
  ]
  node [
    id 76
    label "cz&#322;owiek"
  ]
  node [
    id 77
    label "pozowa&#263;"
  ]
  node [
    id 78
    label "ideal"
  ]
  node [
    id 79
    label "matryca"
  ]
  node [
    id 80
    label "imitacja"
  ]
  node [
    id 81
    label "ruch"
  ]
  node [
    id 82
    label "motif"
  ]
  node [
    id 83
    label "pozowanie"
  ]
  node [
    id 84
    label "wz&#243;r"
  ]
  node [
    id 85
    label "miniatura"
  ]
  node [
    id 86
    label "prezenter"
  ]
  node [
    id 87
    label "facet"
  ]
  node [
    id 88
    label "orygina&#322;"
  ]
  node [
    id 89
    label "mildew"
  ]
  node [
    id 90
    label "spos&#243;b"
  ]
  node [
    id 91
    label "zi&#243;&#322;ko"
  ]
  node [
    id 92
    label "adaptation"
  ]
  node [
    id 93
    label "&#322;&#261;cznie"
  ]
  node [
    id 94
    label "loan"
  ]
  node [
    id 95
    label "impart"
  ]
  node [
    id 96
    label "bra&#263;"
  ]
  node [
    id 97
    label "proszek"
  ]
  node [
    id 98
    label "du&#380;y"
  ]
  node [
    id 99
    label "cz&#281;sto"
  ]
  node [
    id 100
    label "mocno"
  ]
  node [
    id 101
    label "wiela"
  ]
  node [
    id 102
    label "w_chuj"
  ]
  node [
    id 103
    label "zdolno&#347;&#263;"
  ]
  node [
    id 104
    label "cecha"
  ]
  node [
    id 105
    label "miejsce"
  ]
  node [
    id 106
    label "faza"
  ]
  node [
    id 107
    label "upgrade"
  ]
  node [
    id 108
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 109
    label "pierworodztwo"
  ]
  node [
    id 110
    label "nast&#281;pstwo"
  ]
  node [
    id 111
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 112
    label "po&#380;egnanie"
  ]
  node [
    id 113
    label "spowodowanie"
  ]
  node [
    id 114
    label "znalezienie"
  ]
  node [
    id 115
    label "znajomy"
  ]
  node [
    id 116
    label "doznanie"
  ]
  node [
    id 117
    label "employment"
  ]
  node [
    id 118
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 119
    label "gather"
  ]
  node [
    id 120
    label "powitanie"
  ]
  node [
    id 121
    label "spotykanie"
  ]
  node [
    id 122
    label "wydarzenie"
  ]
  node [
    id 123
    label "gathering"
  ]
  node [
    id 124
    label "spotkanie_si&#281;"
  ]
  node [
    id 125
    label "zdarzenie_si&#281;"
  ]
  node [
    id 126
    label "match"
  ]
  node [
    id 127
    label "zawarcie"
  ]
  node [
    id 128
    label "niestosowny"
  ]
  node [
    id 129
    label "nieszcz&#281;&#347;liwie"
  ]
  node [
    id 130
    label "nieudany"
  ]
  node [
    id 131
    label "wybicie"
  ]
  node [
    id 132
    label "konkurencja"
  ]
  node [
    id 133
    label "derail"
  ]
  node [
    id 134
    label "ptak"
  ]
  node [
    id 135
    label "l&#261;dowanie"
  ]
  node [
    id 136
    label "&#322;apa"
  ]
  node [
    id 137
    label "struktura_anatomiczna"
  ]
  node [
    id 138
    label "stroke"
  ]
  node [
    id 139
    label "gimnastyka_przyrz&#261;dowa"
  ]
  node [
    id 140
    label "zmiana"
  ]
  node [
    id 141
    label "caper"
  ]
  node [
    id 142
    label "zaj&#261;c"
  ]
  node [
    id 143
    label "naskok"
  ]
  node [
    id 144
    label "napad"
  ]
  node [
    id 145
    label "ko&#347;&#263;_skokowa"
  ]
  node [
    id 146
    label "noga"
  ]
  node [
    id 147
    label "wzi&#261;&#263;"
  ]
  node [
    id 148
    label "zrobi&#263;"
  ]
  node [
    id 149
    label "preen"
  ]
  node [
    id 150
    label "hire"
  ]
  node [
    id 151
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 152
    label "communicate"
  ]
  node [
    id 153
    label "cause"
  ]
  node [
    id 154
    label "zrezygnowa&#263;"
  ]
  node [
    id 155
    label "wytworzy&#263;"
  ]
  node [
    id 156
    label "przesta&#263;"
  ]
  node [
    id 157
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 158
    label "dispose"
  ]
  node [
    id 159
    label "uraz"
  ]
  node [
    id 160
    label "zrobienie"
  ]
  node [
    id 161
    label "nastawianie"
  ]
  node [
    id 162
    label "nastawia&#263;"
  ]
  node [
    id 163
    label "z&#322;o&#380;enie"
  ]
  node [
    id 164
    label "transgresja"
  ]
  node [
    id 165
    label "szyjka_udowa"
  ]
  node [
    id 166
    label "interruption"
  ]
  node [
    id 167
    label "fracture"
  ]
  node [
    id 168
    label "nastawi&#263;"
  ]
  node [
    id 169
    label "przygn&#281;bienie"
  ]
  node [
    id 170
    label "wygranie"
  ]
  node [
    id 171
    label "dislocation"
  ]
  node [
    id 172
    label "discourtesy"
  ]
  node [
    id 173
    label "gips"
  ]
  node [
    id 174
    label "nastawienie"
  ]
  node [
    id 175
    label "prze&#322;amanie_si&#281;"
  ]
  node [
    id 176
    label "za&#322;amanie_si&#281;"
  ]
  node [
    id 177
    label "podzielenie"
  ]
  node [
    id 178
    label "wy&#322;amanie"
  ]
  node [
    id 179
    label "zawieszenie"
  ]
  node [
    id 180
    label "d&#378;wignia"
  ]
  node [
    id 181
    label "wra&#380;enie"
  ]
  node [
    id 182
    label "przeznaczenie"
  ]
  node [
    id 183
    label "dobrodziejstwo"
  ]
  node [
    id 184
    label "dobro"
  ]
  node [
    id 185
    label "przypadek"
  ]
  node [
    id 186
    label "przedstawienie"
  ]
  node [
    id 187
    label "czynno&#347;&#263;"
  ]
  node [
    id 188
    label "konstrukcja"
  ]
  node [
    id 189
    label "spos&#243;b_dzia&#322;ania"
  ]
  node [
    id 190
    label "podstawa"
  ]
  node [
    id 191
    label "audycja"
  ]
  node [
    id 192
    label "realizacja"
  ]
  node [
    id 193
    label "by&#263;"
  ]
  node [
    id 194
    label "hold"
  ]
  node [
    id 195
    label "sp&#281;dza&#263;"
  ]
  node [
    id 196
    label "look"
  ]
  node [
    id 197
    label "decydowa&#263;"
  ]
  node [
    id 198
    label "oczekiwa&#263;"
  ]
  node [
    id 199
    label "pauzowa&#263;"
  ]
  node [
    id 200
    label "anticipate"
  ]
  node [
    id 201
    label "propozycja"
  ]
  node [
    id 202
    label "offer"
  ]
  node [
    id 203
    label "quicker"
  ]
  node [
    id 204
    label "promptly"
  ]
  node [
    id 205
    label "bezpo&#347;rednio"
  ]
  node [
    id 206
    label "quickest"
  ]
  node [
    id 207
    label "sprawnie"
  ]
  node [
    id 208
    label "dynamicznie"
  ]
  node [
    id 209
    label "szybciej"
  ]
  node [
    id 210
    label "prosto"
  ]
  node [
    id 211
    label "szybciochem"
  ]
  node [
    id 212
    label "szybki"
  ]
  node [
    id 213
    label "return"
  ]
  node [
    id 214
    label "zosta&#263;"
  ]
  node [
    id 215
    label "spowodowa&#263;"
  ]
  node [
    id 216
    label "przyj&#347;&#263;"
  ]
  node [
    id 217
    label "revive"
  ]
  node [
    id 218
    label "podj&#261;&#263;"
  ]
  node [
    id 219
    label "wr&#243;ci&#263;"
  ]
  node [
    id 220
    label "przyby&#263;"
  ]
  node [
    id 221
    label "recur"
  ]
  node [
    id 222
    label "zw&#243;j"
  ]
  node [
    id 223
    label "Tora"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 8
    target 18
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 97
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 102
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 103
  ]
  edge [
    source 19
    target 104
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 33
  ]
  edge [
    source 20
    target 34
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 105
  ]
  edge [
    source 21
    target 106
  ]
  edge [
    source 21
    target 107
  ]
  edge [
    source 21
    target 108
  ]
  edge [
    source 21
    target 109
  ]
  edge [
    source 21
    target 110
  ]
  edge [
    source 21
    target 25
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 111
  ]
  edge [
    source 22
    target 112
  ]
  edge [
    source 22
    target 113
  ]
  edge [
    source 22
    target 114
  ]
  edge [
    source 22
    target 115
  ]
  edge [
    source 22
    target 116
  ]
  edge [
    source 22
    target 117
  ]
  edge [
    source 22
    target 118
  ]
  edge [
    source 22
    target 119
  ]
  edge [
    source 22
    target 120
  ]
  edge [
    source 22
    target 121
  ]
  edge [
    source 22
    target 122
  ]
  edge [
    source 22
    target 123
  ]
  edge [
    source 22
    target 124
  ]
  edge [
    source 22
    target 125
  ]
  edge [
    source 22
    target 126
  ]
  edge [
    source 22
    target 127
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 128
  ]
  edge [
    source 23
    target 129
  ]
  edge [
    source 23
    target 130
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 131
  ]
  edge [
    source 24
    target 132
  ]
  edge [
    source 24
    target 133
  ]
  edge [
    source 24
    target 134
  ]
  edge [
    source 24
    target 81
  ]
  edge [
    source 24
    target 135
  ]
  edge [
    source 24
    target 136
  ]
  edge [
    source 24
    target 137
  ]
  edge [
    source 24
    target 138
  ]
  edge [
    source 24
    target 139
  ]
  edge [
    source 24
    target 108
  ]
  edge [
    source 24
    target 140
  ]
  edge [
    source 24
    target 141
  ]
  edge [
    source 24
    target 142
  ]
  edge [
    source 24
    target 143
  ]
  edge [
    source 24
    target 144
  ]
  edge [
    source 24
    target 145
  ]
  edge [
    source 24
    target 146
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 147
  ]
  edge [
    source 25
    target 148
  ]
  edge [
    source 25
    target 149
  ]
  edge [
    source 25
    target 150
  ]
  edge [
    source 25
    target 151
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 152
  ]
  edge [
    source 27
    target 153
  ]
  edge [
    source 27
    target 154
  ]
  edge [
    source 27
    target 155
  ]
  edge [
    source 27
    target 156
  ]
  edge [
    source 27
    target 157
  ]
  edge [
    source 27
    target 158
  ]
  edge [
    source 27
    target 148
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 159
  ]
  edge [
    source 29
    target 160
  ]
  edge [
    source 29
    target 161
  ]
  edge [
    source 29
    target 162
  ]
  edge [
    source 29
    target 163
  ]
  edge [
    source 29
    target 164
  ]
  edge [
    source 29
    target 165
  ]
  edge [
    source 29
    target 166
  ]
  edge [
    source 29
    target 167
  ]
  edge [
    source 29
    target 168
  ]
  edge [
    source 29
    target 169
  ]
  edge [
    source 29
    target 170
  ]
  edge [
    source 29
    target 171
  ]
  edge [
    source 29
    target 172
  ]
  edge [
    source 29
    target 173
  ]
  edge [
    source 29
    target 174
  ]
  edge [
    source 29
    target 175
  ]
  edge [
    source 29
    target 176
  ]
  edge [
    source 29
    target 177
  ]
  edge [
    source 29
    target 178
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 30
    target 35
  ]
  edge [
    source 30
    target 179
  ]
  edge [
    source 30
    target 180
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 181
  ]
  edge [
    source 31
    target 182
  ]
  edge [
    source 31
    target 183
  ]
  edge [
    source 31
    target 184
  ]
  edge [
    source 31
    target 185
  ]
  edge [
    source 32
    target 186
  ]
  edge [
    source 32
    target 187
  ]
  edge [
    source 32
    target 188
  ]
  edge [
    source 32
    target 189
  ]
  edge [
    source 32
    target 190
  ]
  edge [
    source 32
    target 191
  ]
  edge [
    source 32
    target 192
  ]
  edge [
    source 33
    target 193
  ]
  edge [
    source 33
    target 194
  ]
  edge [
    source 33
    target 195
  ]
  edge [
    source 33
    target 196
  ]
  edge [
    source 33
    target 197
  ]
  edge [
    source 33
    target 198
  ]
  edge [
    source 33
    target 199
  ]
  edge [
    source 33
    target 200
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 201
  ]
  edge [
    source 35
    target 202
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 203
  ]
  edge [
    source 38
    target 204
  ]
  edge [
    source 38
    target 205
  ]
  edge [
    source 38
    target 206
  ]
  edge [
    source 38
    target 207
  ]
  edge [
    source 38
    target 208
  ]
  edge [
    source 38
    target 209
  ]
  edge [
    source 38
    target 210
  ]
  edge [
    source 38
    target 211
  ]
  edge [
    source 38
    target 212
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 213
  ]
  edge [
    source 39
    target 214
  ]
  edge [
    source 39
    target 215
  ]
  edge [
    source 39
    target 216
  ]
  edge [
    source 39
    target 217
  ]
  edge [
    source 39
    target 218
  ]
  edge [
    source 39
    target 219
  ]
  edge [
    source 39
    target 220
  ]
  edge [
    source 39
    target 221
  ]
  edge [
    source 40
    target 222
  ]
  edge [
    source 40
    target 223
  ]
]
