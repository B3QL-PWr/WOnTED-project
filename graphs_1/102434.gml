graph [
  maxDegree 12
  minDegree 1
  meanDegree 2
  density 0.08333333333333333
  graphCliqueNumber 3
  node [
    id 0
    label "g&#322;owacki"
    origin "text"
  ]
  node [
    id 1
    label "te&#380;"
    origin "text"
  ]
  node [
    id 2
    label "korzysta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pomoc"
    origin "text"
  ]
  node [
    id 4
    label "olbrzym"
    origin "text"
  ]
  node [
    id 5
    label "use"
  ]
  node [
    id 6
    label "uzyskiwa&#263;"
  ]
  node [
    id 7
    label "u&#380;ywa&#263;"
  ]
  node [
    id 8
    label "zgodzi&#263;"
  ]
  node [
    id 9
    label "pomocnik"
  ]
  node [
    id 10
    label "doch&#243;d"
  ]
  node [
    id 11
    label "property"
  ]
  node [
    id 12
    label "przedmiot"
  ]
  node [
    id 13
    label "grupa"
  ]
  node [
    id 14
    label "telefon_zaufania"
  ]
  node [
    id 15
    label "darowizna"
  ]
  node [
    id 16
    label "&#347;rodek"
  ]
  node [
    id 17
    label "liga"
  ]
  node [
    id 18
    label "cz&#322;owiek"
  ]
  node [
    id 19
    label "gwiazda"
  ]
  node [
    id 20
    label "Gargantua"
  ]
  node [
    id 21
    label "nieu&#322;omek"
  ]
  node [
    id 22
    label "istota_fantastyczna"
  ]
  node [
    id 23
    label "zwierz&#281;"
  ]
  node [
    id 24
    label "ogromny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
]
