graph [
  maxDegree 12
  minDegree 1
  meanDegree 1.8571428571428572
  density 0.14285714285714285
  graphCliqueNumber 2
  node [
    id 0
    label "cena"
    origin "text"
  ]
  node [
    id 1
    label "lidl"
    origin "text"
  ]
  node [
    id 2
    label "versus"
    origin "text"
  ]
  node [
    id 3
    label "cenowa_zmiana_poda&#380;y"
  ]
  node [
    id 4
    label "warto&#347;&#263;"
  ]
  node [
    id 5
    label "cenowa_elastyczno&#347;&#263;_popytu"
  ]
  node [
    id 6
    label "dyskryminacja_cenowa"
  ]
  node [
    id 7
    label "inflacja"
  ]
  node [
    id 8
    label "kupowanie"
  ]
  node [
    id 9
    label "kosztowa&#263;"
  ]
  node [
    id 10
    label "kosztowanie"
  ]
  node [
    id 11
    label "worth"
  ]
  node [
    id 12
    label "wyceni&#263;"
  ]
  node [
    id 13
    label "wycenienie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
]
