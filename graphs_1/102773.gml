graph [
  maxDegree 12
  minDegree 1
  meanDegree 2.1
  density 0.11052631578947368
  graphCliqueNumber 3
  node [
    id 0
    label "opis"
    origin "text"
  ]
  node [
    id 1
    label "okoliczno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wypadek"
    origin "text"
  ]
  node [
    id 3
    label "exposition"
  ]
  node [
    id 4
    label "czynno&#347;&#263;"
  ]
  node [
    id 5
    label "wypowied&#378;"
  ]
  node [
    id 6
    label "obja&#347;nienie"
  ]
  node [
    id 7
    label "wydarzenie"
  ]
  node [
    id 8
    label "sk&#322;adnik"
  ]
  node [
    id 9
    label "warunki"
  ]
  node [
    id 10
    label "sytuacja"
  ]
  node [
    id 11
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 12
    label "motyw"
  ]
  node [
    id 13
    label "ubezpieczenie_wypadkowe"
  ]
  node [
    id 14
    label "fabu&#322;a"
  ]
  node [
    id 15
    label "przebiec"
  ]
  node [
    id 16
    label "happening"
  ]
  node [
    id 17
    label "przebiegni&#281;cie"
  ]
  node [
    id 18
    label "event"
  ]
  node [
    id 19
    label "charakter"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
]
