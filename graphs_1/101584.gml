graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.133603238866397
  density 0.00432779561636186
  graphCliqueNumber 3
  node [
    id 0
    label "pan"
    origin "text"
  ]
  node [
    id 1
    label "marsza&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "wysoki"
    origin "text"
  ]
  node [
    id 3
    label "izba"
    origin "text"
  ]
  node [
    id 4
    label "d&#322;ugo"
    origin "text"
  ]
  node [
    id 5
    label "czeka&#263;by&#263;"
    origin "text"
  ]
  node [
    id 6
    label "rozwi&#261;zanie"
    origin "text"
  ]
  node [
    id 7
    label "kwestia"
    origin "text"
  ]
  node [
    id 8
    label "mo&#380;liwo&#347;ci"
    origin "text"
  ]
  node [
    id 9
    label "zak&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 10
    label "&#380;&#322;obek"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "naprawd&#281;"
    origin "text"
  ]
  node [
    id 13
    label "bardzo"
    origin "text"
  ]
  node [
    id 14
    label "dobry"
    origin "text"
  ]
  node [
    id 15
    label "moment"
    origin "text"
  ]
  node [
    id 16
    label "analiza"
    origin "text"
  ]
  node [
    id 17
    label "dokument"
    origin "text"
  ]
  node [
    id 18
    label "uzyska&#263;"
    origin "text"
  ]
  node [
    id 19
    label "informacja"
    origin "text"
  ]
  node [
    id 20
    label "pani"
    origin "text"
  ]
  node [
    id 21
    label "minister"
    origin "text"
  ]
  node [
    id 22
    label "niestety"
    origin "text"
  ]
  node [
    id 23
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 24
    label "stwierdzi&#263;"
    origin "text"
  ]
  node [
    id 25
    label "skutek"
    origin "text"
  ]
  node [
    id 26
    label "ten"
    origin "text"
  ]
  node [
    id 27
    label "ustawa"
    origin "text"
  ]
  node [
    id 28
    label "pewnie"
    origin "text"
  ]
  node [
    id 29
    label "od&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 30
    label "czas"
    origin "text"
  ]
  node [
    id 31
    label "wzgl&#261;d"
    origin "text"
  ]
  node [
    id 32
    label "finansowy"
    origin "text"
  ]
  node [
    id 33
    label "generalnie"
    origin "text"
  ]
  node [
    id 34
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 35
    label "prze&#347;ledzi&#263;"
    origin "text"
  ]
  node [
    id 36
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 37
    label "finansowa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "przedsi&#281;wzi&#281;cie"
    origin "text"
  ]
  node [
    id 39
    label "wskaza&#263;"
    origin "text"
  ]
  node [
    id 40
    label "dwa"
    origin "text"
  ]
  node [
    id 41
    label "&#378;r&#243;d&#322;o"
    origin "text"
  ]
  node [
    id 42
    label "samorz&#261;d"
    origin "text"
  ]
  node [
    id 43
    label "rodzic"
    origin "text"
  ]
  node [
    id 44
    label "szacowa&#263;"
    origin "text"
  ]
  node [
    id 45
    label "si&#281;"
    origin "text"
  ]
  node [
    id 46
    label "utrzymanie"
    origin "text"
  ]
  node [
    id 47
    label "jeden"
    origin "text"
  ]
  node [
    id 48
    label "dziecko"
    origin "text"
  ]
  node [
    id 49
    label "kosztowa&#263;"
    origin "text"
  ]
  node [
    id 50
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 51
    label "ogromny"
    origin "text"
  ]
  node [
    id 52
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 53
    label "dla"
    origin "text"
  ]
  node [
    id 54
    label "cz&#322;owiek"
  ]
  node [
    id 55
    label "profesor"
  ]
  node [
    id 56
    label "kszta&#322;ciciel"
  ]
  node [
    id 57
    label "jegomo&#347;&#263;"
  ]
  node [
    id 58
    label "zwrot"
  ]
  node [
    id 59
    label "pracodawca"
  ]
  node [
    id 60
    label "rz&#261;dzenie"
  ]
  node [
    id 61
    label "m&#261;&#380;"
  ]
  node [
    id 62
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 63
    label "ch&#322;opina"
  ]
  node [
    id 64
    label "bratek"
  ]
  node [
    id 65
    label "opiekun"
  ]
  node [
    id 66
    label "doros&#322;y"
  ]
  node [
    id 67
    label "preceptor"
  ]
  node [
    id 68
    label "Midas"
  ]
  node [
    id 69
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 70
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 71
    label "murza"
  ]
  node [
    id 72
    label "ojciec"
  ]
  node [
    id 73
    label "androlog"
  ]
  node [
    id 74
    label "pupil"
  ]
  node [
    id 75
    label "efendi"
  ]
  node [
    id 76
    label "nabab"
  ]
  node [
    id 77
    label "w&#322;odarz"
  ]
  node [
    id 78
    label "szkolnik"
  ]
  node [
    id 79
    label "pedagog"
  ]
  node [
    id 80
    label "popularyzator"
  ]
  node [
    id 81
    label "andropauza"
  ]
  node [
    id 82
    label "gra_w_karty"
  ]
  node [
    id 83
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 84
    label "Mieszko_I"
  ]
  node [
    id 85
    label "bogaty"
  ]
  node [
    id 86
    label "samiec"
  ]
  node [
    id 87
    label "przyw&#243;dca"
  ]
  node [
    id 88
    label "pa&#324;stwo"
  ]
  node [
    id 89
    label "belfer"
  ]
  node [
    id 90
    label "stopie&#324;_wojskowy"
  ]
  node [
    id 91
    label "dostojnik"
  ]
  node [
    id 92
    label "oficer"
  ]
  node [
    id 93
    label "parlamentarzysta"
  ]
  node [
    id 94
    label "Pi&#322;sudski"
  ]
  node [
    id 95
    label "warto&#347;ciowy"
  ]
  node [
    id 96
    label "du&#380;y"
  ]
  node [
    id 97
    label "wysoce"
  ]
  node [
    id 98
    label "daleki"
  ]
  node [
    id 99
    label "znaczny"
  ]
  node [
    id 100
    label "wysoko"
  ]
  node [
    id 101
    label "szczytnie"
  ]
  node [
    id 102
    label "wznios&#322;y"
  ]
  node [
    id 103
    label "wyrafinowany"
  ]
  node [
    id 104
    label "z_wysoka"
  ]
  node [
    id 105
    label "chwalebny"
  ]
  node [
    id 106
    label "uprzywilejowany"
  ]
  node [
    id 107
    label "niepo&#347;ledni"
  ]
  node [
    id 108
    label "pok&#243;j"
  ]
  node [
    id 109
    label "parlament"
  ]
  node [
    id 110
    label "zwi&#261;zek"
  ]
  node [
    id 111
    label "NIK"
  ]
  node [
    id 112
    label "urz&#261;d"
  ]
  node [
    id 113
    label "organ"
  ]
  node [
    id 114
    label "pomieszczenie"
  ]
  node [
    id 115
    label "d&#322;ugi"
  ]
  node [
    id 116
    label "wynik"
  ]
  node [
    id 117
    label "wyj&#347;cie"
  ]
  node [
    id 118
    label "spe&#322;nienie"
  ]
  node [
    id 119
    label "cesarskie_ci&#281;cie"
  ]
  node [
    id 120
    label "po&#322;o&#380;na"
  ]
  node [
    id 121
    label "proces_fizjologiczny"
  ]
  node [
    id 122
    label "przestanie"
  ]
  node [
    id 123
    label "marc&#243;wka"
  ]
  node [
    id 124
    label "usuni&#281;cie"
  ]
  node [
    id 125
    label "uniewa&#380;nienie"
  ]
  node [
    id 126
    label "pomys&#322;"
  ]
  node [
    id 127
    label "birth"
  ]
  node [
    id 128
    label "wymy&#347;lenie"
  ]
  node [
    id 129
    label "po&#322;&#243;g"
  ]
  node [
    id 130
    label "szok_poporodowy"
  ]
  node [
    id 131
    label "event"
  ]
  node [
    id 132
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 133
    label "rozwi&#261;zywanie_si&#281;"
  ]
  node [
    id 134
    label "dula"
  ]
  node [
    id 135
    label "sprawa"
  ]
  node [
    id 136
    label "problemat"
  ]
  node [
    id 137
    label "wypowied&#378;"
  ]
  node [
    id 138
    label "dialog"
  ]
  node [
    id 139
    label "problematyka"
  ]
  node [
    id 140
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 141
    label "subject"
  ]
  node [
    id 142
    label "capability"
  ]
  node [
    id 143
    label "zdolno&#347;&#263;"
  ]
  node [
    id 144
    label "potencja&#322;"
  ]
  node [
    id 145
    label "volunteer"
  ]
  node [
    id 146
    label "podwija&#263;"
  ]
  node [
    id 147
    label "make_bold"
  ]
  node [
    id 148
    label "umieszcza&#263;"
  ]
  node [
    id 149
    label "supply"
  ]
  node [
    id 150
    label "set"
  ]
  node [
    id 151
    label "ubiera&#263;"
  ]
  node [
    id 152
    label "invest"
  ]
  node [
    id 153
    label "p&#322;aci&#263;"
  ]
  node [
    id 154
    label "przewidywa&#263;"
  ]
  node [
    id 155
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 156
    label "obleka&#263;"
  ]
  node [
    id 157
    label "pokrywa&#263;"
  ]
  node [
    id 158
    label "odziewa&#263;"
  ]
  node [
    id 159
    label "organizowa&#263;"
  ]
  node [
    id 160
    label "wk&#322;ada&#263;"
  ]
  node [
    id 161
    label "robi&#263;"
  ]
  node [
    id 162
    label "nosi&#263;"
  ]
  node [
    id 163
    label "introduce"
  ]
  node [
    id 164
    label "obleka&#263;_si&#281;"
  ]
  node [
    id 165
    label "powodowa&#263;"
  ]
  node [
    id 166
    label "izba_wytrze&#378;wie&#324;"
  ]
  node [
    id 167
    label "wci&#281;cie"
  ]
  node [
    id 168
    label "plac&#243;wka_opieku&#324;czo-wychowawcza"
  ]
  node [
    id 169
    label "si&#281;ga&#263;"
  ]
  node [
    id 170
    label "trwa&#263;"
  ]
  node [
    id 171
    label "obecno&#347;&#263;"
  ]
  node [
    id 172
    label "stan"
  ]
  node [
    id 173
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 174
    label "stand"
  ]
  node [
    id 175
    label "mie&#263;_miejsce"
  ]
  node [
    id 176
    label "uczestniczy&#263;"
  ]
  node [
    id 177
    label "chodzi&#263;"
  ]
  node [
    id 178
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 179
    label "equal"
  ]
  node [
    id 180
    label "prawdziwy"
  ]
  node [
    id 181
    label "w_chuj"
  ]
  node [
    id 182
    label "pomy&#347;lny"
  ]
  node [
    id 183
    label "skuteczny"
  ]
  node [
    id 184
    label "moralny"
  ]
  node [
    id 185
    label "korzystny"
  ]
  node [
    id 186
    label "odpowiedni"
  ]
  node [
    id 187
    label "dobrze"
  ]
  node [
    id 188
    label "pozytywny"
  ]
  node [
    id 189
    label "grzeczny"
  ]
  node [
    id 190
    label "powitanie"
  ]
  node [
    id 191
    label "mi&#322;y"
  ]
  node [
    id 192
    label "dobroczynny"
  ]
  node [
    id 193
    label "pos&#322;uszny"
  ]
  node [
    id 194
    label "ca&#322;y"
  ]
  node [
    id 195
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 196
    label "czw&#243;rka"
  ]
  node [
    id 197
    label "spokojny"
  ]
  node [
    id 198
    label "&#347;mieszny"
  ]
  node [
    id 199
    label "drogi"
  ]
  node [
    id 200
    label "okres_czasu"
  ]
  node [
    id 201
    label "minute"
  ]
  node [
    id 202
    label "jednostka_geologiczna"
  ]
  node [
    id 203
    label "warto&#347;&#263;_oczekiwana"
  ]
  node [
    id 204
    label "time"
  ]
  node [
    id 205
    label "chron"
  ]
  node [
    id 206
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 207
    label "fragment"
  ]
  node [
    id 208
    label "opis"
  ]
  node [
    id 209
    label "analysis"
  ]
  node [
    id 210
    label "reakcja_chemiczna"
  ]
  node [
    id 211
    label "dissection"
  ]
  node [
    id 212
    label "badanie"
  ]
  node [
    id 213
    label "metoda"
  ]
  node [
    id 214
    label "record"
  ]
  node [
    id 215
    label "wytw&#243;r"
  ]
  node [
    id 216
    label "&#347;wiadectwo"
  ]
  node [
    id 217
    label "zapis"
  ]
  node [
    id 218
    label "raport&#243;wka"
  ]
  node [
    id 219
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 220
    label "artyku&#322;"
  ]
  node [
    id 221
    label "plik"
  ]
  node [
    id 222
    label "writing"
  ]
  node [
    id 223
    label "utw&#243;r"
  ]
  node [
    id 224
    label "dokumentacja"
  ]
  node [
    id 225
    label "registratura"
  ]
  node [
    id 226
    label "parafa"
  ]
  node [
    id 227
    label "sygnatariusz"
  ]
  node [
    id 228
    label "fascyku&#322;"
  ]
  node [
    id 229
    label "promocja"
  ]
  node [
    id 230
    label "give_birth"
  ]
  node [
    id 231
    label "wytworzy&#263;"
  ]
  node [
    id 232
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 233
    label "realize"
  ]
  node [
    id 234
    label "zrobi&#263;"
  ]
  node [
    id 235
    label "make"
  ]
  node [
    id 236
    label "doj&#347;cie"
  ]
  node [
    id 237
    label "doj&#347;&#263;"
  ]
  node [
    id 238
    label "powzi&#261;&#263;"
  ]
  node [
    id 239
    label "wiedza"
  ]
  node [
    id 240
    label "sygna&#322;"
  ]
  node [
    id 241
    label "obiegni&#281;cie"
  ]
  node [
    id 242
    label "obieganie"
  ]
  node [
    id 243
    label "obiec"
  ]
  node [
    id 244
    label "dane"
  ]
  node [
    id 245
    label "obiega&#263;"
  ]
  node [
    id 246
    label "punkt"
  ]
  node [
    id 247
    label "publikacja"
  ]
  node [
    id 248
    label "powzi&#281;cie"
  ]
  node [
    id 249
    label "przekwitanie"
  ]
  node [
    id 250
    label "uleganie"
  ]
  node [
    id 251
    label "ulega&#263;"
  ]
  node [
    id 252
    label "partner"
  ]
  node [
    id 253
    label "przyw&#243;dczyni"
  ]
  node [
    id 254
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 255
    label "ulec"
  ]
  node [
    id 256
    label "kobita"
  ]
  node [
    id 257
    label "&#322;ono"
  ]
  node [
    id 258
    label "kobieta"
  ]
  node [
    id 259
    label "w&#322;a&#347;cicielka"
  ]
  node [
    id 260
    label "m&#281;&#380;yna"
  ]
  node [
    id 261
    label "babka"
  ]
  node [
    id 262
    label "samica"
  ]
  node [
    id 263
    label "ulegni&#281;cie"
  ]
  node [
    id 264
    label "menopauza"
  ]
  node [
    id 265
    label "Goebbels"
  ]
  node [
    id 266
    label "Sto&#322;ypin"
  ]
  node [
    id 267
    label "rz&#261;d"
  ]
  node [
    id 268
    label "trza"
  ]
  node [
    id 269
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 270
    label "para"
  ]
  node [
    id 271
    label "necessity"
  ]
  node [
    id 272
    label "powiedzie&#263;"
  ]
  node [
    id 273
    label "testify"
  ]
  node [
    id 274
    label "uzna&#263;"
  ]
  node [
    id 275
    label "oznajmi&#263;"
  ]
  node [
    id 276
    label "declare"
  ]
  node [
    id 277
    label "rezultat"
  ]
  node [
    id 278
    label "okre&#347;lony"
  ]
  node [
    id 279
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 280
    label "Karta_Nauczyciela"
  ]
  node [
    id 281
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 282
    label "akt"
  ]
  node [
    id 283
    label "przej&#347;&#263;"
  ]
  node [
    id 284
    label "charter"
  ]
  node [
    id 285
    label "przej&#347;cie"
  ]
  node [
    id 286
    label "zwinnie"
  ]
  node [
    id 287
    label "bezpiecznie"
  ]
  node [
    id 288
    label "wiarygodnie"
  ]
  node [
    id 289
    label "pewniej"
  ]
  node [
    id 290
    label "pewny"
  ]
  node [
    id 291
    label "mocno"
  ]
  node [
    id 292
    label "najpewniej"
  ]
  node [
    id 293
    label "prze&#322;o&#380;y&#263;"
  ]
  node [
    id 294
    label "postpone"
  ]
  node [
    id 295
    label "pozostawi&#263;"
  ]
  node [
    id 296
    label "op&#243;&#378;ni&#263;"
  ]
  node [
    id 297
    label "znie&#347;&#263;"
  ]
  node [
    id 298
    label "zgromadzi&#263;"
  ]
  node [
    id 299
    label "wykre&#347;li&#263;"
  ]
  node [
    id 300
    label "zostawi&#263;"
  ]
  node [
    id 301
    label "po&#322;o&#380;y&#263;"
  ]
  node [
    id 302
    label "decelerate"
  ]
  node [
    id 303
    label "poczeka&#263;"
  ]
  node [
    id 304
    label "odej&#347;&#263;"
  ]
  node [
    id 305
    label "set_down"
  ]
  node [
    id 306
    label "czasokres"
  ]
  node [
    id 307
    label "trawienie"
  ]
  node [
    id 308
    label "kategoria_gramatyczna"
  ]
  node [
    id 309
    label "period"
  ]
  node [
    id 310
    label "odczyt"
  ]
  node [
    id 311
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 312
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 313
    label "chwila"
  ]
  node [
    id 314
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 315
    label "poprzedzenie"
  ]
  node [
    id 316
    label "koniugacja"
  ]
  node [
    id 317
    label "dzieje"
  ]
  node [
    id 318
    label "poprzedzi&#263;"
  ]
  node [
    id 319
    label "przep&#322;ywanie"
  ]
  node [
    id 320
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 321
    label "odwlekanie_si&#281;"
  ]
  node [
    id 322
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 323
    label "Zeitgeist"
  ]
  node [
    id 324
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 325
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 326
    label "pochodzi&#263;"
  ]
  node [
    id 327
    label "schy&#322;ek"
  ]
  node [
    id 328
    label "czwarty_wymiar"
  ]
  node [
    id 329
    label "chronometria"
  ]
  node [
    id 330
    label "poprzedzanie"
  ]
  node [
    id 331
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 332
    label "pogoda"
  ]
  node [
    id 333
    label "zegar"
  ]
  node [
    id 334
    label "trawi&#263;"
  ]
  node [
    id 335
    label "pochodzenie"
  ]
  node [
    id 336
    label "poprzedza&#263;"
  ]
  node [
    id 337
    label "time_period"
  ]
  node [
    id 338
    label "rachuba_czasu"
  ]
  node [
    id 339
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 340
    label "czasoprzestrze&#324;"
  ]
  node [
    id 341
    label "laba"
  ]
  node [
    id 342
    label "przyczyna"
  ]
  node [
    id 343
    label "uwaga"
  ]
  node [
    id 344
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 345
    label "punkt_widzenia"
  ]
  node [
    id 346
    label "mi&#281;dzybankowy"
  ]
  node [
    id 347
    label "finansowo"
  ]
  node [
    id 348
    label "fizyczny"
  ]
  node [
    id 349
    label "pozamaterialny"
  ]
  node [
    id 350
    label "materjalny"
  ]
  node [
    id 351
    label "ca&#322;o&#347;ciowo"
  ]
  node [
    id 352
    label "podstawowo"
  ]
  node [
    id 353
    label "porz&#261;dnie"
  ]
  node [
    id 354
    label "generalny"
  ]
  node [
    id 355
    label "zasadniczo"
  ]
  node [
    id 356
    label "zapozna&#263;_si&#281;"
  ]
  node [
    id 357
    label "model"
  ]
  node [
    id 358
    label "zbi&#243;r"
  ]
  node [
    id 359
    label "tryb"
  ]
  node [
    id 360
    label "narz&#281;dzie"
  ]
  node [
    id 361
    label "nature"
  ]
  node [
    id 362
    label "finance"
  ]
  node [
    id 363
    label "zrobienie"
  ]
  node [
    id 364
    label "consumption"
  ]
  node [
    id 365
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 366
    label "zacz&#281;cie"
  ]
  node [
    id 367
    label "startup"
  ]
  node [
    id 368
    label "plan"
  ]
  node [
    id 369
    label "aim"
  ]
  node [
    id 370
    label "pokaza&#263;"
  ]
  node [
    id 371
    label "podkre&#347;li&#263;"
  ]
  node [
    id 372
    label "wybra&#263;"
  ]
  node [
    id 373
    label "indicate"
  ]
  node [
    id 374
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 375
    label "poda&#263;"
  ]
  node [
    id 376
    label "picture"
  ]
  node [
    id 377
    label "point"
  ]
  node [
    id 378
    label "bra&#263;_si&#281;"
  ]
  node [
    id 379
    label "matuszka"
  ]
  node [
    id 380
    label "geneza"
  ]
  node [
    id 381
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 382
    label "kamena"
  ]
  node [
    id 383
    label "czynnik"
  ]
  node [
    id 384
    label "pocz&#261;tek"
  ]
  node [
    id 385
    label "poci&#261;ganie"
  ]
  node [
    id 386
    label "ciek_wodny"
  ]
  node [
    id 387
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 388
    label "autonomy"
  ]
  node [
    id 389
    label "wapniak"
  ]
  node [
    id 390
    label "rodzic_chrzestny"
  ]
  node [
    id 391
    label "cz&#322;onek_rodziny"
  ]
  node [
    id 392
    label "rodzice"
  ]
  node [
    id 393
    label "liczy&#263;"
  ]
  node [
    id 394
    label "okre&#347;la&#263;"
  ]
  node [
    id 395
    label "gauge"
  ]
  node [
    id 396
    label "potrzymanie"
  ]
  node [
    id 397
    label "manewr"
  ]
  node [
    id 398
    label "podtrzymanie"
  ]
  node [
    id 399
    label "zachowanie"
  ]
  node [
    id 400
    label "obronienie"
  ]
  node [
    id 401
    label "byt"
  ]
  node [
    id 402
    label "zdo&#322;anie"
  ]
  node [
    id 403
    label "zapewnienie"
  ]
  node [
    id 404
    label "bearing"
  ]
  node [
    id 405
    label "subsystencja"
  ]
  node [
    id 406
    label "wy&#380;ywienie"
  ]
  node [
    id 407
    label "wychowanie"
  ]
  node [
    id 408
    label "uniesienie"
  ]
  node [
    id 409
    label "preservation"
  ]
  node [
    id 410
    label "zap&#322;acenie"
  ]
  node [
    id 411
    label "przetrzymanie"
  ]
  node [
    id 412
    label "kieliszek"
  ]
  node [
    id 413
    label "shot"
  ]
  node [
    id 414
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 415
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 416
    label "jaki&#347;"
  ]
  node [
    id 417
    label "jednolicie"
  ]
  node [
    id 418
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 419
    label "w&#243;dka"
  ]
  node [
    id 420
    label "ujednolicenie"
  ]
  node [
    id 421
    label "jednakowy"
  ]
  node [
    id 422
    label "potomstwo"
  ]
  node [
    id 423
    label "organizm"
  ]
  node [
    id 424
    label "sraluch"
  ]
  node [
    id 425
    label "utulanie"
  ]
  node [
    id 426
    label "pediatra"
  ]
  node [
    id 427
    label "dzieciarnia"
  ]
  node [
    id 428
    label "m&#322;odziak"
  ]
  node [
    id 429
    label "dzieciak"
  ]
  node [
    id 430
    label "utula&#263;"
  ]
  node [
    id 431
    label "potomek"
  ]
  node [
    id 432
    label "pedofil"
  ]
  node [
    id 433
    label "entliczek-pentliczek"
  ]
  node [
    id 434
    label "m&#322;odzik"
  ]
  node [
    id 435
    label "cz&#322;owieczek"
  ]
  node [
    id 436
    label "zwierz&#281;"
  ]
  node [
    id 437
    label "niepe&#322;noletni"
  ]
  node [
    id 438
    label "fledgling"
  ]
  node [
    id 439
    label "utuli&#263;"
  ]
  node [
    id 440
    label "utulenie"
  ]
  node [
    id 441
    label "cena"
  ]
  node [
    id 442
    label "try"
  ]
  node [
    id 443
    label "essay"
  ]
  node [
    id 444
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 445
    label "doznawa&#263;"
  ]
  node [
    id 446
    label "savor"
  ]
  node [
    id 447
    label "szlachetny"
  ]
  node [
    id 448
    label "metaliczny"
  ]
  node [
    id 449
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 450
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 451
    label "grosz"
  ]
  node [
    id 452
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 453
    label "utytu&#322;owany"
  ]
  node [
    id 454
    label "poz&#322;ocenie"
  ]
  node [
    id 455
    label "Polska"
  ]
  node [
    id 456
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 457
    label "wspania&#322;y"
  ]
  node [
    id 458
    label "doskona&#322;y"
  ]
  node [
    id 459
    label "kochany"
  ]
  node [
    id 460
    label "jednostka_monetarna"
  ]
  node [
    id 461
    label "z&#322;ocenie"
  ]
  node [
    id 462
    label "olbrzymio"
  ]
  node [
    id 463
    label "wyj&#261;tkowy"
  ]
  node [
    id 464
    label "ogromnie"
  ]
  node [
    id 465
    label "jebitny"
  ]
  node [
    id 466
    label "wa&#380;ny"
  ]
  node [
    id 467
    label "liczny"
  ]
  node [
    id 468
    label "dono&#347;ny"
  ]
  node [
    id 469
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 470
    label "zdewaluowa&#263;"
  ]
  node [
    id 471
    label "moniak"
  ]
  node [
    id 472
    label "zdewaluowanie"
  ]
  node [
    id 473
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 474
    label "numizmat"
  ]
  node [
    id 475
    label "rozmieni&#263;"
  ]
  node [
    id 476
    label "rozmienienie"
  ]
  node [
    id 477
    label "rozmienianie"
  ]
  node [
    id 478
    label "dewaluowanie"
  ]
  node [
    id 479
    label "nomina&#322;"
  ]
  node [
    id 480
    label "coin"
  ]
  node [
    id 481
    label "dewaluowa&#263;"
  ]
  node [
    id 482
    label "pieni&#261;dze"
  ]
  node [
    id 483
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 484
    label "rozmienia&#263;"
  ]
  node [
    id 485
    label "Jerzy"
  ]
  node [
    id 486
    label "Wenderlich"
  ]
  node [
    id 487
    label "Zbys&#322;awa"
  ]
  node [
    id 488
    label "owczarski"
  ]
  node [
    id 489
    label "prawo"
  ]
  node [
    id 490
    label "i"
  ]
  node [
    id 491
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 492
    label "Kluzik"
  ]
  node [
    id 493
    label "Rostkowska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 0
    target 81
  ]
  edge [
    source 0
    target 82
  ]
  edge [
    source 0
    target 83
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 2
    target 102
  ]
  edge [
    source 2
    target 103
  ]
  edge [
    source 2
    target 104
  ]
  edge [
    source 2
    target 105
  ]
  edge [
    source 2
    target 106
  ]
  edge [
    source 2
    target 107
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 110
  ]
  edge [
    source 3
    target 111
  ]
  edge [
    source 3
    target 112
  ]
  edge [
    source 3
    target 113
  ]
  edge [
    source 3
    target 114
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 9
    target 150
  ]
  edge [
    source 9
    target 151
  ]
  edge [
    source 9
    target 152
  ]
  edge [
    source 9
    target 153
  ]
  edge [
    source 9
    target 154
  ]
  edge [
    source 9
    target 155
  ]
  edge [
    source 9
    target 156
  ]
  edge [
    source 9
    target 157
  ]
  edge [
    source 9
    target 158
  ]
  edge [
    source 9
    target 159
  ]
  edge [
    source 9
    target 160
  ]
  edge [
    source 9
    target 161
  ]
  edge [
    source 9
    target 162
  ]
  edge [
    source 9
    target 163
  ]
  edge [
    source 9
    target 164
  ]
  edge [
    source 9
    target 165
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 48
  ]
  edge [
    source 10
    target 49
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 28
  ]
  edge [
    source 11
    target 29
  ]
  edge [
    source 11
    target 50
  ]
  edge [
    source 11
    target 51
  ]
  edge [
    source 11
    target 169
  ]
  edge [
    source 11
    target 170
  ]
  edge [
    source 11
    target 171
  ]
  edge [
    source 11
    target 172
  ]
  edge [
    source 11
    target 173
  ]
  edge [
    source 11
    target 174
  ]
  edge [
    source 11
    target 175
  ]
  edge [
    source 11
    target 176
  ]
  edge [
    source 11
    target 177
  ]
  edge [
    source 11
    target 178
  ]
  edge [
    source 11
    target 179
  ]
  edge [
    source 11
    target 49
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 181
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 14
    target 183
  ]
  edge [
    source 14
    target 184
  ]
  edge [
    source 14
    target 185
  ]
  edge [
    source 14
    target 186
  ]
  edge [
    source 14
    target 58
  ]
  edge [
    source 14
    target 187
  ]
  edge [
    source 14
    target 188
  ]
  edge [
    source 14
    target 189
  ]
  edge [
    source 14
    target 190
  ]
  edge [
    source 14
    target 191
  ]
  edge [
    source 14
    target 192
  ]
  edge [
    source 14
    target 193
  ]
  edge [
    source 14
    target 194
  ]
  edge [
    source 14
    target 195
  ]
  edge [
    source 14
    target 196
  ]
  edge [
    source 14
    target 197
  ]
  edge [
    source 14
    target 198
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 35
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 200
  ]
  edge [
    source 15
    target 201
  ]
  edge [
    source 15
    target 202
  ]
  edge [
    source 15
    target 203
  ]
  edge [
    source 15
    target 204
  ]
  edge [
    source 15
    target 205
  ]
  edge [
    source 15
    target 206
  ]
  edge [
    source 15
    target 207
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 208
  ]
  edge [
    source 16
    target 209
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 211
  ]
  edge [
    source 16
    target 212
  ]
  edge [
    source 16
    target 213
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 214
  ]
  edge [
    source 17
    target 215
  ]
  edge [
    source 17
    target 216
  ]
  edge [
    source 17
    target 217
  ]
  edge [
    source 17
    target 218
  ]
  edge [
    source 17
    target 219
  ]
  edge [
    source 17
    target 220
  ]
  edge [
    source 17
    target 221
  ]
  edge [
    source 17
    target 222
  ]
  edge [
    source 17
    target 223
  ]
  edge [
    source 17
    target 224
  ]
  edge [
    source 17
    target 225
  ]
  edge [
    source 17
    target 226
  ]
  edge [
    source 17
    target 227
  ]
  edge [
    source 17
    target 228
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 229
  ]
  edge [
    source 18
    target 230
  ]
  edge [
    source 18
    target 231
  ]
  edge [
    source 18
    target 232
  ]
  edge [
    source 18
    target 233
  ]
  edge [
    source 18
    target 234
  ]
  edge [
    source 18
    target 235
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 236
  ]
  edge [
    source 19
    target 237
  ]
  edge [
    source 19
    target 238
  ]
  edge [
    source 19
    target 239
  ]
  edge [
    source 19
    target 240
  ]
  edge [
    source 19
    target 241
  ]
  edge [
    source 19
    target 242
  ]
  edge [
    source 19
    target 243
  ]
  edge [
    source 19
    target 244
  ]
  edge [
    source 19
    target 245
  ]
  edge [
    source 19
    target 246
  ]
  edge [
    source 19
    target 247
  ]
  edge [
    source 19
    target 248
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 54
  ]
  edge [
    source 20
    target 249
  ]
  edge [
    source 20
    target 58
  ]
  edge [
    source 20
    target 250
  ]
  edge [
    source 20
    target 251
  ]
  edge [
    source 20
    target 252
  ]
  edge [
    source 20
    target 66
  ]
  edge [
    source 20
    target 253
  ]
  edge [
    source 20
    target 70
  ]
  edge [
    source 20
    target 254
  ]
  edge [
    source 20
    target 255
  ]
  edge [
    source 20
    target 256
  ]
  edge [
    source 20
    target 257
  ]
  edge [
    source 20
    target 258
  ]
  edge [
    source 20
    target 259
  ]
  edge [
    source 20
    target 260
  ]
  edge [
    source 20
    target 261
  ]
  edge [
    source 20
    target 262
  ]
  edge [
    source 20
    target 88
  ]
  edge [
    source 20
    target 263
  ]
  edge [
    source 20
    target 264
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 265
  ]
  edge [
    source 21
    target 266
  ]
  edge [
    source 21
    target 267
  ]
  edge [
    source 21
    target 91
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 38
  ]
  edge [
    source 23
    target 39
  ]
  edge [
    source 23
    target 268
  ]
  edge [
    source 23
    target 176
  ]
  edge [
    source 23
    target 269
  ]
  edge [
    source 23
    target 270
  ]
  edge [
    source 23
    target 271
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 272
  ]
  edge [
    source 24
    target 273
  ]
  edge [
    source 24
    target 274
  ]
  edge [
    source 24
    target 275
  ]
  edge [
    source 24
    target 276
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 277
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 278
  ]
  edge [
    source 26
    target 279
  ]
  edge [
    source 26
    target 47
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 280
  ]
  edge [
    source 27
    target 123
  ]
  edge [
    source 27
    target 281
  ]
  edge [
    source 27
    target 282
  ]
  edge [
    source 27
    target 283
  ]
  edge [
    source 27
    target 284
  ]
  edge [
    source 27
    target 285
  ]
  edge [
    source 28
    target 286
  ]
  edge [
    source 28
    target 287
  ]
  edge [
    source 28
    target 288
  ]
  edge [
    source 28
    target 289
  ]
  edge [
    source 28
    target 290
  ]
  edge [
    source 28
    target 291
  ]
  edge [
    source 28
    target 292
  ]
  edge [
    source 28
    target 47
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 293
  ]
  edge [
    source 29
    target 294
  ]
  edge [
    source 29
    target 295
  ]
  edge [
    source 29
    target 296
  ]
  edge [
    source 29
    target 297
  ]
  edge [
    source 29
    target 298
  ]
  edge [
    source 29
    target 299
  ]
  edge [
    source 29
    target 300
  ]
  edge [
    source 29
    target 301
  ]
  edge [
    source 29
    target 302
  ]
  edge [
    source 29
    target 303
  ]
  edge [
    source 29
    target 304
  ]
  edge [
    source 29
    target 305
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 308
  ]
  edge [
    source 30
    target 309
  ]
  edge [
    source 30
    target 310
  ]
  edge [
    source 30
    target 311
  ]
  edge [
    source 30
    target 312
  ]
  edge [
    source 30
    target 313
  ]
  edge [
    source 30
    target 314
  ]
  edge [
    source 30
    target 315
  ]
  edge [
    source 30
    target 316
  ]
  edge [
    source 30
    target 317
  ]
  edge [
    source 30
    target 318
  ]
  edge [
    source 30
    target 319
  ]
  edge [
    source 30
    target 320
  ]
  edge [
    source 30
    target 321
  ]
  edge [
    source 30
    target 322
  ]
  edge [
    source 30
    target 323
  ]
  edge [
    source 30
    target 324
  ]
  edge [
    source 30
    target 200
  ]
  edge [
    source 30
    target 325
  ]
  edge [
    source 30
    target 326
  ]
  edge [
    source 30
    target 327
  ]
  edge [
    source 30
    target 328
  ]
  edge [
    source 30
    target 329
  ]
  edge [
    source 30
    target 330
  ]
  edge [
    source 30
    target 331
  ]
  edge [
    source 30
    target 332
  ]
  edge [
    source 30
    target 333
  ]
  edge [
    source 30
    target 334
  ]
  edge [
    source 30
    target 335
  ]
  edge [
    source 30
    target 336
  ]
  edge [
    source 30
    target 337
  ]
  edge [
    source 30
    target 338
  ]
  edge [
    source 30
    target 339
  ]
  edge [
    source 30
    target 340
  ]
  edge [
    source 30
    target 341
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 342
  ]
  edge [
    source 31
    target 343
  ]
  edge [
    source 31
    target 344
  ]
  edge [
    source 31
    target 345
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 346
  ]
  edge [
    source 32
    target 347
  ]
  edge [
    source 32
    target 348
  ]
  edge [
    source 32
    target 349
  ]
  edge [
    source 32
    target 350
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 351
  ]
  edge [
    source 33
    target 352
  ]
  edge [
    source 33
    target 353
  ]
  edge [
    source 33
    target 354
  ]
  edge [
    source 33
    target 355
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 356
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 357
  ]
  edge [
    source 36
    target 358
  ]
  edge [
    source 36
    target 359
  ]
  edge [
    source 36
    target 360
  ]
  edge [
    source 36
    target 361
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 362
  ]
  edge [
    source 37
    target 153
  ]
  edge [
    source 38
    target 363
  ]
  edge [
    source 38
    target 364
  ]
  edge [
    source 38
    target 365
  ]
  edge [
    source 38
    target 366
  ]
  edge [
    source 38
    target 367
  ]
  edge [
    source 38
    target 368
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 369
  ]
  edge [
    source 39
    target 370
  ]
  edge [
    source 39
    target 371
  ]
  edge [
    source 39
    target 372
  ]
  edge [
    source 39
    target 373
  ]
  edge [
    source 39
    target 374
  ]
  edge [
    source 39
    target 375
  ]
  edge [
    source 39
    target 376
  ]
  edge [
    source 39
    target 377
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 378
  ]
  edge [
    source 41
    target 216
  ]
  edge [
    source 41
    target 342
  ]
  edge [
    source 41
    target 379
  ]
  edge [
    source 41
    target 380
  ]
  edge [
    source 41
    target 381
  ]
  edge [
    source 41
    target 382
  ]
  edge [
    source 41
    target 219
  ]
  edge [
    source 41
    target 383
  ]
  edge [
    source 41
    target 384
  ]
  edge [
    source 41
    target 385
  ]
  edge [
    source 41
    target 277
  ]
  edge [
    source 41
    target 386
  ]
  edge [
    source 41
    target 387
  ]
  edge [
    source 41
    target 141
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 53
  ]
  edge [
    source 42
    target 51
  ]
  edge [
    source 42
    target 388
  ]
  edge [
    source 42
    target 113
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 53
  ]
  edge [
    source 43
    target 65
  ]
  edge [
    source 43
    target 389
  ]
  edge [
    source 43
    target 390
  ]
  edge [
    source 43
    target 391
  ]
  edge [
    source 43
    target 392
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 393
  ]
  edge [
    source 44
    target 394
  ]
  edge [
    source 44
    target 395
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 396
  ]
  edge [
    source 46
    target 363
  ]
  edge [
    source 46
    target 397
  ]
  edge [
    source 46
    target 398
  ]
  edge [
    source 46
    target 399
  ]
  edge [
    source 46
    target 400
  ]
  edge [
    source 46
    target 401
  ]
  edge [
    source 46
    target 402
  ]
  edge [
    source 46
    target 403
  ]
  edge [
    source 46
    target 404
  ]
  edge [
    source 46
    target 405
  ]
  edge [
    source 46
    target 406
  ]
  edge [
    source 46
    target 407
  ]
  edge [
    source 46
    target 408
  ]
  edge [
    source 46
    target 409
  ]
  edge [
    source 46
    target 410
  ]
  edge [
    source 46
    target 411
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 412
  ]
  edge [
    source 47
    target 413
  ]
  edge [
    source 47
    target 414
  ]
  edge [
    source 47
    target 415
  ]
  edge [
    source 47
    target 416
  ]
  edge [
    source 47
    target 417
  ]
  edge [
    source 47
    target 418
  ]
  edge [
    source 47
    target 419
  ]
  edge [
    source 47
    target 420
  ]
  edge [
    source 47
    target 421
  ]
  edge [
    source 48
    target 54
  ]
  edge [
    source 48
    target 422
  ]
  edge [
    source 48
    target 423
  ]
  edge [
    source 48
    target 424
  ]
  edge [
    source 48
    target 425
  ]
  edge [
    source 48
    target 426
  ]
  edge [
    source 48
    target 427
  ]
  edge [
    source 48
    target 428
  ]
  edge [
    source 48
    target 429
  ]
  edge [
    source 48
    target 430
  ]
  edge [
    source 48
    target 431
  ]
  edge [
    source 48
    target 432
  ]
  edge [
    source 48
    target 433
  ]
  edge [
    source 48
    target 434
  ]
  edge [
    source 48
    target 435
  ]
  edge [
    source 48
    target 436
  ]
  edge [
    source 48
    target 437
  ]
  edge [
    source 48
    target 438
  ]
  edge [
    source 48
    target 439
  ]
  edge [
    source 48
    target 440
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 441
  ]
  edge [
    source 49
    target 442
  ]
  edge [
    source 49
    target 443
  ]
  edge [
    source 49
    target 444
  ]
  edge [
    source 49
    target 445
  ]
  edge [
    source 49
    target 446
  ]
  edge [
    source 50
    target 447
  ]
  edge [
    source 50
    target 448
  ]
  edge [
    source 50
    target 449
  ]
  edge [
    source 50
    target 450
  ]
  edge [
    source 50
    target 451
  ]
  edge [
    source 50
    target 452
  ]
  edge [
    source 50
    target 453
  ]
  edge [
    source 50
    target 454
  ]
  edge [
    source 50
    target 455
  ]
  edge [
    source 50
    target 456
  ]
  edge [
    source 50
    target 457
  ]
  edge [
    source 50
    target 458
  ]
  edge [
    source 50
    target 459
  ]
  edge [
    source 50
    target 460
  ]
  edge [
    source 50
    target 461
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 462
  ]
  edge [
    source 51
    target 463
  ]
  edge [
    source 51
    target 464
  ]
  edge [
    source 51
    target 99
  ]
  edge [
    source 51
    target 465
  ]
  edge [
    source 51
    target 180
  ]
  edge [
    source 51
    target 466
  ]
  edge [
    source 51
    target 467
  ]
  edge [
    source 51
    target 468
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 469
  ]
  edge [
    source 52
    target 470
  ]
  edge [
    source 52
    target 471
  ]
  edge [
    source 52
    target 215
  ]
  edge [
    source 52
    target 472
  ]
  edge [
    source 52
    target 460
  ]
  edge [
    source 52
    target 473
  ]
  edge [
    source 52
    target 474
  ]
  edge [
    source 52
    target 475
  ]
  edge [
    source 52
    target 476
  ]
  edge [
    source 52
    target 477
  ]
  edge [
    source 52
    target 478
  ]
  edge [
    source 52
    target 479
  ]
  edge [
    source 52
    target 480
  ]
  edge [
    source 52
    target 481
  ]
  edge [
    source 52
    target 482
  ]
  edge [
    source 52
    target 483
  ]
  edge [
    source 52
    target 484
  ]
  edge [
    source 485
    target 486
  ]
  edge [
    source 487
    target 488
  ]
  edge [
    source 489
    target 490
  ]
  edge [
    source 489
    target 491
  ]
  edge [
    source 490
    target 491
  ]
  edge [
    source 492
    target 493
  ]
]
