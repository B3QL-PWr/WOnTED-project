graph [
  maxDegree 13
  minDegree 1
  meanDegree 2
  density 0.04081632653061224
  graphCliqueNumber 3
  node [
    id 0
    label "leszek"
    origin "text"
  ]
  node [
    id 1
    label "mazowiecki"
    origin "text"
  ]
  node [
    id 2
    label "urodzony"
    origin "text"
  ]
  node [
    id 3
    label "lub"
    origin "text"
  ]
  node [
    id 4
    label "nieco"
    origin "text"
  ]
  node [
    id 5
    label "wczesno"
    origin "text"
  ]
  node [
    id 6
    label "zmar&#322;"
    origin "text"
  ]
  node [
    id 7
    label "ksi&#261;&#380;&#281;"
    origin "text"
  ]
  node [
    id 8
    label "lato"
    origin "text"
  ]
  node [
    id 9
    label "polski"
  ]
  node [
    id 10
    label "po_mazowiecku"
  ]
  node [
    id 11
    label "regionalny"
  ]
  node [
    id 12
    label "wysoko_urodzony"
  ]
  node [
    id 13
    label "zawo&#322;any"
  ]
  node [
    id 14
    label "wcze&#347;nie"
  ]
  node [
    id 15
    label "Hamlet"
  ]
  node [
    id 16
    label "Kazimierz_II_Sprawiedliwy"
  ]
  node [
    id 17
    label "Piast"
  ]
  node [
    id 18
    label "arystokrata"
  ]
  node [
    id 19
    label "tytu&#322;"
  ]
  node [
    id 20
    label "kochanie"
  ]
  node [
    id 21
    label "Bismarck"
  ]
  node [
    id 22
    label "Herman"
  ]
  node [
    id 23
    label "Mieszko_I"
  ]
  node [
    id 24
    label "w&#322;adca"
  ]
  node [
    id 25
    label "fircyk"
  ]
  node [
    id 26
    label "pora_roku"
  ]
  node [
    id 27
    label "Leszek"
  ]
  node [
    id 28
    label "Wierzchos&#322;awa"
  ]
  node [
    id 29
    label "nowogrodzki"
  ]
  node [
    id 30
    label "Boles&#322;awa"
  ]
  node [
    id 31
    label "IV"
  ]
  node [
    id 32
    label "k&#281;dzierzawy"
  ]
  node [
    id 33
    label "iii"
  ]
  node [
    id 34
    label "Krzywousty"
  ]
  node [
    id 35
    label "Boles&#322;awowica"
  ]
  node [
    id 36
    label "anonim"
  ]
  node [
    id 37
    label "tzw"
  ]
  node [
    id 38
    label "Galla"
  ]
  node [
    id 39
    label "kronika"
  ]
  node [
    id 40
    label "Kazimierz"
  ]
  node [
    id 41
    label "ii"
  ]
  node [
    id 42
    label "sprawiedliwy"
  ]
  node [
    id 43
    label "Wincenty"
  ]
  node [
    id 44
    label "kad&#322;ubek"
  ]
  node [
    id 45
    label "ksi&#281;stwo"
  ]
  node [
    id 46
    label "brzeski"
  ]
  node [
    id 47
    label "mieszka&#263;"
  ]
  node [
    id 48
    label "stary"
  ]
  node [
    id 49
    label "m&#322;ody"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 15
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 25
  ]
  edge [
    source 8
    target 26
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 32
  ]
  edge [
    source 30
    target 33
  ]
  edge [
    source 30
    target 34
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 47
  ]
  edge [
    source 33
    target 48
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
]
