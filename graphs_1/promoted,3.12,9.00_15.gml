graph [
  maxDegree 19
  minDegree 1
  meanDegree 2.026666666666667
  density 0.02738738738738739
  graphCliqueNumber 2
  node [
    id 0
    label "ameryka&#324;ski"
    origin "text"
  ]
  node [
    id 1
    label "media"
    origin "text"
  ]
  node [
    id 2
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 3
    label "nawo&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "eksterminacja"
    origin "text"
  ]
  node [
    id 5
    label "nar&#243;d"
    origin "text"
  ]
  node [
    id 6
    label "palesty&#324;ski"
    origin "text"
  ]
  node [
    id 7
    label "negowa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "istnienie"
    origin "text"
  ]
  node [
    id 9
    label "nowoczesny"
  ]
  node [
    id 10
    label "p&#243;&#322;nocnoameryka&#324;ski"
  ]
  node [
    id 11
    label "boston"
  ]
  node [
    id 12
    label "po_ameryka&#324;sku"
  ]
  node [
    id 13
    label "cake-walk"
  ]
  node [
    id 14
    label "charakterystyczny"
  ]
  node [
    id 15
    label "placek_ameryka&#324;ski"
  ]
  node [
    id 16
    label "fajny"
  ]
  node [
    id 17
    label "j&#281;zyk_angielski"
  ]
  node [
    id 18
    label "Princeton"
  ]
  node [
    id 19
    label "pepperoni"
  ]
  node [
    id 20
    label "sa&#322;atka_Snickers"
  ]
  node [
    id 21
    label "zachodni"
  ]
  node [
    id 22
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 23
    label "anglosaski"
  ]
  node [
    id 24
    label "typowy"
  ]
  node [
    id 25
    label "przekazior"
  ]
  node [
    id 26
    label "mass-media"
  ]
  node [
    id 27
    label "uzbrajanie"
  ]
  node [
    id 28
    label "&#347;rodek_komunikacji"
  ]
  node [
    id 29
    label "&#378;r&#243;d&#322;o_informacji"
  ]
  node [
    id 30
    label "medium"
  ]
  node [
    id 31
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 32
    label "free"
  ]
  node [
    id 33
    label "urge"
  ]
  node [
    id 34
    label "prosi&#263;"
  ]
  node [
    id 35
    label "address"
  ]
  node [
    id 36
    label "wzywa&#263;"
  ]
  node [
    id 37
    label "mord"
  ]
  node [
    id 38
    label "genocide"
  ]
  node [
    id 39
    label "Samojedzi"
  ]
  node [
    id 40
    label "nacja"
  ]
  node [
    id 41
    label "Aztekowie"
  ]
  node [
    id 42
    label "Irokezi"
  ]
  node [
    id 43
    label "Buriaci"
  ]
  node [
    id 44
    label "Komancze"
  ]
  node [
    id 45
    label "t&#322;um"
  ]
  node [
    id 46
    label "ludno&#347;&#263;"
  ]
  node [
    id 47
    label "lud"
  ]
  node [
    id 48
    label "Siuksowie"
  ]
  node [
    id 49
    label "Czejenowie"
  ]
  node [
    id 50
    label "Wotiacy"
  ]
  node [
    id 51
    label "Baszkirzy"
  ]
  node [
    id 52
    label "mniejszo&#347;&#263;_narodowa"
  ]
  node [
    id 53
    label "Mohikanie"
  ]
  node [
    id 54
    label "Apacze"
  ]
  node [
    id 55
    label "Syngalezi"
  ]
  node [
    id 56
    label "kwestionowa&#263;"
  ]
  node [
    id 57
    label "zapiera&#263;"
  ]
  node [
    id 58
    label "repudiate"
  ]
  node [
    id 59
    label "produkowanie"
  ]
  node [
    id 60
    label "robienie"
  ]
  node [
    id 61
    label "byt"
  ]
  node [
    id 62
    label "bycie"
  ]
  node [
    id 63
    label "utrzymywa&#263;"
  ]
  node [
    id 64
    label "znikni&#281;cie"
  ]
  node [
    id 65
    label "utrzyma&#263;"
  ]
  node [
    id 66
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 67
    label "urzeczywistnianie"
  ]
  node [
    id 68
    label "entity"
  ]
  node [
    id 69
    label "egzystencja"
  ]
  node [
    id 70
    label "wyprodukowanie"
  ]
  node [
    id 71
    label "dematerializowanie_si&#281;"
  ]
  node [
    id 72
    label "utrzymanie"
  ]
  node [
    id 73
    label "utrzymywanie"
  ]
  node [
    id 74
    label "being"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 2
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
]
