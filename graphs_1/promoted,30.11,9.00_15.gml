graph [
  maxDegree 29
  minDegree 1
  meanDegree 2
  density 0.022222222222222223
  graphCliqueNumber 3
  node [
    id 0
    label "kiedy"
    origin "text"
  ]
  node [
    id 1
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 2
    label "pomaga&#263;"
    origin "text"
  ]
  node [
    id 3
    label "rodzinny"
    origin "text"
  ]
  node [
    id 4
    label "firma"
    origin "text"
  ]
  node [
    id 5
    label "prosty"
    origin "text"
  ]
  node [
    id 6
    label "roznosi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "ulotka"
    origin "text"
  ]
  node [
    id 8
    label "open"
  ]
  node [
    id 9
    label "odejmowa&#263;"
  ]
  node [
    id 10
    label "mie&#263;_miejsce"
  ]
  node [
    id 11
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 12
    label "set_about"
  ]
  node [
    id 13
    label "begin"
  ]
  node [
    id 14
    label "post&#281;powa&#263;"
  ]
  node [
    id 15
    label "bankrupt"
  ]
  node [
    id 16
    label "sprzyja&#263;"
  ]
  node [
    id 17
    label "back"
  ]
  node [
    id 18
    label "skutkowa&#263;"
  ]
  node [
    id 19
    label "concur"
  ]
  node [
    id 20
    label "Warszawa"
  ]
  node [
    id 21
    label "robi&#263;"
  ]
  node [
    id 22
    label "aid"
  ]
  node [
    id 23
    label "u&#322;atwia&#263;"
  ]
  node [
    id 24
    label "powodowa&#263;"
  ]
  node [
    id 25
    label "digest"
  ]
  node [
    id 26
    label "familijnie"
  ]
  node [
    id 27
    label "rodzinnie"
  ]
  node [
    id 28
    label "przyjemny"
  ]
  node [
    id 29
    label "wsp&#243;lny"
  ]
  node [
    id 30
    label "charakterystyczny"
  ]
  node [
    id 31
    label "swobodny"
  ]
  node [
    id 32
    label "zwi&#261;zany"
  ]
  node [
    id 33
    label "towarzyski"
  ]
  node [
    id 34
    label "ciep&#322;y"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "Hortex"
  ]
  node [
    id 37
    label "MAC"
  ]
  node [
    id 38
    label "reengineering"
  ]
  node [
    id 39
    label "nazwa_w&#322;asna"
  ]
  node [
    id 40
    label "podmiot_gospodarczy"
  ]
  node [
    id 41
    label "Google"
  ]
  node [
    id 42
    label "zaufanie"
  ]
  node [
    id 43
    label "biurowiec"
  ]
  node [
    id 44
    label "networking"
  ]
  node [
    id 45
    label "zasoby_ludzkie"
  ]
  node [
    id 46
    label "interes"
  ]
  node [
    id 47
    label "paczkarnia"
  ]
  node [
    id 48
    label "Canon"
  ]
  node [
    id 49
    label "HP"
  ]
  node [
    id 50
    label "Baltona"
  ]
  node [
    id 51
    label "Pewex"
  ]
  node [
    id 52
    label "MAN_SE"
  ]
  node [
    id 53
    label "Apeks"
  ]
  node [
    id 54
    label "zasoby"
  ]
  node [
    id 55
    label "Orbis"
  ]
  node [
    id 56
    label "miejsce_pracy"
  ]
  node [
    id 57
    label "siedziba"
  ]
  node [
    id 58
    label "Spo&#322;em"
  ]
  node [
    id 59
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 60
    label "Orlen"
  ]
  node [
    id 61
    label "klasa"
  ]
  node [
    id 62
    label "&#322;atwy"
  ]
  node [
    id 63
    label "prostowanie_si&#281;"
  ]
  node [
    id 64
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 65
    label "rozprostowanie"
  ]
  node [
    id 66
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 67
    label "prostoduszny"
  ]
  node [
    id 68
    label "naturalny"
  ]
  node [
    id 69
    label "naiwny"
  ]
  node [
    id 70
    label "cios"
  ]
  node [
    id 71
    label "prostowanie"
  ]
  node [
    id 72
    label "niepozorny"
  ]
  node [
    id 73
    label "zwyk&#322;y"
  ]
  node [
    id 74
    label "prosto"
  ]
  node [
    id 75
    label "po_prostu"
  ]
  node [
    id 76
    label "skromny"
  ]
  node [
    id 77
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 78
    label "niszczy&#263;"
  ]
  node [
    id 79
    label "ogarnia&#263;"
  ]
  node [
    id 80
    label "zanosi&#263;"
  ]
  node [
    id 81
    label "rozpowszechnia&#263;"
  ]
  node [
    id 82
    label "przenosi&#263;"
  ]
  node [
    id 83
    label "explode"
  ]
  node [
    id 84
    label "rumor"
  ]
  node [
    id 85
    label "circulate"
  ]
  node [
    id 86
    label "rozprzestrzenia&#263;"
  ]
  node [
    id 87
    label "deliver"
  ]
  node [
    id 88
    label "booklet"
  ]
  node [
    id 89
    label "druk_ulotny"
  ]
  node [
    id 90
    label "reklama"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
]
