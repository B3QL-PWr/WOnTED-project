graph [
  maxDegree 26
  minDegree 1
  meanDegree 1.9534883720930232
  density 0.046511627906976744
  graphCliqueNumber 2
  node [
    id 0
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 1
    label "moderacja"
    origin "text"
  ]
  node [
    id 2
    label "obcina&#263;"
    origin "text"
  ]
  node [
    id 3
    label "zasi&#281;g"
    origin "text"
  ]
  node [
    id 4
    label "niewygodny"
    origin "text"
  ]
  node [
    id 5
    label "u&#380;ytkownik"
    origin "text"
  ]
  node [
    id 6
    label "cz&#322;owiek"
  ]
  node [
    id 7
    label "czyn"
  ]
  node [
    id 8
    label "przedstawiciel"
  ]
  node [
    id 9
    label "ilustracja"
  ]
  node [
    id 10
    label "fakt"
  ]
  node [
    id 11
    label "zarz&#261;d"
  ]
  node [
    id 12
    label "moderation"
  ]
  node [
    id 13
    label "okrawa&#263;"
  ]
  node [
    id 14
    label "hack"
  ]
  node [
    id 15
    label "rzuca&#263;"
  ]
  node [
    id 16
    label "szkodzi&#263;"
  ]
  node [
    id 17
    label "oblewa&#263;"
  ]
  node [
    id 18
    label "przycina&#263;"
  ]
  node [
    id 19
    label "skraca&#263;"
  ]
  node [
    id 20
    label "ucina&#263;"
  ]
  node [
    id 21
    label "reduce"
  ]
  node [
    id 22
    label "opitala&#263;"
  ]
  node [
    id 23
    label "w&#322;osy"
  ]
  node [
    id 24
    label "kszta&#322;towa&#263;"
  ]
  node [
    id 25
    label "ow&#322;osienie"
  ]
  node [
    id 26
    label "kaleczy&#263;"
  ]
  node [
    id 27
    label "write_out"
  ]
  node [
    id 28
    label "trim"
  ]
  node [
    id 29
    label "obni&#380;a&#263;"
  ]
  node [
    id 30
    label "odcina&#263;"
  ]
  node [
    id 31
    label "gasi&#263;"
  ]
  node [
    id 32
    label "zabiera&#263;"
  ]
  node [
    id 33
    label "os&#261;dza&#263;"
  ]
  node [
    id 34
    label "chop"
  ]
  node [
    id 35
    label "powodowa&#263;"
  ]
  node [
    id 36
    label "przygl&#261;da&#263;_si&#281;"
  ]
  node [
    id 37
    label "obszar"
  ]
  node [
    id 38
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 39
    label "niewygodnie"
  ]
  node [
    id 40
    label "k&#322;opotliwy"
  ]
  node [
    id 41
    label "j&#281;zykowo"
  ]
  node [
    id 42
    label "podmiot"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
]
