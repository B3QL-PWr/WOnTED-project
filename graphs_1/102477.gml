graph [
  maxDegree 15
  minDegree 1
  meanDegree 2.1066666666666665
  density 0.02846846846846847
  graphCliqueNumber 3
  node [
    id 0
    label "miejski"
    origin "text"
  ]
  node [
    id 1
    label "biblioteka"
    origin "text"
  ]
  node [
    id 2
    label "publiczny"
    origin "text"
  ]
  node [
    id 3
    label "baczy&#324;ski"
    origin "text"
  ]
  node [
    id 4
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 5
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wernisa&#380;"
    origin "text"
  ]
  node [
    id 7
    label "wystawa"
    origin "text"
  ]
  node [
    id 8
    label "malarstwo"
    origin "text"
  ]
  node [
    id 9
    label "olejny"
    origin "text"
  ]
  node [
    id 10
    label "grafik"
    origin "text"
  ]
  node [
    id 11
    label "pawe&#322;"
    origin "text"
  ]
  node [
    id 12
    label "jaszczaka"
    origin "text"
  ]
  node [
    id 13
    label "czytelnia"
    origin "text"
  ]
  node [
    id 14
    label "przy"
    origin "text"
  ]
  node [
    id 15
    label "ula"
    origin "text"
  ]
  node [
    id 16
    label "listopadowy"
    origin "text"
  ]
  node [
    id 17
    label "listopad"
    origin "text"
  ]
  node [
    id 18
    label "rocznik"
    origin "text"
  ]
  node [
    id 19
    label "godz"
    origin "text"
  ]
  node [
    id 20
    label "miejsko"
  ]
  node [
    id 21
    label "miastowy"
  ]
  node [
    id 22
    label "typowy"
  ]
  node [
    id 23
    label "pok&#243;j"
  ]
  node [
    id 24
    label "rewers"
  ]
  node [
    id 25
    label "informatorium"
  ]
  node [
    id 26
    label "kolekcja"
  ]
  node [
    id 27
    label "czytelnik"
  ]
  node [
    id 28
    label "budynek"
  ]
  node [
    id 29
    label "zbi&#243;r"
  ]
  node [
    id 30
    label "programowanie"
  ]
  node [
    id 31
    label "ksi&#281;gozbi&#243;r_podr&#281;czny"
  ]
  node [
    id 32
    label "library"
  ]
  node [
    id 33
    label "instytucja"
  ]
  node [
    id 34
    label "jawny"
  ]
  node [
    id 35
    label "upublicznienie"
  ]
  node [
    id 36
    label "upublicznianie"
  ]
  node [
    id 37
    label "publicznie"
  ]
  node [
    id 38
    label "invite"
  ]
  node [
    id 39
    label "ask"
  ]
  node [
    id 40
    label "oferowa&#263;"
  ]
  node [
    id 41
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 42
    label "impreza"
  ]
  node [
    id 43
    label "galeria"
  ]
  node [
    id 44
    label "miejsce"
  ]
  node [
    id 45
    label "sklep"
  ]
  node [
    id 46
    label "muzeum"
  ]
  node [
    id 47
    label "Arsena&#322;"
  ]
  node [
    id 48
    label "szyba"
  ]
  node [
    id 49
    label "kurator"
  ]
  node [
    id 50
    label "Agropromocja"
  ]
  node [
    id 51
    label "kustosz"
  ]
  node [
    id 52
    label "ekspozycja"
  ]
  node [
    id 53
    label "okno"
  ]
  node [
    id 54
    label "syntetyzm"
  ]
  node [
    id 55
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 56
    label "linearyzm"
  ]
  node [
    id 57
    label "kompozycja"
  ]
  node [
    id 58
    label "plastyka"
  ]
  node [
    id 59
    label "gwasz"
  ]
  node [
    id 60
    label "rezultat"
  ]
  node [
    id 61
    label "skr&#243;t_perspektywiczny"
  ]
  node [
    id 62
    label "tempera"
  ]
  node [
    id 63
    label "olejno"
  ]
  node [
    id 64
    label "olej"
  ]
  node [
    id 65
    label "plastyk"
  ]
  node [
    id 66
    label "informatyk"
  ]
  node [
    id 67
    label "rozk&#322;ad"
  ]
  node [
    id 68
    label "diagram"
  ]
  node [
    id 69
    label "pomieszczenie"
  ]
  node [
    id 70
    label "miesi&#261;c"
  ]
  node [
    id 71
    label "formacja"
  ]
  node [
    id 72
    label "kronika"
  ]
  node [
    id 73
    label "czasopismo"
  ]
  node [
    id 74
    label "yearbook"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 69
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 70
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 71
  ]
  edge [
    source 18
    target 72
  ]
  edge [
    source 18
    target 73
  ]
  edge [
    source 18
    target 74
  ]
]
