graph [
  maxDegree 36
  minDegree 1
  meanDegree 2
  density 0.017543859649122806
  graphCliqueNumber 2
  node [
    id 0
    label "wczoraj"
    origin "text"
  ]
  node [
    id 1
    label "podczas"
    origin "text"
  ]
  node [
    id 2
    label "igraszka"
    origin "text"
  ]
  node [
    id 3
    label "dziewczyna"
    origin "text"
  ]
  node [
    id 4
    label "mia&#322;"
    origin "text"
  ]
  node [
    id 5
    label "palce"
    origin "text"
  ]
  node [
    id 6
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 7
    label "wyczu&#263;by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ko&#347;&#263;"
    origin "text"
  ]
  node [
    id 9
    label "przez"
    origin "text"
  ]
  node [
    id 10
    label "sk&#243;ra"
    origin "text"
  ]
  node [
    id 11
    label "pipka"
    origin "text"
  ]
  node [
    id 12
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 13
    label "ogonowy"
    origin "text"
  ]
  node [
    id 14
    label "albo"
    origin "text"
  ]
  node [
    id 15
    label "&#380;ebyby&#263;"
    origin "text"
  ]
  node [
    id 16
    label "tak"
    origin "text"
  ]
  node [
    id 17
    label "g&#322;&#281;boko"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;gn&#261;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "doba"
  ]
  node [
    id 20
    label "dawno"
  ]
  node [
    id 21
    label "niedawno"
  ]
  node [
    id 22
    label "zabawa"
  ]
  node [
    id 23
    label "narz&#281;dzie"
  ]
  node [
    id 24
    label "cz&#322;owiek"
  ]
  node [
    id 25
    label "dziewka"
  ]
  node [
    id 26
    label "dziewoja"
  ]
  node [
    id 27
    label "siksa"
  ]
  node [
    id 28
    label "partnerka"
  ]
  node [
    id 29
    label "dziewczynina"
  ]
  node [
    id 30
    label "dziunia"
  ]
  node [
    id 31
    label "sympatia"
  ]
  node [
    id 32
    label "dziewcz&#281;"
  ]
  node [
    id 33
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 34
    label "kora"
  ]
  node [
    id 35
    label "m&#322;&#243;dka"
  ]
  node [
    id 36
    label "dziecina"
  ]
  node [
    id 37
    label "sikorka"
  ]
  node [
    id 38
    label "proszek"
  ]
  node [
    id 39
    label "stopa"
  ]
  node [
    id 40
    label "miejsce"
  ]
  node [
    id 41
    label "czas"
  ]
  node [
    id 42
    label "abstrakcja"
  ]
  node [
    id 43
    label "punkt"
  ]
  node [
    id 44
    label "substancja"
  ]
  node [
    id 45
    label "spos&#243;b"
  ]
  node [
    id 46
    label "chemikalia"
  ]
  node [
    id 47
    label "kr&#281;tarz"
  ]
  node [
    id 48
    label "bone"
  ]
  node [
    id 49
    label "chip"
  ]
  node [
    id 50
    label "wyluzowanie"
  ]
  node [
    id 51
    label "szkielet"
  ]
  node [
    id 52
    label "chrz&#261;stkozrost"
  ]
  node [
    id 53
    label "uk&#322;ad_scalony"
  ]
  node [
    id 54
    label "zatoka"
  ]
  node [
    id 55
    label "okostna"
  ]
  node [
    id 56
    label "pochewka"
  ]
  node [
    id 57
    label "luzowa&#263;"
  ]
  node [
    id 58
    label "organ"
  ]
  node [
    id 59
    label "rekwizyt_do_gry"
  ]
  node [
    id 60
    label "&#322;uska"
  ]
  node [
    id 61
    label "luzowanie"
  ]
  node [
    id 62
    label "oczko"
  ]
  node [
    id 63
    label "wyluzowa&#263;"
  ]
  node [
    id 64
    label "szpik"
  ]
  node [
    id 65
    label "gruczo&#322;_potowy"
  ]
  node [
    id 66
    label "mizdra"
  ]
  node [
    id 67
    label "sk&#243;ra_w&#322;a&#347;ciwa"
  ]
  node [
    id 68
    label "gestapowiec"
  ]
  node [
    id 69
    label "&#322;uskowato&#347;&#263;"
  ]
  node [
    id 70
    label "tkanka_podsk&#243;rna"
  ]
  node [
    id 71
    label "szczupak"
  ]
  node [
    id 72
    label "nask&#243;rek"
  ]
  node [
    id 73
    label "wi&#243;rkownik"
  ]
  node [
    id 74
    label "gruczo&#322;_&#322;ojowy"
  ]
  node [
    id 75
    label "coating"
  ]
  node [
    id 76
    label "funkcja"
  ]
  node [
    id 77
    label "kurtka"
  ]
  node [
    id 78
    label "&#380;ycie"
  ]
  node [
    id 79
    label "rockers"
  ]
  node [
    id 80
    label "harleyowiec"
  ]
  node [
    id 81
    label "zdrowie"
  ]
  node [
    id 82
    label "dupa"
  ]
  node [
    id 83
    label "surowiec"
  ]
  node [
    id 84
    label "czerniak_z&#322;o&#347;liwy"
  ]
  node [
    id 85
    label "metal"
  ]
  node [
    id 86
    label "pow&#322;oka"
  ]
  node [
    id 87
    label "wyprawa"
  ]
  node [
    id 88
    label "gruczo&#322;_kuprowy"
  ]
  node [
    id 89
    label "p&#322;aszcz"
  ]
  node [
    id 90
    label "krupon"
  ]
  node [
    id 91
    label "wyprze&#263;"
  ]
  node [
    id 92
    label "cia&#322;o"
  ]
  node [
    id 93
    label "okrywa"
  ]
  node [
    id 94
    label "shell"
  ]
  node [
    id 95
    label "&#322;upa"
  ]
  node [
    id 96
    label "hardrockowiec"
  ]
  node [
    id 97
    label "lico"
  ]
  node [
    id 98
    label "daleko"
  ]
  node [
    id 99
    label "silnie"
  ]
  node [
    id 100
    label "nisko"
  ]
  node [
    id 101
    label "g&#322;&#281;boki"
  ]
  node [
    id 102
    label "gruntownie"
  ]
  node [
    id 103
    label "intensywnie"
  ]
  node [
    id 104
    label "mocno"
  ]
  node [
    id 105
    label "szczerze"
  ]
  node [
    id 106
    label "allude"
  ]
  node [
    id 107
    label "dotrze&#263;"
  ]
  node [
    id 108
    label "appreciation"
  ]
  node [
    id 109
    label "u&#380;y&#263;"
  ]
  node [
    id 110
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 111
    label "seize"
  ]
  node [
    id 112
    label "fall_upon"
  ]
  node [
    id 113
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 114
    label "skorzysta&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 98
  ]
  edge [
    source 17
    target 99
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 18
    target 107
  ]
  edge [
    source 18
    target 108
  ]
  edge [
    source 18
    target 109
  ]
  edge [
    source 18
    target 110
  ]
  edge [
    source 18
    target 111
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 18
    target 113
  ]
  edge [
    source 18
    target 114
  ]
]
