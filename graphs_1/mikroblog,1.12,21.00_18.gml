graph [
  maxDegree 4
  minDegree 1
  meanDegree 1.6
  density 0.4
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "zakup"
    origin "text"
  ]
  node [
    id 2
    label "sprzedaj&#261;cy"
  ]
  node [
    id 3
    label "dobro"
  ]
  node [
    id 4
    label "transakcja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
]
