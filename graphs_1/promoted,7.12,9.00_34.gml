graph [
  maxDegree 11
  minDegree 1
  meanDegree 3.5609756097560976
  density 0.08902439024390243
  graphCliqueNumber 9
  node [
    id 0
    label "recent"
    origin "text"
  ]
  node [
    id 1
    label "return"
    origin "text"
  ]
  node [
    id 2
    label "leipzig"
    origin "text"
  ]
  node [
    id 3
    label "reminded"
    origin "text"
  ]
  node [
    id 4
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 5
    label "why"
    origin "text"
  ]
  node [
    id 6
    label "green"
    origin "text"
  ]
  node [
    id 7
    label "has"
    origin "text"
  ]
  node [
    id 8
    label "been"
    origin "text"
  ]
  node [
    id 9
    label "lot"
    origin "text"
  ]
  node [
    id 10
    label "difficult"
    origin "text"
  ]
  node [
    id 11
    label "than"
    origin "text"
  ]
  node [
    id 12
    label "the"
    origin "text"
  ]
  node [
    id 13
    label "rhetoric"
    origin "text"
  ]
  node [
    id 14
    label "would"
    origin "text"
  ]
  node [
    id 15
    label "sometimes"
    origin "text"
  ]
  node [
    id 16
    label "suggest"
    origin "text"
  ]
  node [
    id 17
    label "czyj&#347;"
  ]
  node [
    id 18
    label "m&#261;&#380;"
  ]
  node [
    id 19
    label "obszar"
  ]
  node [
    id 20
    label "pole_golfowe"
  ]
  node [
    id 21
    label "hassium"
  ]
  node [
    id 22
    label "transuranowiec"
  ]
  node [
    id 23
    label "&#380;elazowiec"
  ]
  node [
    id 24
    label "chronometra&#380;ysta"
  ]
  node [
    id 25
    label "start"
  ]
  node [
    id 26
    label "ruch"
  ]
  node [
    id 27
    label "ci&#261;g"
  ]
  node [
    id 28
    label "l&#261;dowanie"
  ]
  node [
    id 29
    label "odlot"
  ]
  node [
    id 30
    label "podr&#243;&#380;"
  ]
  node [
    id 31
    label "flight"
  ]
  node [
    id 32
    label "Just"
  ]
  node [
    id 33
    label "after"
  ]
  node [
    id 34
    label "reunification"
  ]
  node [
    id 35
    label "of"
  ]
  node [
    id 36
    label "german"
  ]
  node [
    id 37
    label "in"
  ]
  node [
    id 38
    label "1990"
  ]
  node [
    id 39
    label "Anda"
  ]
  node [
    id 40
    label "Leipzig"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 9
    target 26
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 28
  ]
  edge [
    source 9
    target 29
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 15
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 32
  ]
  edge [
    source 12
    target 33
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 12
    target 35
  ]
  edge [
    source 12
    target 36
  ]
  edge [
    source 12
    target 37
  ]
  edge [
    source 12
    target 38
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 40
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 32
    target 38
  ]
  edge [
    source 32
    target 39
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 34
    target 39
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 39
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 39
    target 40
  ]
]
