graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9090909090909092
  density 0.09090909090909091
  graphCliqueNumber 2
  node [
    id 0
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "lasek"
    origin "text"
  ]
  node [
    id 2
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 3
    label "chodzi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "liceum"
    origin "text"
  ]
  node [
    id 5
    label "ozdabia&#263;"
  ]
  node [
    id 6
    label "dysgrafia"
  ]
  node [
    id 7
    label "prasa"
  ]
  node [
    id 8
    label "spell"
  ]
  node [
    id 9
    label "skryba"
  ]
  node [
    id 10
    label "donosi&#263;"
  ]
  node [
    id 11
    label "code"
  ]
  node [
    id 12
    label "tekst"
  ]
  node [
    id 13
    label "dysortografia"
  ]
  node [
    id 14
    label "read"
  ]
  node [
    id 15
    label "tworzy&#263;"
  ]
  node [
    id 16
    label "formu&#322;owa&#263;"
  ]
  node [
    id 17
    label "styl"
  ]
  node [
    id 18
    label "stawia&#263;"
  ]
  node [
    id 19
    label "szko&#322;a_ponadgimnazjalna"
  ]
  node [
    id 20
    label "szko&#322;a_&#347;rednia"
  ]
  node [
    id 21
    label "szko&#322;a"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
]
