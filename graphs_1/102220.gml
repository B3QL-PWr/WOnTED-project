graph [
  maxDegree 43
  minDegree 1
  meanDegree 2.1742738589211617
  density 0.009059474412171508
  graphCliqueNumber 3
  node [
    id 0
    label "klasa"
    origin "text"
  ]
  node [
    id 1
    label "versus"
    origin "text"
  ]
  node [
    id 2
    label "sze&#347;cioletni"
    origin "text"
  ]
  node [
    id 3
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 4
    label "podstawowy"
    origin "text"
  ]
  node [
    id 5
    label "przypadek"
    origin "text"
  ]
  node [
    id 6
    label "realizacja"
    origin "text"
  ]
  node [
    id 7
    label "ramowy"
    origin "text"
  ]
  node [
    id 8
    label "plan"
    origin "text"
  ]
  node [
    id 9
    label "nauczanie"
    origin "text"
  ]
  node [
    id 10
    label "okre&#347;lony"
    origin "text"
  ]
  node [
    id 11
    label "za&#322;&#261;cznik"
    origin "text"
  ]
  node [
    id 12
    label "rozporz&#261;dzenie"
    origin "text"
  ]
  node [
    id 13
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 14
    label "mowa"
    origin "text"
  ]
  node [
    id 15
    label "pkt"
    origin "text"
  ]
  node [
    id 16
    label "viii"
    origin "text"
  ]
  node [
    id 17
    label "dotychczasowy"
    origin "text"
  ]
  node [
    id 18
    label "o&#347;mioletni"
    origin "text"
  ]
  node [
    id 19
    label "jako"
    origin "text"
  ]
  node [
    id 20
    label "zaj&#281;cia"
    origin "text"
  ]
  node [
    id 21
    label "dodatkowy"
    origin "text"
  ]
  node [
    id 22
    label "godzina"
    origin "text"
  ]
  node [
    id 23
    label "tym"
    origin "text"
  ]
  node [
    id 24
    label "podzia&#322;"
    origin "text"
  ]
  node [
    id 25
    label "grupa"
    origin "text"
  ]
  node [
    id 26
    label "dziewcz&#281;"
    origin "text"
  ]
  node [
    id 27
    label "ch&#322;opiec"
    origin "text"
  ]
  node [
    id 28
    label "typ"
  ]
  node [
    id 29
    label "warstwa"
  ]
  node [
    id 30
    label "znak_jako&#347;ci"
  ]
  node [
    id 31
    label "przedmiot"
  ]
  node [
    id 32
    label "przepisa&#263;"
  ]
  node [
    id 33
    label "pomoc"
  ]
  node [
    id 34
    label "arrangement"
  ]
  node [
    id 35
    label "wagon"
  ]
  node [
    id 36
    label "form"
  ]
  node [
    id 37
    label "zaleta"
  ]
  node [
    id 38
    label "poziom"
  ]
  node [
    id 39
    label "dziennik_lekcyjny"
  ]
  node [
    id 40
    label "&#347;rodowisko"
  ]
  node [
    id 41
    label "atak"
  ]
  node [
    id 42
    label "przepisanie"
  ]
  node [
    id 43
    label "class"
  ]
  node [
    id 44
    label "organizacja"
  ]
  node [
    id 45
    label "obrona"
  ]
  node [
    id 46
    label "type"
  ]
  node [
    id 47
    label "promocja"
  ]
  node [
    id 48
    label "&#322;awka"
  ]
  node [
    id 49
    label "kurs"
  ]
  node [
    id 50
    label "botanika"
  ]
  node [
    id 51
    label "sala"
  ]
  node [
    id 52
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 53
    label "gromada"
  ]
  node [
    id 54
    label "obiekt"
  ]
  node [
    id 55
    label "Ekwici"
  ]
  node [
    id 56
    label "fakcja"
  ]
  node [
    id 57
    label "tablica"
  ]
  node [
    id 58
    label "programowanie_obiektowe"
  ]
  node [
    id 59
    label "wykrzyknik"
  ]
  node [
    id 60
    label "jednostka_systematyczna"
  ]
  node [
    id 61
    label "mecz_mistrzowski"
  ]
  node [
    id 62
    label "zbi&#243;r"
  ]
  node [
    id 63
    label "jako&#347;&#263;"
  ]
  node [
    id 64
    label "rezerwa"
  ]
  node [
    id 65
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 66
    label "kilkuletni"
  ]
  node [
    id 67
    label "Mickiewicz"
  ]
  node [
    id 68
    label "czas"
  ]
  node [
    id 69
    label "szkolenie"
  ]
  node [
    id 70
    label "lesson"
  ]
  node [
    id 71
    label "praktyka"
  ]
  node [
    id 72
    label "metoda"
  ]
  node [
    id 73
    label "niepokalanki"
  ]
  node [
    id 74
    label "kara"
  ]
  node [
    id 75
    label "zda&#263;"
  ]
  node [
    id 76
    label "kwalifikacje"
  ]
  node [
    id 77
    label "system"
  ]
  node [
    id 78
    label "sztuba"
  ]
  node [
    id 79
    label "wiedza"
  ]
  node [
    id 80
    label "stopek"
  ]
  node [
    id 81
    label "school"
  ]
  node [
    id 82
    label "absolwent"
  ]
  node [
    id 83
    label "urszulanki"
  ]
  node [
    id 84
    label "gabinet"
  ]
  node [
    id 85
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 86
    label "ideologia"
  ]
  node [
    id 87
    label "lekcja"
  ]
  node [
    id 88
    label "muzyka"
  ]
  node [
    id 89
    label "podr&#281;cznik"
  ]
  node [
    id 90
    label "zdanie"
  ]
  node [
    id 91
    label "siedziba"
  ]
  node [
    id 92
    label "sekretariat"
  ]
  node [
    id 93
    label "nauka"
  ]
  node [
    id 94
    label "do&#347;wiadczenie"
  ]
  node [
    id 95
    label "teren_szko&#322;y"
  ]
  node [
    id 96
    label "instytucja"
  ]
  node [
    id 97
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 98
    label "skolaryzacja"
  ]
  node [
    id 99
    label "&#322;awa_szkolna"
  ]
  node [
    id 100
    label "pocz&#261;tkowy"
  ]
  node [
    id 101
    label "podstawowo"
  ]
  node [
    id 102
    label "najwa&#380;niejszy"
  ]
  node [
    id 103
    label "niezaawansowany"
  ]
  node [
    id 104
    label "pacjent"
  ]
  node [
    id 105
    label "kategoria_gramatyczna"
  ]
  node [
    id 106
    label "schorzenie"
  ]
  node [
    id 107
    label "przeznaczenie"
  ]
  node [
    id 108
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 109
    label "wydarzenie"
  ]
  node [
    id 110
    label "happening"
  ]
  node [
    id 111
    label "przyk&#322;ad"
  ]
  node [
    id 112
    label "monta&#380;"
  ]
  node [
    id 113
    label "fabrication"
  ]
  node [
    id 114
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 115
    label "kreacja"
  ]
  node [
    id 116
    label "performance"
  ]
  node [
    id 117
    label "dzie&#322;o"
  ]
  node [
    id 118
    label "proces"
  ]
  node [
    id 119
    label "postprodukcja"
  ]
  node [
    id 120
    label "scheduling"
  ]
  node [
    id 121
    label "operacja"
  ]
  node [
    id 122
    label "zarysowy"
  ]
  node [
    id 123
    label "ramowo"
  ]
  node [
    id 124
    label "uproszczony"
  ]
  node [
    id 125
    label "device"
  ]
  node [
    id 126
    label "model"
  ]
  node [
    id 127
    label "wytw&#243;r"
  ]
  node [
    id 128
    label "obraz"
  ]
  node [
    id 129
    label "przestrze&#324;"
  ]
  node [
    id 130
    label "dekoracja"
  ]
  node [
    id 131
    label "intencja"
  ]
  node [
    id 132
    label "agreement"
  ]
  node [
    id 133
    label "pomys&#322;"
  ]
  node [
    id 134
    label "punkt"
  ]
  node [
    id 135
    label "miejsce_pracy"
  ]
  node [
    id 136
    label "perspektywa"
  ]
  node [
    id 137
    label "rysunek"
  ]
  node [
    id 138
    label "reprezentacja"
  ]
  node [
    id 139
    label "zapoznawanie"
  ]
  node [
    id 140
    label "teaching"
  ]
  node [
    id 141
    label "formation"
  ]
  node [
    id 142
    label "training"
  ]
  node [
    id 143
    label "pracowanie"
  ]
  node [
    id 144
    label "pomaganie"
  ]
  node [
    id 145
    label "o&#347;wiecanie"
  ]
  node [
    id 146
    label "kliker"
  ]
  node [
    id 147
    label "heureza"
  ]
  node [
    id 148
    label "pouczenie"
  ]
  node [
    id 149
    label "przem&#243;wienie"
  ]
  node [
    id 150
    label "wiadomy"
  ]
  node [
    id 151
    label "dodatek"
  ]
  node [
    id 152
    label "rule"
  ]
  node [
    id 153
    label "polecenie"
  ]
  node [
    id 154
    label "akt"
  ]
  node [
    id 155
    label "zarz&#261;dzenie"
  ]
  node [
    id 156
    label "commission"
  ]
  node [
    id 157
    label "ordonans"
  ]
  node [
    id 158
    label "wypowied&#378;"
  ]
  node [
    id 159
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 160
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 161
    label "po_koroniarsku"
  ]
  node [
    id 162
    label "m&#243;wienie"
  ]
  node [
    id 163
    label "rozumie&#263;"
  ]
  node [
    id 164
    label "komunikacja"
  ]
  node [
    id 165
    label "rozumienie"
  ]
  node [
    id 166
    label "m&#243;wi&#263;"
  ]
  node [
    id 167
    label "gramatyka"
  ]
  node [
    id 168
    label "address"
  ]
  node [
    id 169
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 170
    label "przet&#322;umaczenie"
  ]
  node [
    id 171
    label "czynno&#347;&#263;"
  ]
  node [
    id 172
    label "tongue"
  ]
  node [
    id 173
    label "t&#322;umaczenie"
  ]
  node [
    id 174
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 175
    label "pismo"
  ]
  node [
    id 176
    label "zdolno&#347;&#263;"
  ]
  node [
    id 177
    label "fonetyka"
  ]
  node [
    id 178
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 179
    label "wokalizm"
  ]
  node [
    id 180
    label "s&#322;ownictwo"
  ]
  node [
    id 181
    label "konsonantyzm"
  ]
  node [
    id 182
    label "kod"
  ]
  node [
    id 183
    label "dotychczasowo"
  ]
  node [
    id 184
    label "pensum"
  ]
  node [
    id 185
    label "enroll"
  ]
  node [
    id 186
    label "poboczny"
  ]
  node [
    id 187
    label "dodatkowo"
  ]
  node [
    id 188
    label "uboczny"
  ]
  node [
    id 189
    label "minuta"
  ]
  node [
    id 190
    label "doba"
  ]
  node [
    id 191
    label "p&#243;&#322;godzina"
  ]
  node [
    id 192
    label "kwadrans"
  ]
  node [
    id 193
    label "time"
  ]
  node [
    id 194
    label "jednostka_czasu"
  ]
  node [
    id 195
    label "competence"
  ]
  node [
    id 196
    label "eksdywizja"
  ]
  node [
    id 197
    label "stopie&#324;"
  ]
  node [
    id 198
    label "blastogeneza"
  ]
  node [
    id 199
    label "fission"
  ]
  node [
    id 200
    label "distribution"
  ]
  node [
    id 201
    label "odm&#322;adza&#263;"
  ]
  node [
    id 202
    label "asymilowa&#263;"
  ]
  node [
    id 203
    label "cz&#261;steczka"
  ]
  node [
    id 204
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 205
    label "egzemplarz"
  ]
  node [
    id 206
    label "formacja_geologiczna"
  ]
  node [
    id 207
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 208
    label "harcerze_starsi"
  ]
  node [
    id 209
    label "liga"
  ]
  node [
    id 210
    label "Terranie"
  ]
  node [
    id 211
    label "&#346;wietliki"
  ]
  node [
    id 212
    label "pakiet_klimatyczny"
  ]
  node [
    id 213
    label "oddzia&#322;"
  ]
  node [
    id 214
    label "stage_set"
  ]
  node [
    id 215
    label "Entuzjastki"
  ]
  node [
    id 216
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 217
    label "odm&#322;odzenie"
  ]
  node [
    id 218
    label "category"
  ]
  node [
    id 219
    label "asymilowanie"
  ]
  node [
    id 220
    label "specgrupa"
  ]
  node [
    id 221
    label "odm&#322;adzanie"
  ]
  node [
    id 222
    label "Eurogrupa"
  ]
  node [
    id 223
    label "kompozycja"
  ]
  node [
    id 224
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 225
    label "dziewczyna"
  ]
  node [
    id 226
    label "cz&#322;owiek"
  ]
  node [
    id 227
    label "pomocnik"
  ]
  node [
    id 228
    label "g&#243;wniarz"
  ]
  node [
    id 229
    label "&#347;l&#261;ski"
  ]
  node [
    id 230
    label "m&#322;odzieniec"
  ]
  node [
    id 231
    label "kajtek"
  ]
  node [
    id 232
    label "kawaler"
  ]
  node [
    id 233
    label "usynawianie"
  ]
  node [
    id 234
    label "dziecko"
  ]
  node [
    id 235
    label "okrzos"
  ]
  node [
    id 236
    label "usynowienie"
  ]
  node [
    id 237
    label "sympatia"
  ]
  node [
    id 238
    label "pederasta"
  ]
  node [
    id 239
    label "synek"
  ]
  node [
    id 240
    label "boyfriend"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 80
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 3
    target 93
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 5
    target 107
  ]
  edge [
    source 5
    target 108
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 147
  ]
  edge [
    source 9
    target 148
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 149
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 161
  ]
  edge [
    source 14
    target 162
  ]
  edge [
    source 14
    target 163
  ]
  edge [
    source 14
    target 164
  ]
  edge [
    source 14
    target 165
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 14
    target 182
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 18
    target 66
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 189
  ]
  edge [
    source 22
    target 190
  ]
  edge [
    source 22
    target 68
  ]
  edge [
    source 22
    target 191
  ]
  edge [
    source 22
    target 192
  ]
  edge [
    source 22
    target 193
  ]
  edge [
    source 22
    target 194
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 127
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 196
  ]
  edge [
    source 24
    target 197
  ]
  edge [
    source 24
    target 198
  ]
  edge [
    source 24
    target 109
  ]
  edge [
    source 24
    target 199
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 25
    target 202
  ]
  edge [
    source 25
    target 203
  ]
  edge [
    source 25
    target 204
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 25
    target 206
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 25
    target 216
  ]
  edge [
    source 25
    target 217
  ]
  edge [
    source 25
    target 46
  ]
  edge [
    source 25
    target 218
  ]
  edge [
    source 25
    target 219
  ]
  edge [
    source 25
    target 220
  ]
  edge [
    source 25
    target 221
  ]
  edge [
    source 25
    target 53
  ]
  edge [
    source 25
    target 222
  ]
  edge [
    source 25
    target 60
  ]
  edge [
    source 25
    target 223
  ]
  edge [
    source 25
    target 224
  ]
  edge [
    source 25
    target 62
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 225
  ]
  edge [
    source 27
    target 226
  ]
  edge [
    source 27
    target 227
  ]
  edge [
    source 27
    target 228
  ]
  edge [
    source 27
    target 229
  ]
  edge [
    source 27
    target 230
  ]
  edge [
    source 27
    target 231
  ]
  edge [
    source 27
    target 232
  ]
  edge [
    source 27
    target 233
  ]
  edge [
    source 27
    target 234
  ]
  edge [
    source 27
    target 235
  ]
  edge [
    source 27
    target 236
  ]
  edge [
    source 27
    target 237
  ]
  edge [
    source 27
    target 238
  ]
  edge [
    source 27
    target 239
  ]
  edge [
    source 27
    target 240
  ]
]
