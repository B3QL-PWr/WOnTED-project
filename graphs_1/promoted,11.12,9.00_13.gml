graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9867549668874172
  density 0.013245033112582781
  graphCliqueNumber 2
  node [
    id 0
    label "filmik"
    origin "text"
  ]
  node [
    id 1
    label "pokazowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "berlina"
    origin "text"
  ]
  node [
    id 3
    label "rok"
    origin "text"
  ]
  node [
    id 4
    label "niemiecki"
    origin "text"
  ]
  node [
    id 5
    label "&#378;r&#243;d&#322;o"
    origin "text"
  ]
  node [
    id 6
    label "historyczny"
    origin "text"
  ]
  node [
    id 7
    label "zostawa&#263;"
    origin "text"
  ]
  node [
    id 8
    label "zablokowa&#263;"
    origin "text"
  ]
  node [
    id 9
    label "tre&#347;&#263;"
    origin "text"
  ]
  node [
    id 10
    label "obra&#378;liwy"
    origin "text"
  ]
  node [
    id 11
    label "nienawi&#347;&#263;"
    origin "text"
  ]
  node [
    id 12
    label "pomimo"
    origin "text"
  ]
  node [
    id 13
    label "opis"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "kana&#322;"
    origin "text"
  ]
  node [
    id 16
    label "zastrzega&#263;"
    origin "text"
  ]
  node [
    id 17
    label "promowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "nazizm"
    origin "text"
  ]
  node [
    id 19
    label "gif"
  ]
  node [
    id 20
    label "wideo"
  ]
  node [
    id 21
    label "stulecie"
  ]
  node [
    id 22
    label "kalendarz"
  ]
  node [
    id 23
    label "czas"
  ]
  node [
    id 24
    label "pora_roku"
  ]
  node [
    id 25
    label "cykl_astronomiczny"
  ]
  node [
    id 26
    label "p&#243;&#322;rocze"
  ]
  node [
    id 27
    label "grupa"
  ]
  node [
    id 28
    label "kwarta&#322;"
  ]
  node [
    id 29
    label "kurs"
  ]
  node [
    id 30
    label "jubileusz"
  ]
  node [
    id 31
    label "miesi&#261;c"
  ]
  node [
    id 32
    label "lata"
  ]
  node [
    id 33
    label "martwy_sezon"
  ]
  node [
    id 34
    label "szwabski"
  ]
  node [
    id 35
    label "po_niemiecku"
  ]
  node [
    id 36
    label "niemiec"
  ]
  node [
    id 37
    label "cenar"
  ]
  node [
    id 38
    label "j&#281;zyk"
  ]
  node [
    id 39
    label "europejski"
  ]
  node [
    id 40
    label "German"
  ]
  node [
    id 41
    label "pionier"
  ]
  node [
    id 42
    label "niemiecko"
  ]
  node [
    id 43
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 44
    label "zachodnioeuropejski"
  ]
  node [
    id 45
    label "strudel"
  ]
  node [
    id 46
    label "junkers"
  ]
  node [
    id 47
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 48
    label "bra&#263;_si&#281;"
  ]
  node [
    id 49
    label "&#347;wiadectwo"
  ]
  node [
    id 50
    label "przyczyna"
  ]
  node [
    id 51
    label "matuszka"
  ]
  node [
    id 52
    label "geneza"
  ]
  node [
    id 53
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 54
    label "kamena"
  ]
  node [
    id 55
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 56
    label "czynnik"
  ]
  node [
    id 57
    label "pocz&#261;tek"
  ]
  node [
    id 58
    label "poci&#261;ganie"
  ]
  node [
    id 59
    label "rezultat"
  ]
  node [
    id 60
    label "ciek_wodny"
  ]
  node [
    id 61
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 62
    label "subject"
  ]
  node [
    id 63
    label "dawny"
  ]
  node [
    id 64
    label "zgodny"
  ]
  node [
    id 65
    label "historycznie"
  ]
  node [
    id 66
    label "wiekopomny"
  ]
  node [
    id 67
    label "prawdziwy"
  ]
  node [
    id 68
    label "dziejowo"
  ]
  node [
    id 69
    label "pozostawa&#263;"
  ]
  node [
    id 70
    label "by&#263;"
  ]
  node [
    id 71
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 72
    label "ostawa&#263;_si&#281;"
  ]
  node [
    id 73
    label "stop"
  ]
  node [
    id 74
    label "przebywa&#263;"
  ]
  node [
    id 75
    label "oddala&#263;_si&#281;"
  ]
  node [
    id 76
    label "blend"
  ]
  node [
    id 77
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 78
    label "change"
  ]
  node [
    id 79
    label "throng"
  ]
  node [
    id 80
    label "zaj&#261;&#263;"
  ]
  node [
    id 81
    label "przerwa&#263;"
  ]
  node [
    id 82
    label "uniemo&#380;liwi&#263;"
  ]
  node [
    id 83
    label "zatrzyma&#263;"
  ]
  node [
    id 84
    label "wstrzyma&#263;"
  ]
  node [
    id 85
    label "interlock"
  ]
  node [
    id 86
    label "unieruchomi&#263;"
  ]
  node [
    id 87
    label "suspend"
  ]
  node [
    id 88
    label "przeszkodzi&#263;"
  ]
  node [
    id 89
    label "lock"
  ]
  node [
    id 90
    label "z&#322;o&#380;y&#263;"
  ]
  node [
    id 91
    label "zawarto&#347;&#263;"
  ]
  node [
    id 92
    label "temat"
  ]
  node [
    id 93
    label "istota"
  ]
  node [
    id 94
    label "trzyma&#263;_si&#281;_kupy"
  ]
  node [
    id 95
    label "informacja"
  ]
  node [
    id 96
    label "z&#322;y"
  ]
  node [
    id 97
    label "zel&#380;ywy"
  ]
  node [
    id 98
    label "obra&#378;liwie"
  ]
  node [
    id 99
    label "emocja"
  ]
  node [
    id 100
    label "exposition"
  ]
  node [
    id 101
    label "czynno&#347;&#263;"
  ]
  node [
    id 102
    label "wypowied&#378;"
  ]
  node [
    id 103
    label "obja&#347;nienie"
  ]
  node [
    id 104
    label "chody"
  ]
  node [
    id 105
    label "szaniec"
  ]
  node [
    id 106
    label "gara&#380;"
  ]
  node [
    id 107
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 108
    label "tarapaty"
  ]
  node [
    id 109
    label "wg&#322;&#281;bienie"
  ]
  node [
    id 110
    label "budowa"
  ]
  node [
    id 111
    label "pit"
  ]
  node [
    id 112
    label "ciek"
  ]
  node [
    id 113
    label "spos&#243;b"
  ]
  node [
    id 114
    label "odwa&#322;"
  ]
  node [
    id 115
    label "bystrza"
  ]
  node [
    id 116
    label "teatr"
  ]
  node [
    id 117
    label "piaskownik"
  ]
  node [
    id 118
    label "zrzutowy"
  ]
  node [
    id 119
    label "przew&#243;d"
  ]
  node [
    id 120
    label "syfon"
  ]
  node [
    id 121
    label "klarownia"
  ]
  node [
    id 122
    label "miejsce"
  ]
  node [
    id 123
    label "topologia_magistrali"
  ]
  node [
    id 124
    label "struktura_anatomiczna"
  ]
  node [
    id 125
    label "odk&#322;ad"
  ]
  node [
    id 126
    label "kanalizacja"
  ]
  node [
    id 127
    label "urz&#261;dzenie"
  ]
  node [
    id 128
    label "warsztat"
  ]
  node [
    id 129
    label "grodzisko"
  ]
  node [
    id 130
    label "wymawia&#263;"
  ]
  node [
    id 131
    label "zapewnia&#263;"
  ]
  node [
    id 132
    label "condition"
  ]
  node [
    id 133
    label "uprzedza&#263;"
  ]
  node [
    id 134
    label "wypromowywa&#263;"
  ]
  node [
    id 135
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 136
    label "rozpowszechnia&#263;"
  ]
  node [
    id 137
    label "nada&#263;"
  ]
  node [
    id 138
    label "zach&#281;ca&#263;"
  ]
  node [
    id 139
    label "promocja"
  ]
  node [
    id 140
    label "udzieli&#263;"
  ]
  node [
    id 141
    label "udziela&#263;"
  ]
  node [
    id 142
    label "pomaga&#263;"
  ]
  node [
    id 143
    label "advance"
  ]
  node [
    id 144
    label "doprowadza&#263;"
  ]
  node [
    id 145
    label "reklama"
  ]
  node [
    id 146
    label "nadawa&#263;"
  ]
  node [
    id 147
    label "socjalizm"
  ]
  node [
    id 148
    label "Aryjczyk"
  ]
  node [
    id 149
    label "faszyzm"
  ]
  node [
    id 150
    label "Nazism"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 104
  ]
  edge [
    source 15
    target 105
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 115
  ]
  edge [
    source 15
    target 116
  ]
  edge [
    source 15
    target 117
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
]
