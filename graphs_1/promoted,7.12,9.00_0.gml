graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.894736842105263
  density 0.10526315789473684
  graphCliqueNumber 2
  node [
    id 0
    label "zwolni&#263;"
    origin "text"
  ]
  node [
    id 1
    label "uzycie"
    origin "text"
  ]
  node [
    id 2
    label "hashtaga"
    origin "text"
  ]
  node [
    id 3
    label "wontbeerased"
    origin "text"
  ]
  node [
    id 4
    label "sta&#263;_si&#281;"
  ]
  node [
    id 5
    label "advise"
  ]
  node [
    id 6
    label "render"
  ]
  node [
    id 7
    label "spowodowa&#263;"
  ]
  node [
    id 8
    label "wypu&#347;ci&#263;"
  ]
  node [
    id 9
    label "uprzedzi&#263;"
  ]
  node [
    id 10
    label "oddali&#263;"
  ]
  node [
    id 11
    label "wyla&#263;"
  ]
  node [
    id 12
    label "da&#263;_spok&#243;j"
  ]
  node [
    id 13
    label "wypowiedzie&#263;"
  ]
  node [
    id 14
    label "sprawi&#263;"
  ]
  node [
    id 15
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 16
    label "usprawiedliwi&#263;"
  ]
  node [
    id 17
    label "poluzowa&#263;"
  ]
  node [
    id 18
    label "deliver"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
]
