graph [
  maxDegree 15
  minDegree 1
  meanDegree 5.48936170212766
  density 0.11933395004625347
  graphCliqueNumber 11
  node [
    id 0
    label "ludwik"
    origin "text"
  ]
  node [
    id 1
    label "antoni"
    origin "text"
  ]
  node [
    id 2
    label "paszkiewicz"
    origin "text"
  ]
  node [
    id 3
    label "moneta"
  ]
  node [
    id 4
    label "antyk"
  ]
  node [
    id 5
    label "Antoni"
  ]
  node [
    id 6
    label "Paszkiewicz"
  ]
  node [
    id 7
    label "akademia"
  ]
  node [
    id 8
    label "medyczny"
  ]
  node [
    id 9
    label "pa&#324;stwowy"
  ]
  node [
    id 10
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 11
    label "nauka"
  ]
  node [
    id 12
    label "polskie"
  ]
  node [
    id 13
    label "towarzystwo"
  ]
  node [
    id 14
    label "anatomopatologiczny"
  ]
  node [
    id 15
    label "zak&#322;ad"
  ]
  node [
    id 16
    label "patologia"
  ]
  node [
    id 17
    label "do&#347;wiadczalny"
  ]
  node [
    id 18
    label "tygodnik"
  ]
  node [
    id 19
    label "lekarski"
  ]
  node [
    id 20
    label "anatomia"
  ]
  node [
    id 21
    label "patologiczny"
  ]
  node [
    id 22
    label "wyspa"
  ]
  node [
    id 23
    label "sprawa"
  ]
  node [
    id 24
    label "do&#347;wiadczy&#263;"
  ]
  node [
    id 25
    label "wywo&#322;ywa&#263;"
  ]
  node [
    id 26
    label "nowotw&#243;r"
  ]
  node [
    id 27
    label "przez"
  ]
  node [
    id 28
    label "dra&#380;ni&#263;"
  ]
  node [
    id 29
    label "smo&#322;a"
  ]
  node [
    id 30
    label "pogazowy"
  ]
  node [
    id 31
    label "ojciec"
  ]
  node [
    id 32
    label "utrwala&#263;"
  ]
  node [
    id 33
    label "i"
  ]
  node [
    id 34
    label "przechowywa&#263;"
  ]
  node [
    id 35
    label "preparat"
  ]
  node [
    id 36
    label "anatomiczny"
  ]
  node [
    id 37
    label "zeszyt"
  ]
  node [
    id 38
    label "utrzyma&#263;"
  ]
  node [
    id 39
    label "on"
  ]
  node [
    id 40
    label "barwa"
  ]
  node [
    id 41
    label "naturalny"
  ]
  node [
    id 42
    label "technika"
  ]
  node [
    id 43
    label "sekcja"
  ]
  node [
    id 44
    label "zw&#322;oki"
  ]
  node [
    id 45
    label "297"
  ]
  node [
    id 46
    label "rysunki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 12
    target 19
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 24
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 26
  ]
  edge [
    source 22
    target 27
  ]
  edge [
    source 22
    target 28
  ]
  edge [
    source 22
    target 29
  ]
  edge [
    source 22
    target 30
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 25
  ]
  edge [
    source 23
    target 26
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 28
  ]
  edge [
    source 23
    target 29
  ]
  edge [
    source 23
    target 30
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 27
  ]
  edge [
    source 24
    target 28
  ]
  edge [
    source 24
    target 29
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 31
    target 37
  ]
  edge [
    source 31
    target 38
  ]
  edge [
    source 31
    target 39
  ]
  edge [
    source 31
    target 40
  ]
  edge [
    source 31
    target 41
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 32
    target 36
  ]
  edge [
    source 32
    target 37
  ]
  edge [
    source 32
    target 38
  ]
  edge [
    source 32
    target 39
  ]
  edge [
    source 32
    target 40
  ]
  edge [
    source 32
    target 41
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 35
  ]
  edge [
    source 33
    target 36
  ]
  edge [
    source 33
    target 37
  ]
  edge [
    source 33
    target 38
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 33
    target 40
  ]
  edge [
    source 33
    target 41
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 36
  ]
  edge [
    source 34
    target 37
  ]
  edge [
    source 34
    target 38
  ]
  edge [
    source 34
    target 39
  ]
  edge [
    source 34
    target 40
  ]
  edge [
    source 34
    target 41
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 35
    target 38
  ]
  edge [
    source 35
    target 39
  ]
  edge [
    source 35
    target 40
  ]
  edge [
    source 35
    target 41
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 36
    target 41
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 40
  ]
  edge [
    source 37
    target 41
  ]
  edge [
    source 37
    target 42
  ]
  edge [
    source 37
    target 43
  ]
  edge [
    source 37
    target 44
  ]
  edge [
    source 37
    target 45
  ]
  edge [
    source 37
    target 46
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 45
    target 46
  ]
]
