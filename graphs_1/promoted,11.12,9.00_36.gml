graph [
  maxDegree 31
  minDegree 1
  meanDegree 1.9705882352941178
  density 0.029411764705882353
  graphCliqueNumber 2
  node [
    id 0
    label "cz&#281;stochowa"
    origin "text"
  ]
  node [
    id 1
    label "blachownia"
    origin "text"
  ]
  node [
    id 2
    label "automobilklub"
    origin "text"
  ]
  node [
    id 3
    label "cz&#281;stochowski"
    origin "text"
  ]
  node [
    id 4
    label "podpisa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "porozumienie"
    origin "text"
  ]
  node [
    id 6
    label "intencyjny"
    origin "text"
  ]
  node [
    id 7
    label "sprawa"
    origin "text"
  ]
  node [
    id 8
    label "odbudowa"
    origin "text"
  ]
  node [
    id 9
    label "tor"
    origin "text"
  ]
  node [
    id 10
    label "samochodowo"
    origin "text"
  ]
  node [
    id 11
    label "kartingowo"
    origin "text"
  ]
  node [
    id 12
    label "motocyklowy"
    origin "text"
  ]
  node [
    id 13
    label "wyrazowie"
    origin "text"
  ]
  node [
    id 14
    label "warsztat"
  ]
  node [
    id 15
    label "pracownia"
  ]
  node [
    id 16
    label "klub"
  ]
  node [
    id 17
    label "za&#347;wiadczy&#263;"
  ]
  node [
    id 18
    label "postawi&#263;"
  ]
  node [
    id 19
    label "sign"
  ]
  node [
    id 20
    label "opatrzy&#263;"
  ]
  node [
    id 21
    label "zgoda"
  ]
  node [
    id 22
    label "uk&#322;ad_dayto&#324;ski"
  ]
  node [
    id 23
    label "rozstrzygni&#281;cie"
  ]
  node [
    id 24
    label "umowa"
  ]
  node [
    id 25
    label "agent"
  ]
  node [
    id 26
    label "communication"
  ]
  node [
    id 27
    label "z&#322;oty_blok"
  ]
  node [
    id 28
    label "temat"
  ]
  node [
    id 29
    label "kognicja"
  ]
  node [
    id 30
    label "idea"
  ]
  node [
    id 31
    label "szczeg&#243;&#322;"
  ]
  node [
    id 32
    label "rzecz"
  ]
  node [
    id 33
    label "wydarzenie"
  ]
  node [
    id 34
    label "przes&#322;anka"
  ]
  node [
    id 35
    label "rozprawa"
  ]
  node [
    id 36
    label "object"
  ]
  node [
    id 37
    label "proposition"
  ]
  node [
    id 38
    label "naprawa"
  ]
  node [
    id 39
    label "torowisko"
  ]
  node [
    id 40
    label "droga"
  ]
  node [
    id 41
    label "szyna"
  ]
  node [
    id 42
    label "rozstaw_przyl&#261;dkowy"
  ]
  node [
    id 43
    label "balastowanie"
  ]
  node [
    id 44
    label "aktynowiec"
  ]
  node [
    id 45
    label "jednostka_ci&#347;nienia"
  ]
  node [
    id 46
    label "spos&#243;b"
  ]
  node [
    id 47
    label "nawierzchnia_kolejowa"
  ]
  node [
    id 48
    label "rozstaw_bo&#347;niacki"
  ]
  node [
    id 49
    label "przeorientowa&#263;_si&#281;"
  ]
  node [
    id 50
    label "linia_kolejowa"
  ]
  node [
    id 51
    label "przeorientowywa&#263;"
  ]
  node [
    id 52
    label "bearing"
  ]
  node [
    id 53
    label "trasa"
  ]
  node [
    id 54
    label "kszta&#322;t"
  ]
  node [
    id 55
    label "d&#261;&#380;no&#347;&#263;"
  ]
  node [
    id 56
    label "przeorientowanie_si&#281;"
  ]
  node [
    id 57
    label "miejsce"
  ]
  node [
    id 58
    label "kolej"
  ]
  node [
    id 59
    label "podbijarka_torowa"
  ]
  node [
    id 60
    label "przeorientowa&#263;"
  ]
  node [
    id 61
    label "rozstaw_tr&#243;jstopowy"
  ]
  node [
    id 62
    label "przeorientowanie"
  ]
  node [
    id 63
    label "podk&#322;ad"
  ]
  node [
    id 64
    label "przeorientowywanie"
  ]
  node [
    id 65
    label "duchowy_przyw&#243;dca"
  ]
  node [
    id 66
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 67
    label "lane"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 38
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 39
  ]
  edge [
    source 9
    target 40
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 9
    target 42
  ]
  edge [
    source 9
    target 43
  ]
  edge [
    source 9
    target 44
  ]
  edge [
    source 9
    target 45
  ]
  edge [
    source 9
    target 46
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 9
    target 56
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
]
