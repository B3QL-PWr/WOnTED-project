graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.0491803278688523
  density 0.016935374610486383
  graphCliqueNumber 3
  node [
    id 0
    label "umorzy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "post&#281;powanie"
    origin "text"
  ]
  node [
    id 2
    label "dyscyplinarny"
    origin "text"
  ]
  node [
    id 3
    label "wobec"
    origin "text"
  ]
  node [
    id 4
    label "s&#281;dzia"
    origin "text"
  ]
  node [
    id 5
    label "s&#261;d"
    origin "text"
  ]
  node [
    id 6
    label "okr&#281;gowy"
    origin "text"
  ]
  node [
    id 7
    label "katowice"
    origin "text"
  ]
  node [
    id 8
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 9
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 10
    label "potr&#261;ci&#263;"
    origin "text"
  ]
  node [
    id 11
    label "dziecko"
    origin "text"
  ]
  node [
    id 12
    label "samoch&#243;d"
    origin "text"
  ]
  node [
    id 13
    label "remit"
  ]
  node [
    id 14
    label "spowodowa&#263;"
  ]
  node [
    id 15
    label "obni&#380;y&#263;"
  ]
  node [
    id 16
    label "break"
  ]
  node [
    id 17
    label "uniewa&#380;ni&#263;"
  ]
  node [
    id 18
    label "sko&#324;czy&#263;"
  ]
  node [
    id 19
    label "zabi&#263;"
  ]
  node [
    id 20
    label "robienie"
  ]
  node [
    id 21
    label "czynno&#347;&#263;"
  ]
  node [
    id 22
    label "zachowanie"
  ]
  node [
    id 23
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 24
    label "zako&#324;czenie_si&#281;"
  ]
  node [
    id 25
    label "kognicja"
  ]
  node [
    id 26
    label "rozprawa"
  ]
  node [
    id 27
    label "kazanie"
  ]
  node [
    id 28
    label "ko&#324;czenie_si&#281;"
  ]
  node [
    id 29
    label "campaign"
  ]
  node [
    id 30
    label "fashion"
  ]
  node [
    id 31
    label "wydarzenie"
  ]
  node [
    id 32
    label "przes&#322;anka"
  ]
  node [
    id 33
    label "zmierzanie"
  ]
  node [
    id 34
    label "dyscyplinarnie"
  ]
  node [
    id 35
    label "cz&#322;owiek"
  ]
  node [
    id 36
    label "sport"
  ]
  node [
    id 37
    label "prawnik"
  ]
  node [
    id 38
    label "os&#261;dziciel"
  ]
  node [
    id 39
    label "kartka"
  ]
  node [
    id 40
    label "opiniodawca"
  ]
  node [
    id 41
    label "pracownik"
  ]
  node [
    id 42
    label "orzeka&#263;"
  ]
  node [
    id 43
    label "orzekanie"
  ]
  node [
    id 44
    label "procesowicz"
  ]
  node [
    id 45
    label "wypowied&#378;"
  ]
  node [
    id 46
    label "pods&#261;dny"
  ]
  node [
    id 47
    label "podejrzany"
  ]
  node [
    id 48
    label "broni&#263;"
  ]
  node [
    id 49
    label "bronienie"
  ]
  node [
    id 50
    label "system"
  ]
  node [
    id 51
    label "my&#347;l"
  ]
  node [
    id 52
    label "wytw&#243;r"
  ]
  node [
    id 53
    label "urz&#261;d"
  ]
  node [
    id 54
    label "konektyw"
  ]
  node [
    id 55
    label "court"
  ]
  node [
    id 56
    label "obrona"
  ]
  node [
    id 57
    label "s&#261;downictwo"
  ]
  node [
    id 58
    label "nadzorca_s&#261;dowy"
  ]
  node [
    id 59
    label "forum"
  ]
  node [
    id 60
    label "zesp&#243;&#322;"
  ]
  node [
    id 61
    label "skazany"
  ]
  node [
    id 62
    label "&#347;wiadek"
  ]
  node [
    id 63
    label "antylogizm"
  ]
  node [
    id 64
    label "strona"
  ]
  node [
    id 65
    label "oskar&#380;yciel"
  ]
  node [
    id 66
    label "&#322;awa_przysi&#281;g&#322;ych"
  ]
  node [
    id 67
    label "biuro"
  ]
  node [
    id 68
    label "instytucja"
  ]
  node [
    id 69
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 70
    label "czu&#263;"
  ]
  node [
    id 71
    label "need"
  ]
  node [
    id 72
    label "hide"
  ]
  node [
    id 73
    label "support"
  ]
  node [
    id 74
    label "precipitate"
  ]
  node [
    id 75
    label "allude"
  ]
  node [
    id 76
    label "odliczy&#263;"
  ]
  node [
    id 77
    label "uderzy&#263;"
  ]
  node [
    id 78
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 79
    label "smell"
  ]
  node [
    id 80
    label "potomstwo"
  ]
  node [
    id 81
    label "organizm"
  ]
  node [
    id 82
    label "sraluch"
  ]
  node [
    id 83
    label "utulanie"
  ]
  node [
    id 84
    label "pediatra"
  ]
  node [
    id 85
    label "dzieciarnia"
  ]
  node [
    id 86
    label "m&#322;odziak"
  ]
  node [
    id 87
    label "dzieciak"
  ]
  node [
    id 88
    label "utula&#263;"
  ]
  node [
    id 89
    label "potomek"
  ]
  node [
    id 90
    label "entliczek-pentliczek"
  ]
  node [
    id 91
    label "pedofil"
  ]
  node [
    id 92
    label "m&#322;odzik"
  ]
  node [
    id 93
    label "cz&#322;owieczek"
  ]
  node [
    id 94
    label "zwierz&#281;"
  ]
  node [
    id 95
    label "niepe&#322;noletni"
  ]
  node [
    id 96
    label "fledgling"
  ]
  node [
    id 97
    label "utuli&#263;"
  ]
  node [
    id 98
    label "utulenie"
  ]
  node [
    id 99
    label "baga&#380;nik"
  ]
  node [
    id 100
    label "immobilizer"
  ]
  node [
    id 101
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 102
    label "ko&#322;o_samochodowe"
  ]
  node [
    id 103
    label "poduszka_powietrzna"
  ]
  node [
    id 104
    label "dachowanie"
  ]
  node [
    id 105
    label "dwu&#347;lad"
  ]
  node [
    id 106
    label "deska_rozdzielcza"
  ]
  node [
    id 107
    label "poci&#261;g_drogowy"
  ]
  node [
    id 108
    label "kierownica"
  ]
  node [
    id 109
    label "pojazd_drogowy"
  ]
  node [
    id 110
    label "uk&#322;ad_kierowniczy"
  ]
  node [
    id 111
    label "pompa_wodna"
  ]
  node [
    id 112
    label "silnik"
  ]
  node [
    id 113
    label "wycieraczka"
  ]
  node [
    id 114
    label "bak"
  ]
  node [
    id 115
    label "ABS"
  ]
  node [
    id 116
    label "most"
  ]
  node [
    id 117
    label "pas_bezpiecze&#324;stwa"
  ]
  node [
    id 118
    label "spryskiwacz"
  ]
  node [
    id 119
    label "t&#322;umik"
  ]
  node [
    id 120
    label "tempomat"
  ]
  node [
    id 121
    label "rzecznik"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 121
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 35
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 12
    target 112
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 12
    target 117
  ]
  edge [
    source 12
    target 118
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
]
