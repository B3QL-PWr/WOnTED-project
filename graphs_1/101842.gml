graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.083989501312336
  density 0.005484182898190358
  graphCliqueNumber 2
  node [
    id 0
    label "tekst"
    origin "text"
  ]
  node [
    id 1
    label "silvermana"
    origin "text"
  ]
  node [
    id 2
    label "po&#347;wi&#281;cony"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "sundance"
    origin "text"
  ]
  node [
    id 5
    label "jeden"
    origin "text"
  ]
  node [
    id 6
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 7
    label "festiwal"
    origin "text"
  ]
  node [
    id 8
    label "kino"
    origin "text"
  ]
  node [
    id 9
    label "niezale&#380;ny"
    origin "text"
  ]
  node [
    id 10
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 11
    label "tegoroczny"
    origin "text"
  ]
  node [
    id 12
    label "edycja"
    origin "text"
  ]
  node [
    id 13
    label "startowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "w&#322;a&#347;nie"
    origin "text"
  ]
  node [
    id 15
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 16
    label "organizator"
    origin "text"
  ]
  node [
    id 17
    label "przygotowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "tym"
    origin "text"
  ]
  node [
    id 19
    label "rok"
    origin "text"
  ]
  node [
    id 20
    label "metr"
    origin "text"
  ]
  node [
    id 21
    label "panel"
    origin "text"
  ]
  node [
    id 22
    label "jaki"
    origin "text"
  ]
  node [
    id 23
    label "filmowy"
    origin "text"
  ]
  node [
    id 24
    label "przynie&#347;&#263;"
    origin "text"
  ]
  node [
    id 25
    label "internet"
    origin "text"
  ]
  node [
    id 26
    label "silverman"
    origin "text"
  ]
  node [
    id 27
    label "zadowala&#263;"
    origin "text"
  ]
  node [
    id 28
    label "si&#281;"
    origin "text"
  ]
  node [
    id 29
    label "g&#322;adki"
    origin "text"
  ]
  node [
    id 30
    label "zdanie"
    origin "text"
  ]
  node [
    id 31
    label "sieciowy"
    origin "text"
  ]
  node [
    id 32
    label "rewolucja"
    origin "text"
  ]
  node [
    id 33
    label "pokazywa&#263;"
    origin "text"
  ]
  node [
    id 34
    label "usa"
    origin "text"
  ]
  node [
    id 35
    label "odwrocie"
    origin "text"
  ]
  node [
    id 36
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 37
    label "zainteresowanie"
    origin "text"
  ]
  node [
    id 38
    label "internauta"
    origin "text"
  ]
  node [
    id 39
    label "duha"
    origin "text"
  ]
  node [
    id 40
    label "tw&#243;rca"
    origin "text"
  ]
  node [
    id 41
    label "trudno"
    origin "text"
  ]
  node [
    id 42
    label "spieni&#281;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 43
    label "bez"
    origin "text"
  ]
  node [
    id 44
    label "pieni&#261;dz"
    origin "text"
  ]
  node [
    id 45
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 46
    label "film"
    origin "text"
  ]
  node [
    id 47
    label "pisa&#263;"
  ]
  node [
    id 48
    label "odmianka"
  ]
  node [
    id 49
    label "opu&#347;ci&#263;"
  ]
  node [
    id 50
    label "wypowied&#378;"
  ]
  node [
    id 51
    label "wytw&#243;r"
  ]
  node [
    id 52
    label "koniektura"
  ]
  node [
    id 53
    label "preparacja"
  ]
  node [
    id 54
    label "ekscerpcja"
  ]
  node [
    id 55
    label "j&#281;zykowo"
  ]
  node [
    id 56
    label "obelga"
  ]
  node [
    id 57
    label "dzie&#322;o"
  ]
  node [
    id 58
    label "redakcja"
  ]
  node [
    id 59
    label "pomini&#281;cie"
  ]
  node [
    id 60
    label "oddany"
  ]
  node [
    id 61
    label "si&#281;ga&#263;"
  ]
  node [
    id 62
    label "trwa&#263;"
  ]
  node [
    id 63
    label "obecno&#347;&#263;"
  ]
  node [
    id 64
    label "stan"
  ]
  node [
    id 65
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 66
    label "stand"
  ]
  node [
    id 67
    label "mie&#263;_miejsce"
  ]
  node [
    id 68
    label "uczestniczy&#263;"
  ]
  node [
    id 69
    label "chodzi&#263;"
  ]
  node [
    id 70
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 71
    label "equal"
  ]
  node [
    id 72
    label "kieliszek"
  ]
  node [
    id 73
    label "shot"
  ]
  node [
    id 74
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 75
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 76
    label "jaki&#347;"
  ]
  node [
    id 77
    label "jednolicie"
  ]
  node [
    id 78
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 79
    label "w&#243;dka"
  ]
  node [
    id 80
    label "ten"
  ]
  node [
    id 81
    label "ujednolicenie"
  ]
  node [
    id 82
    label "jednakowy"
  ]
  node [
    id 83
    label "silny"
  ]
  node [
    id 84
    label "wa&#380;nie"
  ]
  node [
    id 85
    label "eksponowany"
  ]
  node [
    id 86
    label "istotnie"
  ]
  node [
    id 87
    label "znaczny"
  ]
  node [
    id 88
    label "dobry"
  ]
  node [
    id 89
    label "wynios&#322;y"
  ]
  node [
    id 90
    label "dono&#347;ny"
  ]
  node [
    id 91
    label "Nowe_Horyzonty"
  ]
  node [
    id 92
    label "Interwizja"
  ]
  node [
    id 93
    label "Open'er"
  ]
  node [
    id 94
    label "Przystanek_Woodstock"
  ]
  node [
    id 95
    label "impreza"
  ]
  node [
    id 96
    label "Woodstock"
  ]
  node [
    id 97
    label "Metalmania"
  ]
  node [
    id 98
    label "Opole"
  ]
  node [
    id 99
    label "FAMA"
  ]
  node [
    id 100
    label "Eurowizja"
  ]
  node [
    id 101
    label "Brutal"
  ]
  node [
    id 102
    label "animatronika"
  ]
  node [
    id 103
    label "cyrk"
  ]
  node [
    id 104
    label "seans"
  ]
  node [
    id 105
    label "dorobek"
  ]
  node [
    id 106
    label "ekran"
  ]
  node [
    id 107
    label "budynek"
  ]
  node [
    id 108
    label "kinoteatr"
  ]
  node [
    id 109
    label "picture"
  ]
  node [
    id 110
    label "bioskop"
  ]
  node [
    id 111
    label "sztuka"
  ]
  node [
    id 112
    label "muza"
  ]
  node [
    id 113
    label "usamodzielnianie_si&#281;"
  ]
  node [
    id 114
    label "usamodzielnienie"
  ]
  node [
    id 115
    label "niezale&#380;nie"
  ]
  node [
    id 116
    label "usamodzielnianie"
  ]
  node [
    id 117
    label "tegorocznie"
  ]
  node [
    id 118
    label "bie&#380;&#261;cy"
  ]
  node [
    id 119
    label "impression"
  ]
  node [
    id 120
    label "odmiana"
  ]
  node [
    id 121
    label "zjawisko_spo&#322;eczne"
  ]
  node [
    id 122
    label "notification"
  ]
  node [
    id 123
    label "cykl"
  ]
  node [
    id 124
    label "zmiana"
  ]
  node [
    id 125
    label "produkcja"
  ]
  node [
    id 126
    label "egzemplarz"
  ]
  node [
    id 127
    label "katapultowa&#263;"
  ]
  node [
    id 128
    label "zaleca&#263;_si&#281;"
  ]
  node [
    id 129
    label "begin"
  ]
  node [
    id 130
    label "samolot"
  ]
  node [
    id 131
    label "zaczyna&#263;"
  ]
  node [
    id 132
    label "odchodzi&#263;"
  ]
  node [
    id 133
    label "dok&#322;adnie"
  ]
  node [
    id 134
    label "doba"
  ]
  node [
    id 135
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 136
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 137
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 138
    label "spiritus_movens"
  ]
  node [
    id 139
    label "realizator"
  ]
  node [
    id 140
    label "arrange"
  ]
  node [
    id 141
    label "spowodowa&#263;"
  ]
  node [
    id 142
    label "dress"
  ]
  node [
    id 143
    label "wyszkoli&#263;"
  ]
  node [
    id 144
    label "narz&#261;dzi&#263;"
  ]
  node [
    id 145
    label "wytworzy&#263;"
  ]
  node [
    id 146
    label "ukierunkowa&#263;"
  ]
  node [
    id 147
    label "train"
  ]
  node [
    id 148
    label "wykona&#263;"
  ]
  node [
    id 149
    label "zrobi&#263;"
  ]
  node [
    id 150
    label "cook"
  ]
  node [
    id 151
    label "set"
  ]
  node [
    id 152
    label "stulecie"
  ]
  node [
    id 153
    label "kalendarz"
  ]
  node [
    id 154
    label "czas"
  ]
  node [
    id 155
    label "pora_roku"
  ]
  node [
    id 156
    label "cykl_astronomiczny"
  ]
  node [
    id 157
    label "p&#243;&#322;rocze"
  ]
  node [
    id 158
    label "grupa"
  ]
  node [
    id 159
    label "kwarta&#322;"
  ]
  node [
    id 160
    label "kurs"
  ]
  node [
    id 161
    label "jubileusz"
  ]
  node [
    id 162
    label "miesi&#261;c"
  ]
  node [
    id 163
    label "lata"
  ]
  node [
    id 164
    label "martwy_sezon"
  ]
  node [
    id 165
    label "meter"
  ]
  node [
    id 166
    label "decymetr"
  ]
  node [
    id 167
    label "megabyte"
  ]
  node [
    id 168
    label "plon"
  ]
  node [
    id 169
    label "metrum"
  ]
  node [
    id 170
    label "dekametr"
  ]
  node [
    id 171
    label "jednostka_powierzchni"
  ]
  node [
    id 172
    label "uk&#322;ad_SI"
  ]
  node [
    id 173
    label "literaturoznawstwo"
  ]
  node [
    id 174
    label "wiersz"
  ]
  node [
    id 175
    label "gigametr"
  ]
  node [
    id 176
    label "miara"
  ]
  node [
    id 177
    label "nauczyciel"
  ]
  node [
    id 178
    label "kilometr_kwadratowy"
  ]
  node [
    id 179
    label "jednostka_metryczna"
  ]
  node [
    id 180
    label "jednostka_masy"
  ]
  node [
    id 181
    label "centymetr_kwadratowy"
  ]
  node [
    id 182
    label "sonda&#380;"
  ]
  node [
    id 183
    label "dyskusja"
  ]
  node [
    id 184
    label "coffer"
  ]
  node [
    id 185
    label "p&#322;yta"
  ]
  node [
    id 186
    label "konsola"
  ]
  node [
    id 187
    label "ok&#322;adzina"
  ]
  node [
    id 188
    label "opakowanie"
  ]
  node [
    id 189
    label "cinematic"
  ]
  node [
    id 190
    label "filmowo"
  ]
  node [
    id 191
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 192
    label "da&#263;"
  ]
  node [
    id 193
    label "ponie&#347;&#263;"
  ]
  node [
    id 194
    label "get"
  ]
  node [
    id 195
    label "przytacha&#263;"
  ]
  node [
    id 196
    label "increase"
  ]
  node [
    id 197
    label "doda&#263;"
  ]
  node [
    id 198
    label "zanie&#347;&#263;"
  ]
  node [
    id 199
    label "poda&#263;"
  ]
  node [
    id 200
    label "carry"
  ]
  node [
    id 201
    label "us&#322;uga_internetowa"
  ]
  node [
    id 202
    label "biznes_elektroniczny"
  ]
  node [
    id 203
    label "punkt_dost&#281;pu"
  ]
  node [
    id 204
    label "hipertekst"
  ]
  node [
    id 205
    label "gra_sieciowa"
  ]
  node [
    id 206
    label "mem"
  ]
  node [
    id 207
    label "e-hazard"
  ]
  node [
    id 208
    label "sie&#263;_komputerowa"
  ]
  node [
    id 209
    label "media"
  ]
  node [
    id 210
    label "podcast"
  ]
  node [
    id 211
    label "netbook"
  ]
  node [
    id 212
    label "provider"
  ]
  node [
    id 213
    label "cyberprzestrze&#324;"
  ]
  node [
    id 214
    label "grooming"
  ]
  node [
    id 215
    label "strona"
  ]
  node [
    id 216
    label "satisfy"
  ]
  node [
    id 217
    label "wzbudza&#263;"
  ]
  node [
    id 218
    label "kontentowa&#263;"
  ]
  node [
    id 219
    label "elegancki"
  ]
  node [
    id 220
    label "&#322;adny"
  ]
  node [
    id 221
    label "kulturalny"
  ]
  node [
    id 222
    label "wyg&#322;adzanie_si&#281;"
  ]
  node [
    id 223
    label "prosty"
  ]
  node [
    id 224
    label "g&#322;adzenie"
  ]
  node [
    id 225
    label "r&#243;wny"
  ]
  node [
    id 226
    label "og&#243;lnikowy"
  ]
  node [
    id 227
    label "g&#322;adko"
  ]
  node [
    id 228
    label "przyg&#322;adzanie"
  ]
  node [
    id 229
    label "jednobarwny"
  ]
  node [
    id 230
    label "cisza"
  ]
  node [
    id 231
    label "obtaczanie"
  ]
  node [
    id 232
    label "wyg&#322;adzenie"
  ]
  node [
    id 233
    label "&#322;atwy"
  ]
  node [
    id 234
    label "wyr&#243;wnanie"
  ]
  node [
    id 235
    label "atrakcyjny"
  ]
  node [
    id 236
    label "wyg&#322;adzenie_si&#281;"
  ]
  node [
    id 237
    label "okr&#261;g&#322;y"
  ]
  node [
    id 238
    label "bezproblemowy"
  ]
  node [
    id 239
    label "grzeczny"
  ]
  node [
    id 240
    label "przyg&#322;adzenie"
  ]
  node [
    id 241
    label "nieruchomy"
  ]
  node [
    id 242
    label "attitude"
  ]
  node [
    id 243
    label "system"
  ]
  node [
    id 244
    label "przedstawienie"
  ]
  node [
    id 245
    label "fraza"
  ]
  node [
    id 246
    label "prison_term"
  ]
  node [
    id 247
    label "adjudication"
  ]
  node [
    id 248
    label "przekazanie"
  ]
  node [
    id 249
    label "pass"
  ]
  node [
    id 250
    label "wyra&#380;enie"
  ]
  node [
    id 251
    label "okres"
  ]
  node [
    id 252
    label "z&#322;o&#380;enie_egzaminu"
  ]
  node [
    id 253
    label "wypowiedzenie"
  ]
  node [
    id 254
    label "konektyw"
  ]
  node [
    id 255
    label "zaliczenie"
  ]
  node [
    id 256
    label "stanowisko"
  ]
  node [
    id 257
    label "powierzenie"
  ]
  node [
    id 258
    label "antylogizm"
  ]
  node [
    id 259
    label "zmuszenie"
  ]
  node [
    id 260
    label "szko&#322;a"
  ]
  node [
    id 261
    label "elektroniczny"
  ]
  node [
    id 262
    label "sieciowo"
  ]
  node [
    id 263
    label "internetowo"
  ]
  node [
    id 264
    label "netowy"
  ]
  node [
    id 265
    label "typowy"
  ]
  node [
    id 266
    label "komisarz_wojskowy"
  ]
  node [
    id 267
    label "aksamitna_rewolucja"
  ]
  node [
    id 268
    label "zwrot"
  ]
  node [
    id 269
    label "walka"
  ]
  node [
    id 270
    label "bunt"
  ]
  node [
    id 271
    label "burza"
  ]
  node [
    id 272
    label "przeszkala&#263;"
  ]
  node [
    id 273
    label "warto&#347;&#263;"
  ]
  node [
    id 274
    label "informowa&#263;"
  ]
  node [
    id 275
    label "introduce"
  ]
  node [
    id 276
    label "bespeak"
  ]
  node [
    id 277
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 278
    label "represent"
  ]
  node [
    id 279
    label "indicate"
  ]
  node [
    id 280
    label "wyraz"
  ]
  node [
    id 281
    label "wyra&#380;a&#263;"
  ]
  node [
    id 282
    label "exhibit"
  ]
  node [
    id 283
    label "powodowa&#263;"
  ]
  node [
    id 284
    label "podawa&#263;"
  ]
  node [
    id 285
    label "exsert"
  ]
  node [
    id 286
    label "przedstawia&#263;"
  ]
  node [
    id 287
    label "ciekawo&#347;&#263;"
  ]
  node [
    id 288
    label "care"
  ]
  node [
    id 289
    label "emocja"
  ]
  node [
    id 290
    label "zainteresowanie_si&#281;"
  ]
  node [
    id 291
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 292
    label "love"
  ]
  node [
    id 293
    label "wzbudzenie"
  ]
  node [
    id 294
    label "u&#380;ytkownik"
  ]
  node [
    id 295
    label "pomys&#322;odawca"
  ]
  node [
    id 296
    label "kszta&#322;ciciel"
  ]
  node [
    id 297
    label "tworzyciel"
  ]
  node [
    id 298
    label "&#347;w"
  ]
  node [
    id 299
    label "wykonawca"
  ]
  node [
    id 300
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 301
    label "trudny"
  ]
  node [
    id 302
    label "hard"
  ]
  node [
    id 303
    label "capitalize"
  ]
  node [
    id 304
    label "zhandlowa&#263;"
  ]
  node [
    id 305
    label "ki&#347;&#263;"
  ]
  node [
    id 306
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 307
    label "krzew"
  ]
  node [
    id 308
    label "pi&#380;maczkowate"
  ]
  node [
    id 309
    label "pestkowiec"
  ]
  node [
    id 310
    label "kwiat"
  ]
  node [
    id 311
    label "owoc"
  ]
  node [
    id 312
    label "oliwkowate"
  ]
  node [
    id 313
    label "ro&#347;lina"
  ]
  node [
    id 314
    label "hy&#263;ka"
  ]
  node [
    id 315
    label "lilac"
  ]
  node [
    id 316
    label "delfinidyna"
  ]
  node [
    id 317
    label "poda&#380;_pieni&#261;dza"
  ]
  node [
    id 318
    label "zdewaluowa&#263;"
  ]
  node [
    id 319
    label "moniak"
  ]
  node [
    id 320
    label "zdewaluowanie"
  ]
  node [
    id 321
    label "jednostka_monetarna"
  ]
  node [
    id 322
    label "&#347;rodek_p&#322;atniczy"
  ]
  node [
    id 323
    label "numizmat"
  ]
  node [
    id 324
    label "rozmienianie"
  ]
  node [
    id 325
    label "rozmienienie"
  ]
  node [
    id 326
    label "rozmieni&#263;"
  ]
  node [
    id 327
    label "dewaluowanie"
  ]
  node [
    id 328
    label "nomina&#322;"
  ]
  node [
    id 329
    label "coin"
  ]
  node [
    id 330
    label "dewaluowa&#263;"
  ]
  node [
    id 331
    label "pieni&#261;dze"
  ]
  node [
    id 332
    label "znak_pieni&#281;&#380;ny"
  ]
  node [
    id 333
    label "rozmienia&#263;"
  ]
  node [
    id 334
    label "tentegowa&#263;"
  ]
  node [
    id 335
    label "urz&#261;dza&#263;"
  ]
  node [
    id 336
    label "give"
  ]
  node [
    id 337
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 338
    label "czyni&#263;"
  ]
  node [
    id 339
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 340
    label "post&#281;powa&#263;"
  ]
  node [
    id 341
    label "wydala&#263;"
  ]
  node [
    id 342
    label "oszukiwa&#263;"
  ]
  node [
    id 343
    label "organizowa&#263;"
  ]
  node [
    id 344
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 345
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 346
    label "work"
  ]
  node [
    id 347
    label "przerabia&#263;"
  ]
  node [
    id 348
    label "stylizowa&#263;"
  ]
  node [
    id 349
    label "falowa&#263;"
  ]
  node [
    id 350
    label "act"
  ]
  node [
    id 351
    label "peddle"
  ]
  node [
    id 352
    label "ukazywa&#263;"
  ]
  node [
    id 353
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 354
    label "praca"
  ]
  node [
    id 355
    label "rozbieg&#243;wka"
  ]
  node [
    id 356
    label "block"
  ]
  node [
    id 357
    label "blik"
  ]
  node [
    id 358
    label "odczula&#263;"
  ]
  node [
    id 359
    label "rola"
  ]
  node [
    id 360
    label "trawiarnia"
  ]
  node [
    id 361
    label "b&#322;ona"
  ]
  node [
    id 362
    label "filmoteka"
  ]
  node [
    id 363
    label "odczuli&#263;"
  ]
  node [
    id 364
    label "klatka"
  ]
  node [
    id 365
    label "odczulenie"
  ]
  node [
    id 366
    label "emulsja_fotograficzna"
  ]
  node [
    id 367
    label "odczulanie"
  ]
  node [
    id 368
    label "scena"
  ]
  node [
    id 369
    label "czo&#322;&#243;wka"
  ]
  node [
    id 370
    label "ty&#322;&#243;wka"
  ]
  node [
    id 371
    label "napisy"
  ]
  node [
    id 372
    label "photograph"
  ]
  node [
    id 373
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 374
    label "postprodukcja"
  ]
  node [
    id 375
    label "sklejarka"
  ]
  node [
    id 376
    label "anamorfoza"
  ]
  node [
    id 377
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 378
    label "ta&#347;ma"
  ]
  node [
    id 379
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 380
    label "uj&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 34
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 41
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 45
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 119
  ]
  edge [
    source 12
    target 120
  ]
  edge [
    source 12
    target 121
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 68
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 17
    target 145
  ]
  edge [
    source 17
    target 146
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 20
    target 166
  ]
  edge [
    source 20
    target 167
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 20
    target 172
  ]
  edge [
    source 20
    target 173
  ]
  edge [
    source 20
    target 174
  ]
  edge [
    source 20
    target 175
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 191
  ]
  edge [
    source 24
    target 192
  ]
  edge [
    source 24
    target 193
  ]
  edge [
    source 24
    target 194
  ]
  edge [
    source 24
    target 195
  ]
  edge [
    source 24
    target 196
  ]
  edge [
    source 24
    target 197
  ]
  edge [
    source 24
    target 198
  ]
  edge [
    source 24
    target 199
  ]
  edge [
    source 24
    target 200
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 201
  ]
  edge [
    source 25
    target 202
  ]
  edge [
    source 25
    target 203
  ]
  edge [
    source 25
    target 204
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 25
    target 206
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 25
    target 209
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 25
    target 213
  ]
  edge [
    source 25
    target 214
  ]
  edge [
    source 25
    target 215
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 216
  ]
  edge [
    source 27
    target 217
  ]
  edge [
    source 27
    target 218
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 219
  ]
  edge [
    source 29
    target 220
  ]
  edge [
    source 29
    target 221
  ]
  edge [
    source 29
    target 222
  ]
  edge [
    source 29
    target 223
  ]
  edge [
    source 29
    target 224
  ]
  edge [
    source 29
    target 225
  ]
  edge [
    source 29
    target 226
  ]
  edge [
    source 29
    target 227
  ]
  edge [
    source 29
    target 228
  ]
  edge [
    source 29
    target 229
  ]
  edge [
    source 29
    target 230
  ]
  edge [
    source 29
    target 231
  ]
  edge [
    source 29
    target 232
  ]
  edge [
    source 29
    target 233
  ]
  edge [
    source 29
    target 234
  ]
  edge [
    source 29
    target 235
  ]
  edge [
    source 29
    target 236
  ]
  edge [
    source 29
    target 237
  ]
  edge [
    source 29
    target 238
  ]
  edge [
    source 29
    target 239
  ]
  edge [
    source 29
    target 240
  ]
  edge [
    source 29
    target 241
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 242
  ]
  edge [
    source 30
    target 243
  ]
  edge [
    source 30
    target 244
  ]
  edge [
    source 30
    target 245
  ]
  edge [
    source 30
    target 246
  ]
  edge [
    source 30
    target 247
  ]
  edge [
    source 30
    target 248
  ]
  edge [
    source 30
    target 249
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 30
    target 256
  ]
  edge [
    source 30
    target 257
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 30
    target 259
  ]
  edge [
    source 30
    target 260
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 261
  ]
  edge [
    source 31
    target 262
  ]
  edge [
    source 31
    target 263
  ]
  edge [
    source 31
    target 264
  ]
  edge [
    source 31
    target 265
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 266
  ]
  edge [
    source 32
    target 267
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 32
    target 270
  ]
  edge [
    source 32
    target 271
  ]
  edge [
    source 33
    target 272
  ]
  edge [
    source 33
    target 273
  ]
  edge [
    source 33
    target 274
  ]
  edge [
    source 33
    target 275
  ]
  edge [
    source 33
    target 276
  ]
  edge [
    source 33
    target 277
  ]
  edge [
    source 33
    target 278
  ]
  edge [
    source 33
    target 279
  ]
  edge [
    source 33
    target 280
  ]
  edge [
    source 33
    target 281
  ]
  edge [
    source 33
    target 282
  ]
  edge [
    source 33
    target 283
  ]
  edge [
    source 33
    target 284
  ]
  edge [
    source 33
    target 285
  ]
  edge [
    source 33
    target 286
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 287
  ]
  edge [
    source 37
    target 288
  ]
  edge [
    source 37
    target 289
  ]
  edge [
    source 37
    target 290
  ]
  edge [
    source 37
    target 291
  ]
  edge [
    source 37
    target 292
  ]
  edge [
    source 37
    target 293
  ]
  edge [
    source 38
    target 294
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 295
  ]
  edge [
    source 40
    target 296
  ]
  edge [
    source 40
    target 297
  ]
  edge [
    source 40
    target 298
  ]
  edge [
    source 40
    target 299
  ]
  edge [
    source 40
    target 300
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 41
    target 45
  ]
  edge [
    source 41
    target 301
  ]
  edge [
    source 41
    target 302
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 303
  ]
  edge [
    source 42
    target 304
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 305
  ]
  edge [
    source 43
    target 306
  ]
  edge [
    source 43
    target 307
  ]
  edge [
    source 43
    target 308
  ]
  edge [
    source 43
    target 309
  ]
  edge [
    source 43
    target 310
  ]
  edge [
    source 43
    target 311
  ]
  edge [
    source 43
    target 312
  ]
  edge [
    source 43
    target 313
  ]
  edge [
    source 43
    target 314
  ]
  edge [
    source 43
    target 315
  ]
  edge [
    source 43
    target 316
  ]
  edge [
    source 44
    target 317
  ]
  edge [
    source 44
    target 318
  ]
  edge [
    source 44
    target 319
  ]
  edge [
    source 44
    target 51
  ]
  edge [
    source 44
    target 320
  ]
  edge [
    source 44
    target 321
  ]
  edge [
    source 44
    target 322
  ]
  edge [
    source 44
    target 323
  ]
  edge [
    source 44
    target 324
  ]
  edge [
    source 44
    target 325
  ]
  edge [
    source 44
    target 326
  ]
  edge [
    source 44
    target 327
  ]
  edge [
    source 44
    target 328
  ]
  edge [
    source 44
    target 329
  ]
  edge [
    source 44
    target 330
  ]
  edge [
    source 44
    target 331
  ]
  edge [
    source 44
    target 332
  ]
  edge [
    source 44
    target 333
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 334
  ]
  edge [
    source 45
    target 335
  ]
  edge [
    source 45
    target 336
  ]
  edge [
    source 45
    target 337
  ]
  edge [
    source 45
    target 338
  ]
  edge [
    source 45
    target 339
  ]
  edge [
    source 45
    target 340
  ]
  edge [
    source 45
    target 341
  ]
  edge [
    source 45
    target 342
  ]
  edge [
    source 45
    target 343
  ]
  edge [
    source 45
    target 344
  ]
  edge [
    source 45
    target 345
  ]
  edge [
    source 45
    target 346
  ]
  edge [
    source 45
    target 347
  ]
  edge [
    source 45
    target 348
  ]
  edge [
    source 45
    target 349
  ]
  edge [
    source 45
    target 350
  ]
  edge [
    source 45
    target 351
  ]
  edge [
    source 45
    target 352
  ]
  edge [
    source 45
    target 353
  ]
  edge [
    source 45
    target 354
  ]
  edge [
    source 46
    target 355
  ]
  edge [
    source 46
    target 356
  ]
  edge [
    source 46
    target 357
  ]
  edge [
    source 46
    target 358
  ]
  edge [
    source 46
    target 359
  ]
  edge [
    source 46
    target 360
  ]
  edge [
    source 46
    target 361
  ]
  edge [
    source 46
    target 362
  ]
  edge [
    source 46
    target 111
  ]
  edge [
    source 46
    target 112
  ]
  edge [
    source 46
    target 363
  ]
  edge [
    source 46
    target 364
  ]
  edge [
    source 46
    target 365
  ]
  edge [
    source 46
    target 366
  ]
  edge [
    source 46
    target 102
  ]
  edge [
    source 46
    target 105
  ]
  edge [
    source 46
    target 367
  ]
  edge [
    source 46
    target 368
  ]
  edge [
    source 46
    target 369
  ]
  edge [
    source 46
    target 370
  ]
  edge [
    source 46
    target 371
  ]
  edge [
    source 46
    target 372
  ]
  edge [
    source 46
    target 373
  ]
  edge [
    source 46
    target 374
  ]
  edge [
    source 46
    target 375
  ]
  edge [
    source 46
    target 376
  ]
  edge [
    source 46
    target 377
  ]
  edge [
    source 46
    target 378
  ]
  edge [
    source 46
    target 379
  ]
  edge [
    source 46
    target 380
  ]
]
