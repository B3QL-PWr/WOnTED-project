graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9230769230769231
  density 0.07692307692307693
  graphCliqueNumber 2
  node [
    id 0
    label "okej"
    origin "text"
  ]
  node [
    id 1
    label "mirka"
    origin "text"
  ]
  node [
    id 2
    label "prawojazdy"
    origin "text"
  ]
  node [
    id 3
    label "pytanie"
    origin "text"
  ]
  node [
    id 4
    label "w_porz&#261;dku"
  ]
  node [
    id 5
    label "dobry"
  ]
  node [
    id 6
    label "dobrze"
  ]
  node [
    id 7
    label "sprawa"
  ]
  node [
    id 8
    label "zadanie"
  ]
  node [
    id 9
    label "wypowied&#378;"
  ]
  node [
    id 10
    label "problemat"
  ]
  node [
    id 11
    label "rozpytywanie"
  ]
  node [
    id 12
    label "sprawdzian"
  ]
  node [
    id 13
    label "przes&#322;uchiwanie"
  ]
  node [
    id 14
    label "wypytanie"
  ]
  node [
    id 15
    label "zwracanie_si&#281;"
  ]
  node [
    id 16
    label "wypowiedzenie"
  ]
  node [
    id 17
    label "wywo&#322;ywanie"
  ]
  node [
    id 18
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 19
    label "problematyka"
  ]
  node [
    id 20
    label "question"
  ]
  node [
    id 21
    label "sprawdzanie"
  ]
  node [
    id 22
    label "odpowiadanie"
  ]
  node [
    id 23
    label "survey"
  ]
  node [
    id 24
    label "odpowiada&#263;"
  ]
  node [
    id 25
    label "egzaminowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
]
