graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "strach"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "ba&#263;"
    origin "text"
  ]
  node [
    id 3
    label "akatyzja"
  ]
  node [
    id 4
    label "phobia"
  ]
  node [
    id 5
    label "ba&#263;_si&#281;"
  ]
  node [
    id 6
    label "emocja"
  ]
  node [
    id 7
    label "zastraszenie"
  ]
  node [
    id 8
    label "zjawa"
  ]
  node [
    id 9
    label "straszyd&#322;o"
  ]
  node [
    id 10
    label "zastraszanie"
  ]
  node [
    id 11
    label "spirit"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
]
