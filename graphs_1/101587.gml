graph [
  maxDegree 31
  minDegree 1
  meanDegree 2.5054945054945055
  density 0.01384251108008014
  graphCliqueNumber 7
  node [
    id 0
    label "posiedzenie"
    origin "text"
  ]
  node [
    id 1
    label "parlamentarny"
    origin "text"
  ]
  node [
    id 2
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 3
    label "rzecz"
    origin "text"
  ]
  node [
    id 4
    label "katolicki"
    origin "text"
  ]
  node [
    id 5
    label "nauka"
    origin "text"
  ]
  node [
    id 6
    label "spo&#322;eczny"
    origin "text"
  ]
  node [
    id 7
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 8
    label "si&#281;"
    origin "text"
  ]
  node [
    id 9
    label "dzie&#324;"
    origin "text"
  ]
  node [
    id 10
    label "dzisiejsi"
    origin "text"
  ]
  node [
    id 11
    label "godz"
    origin "text"
  ]
  node [
    id 12
    label "sala"
    origin "text"
  ]
  node [
    id 13
    label "klubowy"
    origin "text"
  ]
  node [
    id 14
    label "nowa"
    origin "text"
  ]
  node [
    id 15
    label "dom"
    origin "text"
  ]
  node [
    id 16
    label "poselski"
    origin "text"
  ]
  node [
    id 17
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 18
    label "bardzo"
    origin "text"
  ]
  node [
    id 19
    label "porobienie"
  ]
  node [
    id 20
    label "odsiedzenie"
  ]
  node [
    id 21
    label "dyskusja"
  ]
  node [
    id 22
    label "conference"
  ]
  node [
    id 23
    label "convention"
  ]
  node [
    id 24
    label "adjustment"
  ]
  node [
    id 25
    label "zgromadzenie"
  ]
  node [
    id 26
    label "pobycie"
  ]
  node [
    id 27
    label "konsylium"
  ]
  node [
    id 28
    label "sp&#281;dzenie_czasu"
  ]
  node [
    id 29
    label "parlamentowy"
  ]
  node [
    id 30
    label "stosowny"
  ]
  node [
    id 31
    label "demokratyczny"
  ]
  node [
    id 32
    label "parlamentarnie"
  ]
  node [
    id 33
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 34
    label "whole"
  ]
  node [
    id 35
    label "odm&#322;adza&#263;"
  ]
  node [
    id 36
    label "zabudowania"
  ]
  node [
    id 37
    label "odm&#322;odzenie"
  ]
  node [
    id 38
    label "zespolik"
  ]
  node [
    id 39
    label "skupienie"
  ]
  node [
    id 40
    label "schorzenie"
  ]
  node [
    id 41
    label "grupa"
  ]
  node [
    id 42
    label "Depeche_Mode"
  ]
  node [
    id 43
    label "Mazowsze"
  ]
  node [
    id 44
    label "ro&#347;lina"
  ]
  node [
    id 45
    label "zbi&#243;r"
  ]
  node [
    id 46
    label "The_Beatles"
  ]
  node [
    id 47
    label "group"
  ]
  node [
    id 48
    label "&#346;wietliki"
  ]
  node [
    id 49
    label "odm&#322;adzanie"
  ]
  node [
    id 50
    label "batch"
  ]
  node [
    id 51
    label "obiekt"
  ]
  node [
    id 52
    label "temat"
  ]
  node [
    id 53
    label "istota"
  ]
  node [
    id 54
    label "wpada&#263;"
  ]
  node [
    id 55
    label "wpa&#347;&#263;"
  ]
  node [
    id 56
    label "przedmiot"
  ]
  node [
    id 57
    label "wpadanie"
  ]
  node [
    id 58
    label "kultura"
  ]
  node [
    id 59
    label "przyroda"
  ]
  node [
    id 60
    label "mienie"
  ]
  node [
    id 61
    label "object"
  ]
  node [
    id 62
    label "wpadni&#281;cie"
  ]
  node [
    id 63
    label "po_katolicku"
  ]
  node [
    id 64
    label "zgodny"
  ]
  node [
    id 65
    label "chrze&#347;cija&#324;ski"
  ]
  node [
    id 66
    label "wyznaniowy"
  ]
  node [
    id 67
    label "powszechny"
  ]
  node [
    id 68
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 69
    label "typowy"
  ]
  node [
    id 70
    label "nauki_o_Ziemi"
  ]
  node [
    id 71
    label "teoria_naukowa"
  ]
  node [
    id 72
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 73
    label "nauki_o_poznaniu"
  ]
  node [
    id 74
    label "nomotetyczny"
  ]
  node [
    id 75
    label "metodologia"
  ]
  node [
    id 76
    label "przem&#243;wienie"
  ]
  node [
    id 77
    label "wiedza"
  ]
  node [
    id 78
    label "kultura_duchowa"
  ]
  node [
    id 79
    label "nauki_penalne"
  ]
  node [
    id 80
    label "systematyka"
  ]
  node [
    id 81
    label "inwentyka"
  ]
  node [
    id 82
    label "dziedzina"
  ]
  node [
    id 83
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 84
    label "miasteczko_rowerowe"
  ]
  node [
    id 85
    label "fotowoltaika"
  ]
  node [
    id 86
    label "porada"
  ]
  node [
    id 87
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 88
    label "proces"
  ]
  node [
    id 89
    label "imagineskopia"
  ]
  node [
    id 90
    label "typologia"
  ]
  node [
    id 91
    label "&#322;awa_szkolna"
  ]
  node [
    id 92
    label "niepubliczny"
  ]
  node [
    id 93
    label "spo&#322;ecznie"
  ]
  node [
    id 94
    label "publiczny"
  ]
  node [
    id 95
    label "reserve"
  ]
  node [
    id 96
    label "przej&#347;&#263;"
  ]
  node [
    id 97
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 98
    label "s&#322;o&#324;ce"
  ]
  node [
    id 99
    label "czynienie_si&#281;"
  ]
  node [
    id 100
    label "&#347;wiat&#322;o_dzienne"
  ]
  node [
    id 101
    label "czas"
  ]
  node [
    id 102
    label "long_time"
  ]
  node [
    id 103
    label "przedpo&#322;udnie"
  ]
  node [
    id 104
    label "Dzie&#324;_Zaduszny"
  ]
  node [
    id 105
    label "czyni&#263;_si&#281;"
  ]
  node [
    id 106
    label "tydzie&#324;"
  ]
  node [
    id 107
    label "godzina"
  ]
  node [
    id 108
    label "t&#322;usty_czwartek"
  ]
  node [
    id 109
    label "wsta&#263;"
  ]
  node [
    id 110
    label "day"
  ]
  node [
    id 111
    label "&#347;wi&#281;ty_Jan"
  ]
  node [
    id 112
    label "przedwiecz&#243;r"
  ]
  node [
    id 113
    label "Sylwester"
  ]
  node [
    id 114
    label "po&#322;udnie"
  ]
  node [
    id 115
    label "wzej&#347;cie"
  ]
  node [
    id 116
    label "podwiecz&#243;r"
  ]
  node [
    id 117
    label "&#347;wi&#281;ty_Miko&#322;aj"
  ]
  node [
    id 118
    label "rano"
  ]
  node [
    id 119
    label "termin"
  ]
  node [
    id 120
    label "ranek"
  ]
  node [
    id 121
    label "doba"
  ]
  node [
    id 122
    label "wiecz&#243;r"
  ]
  node [
    id 123
    label "walentynki"
  ]
  node [
    id 124
    label "popo&#322;udnie"
  ]
  node [
    id 125
    label "noc"
  ]
  node [
    id 126
    label "wstanie"
  ]
  node [
    id 127
    label "publiczno&#347;&#263;"
  ]
  node [
    id 128
    label "audience"
  ]
  node [
    id 129
    label "pomieszczenie"
  ]
  node [
    id 130
    label "klubowo"
  ]
  node [
    id 131
    label "rozrywkowy"
  ]
  node [
    id 132
    label "gwiazda"
  ]
  node [
    id 133
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 134
    label "garderoba"
  ]
  node [
    id 135
    label "wiecha"
  ]
  node [
    id 136
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 137
    label "budynek"
  ]
  node [
    id 138
    label "fratria"
  ]
  node [
    id 139
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 140
    label "poj&#281;cie"
  ]
  node [
    id 141
    label "rodzina"
  ]
  node [
    id 142
    label "substancja_mieszkaniowa"
  ]
  node [
    id 143
    label "instytucja"
  ]
  node [
    id 144
    label "dom_rodzinny"
  ]
  node [
    id 145
    label "stead"
  ]
  node [
    id 146
    label "siedziba"
  ]
  node [
    id 147
    label "odmawia&#263;"
  ]
  node [
    id 148
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 149
    label "sk&#322;ada&#263;"
  ]
  node [
    id 150
    label "thank"
  ]
  node [
    id 151
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 152
    label "wyra&#380;a&#263;"
  ]
  node [
    id 153
    label "etykieta"
  ]
  node [
    id 154
    label "w_chuj"
  ]
  node [
    id 155
    label "na"
  ]
  node [
    id 156
    label "Jaros&#322;awa"
  ]
  node [
    id 157
    label "Kalinowski"
  ]
  node [
    id 158
    label "fundusz"
  ]
  node [
    id 159
    label "sp&#243;jno&#347;&#263;"
  ]
  node [
    id 160
    label "ustawa"
  ]
  node [
    id 161
    label "ojciec"
  ]
  node [
    id 162
    label "podatek"
  ]
  node [
    id 163
    label "dochodowy"
  ]
  node [
    id 164
    label "od"
  ]
  node [
    id 165
    label "osoba"
  ]
  node [
    id 166
    label "fizyczny"
  ]
  node [
    id 167
    label "prawny"
  ]
  node [
    id 168
    label "prawo"
  ]
  node [
    id 169
    label "celny"
  ]
  node [
    id 170
    label "kodeks"
  ]
  node [
    id 171
    label "karny"
  ]
  node [
    id 172
    label "skarbowy"
  ]
  node [
    id 173
    label "ordynacja"
  ]
  node [
    id 174
    label "podatkowy"
  ]
  node [
    id 175
    label "komitet"
  ]
  node [
    id 176
    label "stabilno&#347;&#263;"
  ]
  node [
    id 177
    label "finansowy"
  ]
  node [
    id 178
    label "ustr&#243;j"
  ]
  node [
    id 179
    label "s&#261;d"
  ]
  node [
    id 180
    label "konwent"
  ]
  node [
    id 181
    label "senior"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 155
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 155
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 155
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 5
    target 75
  ]
  edge [
    source 5
    target 76
  ]
  edge [
    source 5
    target 77
  ]
  edge [
    source 5
    target 78
  ]
  edge [
    source 5
    target 79
  ]
  edge [
    source 5
    target 80
  ]
  edge [
    source 5
    target 81
  ]
  edge [
    source 5
    target 82
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 155
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 25
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 132
  ]
  edge [
    source 14
    target 133
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 41
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 147
  ]
  edge [
    source 17
    target 148
  ]
  edge [
    source 17
    target 149
  ]
  edge [
    source 17
    target 150
  ]
  edge [
    source 17
    target 151
  ]
  edge [
    source 17
    target 152
  ]
  edge [
    source 17
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 67
    target 160
  ]
  edge [
    source 67
    target 168
  ]
  edge [
    source 67
    target 161
  ]
  edge [
    source 67
    target 178
  ]
  edge [
    source 67
    target 179
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 160
    target 161
  ]
  edge [
    source 160
    target 162
  ]
  edge [
    source 160
    target 163
  ]
  edge [
    source 160
    target 164
  ]
  edge [
    source 160
    target 165
  ]
  edge [
    source 160
    target 166
  ]
  edge [
    source 160
    target 167
  ]
  edge [
    source 160
    target 168
  ]
  edge [
    source 160
    target 169
  ]
  edge [
    source 160
    target 170
  ]
  edge [
    source 160
    target 171
  ]
  edge [
    source 160
    target 172
  ]
  edge [
    source 160
    target 178
  ]
  edge [
    source 160
    target 179
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 161
    target 163
  ]
  edge [
    source 161
    target 164
  ]
  edge [
    source 161
    target 165
  ]
  edge [
    source 161
    target 166
  ]
  edge [
    source 161
    target 167
  ]
  edge [
    source 161
    target 168
  ]
  edge [
    source 161
    target 178
  ]
  edge [
    source 161
    target 179
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 162
    target 164
  ]
  edge [
    source 162
    target 165
  ]
  edge [
    source 162
    target 166
  ]
  edge [
    source 162
    target 167
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 163
    target 165
  ]
  edge [
    source 163
    target 166
  ]
  edge [
    source 163
    target 167
  ]
  edge [
    source 164
    target 165
  ]
  edge [
    source 164
    target 166
  ]
  edge [
    source 164
    target 167
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 165
    target 167
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 168
    target 178
  ]
  edge [
    source 168
    target 179
  ]
  edge [
    source 170
    target 171
  ]
  edge [
    source 170
    target 172
  ]
  edge [
    source 171
    target 172
  ]
  edge [
    source 173
    target 174
  ]
  edge [
    source 175
    target 176
  ]
  edge [
    source 175
    target 177
  ]
  edge [
    source 176
    target 177
  ]
  edge [
    source 178
    target 179
  ]
  edge [
    source 180
    target 181
  ]
]
