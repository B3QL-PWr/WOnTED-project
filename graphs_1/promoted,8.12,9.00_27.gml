graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9259259259259258
  density 0.07407407407407407
  graphCliqueNumber 2
  node [
    id 0
    label "niezwykle"
    origin "text"
  ]
  node [
    id 1
    label "skuteczny"
    origin "text"
  ]
  node [
    id 2
    label "spalanie"
    origin "text"
  ]
  node [
    id 3
    label "kaloria"
    origin "text"
  ]
  node [
    id 4
    label "niezwyk&#322;y"
  ]
  node [
    id 5
    label "skutkowanie"
  ]
  node [
    id 6
    label "poskutkowanie"
  ]
  node [
    id 7
    label "dobry"
  ]
  node [
    id 8
    label "sprawny"
  ]
  node [
    id 9
    label "skutecznie"
  ]
  node [
    id 10
    label "zabijanie"
  ]
  node [
    id 11
    label "proces_chemiczny"
  ]
  node [
    id 12
    label "chemikalia"
  ]
  node [
    id 13
    label "metabolizowanie"
  ]
  node [
    id 14
    label "utlenianie"
  ]
  node [
    id 15
    label "paliwo"
  ]
  node [
    id 16
    label "przygrzewanie"
  ]
  node [
    id 17
    label "zu&#380;ywanie"
  ]
  node [
    id 18
    label "podpalanie"
  ]
  node [
    id 19
    label "burning"
  ]
  node [
    id 20
    label "niszczenie"
  ]
  node [
    id 21
    label "palenie_si&#281;"
  ]
  node [
    id 22
    label "incineration"
  ]
  node [
    id 23
    label "combustion"
  ]
  node [
    id 24
    label "spiekanie_si&#281;"
  ]
  node [
    id 25
    label "jednostka_energii"
  ]
  node [
    id 26
    label "kilokaloria"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
]
