graph [
  maxDegree 43
  minDegree 1
  meanDegree 2.1774193548387095
  density 0.005869054864794366
  graphCliqueNumber 3
  node [
    id 0
    label "wniosek"
    origin "text"
  ]
  node [
    id 1
    label "zasi&#322;ek"
    origin "text"
  ]
  node [
    id 2
    label "rodzinny"
    origin "text"
  ]
  node [
    id 3
    label "nale&#380;e&#263;"
    origin "text"
  ]
  node [
    id 4
    label "do&#322;&#261;czy&#263;"
    origin "text"
  ]
  node [
    id 5
    label "kopia"
    origin "text"
  ]
  node [
    id 6
    label "dow&#243;d"
    origin "text"
  ]
  node [
    id 7
    label "osobisty"
    origin "text"
  ]
  node [
    id 8
    label "dokument"
    origin "text"
  ]
  node [
    id 9
    label "wgl&#261;d"
    origin "text"
  ]
  node [
    id 10
    label "odpis"
    origin "text"
  ]
  node [
    id 11
    label "akt"
    origin "text"
  ]
  node [
    id 12
    label "urodzenie"
    origin "text"
  ]
  node [
    id 13
    label "dziecko"
    origin "text"
  ]
  node [
    id 14
    label "orzeczenie"
    origin "text"
  ]
  node [
    id 15
    label "niepe&#322;nosprawno&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "lub"
    origin "text"
  ]
  node [
    id 17
    label "stopie&#324;"
    origin "text"
  ]
  node [
    id 18
    label "przypadek"
    origin "text"
  ]
  node [
    id 19
    label "przyznanie"
    origin "text"
  ]
  node [
    id 20
    label "&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 21
    label "uzale&#380;niony"
    origin "text"
  ]
  node [
    id 22
    label "by&#263;"
    origin "text"
  ]
  node [
    id 23
    label "orygina&#322;"
    origin "text"
  ]
  node [
    id 24
    label "za&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 25
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 26
    label "wysoki"
    origin "text"
  ]
  node [
    id 27
    label "potwierdzaj&#261;cy"
    origin "text"
  ]
  node [
    id 28
    label "kontynuowa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "przez"
    origin "text"
  ]
  node [
    id 30
    label "nauka"
    origin "text"
  ]
  node [
    id 31
    label "gdy"
    origin "text"
  ]
  node [
    id 32
    label "uko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 33
    label "rok"
    origin "text"
  ]
  node [
    id 34
    label "&#380;ycie"
    origin "text"
  ]
  node [
    id 35
    label "twierdzenie"
  ]
  node [
    id 36
    label "my&#347;l"
  ]
  node [
    id 37
    label "wnioskowanie"
  ]
  node [
    id 38
    label "propozycja"
  ]
  node [
    id 39
    label "motion"
  ]
  node [
    id 40
    label "pismo"
  ]
  node [
    id 41
    label "prayer"
  ]
  node [
    id 42
    label "zapomoga"
  ]
  node [
    id 43
    label "familijnie"
  ]
  node [
    id 44
    label "rodzinnie"
  ]
  node [
    id 45
    label "przyjemny"
  ]
  node [
    id 46
    label "wsp&#243;lny"
  ]
  node [
    id 47
    label "charakterystyczny"
  ]
  node [
    id 48
    label "swobodny"
  ]
  node [
    id 49
    label "zwi&#261;zany"
  ]
  node [
    id 50
    label "towarzyski"
  ]
  node [
    id 51
    label "ciep&#322;y"
  ]
  node [
    id 52
    label "trza"
  ]
  node [
    id 53
    label "uczestniczy&#263;"
  ]
  node [
    id 54
    label "zalicza&#263;_si&#281;"
  ]
  node [
    id 55
    label "para"
  ]
  node [
    id 56
    label "necessity"
  ]
  node [
    id 57
    label "catch"
  ]
  node [
    id 58
    label "spowodowa&#263;"
  ]
  node [
    id 59
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 60
    label "zrobi&#263;"
  ]
  node [
    id 61
    label "articulation"
  ]
  node [
    id 62
    label "dokoptowa&#263;"
  ]
  node [
    id 63
    label "wytw&#243;r"
  ]
  node [
    id 64
    label "przedmiot"
  ]
  node [
    id 65
    label "formacja"
  ]
  node [
    id 66
    label "odbitka"
  ]
  node [
    id 67
    label "extra"
  ]
  node [
    id 68
    label "chor&#261;giew"
  ]
  node [
    id 69
    label "miniatura"
  ]
  node [
    id 70
    label "ta&#347;ma_filmowa"
  ]
  node [
    id 71
    label "bro&#324;_drzewcowa"
  ]
  node [
    id 72
    label "egzemplarz"
  ]
  node [
    id 73
    label "picture"
  ]
  node [
    id 74
    label "forsing"
  ]
  node [
    id 75
    label "certificate"
  ]
  node [
    id 76
    label "rewizja"
  ]
  node [
    id 77
    label "argument"
  ]
  node [
    id 78
    label "act"
  ]
  node [
    id 79
    label "rzecz"
  ]
  node [
    id 80
    label "&#347;rodek"
  ]
  node [
    id 81
    label "uzasadnienie"
  ]
  node [
    id 82
    label "osobi&#347;cie"
  ]
  node [
    id 83
    label "bezpo&#347;redni"
  ]
  node [
    id 84
    label "szczery"
  ]
  node [
    id 85
    label "czyj&#347;"
  ]
  node [
    id 86
    label "intymny"
  ]
  node [
    id 87
    label "prywatny"
  ]
  node [
    id 88
    label "personalny"
  ]
  node [
    id 89
    label "w&#322;asny"
  ]
  node [
    id 90
    label "prywatnie"
  ]
  node [
    id 91
    label "emocjonalny"
  ]
  node [
    id 92
    label "record"
  ]
  node [
    id 93
    label "&#347;wiadectwo"
  ]
  node [
    id 94
    label "zapis"
  ]
  node [
    id 95
    label "fascyku&#322;"
  ]
  node [
    id 96
    label "raport&#243;wka"
  ]
  node [
    id 97
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 98
    label "artyku&#322;"
  ]
  node [
    id 99
    label "plik"
  ]
  node [
    id 100
    label "writing"
  ]
  node [
    id 101
    label "utw&#243;r"
  ]
  node [
    id 102
    label "dokumentacja"
  ]
  node [
    id 103
    label "parafa"
  ]
  node [
    id 104
    label "sygnatariusz"
  ]
  node [
    id 105
    label "registratura"
  ]
  node [
    id 106
    label "dost&#281;p"
  ]
  node [
    id 107
    label "inspection"
  ]
  node [
    id 108
    label "duplikat"
  ]
  node [
    id 109
    label "erotyka"
  ]
  node [
    id 110
    label "podniecanie"
  ]
  node [
    id 111
    label "wzw&#243;d"
  ]
  node [
    id 112
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 113
    label "rozmna&#380;anie"
  ]
  node [
    id 114
    label "ontologia"
  ]
  node [
    id 115
    label "fragment"
  ]
  node [
    id 116
    label "po&#380;&#261;danie"
  ]
  node [
    id 117
    label "imisja"
  ]
  node [
    id 118
    label "po&#380;ycie"
  ]
  node [
    id 119
    label "pozycja_misjonarska"
  ]
  node [
    id 120
    label "uroczysto&#347;&#263;"
  ]
  node [
    id 121
    label "podnieci&#263;"
  ]
  node [
    id 122
    label "podnieca&#263;"
  ]
  node [
    id 123
    label "funkcja"
  ]
  node [
    id 124
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 125
    label "czynno&#347;&#263;"
  ]
  node [
    id 126
    label "urzeczywistnienie"
  ]
  node [
    id 127
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 128
    label "gra_wst&#281;pna"
  ]
  node [
    id 129
    label "scena"
  ]
  node [
    id 130
    label "nago&#347;&#263;"
  ]
  node [
    id 131
    label "wydarzenie"
  ]
  node [
    id 132
    label "poj&#281;cie"
  ]
  node [
    id 133
    label "numer"
  ]
  node [
    id 134
    label "ruch_frykcyjny"
  ]
  node [
    id 135
    label "baraszki"
  ]
  node [
    id 136
    label "na_pieska"
  ]
  node [
    id 137
    label "arystotelizm"
  ]
  node [
    id 138
    label "z&#322;&#261;czenie"
  ]
  node [
    id 139
    label "seks"
  ]
  node [
    id 140
    label "zwyczaj"
  ]
  node [
    id 141
    label "podniecenie"
  ]
  node [
    id 142
    label "zrobienie"
  ]
  node [
    id 143
    label "zlegni&#281;cie"
  ]
  node [
    id 144
    label "donoszenie"
  ]
  node [
    id 145
    label "powicie"
  ]
  node [
    id 146
    label "status"
  ]
  node [
    id 147
    label "beginning"
  ]
  node [
    id 148
    label "urodzenie_si&#281;"
  ]
  node [
    id 149
    label "pocz&#261;tek"
  ]
  node [
    id 150
    label "narodzenie"
  ]
  node [
    id 151
    label "porodzenie"
  ]
  node [
    id 152
    label "cz&#322;owiek"
  ]
  node [
    id 153
    label "potomstwo"
  ]
  node [
    id 154
    label "organizm"
  ]
  node [
    id 155
    label "sraluch"
  ]
  node [
    id 156
    label "utulanie"
  ]
  node [
    id 157
    label "pediatra"
  ]
  node [
    id 158
    label "dzieciarnia"
  ]
  node [
    id 159
    label "m&#322;odziak"
  ]
  node [
    id 160
    label "dzieciak"
  ]
  node [
    id 161
    label "utula&#263;"
  ]
  node [
    id 162
    label "potomek"
  ]
  node [
    id 163
    label "entliczek-pentliczek"
  ]
  node [
    id 164
    label "pedofil"
  ]
  node [
    id 165
    label "m&#322;odzik"
  ]
  node [
    id 166
    label "cz&#322;owieczek"
  ]
  node [
    id 167
    label "zwierz&#281;"
  ]
  node [
    id 168
    label "niepe&#322;noletni"
  ]
  node [
    id 169
    label "fledgling"
  ]
  node [
    id 170
    label "utuli&#263;"
  ]
  node [
    id 171
    label "utulenie"
  ]
  node [
    id 172
    label "wypowied&#378;"
  ]
  node [
    id 173
    label "cz&#281;&#347;&#263;_zdania"
  ]
  node [
    id 174
    label "decyzja"
  ]
  node [
    id 175
    label "predykatywno&#347;&#263;"
  ]
  node [
    id 176
    label "specjalny"
  ]
  node [
    id 177
    label "niesprawno&#347;&#263;"
  ]
  node [
    id 178
    label "minuta"
  ]
  node [
    id 179
    label "forma"
  ]
  node [
    id 180
    label "znaczenie"
  ]
  node [
    id 181
    label "kategoria_gramatyczna"
  ]
  node [
    id 182
    label "przys&#322;&#243;wek"
  ]
  node [
    id 183
    label "szczebel"
  ]
  node [
    id 184
    label "element"
  ]
  node [
    id 185
    label "poziom"
  ]
  node [
    id 186
    label "degree"
  ]
  node [
    id 187
    label "podn&#243;&#380;ek"
  ]
  node [
    id 188
    label "rank"
  ]
  node [
    id 189
    label "przymiotnik"
  ]
  node [
    id 190
    label "podzia&#322;"
  ]
  node [
    id 191
    label "ocena"
  ]
  node [
    id 192
    label "kszta&#322;t"
  ]
  node [
    id 193
    label "wschodek"
  ]
  node [
    id 194
    label "miejsce"
  ]
  node [
    id 195
    label "schody"
  ]
  node [
    id 196
    label "gama"
  ]
  node [
    id 197
    label "podstopie&#324;"
  ]
  node [
    id 198
    label "d&#378;wi&#281;k"
  ]
  node [
    id 199
    label "wielko&#347;&#263;"
  ]
  node [
    id 200
    label "jednostka"
  ]
  node [
    id 201
    label "pacjent"
  ]
  node [
    id 202
    label "schorzenie"
  ]
  node [
    id 203
    label "przeznaczenie"
  ]
  node [
    id 204
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 205
    label "happening"
  ]
  node [
    id 206
    label "przyk&#322;ad"
  ]
  node [
    id 207
    label "recognition"
  ]
  node [
    id 208
    label "danie"
  ]
  node [
    id 209
    label "stwierdzenie"
  ]
  node [
    id 210
    label "confession"
  ]
  node [
    id 211
    label "oznajmienie"
  ]
  node [
    id 212
    label "p&#322;acenie"
  ]
  node [
    id 213
    label "sk&#322;adanie"
  ]
  node [
    id 214
    label "service"
  ]
  node [
    id 215
    label "czynienie_dobra"
  ]
  node [
    id 216
    label "informowanie"
  ]
  node [
    id 217
    label "command"
  ]
  node [
    id 218
    label "opowiadanie"
  ]
  node [
    id 219
    label "koszt_rodzajowy"
  ]
  node [
    id 220
    label "pracowanie"
  ]
  node [
    id 221
    label "przekonywanie"
  ]
  node [
    id 222
    label "wyraz"
  ]
  node [
    id 223
    label "us&#322;uga"
  ]
  node [
    id 224
    label "performance"
  ]
  node [
    id 225
    label "zobowi&#261;zanie"
  ]
  node [
    id 226
    label "uzale&#380;nienie_si&#281;"
  ]
  node [
    id 227
    label "uzale&#380;nianie_si&#281;"
  ]
  node [
    id 228
    label "uzale&#380;nianie"
  ]
  node [
    id 229
    label "uzale&#380;nienie"
  ]
  node [
    id 230
    label "si&#281;ga&#263;"
  ]
  node [
    id 231
    label "trwa&#263;"
  ]
  node [
    id 232
    label "obecno&#347;&#263;"
  ]
  node [
    id 233
    label "stan"
  ]
  node [
    id 234
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 235
    label "stand"
  ]
  node [
    id 236
    label "mie&#263;_miejsce"
  ]
  node [
    id 237
    label "chodzi&#263;"
  ]
  node [
    id 238
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 239
    label "equal"
  ]
  node [
    id 240
    label "model"
  ]
  node [
    id 241
    label "potwierdzenie"
  ]
  node [
    id 242
    label "Mickiewicz"
  ]
  node [
    id 243
    label "czas"
  ]
  node [
    id 244
    label "szkolenie"
  ]
  node [
    id 245
    label "przepisa&#263;"
  ]
  node [
    id 246
    label "lesson"
  ]
  node [
    id 247
    label "grupa"
  ]
  node [
    id 248
    label "praktyka"
  ]
  node [
    id 249
    label "metoda"
  ]
  node [
    id 250
    label "niepokalanki"
  ]
  node [
    id 251
    label "kara"
  ]
  node [
    id 252
    label "zda&#263;"
  ]
  node [
    id 253
    label "form"
  ]
  node [
    id 254
    label "kwalifikacje"
  ]
  node [
    id 255
    label "system"
  ]
  node [
    id 256
    label "przepisanie"
  ]
  node [
    id 257
    label "sztuba"
  ]
  node [
    id 258
    label "wiedza"
  ]
  node [
    id 259
    label "stopek"
  ]
  node [
    id 260
    label "school"
  ]
  node [
    id 261
    label "absolwent"
  ]
  node [
    id 262
    label "urszulanki"
  ]
  node [
    id 263
    label "gabinet"
  ]
  node [
    id 264
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 265
    label "ideologia"
  ]
  node [
    id 266
    label "lekcja"
  ]
  node [
    id 267
    label "muzyka"
  ]
  node [
    id 268
    label "podr&#281;cznik"
  ]
  node [
    id 269
    label "zdanie"
  ]
  node [
    id 270
    label "siedziba"
  ]
  node [
    id 271
    label "sekretariat"
  ]
  node [
    id 272
    label "do&#347;wiadczenie"
  ]
  node [
    id 273
    label "tablica"
  ]
  node [
    id 274
    label "teren_szko&#322;y"
  ]
  node [
    id 275
    label "instytucja"
  ]
  node [
    id 276
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 277
    label "skolaryzacja"
  ]
  node [
    id 278
    label "&#322;awa_szkolna"
  ]
  node [
    id 279
    label "klasa"
  ]
  node [
    id 280
    label "warto&#347;ciowy"
  ]
  node [
    id 281
    label "du&#380;y"
  ]
  node [
    id 282
    label "wysoce"
  ]
  node [
    id 283
    label "daleki"
  ]
  node [
    id 284
    label "znaczny"
  ]
  node [
    id 285
    label "wysoko"
  ]
  node [
    id 286
    label "szczytnie"
  ]
  node [
    id 287
    label "wznios&#322;y"
  ]
  node [
    id 288
    label "wyrafinowany"
  ]
  node [
    id 289
    label "z_wysoka"
  ]
  node [
    id 290
    label "chwalebny"
  ]
  node [
    id 291
    label "uprzywilejowany"
  ]
  node [
    id 292
    label "niepo&#347;ledni"
  ]
  node [
    id 293
    label "potakuj&#261;cy"
  ]
  node [
    id 294
    label "potwierdzaj&#261;co"
  ]
  node [
    id 295
    label "twierdz&#261;cy"
  ]
  node [
    id 296
    label "robi&#263;"
  ]
  node [
    id 297
    label "prosecute"
  ]
  node [
    id 298
    label "nauki_o_Ziemi"
  ]
  node [
    id 299
    label "teoria_naukowa"
  ]
  node [
    id 300
    label "dzia&#322;alno&#347;&#263;_statutowa"
  ]
  node [
    id 301
    label "nauki_o_poznaniu"
  ]
  node [
    id 302
    label "nomotetyczny"
  ]
  node [
    id 303
    label "metodologia"
  ]
  node [
    id 304
    label "przem&#243;wienie"
  ]
  node [
    id 305
    label "kultura_duchowa"
  ]
  node [
    id 306
    label "nauki_penalne"
  ]
  node [
    id 307
    label "systematyka"
  ]
  node [
    id 308
    label "inwentyka"
  ]
  node [
    id 309
    label "dziedzina"
  ]
  node [
    id 310
    label "nadawca_spo&#322;eczny"
  ]
  node [
    id 311
    label "miasteczko_rowerowe"
  ]
  node [
    id 312
    label "fotowoltaika"
  ]
  node [
    id 313
    label "porada"
  ]
  node [
    id 314
    label "miasteczko_komunikacyjne"
  ]
  node [
    id 315
    label "proces"
  ]
  node [
    id 316
    label "imagineskopia"
  ]
  node [
    id 317
    label "typologia"
  ]
  node [
    id 318
    label "communicate"
  ]
  node [
    id 319
    label "przesta&#263;"
  ]
  node [
    id 320
    label "end"
  ]
  node [
    id 321
    label "sko&#324;czy&#263;"
  ]
  node [
    id 322
    label "stulecie"
  ]
  node [
    id 323
    label "kalendarz"
  ]
  node [
    id 324
    label "pora_roku"
  ]
  node [
    id 325
    label "cykl_astronomiczny"
  ]
  node [
    id 326
    label "p&#243;&#322;rocze"
  ]
  node [
    id 327
    label "kwarta&#322;"
  ]
  node [
    id 328
    label "kurs"
  ]
  node [
    id 329
    label "jubileusz"
  ]
  node [
    id 330
    label "miesi&#261;c"
  ]
  node [
    id 331
    label "lata"
  ]
  node [
    id 332
    label "martwy_sezon"
  ]
  node [
    id 333
    label "energy"
  ]
  node [
    id 334
    label "bycie"
  ]
  node [
    id 335
    label "zegar_biologiczny"
  ]
  node [
    id 336
    label "okres_noworodkowy"
  ]
  node [
    id 337
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 338
    label "entity"
  ]
  node [
    id 339
    label "prze&#380;ywanie"
  ]
  node [
    id 340
    label "prze&#380;ycie"
  ]
  node [
    id 341
    label "wiek_przej&#347;ciowy"
  ]
  node [
    id 342
    label "wiek_matuzalemowy"
  ]
  node [
    id 343
    label "m&#322;odo&#347;&#263;"
  ]
  node [
    id 344
    label "dzieci&#324;stwo"
  ]
  node [
    id 345
    label "power"
  ]
  node [
    id 346
    label "szwung"
  ]
  node [
    id 347
    label "menopauza"
  ]
  node [
    id 348
    label "umarcie"
  ]
  node [
    id 349
    label "ch&#322;opi&#281;ctwo"
  ]
  node [
    id 350
    label "life"
  ]
  node [
    id 351
    label "doros&#322;o&#347;&#263;"
  ]
  node [
    id 352
    label "&#380;ywy"
  ]
  node [
    id 353
    label "rozw&#243;j"
  ]
  node [
    id 354
    label "po&#322;&#243;g"
  ]
  node [
    id 355
    label "byt"
  ]
  node [
    id 356
    label "przebywanie"
  ]
  node [
    id 357
    label "subsistence"
  ]
  node [
    id 358
    label "koleje_losu"
  ]
  node [
    id 359
    label "raj_utracony"
  ]
  node [
    id 360
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 361
    label "okres_oko&#322;oporodowy"
  ]
  node [
    id 362
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 363
    label "andropauza"
  ]
  node [
    id 364
    label "warunki"
  ]
  node [
    id 365
    label "do&#380;ywanie"
  ]
  node [
    id 366
    label "niemowl&#281;ctwo"
  ]
  node [
    id 367
    label "umieranie"
  ]
  node [
    id 368
    label "wiek_przedprodukcyjny"
  ]
  node [
    id 369
    label "staro&#347;&#263;"
  ]
  node [
    id 370
    label "wiek_nieprodukcyjny"
  ]
  node [
    id 371
    label "&#347;mier&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 11
  ]
  edge [
    source 8
    target 24
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 23
  ]
  edge [
    source 9
    target 24
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 25
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 29
  ]
  edge [
    source 13
    target 30
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 18
  ]
  edge [
    source 15
    target 22
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 15
    target 177
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 25
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 17
    target 188
  ]
  edge [
    source 17
    target 189
  ]
  edge [
    source 17
    target 190
  ]
  edge [
    source 17
    target 191
  ]
  edge [
    source 17
    target 192
  ]
  edge [
    source 17
    target 193
  ]
  edge [
    source 17
    target 194
  ]
  edge [
    source 17
    target 195
  ]
  edge [
    source 17
    target 196
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 24
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 202
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 131
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 24
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 20
    target 221
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 225
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 152
  ]
  edge [
    source 21
    target 226
  ]
  edge [
    source 21
    target 227
  ]
  edge [
    source 21
    target 228
  ]
  edge [
    source 21
    target 229
  ]
  edge [
    source 22
    target 230
  ]
  edge [
    source 22
    target 231
  ]
  edge [
    source 22
    target 232
  ]
  edge [
    source 22
    target 233
  ]
  edge [
    source 22
    target 234
  ]
  edge [
    source 22
    target 235
  ]
  edge [
    source 22
    target 236
  ]
  edge [
    source 22
    target 53
  ]
  edge [
    source 22
    target 237
  ]
  edge [
    source 22
    target 238
  ]
  edge [
    source 22
    target 239
  ]
  edge [
    source 23
    target 152
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 241
  ]
  edge [
    source 24
    target 75
  ]
  edge [
    source 24
    target 142
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 242
  ]
  edge [
    source 25
    target 243
  ]
  edge [
    source 25
    target 244
  ]
  edge [
    source 25
    target 245
  ]
  edge [
    source 25
    target 246
  ]
  edge [
    source 25
    target 247
  ]
  edge [
    source 25
    target 248
  ]
  edge [
    source 25
    target 249
  ]
  edge [
    source 25
    target 250
  ]
  edge [
    source 25
    target 251
  ]
  edge [
    source 25
    target 252
  ]
  edge [
    source 25
    target 253
  ]
  edge [
    source 25
    target 254
  ]
  edge [
    source 25
    target 255
  ]
  edge [
    source 25
    target 256
  ]
  edge [
    source 25
    target 257
  ]
  edge [
    source 25
    target 258
  ]
  edge [
    source 25
    target 259
  ]
  edge [
    source 25
    target 260
  ]
  edge [
    source 25
    target 261
  ]
  edge [
    source 25
    target 262
  ]
  edge [
    source 25
    target 263
  ]
  edge [
    source 25
    target 264
  ]
  edge [
    source 25
    target 265
  ]
  edge [
    source 25
    target 266
  ]
  edge [
    source 25
    target 267
  ]
  edge [
    source 25
    target 268
  ]
  edge [
    source 25
    target 269
  ]
  edge [
    source 25
    target 270
  ]
  edge [
    source 25
    target 271
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 25
    target 272
  ]
  edge [
    source 25
    target 273
  ]
  edge [
    source 25
    target 274
  ]
  edge [
    source 25
    target 275
  ]
  edge [
    source 25
    target 276
  ]
  edge [
    source 25
    target 277
  ]
  edge [
    source 25
    target 278
  ]
  edge [
    source 25
    target 279
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 280
  ]
  edge [
    source 26
    target 281
  ]
  edge [
    source 26
    target 282
  ]
  edge [
    source 26
    target 283
  ]
  edge [
    source 26
    target 284
  ]
  edge [
    source 26
    target 285
  ]
  edge [
    source 26
    target 286
  ]
  edge [
    source 26
    target 287
  ]
  edge [
    source 26
    target 288
  ]
  edge [
    source 26
    target 289
  ]
  edge [
    source 26
    target 290
  ]
  edge [
    source 26
    target 291
  ]
  edge [
    source 26
    target 292
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 293
  ]
  edge [
    source 27
    target 294
  ]
  edge [
    source 27
    target 295
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 296
  ]
  edge [
    source 28
    target 297
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 298
  ]
  edge [
    source 30
    target 299
  ]
  edge [
    source 30
    target 300
  ]
  edge [
    source 30
    target 301
  ]
  edge [
    source 30
    target 302
  ]
  edge [
    source 30
    target 303
  ]
  edge [
    source 30
    target 304
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 30
    target 305
  ]
  edge [
    source 30
    target 306
  ]
  edge [
    source 30
    target 307
  ]
  edge [
    source 30
    target 308
  ]
  edge [
    source 30
    target 309
  ]
  edge [
    source 30
    target 310
  ]
  edge [
    source 30
    target 311
  ]
  edge [
    source 30
    target 312
  ]
  edge [
    source 30
    target 313
  ]
  edge [
    source 30
    target 314
  ]
  edge [
    source 30
    target 315
  ]
  edge [
    source 30
    target 316
  ]
  edge [
    source 30
    target 317
  ]
  edge [
    source 30
    target 278
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 318
  ]
  edge [
    source 32
    target 319
  ]
  edge [
    source 32
    target 320
  ]
  edge [
    source 32
    target 321
  ]
  edge [
    source 32
    target 60
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 322
  ]
  edge [
    source 33
    target 323
  ]
  edge [
    source 33
    target 243
  ]
  edge [
    source 33
    target 324
  ]
  edge [
    source 33
    target 325
  ]
  edge [
    source 33
    target 326
  ]
  edge [
    source 33
    target 247
  ]
  edge [
    source 33
    target 327
  ]
  edge [
    source 33
    target 328
  ]
  edge [
    source 33
    target 329
  ]
  edge [
    source 33
    target 330
  ]
  edge [
    source 33
    target 331
  ]
  edge [
    source 33
    target 332
  ]
  edge [
    source 34
    target 333
  ]
  edge [
    source 34
    target 243
  ]
  edge [
    source 34
    target 334
  ]
  edge [
    source 34
    target 335
  ]
  edge [
    source 34
    target 336
  ]
  edge [
    source 34
    target 337
  ]
  edge [
    source 34
    target 338
  ]
  edge [
    source 34
    target 339
  ]
  edge [
    source 34
    target 340
  ]
  edge [
    source 34
    target 341
  ]
  edge [
    source 34
    target 342
  ]
  edge [
    source 34
    target 343
  ]
  edge [
    source 34
    target 344
  ]
  edge [
    source 34
    target 345
  ]
  edge [
    source 34
    target 346
  ]
  edge [
    source 34
    target 347
  ]
  edge [
    source 34
    target 348
  ]
  edge [
    source 34
    target 349
  ]
  edge [
    source 34
    target 350
  ]
  edge [
    source 34
    target 351
  ]
  edge [
    source 34
    target 352
  ]
  edge [
    source 34
    target 353
  ]
  edge [
    source 34
    target 354
  ]
  edge [
    source 34
    target 355
  ]
  edge [
    source 34
    target 356
  ]
  edge [
    source 34
    target 357
  ]
  edge [
    source 34
    target 358
  ]
  edge [
    source 34
    target 359
  ]
  edge [
    source 34
    target 360
  ]
  edge [
    source 34
    target 361
  ]
  edge [
    source 34
    target 362
  ]
  edge [
    source 34
    target 363
  ]
  edge [
    source 34
    target 364
  ]
  edge [
    source 34
    target 365
  ]
  edge [
    source 34
    target 366
  ]
  edge [
    source 34
    target 367
  ]
  edge [
    source 34
    target 368
  ]
  edge [
    source 34
    target 369
  ]
  edge [
    source 34
    target 370
  ]
  edge [
    source 34
    target 371
  ]
]
