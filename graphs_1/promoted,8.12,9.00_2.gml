graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.087719298245614
  density 0.00919700131385733
  graphCliqueNumber 4
  node [
    id 0
    label "zosta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "bez"
    origin "text"
  ]
  node [
    id 2
    label "ogrzewanie"
    origin "text"
  ]
  node [
    id 3
    label "dro&#380;e&#263;"
    origin "text"
  ]
  node [
    id 4
    label "energia"
    origin "text"
  ]
  node [
    id 5
    label "kiedy"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "sko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 9
    label "program"
    origin "text"
  ]
  node [
    id 10
    label "wiesti"
    origin "text"
  ]
  node [
    id 11
    label "rosyjski"
    origin "text"
  ]
  node [
    id 12
    label "marzn&#261;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "mieszkanie"
    origin "text"
  ]
  node [
    id 14
    label "osobnik"
    origin "text"
  ]
  node [
    id 15
    label "przedstawia&#263;"
    origin "text"
  ]
  node [
    id 16
    label "jako"
    origin "text"
  ]
  node [
    id 17
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 18
    label "ukrai&#324;ski"
    origin "text"
  ]
  node [
    id 19
    label "r&#243;g"
    origin "text"
  ]
  node [
    id 20
    label "kiri&#322;"
    origin "text"
  ]
  node [
    id 21
    label "czubenko"
    origin "text"
  ]
  node [
    id 22
    label "ki&#347;&#263;"
  ]
  node [
    id 23
    label "ro&#347;lina_drzewiasta"
  ]
  node [
    id 24
    label "krzew"
  ]
  node [
    id 25
    label "pi&#380;maczkowate"
  ]
  node [
    id 26
    label "pestkowiec"
  ]
  node [
    id 27
    label "kwiat"
  ]
  node [
    id 28
    label "owoc"
  ]
  node [
    id 29
    label "oliwkowate"
  ]
  node [
    id 30
    label "ro&#347;lina"
  ]
  node [
    id 31
    label "hy&#263;ka"
  ]
  node [
    id 32
    label "lilac"
  ]
  node [
    id 33
    label "delfinidyna"
  ]
  node [
    id 34
    label "podnoszenie"
  ]
  node [
    id 35
    label "zw&#281;glenie"
  ]
  node [
    id 36
    label "roztapianie"
  ]
  node [
    id 37
    label "regulator_pogodowy"
  ]
  node [
    id 38
    label "regulator_pokojowy"
  ]
  node [
    id 39
    label "spieczenie"
  ]
  node [
    id 40
    label "instalacja"
  ]
  node [
    id 41
    label "odpuszczanie"
  ]
  node [
    id 42
    label "wypra&#380;anie"
  ]
  node [
    id 43
    label "spiekanie"
  ]
  node [
    id 44
    label "automatyka_pogodowa"
  ]
  node [
    id 45
    label "wypra&#380;enie"
  ]
  node [
    id 46
    label "heater"
  ]
  node [
    id 47
    label "rozgrzewanie_si&#281;"
  ]
  node [
    id 48
    label "rozdzia&#322;"
  ]
  node [
    id 49
    label "heating"
  ]
  node [
    id 50
    label "ciep&#322;y"
  ]
  node [
    id 51
    label "odwadnianie"
  ]
  node [
    id 52
    label "zw&#281;glanie"
  ]
  node [
    id 53
    label "monetary_value"
  ]
  node [
    id 54
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 55
    label "egzergia"
  ]
  node [
    id 56
    label "kwant_energii"
  ]
  node [
    id 57
    label "szwung"
  ]
  node [
    id 58
    label "energy"
  ]
  node [
    id 59
    label "licznik_energii_elektrycznej"
  ]
  node [
    id 60
    label "cecha"
  ]
  node [
    id 61
    label "emitowanie"
  ]
  node [
    id 62
    label "&#380;ywo&#347;&#263;"
  ]
  node [
    id 63
    label "power"
  ]
  node [
    id 64
    label "zjawisko"
  ]
  node [
    id 65
    label "emitowa&#263;"
  ]
  node [
    id 66
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 67
    label "communicate"
  ]
  node [
    id 68
    label "zako&#324;czy&#263;"
  ]
  node [
    id 69
    label "przesta&#263;"
  ]
  node [
    id 70
    label "end"
  ]
  node [
    id 71
    label "dokona&#263;_&#380;ywota"
  ]
  node [
    id 72
    label "zrobi&#263;"
  ]
  node [
    id 73
    label "remark"
  ]
  node [
    id 74
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 75
    label "u&#380;ywa&#263;"
  ]
  node [
    id 76
    label "okre&#347;la&#263;"
  ]
  node [
    id 77
    label "j&#281;zyk"
  ]
  node [
    id 78
    label "say"
  ]
  node [
    id 79
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 80
    label "formu&#322;owa&#263;"
  ]
  node [
    id 81
    label "talk"
  ]
  node [
    id 82
    label "powiada&#263;"
  ]
  node [
    id 83
    label "informowa&#263;"
  ]
  node [
    id 84
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 85
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 86
    label "wydobywa&#263;"
  ]
  node [
    id 87
    label "express"
  ]
  node [
    id 88
    label "chew_the_fat"
  ]
  node [
    id 89
    label "dysfonia"
  ]
  node [
    id 90
    label "umie&#263;"
  ]
  node [
    id 91
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 92
    label "tell"
  ]
  node [
    id 93
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 94
    label "wyra&#380;a&#263;"
  ]
  node [
    id 95
    label "gaworzy&#263;"
  ]
  node [
    id 96
    label "rozmawia&#263;"
  ]
  node [
    id 97
    label "dziama&#263;"
  ]
  node [
    id 98
    label "prawi&#263;"
  ]
  node [
    id 99
    label "spis"
  ]
  node [
    id 100
    label "odinstalowanie"
  ]
  node [
    id 101
    label "rozpi&#281;to&#347;&#263;"
  ]
  node [
    id 102
    label "za&#322;o&#380;enie"
  ]
  node [
    id 103
    label "podstawa"
  ]
  node [
    id 104
    label "odinstalowywanie"
  ]
  node [
    id 105
    label "instrukcja"
  ]
  node [
    id 106
    label "punkt"
  ]
  node [
    id 107
    label "teleferie"
  ]
  node [
    id 108
    label "wytw&#243;r"
  ]
  node [
    id 109
    label "wielodost&#281;pno&#347;&#263;"
  ]
  node [
    id 110
    label "sekcja_krytyczna"
  ]
  node [
    id 111
    label "prezentowa&#263;"
  ]
  node [
    id 112
    label "blok"
  ]
  node [
    id 113
    label "podprogram"
  ]
  node [
    id 114
    label "tryb"
  ]
  node [
    id 115
    label "dzia&#322;"
  ]
  node [
    id 116
    label "broszura"
  ]
  node [
    id 117
    label "deklaracja"
  ]
  node [
    id 118
    label "kod_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 119
    label "struktura_organizacyjna"
  ]
  node [
    id 120
    label "zaprezentowanie"
  ]
  node [
    id 121
    label "informatyka"
  ]
  node [
    id 122
    label "booklet"
  ]
  node [
    id 123
    label "menu"
  ]
  node [
    id 124
    label "oprogramowanie"
  ]
  node [
    id 125
    label "instalowanie"
  ]
  node [
    id 126
    label "furkacja"
  ]
  node [
    id 127
    label "odinstalowa&#263;"
  ]
  node [
    id 128
    label "instalowa&#263;"
  ]
  node [
    id 129
    label "okno"
  ]
  node [
    id 130
    label "pirat"
  ]
  node [
    id 131
    label "zainstalowanie"
  ]
  node [
    id 132
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 133
    label "ogranicznik_referencyjny"
  ]
  node [
    id 134
    label "zainstalowa&#263;"
  ]
  node [
    id 135
    label "kana&#322;"
  ]
  node [
    id 136
    label "zaprezentowa&#263;"
  ]
  node [
    id 137
    label "interfejs"
  ]
  node [
    id 138
    label "odinstalowywa&#263;"
  ]
  node [
    id 139
    label "folder"
  ]
  node [
    id 140
    label "course_of_study"
  ]
  node [
    id 141
    label "ram&#243;wka"
  ]
  node [
    id 142
    label "prezentowanie"
  ]
  node [
    id 143
    label "oferta"
  ]
  node [
    id 144
    label "po_rosyjsku"
  ]
  node [
    id 145
    label "wielkoruski"
  ]
  node [
    id 146
    label "kacapski"
  ]
  node [
    id 147
    label "Russian"
  ]
  node [
    id 148
    label "rusek"
  ]
  node [
    id 149
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 150
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 151
    label "twardnie&#263;"
  ]
  node [
    id 152
    label "doznawa&#263;"
  ]
  node [
    id 153
    label "freeze"
  ]
  node [
    id 154
    label "ulega&#263;"
  ]
  node [
    id 155
    label "stanie"
  ]
  node [
    id 156
    label "przebywanie"
  ]
  node [
    id 157
    label "panowanie"
  ]
  node [
    id 158
    label "zajmowanie"
  ]
  node [
    id 159
    label "pomieszkanie"
  ]
  node [
    id 160
    label "adjustment"
  ]
  node [
    id 161
    label "modu&#322;_mieszkalny"
  ]
  node [
    id 162
    label "lokal"
  ]
  node [
    id 163
    label "kwadrat"
  ]
  node [
    id 164
    label "animation"
  ]
  node [
    id 165
    label "dom"
  ]
  node [
    id 166
    label "przyswoi&#263;"
  ]
  node [
    id 167
    label "ewoluowanie"
  ]
  node [
    id 168
    label "potencja&#322;_biotyczny"
  ]
  node [
    id 169
    label "przyswajanie"
  ]
  node [
    id 170
    label "obiekt"
  ]
  node [
    id 171
    label "reakcja"
  ]
  node [
    id 172
    label "wyewoluowa&#263;"
  ]
  node [
    id 173
    label "wyewoluowanie"
  ]
  node [
    id 174
    label "ewoluowa&#263;"
  ]
  node [
    id 175
    label "przyswojenie"
  ]
  node [
    id 176
    label "individual"
  ]
  node [
    id 177
    label "czynnik_biotyczny"
  ]
  node [
    id 178
    label "starzenie_si&#281;"
  ]
  node [
    id 179
    label "przyswaja&#263;"
  ]
  node [
    id 180
    label "facet"
  ]
  node [
    id 181
    label "sztuka"
  ]
  node [
    id 182
    label "przedstawienie"
  ]
  node [
    id 183
    label "pokazywa&#263;"
  ]
  node [
    id 184
    label "zapoznawa&#263;"
  ]
  node [
    id 185
    label "typify"
  ]
  node [
    id 186
    label "opisywa&#263;"
  ]
  node [
    id 187
    label "teatr"
  ]
  node [
    id 188
    label "podawa&#263;"
  ]
  node [
    id 189
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 190
    label "demonstrowa&#263;"
  ]
  node [
    id 191
    label "represent"
  ]
  node [
    id 192
    label "ukazywa&#263;"
  ]
  node [
    id 193
    label "attest"
  ]
  node [
    id 194
    label "exhibit"
  ]
  node [
    id 195
    label "stanowi&#263;"
  ]
  node [
    id 196
    label "zg&#322;asza&#263;"
  ]
  node [
    id 197
    label "display"
  ]
  node [
    id 198
    label "cz&#322;owiek"
  ]
  node [
    id 199
    label "ludno&#347;&#263;"
  ]
  node [
    id 200
    label "zwierz&#281;"
  ]
  node [
    id 201
    label "pierogi_ruskie"
  ]
  node [
    id 202
    label "ma&#322;oruski"
  ]
  node [
    id 203
    label "ukrainny"
  ]
  node [
    id 204
    label "po_ukrai&#324;sku"
  ]
  node [
    id 205
    label "ukrai&#324;sko"
  ]
  node [
    id 206
    label "so&#322;omacha"
  ]
  node [
    id 207
    label "sa&#322;o"
  ]
  node [
    id 208
    label "poro&#380;e"
  ]
  node [
    id 209
    label "tworzywo"
  ]
  node [
    id 210
    label "miejsce"
  ]
  node [
    id 211
    label "zawarto&#347;&#263;"
  ]
  node [
    id 212
    label "podanie"
  ]
  node [
    id 213
    label "kraw&#281;d&#378;"
  ]
  node [
    id 214
    label "linia"
  ]
  node [
    id 215
    label "wyrostek"
  ]
  node [
    id 216
    label "naczynie"
  ]
  node [
    id 217
    label "zbieg"
  ]
  node [
    id 218
    label "instrument_d&#281;ty"
  ]
  node [
    id 219
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 220
    label "aut_bramkowy"
  ]
  node [
    id 221
    label "instrument_d&#281;ty_blaszany"
  ]
  node [
    id 222
    label "krzywy"
  ]
  node [
    id 223
    label "R&#243;g"
  ]
  node [
    id 224
    label "Kiri&#322;"
  ]
  node [
    id 225
    label "Czubenko"
  ]
  node [
    id 226
    label "Wiesti"
  ]
  node [
    id 227
    label "tv"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 9
    target 135
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 11
    target 148
  ]
  edge [
    source 11
    target 149
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 226
  ]
  edge [
    source 11
    target 227
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 226
  ]
  edge [
    source 12
    target 227
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 166
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 15
    target 189
  ]
  edge [
    source 15
    target 190
  ]
  edge [
    source 15
    target 191
  ]
  edge [
    source 15
    target 192
  ]
  edge [
    source 15
    target 193
  ]
  edge [
    source 15
    target 194
  ]
  edge [
    source 15
    target 195
  ]
  edge [
    source 15
    target 196
  ]
  edge [
    source 15
    target 197
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 201
  ]
  edge [
    source 18
    target 202
  ]
  edge [
    source 18
    target 203
  ]
  edge [
    source 18
    target 77
  ]
  edge [
    source 18
    target 204
  ]
  edge [
    source 18
    target 205
  ]
  edge [
    source 18
    target 206
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 207
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 19
    target 221
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 222
    target 223
  ]
  edge [
    source 222
    target 224
  ]
  edge [
    source 222
    target 225
  ]
  edge [
    source 223
    target 224
  ]
  edge [
    source 223
    target 225
  ]
  edge [
    source 224
    target 225
  ]
  edge [
    source 226
    target 227
  ]
]
