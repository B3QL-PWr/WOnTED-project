graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "sulej&#243;w"
    origin "text"
  ]
  node [
    id 1
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 2
    label "mazowiecki"
    origin "text"
  ]
  node [
    id 3
    label "jednostka_administracyjna"
  ]
  node [
    id 4
    label "makroregion"
  ]
  node [
    id 5
    label "powiat"
  ]
  node [
    id 6
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 7
    label "mikroregion"
  ]
  node [
    id 8
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 9
    label "pa&#324;stwo"
  ]
  node [
    id 10
    label "polski"
  ]
  node [
    id 11
    label "po_mazowiecku"
  ]
  node [
    id 12
    label "regionalny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
]
