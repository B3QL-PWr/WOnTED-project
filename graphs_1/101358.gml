graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.2
  density 0.08571428571428572
  graphCliqueNumber 3
  node [
    id 0
    label "osvaldo"
    origin "text"
  ]
  node [
    id 1
    label "ardiles"
    origin "text"
  ]
  node [
    id 2
    label "Osvaldo"
  ]
  node [
    id 3
    label "Ardiles"
  ]
  node [
    id 4
    label "Swindon"
  ]
  node [
    id 5
    label "Town"
  ]
  node [
    id 6
    label "Arabia"
  ]
  node [
    id 7
    label "saudyjski"
  ]
  node [
    id 8
    label "Tokyo"
  ]
  node [
    id 9
    label "Verdy"
  ]
  node [
    id 10
    label "John"
  ]
  node [
    id 11
    label "Hustona"
  ]
  node [
    id 12
    label "ucieczka"
  ]
  node [
    id 13
    label "do"
  ]
  node [
    id 14
    label "zwyci&#281;stwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 14
  ]
  edge [
    source 13
    target 14
  ]
]
