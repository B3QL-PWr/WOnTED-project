graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9655172413793103
  density 0.034482758620689655
  graphCliqueNumber 2
  node [
    id 0
    label "cztery"
    origin "text"
  ]
  node [
    id 1
    label "mail"
    origin "text"
  ]
  node [
    id 2
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wys&#322;a&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ponad"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "rzekomy"
    origin "text"
  ]
  node [
    id 7
    label "gwa&#322;t"
    origin "text"
  ]
  node [
    id 8
    label "us&#322;uga_internetowa"
  ]
  node [
    id 9
    label "identyfikator"
  ]
  node [
    id 10
    label "mailowanie"
  ]
  node [
    id 11
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 12
    label "skrzynka_mailowa"
  ]
  node [
    id 13
    label "skrzynka_odbiorcza"
  ]
  node [
    id 14
    label "poczta_elektroniczna"
  ]
  node [
    id 15
    label "konto"
  ]
  node [
    id 16
    label "mailowa&#263;"
  ]
  node [
    id 17
    label "proceed"
  ]
  node [
    id 18
    label "catch"
  ]
  node [
    id 19
    label "pozosta&#263;"
  ]
  node [
    id 20
    label "osta&#263;_si&#281;"
  ]
  node [
    id 21
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 22
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 23
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 24
    label "change"
  ]
  node [
    id 25
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 26
    label "wytworzy&#263;"
  ]
  node [
    id 27
    label "line"
  ]
  node [
    id 28
    label "ship"
  ]
  node [
    id 29
    label "convey"
  ]
  node [
    id 30
    label "przekaza&#263;"
  ]
  node [
    id 31
    label "post"
  ]
  node [
    id 32
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 33
    label "nakaza&#263;"
  ]
  node [
    id 34
    label "stulecie"
  ]
  node [
    id 35
    label "kalendarz"
  ]
  node [
    id 36
    label "czas"
  ]
  node [
    id 37
    label "pora_roku"
  ]
  node [
    id 38
    label "cykl_astronomiczny"
  ]
  node [
    id 39
    label "p&#243;&#322;rocze"
  ]
  node [
    id 40
    label "grupa"
  ]
  node [
    id 41
    label "kwarta&#322;"
  ]
  node [
    id 42
    label "kurs"
  ]
  node [
    id 43
    label "jubileusz"
  ]
  node [
    id 44
    label "miesi&#261;c"
  ]
  node [
    id 45
    label "lata"
  ]
  node [
    id 46
    label "martwy_sezon"
  ]
  node [
    id 47
    label "pozorny"
  ]
  node [
    id 48
    label "czyn_nierz&#261;dny"
  ]
  node [
    id 49
    label "przewaga"
  ]
  node [
    id 50
    label "violation"
  ]
  node [
    id 51
    label "patologia"
  ]
  node [
    id 52
    label "molestowanie_seksualne"
  ]
  node [
    id 53
    label "zamieszanie"
  ]
  node [
    id 54
    label "drastyczny"
  ]
  node [
    id 55
    label "post&#281;pek"
  ]
  node [
    id 56
    label "agresja"
  ]
  node [
    id 57
    label "po&#347;piech"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
]
