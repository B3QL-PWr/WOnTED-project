graph [
  maxDegree 43
  minDegree 1
  meanDegree 1.9682539682539681
  density 0.031746031746031744
  graphCliqueNumber 2
  node [
    id 0
    label "wynik"
    origin "text"
  ]
  node [
    id 1
    label "etap"
    origin "text"
  ]
  node [
    id 2
    label "okre&#347;la&#263;"
    origin "text"
  ]
  node [
    id 3
    label "wymagania"
    origin "text"
  ]
  node [
    id 4
    label "system"
    origin "text"
  ]
  node [
    id 5
    label "informatyczny"
    origin "text"
  ]
  node [
    id 6
    label "typ"
  ]
  node [
    id 7
    label "dzia&#322;anie"
  ]
  node [
    id 8
    label "przyczyna"
  ]
  node [
    id 9
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 10
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 11
    label "zaokr&#261;glenie"
  ]
  node [
    id 12
    label "event"
  ]
  node [
    id 13
    label "rezultat"
  ]
  node [
    id 14
    label "ci&#261;g_zdarze&#324;"
  ]
  node [
    id 15
    label "miejsce"
  ]
  node [
    id 16
    label "odcinek"
  ]
  node [
    id 17
    label "czas"
  ]
  node [
    id 18
    label "signify"
  ]
  node [
    id 19
    label "powodowa&#263;"
  ]
  node [
    id 20
    label "decydowa&#263;"
  ]
  node [
    id 21
    label "style"
  ]
  node [
    id 22
    label "model"
  ]
  node [
    id 23
    label "sk&#322;ad"
  ]
  node [
    id 24
    label "zachowanie"
  ]
  node [
    id 25
    label "podstawa"
  ]
  node [
    id 26
    label "porz&#261;dek"
  ]
  node [
    id 27
    label "Android"
  ]
  node [
    id 28
    label "przyn&#281;ta"
  ]
  node [
    id 29
    label "jednostka_geologiczna"
  ]
  node [
    id 30
    label "metoda"
  ]
  node [
    id 31
    label "podsystem"
  ]
  node [
    id 32
    label "p&#322;&#243;d"
  ]
  node [
    id 33
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 34
    label "s&#261;d"
  ]
  node [
    id 35
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 36
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 37
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 38
    label "j&#261;dro"
  ]
  node [
    id 39
    label "eratem"
  ]
  node [
    id 40
    label "ryba"
  ]
  node [
    id 41
    label "pulpit"
  ]
  node [
    id 42
    label "struktura"
  ]
  node [
    id 43
    label "spos&#243;b"
  ]
  node [
    id 44
    label "oddzia&#322;"
  ]
  node [
    id 45
    label "usenet"
  ]
  node [
    id 46
    label "o&#347;"
  ]
  node [
    id 47
    label "oprogramowanie"
  ]
  node [
    id 48
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 49
    label "poj&#281;cie"
  ]
  node [
    id 50
    label "w&#281;dkarstwo"
  ]
  node [
    id 51
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 52
    label "Leopard"
  ]
  node [
    id 53
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 54
    label "systemik"
  ]
  node [
    id 55
    label "rozprz&#261;c"
  ]
  node [
    id 56
    label "cybernetyk"
  ]
  node [
    id 57
    label "konstelacja"
  ]
  node [
    id 58
    label "doktryna"
  ]
  node [
    id 59
    label "net"
  ]
  node [
    id 60
    label "zbi&#243;r"
  ]
  node [
    id 61
    label "method"
  ]
  node [
    id 62
    label "systemat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
]
