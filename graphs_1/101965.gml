graph [
  maxDegree 71
  minDegree 1
  meanDegree 2.0098522167487687
  density 0.00994976344925133
  graphCliqueNumber 3
  node [
    id 0
    label "znale&#378;&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "ostatnio"
    origin "text"
  ]
  node [
    id 3
    label "powa&#380;ny"
    origin "text"
  ]
  node [
    id 4
    label "koncert"
    origin "text"
  ]
  node [
    id 5
    label "wratislavia"
    origin "text"
  ]
  node [
    id 6
    label "cantans"
    origin "text"
  ]
  node [
    id 7
    label "sta&#263;"
    origin "text"
  ]
  node [
    id 8
    label "gdyby"
    origin "text"
  ]
  node [
    id 9
    label "odcinek"
    origin "text"
  ]
  node [
    id 10
    label "south"
    origin "text"
  ]
  node [
    id 11
    label "park"
    origin "text"
  ]
  node [
    id 12
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 13
    label "meloman"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "wiedza"
    origin "text"
  ]
  node [
    id 16
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 17
    label "czerp"
    origin "text"
  ]
  node [
    id 18
    label "literatura"
    origin "text"
  ]
  node [
    id 19
    label "sprzed"
    origin "text"
  ]
  node [
    id 20
    label "rok"
    origin "text"
  ]
  node [
    id 21
    label "ostatni"
  ]
  node [
    id 22
    label "poprzednio"
  ]
  node [
    id 23
    label "aktualnie"
  ]
  node [
    id 24
    label "gro&#378;ny"
  ]
  node [
    id 25
    label "trudny"
  ]
  node [
    id 26
    label "du&#380;y"
  ]
  node [
    id 27
    label "spowa&#380;nienie"
  ]
  node [
    id 28
    label "prawdziwy"
  ]
  node [
    id 29
    label "powa&#380;nienie"
  ]
  node [
    id 30
    label "powa&#380;nie"
  ]
  node [
    id 31
    label "ci&#281;&#380;ko"
  ]
  node [
    id 32
    label "ci&#281;&#380;ki"
  ]
  node [
    id 33
    label "p&#322;acz"
  ]
  node [
    id 34
    label "wyst&#281;p"
  ]
  node [
    id 35
    label "performance"
  ]
  node [
    id 36
    label "bogactwo"
  ]
  node [
    id 37
    label "mn&#243;stwo"
  ]
  node [
    id 38
    label "utw&#243;r"
  ]
  node [
    id 39
    label "show"
  ]
  node [
    id 40
    label "pokaz"
  ]
  node [
    id 41
    label "zjawisko"
  ]
  node [
    id 42
    label "szale&#324;stwo"
  ]
  node [
    id 43
    label "pozostawa&#263;"
  ]
  node [
    id 44
    label "trwa&#263;"
  ]
  node [
    id 45
    label "by&#263;"
  ]
  node [
    id 46
    label "wystarcza&#263;"
  ]
  node [
    id 47
    label "obowi&#261;zywa&#263;"
  ]
  node [
    id 48
    label "czeka&#263;"
  ]
  node [
    id 49
    label "stand"
  ]
  node [
    id 50
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 51
    label "mieszka&#263;"
  ]
  node [
    id 52
    label "wystarczy&#263;"
  ]
  node [
    id 53
    label "sprawowa&#263;"
  ]
  node [
    id 54
    label "przebywa&#263;"
  ]
  node [
    id 55
    label "kosztowa&#263;"
  ]
  node [
    id 56
    label "undertaking"
  ]
  node [
    id 57
    label "wystawa&#263;"
  ]
  node [
    id 58
    label "base"
  ]
  node [
    id 59
    label "digest"
  ]
  node [
    id 60
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 61
    label "pole"
  ]
  node [
    id 62
    label "part"
  ]
  node [
    id 63
    label "pokwitowanie"
  ]
  node [
    id 64
    label "kawa&#322;ek"
  ]
  node [
    id 65
    label "coupon"
  ]
  node [
    id 66
    label "teren"
  ]
  node [
    id 67
    label "line"
  ]
  node [
    id 68
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 69
    label "epizod"
  ]
  node [
    id 70
    label "moneta"
  ]
  node [
    id 71
    label "fragment"
  ]
  node [
    id 72
    label "teren_przemys&#322;owy"
  ]
  node [
    id 73
    label "miejsce"
  ]
  node [
    id 74
    label "ballpark"
  ]
  node [
    id 75
    label "obszar"
  ]
  node [
    id 76
    label "sprz&#281;t"
  ]
  node [
    id 77
    label "tabor"
  ]
  node [
    id 78
    label "&#321;azienki_Kr&#243;lewskie"
  ]
  node [
    id 79
    label "teren_zielony"
  ]
  node [
    id 80
    label "spowodowa&#263;"
  ]
  node [
    id 81
    label "wyrazi&#263;"
  ]
  node [
    id 82
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 83
    label "przedstawi&#263;"
  ]
  node [
    id 84
    label "testify"
  ]
  node [
    id 85
    label "indicate"
  ]
  node [
    id 86
    label "przeszkoli&#263;"
  ]
  node [
    id 87
    label "udowodni&#263;"
  ]
  node [
    id 88
    label "poinformowa&#263;"
  ]
  node [
    id 89
    label "poda&#263;"
  ]
  node [
    id 90
    label "point"
  ]
  node [
    id 91
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 92
    label "pozwolenie"
  ]
  node [
    id 93
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 94
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 95
    label "wykszta&#322;cenie"
  ]
  node [
    id 96
    label "zaawansowanie"
  ]
  node [
    id 97
    label "&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 98
    label "intelekt"
  ]
  node [
    id 99
    label "cognition"
  ]
  node [
    id 100
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 101
    label "obiekt_naturalny"
  ]
  node [
    id 102
    label "przedmiot"
  ]
  node [
    id 103
    label "Stary_&#346;wiat"
  ]
  node [
    id 104
    label "grupa"
  ]
  node [
    id 105
    label "stw&#243;r"
  ]
  node [
    id 106
    label "biosfera"
  ]
  node [
    id 107
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 108
    label "rzecz"
  ]
  node [
    id 109
    label "magnetosfera"
  ]
  node [
    id 110
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 111
    label "environment"
  ]
  node [
    id 112
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 113
    label "geosfera"
  ]
  node [
    id 114
    label "Nowy_&#346;wiat"
  ]
  node [
    id 115
    label "planeta"
  ]
  node [
    id 116
    label "przejmowa&#263;"
  ]
  node [
    id 117
    label "litosfera"
  ]
  node [
    id 118
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 119
    label "makrokosmos"
  ]
  node [
    id 120
    label "barysfera"
  ]
  node [
    id 121
    label "biota"
  ]
  node [
    id 122
    label "p&#243;&#322;noc"
  ]
  node [
    id 123
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 124
    label "fauna"
  ]
  node [
    id 125
    label "wszechstworzenie"
  ]
  node [
    id 126
    label "geotermia"
  ]
  node [
    id 127
    label "biegun"
  ]
  node [
    id 128
    label "ekosystem"
  ]
  node [
    id 129
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 130
    label "p&#243;&#322;kula"
  ]
  node [
    id 131
    label "atmosfera"
  ]
  node [
    id 132
    label "mikrokosmos"
  ]
  node [
    id 133
    label "class"
  ]
  node [
    id 134
    label "po&#322;udnie"
  ]
  node [
    id 135
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 136
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 137
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 138
    label "przejmowanie"
  ]
  node [
    id 139
    label "przestrze&#324;"
  ]
  node [
    id 140
    label "asymilowanie_si&#281;"
  ]
  node [
    id 141
    label "przej&#261;&#263;"
  ]
  node [
    id 142
    label "ekosfera"
  ]
  node [
    id 143
    label "przyroda"
  ]
  node [
    id 144
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 145
    label "ciemna_materia"
  ]
  node [
    id 146
    label "geoida"
  ]
  node [
    id 147
    label "Wsch&#243;d"
  ]
  node [
    id 148
    label "populace"
  ]
  node [
    id 149
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 150
    label "huczek"
  ]
  node [
    id 151
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 152
    label "Ziemia"
  ]
  node [
    id 153
    label "universe"
  ]
  node [
    id 154
    label "ozonosfera"
  ]
  node [
    id 155
    label "rze&#378;ba"
  ]
  node [
    id 156
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 157
    label "zagranica"
  ]
  node [
    id 158
    label "hydrosfera"
  ]
  node [
    id 159
    label "woda"
  ]
  node [
    id 160
    label "kuchnia"
  ]
  node [
    id 161
    label "przej&#281;cie"
  ]
  node [
    id 162
    label "czarna_dziura"
  ]
  node [
    id 163
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 164
    label "morze"
  ]
  node [
    id 165
    label "dokument"
  ]
  node [
    id 166
    label "amorfizm"
  ]
  node [
    id 167
    label "liryka"
  ]
  node [
    id 168
    label "bibliografia"
  ]
  node [
    id 169
    label "translator"
  ]
  node [
    id 170
    label "dramat"
  ]
  node [
    id 171
    label "pi&#347;miennictwo"
  ]
  node [
    id 172
    label "zoologia_fantastyczna"
  ]
  node [
    id 173
    label "pisarstwo"
  ]
  node [
    id 174
    label "epika"
  ]
  node [
    id 175
    label "sztuka"
  ]
  node [
    id 176
    label "stulecie"
  ]
  node [
    id 177
    label "kalendarz"
  ]
  node [
    id 178
    label "czas"
  ]
  node [
    id 179
    label "pora_roku"
  ]
  node [
    id 180
    label "cykl_astronomiczny"
  ]
  node [
    id 181
    label "p&#243;&#322;rocze"
  ]
  node [
    id 182
    label "kwarta&#322;"
  ]
  node [
    id 183
    label "kurs"
  ]
  node [
    id 184
    label "jubileusz"
  ]
  node [
    id 185
    label "miesi&#261;c"
  ]
  node [
    id 186
    label "lata"
  ]
  node [
    id 187
    label "martwy_sezon"
  ]
  node [
    id 188
    label "Wratislavia"
  ]
  node [
    id 189
    label "Cantans"
  ]
  node [
    id 190
    label "South"
  ]
  node [
    id 191
    label "w&#322;adca"
  ]
  node [
    id 192
    label "pier&#347;cie&#324;"
  ]
  node [
    id 193
    label "zakocha&#263;"
  ]
  node [
    id 194
    label "Flaubert"
  ]
  node [
    id 195
    label "Agata"
  ]
  node [
    id 196
    label "Zubel"
  ]
  node [
    id 197
    label "Sun"
  ]
  node [
    id 198
    label "Wo"
  ]
  node [
    id 199
    label "Sonic"
  ]
  node [
    id 200
    label "Youth"
  ]
  node [
    id 201
    label "Walter"
  ]
  node [
    id 202
    label "Klemmer"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 9
    target 64
  ]
  edge [
    source 9
    target 65
  ]
  edge [
    source 9
    target 66
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 190
  ]
  edge [
    source 11
    target 197
  ]
  edge [
    source 11
    target 198
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 91
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 98
  ]
  edge [
    source 15
    target 99
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 75
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 16
    target 106
  ]
  edge [
    source 16
    target 107
  ]
  edge [
    source 16
    target 108
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 16
    target 123
  ]
  edge [
    source 16
    target 124
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 66
  ]
  edge [
    source 16
    target 41
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 16
    target 146
  ]
  edge [
    source 16
    target 147
  ]
  edge [
    source 16
    target 148
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 176
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 104
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 188
    target 189
  ]
  edge [
    source 191
    target 192
  ]
  edge [
    source 193
    target 194
  ]
  edge [
    source 195
    target 196
  ]
  edge [
    source 197
    target 198
  ]
  edge [
    source 199
    target 200
  ]
  edge [
    source 201
    target 202
  ]
]
