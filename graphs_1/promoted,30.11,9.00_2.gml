graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.029850746268657
  density 0.03075531433740389
  graphCliqueNumber 3
  node [
    id 0
    label "fanatyczny"
    origin "text"
  ]
  node [
    id 1
    label "nazistka"
    origin "text"
  ]
  node [
    id 2
    label "kaza&#263;"
    origin "text"
  ]
  node [
    id 3
    label "skatowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "kole&#380;anka"
    origin "text"
  ]
  node [
    id 5
    label "dlatego"
    origin "text"
  ]
  node [
    id 6
    label "m&#243;wi&#263;"
    origin "text"
  ]
  node [
    id 7
    label "polski"
    origin "text"
  ]
  node [
    id 8
    label "fanatycznie"
  ]
  node [
    id 9
    label "zmusza&#263;"
  ]
  node [
    id 10
    label "nakazywa&#263;"
  ]
  node [
    id 11
    label "wymaga&#263;"
  ]
  node [
    id 12
    label "zmusi&#263;"
  ]
  node [
    id 13
    label "wyg&#322;asza&#263;"
  ]
  node [
    id 14
    label "command"
  ]
  node [
    id 15
    label "order"
  ]
  node [
    id 16
    label "say"
  ]
  node [
    id 17
    label "wyg&#322;osi&#263;"
  ]
  node [
    id 18
    label "nakaza&#263;"
  ]
  node [
    id 19
    label "torment"
  ]
  node [
    id 20
    label "pobi&#263;"
  ]
  node [
    id 21
    label "kuma"
  ]
  node [
    id 22
    label "remark"
  ]
  node [
    id 23
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 24
    label "u&#380;ywa&#263;"
  ]
  node [
    id 25
    label "okre&#347;la&#263;"
  ]
  node [
    id 26
    label "j&#281;zyk"
  ]
  node [
    id 27
    label "kontaktowa&#263;_si&#281;"
  ]
  node [
    id 28
    label "formu&#322;owa&#263;"
  ]
  node [
    id 29
    label "talk"
  ]
  node [
    id 30
    label "powiada&#263;"
  ]
  node [
    id 31
    label "informowa&#263;"
  ]
  node [
    id 32
    label "wydawa&#263;_g&#322;os"
  ]
  node [
    id 33
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 34
    label "wydobywa&#263;"
  ]
  node [
    id 35
    label "express"
  ]
  node [
    id 36
    label "chew_the_fat"
  ]
  node [
    id 37
    label "dysfonia"
  ]
  node [
    id 38
    label "umie&#263;"
  ]
  node [
    id 39
    label "mie&#263;_na_j&#281;zykach"
  ]
  node [
    id 40
    label "tell"
  ]
  node [
    id 41
    label "ozywa&#263;_si&#281;"
  ]
  node [
    id 42
    label "wyra&#380;a&#263;"
  ]
  node [
    id 43
    label "gaworzy&#263;"
  ]
  node [
    id 44
    label "rozmawia&#263;"
  ]
  node [
    id 45
    label "dziama&#263;"
  ]
  node [
    id 46
    label "prawi&#263;"
  ]
  node [
    id 47
    label "lacki"
  ]
  node [
    id 48
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 49
    label "przedmiot"
  ]
  node [
    id 50
    label "sztajer"
  ]
  node [
    id 51
    label "drabant"
  ]
  node [
    id 52
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 53
    label "polak"
  ]
  node [
    id 54
    label "pierogi_ruskie"
  ]
  node [
    id 55
    label "krakowiak"
  ]
  node [
    id 56
    label "Polish"
  ]
  node [
    id 57
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 58
    label "oberek"
  ]
  node [
    id 59
    label "po_polsku"
  ]
  node [
    id 60
    label "mazur"
  ]
  node [
    id 61
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 62
    label "chodzony"
  ]
  node [
    id 63
    label "skoczny"
  ]
  node [
    id 64
    label "ryba_po_grecku"
  ]
  node [
    id 65
    label "goniony"
  ]
  node [
    id 66
    label "polsko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 26
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
]
