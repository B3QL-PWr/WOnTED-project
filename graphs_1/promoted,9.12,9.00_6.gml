graph [
  maxDegree 17
  minDegree 1
  meanDegree 1.9574468085106382
  density 0.0425531914893617
  graphCliqueNumber 2
  node [
    id 0
    label "kolejny"
    origin "text"
  ]
  node [
    id 1
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 2
    label "zapowiada&#263;"
    origin "text"
  ]
  node [
    id 3
    label "cykl"
    origin "text"
  ]
  node [
    id 4
    label "anw"
    origin "text"
  ]
  node [
    id 5
    label "weekend"
    origin "text"
  ]
  node [
    id 6
    label "inny"
  ]
  node [
    id 7
    label "nast&#281;pnie"
  ]
  node [
    id 8
    label "kt&#243;ry&#347;"
  ]
  node [
    id 9
    label "kolejno"
  ]
  node [
    id 10
    label "nastopny"
  ]
  node [
    id 11
    label "dokument"
  ]
  node [
    id 12
    label "towar"
  ]
  node [
    id 13
    label "nag&#322;&#243;wek"
  ]
  node [
    id 14
    label "znak_j&#281;zykowy"
  ]
  node [
    id 15
    label "wyr&#243;b"
  ]
  node [
    id 16
    label "blok"
  ]
  node [
    id 17
    label "line"
  ]
  node [
    id 18
    label "paragraf"
  ]
  node [
    id 19
    label "rodzajnik"
  ]
  node [
    id 20
    label "prawda"
  ]
  node [
    id 21
    label "szkic"
  ]
  node [
    id 22
    label "tekst"
  ]
  node [
    id 23
    label "fragment"
  ]
  node [
    id 24
    label "informowa&#263;"
  ]
  node [
    id 25
    label "og&#322;asza&#263;"
  ]
  node [
    id 26
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 27
    label "bode"
  ]
  node [
    id 28
    label "post"
  ]
  node [
    id 29
    label "ostrzega&#263;"
  ]
  node [
    id 30
    label "harbinger"
  ]
  node [
    id 31
    label "sekwencja"
  ]
  node [
    id 32
    label "czas"
  ]
  node [
    id 33
    label "edycja"
  ]
  node [
    id 34
    label "przebieg"
  ]
  node [
    id 35
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 36
    label "okres"
  ]
  node [
    id 37
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 38
    label "cycle"
  ]
  node [
    id 39
    label "owulacja"
  ]
  node [
    id 40
    label "miesi&#261;czka"
  ]
  node [
    id 41
    label "set"
  ]
  node [
    id 42
    label "niedziela"
  ]
  node [
    id 43
    label "sobota"
  ]
  node [
    id 44
    label "tydzie&#324;"
  ]
  node [
    id 45
    label "uk&#322;ad"
  ]
  node [
    id 46
    label "s&#322;oneczny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 45
    target 46
  ]
]
