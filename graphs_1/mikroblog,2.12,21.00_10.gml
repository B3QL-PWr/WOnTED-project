graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8571428571428572
  density 0.14285714285714285
  graphCliqueNumber 2
  node [
    id 0
    label "codziennie"
    origin "text"
  ]
  node [
    id 1
    label "rano"
    origin "text"
  ]
  node [
    id 2
    label "daily"
  ]
  node [
    id 3
    label "cz&#281;sto"
  ]
  node [
    id 4
    label "codzienny"
  ]
  node [
    id 5
    label "prozaicznie"
  ]
  node [
    id 6
    label "stale"
  ]
  node [
    id 7
    label "regularnie"
  ]
  node [
    id 8
    label "pospolicie"
  ]
  node [
    id 9
    label "wsch&#243;d"
  ]
  node [
    id 10
    label "aurora"
  ]
  node [
    id 11
    label "pora"
  ]
  node [
    id 12
    label "zjawisko"
  ]
  node [
    id 13
    label "dzie&#324;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
]
