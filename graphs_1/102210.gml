graph [
  maxDegree 153
  minDegree 1
  meanDegree 2.0203389830508476
  density 0.0068719013028940385
  graphCliqueNumber 2
  node [
    id 0
    label "zdawa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 3
    label "szcz&#281;&#347;liwy"
    origin "text"
  ]
  node [
    id 4
    label "gdybyby&#263;"
    origin "text"
  ]
  node [
    id 5
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 6
    label "da&#263;"
    origin "text"
  ]
  node [
    id 7
    label "znak"
    origin "text"
  ]
  node [
    id 8
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 9
    label "cz&#322;owiek"
    origin "text"
  ]
  node [
    id 10
    label "ziemia"
    origin "text"
  ]
  node [
    id 11
    label "my&#347;le&#263;"
    origin "text"
  ]
  node [
    id 12
    label "by&#263;"
    origin "text"
  ]
  node [
    id 13
    label "tak"
    origin "text"
  ]
  node [
    id 14
    label "niewiele"
    origin "text"
  ]
  node [
    id 15
    label "byby&#263;"
    origin "text"
  ]
  node [
    id 16
    label "pragn&#261;&#263;"
    origin "text"
  ]
  node [
    id 17
    label "uczyni&#263;"
    origin "text"
  ]
  node [
    id 18
    label "zalicza&#263;"
  ]
  node [
    id 19
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 20
    label "opowiada&#263;"
  ]
  node [
    id 21
    label "render"
  ]
  node [
    id 22
    label "impart"
  ]
  node [
    id 23
    label "oddawa&#263;"
  ]
  node [
    id 24
    label "zostawia&#263;"
  ]
  node [
    id 25
    label "sk&#322;ada&#263;"
  ]
  node [
    id 26
    label "convey"
  ]
  node [
    id 27
    label "powierza&#263;"
  ]
  node [
    id 28
    label "bequeath"
  ]
  node [
    id 29
    label "pomy&#347;lny"
  ]
  node [
    id 30
    label "zadowolony"
  ]
  node [
    id 31
    label "udany"
  ]
  node [
    id 32
    label "szcz&#281;&#347;liwie"
  ]
  node [
    id 33
    label "pogodny"
  ]
  node [
    id 34
    label "dobry"
  ]
  node [
    id 35
    label "pe&#322;ny"
  ]
  node [
    id 36
    label "uprawi&#263;"
  ]
  node [
    id 37
    label "gotowy"
  ]
  node [
    id 38
    label "might"
  ]
  node [
    id 39
    label "dostarczy&#263;"
  ]
  node [
    id 40
    label "obieca&#263;"
  ]
  node [
    id 41
    label "pozwoli&#263;"
  ]
  node [
    id 42
    label "przeznaczy&#263;"
  ]
  node [
    id 43
    label "doda&#263;"
  ]
  node [
    id 44
    label "give"
  ]
  node [
    id 45
    label "wyposa&#380;a&#263;"
  ]
  node [
    id 46
    label "wyrzec_si&#281;"
  ]
  node [
    id 47
    label "supply"
  ]
  node [
    id 48
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 49
    label "zada&#263;"
  ]
  node [
    id 50
    label "odst&#261;pi&#263;"
  ]
  node [
    id 51
    label "feed"
  ]
  node [
    id 52
    label "testify"
  ]
  node [
    id 53
    label "powierzy&#263;"
  ]
  node [
    id 54
    label "przekaza&#263;"
  ]
  node [
    id 55
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 56
    label "zap&#322;aci&#263;"
  ]
  node [
    id 57
    label "dress"
  ]
  node [
    id 58
    label "wyposa&#380;y&#263;"
  ]
  node [
    id 59
    label "udost&#281;pni&#263;"
  ]
  node [
    id 60
    label "sztachn&#261;&#263;"
  ]
  node [
    id 61
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 62
    label "zrobi&#263;"
  ]
  node [
    id 63
    label "przywali&#263;"
  ]
  node [
    id 64
    label "rap"
  ]
  node [
    id 65
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 66
    label "picture"
  ]
  node [
    id 67
    label "postawi&#263;"
  ]
  node [
    id 68
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 69
    label "wytw&#243;r"
  ]
  node [
    id 70
    label "implikowa&#263;"
  ]
  node [
    id 71
    label "stawia&#263;"
  ]
  node [
    id 72
    label "mark"
  ]
  node [
    id 73
    label "kodzik"
  ]
  node [
    id 74
    label "attribute"
  ]
  node [
    id 75
    label "dow&#243;d"
  ]
  node [
    id 76
    label "herb"
  ]
  node [
    id 77
    label "fakt"
  ]
  node [
    id 78
    label "oznakowanie"
  ]
  node [
    id 79
    label "point"
  ]
  node [
    id 80
    label "jako&#347;"
  ]
  node [
    id 81
    label "charakterystyczny"
  ]
  node [
    id 82
    label "ciekawy"
  ]
  node [
    id 83
    label "jako_tako"
  ]
  node [
    id 84
    label "dziwny"
  ]
  node [
    id 85
    label "niez&#322;y"
  ]
  node [
    id 86
    label "przyzwoity"
  ]
  node [
    id 87
    label "asymilowa&#263;"
  ]
  node [
    id 88
    label "wapniak"
  ]
  node [
    id 89
    label "dwun&#243;g"
  ]
  node [
    id 90
    label "polifag"
  ]
  node [
    id 91
    label "wz&#243;r"
  ]
  node [
    id 92
    label "profanum"
  ]
  node [
    id 93
    label "hominid"
  ]
  node [
    id 94
    label "homo_sapiens"
  ]
  node [
    id 95
    label "nasada"
  ]
  node [
    id 96
    label "podw&#322;adny"
  ]
  node [
    id 97
    label "ludzko&#347;&#263;"
  ]
  node [
    id 98
    label "os&#322;abianie"
  ]
  node [
    id 99
    label "mikrokosmos"
  ]
  node [
    id 100
    label "portrecista"
  ]
  node [
    id 101
    label "duch"
  ]
  node [
    id 102
    label "g&#322;owa"
  ]
  node [
    id 103
    label "oddzia&#322;ywanie"
  ]
  node [
    id 104
    label "asymilowanie"
  ]
  node [
    id 105
    label "osoba"
  ]
  node [
    id 106
    label "os&#322;abia&#263;"
  ]
  node [
    id 107
    label "figura"
  ]
  node [
    id 108
    label "Adam"
  ]
  node [
    id 109
    label "senior"
  ]
  node [
    id 110
    label "antropochoria"
  ]
  node [
    id 111
    label "posta&#263;"
  ]
  node [
    id 112
    label "Skandynawia"
  ]
  node [
    id 113
    label "Yorkshire"
  ]
  node [
    id 114
    label "Kaukaz"
  ]
  node [
    id 115
    label "Kaszmir"
  ]
  node [
    id 116
    label "Toskania"
  ]
  node [
    id 117
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 118
    label "&#321;emkowszczyzna"
  ]
  node [
    id 119
    label "obszar"
  ]
  node [
    id 120
    label "Amhara"
  ]
  node [
    id 121
    label "Lombardia"
  ]
  node [
    id 122
    label "Podbeskidzie"
  ]
  node [
    id 123
    label "Kalabria"
  ]
  node [
    id 124
    label "kort"
  ]
  node [
    id 125
    label "Tyrol"
  ]
  node [
    id 126
    label "Pamir"
  ]
  node [
    id 127
    label "Lubelszczyzna"
  ]
  node [
    id 128
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 129
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 130
    label "&#379;ywiecczyzna"
  ]
  node [
    id 131
    label "ryzosfera"
  ]
  node [
    id 132
    label "Europa_Wschodnia"
  ]
  node [
    id 133
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 134
    label "Zabajkale"
  ]
  node [
    id 135
    label "Kaszuby"
  ]
  node [
    id 136
    label "Bo&#347;nia"
  ]
  node [
    id 137
    label "Noworosja"
  ]
  node [
    id 138
    label "Ba&#322;kany"
  ]
  node [
    id 139
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 140
    label "Anglia"
  ]
  node [
    id 141
    label "Kielecczyzna"
  ]
  node [
    id 142
    label "Pomorze_Zachodnie"
  ]
  node [
    id 143
    label "Opolskie"
  ]
  node [
    id 144
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 145
    label "skorupa_ziemska"
  ]
  node [
    id 146
    label "Ko&#322;yma"
  ]
  node [
    id 147
    label "Oksytania"
  ]
  node [
    id 148
    label "Syjon"
  ]
  node [
    id 149
    label "posadzka"
  ]
  node [
    id 150
    label "pa&#324;stwo"
  ]
  node [
    id 151
    label "Kociewie"
  ]
  node [
    id 152
    label "Huculszczyzna"
  ]
  node [
    id 153
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 154
    label "budynek"
  ]
  node [
    id 155
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 156
    label "Bawaria"
  ]
  node [
    id 157
    label "pomieszczenie"
  ]
  node [
    id 158
    label "pr&#243;chnica"
  ]
  node [
    id 159
    label "glinowanie"
  ]
  node [
    id 160
    label "Maghreb"
  ]
  node [
    id 161
    label "Bory_Tucholskie"
  ]
  node [
    id 162
    label "Europa_Zachodnia"
  ]
  node [
    id 163
    label "Kerala"
  ]
  node [
    id 164
    label "Podhale"
  ]
  node [
    id 165
    label "Kabylia"
  ]
  node [
    id 166
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 167
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 168
    label "Ma&#322;opolska"
  ]
  node [
    id 169
    label "Polesie"
  ]
  node [
    id 170
    label "Liguria"
  ]
  node [
    id 171
    label "&#321;&#243;dzkie"
  ]
  node [
    id 172
    label "geosystem"
  ]
  node [
    id 173
    label "Palestyna"
  ]
  node [
    id 174
    label "Bojkowszczyzna"
  ]
  node [
    id 175
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 176
    label "Karaiby"
  ]
  node [
    id 177
    label "S&#261;decczyzna"
  ]
  node [
    id 178
    label "Sand&#380;ak"
  ]
  node [
    id 179
    label "Nadrenia"
  ]
  node [
    id 180
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 181
    label "Zakarpacie"
  ]
  node [
    id 182
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 183
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 184
    label "Zag&#243;rze"
  ]
  node [
    id 185
    label "Andaluzja"
  ]
  node [
    id 186
    label "Turkiestan"
  ]
  node [
    id 187
    label "Naddniestrze"
  ]
  node [
    id 188
    label "Hercegowina"
  ]
  node [
    id 189
    label "p&#322;aszczyzna"
  ]
  node [
    id 190
    label "Opolszczyzna"
  ]
  node [
    id 191
    label "jednostka_administracyjna"
  ]
  node [
    id 192
    label "Lotaryngia"
  ]
  node [
    id 193
    label "Afryka_Wschodnia"
  ]
  node [
    id 194
    label "Szlezwik"
  ]
  node [
    id 195
    label "powierzchnia"
  ]
  node [
    id 196
    label "glinowa&#263;"
  ]
  node [
    id 197
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 198
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 199
    label "podglebie"
  ]
  node [
    id 200
    label "Mazowsze"
  ]
  node [
    id 201
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 202
    label "teren"
  ]
  node [
    id 203
    label "Afryka_Zachodnia"
  ]
  node [
    id 204
    label "czynnik_produkcji"
  ]
  node [
    id 205
    label "Galicja"
  ]
  node [
    id 206
    label "Szkocja"
  ]
  node [
    id 207
    label "Walia"
  ]
  node [
    id 208
    label "Powi&#347;le"
  ]
  node [
    id 209
    label "penetrator"
  ]
  node [
    id 210
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 211
    label "kompleks_sorpcyjny"
  ]
  node [
    id 212
    label "Zamojszczyzna"
  ]
  node [
    id 213
    label "Kujawy"
  ]
  node [
    id 214
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 215
    label "Podlasie"
  ]
  node [
    id 216
    label "Laponia"
  ]
  node [
    id 217
    label "Umbria"
  ]
  node [
    id 218
    label "plantowa&#263;"
  ]
  node [
    id 219
    label "Mezoameryka"
  ]
  node [
    id 220
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 221
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 222
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 223
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 224
    label "Kurdystan"
  ]
  node [
    id 225
    label "Kampania"
  ]
  node [
    id 226
    label "Armagnac"
  ]
  node [
    id 227
    label "Polinezja"
  ]
  node [
    id 228
    label "Warmia"
  ]
  node [
    id 229
    label "Wielkopolska"
  ]
  node [
    id 230
    label "litosfera"
  ]
  node [
    id 231
    label "Bordeaux"
  ]
  node [
    id 232
    label "Lauda"
  ]
  node [
    id 233
    label "Mazury"
  ]
  node [
    id 234
    label "Podkarpacie"
  ]
  node [
    id 235
    label "Oceania"
  ]
  node [
    id 236
    label "Lasko"
  ]
  node [
    id 237
    label "Amazonia"
  ]
  node [
    id 238
    label "pojazd"
  ]
  node [
    id 239
    label "glej"
  ]
  node [
    id 240
    label "martwica"
  ]
  node [
    id 241
    label "zapadnia"
  ]
  node [
    id 242
    label "przestrze&#324;"
  ]
  node [
    id 243
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 244
    label "dotleni&#263;"
  ]
  node [
    id 245
    label "Kurpie"
  ]
  node [
    id 246
    label "Tonkin"
  ]
  node [
    id 247
    label "Azja_Wschodnia"
  ]
  node [
    id 248
    label "Mikronezja"
  ]
  node [
    id 249
    label "Ukraina_Zachodnia"
  ]
  node [
    id 250
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 251
    label "Turyngia"
  ]
  node [
    id 252
    label "Baszkiria"
  ]
  node [
    id 253
    label "Apulia"
  ]
  node [
    id 254
    label "miejsce"
  ]
  node [
    id 255
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 256
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 257
    label "Indochiny"
  ]
  node [
    id 258
    label "Biskupizna"
  ]
  node [
    id 259
    label "Lubuskie"
  ]
  node [
    id 260
    label "domain"
  ]
  node [
    id 261
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 262
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 263
    label "take_care"
  ]
  node [
    id 264
    label "troska&#263;_si&#281;"
  ]
  node [
    id 265
    label "zamierza&#263;"
  ]
  node [
    id 266
    label "os&#261;dza&#263;"
  ]
  node [
    id 267
    label "robi&#263;"
  ]
  node [
    id 268
    label "argue"
  ]
  node [
    id 269
    label "rozpatrywa&#263;"
  ]
  node [
    id 270
    label "deliver"
  ]
  node [
    id 271
    label "si&#281;ga&#263;"
  ]
  node [
    id 272
    label "trwa&#263;"
  ]
  node [
    id 273
    label "obecno&#347;&#263;"
  ]
  node [
    id 274
    label "stan"
  ]
  node [
    id 275
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 276
    label "stand"
  ]
  node [
    id 277
    label "mie&#263;_miejsce"
  ]
  node [
    id 278
    label "uczestniczy&#263;"
  ]
  node [
    id 279
    label "chodzi&#263;"
  ]
  node [
    id 280
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 281
    label "equal"
  ]
  node [
    id 282
    label "nieistotnie"
  ]
  node [
    id 283
    label "nieznaczny"
  ]
  node [
    id 284
    label "pomiernie"
  ]
  node [
    id 285
    label "ma&#322;y"
  ]
  node [
    id 286
    label "desire"
  ]
  node [
    id 287
    label "&#322;akn&#261;&#263;"
  ]
  node [
    id 288
    label "chcie&#263;"
  ]
  node [
    id 289
    label "czu&#263;"
  ]
  node [
    id 290
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 291
    label "t&#281;skni&#263;"
  ]
  node [
    id 292
    label "sprawi&#263;"
  ]
  node [
    id 293
    label "post&#261;pi&#263;"
  ]
  node [
    id 294
    label "zrz&#261;dzi&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 163
  ]
  edge [
    source 10
    target 164
  ]
  edge [
    source 10
    target 165
  ]
  edge [
    source 10
    target 166
  ]
  edge [
    source 10
    target 167
  ]
  edge [
    source 10
    target 168
  ]
  edge [
    source 10
    target 169
  ]
  edge [
    source 10
    target 170
  ]
  edge [
    source 10
    target 171
  ]
  edge [
    source 10
    target 172
  ]
  edge [
    source 10
    target 173
  ]
  edge [
    source 10
    target 174
  ]
  edge [
    source 10
    target 175
  ]
  edge [
    source 10
    target 176
  ]
  edge [
    source 10
    target 177
  ]
  edge [
    source 10
    target 178
  ]
  edge [
    source 10
    target 179
  ]
  edge [
    source 10
    target 180
  ]
  edge [
    source 10
    target 181
  ]
  edge [
    source 10
    target 182
  ]
  edge [
    source 10
    target 183
  ]
  edge [
    source 10
    target 184
  ]
  edge [
    source 10
    target 185
  ]
  edge [
    source 10
    target 186
  ]
  edge [
    source 10
    target 187
  ]
  edge [
    source 10
    target 188
  ]
  edge [
    source 10
    target 189
  ]
  edge [
    source 10
    target 190
  ]
  edge [
    source 10
    target 191
  ]
  edge [
    source 10
    target 192
  ]
  edge [
    source 10
    target 193
  ]
  edge [
    source 10
    target 194
  ]
  edge [
    source 10
    target 195
  ]
  edge [
    source 10
    target 196
  ]
  edge [
    source 10
    target 197
  ]
  edge [
    source 10
    target 198
  ]
  edge [
    source 10
    target 199
  ]
  edge [
    source 10
    target 200
  ]
  edge [
    source 10
    target 201
  ]
  edge [
    source 10
    target 202
  ]
  edge [
    source 10
    target 203
  ]
  edge [
    source 10
    target 204
  ]
  edge [
    source 10
    target 205
  ]
  edge [
    source 10
    target 206
  ]
  edge [
    source 10
    target 207
  ]
  edge [
    source 10
    target 208
  ]
  edge [
    source 10
    target 209
  ]
  edge [
    source 10
    target 210
  ]
  edge [
    source 10
    target 211
  ]
  edge [
    source 10
    target 212
  ]
  edge [
    source 10
    target 213
  ]
  edge [
    source 10
    target 214
  ]
  edge [
    source 10
    target 215
  ]
  edge [
    source 10
    target 216
  ]
  edge [
    source 10
    target 217
  ]
  edge [
    source 10
    target 218
  ]
  edge [
    source 10
    target 219
  ]
  edge [
    source 10
    target 220
  ]
  edge [
    source 10
    target 221
  ]
  edge [
    source 10
    target 222
  ]
  edge [
    source 10
    target 223
  ]
  edge [
    source 10
    target 224
  ]
  edge [
    source 10
    target 225
  ]
  edge [
    source 10
    target 226
  ]
  edge [
    source 10
    target 227
  ]
  edge [
    source 10
    target 228
  ]
  edge [
    source 10
    target 229
  ]
  edge [
    source 10
    target 230
  ]
  edge [
    source 10
    target 231
  ]
  edge [
    source 10
    target 232
  ]
  edge [
    source 10
    target 233
  ]
  edge [
    source 10
    target 234
  ]
  edge [
    source 10
    target 235
  ]
  edge [
    source 10
    target 236
  ]
  edge [
    source 10
    target 237
  ]
  edge [
    source 10
    target 238
  ]
  edge [
    source 10
    target 239
  ]
  edge [
    source 10
    target 240
  ]
  edge [
    source 10
    target 241
  ]
  edge [
    source 10
    target 242
  ]
  edge [
    source 10
    target 243
  ]
  edge [
    source 10
    target 244
  ]
  edge [
    source 10
    target 245
  ]
  edge [
    source 10
    target 246
  ]
  edge [
    source 10
    target 247
  ]
  edge [
    source 10
    target 248
  ]
  edge [
    source 10
    target 249
  ]
  edge [
    source 10
    target 250
  ]
  edge [
    source 10
    target 251
  ]
  edge [
    source 10
    target 252
  ]
  edge [
    source 10
    target 253
  ]
  edge [
    source 10
    target 254
  ]
  edge [
    source 10
    target 255
  ]
  edge [
    source 10
    target 256
  ]
  edge [
    source 10
    target 257
  ]
  edge [
    source 10
    target 258
  ]
  edge [
    source 10
    target 259
  ]
  edge [
    source 10
    target 260
  ]
  edge [
    source 10
    target 261
  ]
  edge [
    source 10
    target 262
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 263
  ]
  edge [
    source 11
    target 264
  ]
  edge [
    source 11
    target 265
  ]
  edge [
    source 11
    target 266
  ]
  edge [
    source 11
    target 267
  ]
  edge [
    source 11
    target 268
  ]
  edge [
    source 11
    target 269
  ]
  edge [
    source 11
    target 270
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 271
  ]
  edge [
    source 12
    target 272
  ]
  edge [
    source 12
    target 273
  ]
  edge [
    source 12
    target 274
  ]
  edge [
    source 12
    target 275
  ]
  edge [
    source 12
    target 276
  ]
  edge [
    source 12
    target 277
  ]
  edge [
    source 12
    target 278
  ]
  edge [
    source 12
    target 279
  ]
  edge [
    source 12
    target 280
  ]
  edge [
    source 12
    target 281
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 15
  ]
  edge [
    source 14
    target 282
  ]
  edge [
    source 14
    target 283
  ]
  edge [
    source 14
    target 284
  ]
  edge [
    source 14
    target 285
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 286
  ]
  edge [
    source 16
    target 287
  ]
  edge [
    source 16
    target 288
  ]
  edge [
    source 16
    target 289
  ]
  edge [
    source 16
    target 290
  ]
  edge [
    source 16
    target 291
  ]
  edge [
    source 17
    target 292
  ]
  edge [
    source 17
    target 293
  ]
  edge [
    source 17
    target 294
  ]
  edge [
    source 17
    target 62
  ]
]
