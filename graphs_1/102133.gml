graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.184971098265896
  density 0.012703320338755208
  graphCliqueNumber 6
  node [
    id 0
    label "wygrana"
    origin "text"
  ]
  node [
    id 1
    label "loteria"
    origin "text"
  ]
  node [
    id 2
    label "rzeczywi&#347;cie"
    origin "text"
  ]
  node [
    id 3
    label "odmieni&#263;by"
    origin "text"
  ]
  node [
    id 4
    label "nasz"
    origin "text"
  ]
  node [
    id 5
    label "los"
    origin "text"
  ]
  node [
    id 6
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 7
    label "mebel"
    origin "text"
  ]
  node [
    id 8
    label "rozrywka"
    origin "text"
  ]
  node [
    id 9
    label "lub"
    origin "text"
  ]
  node [
    id 10
    label "got&#243;wka"
    origin "text"
  ]
  node [
    id 11
    label "m&#243;cby"
    origin "text"
  ]
  node [
    id 12
    label "podnie&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "poziom"
    origin "text"
  ]
  node [
    id 14
    label "odczuwa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "przez"
    origin "text"
  ]
  node [
    id 16
    label "nasa"
    origin "text"
  ]
  node [
    id 17
    label "szcz&#281;&#347;cie"
    origin "text"
  ]
  node [
    id 18
    label "raczej"
    origin "text"
  ]
  node [
    id 19
    label "ale"
    origin "text"
  ]
  node [
    id 20
    label "inny"
    origin "text"
  ]
  node [
    id 21
    label "prze&#380;ycie"
    origin "text"
  ]
  node [
    id 22
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 23
    label "uszcz&#281;&#347;liwi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 25
    label "raport"
    origin "text"
  ]
  node [
    id 26
    label "opublikowa&#263;"
    origin "text"
  ]
  node [
    id 27
    label "przegl&#261;d"
    origin "text"
  ]
  node [
    id 28
    label "journal"
    origin "text"
  ]
  node [
    id 29
    label "personality"
    origin "text"
  ]
  node [
    id 30
    label "anda"
    origin "text"
  ]
  node [
    id 31
    label "social"
    origin "text"
  ]
  node [
    id 32
    label "psychology"
    origin "text"
  ]
  node [
    id 33
    label "puchar"
  ]
  node [
    id 34
    label "korzy&#347;&#263;"
  ]
  node [
    id 35
    label "sukces"
  ]
  node [
    id 36
    label "przedmiot"
  ]
  node [
    id 37
    label "conquest"
  ]
  node [
    id 38
    label "gra_losowa"
  ]
  node [
    id 39
    label "realny"
  ]
  node [
    id 40
    label "naprawd&#281;"
  ]
  node [
    id 41
    label "czyj&#347;"
  ]
  node [
    id 42
    label "si&#322;a"
  ]
  node [
    id 43
    label "przymus"
  ]
  node [
    id 44
    label "rzuci&#263;"
  ]
  node [
    id 45
    label "hazard"
  ]
  node [
    id 46
    label "destiny"
  ]
  node [
    id 47
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 48
    label "bilet"
  ]
  node [
    id 49
    label "przebieg_&#380;ycia"
  ]
  node [
    id 50
    label "&#380;ycie"
  ]
  node [
    id 51
    label "rzucenie"
  ]
  node [
    id 52
    label "du&#380;y"
  ]
  node [
    id 53
    label "cz&#281;sto"
  ]
  node [
    id 54
    label "bardzo"
  ]
  node [
    id 55
    label "mocno"
  ]
  node [
    id 56
    label "wiela"
  ]
  node [
    id 57
    label "nadstawa"
  ]
  node [
    id 58
    label "umeblowanie"
  ]
  node [
    id 59
    label "obudowywanie"
  ]
  node [
    id 60
    label "obudowywa&#263;"
  ]
  node [
    id 61
    label "przeszklenie"
  ]
  node [
    id 62
    label "obudowanie"
  ]
  node [
    id 63
    label "sprz&#281;t"
  ]
  node [
    id 64
    label "element_wyposa&#380;enia"
  ]
  node [
    id 65
    label "obudowa&#263;"
  ]
  node [
    id 66
    label "ramiak"
  ]
  node [
    id 67
    label "gzyms"
  ]
  node [
    id 68
    label "czasoumilacz"
  ]
  node [
    id 69
    label "game"
  ]
  node [
    id 70
    label "odpoczynek"
  ]
  node [
    id 71
    label "pieni&#261;dze"
  ]
  node [
    id 72
    label "brz&#281;cz&#261;ca_moneta"
  ]
  node [
    id 73
    label "money"
  ]
  node [
    id 74
    label "float"
  ]
  node [
    id 75
    label "przybli&#380;y&#263;"
  ]
  node [
    id 76
    label "heft"
  ]
  node [
    id 77
    label "za&#322;apa&#263;"
  ]
  node [
    id 78
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 79
    label "better"
  ]
  node [
    id 80
    label "pochwali&#263;"
  ]
  node [
    id 81
    label "ascend"
  ]
  node [
    id 82
    label "surface"
  ]
  node [
    id 83
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 84
    label "zacz&#261;&#263;"
  ]
  node [
    id 85
    label "sorb"
  ]
  node [
    id 86
    label "os&#322;awi&#263;"
  ]
  node [
    id 87
    label "zmieni&#263;"
  ]
  node [
    id 88
    label "odbudowa&#263;"
  ]
  node [
    id 89
    label "ulepszy&#263;"
  ]
  node [
    id 90
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 91
    label "policzy&#263;"
  ]
  node [
    id 92
    label "powi&#281;kszy&#263;"
  ]
  node [
    id 93
    label "przemie&#347;ci&#263;"
  ]
  node [
    id 94
    label "pom&#243;c"
  ]
  node [
    id 95
    label "allude"
  ]
  node [
    id 96
    label "laud"
  ]
  node [
    id 97
    label "resume"
  ]
  node [
    id 98
    label "raise"
  ]
  node [
    id 99
    label "wysoko&#347;&#263;"
  ]
  node [
    id 100
    label "faza"
  ]
  node [
    id 101
    label "szczebel"
  ]
  node [
    id 102
    label "po&#322;o&#380;enie"
  ]
  node [
    id 103
    label "kierunek"
  ]
  node [
    id 104
    label "wyk&#322;adnik"
  ]
  node [
    id 105
    label "budynek"
  ]
  node [
    id 106
    label "punkt_widzenia"
  ]
  node [
    id 107
    label "jako&#347;&#263;"
  ]
  node [
    id 108
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 109
    label "ranga"
  ]
  node [
    id 110
    label "p&#322;aszczyzna"
  ]
  node [
    id 111
    label "feel"
  ]
  node [
    id 112
    label "widzie&#263;"
  ]
  node [
    id 113
    label "uczuwa&#263;"
  ]
  node [
    id 114
    label "notice"
  ]
  node [
    id 115
    label "konsekwencja"
  ]
  node [
    id 116
    label "smell"
  ]
  node [
    id 117
    label "postrzega&#263;"
  ]
  node [
    id 118
    label "doznawa&#263;"
  ]
  node [
    id 119
    label "wra&#380;enie"
  ]
  node [
    id 120
    label "przeznaczenie"
  ]
  node [
    id 121
    label "dobrodziejstwo"
  ]
  node [
    id 122
    label "dobro"
  ]
  node [
    id 123
    label "przypadek"
  ]
  node [
    id 124
    label "piwo"
  ]
  node [
    id 125
    label "kolejny"
  ]
  node [
    id 126
    label "inaczej"
  ]
  node [
    id 127
    label "r&#243;&#380;ny"
  ]
  node [
    id 128
    label "inszy"
  ]
  node [
    id 129
    label "osobno"
  ]
  node [
    id 130
    label "doznanie"
  ]
  node [
    id 131
    label "survival"
  ]
  node [
    id 132
    label "przetrwanie"
  ]
  node [
    id 133
    label "poradzenie_sobie"
  ]
  node [
    id 134
    label "przej&#347;cie"
  ]
  node [
    id 135
    label "by&#263;"
  ]
  node [
    id 136
    label "uprawi&#263;"
  ]
  node [
    id 137
    label "gotowy"
  ]
  node [
    id 138
    label "might"
  ]
  node [
    id 139
    label "wzbudzi&#263;"
  ]
  node [
    id 140
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 141
    label "rise"
  ]
  node [
    id 142
    label "appear"
  ]
  node [
    id 143
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 144
    label "raport_Beveridge'a"
  ]
  node [
    id 145
    label "relacja"
  ]
  node [
    id 146
    label "raport_Fischlera"
  ]
  node [
    id 147
    label "statement"
  ]
  node [
    id 148
    label "upubliczni&#263;"
  ]
  node [
    id 149
    label "wydawnictwo"
  ]
  node [
    id 150
    label "wprowadzi&#263;"
  ]
  node [
    id 151
    label "picture"
  ]
  node [
    id 152
    label "impreza"
  ]
  node [
    id 153
    label "kontrola"
  ]
  node [
    id 154
    label "inspection"
  ]
  node [
    id 155
    label "Journal"
  ]
  node [
    id 156
    label "of"
  ]
  node [
    id 157
    label "Personality"
  ]
  node [
    id 158
    label "Anda"
  ]
  node [
    id 159
    label "Social"
  ]
  node [
    id 160
    label "Psychology"
  ]
  node [
    id 161
    label "Leaf"
  ]
  node [
    id 162
    label "van"
  ]
  node [
    id 163
    label "Boven"
  ]
  node [
    id 164
    label "Thomas"
  ]
  node [
    id 165
    label "Gilovich"
  ]
  node [
    id 166
    label "uniwersytet"
  ]
  node [
    id 167
    label "Colorado"
  ]
  node [
    id 168
    label "wyspa"
  ]
  node [
    id 169
    label "Boulder"
  ]
  node [
    id 170
    label "Cornell"
  ]
  node [
    id 171
    label "Mount"
  ]
  node [
    id 172
    label "Everest"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 19
  ]
  edge [
    source 8
    target 20
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 25
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 12
    target 81
  ]
  edge [
    source 12
    target 82
  ]
  edge [
    source 12
    target 83
  ]
  edge [
    source 12
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 28
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 101
  ]
  edge [
    source 13
    target 102
  ]
  edge [
    source 13
    target 103
  ]
  edge [
    source 13
    target 104
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 111
  ]
  edge [
    source 14
    target 112
  ]
  edge [
    source 14
    target 113
  ]
  edge [
    source 14
    target 114
  ]
  edge [
    source 14
    target 115
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 22
  ]
  edge [
    source 16
    target 23
  ]
  edge [
    source 17
    target 119
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 20
    target 127
  ]
  edge [
    source 20
    target 128
  ]
  edge [
    source 20
    target 129
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 119
  ]
  edge [
    source 21
    target 130
  ]
  edge [
    source 21
    target 131
  ]
  edge [
    source 21
    target 132
  ]
  edge [
    source 21
    target 133
  ]
  edge [
    source 21
    target 134
  ]
  edge [
    source 22
    target 135
  ]
  edge [
    source 22
    target 136
  ]
  edge [
    source 22
    target 137
  ]
  edge [
    source 22
    target 138
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 140
  ]
  edge [
    source 24
    target 141
  ]
  edge [
    source 24
    target 142
  ]
  edge [
    source 24
    target 143
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 144
  ]
  edge [
    source 25
    target 145
  ]
  edge [
    source 25
    target 146
  ]
  edge [
    source 25
    target 147
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 148
  ]
  edge [
    source 26
    target 149
  ]
  edge [
    source 26
    target 150
  ]
  edge [
    source 26
    target 151
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 152
  ]
  edge [
    source 27
    target 153
  ]
  edge [
    source 27
    target 154
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 155
    target 156
  ]
  edge [
    source 155
    target 157
  ]
  edge [
    source 155
    target 158
  ]
  edge [
    source 155
    target 159
  ]
  edge [
    source 155
    target 160
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 156
    target 158
  ]
  edge [
    source 156
    target 159
  ]
  edge [
    source 156
    target 160
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 157
    target 159
  ]
  edge [
    source 157
    target 160
  ]
  edge [
    source 158
    target 159
  ]
  edge [
    source 158
    target 160
  ]
  edge [
    source 159
    target 160
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 161
    target 163
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 164
    target 165
  ]
  edge [
    source 166
    target 167
  ]
  edge [
    source 166
    target 168
  ]
  edge [
    source 166
    target 169
  ]
  edge [
    source 166
    target 170
  ]
  edge [
    source 167
    target 168
  ]
  edge [
    source 167
    target 169
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 168
    target 170
  ]
  edge [
    source 171
    target 172
  ]
]
