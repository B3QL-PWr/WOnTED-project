graph [
  maxDegree 46
  minDegree 1
  meanDegree 2.044198895027624
  density 0.011356660527931247
  graphCliqueNumber 2
  node [
    id 0
    label "gazeta"
    origin "text"
  ]
  node [
    id 1
    label "wyborczy"
    origin "text"
  ]
  node [
    id 2
    label "tekst"
    origin "text"
  ]
  node [
    id 3
    label "polski"
    origin "text"
  ]
  node [
    id 4
    label "bootlegowym"
    origin "text"
  ]
  node [
    id 5
    label "wideo"
    origin "text"
  ]
  node [
    id 6
    label "koncert"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "ten"
    origin "text"
  ]
  node [
    id 9
    label "historia"
    origin "text"
  ]
  node [
    id 10
    label "wszystko"
    origin "text"
  ]
  node [
    id 11
    label "ekscytowa&#263;"
    origin "text"
  ]
  node [
    id 12
    label "nowa"
    origin "text"
  ]
  node [
    id 13
    label "kultura"
    origin "text"
  ]
  node [
    id 14
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 15
    label "wyra&#378;nie"
    origin "text"
  ]
  node [
    id 16
    label "wreszcie"
    origin "text"
  ]
  node [
    id 17
    label "si&#281;"
    origin "text"
  ]
  node [
    id 18
    label "otwiera&#263;"
    origin "text"
  ]
  node [
    id 19
    label "prasa"
  ]
  node [
    id 20
    label "redakcja"
  ]
  node [
    id 21
    label "tytu&#322;"
  ]
  node [
    id 22
    label "rozk&#322;ad&#243;wka"
  ]
  node [
    id 23
    label "czasopismo"
  ]
  node [
    id 24
    label "pisa&#263;"
  ]
  node [
    id 25
    label "odmianka"
  ]
  node [
    id 26
    label "opu&#347;ci&#263;"
  ]
  node [
    id 27
    label "wypowied&#378;"
  ]
  node [
    id 28
    label "wytw&#243;r"
  ]
  node [
    id 29
    label "koniektura"
  ]
  node [
    id 30
    label "preparacja"
  ]
  node [
    id 31
    label "ekscerpcja"
  ]
  node [
    id 32
    label "j&#281;zykowo"
  ]
  node [
    id 33
    label "obelga"
  ]
  node [
    id 34
    label "dzie&#322;o"
  ]
  node [
    id 35
    label "pomini&#281;cie"
  ]
  node [
    id 36
    label "lacki"
  ]
  node [
    id 37
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 38
    label "przedmiot"
  ]
  node [
    id 39
    label "sztajer"
  ]
  node [
    id 40
    label "drabant"
  ]
  node [
    id 41
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 42
    label "polak"
  ]
  node [
    id 43
    label "pierogi_ruskie"
  ]
  node [
    id 44
    label "krakowiak"
  ]
  node [
    id 45
    label "Polish"
  ]
  node [
    id 46
    label "j&#281;zyk"
  ]
  node [
    id 47
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 48
    label "oberek"
  ]
  node [
    id 49
    label "po_polsku"
  ]
  node [
    id 50
    label "mazur"
  ]
  node [
    id 51
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 52
    label "chodzony"
  ]
  node [
    id 53
    label "skoczny"
  ]
  node [
    id 54
    label "ryba_po_grecku"
  ]
  node [
    id 55
    label "goniony"
  ]
  node [
    id 56
    label "polsko"
  ]
  node [
    id 57
    label "odtwarzacz"
  ]
  node [
    id 58
    label "g&#322;owica_elektromagnetyczna"
  ]
  node [
    id 59
    label "film"
  ]
  node [
    id 60
    label "technika"
  ]
  node [
    id 61
    label "wideokaseta"
  ]
  node [
    id 62
    label "p&#322;acz"
  ]
  node [
    id 63
    label "wyst&#281;p"
  ]
  node [
    id 64
    label "performance"
  ]
  node [
    id 65
    label "bogactwo"
  ]
  node [
    id 66
    label "mn&#243;stwo"
  ]
  node [
    id 67
    label "utw&#243;r"
  ]
  node [
    id 68
    label "show"
  ]
  node [
    id 69
    label "pokaz"
  ]
  node [
    id 70
    label "zjawisko"
  ]
  node [
    id 71
    label "szale&#324;stwo"
  ]
  node [
    id 72
    label "si&#281;ga&#263;"
  ]
  node [
    id 73
    label "trwa&#263;"
  ]
  node [
    id 74
    label "obecno&#347;&#263;"
  ]
  node [
    id 75
    label "stan"
  ]
  node [
    id 76
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 77
    label "stand"
  ]
  node [
    id 78
    label "mie&#263;_miejsce"
  ]
  node [
    id 79
    label "uczestniczy&#263;"
  ]
  node [
    id 80
    label "chodzi&#263;"
  ]
  node [
    id 81
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 82
    label "equal"
  ]
  node [
    id 83
    label "okre&#347;lony"
  ]
  node [
    id 84
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 85
    label "report"
  ]
  node [
    id 86
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 87
    label "neografia"
  ]
  node [
    id 88
    label "papirologia"
  ]
  node [
    id 89
    label "historia_gospodarcza"
  ]
  node [
    id 90
    label "przebiec"
  ]
  node [
    id 91
    label "hista"
  ]
  node [
    id 92
    label "nauka_humanistyczna"
  ]
  node [
    id 93
    label "filigranistyka"
  ]
  node [
    id 94
    label "dyplomatyka"
  ]
  node [
    id 95
    label "annalistyka"
  ]
  node [
    id 96
    label "historyka"
  ]
  node [
    id 97
    label "heraldyka"
  ]
  node [
    id 98
    label "fabu&#322;a"
  ]
  node [
    id 99
    label "muzealnictwo"
  ]
  node [
    id 100
    label "varsavianistyka"
  ]
  node [
    id 101
    label "prezentyzm"
  ]
  node [
    id 102
    label "mediewistyka"
  ]
  node [
    id 103
    label "przebiegni&#281;cie"
  ]
  node [
    id 104
    label "charakter"
  ]
  node [
    id 105
    label "paleografia"
  ]
  node [
    id 106
    label "genealogia"
  ]
  node [
    id 107
    label "czynno&#347;&#263;"
  ]
  node [
    id 108
    label "prozopografia"
  ]
  node [
    id 109
    label "motyw"
  ]
  node [
    id 110
    label "nautologia"
  ]
  node [
    id 111
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 112
    label "epoka"
  ]
  node [
    id 113
    label "numizmatyka"
  ]
  node [
    id 114
    label "ruralistyka"
  ]
  node [
    id 115
    label "epigrafika"
  ]
  node [
    id 116
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 117
    label "historiografia"
  ]
  node [
    id 118
    label "bizantynistyka"
  ]
  node [
    id 119
    label "weksylologia"
  ]
  node [
    id 120
    label "kierunek"
  ]
  node [
    id 121
    label "ikonografia"
  ]
  node [
    id 122
    label "chronologia"
  ]
  node [
    id 123
    label "archiwistyka"
  ]
  node [
    id 124
    label "sfragistyka"
  ]
  node [
    id 125
    label "zabytkoznawstwo"
  ]
  node [
    id 126
    label "historia_sztuki"
  ]
  node [
    id 127
    label "lock"
  ]
  node [
    id 128
    label "absolut"
  ]
  node [
    id 129
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 130
    label "porusza&#263;"
  ]
  node [
    id 131
    label "revolutionize"
  ]
  node [
    id 132
    label "gwiazda"
  ]
  node [
    id 133
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 134
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 135
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 136
    label "Wsch&#243;d"
  ]
  node [
    id 137
    label "rzecz"
  ]
  node [
    id 138
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 139
    label "sztuka"
  ]
  node [
    id 140
    label "religia"
  ]
  node [
    id 141
    label "przejmowa&#263;"
  ]
  node [
    id 142
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 143
    label "makrokosmos"
  ]
  node [
    id 144
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 145
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 146
    label "praca_rolnicza"
  ]
  node [
    id 147
    label "tradycja"
  ]
  node [
    id 148
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 149
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 150
    label "przejmowanie"
  ]
  node [
    id 151
    label "cecha"
  ]
  node [
    id 152
    label "asymilowanie_si&#281;"
  ]
  node [
    id 153
    label "przej&#261;&#263;"
  ]
  node [
    id 154
    label "hodowla"
  ]
  node [
    id 155
    label "brzoskwiniarnia"
  ]
  node [
    id 156
    label "populace"
  ]
  node [
    id 157
    label "konwencja"
  ]
  node [
    id 158
    label "propriety"
  ]
  node [
    id 159
    label "jako&#347;&#263;"
  ]
  node [
    id 160
    label "kuchnia"
  ]
  node [
    id 161
    label "zwyczaj"
  ]
  node [
    id 162
    label "przej&#281;cie"
  ]
  node [
    id 163
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 164
    label "zdecydowanie"
  ]
  node [
    id 165
    label "distinctly"
  ]
  node [
    id 166
    label "wyra&#378;ny"
  ]
  node [
    id 167
    label "zauwa&#380;alnie"
  ]
  node [
    id 168
    label "nieneutralnie"
  ]
  node [
    id 169
    label "w&#380;dy"
  ]
  node [
    id 170
    label "cia&#322;o"
  ]
  node [
    id 171
    label "begin"
  ]
  node [
    id 172
    label "train"
  ]
  node [
    id 173
    label "uruchamia&#263;"
  ]
  node [
    id 174
    label "rozk&#322;ada&#263;"
  ]
  node [
    id 175
    label "robi&#263;"
  ]
  node [
    id 176
    label "zaczyna&#263;"
  ]
  node [
    id 177
    label "unboxing"
  ]
  node [
    id 178
    label "przecina&#263;"
  ]
  node [
    id 179
    label "powodowa&#263;"
  ]
  node [
    id 180
    label "udost&#281;pnia&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 27
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 10
    target 129
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 132
  ]
  edge [
    source 12
    target 133
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 38
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
]
