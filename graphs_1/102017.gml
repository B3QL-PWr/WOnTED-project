graph [
  maxDegree 157
  minDegree 1
  meanDegree 2.332965821389195
  density 0.0025750174629019813
  graphCliqueNumber 6
  node [
    id 0
    label "zamieszanie"
    origin "text"
  ]
  node [
    id 1
    label "zwi&#261;za&#263;"
    origin "text"
  ]
  node [
    id 2
    label "rzekomy"
    origin "text"
  ]
  node [
    id 3
    label "koniec"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 5
    label "rok"
    origin "text"
  ]
  node [
    id 6
    label "wzi&#261;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "niew&#322;a&#347;ciwy"
    origin "text"
  ]
  node [
    id 9
    label "zrozumienie"
    origin "text"
  ]
  node [
    id 10
    label "wiedza"
    origin "text"
  ]
  node [
    id 11
    label "pochodzi&#263;"
    origin "text"
  ]
  node [
    id 12
    label "kalendarz"
    origin "text"
  ]
  node [
    id 13
    label "maj"
    origin "text"
  ]
  node [
    id 14
    label "sprawa"
    origin "text"
  ]
  node [
    id 15
    label "precesja"
    origin "text"
  ]
  node [
    id 16
    label "ziemia"
    origin "text"
  ]
  node [
    id 17
    label "tak"
    origin "text"
  ]
  node [
    id 18
    label "zwa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "punkt"
    origin "text"
  ]
  node [
    id 20
    label "baran"
    origin "text"
  ]
  node [
    id 21
    label "inaczej"
    origin "text"
  ]
  node [
    id 22
    label "r&#243;wnonoc"
    origin "text"
  ]
  node [
    id 23
    label "przesuwa&#263;"
    origin "text"
  ]
  node [
    id 24
    label "w&#281;drowa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "przed"
    origin "text"
  ]
  node [
    id 26
    label "kolejny"
    origin "text"
  ]
  node [
    id 27
    label "gwiazdozbi&#243;r"
    origin "text"
  ]
  node [
    id 28
    label "przeciwny"
    origin "text"
  ]
  node [
    id 29
    label "strona"
    origin "text"
  ]
  node [
    id 30
    label "ni&#380;"
    origin "text"
  ]
  node [
    id 31
    label "znak"
    origin "text"
  ]
  node [
    id 32
    label "zodiak"
    origin "text"
  ]
  node [
    id 33
    label "zale&#380;nie"
    origin "text"
  ]
  node [
    id 34
    label "t&#322;o"
    origin "text"
  ]
  node [
    id 35
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 36
    label "aktualnie"
    origin "text"
  ]
  node [
    id 37
    label "znajdowa&#263;"
    origin "text"
  ]
  node [
    id 38
    label "nast&#281;powa&#263;"
    origin "text"
  ]
  node [
    id 39
    label "era"
    origin "text"
  ]
  node [
    id 40
    label "obecnie"
    origin "text"
  ]
  node [
    id 41
    label "ko&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 42
    label "ryba"
    origin "text"
  ]
  node [
    id 43
    label "zbli&#380;a&#263;"
    origin "text"
  ]
  node [
    id 44
    label "wodnik"
    origin "text"
  ]
  node [
    id 45
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 46
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 47
    label "przybli&#380;enie"
    origin "text"
  ]
  node [
    id 48
    label "lata"
    origin "text"
  ]
  node [
    id 49
    label "pe&#322;ny"
    origin "text"
  ]
  node [
    id 50
    label "cykl"
    origin "text"
  ]
  node [
    id 51
    label "obieg"
    origin "text"
  ]
  node [
    id 52
    label "trwa&#263;"
    origin "text"
  ]
  node [
    id 53
    label "nasi"
    origin "text"
  ]
  node [
    id 54
    label "uczony"
    origin "text"
  ]
  node [
    id 55
    label "ten"
    origin "text"
  ]
  node [
    id 56
    label "okres"
    origin "text"
  ]
  node [
    id 57
    label "nazywa&#263;"
    origin "text"
  ]
  node [
    id 58
    label "plato&#324;ski"
    origin "text"
  ]
  node [
    id 59
    label "aztekowie"
    origin "text"
  ]
  node [
    id 60
    label "s&#322;o&#324;ce"
    origin "text"
  ]
  node [
    id 61
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 62
    label "tylko"
    origin "text"
  ]
  node [
    id 63
    label "ale"
    origin "text"
  ]
  node [
    id 64
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 65
    label "oznacza&#263;"
    origin "text"
  ]
  node [
    id 66
    label "wcale"
    origin "text"
  ]
  node [
    id 67
    label "ludzko&#347;&#263;"
    origin "text"
  ]
  node [
    id 68
    label "ewolucyjny"
    origin "text"
  ]
  node [
    id 69
    label "pocz&#261;tek"
    origin "text"
  ]
  node [
    id 70
    label "by&#322;y"
    origin "text"
  ]
  node [
    id 71
    label "narodziny"
    origin "text"
  ]
  node [
    id 72
    label "planet"
    origin "text"
  ]
  node [
    id 73
    label "wenus"
    origin "text"
  ]
  node [
    id 74
    label "przej&#347;cie"
    origin "text"
  ]
  node [
    id 75
    label "nowa"
    origin "text"
  ]
  node [
    id 76
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 77
    label "przynie&#347;&#263;"
    origin "text"
  ]
  node [
    id 78
    label "wielki"
    origin "text"
  ]
  node [
    id 79
    label "zmiana"
    origin "text"
  ]
  node [
    id 80
    label "&#347;wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 81
    label "znacz&#261;cy"
    origin "text"
  ]
  node [
    id 82
    label "przeskok"
    origin "text"
  ]
  node [
    id 83
    label "cywilizacyjny"
    origin "text"
  ]
  node [
    id 84
    label "chaos"
  ]
  node [
    id 85
    label "z&#322;&#261;czenie"
  ]
  node [
    id 86
    label "poruszenie"
  ]
  node [
    id 87
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 88
    label "perturbation"
  ]
  node [
    id 89
    label "wci&#261;gni&#281;cie"
  ]
  node [
    id 90
    label "szale&#324;stwo"
  ]
  node [
    id 91
    label "st&#281;&#380;e&#263;"
  ]
  node [
    id 92
    label "tobo&#322;ek"
  ]
  node [
    id 93
    label "obezw&#322;adni&#263;"
  ]
  node [
    id 94
    label "scali&#263;"
  ]
  node [
    id 95
    label "zawi&#261;za&#263;"
  ]
  node [
    id 96
    label "zatrzyma&#263;"
  ]
  node [
    id 97
    label "form"
  ]
  node [
    id 98
    label "bind"
  ]
  node [
    id 99
    label "wi&#261;zanie_chemiczne"
  ]
  node [
    id 100
    label "unify"
  ]
  node [
    id 101
    label "consort"
  ]
  node [
    id 102
    label "incorporate"
  ]
  node [
    id 103
    label "wi&#281;&#378;"
  ]
  node [
    id 104
    label "zobowi&#261;za&#263;"
  ]
  node [
    id 105
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 106
    label "w&#281;ze&#322;"
  ]
  node [
    id 107
    label "cz&#261;steczka_chemiczna"
  ]
  node [
    id 108
    label "powi&#261;za&#263;"
  ]
  node [
    id 109
    label "opakowa&#263;"
  ]
  node [
    id 110
    label "do&#322;&#261;czy&#263;"
  ]
  node [
    id 111
    label "cement"
  ]
  node [
    id 112
    label "zaprawa"
  ]
  node [
    id 113
    label "relate"
  ]
  node [
    id 114
    label "pozorny"
  ]
  node [
    id 115
    label "defenestracja"
  ]
  node [
    id 116
    label "szereg"
  ]
  node [
    id 117
    label "dzia&#322;anie"
  ]
  node [
    id 118
    label "miejsce"
  ]
  node [
    id 119
    label "ostatnie_podrygi"
  ]
  node [
    id 120
    label "kres"
  ]
  node [
    id 121
    label "agonia"
  ]
  node [
    id 122
    label "visitation"
  ]
  node [
    id 123
    label "szeol"
  ]
  node [
    id 124
    label "mogi&#322;a"
  ]
  node [
    id 125
    label "chwila"
  ]
  node [
    id 126
    label "wydarzenie"
  ]
  node [
    id 127
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 128
    label "pogrzebanie"
  ]
  node [
    id 129
    label "&#380;a&#322;oba"
  ]
  node [
    id 130
    label "zabicie"
  ]
  node [
    id 131
    label "kres_&#380;ycia"
  ]
  node [
    id 132
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 133
    label "obszar"
  ]
  node [
    id 134
    label "obiekt_naturalny"
  ]
  node [
    id 135
    label "przedmiot"
  ]
  node [
    id 136
    label "Stary_&#346;wiat"
  ]
  node [
    id 137
    label "grupa"
  ]
  node [
    id 138
    label "stw&#243;r"
  ]
  node [
    id 139
    label "biosfera"
  ]
  node [
    id 140
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 141
    label "rzecz"
  ]
  node [
    id 142
    label "magnetosfera"
  ]
  node [
    id 143
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 144
    label "environment"
  ]
  node [
    id 145
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 146
    label "geosfera"
  ]
  node [
    id 147
    label "Nowy_&#346;wiat"
  ]
  node [
    id 148
    label "planeta"
  ]
  node [
    id 149
    label "przejmowa&#263;"
  ]
  node [
    id 150
    label "litosfera"
  ]
  node [
    id 151
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 152
    label "makrokosmos"
  ]
  node [
    id 153
    label "barysfera"
  ]
  node [
    id 154
    label "biota"
  ]
  node [
    id 155
    label "p&#243;&#322;noc"
  ]
  node [
    id 156
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 157
    label "fauna"
  ]
  node [
    id 158
    label "wszechstworzenie"
  ]
  node [
    id 159
    label "geotermia"
  ]
  node [
    id 160
    label "biegun"
  ]
  node [
    id 161
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 162
    label "ekosystem"
  ]
  node [
    id 163
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 164
    label "teren"
  ]
  node [
    id 165
    label "zjawisko"
  ]
  node [
    id 166
    label "p&#243;&#322;kula"
  ]
  node [
    id 167
    label "atmosfera"
  ]
  node [
    id 168
    label "mikrokosmos"
  ]
  node [
    id 169
    label "class"
  ]
  node [
    id 170
    label "po&#322;udnie"
  ]
  node [
    id 171
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 172
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 173
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 174
    label "przejmowanie"
  ]
  node [
    id 175
    label "przestrze&#324;"
  ]
  node [
    id 176
    label "asymilowanie_si&#281;"
  ]
  node [
    id 177
    label "przej&#261;&#263;"
  ]
  node [
    id 178
    label "ekosfera"
  ]
  node [
    id 179
    label "przyroda"
  ]
  node [
    id 180
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 181
    label "ciemna_materia"
  ]
  node [
    id 182
    label "geoida"
  ]
  node [
    id 183
    label "Wsch&#243;d"
  ]
  node [
    id 184
    label "populace"
  ]
  node [
    id 185
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 186
    label "huczek"
  ]
  node [
    id 187
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 188
    label "Ziemia"
  ]
  node [
    id 189
    label "universe"
  ]
  node [
    id 190
    label "ozonosfera"
  ]
  node [
    id 191
    label "rze&#378;ba"
  ]
  node [
    id 192
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 193
    label "zagranica"
  ]
  node [
    id 194
    label "hydrosfera"
  ]
  node [
    id 195
    label "woda"
  ]
  node [
    id 196
    label "kuchnia"
  ]
  node [
    id 197
    label "przej&#281;cie"
  ]
  node [
    id 198
    label "czarna_dziura"
  ]
  node [
    id 199
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 200
    label "morze"
  ]
  node [
    id 201
    label "stulecie"
  ]
  node [
    id 202
    label "czas"
  ]
  node [
    id 203
    label "pora_roku"
  ]
  node [
    id 204
    label "cykl_astronomiczny"
  ]
  node [
    id 205
    label "p&#243;&#322;rocze"
  ]
  node [
    id 206
    label "kwarta&#322;"
  ]
  node [
    id 207
    label "kurs"
  ]
  node [
    id 208
    label "jubileusz"
  ]
  node [
    id 209
    label "miesi&#261;c"
  ]
  node [
    id 210
    label "martwy_sezon"
  ]
  node [
    id 211
    label "wej&#347;&#263;"
  ]
  node [
    id 212
    label "get"
  ]
  node [
    id 213
    label "wzi&#281;cie"
  ]
  node [
    id 214
    label "wyrucha&#263;"
  ]
  node [
    id 215
    label "uciec"
  ]
  node [
    id 216
    label "ruszy&#263;"
  ]
  node [
    id 217
    label "wygra&#263;"
  ]
  node [
    id 218
    label "obj&#261;&#263;"
  ]
  node [
    id 219
    label "zacz&#261;&#263;"
  ]
  node [
    id 220
    label "wyciupcia&#263;"
  ]
  node [
    id 221
    label "World_Health_Organization"
  ]
  node [
    id 222
    label "skorzysta&#263;"
  ]
  node [
    id 223
    label "pokona&#263;"
  ]
  node [
    id 224
    label "poczyta&#263;"
  ]
  node [
    id 225
    label "poruszy&#263;"
  ]
  node [
    id 226
    label "odby&#263;_stosunek_p&#322;ciowy"
  ]
  node [
    id 227
    label "take"
  ]
  node [
    id 228
    label "aim"
  ]
  node [
    id 229
    label "arise"
  ]
  node [
    id 230
    label "u&#380;y&#263;"
  ]
  node [
    id 231
    label "zaatakowa&#263;"
  ]
  node [
    id 232
    label "receive"
  ]
  node [
    id 233
    label "uda&#263;_si&#281;"
  ]
  node [
    id 234
    label "dosta&#263;"
  ]
  node [
    id 235
    label "otrzyma&#263;"
  ]
  node [
    id 236
    label "obskoczy&#263;"
  ]
  node [
    id 237
    label "zaopatrzy&#263;_si&#281;"
  ]
  node [
    id 238
    label "zrobi&#263;"
  ]
  node [
    id 239
    label "bra&#263;"
  ]
  node [
    id 240
    label "nakaza&#263;"
  ]
  node [
    id 241
    label "chwyci&#263;"
  ]
  node [
    id 242
    label "przyj&#261;&#263;"
  ]
  node [
    id 243
    label "seize"
  ]
  node [
    id 244
    label "odziedziczy&#263;"
  ]
  node [
    id 245
    label "withdraw"
  ]
  node [
    id 246
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 247
    label "wch&#322;on&#261;&#263;"
  ]
  node [
    id 248
    label "swoisty"
  ]
  node [
    id 249
    label "nienale&#380;yty"
  ]
  node [
    id 250
    label "niew&#322;a&#347;ciwie"
  ]
  node [
    id 251
    label "r&#243;&#380;ny"
  ]
  node [
    id 252
    label "z&#322;y"
  ]
  node [
    id 253
    label "dziwny"
  ]
  node [
    id 254
    label "nieodpowiednio"
  ]
  node [
    id 255
    label "creation"
  ]
  node [
    id 256
    label "skumanie"
  ]
  node [
    id 257
    label "zorientowanie"
  ]
  node [
    id 258
    label "przem&#243;wienie"
  ]
  node [
    id 259
    label "appreciation"
  ]
  node [
    id 260
    label "zacz&#281;cie_si&#281;"
  ]
  node [
    id 261
    label "clasp"
  ]
  node [
    id 262
    label "poczucie"
  ]
  node [
    id 263
    label "ocenienie"
  ]
  node [
    id 264
    label "pos&#322;uchanie"
  ]
  node [
    id 265
    label "sympathy"
  ]
  node [
    id 266
    label "&#380;yczliwo&#347;&#263;"
  ]
  node [
    id 267
    label "pozwolenie"
  ]
  node [
    id 268
    label "umiej&#281;tno&#347;&#263;"
  ]
  node [
    id 269
    label "wykszta&#322;cenie"
  ]
  node [
    id 270
    label "zaawansowanie"
  ]
  node [
    id 271
    label "intelekt"
  ]
  node [
    id 272
    label "cognition"
  ]
  node [
    id 273
    label "date"
  ]
  node [
    id 274
    label "str&#243;j"
  ]
  node [
    id 275
    label "sp&#281;dzi&#263;_czas"
  ]
  node [
    id 276
    label "spowodowa&#263;"
  ]
  node [
    id 277
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 278
    label "spotka&#263;_si&#281;"
  ]
  node [
    id 279
    label "poby&#263;"
  ]
  node [
    id 280
    label "pop&#322;ywa&#263;"
  ]
  node [
    id 281
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 282
    label "wynika&#263;"
  ]
  node [
    id 283
    label "fall"
  ]
  node [
    id 284
    label "bolt"
  ]
  node [
    id 285
    label "almanac"
  ]
  node [
    id 286
    label "wydawnictwo"
  ]
  node [
    id 287
    label "rozk&#322;ad"
  ]
  node [
    id 288
    label "rachuba_czasu"
  ]
  node [
    id 289
    label "Juliusz_Cezar"
  ]
  node [
    id 290
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 291
    label "temat"
  ]
  node [
    id 292
    label "kognicja"
  ]
  node [
    id 293
    label "idea"
  ]
  node [
    id 294
    label "szczeg&#243;&#322;"
  ]
  node [
    id 295
    label "przes&#322;anka"
  ]
  node [
    id 296
    label "rozprawa"
  ]
  node [
    id 297
    label "object"
  ]
  node [
    id 298
    label "proposition"
  ]
  node [
    id 299
    label "obr&#243;t"
  ]
  node [
    id 300
    label "precession"
  ]
  node [
    id 301
    label "Skandynawia"
  ]
  node [
    id 302
    label "Yorkshire"
  ]
  node [
    id 303
    label "Kaukaz"
  ]
  node [
    id 304
    label "Kaszmir"
  ]
  node [
    id 305
    label "Podbeskidzie"
  ]
  node [
    id 306
    label "Toskania"
  ]
  node [
    id 307
    label "&#321;emkowszczyzna"
  ]
  node [
    id 308
    label "Amhara"
  ]
  node [
    id 309
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 310
    label "Lombardia"
  ]
  node [
    id 311
    label "Kalabria"
  ]
  node [
    id 312
    label "kort"
  ]
  node [
    id 313
    label "Tyrol"
  ]
  node [
    id 314
    label "Pamir"
  ]
  node [
    id 315
    label "Lubelszczyzna"
  ]
  node [
    id 316
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 317
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 318
    label "&#379;ywiecczyzna"
  ]
  node [
    id 319
    label "ryzosfera"
  ]
  node [
    id 320
    label "Europa_Wschodnia"
  ]
  node [
    id 321
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 322
    label "Zabajkale"
  ]
  node [
    id 323
    label "Kaszuby"
  ]
  node [
    id 324
    label "Noworosja"
  ]
  node [
    id 325
    label "Bo&#347;nia"
  ]
  node [
    id 326
    label "Ba&#322;kany"
  ]
  node [
    id 327
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 328
    label "Anglia"
  ]
  node [
    id 329
    label "Kielecczyzna"
  ]
  node [
    id 330
    label "Pomorze_Zachodnie"
  ]
  node [
    id 331
    label "Opolskie"
  ]
  node [
    id 332
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 333
    label "skorupa_ziemska"
  ]
  node [
    id 334
    label "Ko&#322;yma"
  ]
  node [
    id 335
    label "Oksytania"
  ]
  node [
    id 336
    label "Syjon"
  ]
  node [
    id 337
    label "posadzka"
  ]
  node [
    id 338
    label "pa&#324;stwo"
  ]
  node [
    id 339
    label "Kociewie"
  ]
  node [
    id 340
    label "Huculszczyzna"
  ]
  node [
    id 341
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 342
    label "budynek"
  ]
  node [
    id 343
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 344
    label "Bawaria"
  ]
  node [
    id 345
    label "pomieszczenie"
  ]
  node [
    id 346
    label "pr&#243;chnica"
  ]
  node [
    id 347
    label "glinowanie"
  ]
  node [
    id 348
    label "Maghreb"
  ]
  node [
    id 349
    label "Bory_Tucholskie"
  ]
  node [
    id 350
    label "Europa_Zachodnia"
  ]
  node [
    id 351
    label "Kerala"
  ]
  node [
    id 352
    label "Podhale"
  ]
  node [
    id 353
    label "Kabylia"
  ]
  node [
    id 354
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 355
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 356
    label "Ma&#322;opolska"
  ]
  node [
    id 357
    label "Polesie"
  ]
  node [
    id 358
    label "Liguria"
  ]
  node [
    id 359
    label "&#321;&#243;dzkie"
  ]
  node [
    id 360
    label "geosystem"
  ]
  node [
    id 361
    label "Palestyna"
  ]
  node [
    id 362
    label "Bojkowszczyzna"
  ]
  node [
    id 363
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 364
    label "Karaiby"
  ]
  node [
    id 365
    label "S&#261;decczyzna"
  ]
  node [
    id 366
    label "Sand&#380;ak"
  ]
  node [
    id 367
    label "Nadrenia"
  ]
  node [
    id 368
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 369
    label "Zakarpacie"
  ]
  node [
    id 370
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 371
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 372
    label "Zag&#243;rze"
  ]
  node [
    id 373
    label "Andaluzja"
  ]
  node [
    id 374
    label "Turkiestan"
  ]
  node [
    id 375
    label "Naddniestrze"
  ]
  node [
    id 376
    label "Hercegowina"
  ]
  node [
    id 377
    label "p&#322;aszczyzna"
  ]
  node [
    id 378
    label "Opolszczyzna"
  ]
  node [
    id 379
    label "jednostka_administracyjna"
  ]
  node [
    id 380
    label "Lotaryngia"
  ]
  node [
    id 381
    label "Afryka_Wschodnia"
  ]
  node [
    id 382
    label "Szlezwik"
  ]
  node [
    id 383
    label "powierzchnia"
  ]
  node [
    id 384
    label "glinowa&#263;"
  ]
  node [
    id 385
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 386
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 387
    label "podglebie"
  ]
  node [
    id 388
    label "Mazowsze"
  ]
  node [
    id 389
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 390
    label "Afryka_Zachodnia"
  ]
  node [
    id 391
    label "czynnik_produkcji"
  ]
  node [
    id 392
    label "Galicja"
  ]
  node [
    id 393
    label "Szkocja"
  ]
  node [
    id 394
    label "Walia"
  ]
  node [
    id 395
    label "Powi&#347;le"
  ]
  node [
    id 396
    label "penetrator"
  ]
  node [
    id 397
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 398
    label "kompleks_sorpcyjny"
  ]
  node [
    id 399
    label "Zamojszczyzna"
  ]
  node [
    id 400
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 401
    label "Kujawy"
  ]
  node [
    id 402
    label "Podlasie"
  ]
  node [
    id 403
    label "Laponia"
  ]
  node [
    id 404
    label "Umbria"
  ]
  node [
    id 405
    label "plantowa&#263;"
  ]
  node [
    id 406
    label "Mezoameryka"
  ]
  node [
    id 407
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 408
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 409
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 410
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 411
    label "Kurdystan"
  ]
  node [
    id 412
    label "Kampania"
  ]
  node [
    id 413
    label "Armagnac"
  ]
  node [
    id 414
    label "Polinezja"
  ]
  node [
    id 415
    label "Warmia"
  ]
  node [
    id 416
    label "Wielkopolska"
  ]
  node [
    id 417
    label "Bordeaux"
  ]
  node [
    id 418
    label "Lauda"
  ]
  node [
    id 419
    label "Mazury"
  ]
  node [
    id 420
    label "Podkarpacie"
  ]
  node [
    id 421
    label "Oceania"
  ]
  node [
    id 422
    label "Lasko"
  ]
  node [
    id 423
    label "Amazonia"
  ]
  node [
    id 424
    label "pojazd"
  ]
  node [
    id 425
    label "glej"
  ]
  node [
    id 426
    label "martwica"
  ]
  node [
    id 427
    label "zapadnia"
  ]
  node [
    id 428
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 429
    label "dotleni&#263;"
  ]
  node [
    id 430
    label "Tonkin"
  ]
  node [
    id 431
    label "Kurpie"
  ]
  node [
    id 432
    label "Azja_Wschodnia"
  ]
  node [
    id 433
    label "Mikronezja"
  ]
  node [
    id 434
    label "Ukraina_Zachodnia"
  ]
  node [
    id 435
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 436
    label "Turyngia"
  ]
  node [
    id 437
    label "Baszkiria"
  ]
  node [
    id 438
    label "Apulia"
  ]
  node [
    id 439
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 440
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 441
    label "Indochiny"
  ]
  node [
    id 442
    label "Lubuskie"
  ]
  node [
    id 443
    label "Biskupizna"
  ]
  node [
    id 444
    label "domain"
  ]
  node [
    id 445
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 446
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 447
    label "prosta"
  ]
  node [
    id 448
    label "po&#322;o&#380;enie"
  ]
  node [
    id 449
    label "ust&#281;p"
  ]
  node [
    id 450
    label "problemat"
  ]
  node [
    id 451
    label "mark"
  ]
  node [
    id 452
    label "pozycja"
  ]
  node [
    id 453
    label "point"
  ]
  node [
    id 454
    label "stopie&#324;_pisma"
  ]
  node [
    id 455
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 456
    label "wojsko"
  ]
  node [
    id 457
    label "problematyka"
  ]
  node [
    id 458
    label "zapunktowa&#263;"
  ]
  node [
    id 459
    label "uk&#322;ad_odniesienia"
  ]
  node [
    id 460
    label "obiekt_matematyczny"
  ]
  node [
    id 461
    label "plamka"
  ]
  node [
    id 462
    label "obiekt"
  ]
  node [
    id 463
    label "plan"
  ]
  node [
    id 464
    label "podpunkt"
  ]
  node [
    id 465
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 466
    label "jednostka"
  ]
  node [
    id 467
    label "cz&#322;owiek"
  ]
  node [
    id 468
    label "becze&#263;"
  ]
  node [
    id 469
    label "zabecze&#263;"
  ]
  node [
    id 470
    label "futro"
  ]
  node [
    id 471
    label "czaban"
  ]
  node [
    id 472
    label "samiec"
  ]
  node [
    id 473
    label "owca_domowa"
  ]
  node [
    id 474
    label "g&#322;upek"
  ]
  node [
    id 475
    label "niestandardowo"
  ]
  node [
    id 476
    label "inny"
  ]
  node [
    id 477
    label "zmienia&#263;"
  ]
  node [
    id 478
    label "postpone"
  ]
  node [
    id 479
    label "dostosowywa&#263;"
  ]
  node [
    id 480
    label "translate"
  ]
  node [
    id 481
    label "przenosi&#263;"
  ]
  node [
    id 482
    label "transfer"
  ]
  node [
    id 483
    label "estrange"
  ]
  node [
    id 484
    label "go"
  ]
  node [
    id 485
    label "przestawia&#263;"
  ]
  node [
    id 486
    label "rusza&#263;"
  ]
  node [
    id 487
    label "ramble_on"
  ]
  node [
    id 488
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 489
    label "i&#347;&#263;"
  ]
  node [
    id 490
    label "nast&#281;pnie"
  ]
  node [
    id 491
    label "kt&#243;ry&#347;"
  ]
  node [
    id 492
    label "kolejno"
  ]
  node [
    id 493
    label "nastopny"
  ]
  node [
    id 494
    label "W&#281;&#380;ownik"
  ]
  node [
    id 495
    label "constellation"
  ]
  node [
    id 496
    label "Panna"
  ]
  node [
    id 497
    label "zbi&#243;r"
  ]
  node [
    id 498
    label "Ptak_Rajski"
  ]
  node [
    id 499
    label "Bli&#378;ni&#281;ta"
  ]
  node [
    id 500
    label "W&#261;&#380;"
  ]
  node [
    id 501
    label "odmienny"
  ]
  node [
    id 502
    label "po_przeciwnej_stronie"
  ]
  node [
    id 503
    label "odwrotnie"
  ]
  node [
    id 504
    label "przeciwnie"
  ]
  node [
    id 505
    label "odwrotno&#347;&#263;"
  ]
  node [
    id 506
    label "niech&#281;tny"
  ]
  node [
    id 507
    label "skr&#281;canie"
  ]
  node [
    id 508
    label "voice"
  ]
  node [
    id 509
    label "forma"
  ]
  node [
    id 510
    label "internet"
  ]
  node [
    id 511
    label "skr&#281;ci&#263;"
  ]
  node [
    id 512
    label "kartka"
  ]
  node [
    id 513
    label "orientowa&#263;"
  ]
  node [
    id 514
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 515
    label "plik"
  ]
  node [
    id 516
    label "bok"
  ]
  node [
    id 517
    label "pagina"
  ]
  node [
    id 518
    label "orientowanie"
  ]
  node [
    id 519
    label "fragment"
  ]
  node [
    id 520
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 521
    label "s&#261;d"
  ]
  node [
    id 522
    label "skr&#281;ca&#263;"
  ]
  node [
    id 523
    label "g&#243;ra"
  ]
  node [
    id 524
    label "serwis_internetowy"
  ]
  node [
    id 525
    label "orientacja"
  ]
  node [
    id 526
    label "linia"
  ]
  node [
    id 527
    label "skr&#281;cenie"
  ]
  node [
    id 528
    label "layout"
  ]
  node [
    id 529
    label "zorientowa&#263;"
  ]
  node [
    id 530
    label "podmiot"
  ]
  node [
    id 531
    label "ty&#322;"
  ]
  node [
    id 532
    label "logowanie"
  ]
  node [
    id 533
    label "adres_internetowy"
  ]
  node [
    id 534
    label "uj&#281;cie"
  ]
  node [
    id 535
    label "prz&#243;d"
  ]
  node [
    id 536
    label "posta&#263;"
  ]
  node [
    id 537
    label "poziom"
  ]
  node [
    id 538
    label "faza"
  ]
  node [
    id 539
    label "depression"
  ]
  node [
    id 540
    label "nizina"
  ]
  node [
    id 541
    label "postawi&#263;"
  ]
  node [
    id 542
    label "pozosta&#322;o&#347;&#263;"
  ]
  node [
    id 543
    label "wytw&#243;r"
  ]
  node [
    id 544
    label "implikowa&#263;"
  ]
  node [
    id 545
    label "stawia&#263;"
  ]
  node [
    id 546
    label "kodzik"
  ]
  node [
    id 547
    label "attribute"
  ]
  node [
    id 548
    label "dow&#243;d"
  ]
  node [
    id 549
    label "herb"
  ]
  node [
    id 550
    label "fakt"
  ]
  node [
    id 551
    label "oznakowanie"
  ]
  node [
    id 552
    label "znak_zodiaku"
  ]
  node [
    id 553
    label "niebo"
  ]
  node [
    id 554
    label "zodiac"
  ]
  node [
    id 555
    label "pas"
  ]
  node [
    id 556
    label "ekliptyka"
  ]
  node [
    id 557
    label "lennie"
  ]
  node [
    id 558
    label "zale&#380;ny"
  ]
  node [
    id 559
    label "melodia"
  ]
  node [
    id 560
    label "partia"
  ]
  node [
    id 561
    label "&#347;rodowisko"
  ]
  node [
    id 562
    label "ubarwienie"
  ]
  node [
    id 563
    label "gestaltyzm"
  ]
  node [
    id 564
    label "lingwistyka_kognitywna"
  ]
  node [
    id 565
    label "obraz"
  ]
  node [
    id 566
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 567
    label "pod&#322;o&#380;e"
  ]
  node [
    id 568
    label "informacja"
  ]
  node [
    id 569
    label "causal_agent"
  ]
  node [
    id 570
    label "dalszoplanowy"
  ]
  node [
    id 571
    label "layer"
  ]
  node [
    id 572
    label "warunki"
  ]
  node [
    id 573
    label "background"
  ]
  node [
    id 574
    label "ninie"
  ]
  node [
    id 575
    label "aktualny"
  ]
  node [
    id 576
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 577
    label "doznawa&#263;"
  ]
  node [
    id 578
    label "znachodzi&#263;"
  ]
  node [
    id 579
    label "dowiadywa&#263;_si&#281;"
  ]
  node [
    id 580
    label "pozyskiwa&#263;"
  ]
  node [
    id 581
    label "odzyskiwa&#263;"
  ]
  node [
    id 582
    label "os&#261;dza&#263;"
  ]
  node [
    id 583
    label "wykrywa&#263;"
  ]
  node [
    id 584
    label "unwrap"
  ]
  node [
    id 585
    label "detect"
  ]
  node [
    id 586
    label "wymy&#347;la&#263;"
  ]
  node [
    id 587
    label "powodowa&#263;"
  ]
  node [
    id 588
    label "mie&#263;_miejsce"
  ]
  node [
    id 589
    label "chance"
  ]
  node [
    id 590
    label "zdarza&#263;_si&#281;"
  ]
  node [
    id 591
    label "alternate"
  ]
  node [
    id 592
    label "naciska&#263;"
  ]
  node [
    id 593
    label "atakowa&#263;"
  ]
  node [
    id 594
    label "eon"
  ]
  node [
    id 595
    label "schy&#322;ek"
  ]
  node [
    id 596
    label "jednostka_geologiczna"
  ]
  node [
    id 597
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 598
    label "Zeitgeist"
  ]
  node [
    id 599
    label "dzieje"
  ]
  node [
    id 600
    label "zako&#324;cza&#263;"
  ]
  node [
    id 601
    label "przestawa&#263;"
  ]
  node [
    id 602
    label "robi&#263;"
  ]
  node [
    id 603
    label "satisfy"
  ]
  node [
    id 604
    label "close"
  ]
  node [
    id 605
    label "determine"
  ]
  node [
    id 606
    label "dokonywa&#263;_&#380;ywota"
  ]
  node [
    id 607
    label "stanowi&#263;"
  ]
  node [
    id 608
    label "wyrostek_filtracyjny"
  ]
  node [
    id 609
    label "doniczkowiec"
  ]
  node [
    id 610
    label "tar&#322;o"
  ]
  node [
    id 611
    label "patroszy&#263;"
  ]
  node [
    id 612
    label "rakowato&#347;&#263;"
  ]
  node [
    id 613
    label "system"
  ]
  node [
    id 614
    label "p&#281;cherz_p&#322;awny"
  ]
  node [
    id 615
    label "p&#322;etwa_brzuszna"
  ]
  node [
    id 616
    label "pokrywa_skrzelowa"
  ]
  node [
    id 617
    label "ikra"
  ]
  node [
    id 618
    label "fish"
  ]
  node [
    id 619
    label "w&#281;dkarstwo"
  ]
  node [
    id 620
    label "ryby"
  ]
  node [
    id 621
    label "szczelina_skrzelowa"
  ]
  node [
    id 622
    label "m&#281;tnooki"
  ]
  node [
    id 623
    label "systemik"
  ]
  node [
    id 624
    label "linia_boczna"
  ]
  node [
    id 625
    label "kr&#281;gowiec"
  ]
  node [
    id 626
    label "p&#322;etwa_odbytowa"
  ]
  node [
    id 627
    label "mi&#281;so"
  ]
  node [
    id 628
    label "przemieszcza&#263;"
  ]
  node [
    id 629
    label "set_about"
  ]
  node [
    id 630
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 631
    label "duch"
  ]
  node [
    id 632
    label "chru&#347;ciele"
  ]
  node [
    id 633
    label "ptak_wodny"
  ]
  node [
    id 634
    label "jaki&#347;"
  ]
  node [
    id 635
    label "report"
  ]
  node [
    id 636
    label "dodawa&#263;"
  ]
  node [
    id 637
    label "wymienia&#263;"
  ]
  node [
    id 638
    label "okre&#347;la&#263;"
  ]
  node [
    id 639
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 640
    label "dyskalkulia"
  ]
  node [
    id 641
    label "wynagrodzenie"
  ]
  node [
    id 642
    label "admit"
  ]
  node [
    id 643
    label "osi&#261;ga&#263;"
  ]
  node [
    id 644
    label "wyznacza&#263;"
  ]
  node [
    id 645
    label "posiada&#263;"
  ]
  node [
    id 646
    label "mierzy&#263;"
  ]
  node [
    id 647
    label "odlicza&#263;"
  ]
  node [
    id 648
    label "wycenia&#263;"
  ]
  node [
    id 649
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 650
    label "rachowa&#263;"
  ]
  node [
    id 651
    label "tell"
  ]
  node [
    id 652
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 653
    label "policza&#263;"
  ]
  node [
    id 654
    label "count"
  ]
  node [
    id 655
    label "pickup"
  ]
  node [
    id 656
    label "approach"
  ]
  node [
    id 657
    label "bliski"
  ]
  node [
    id 658
    label "podanie"
  ]
  node [
    id 659
    label "przemieszczenie"
  ]
  node [
    id 660
    label "po&#322;&#261;czenie"
  ]
  node [
    id 661
    label "prze&#322;o&#380;enie"
  ]
  node [
    id 662
    label "zapoznanie"
  ]
  node [
    id 663
    label "ocena"
  ]
  node [
    id 664
    label "wyja&#347;nienie"
  ]
  node [
    id 665
    label "estimate"
  ]
  node [
    id 666
    label "summer"
  ]
  node [
    id 667
    label "szcz&#281;&#347;liwy"
  ]
  node [
    id 668
    label "nieograniczony"
  ]
  node [
    id 669
    label "nape&#322;nienie_si&#281;"
  ]
  node [
    id 670
    label "kompletny"
  ]
  node [
    id 671
    label "r&#243;wny"
  ]
  node [
    id 672
    label "wype&#322;nianie_si&#281;"
  ]
  node [
    id 673
    label "bezwzgl&#281;dny"
  ]
  node [
    id 674
    label "zupe&#322;ny"
  ]
  node [
    id 675
    label "satysfakcja"
  ]
  node [
    id 676
    label "wszechogarniaj&#261;cy"
  ]
  node [
    id 677
    label "pe&#322;no"
  ]
  node [
    id 678
    label "wype&#322;nienie"
  ]
  node [
    id 679
    label "otwarty"
  ]
  node [
    id 680
    label "sekwencja"
  ]
  node [
    id 681
    label "edycja"
  ]
  node [
    id 682
    label "przebieg"
  ]
  node [
    id 683
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 684
    label "cycle"
  ]
  node [
    id 685
    label "owulacja"
  ]
  node [
    id 686
    label "miesi&#261;czka"
  ]
  node [
    id 687
    label "set"
  ]
  node [
    id 688
    label "przep&#322;yw"
  ]
  node [
    id 689
    label "obecno&#347;&#263;"
  ]
  node [
    id 690
    label "tour"
  ]
  node [
    id 691
    label "ruch"
  ]
  node [
    id 692
    label "dzia&#322;alno&#347;&#263;_gospodarcza"
  ]
  node [
    id 693
    label "circulation"
  ]
  node [
    id 694
    label "zostawa&#263;"
  ]
  node [
    id 695
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 696
    label "pozostawa&#263;"
  ]
  node [
    id 697
    label "stand"
  ]
  node [
    id 698
    label "adhere"
  ]
  node [
    id 699
    label "istnie&#263;"
  ]
  node [
    id 700
    label "inteligent"
  ]
  node [
    id 701
    label "nauczny"
  ]
  node [
    id 702
    label "uczenie"
  ]
  node [
    id 703
    label "wykszta&#322;cony"
  ]
  node [
    id 704
    label "m&#261;dry"
  ]
  node [
    id 705
    label "Awerroes"
  ]
  node [
    id 706
    label "intelektualista"
  ]
  node [
    id 707
    label "okre&#347;lony"
  ]
  node [
    id 708
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 709
    label "paleogen"
  ]
  node [
    id 710
    label "spell"
  ]
  node [
    id 711
    label "period"
  ]
  node [
    id 712
    label "prekambr"
  ]
  node [
    id 713
    label "jura"
  ]
  node [
    id 714
    label "interstadia&#322;"
  ]
  node [
    id 715
    label "izochronizm"
  ]
  node [
    id 716
    label "okres_noachijski"
  ]
  node [
    id 717
    label "orosir"
  ]
  node [
    id 718
    label "kreda"
  ]
  node [
    id 719
    label "sten"
  ]
  node [
    id 720
    label "drugorz&#281;d"
  ]
  node [
    id 721
    label "semester"
  ]
  node [
    id 722
    label "trzeciorz&#281;d"
  ]
  node [
    id 723
    label "ton"
  ]
  node [
    id 724
    label "poprzednik"
  ]
  node [
    id 725
    label "ordowik"
  ]
  node [
    id 726
    label "karbon"
  ]
  node [
    id 727
    label "trias"
  ]
  node [
    id 728
    label "kalim"
  ]
  node [
    id 729
    label "stater"
  ]
  node [
    id 730
    label "p&#243;&#322;okres"
  ]
  node [
    id 731
    label "czwartorz&#281;d"
  ]
  node [
    id 732
    label "pulsacja"
  ]
  node [
    id 733
    label "okres_amazo&#324;ski"
  ]
  node [
    id 734
    label "kambr"
  ]
  node [
    id 735
    label "nast&#281;pnik"
  ]
  node [
    id 736
    label "kriogen"
  ]
  node [
    id 737
    label "glacja&#322;"
  ]
  node [
    id 738
    label "fala"
  ]
  node [
    id 739
    label "okres_czasu"
  ]
  node [
    id 740
    label "riak"
  ]
  node [
    id 741
    label "okres_hesperyjski"
  ]
  node [
    id 742
    label "sylur"
  ]
  node [
    id 743
    label "dewon"
  ]
  node [
    id 744
    label "ciota"
  ]
  node [
    id 745
    label "epoka"
  ]
  node [
    id 746
    label "pierwszorz&#281;d"
  ]
  node [
    id 747
    label "okres_halsztacki"
  ]
  node [
    id 748
    label "ektas"
  ]
  node [
    id 749
    label "zdanie"
  ]
  node [
    id 750
    label "condition"
  ]
  node [
    id 751
    label "cykl_miesi&#261;czkowy"
  ]
  node [
    id 752
    label "rok_akademicki"
  ]
  node [
    id 753
    label "wapie&#324;_muszlowy"
  ]
  node [
    id 754
    label "postglacja&#322;"
  ]
  node [
    id 755
    label "proces_fizjologiczny"
  ]
  node [
    id 756
    label "ediakar"
  ]
  node [
    id 757
    label "time_period"
  ]
  node [
    id 758
    label "ruch_drgaj&#261;cy"
  ]
  node [
    id 759
    label "perm"
  ]
  node [
    id 760
    label "rok_szkolny"
  ]
  node [
    id 761
    label "neogen"
  ]
  node [
    id 762
    label "sider"
  ]
  node [
    id 763
    label "flow"
  ]
  node [
    id 764
    label "podokres"
  ]
  node [
    id 765
    label "preglacja&#322;"
  ]
  node [
    id 766
    label "retoryka"
  ]
  node [
    id 767
    label "choroba_przyrodzona"
  ]
  node [
    id 768
    label "give"
  ]
  node [
    id 769
    label "mieni&#263;"
  ]
  node [
    id 770
    label "nadawa&#263;"
  ]
  node [
    id 771
    label "plato&#324;sko"
  ]
  node [
    id 772
    label "charakterystyczny"
  ]
  node [
    id 773
    label "S&#322;o&#324;ce"
  ]
  node [
    id 774
    label "zach&#243;d"
  ]
  node [
    id 775
    label "atmosfera_s&#322;oneczna"
  ]
  node [
    id 776
    label "&#347;wiat&#322;o"
  ]
  node [
    id 777
    label "sunlight"
  ]
  node [
    id 778
    label "wsch&#243;d"
  ]
  node [
    id 779
    label "kochanie"
  ]
  node [
    id 780
    label "kalendarz_s&#322;oneczny"
  ]
  node [
    id 781
    label "pogoda"
  ]
  node [
    id 782
    label "dzie&#324;"
  ]
  node [
    id 783
    label "piwo"
  ]
  node [
    id 784
    label "du&#380;y"
  ]
  node [
    id 785
    label "jedyny"
  ]
  node [
    id 786
    label "zdr&#243;w"
  ]
  node [
    id 787
    label "&#380;ywy"
  ]
  node [
    id 788
    label "ca&#322;o"
  ]
  node [
    id 789
    label "calu&#347;ko"
  ]
  node [
    id 790
    label "podobny"
  ]
  node [
    id 791
    label "by&#263;"
  ]
  node [
    id 792
    label "represent"
  ]
  node [
    id 793
    label "wyraz"
  ]
  node [
    id 794
    label "wskazywa&#263;"
  ]
  node [
    id 795
    label "signify"
  ]
  node [
    id 796
    label "ustala&#263;"
  ]
  node [
    id 797
    label "ni_chuja"
  ]
  node [
    id 798
    label "ca&#322;kiem"
  ]
  node [
    id 799
    label "zupe&#322;nie"
  ]
  node [
    id 800
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 801
    label "ewolucyjnie"
  ]
  node [
    id 802
    label "stopniowy"
  ]
  node [
    id 803
    label "upgrade"
  ]
  node [
    id 804
    label "pierworodztwo"
  ]
  node [
    id 805
    label "nast&#281;pstwo"
  ]
  node [
    id 806
    label "dawny"
  ]
  node [
    id 807
    label "rozw&#243;d"
  ]
  node [
    id 808
    label "rozwodzi&#263;_si&#281;"
  ]
  node [
    id 809
    label "eksprezydent"
  ]
  node [
    id 810
    label "rozwie&#347;&#263;_si&#281;"
  ]
  node [
    id 811
    label "partner"
  ]
  node [
    id 812
    label "rozwiedzenie_si&#281;"
  ]
  node [
    id 813
    label "rozwodzenie_si&#281;"
  ]
  node [
    id 814
    label "wcze&#347;niejszy"
  ]
  node [
    id 815
    label "narz&#281;dzie"
  ]
  node [
    id 816
    label "nale&#380;enie"
  ]
  node [
    id 817
    label "odmienienie"
  ]
  node [
    id 818
    label "przynale&#380;no&#347;&#263;"
  ]
  node [
    id 819
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 820
    label "mini&#281;cie"
  ]
  node [
    id 821
    label "prze&#380;ycie"
  ]
  node [
    id 822
    label "strain"
  ]
  node [
    id 823
    label "przerobienie"
  ]
  node [
    id 824
    label "stanie_si&#281;"
  ]
  node [
    id 825
    label "dostanie_si&#281;"
  ]
  node [
    id 826
    label "wydeptanie"
  ]
  node [
    id 827
    label "wydeptywanie"
  ]
  node [
    id 828
    label "offense"
  ]
  node [
    id 829
    label "wymienienie"
  ]
  node [
    id 830
    label "zacz&#281;cie"
  ]
  node [
    id 831
    label "trwanie"
  ]
  node [
    id 832
    label "przepojenie"
  ]
  node [
    id 833
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 834
    label "zaliczenie"
  ]
  node [
    id 835
    label "zdarzenie_si&#281;"
  ]
  node [
    id 836
    label "uznanie"
  ]
  node [
    id 837
    label "nasycenie_si&#281;"
  ]
  node [
    id 838
    label "przemokni&#281;cie"
  ]
  node [
    id 839
    label "nas&#261;czenie"
  ]
  node [
    id 840
    label "mienie"
  ]
  node [
    id 841
    label "ustawa"
  ]
  node [
    id 842
    label "experience"
  ]
  node [
    id 843
    label "przewy&#380;szenie"
  ]
  node [
    id 844
    label "doznanie"
  ]
  node [
    id 845
    label "przestanie"
  ]
  node [
    id 846
    label "traversal"
  ]
  node [
    id 847
    label "przebycie"
  ]
  node [
    id 848
    label "przedostanie_si&#281;"
  ]
  node [
    id 849
    label "sko&#324;czenie_si&#281;"
  ]
  node [
    id 850
    label "wstawka"
  ]
  node [
    id 851
    label "przepuszczenie"
  ]
  node [
    id 852
    label "wytyczenie"
  ]
  node [
    id 853
    label "crack"
  ]
  node [
    id 854
    label "gwiazda"
  ]
  node [
    id 855
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 856
    label "czyj&#347;"
  ]
  node [
    id 857
    label "m&#261;&#380;"
  ]
  node [
    id 858
    label "zwi&#281;kszy&#263;"
  ]
  node [
    id 859
    label "da&#263;"
  ]
  node [
    id 860
    label "ponie&#347;&#263;"
  ]
  node [
    id 861
    label "przytacha&#263;"
  ]
  node [
    id 862
    label "increase"
  ]
  node [
    id 863
    label "doda&#263;"
  ]
  node [
    id 864
    label "zanie&#347;&#263;"
  ]
  node [
    id 865
    label "poda&#263;"
  ]
  node [
    id 866
    label "carry"
  ]
  node [
    id 867
    label "dupny"
  ]
  node [
    id 868
    label "wysoce"
  ]
  node [
    id 869
    label "wyj&#261;tkowy"
  ]
  node [
    id 870
    label "wybitny"
  ]
  node [
    id 871
    label "znaczny"
  ]
  node [
    id 872
    label "prawdziwy"
  ]
  node [
    id 873
    label "wa&#380;ny"
  ]
  node [
    id 874
    label "nieprzeci&#281;tny"
  ]
  node [
    id 875
    label "anatomopatolog"
  ]
  node [
    id 876
    label "rewizja"
  ]
  node [
    id 877
    label "oznaka"
  ]
  node [
    id 878
    label "ferment"
  ]
  node [
    id 879
    label "komplet"
  ]
  node [
    id 880
    label "tura"
  ]
  node [
    id 881
    label "amendment"
  ]
  node [
    id 882
    label "zmianka"
  ]
  node [
    id 883
    label "odmienianie"
  ]
  node [
    id 884
    label "passage"
  ]
  node [
    id 885
    label "change"
  ]
  node [
    id 886
    label "praca"
  ]
  node [
    id 887
    label "stan"
  ]
  node [
    id 888
    label "psychika"
  ]
  node [
    id 889
    label "psychoanaliza"
  ]
  node [
    id 890
    label "ekstraspekcja"
  ]
  node [
    id 891
    label "zemdle&#263;"
  ]
  node [
    id 892
    label "conscience"
  ]
  node [
    id 893
    label "Freud"
  ]
  node [
    id 894
    label "feeling"
  ]
  node [
    id 895
    label "istotnie"
  ]
  node [
    id 896
    label "znacz&#261;co"
  ]
  node [
    id 897
    label "dono&#347;ny"
  ]
  node [
    id 898
    label "hop"
  ]
  node [
    id 899
    label "skip"
  ]
  node [
    id 900
    label "zwrot"
  ]
  node [
    id 901
    label "skok"
  ]
  node [
    id 902
    label "jump"
  ]
  node [
    id 903
    label "cywilizacyjnie"
  ]
  node [
    id 904
    label "nowy"
  ]
  node [
    id 905
    label "Richard"
  ]
  node [
    id 906
    label "Dawkins"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 84
  ]
  edge [
    source 0
    target 85
  ]
  edge [
    source 0
    target 86
  ]
  edge [
    source 0
    target 87
  ]
  edge [
    source 0
    target 88
  ]
  edge [
    source 0
    target 89
  ]
  edge [
    source 0
    target 90
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 1
    target 92
  ]
  edge [
    source 1
    target 93
  ]
  edge [
    source 1
    target 94
  ]
  edge [
    source 1
    target 95
  ]
  edge [
    source 1
    target 96
  ]
  edge [
    source 1
    target 97
  ]
  edge [
    source 1
    target 98
  ]
  edge [
    source 1
    target 99
  ]
  edge [
    source 1
    target 100
  ]
  edge [
    source 1
    target 101
  ]
  edge [
    source 1
    target 102
  ]
  edge [
    source 1
    target 103
  ]
  edge [
    source 1
    target 104
  ]
  edge [
    source 1
    target 105
  ]
  edge [
    source 1
    target 106
  ]
  edge [
    source 1
    target 107
  ]
  edge [
    source 1
    target 108
  ]
  edge [
    source 1
    target 109
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 1
    target 111
  ]
  edge [
    source 1
    target 112
  ]
  edge [
    source 1
    target 113
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 114
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 115
  ]
  edge [
    source 3
    target 116
  ]
  edge [
    source 3
    target 117
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 119
  ]
  edge [
    source 3
    target 120
  ]
  edge [
    source 3
    target 121
  ]
  edge [
    source 3
    target 122
  ]
  edge [
    source 3
    target 123
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 3
    target 125
  ]
  edge [
    source 3
    target 126
  ]
  edge [
    source 3
    target 127
  ]
  edge [
    source 3
    target 128
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 129
  ]
  edge [
    source 3
    target 130
  ]
  edge [
    source 3
    target 131
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 132
  ]
  edge [
    source 4
    target 133
  ]
  edge [
    source 4
    target 134
  ]
  edge [
    source 4
    target 135
  ]
  edge [
    source 4
    target 136
  ]
  edge [
    source 4
    target 137
  ]
  edge [
    source 4
    target 138
  ]
  edge [
    source 4
    target 139
  ]
  edge [
    source 4
    target 140
  ]
  edge [
    source 4
    target 141
  ]
  edge [
    source 4
    target 142
  ]
  edge [
    source 4
    target 143
  ]
  edge [
    source 4
    target 144
  ]
  edge [
    source 4
    target 145
  ]
  edge [
    source 4
    target 146
  ]
  edge [
    source 4
    target 147
  ]
  edge [
    source 4
    target 148
  ]
  edge [
    source 4
    target 149
  ]
  edge [
    source 4
    target 150
  ]
  edge [
    source 4
    target 151
  ]
  edge [
    source 4
    target 152
  ]
  edge [
    source 4
    target 153
  ]
  edge [
    source 4
    target 154
  ]
  edge [
    source 4
    target 155
  ]
  edge [
    source 4
    target 156
  ]
  edge [
    source 4
    target 157
  ]
  edge [
    source 4
    target 158
  ]
  edge [
    source 4
    target 159
  ]
  edge [
    source 4
    target 160
  ]
  edge [
    source 4
    target 161
  ]
  edge [
    source 4
    target 162
  ]
  edge [
    source 4
    target 163
  ]
  edge [
    source 4
    target 164
  ]
  edge [
    source 4
    target 165
  ]
  edge [
    source 4
    target 166
  ]
  edge [
    source 4
    target 167
  ]
  edge [
    source 4
    target 168
  ]
  edge [
    source 4
    target 169
  ]
  edge [
    source 4
    target 170
  ]
  edge [
    source 4
    target 171
  ]
  edge [
    source 4
    target 172
  ]
  edge [
    source 4
    target 173
  ]
  edge [
    source 4
    target 174
  ]
  edge [
    source 4
    target 175
  ]
  edge [
    source 4
    target 176
  ]
  edge [
    source 4
    target 177
  ]
  edge [
    source 4
    target 178
  ]
  edge [
    source 4
    target 179
  ]
  edge [
    source 4
    target 180
  ]
  edge [
    source 4
    target 181
  ]
  edge [
    source 4
    target 182
  ]
  edge [
    source 4
    target 183
  ]
  edge [
    source 4
    target 184
  ]
  edge [
    source 4
    target 185
  ]
  edge [
    source 4
    target 186
  ]
  edge [
    source 4
    target 187
  ]
  edge [
    source 4
    target 188
  ]
  edge [
    source 4
    target 189
  ]
  edge [
    source 4
    target 190
  ]
  edge [
    source 4
    target 191
  ]
  edge [
    source 4
    target 192
  ]
  edge [
    source 4
    target 193
  ]
  edge [
    source 4
    target 194
  ]
  edge [
    source 4
    target 195
  ]
  edge [
    source 4
    target 196
  ]
  edge [
    source 4
    target 197
  ]
  edge [
    source 4
    target 198
  ]
  edge [
    source 4
    target 199
  ]
  edge [
    source 4
    target 200
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 201
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 202
  ]
  edge [
    source 5
    target 203
  ]
  edge [
    source 5
    target 204
  ]
  edge [
    source 5
    target 205
  ]
  edge [
    source 5
    target 137
  ]
  edge [
    source 5
    target 206
  ]
  edge [
    source 5
    target 207
  ]
  edge [
    source 5
    target 208
  ]
  edge [
    source 5
    target 209
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 210
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 23
  ]
  edge [
    source 7
    target 24
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 248
  ]
  edge [
    source 8
    target 249
  ]
  edge [
    source 8
    target 250
  ]
  edge [
    source 8
    target 251
  ]
  edge [
    source 8
    target 252
  ]
  edge [
    source 8
    target 253
  ]
  edge [
    source 8
    target 254
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 255
  ]
  edge [
    source 9
    target 256
  ]
  edge [
    source 9
    target 257
  ]
  edge [
    source 9
    target 258
  ]
  edge [
    source 9
    target 259
  ]
  edge [
    source 9
    target 260
  ]
  edge [
    source 9
    target 261
  ]
  edge [
    source 9
    target 262
  ]
  edge [
    source 9
    target 263
  ]
  edge [
    source 9
    target 264
  ]
  edge [
    source 9
    target 265
  ]
  edge [
    source 9
    target 266
  ]
  edge [
    source 9
    target 32
  ]
  edge [
    source 9
    target 47
  ]
  edge [
    source 9
    target 57
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 267
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 268
  ]
  edge [
    source 10
    target 269
  ]
  edge [
    source 10
    target 270
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 271
  ]
  edge [
    source 10
    target 272
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 273
  ]
  edge [
    source 11
    target 274
  ]
  edge [
    source 11
    target 202
  ]
  edge [
    source 11
    target 275
  ]
  edge [
    source 11
    target 276
  ]
  edge [
    source 11
    target 277
  ]
  edge [
    source 11
    target 233
  ]
  edge [
    source 11
    target 278
  ]
  edge [
    source 11
    target 279
  ]
  edge [
    source 11
    target 280
  ]
  edge [
    source 11
    target 281
  ]
  edge [
    source 11
    target 282
  ]
  edge [
    source 11
    target 283
  ]
  edge [
    source 11
    target 284
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 61
  ]
  edge [
    source 12
    target 285
  ]
  edge [
    source 12
    target 286
  ]
  edge [
    source 12
    target 287
  ]
  edge [
    source 12
    target 288
  ]
  edge [
    source 12
    target 289
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 60
  ]
  edge [
    source 13
    target 61
  ]
  edge [
    source 13
    target 70
  ]
  edge [
    source 13
    target 290
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 291
  ]
  edge [
    source 14
    target 292
  ]
  edge [
    source 14
    target 293
  ]
  edge [
    source 14
    target 294
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 295
  ]
  edge [
    source 14
    target 296
  ]
  edge [
    source 14
    target 297
  ]
  edge [
    source 14
    target 298
  ]
  edge [
    source 14
    target 19
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 299
  ]
  edge [
    source 15
    target 300
  ]
  edge [
    source 15
    target 82
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 37
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 301
  ]
  edge [
    source 16
    target 302
  ]
  edge [
    source 16
    target 303
  ]
  edge [
    source 16
    target 304
  ]
  edge [
    source 16
    target 305
  ]
  edge [
    source 16
    target 306
  ]
  edge [
    source 16
    target 307
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 308
  ]
  edge [
    source 16
    target 309
  ]
  edge [
    source 16
    target 310
  ]
  edge [
    source 16
    target 311
  ]
  edge [
    source 16
    target 312
  ]
  edge [
    source 16
    target 313
  ]
  edge [
    source 16
    target 314
  ]
  edge [
    source 16
    target 315
  ]
  edge [
    source 16
    target 316
  ]
  edge [
    source 16
    target 317
  ]
  edge [
    source 16
    target 318
  ]
  edge [
    source 16
    target 319
  ]
  edge [
    source 16
    target 320
  ]
  edge [
    source 16
    target 321
  ]
  edge [
    source 16
    target 322
  ]
  edge [
    source 16
    target 323
  ]
  edge [
    source 16
    target 324
  ]
  edge [
    source 16
    target 325
  ]
  edge [
    source 16
    target 326
  ]
  edge [
    source 16
    target 327
  ]
  edge [
    source 16
    target 328
  ]
  edge [
    source 16
    target 329
  ]
  edge [
    source 16
    target 330
  ]
  edge [
    source 16
    target 331
  ]
  edge [
    source 16
    target 332
  ]
  edge [
    source 16
    target 333
  ]
  edge [
    source 16
    target 334
  ]
  edge [
    source 16
    target 335
  ]
  edge [
    source 16
    target 336
  ]
  edge [
    source 16
    target 337
  ]
  edge [
    source 16
    target 338
  ]
  edge [
    source 16
    target 339
  ]
  edge [
    source 16
    target 340
  ]
  edge [
    source 16
    target 341
  ]
  edge [
    source 16
    target 342
  ]
  edge [
    source 16
    target 343
  ]
  edge [
    source 16
    target 344
  ]
  edge [
    source 16
    target 345
  ]
  edge [
    source 16
    target 346
  ]
  edge [
    source 16
    target 347
  ]
  edge [
    source 16
    target 348
  ]
  edge [
    source 16
    target 349
  ]
  edge [
    source 16
    target 350
  ]
  edge [
    source 16
    target 351
  ]
  edge [
    source 16
    target 352
  ]
  edge [
    source 16
    target 353
  ]
  edge [
    source 16
    target 354
  ]
  edge [
    source 16
    target 355
  ]
  edge [
    source 16
    target 356
  ]
  edge [
    source 16
    target 357
  ]
  edge [
    source 16
    target 358
  ]
  edge [
    source 16
    target 359
  ]
  edge [
    source 16
    target 360
  ]
  edge [
    source 16
    target 361
  ]
  edge [
    source 16
    target 362
  ]
  edge [
    source 16
    target 363
  ]
  edge [
    source 16
    target 364
  ]
  edge [
    source 16
    target 365
  ]
  edge [
    source 16
    target 366
  ]
  edge [
    source 16
    target 367
  ]
  edge [
    source 16
    target 368
  ]
  edge [
    source 16
    target 369
  ]
  edge [
    source 16
    target 370
  ]
  edge [
    source 16
    target 371
  ]
  edge [
    source 16
    target 372
  ]
  edge [
    source 16
    target 373
  ]
  edge [
    source 16
    target 374
  ]
  edge [
    source 16
    target 375
  ]
  edge [
    source 16
    target 376
  ]
  edge [
    source 16
    target 377
  ]
  edge [
    source 16
    target 378
  ]
  edge [
    source 16
    target 379
  ]
  edge [
    source 16
    target 380
  ]
  edge [
    source 16
    target 381
  ]
  edge [
    source 16
    target 382
  ]
  edge [
    source 16
    target 383
  ]
  edge [
    source 16
    target 384
  ]
  edge [
    source 16
    target 385
  ]
  edge [
    source 16
    target 386
  ]
  edge [
    source 16
    target 387
  ]
  edge [
    source 16
    target 388
  ]
  edge [
    source 16
    target 389
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 390
  ]
  edge [
    source 16
    target 391
  ]
  edge [
    source 16
    target 392
  ]
  edge [
    source 16
    target 393
  ]
  edge [
    source 16
    target 394
  ]
  edge [
    source 16
    target 395
  ]
  edge [
    source 16
    target 396
  ]
  edge [
    source 16
    target 397
  ]
  edge [
    source 16
    target 398
  ]
  edge [
    source 16
    target 399
  ]
  edge [
    source 16
    target 400
  ]
  edge [
    source 16
    target 401
  ]
  edge [
    source 16
    target 402
  ]
  edge [
    source 16
    target 403
  ]
  edge [
    source 16
    target 404
  ]
  edge [
    source 16
    target 405
  ]
  edge [
    source 16
    target 406
  ]
  edge [
    source 16
    target 407
  ]
  edge [
    source 16
    target 408
  ]
  edge [
    source 16
    target 409
  ]
  edge [
    source 16
    target 410
  ]
  edge [
    source 16
    target 411
  ]
  edge [
    source 16
    target 412
  ]
  edge [
    source 16
    target 413
  ]
  edge [
    source 16
    target 414
  ]
  edge [
    source 16
    target 415
  ]
  edge [
    source 16
    target 416
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 417
  ]
  edge [
    source 16
    target 418
  ]
  edge [
    source 16
    target 419
  ]
  edge [
    source 16
    target 420
  ]
  edge [
    source 16
    target 421
  ]
  edge [
    source 16
    target 422
  ]
  edge [
    source 16
    target 423
  ]
  edge [
    source 16
    target 424
  ]
  edge [
    source 16
    target 425
  ]
  edge [
    source 16
    target 426
  ]
  edge [
    source 16
    target 427
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 428
  ]
  edge [
    source 16
    target 429
  ]
  edge [
    source 16
    target 430
  ]
  edge [
    source 16
    target 431
  ]
  edge [
    source 16
    target 432
  ]
  edge [
    source 16
    target 433
  ]
  edge [
    source 16
    target 434
  ]
  edge [
    source 16
    target 435
  ]
  edge [
    source 16
    target 436
  ]
  edge [
    source 16
    target 437
  ]
  edge [
    source 16
    target 438
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 439
  ]
  edge [
    source 16
    target 440
  ]
  edge [
    source 16
    target 441
  ]
  edge [
    source 16
    target 442
  ]
  edge [
    source 16
    target 443
  ]
  edge [
    source 16
    target 444
  ]
  edge [
    source 16
    target 445
  ]
  edge [
    source 16
    target 446
  ]
  edge [
    source 16
    target 74
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 21
  ]
  edge [
    source 18
    target 57
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 21
  ]
  edge [
    source 19
    target 22
  ]
  edge [
    source 19
    target 51
  ]
  edge [
    source 19
    target 447
  ]
  edge [
    source 19
    target 448
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 449
  ]
  edge [
    source 19
    target 450
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 451
  ]
  edge [
    source 19
    target 452
  ]
  edge [
    source 19
    target 453
  ]
  edge [
    source 19
    target 454
  ]
  edge [
    source 19
    target 455
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 456
  ]
  edge [
    source 19
    target 457
  ]
  edge [
    source 19
    target 458
  ]
  edge [
    source 19
    target 459
  ]
  edge [
    source 19
    target 460
  ]
  edge [
    source 19
    target 461
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 462
  ]
  edge [
    source 19
    target 463
  ]
  edge [
    source 19
    target 464
  ]
  edge [
    source 19
    target 465
  ]
  edge [
    source 19
    target 466
  ]
  edge [
    source 20
    target 52
  ]
  edge [
    source 20
    target 467
  ]
  edge [
    source 20
    target 468
  ]
  edge [
    source 20
    target 469
  ]
  edge [
    source 20
    target 470
  ]
  edge [
    source 20
    target 471
  ]
  edge [
    source 20
    target 472
  ]
  edge [
    source 20
    target 473
  ]
  edge [
    source 20
    target 474
  ]
  edge [
    source 20
    target 27
  ]
  edge [
    source 21
    target 475
  ]
  edge [
    source 21
    target 476
  ]
  edge [
    source 21
    target 51
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 165
  ]
  edge [
    source 22
    target 25
  ]
  edge [
    source 22
    target 69
  ]
  edge [
    source 23
    target 477
  ]
  edge [
    source 23
    target 478
  ]
  edge [
    source 23
    target 479
  ]
  edge [
    source 23
    target 480
  ]
  edge [
    source 23
    target 481
  ]
  edge [
    source 23
    target 482
  ]
  edge [
    source 23
    target 483
  ]
  edge [
    source 23
    target 484
  ]
  edge [
    source 23
    target 485
  ]
  edge [
    source 23
    target 486
  ]
  edge [
    source 23
    target 27
  ]
  edge [
    source 23
    target 66
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 487
  ]
  edge [
    source 24
    target 488
  ]
  edge [
    source 24
    target 489
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 69
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 38
  ]
  edge [
    source 26
    target 39
  ]
  edge [
    source 26
    target 46
  ]
  edge [
    source 26
    target 476
  ]
  edge [
    source 26
    target 490
  ]
  edge [
    source 26
    target 491
  ]
  edge [
    source 26
    target 492
  ]
  edge [
    source 26
    target 493
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 35
  ]
  edge [
    source 27
    target 36
  ]
  edge [
    source 27
    target 494
  ]
  edge [
    source 27
    target 495
  ]
  edge [
    source 27
    target 496
  ]
  edge [
    source 27
    target 497
  ]
  edge [
    source 27
    target 498
  ]
  edge [
    source 27
    target 499
  ]
  edge [
    source 27
    target 500
  ]
  edge [
    source 27
    target 66
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 476
  ]
  edge [
    source 28
    target 501
  ]
  edge [
    source 28
    target 502
  ]
  edge [
    source 28
    target 503
  ]
  edge [
    source 28
    target 504
  ]
  edge [
    source 28
    target 505
  ]
  edge [
    source 28
    target 506
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 507
  ]
  edge [
    source 29
    target 508
  ]
  edge [
    source 29
    target 509
  ]
  edge [
    source 29
    target 510
  ]
  edge [
    source 29
    target 511
  ]
  edge [
    source 29
    target 512
  ]
  edge [
    source 29
    target 513
  ]
  edge [
    source 29
    target 514
  ]
  edge [
    source 29
    target 383
  ]
  edge [
    source 29
    target 515
  ]
  edge [
    source 29
    target 516
  ]
  edge [
    source 29
    target 517
  ]
  edge [
    source 29
    target 518
  ]
  edge [
    source 29
    target 519
  ]
  edge [
    source 29
    target 520
  ]
  edge [
    source 29
    target 521
  ]
  edge [
    source 29
    target 522
  ]
  edge [
    source 29
    target 523
  ]
  edge [
    source 29
    target 524
  ]
  edge [
    source 29
    target 525
  ]
  edge [
    source 29
    target 526
  ]
  edge [
    source 29
    target 527
  ]
  edge [
    source 29
    target 528
  ]
  edge [
    source 29
    target 529
  ]
  edge [
    source 29
    target 257
  ]
  edge [
    source 29
    target 462
  ]
  edge [
    source 29
    target 530
  ]
  edge [
    source 29
    target 531
  ]
  edge [
    source 29
    target 127
  ]
  edge [
    source 29
    target 532
  ]
  edge [
    source 29
    target 533
  ]
  edge [
    source 29
    target 534
  ]
  edge [
    source 29
    target 535
  ]
  edge [
    source 29
    target 536
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 537
  ]
  edge [
    source 30
    target 538
  ]
  edge [
    source 30
    target 539
  ]
  edge [
    source 30
    target 165
  ]
  edge [
    source 30
    target 540
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 541
  ]
  edge [
    source 31
    target 542
  ]
  edge [
    source 31
    target 543
  ]
  edge [
    source 31
    target 544
  ]
  edge [
    source 31
    target 545
  ]
  edge [
    source 31
    target 451
  ]
  edge [
    source 31
    target 546
  ]
  edge [
    source 31
    target 547
  ]
  edge [
    source 31
    target 548
  ]
  edge [
    source 31
    target 549
  ]
  edge [
    source 31
    target 550
  ]
  edge [
    source 31
    target 551
  ]
  edge [
    source 31
    target 453
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 552
  ]
  edge [
    source 32
    target 553
  ]
  edge [
    source 32
    target 554
  ]
  edge [
    source 32
    target 555
  ]
  edge [
    source 32
    target 556
  ]
  edge [
    source 32
    target 47
  ]
  edge [
    source 32
    target 57
  ]
  edge [
    source 32
    target 78
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 557
  ]
  edge [
    source 33
    target 558
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 559
  ]
  edge [
    source 34
    target 560
  ]
  edge [
    source 34
    target 561
  ]
  edge [
    source 34
    target 462
  ]
  edge [
    source 34
    target 562
  ]
  edge [
    source 34
    target 563
  ]
  edge [
    source 34
    target 564
  ]
  edge [
    source 34
    target 565
  ]
  edge [
    source 34
    target 566
  ]
  edge [
    source 34
    target 567
  ]
  edge [
    source 34
    target 568
  ]
  edge [
    source 34
    target 569
  ]
  edge [
    source 34
    target 570
  ]
  edge [
    source 34
    target 571
  ]
  edge [
    source 34
    target 463
  ]
  edge [
    source 34
    target 572
  ]
  edge [
    source 34
    target 573
  ]
  edge [
    source 34
    target 377
  ]
  edge [
    source 35
    target 68
  ]
  edge [
    source 35
    target 69
  ]
  edge [
    source 36
    target 574
  ]
  edge [
    source 36
    target 575
  ]
  edge [
    source 36
    target 576
  ]
  edge [
    source 36
    target 45
  ]
  edge [
    source 37
    target 577
  ]
  edge [
    source 37
    target 578
  ]
  edge [
    source 37
    target 579
  ]
  edge [
    source 37
    target 580
  ]
  edge [
    source 37
    target 581
  ]
  edge [
    source 37
    target 582
  ]
  edge [
    source 37
    target 583
  ]
  edge [
    source 37
    target 584
  ]
  edge [
    source 37
    target 585
  ]
  edge [
    source 37
    target 586
  ]
  edge [
    source 37
    target 587
  ]
  edge [
    source 38
    target 588
  ]
  edge [
    source 38
    target 589
  ]
  edge [
    source 38
    target 590
  ]
  edge [
    source 38
    target 591
  ]
  edge [
    source 38
    target 592
  ]
  edge [
    source 38
    target 593
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 42
  ]
  edge [
    source 39
    target 44
  ]
  edge [
    source 39
    target 45
  ]
  edge [
    source 39
    target 46
  ]
  edge [
    source 39
    target 62
  ]
  edge [
    source 39
    target 75
  ]
  edge [
    source 39
    target 76
  ]
  edge [
    source 39
    target 202
  ]
  edge [
    source 39
    target 594
  ]
  edge [
    source 39
    target 595
  ]
  edge [
    source 39
    target 56
  ]
  edge [
    source 39
    target 596
  ]
  edge [
    source 39
    target 597
  ]
  edge [
    source 39
    target 598
  ]
  edge [
    source 39
    target 599
  ]
  edge [
    source 39
    target 904
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 43
  ]
  edge [
    source 40
    target 574
  ]
  edge [
    source 40
    target 575
  ]
  edge [
    source 40
    target 576
  ]
  edge [
    source 40
    target 52
  ]
  edge [
    source 40
    target 58
  ]
  edge [
    source 41
    target 600
  ]
  edge [
    source 41
    target 601
  ]
  edge [
    source 41
    target 602
  ]
  edge [
    source 41
    target 603
  ]
  edge [
    source 41
    target 604
  ]
  edge [
    source 41
    target 605
  ]
  edge [
    source 41
    target 606
  ]
  edge [
    source 41
    target 607
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 63
  ]
  edge [
    source 42
    target 467
  ]
  edge [
    source 42
    target 608
  ]
  edge [
    source 42
    target 609
  ]
  edge [
    source 42
    target 610
  ]
  edge [
    source 42
    target 611
  ]
  edge [
    source 42
    target 612
  ]
  edge [
    source 42
    target 613
  ]
  edge [
    source 42
    target 614
  ]
  edge [
    source 42
    target 615
  ]
  edge [
    source 42
    target 616
  ]
  edge [
    source 42
    target 617
  ]
  edge [
    source 42
    target 618
  ]
  edge [
    source 42
    target 619
  ]
  edge [
    source 42
    target 620
  ]
  edge [
    source 42
    target 621
  ]
  edge [
    source 42
    target 622
  ]
  edge [
    source 42
    target 623
  ]
  edge [
    source 42
    target 624
  ]
  edge [
    source 42
    target 625
  ]
  edge [
    source 42
    target 626
  ]
  edge [
    source 42
    target 627
  ]
  edge [
    source 43
    target 628
  ]
  edge [
    source 43
    target 629
  ]
  edge [
    source 43
    target 630
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 631
  ]
  edge [
    source 44
    target 467
  ]
  edge [
    source 44
    target 632
  ]
  edge [
    source 44
    target 195
  ]
  edge [
    source 44
    target 633
  ]
  edge [
    source 45
    target 634
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 635
  ]
  edge [
    source 46
    target 636
  ]
  edge [
    source 46
    target 637
  ]
  edge [
    source 46
    target 638
  ]
  edge [
    source 46
    target 639
  ]
  edge [
    source 46
    target 640
  ]
  edge [
    source 46
    target 641
  ]
  edge [
    source 46
    target 642
  ]
  edge [
    source 46
    target 643
  ]
  edge [
    source 46
    target 644
  ]
  edge [
    source 46
    target 645
  ]
  edge [
    source 46
    target 646
  ]
  edge [
    source 46
    target 647
  ]
  edge [
    source 46
    target 239
  ]
  edge [
    source 46
    target 648
  ]
  edge [
    source 46
    target 649
  ]
  edge [
    source 46
    target 650
  ]
  edge [
    source 46
    target 651
  ]
  edge [
    source 46
    target 652
  ]
  edge [
    source 46
    target 653
  ]
  edge [
    source 46
    target 654
  ]
  edge [
    source 46
    target 54
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 649
  ]
  edge [
    source 47
    target 655
  ]
  edge [
    source 47
    target 656
  ]
  edge [
    source 47
    target 657
  ]
  edge [
    source 47
    target 658
  ]
  edge [
    source 47
    target 659
  ]
  edge [
    source 47
    target 660
  ]
  edge [
    source 47
    target 661
  ]
  edge [
    source 47
    target 662
  ]
  edge [
    source 47
    target 663
  ]
  edge [
    source 47
    target 664
  ]
  edge [
    source 47
    target 665
  ]
  edge [
    source 47
    target 57
  ]
  edge [
    source 47
    target 78
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 48
    target 53
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 666
  ]
  edge [
    source 48
    target 202
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 667
  ]
  edge [
    source 49
    target 668
  ]
  edge [
    source 49
    target 669
  ]
  edge [
    source 49
    target 670
  ]
  edge [
    source 49
    target 161
  ]
  edge [
    source 49
    target 671
  ]
  edge [
    source 49
    target 672
  ]
  edge [
    source 49
    target 673
  ]
  edge [
    source 49
    target 674
  ]
  edge [
    source 49
    target 64
  ]
  edge [
    source 49
    target 675
  ]
  edge [
    source 49
    target 676
  ]
  edge [
    source 49
    target 677
  ]
  edge [
    source 49
    target 678
  ]
  edge [
    source 49
    target 679
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 68
  ]
  edge [
    source 50
    target 680
  ]
  edge [
    source 50
    target 202
  ]
  edge [
    source 50
    target 681
  ]
  edge [
    source 50
    target 682
  ]
  edge [
    source 50
    target 161
  ]
  edge [
    source 50
    target 56
  ]
  edge [
    source 50
    target 683
  ]
  edge [
    source 50
    target 684
  ]
  edge [
    source 50
    target 685
  ]
  edge [
    source 50
    target 686
  ]
  edge [
    source 50
    target 687
  ]
  edge [
    source 51
    target 688
  ]
  edge [
    source 51
    target 689
  ]
  edge [
    source 51
    target 690
  ]
  edge [
    source 51
    target 691
  ]
  edge [
    source 51
    target 692
  ]
  edge [
    source 51
    target 693
  ]
  edge [
    source 52
    target 694
  ]
  edge [
    source 52
    target 695
  ]
  edge [
    source 52
    target 696
  ]
  edge [
    source 52
    target 697
  ]
  edge [
    source 52
    target 698
  ]
  edge [
    source 52
    target 699
  ]
  edge [
    source 52
    target 58
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 700
  ]
  edge [
    source 54
    target 467
  ]
  edge [
    source 54
    target 701
  ]
  edge [
    source 54
    target 702
  ]
  edge [
    source 54
    target 703
  ]
  edge [
    source 54
    target 704
  ]
  edge [
    source 54
    target 705
  ]
  edge [
    source 54
    target 706
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 707
  ]
  edge [
    source 55
    target 708
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 709
  ]
  edge [
    source 56
    target 710
  ]
  edge [
    source 56
    target 202
  ]
  edge [
    source 56
    target 711
  ]
  edge [
    source 56
    target 712
  ]
  edge [
    source 56
    target 713
  ]
  edge [
    source 56
    target 714
  ]
  edge [
    source 56
    target 596
  ]
  edge [
    source 56
    target 715
  ]
  edge [
    source 56
    target 597
  ]
  edge [
    source 56
    target 716
  ]
  edge [
    source 56
    target 717
  ]
  edge [
    source 56
    target 718
  ]
  edge [
    source 56
    target 719
  ]
  edge [
    source 56
    target 720
  ]
  edge [
    source 56
    target 721
  ]
  edge [
    source 56
    target 722
  ]
  edge [
    source 56
    target 723
  ]
  edge [
    source 56
    target 599
  ]
  edge [
    source 56
    target 724
  ]
  edge [
    source 56
    target 725
  ]
  edge [
    source 56
    target 726
  ]
  edge [
    source 56
    target 727
  ]
  edge [
    source 56
    target 728
  ]
  edge [
    source 56
    target 729
  ]
  edge [
    source 56
    target 730
  ]
  edge [
    source 56
    target 731
  ]
  edge [
    source 56
    target 732
  ]
  edge [
    source 56
    target 733
  ]
  edge [
    source 56
    target 734
  ]
  edge [
    source 56
    target 598
  ]
  edge [
    source 56
    target 735
  ]
  edge [
    source 56
    target 736
  ]
  edge [
    source 56
    target 737
  ]
  edge [
    source 56
    target 738
  ]
  edge [
    source 56
    target 739
  ]
  edge [
    source 56
    target 740
  ]
  edge [
    source 56
    target 595
  ]
  edge [
    source 56
    target 741
  ]
  edge [
    source 56
    target 742
  ]
  edge [
    source 56
    target 743
  ]
  edge [
    source 56
    target 744
  ]
  edge [
    source 56
    target 745
  ]
  edge [
    source 56
    target 746
  ]
  edge [
    source 56
    target 747
  ]
  edge [
    source 56
    target 748
  ]
  edge [
    source 56
    target 749
  ]
  edge [
    source 56
    target 750
  ]
  edge [
    source 56
    target 751
  ]
  edge [
    source 56
    target 752
  ]
  edge [
    source 56
    target 753
  ]
  edge [
    source 56
    target 754
  ]
  edge [
    source 56
    target 538
  ]
  edge [
    source 56
    target 755
  ]
  edge [
    source 56
    target 756
  ]
  edge [
    source 56
    target 757
  ]
  edge [
    source 56
    target 758
  ]
  edge [
    source 56
    target 759
  ]
  edge [
    source 56
    target 760
  ]
  edge [
    source 56
    target 761
  ]
  edge [
    source 56
    target 762
  ]
  edge [
    source 56
    target 763
  ]
  edge [
    source 56
    target 764
  ]
  edge [
    source 56
    target 765
  ]
  edge [
    source 56
    target 766
  ]
  edge [
    source 56
    target 767
  ]
  edge [
    source 57
    target 768
  ]
  edge [
    source 57
    target 769
  ]
  edge [
    source 57
    target 638
  ]
  edge [
    source 57
    target 770
  ]
  edge [
    source 57
    target 78
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 65
  ]
  edge [
    source 58
    target 771
  ]
  edge [
    source 58
    target 772
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 60
    target 773
  ]
  edge [
    source 60
    target 774
  ]
  edge [
    source 60
    target 775
  ]
  edge [
    source 60
    target 776
  ]
  edge [
    source 60
    target 777
  ]
  edge [
    source 60
    target 778
  ]
  edge [
    source 60
    target 779
  ]
  edge [
    source 60
    target 780
  ]
  edge [
    source 60
    target 781
  ]
  edge [
    source 60
    target 782
  ]
  edge [
    source 60
    target 67
  ]
  edge [
    source 61
    target 69
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 63
    target 783
  ]
  edge [
    source 64
    target 784
  ]
  edge [
    source 64
    target 785
  ]
  edge [
    source 64
    target 670
  ]
  edge [
    source 64
    target 786
  ]
  edge [
    source 64
    target 787
  ]
  edge [
    source 64
    target 788
  ]
  edge [
    source 64
    target 789
  ]
  edge [
    source 64
    target 790
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 791
  ]
  edge [
    source 65
    target 638
  ]
  edge [
    source 65
    target 792
  ]
  edge [
    source 65
    target 793
  ]
  edge [
    source 65
    target 794
  ]
  edge [
    source 65
    target 607
  ]
  edge [
    source 65
    target 795
  ]
  edge [
    source 65
    target 687
  ]
  edge [
    source 65
    target 796
  ]
  edge [
    source 66
    target 797
  ]
  edge [
    source 66
    target 798
  ]
  edge [
    source 66
    target 799
  ]
  edge [
    source 67
    target 78
  ]
  edge [
    source 67
    target 800
  ]
  edge [
    source 67
    target 467
  ]
  edge [
    source 68
    target 801
  ]
  edge [
    source 68
    target 802
  ]
  edge [
    source 69
    target 118
  ]
  edge [
    source 69
    target 538
  ]
  edge [
    source 69
    target 803
  ]
  edge [
    source 69
    target 127
  ]
  edge [
    source 69
    target 804
  ]
  edge [
    source 69
    target 805
  ]
  edge [
    source 69
    target 71
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 806
  ]
  edge [
    source 70
    target 807
  ]
  edge [
    source 70
    target 808
  ]
  edge [
    source 70
    target 809
  ]
  edge [
    source 70
    target 810
  ]
  edge [
    source 70
    target 811
  ]
  edge [
    source 70
    target 812
  ]
  edge [
    source 70
    target 813
  ]
  edge [
    source 70
    target 814
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 815
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 74
    target 75
  ]
  edge [
    source 74
    target 816
  ]
  edge [
    source 74
    target 817
  ]
  edge [
    source 74
    target 818
  ]
  edge [
    source 74
    target 819
  ]
  edge [
    source 74
    target 820
  ]
  edge [
    source 74
    target 821
  ]
  edge [
    source 74
    target 822
  ]
  edge [
    source 74
    target 823
  ]
  edge [
    source 74
    target 824
  ]
  edge [
    source 74
    target 825
  ]
  edge [
    source 74
    target 826
  ]
  edge [
    source 74
    target 827
  ]
  edge [
    source 74
    target 828
  ]
  edge [
    source 74
    target 829
  ]
  edge [
    source 74
    target 830
  ]
  edge [
    source 74
    target 831
  ]
  edge [
    source 74
    target 832
  ]
  edge [
    source 74
    target 833
  ]
  edge [
    source 74
    target 834
  ]
  edge [
    source 74
    target 835
  ]
  edge [
    source 74
    target 836
  ]
  edge [
    source 74
    target 837
  ]
  edge [
    source 74
    target 838
  ]
  edge [
    source 74
    target 839
  ]
  edge [
    source 74
    target 840
  ]
  edge [
    source 74
    target 841
  ]
  edge [
    source 74
    target 842
  ]
  edge [
    source 74
    target 843
  ]
  edge [
    source 74
    target 118
  ]
  edge [
    source 74
    target 538
  ]
  edge [
    source 74
    target 844
  ]
  edge [
    source 74
    target 845
  ]
  edge [
    source 74
    target 846
  ]
  edge [
    source 74
    target 847
  ]
  edge [
    source 74
    target 848
  ]
  edge [
    source 74
    target 849
  ]
  edge [
    source 74
    target 850
  ]
  edge [
    source 74
    target 851
  ]
  edge [
    source 74
    target 852
  ]
  edge [
    source 74
    target 853
  ]
  edge [
    source 74
    target 83
  ]
  edge [
    source 75
    target 854
  ]
  edge [
    source 75
    target 855
  ]
  edge [
    source 76
    target 77
  ]
  edge [
    source 76
    target 856
  ]
  edge [
    source 76
    target 857
  ]
  edge [
    source 77
    target 858
  ]
  edge [
    source 77
    target 859
  ]
  edge [
    source 77
    target 860
  ]
  edge [
    source 77
    target 212
  ]
  edge [
    source 77
    target 861
  ]
  edge [
    source 77
    target 862
  ]
  edge [
    source 77
    target 863
  ]
  edge [
    source 77
    target 864
  ]
  edge [
    source 77
    target 865
  ]
  edge [
    source 77
    target 866
  ]
  edge [
    source 77
    target 80
  ]
  edge [
    source 78
    target 79
  ]
  edge [
    source 78
    target 867
  ]
  edge [
    source 78
    target 868
  ]
  edge [
    source 78
    target 869
  ]
  edge [
    source 78
    target 870
  ]
  edge [
    source 78
    target 871
  ]
  edge [
    source 78
    target 872
  ]
  edge [
    source 78
    target 873
  ]
  edge [
    source 78
    target 874
  ]
  edge [
    source 79
    target 80
  ]
  edge [
    source 79
    target 875
  ]
  edge [
    source 79
    target 876
  ]
  edge [
    source 79
    target 877
  ]
  edge [
    source 79
    target 202
  ]
  edge [
    source 79
    target 878
  ]
  edge [
    source 79
    target 879
  ]
  edge [
    source 79
    target 880
  ]
  edge [
    source 79
    target 881
  ]
  edge [
    source 79
    target 882
  ]
  edge [
    source 79
    target 883
  ]
  edge [
    source 79
    target 884
  ]
  edge [
    source 79
    target 165
  ]
  edge [
    source 79
    target 885
  ]
  edge [
    source 79
    target 886
  ]
  edge [
    source 80
    target 81
  ]
  edge [
    source 80
    target 887
  ]
  edge [
    source 80
    target 888
  ]
  edge [
    source 80
    target 889
  ]
  edge [
    source 80
    target 890
  ]
  edge [
    source 80
    target 891
  ]
  edge [
    source 80
    target 892
  ]
  edge [
    source 80
    target 893
  ]
  edge [
    source 80
    target 894
  ]
  edge [
    source 81
    target 82
  ]
  edge [
    source 81
    target 895
  ]
  edge [
    source 81
    target 871
  ]
  edge [
    source 81
    target 896
  ]
  edge [
    source 81
    target 897
  ]
  edge [
    source 82
    target 83
  ]
  edge [
    source 82
    target 898
  ]
  edge [
    source 82
    target 899
  ]
  edge [
    source 82
    target 900
  ]
  edge [
    source 82
    target 691
  ]
  edge [
    source 82
    target 901
  ]
  edge [
    source 82
    target 902
  ]
  edge [
    source 83
    target 903
  ]
  edge [
    source 905
    target 906
  ]
]
