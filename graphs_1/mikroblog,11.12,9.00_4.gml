graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9375
  density 0.0625
  graphCliqueNumber 2
  node [
    id 0
    label "mama"
    origin "text"
  ]
  node [
    id 1
    label "pytanie"
    origin "text"
  ]
  node [
    id 2
    label "zwiazki"
    origin "text"
  ]
  node [
    id 3
    label "logikarozowychpaskow"
    origin "text"
  ]
  node [
    id 4
    label "matczysko"
  ]
  node [
    id 5
    label "macierz"
  ]
  node [
    id 6
    label "przodkini"
  ]
  node [
    id 7
    label "Matka_Boska"
  ]
  node [
    id 8
    label "macocha"
  ]
  node [
    id 9
    label "matka_zast&#281;pcza"
  ]
  node [
    id 10
    label "stara"
  ]
  node [
    id 11
    label "rodzice"
  ]
  node [
    id 12
    label "rodzic"
  ]
  node [
    id 13
    label "sprawa"
  ]
  node [
    id 14
    label "zadanie"
  ]
  node [
    id 15
    label "wypowied&#378;"
  ]
  node [
    id 16
    label "problemat"
  ]
  node [
    id 17
    label "rozpytywanie"
  ]
  node [
    id 18
    label "sprawdzian"
  ]
  node [
    id 19
    label "przes&#322;uchiwanie"
  ]
  node [
    id 20
    label "wypytanie"
  ]
  node [
    id 21
    label "zwracanie_si&#281;"
  ]
  node [
    id 22
    label "wypowiedzenie"
  ]
  node [
    id 23
    label "wywo&#322;ywanie"
  ]
  node [
    id 24
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 25
    label "problematyka"
  ]
  node [
    id 26
    label "question"
  ]
  node [
    id 27
    label "sprawdzanie"
  ]
  node [
    id 28
    label "odpowiadanie"
  ]
  node [
    id 29
    label "survey"
  ]
  node [
    id 30
    label "odpowiada&#263;"
  ]
  node [
    id 31
    label "egzaminowanie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
]
