graph [
  maxDegree 22
  minDegree 1
  meanDegree 2
  density 0.016129032258064516
  graphCliqueNumber 3
  node [
    id 0
    label "ksi&#261;&#380;&#281;"
    origin "text"
  ]
  node [
    id 1
    label "koronny"
    origin "text"
  ]
  node [
    id 2
    label "arabia"
    origin "text"
  ]
  node [
    id 3
    label "saudyjski"
    origin "text"
  ]
  node [
    id 4
    label "mohammad"
    origin "text"
  ]
  node [
    id 5
    label "bin"
    origin "text"
  ]
  node [
    id 6
    label "salman"
    origin "text"
  ]
  node [
    id 7
    label "wys&#322;a&#263;"
    origin "text"
  ]
  node [
    id 8
    label "przynajmniej"
    origin "text"
  ]
  node [
    id 9
    label "wiadomo&#347;ci"
    origin "text"
  ]
  node [
    id 10
    label "swoje"
    origin "text"
  ]
  node [
    id 11
    label "doradca"
    origin "text"
  ]
  node [
    id 12
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 13
    label "nadzorowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "operacja"
    origin "text"
  ]
  node [
    id 15
    label "morderstwo"
    origin "text"
  ]
  node [
    id 16
    label "dziennikarz"
    origin "text"
  ]
  node [
    id 17
    label "d&#380;amala"
    origin "text"
  ]
  node [
    id 18
    label "chaszukd&#380;iego"
    origin "text"
  ]
  node [
    id 19
    label "zaledwie"
    origin "text"
  ]
  node [
    id 20
    label "kilka"
    origin "text"
  ]
  node [
    id 21
    label "godzina"
    origin "text"
  ]
  node [
    id 22
    label "przed"
    origin "text"
  ]
  node [
    id 23
    label "zbrodnia"
    origin "text"
  ]
  node [
    id 24
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "wall"
    origin "text"
  ]
  node [
    id 26
    label "street"
    origin "text"
  ]
  node [
    id 27
    label "journal"
    origin "text"
  ]
  node [
    id 28
    label "powo&#322;ywa&#263;"
    origin "text"
  ]
  node [
    id 29
    label "si&#281;"
    origin "text"
  ]
  node [
    id 30
    label "dokument"
    origin "text"
  ]
  node [
    id 31
    label "Hamlet"
  ]
  node [
    id 32
    label "Kazimierz_II_Sprawiedliwy"
  ]
  node [
    id 33
    label "Piast"
  ]
  node [
    id 34
    label "arystokrata"
  ]
  node [
    id 35
    label "tytu&#322;"
  ]
  node [
    id 36
    label "kochanie"
  ]
  node [
    id 37
    label "Bismarck"
  ]
  node [
    id 38
    label "Herman"
  ]
  node [
    id 39
    label "Mieszko_I"
  ]
  node [
    id 40
    label "w&#322;adca"
  ]
  node [
    id 41
    label "fircyk"
  ]
  node [
    id 42
    label "najwa&#380;niejszy"
  ]
  node [
    id 43
    label "arabski"
  ]
  node [
    id 44
    label "wytworzy&#263;"
  ]
  node [
    id 45
    label "line"
  ]
  node [
    id 46
    label "ship"
  ]
  node [
    id 47
    label "convey"
  ]
  node [
    id 48
    label "przekaza&#263;"
  ]
  node [
    id 49
    label "post"
  ]
  node [
    id 50
    label "wy&#322;o&#380;y&#263;"
  ]
  node [
    id 51
    label "nakaza&#263;"
  ]
  node [
    id 52
    label "czynnik"
  ]
  node [
    id 53
    label "pomocnik"
  ]
  node [
    id 54
    label "radziciel"
  ]
  node [
    id 55
    label "manipulate"
  ]
  node [
    id 56
    label "pracowa&#263;"
  ]
  node [
    id 57
    label "infimum"
  ]
  node [
    id 58
    label "laparotomia"
  ]
  node [
    id 59
    label "chirurg"
  ]
  node [
    id 60
    label "manewr_Pringle&#8217;a"
  ]
  node [
    id 61
    label "supremum"
  ]
  node [
    id 62
    label "torakotomia"
  ]
  node [
    id 63
    label "funkcja"
  ]
  node [
    id 64
    label "strategia"
  ]
  node [
    id 65
    label "szew"
  ]
  node [
    id 66
    label "rzut"
  ]
  node [
    id 67
    label "liczenie"
  ]
  node [
    id 68
    label "ob&#322;awa_augustowska"
  ]
  node [
    id 69
    label "proces_my&#347;lowy"
  ]
  node [
    id 70
    label "czyn"
  ]
  node [
    id 71
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 72
    label "matematyka"
  ]
  node [
    id 73
    label "zabieg"
  ]
  node [
    id 74
    label "mathematical_process"
  ]
  node [
    id 75
    label "liczy&#263;"
  ]
  node [
    id 76
    label "jednostka"
  ]
  node [
    id 77
    label "zabicie"
  ]
  node [
    id 78
    label "przest&#281;pstwo"
  ]
  node [
    id 79
    label "nowiniarz"
  ]
  node [
    id 80
    label "akredytowa&#263;"
  ]
  node [
    id 81
    label "akredytowanie"
  ]
  node [
    id 82
    label "bran&#380;owiec"
  ]
  node [
    id 83
    label "publicysta"
  ]
  node [
    id 84
    label "&#347;ledziowate"
  ]
  node [
    id 85
    label "ryba"
  ]
  node [
    id 86
    label "minuta"
  ]
  node [
    id 87
    label "doba"
  ]
  node [
    id 88
    label "czas"
  ]
  node [
    id 89
    label "p&#243;&#322;godzina"
  ]
  node [
    id 90
    label "kwadrans"
  ]
  node [
    id 91
    label "time"
  ]
  node [
    id 92
    label "jednostka_czasu"
  ]
  node [
    id 93
    label "crime"
  ]
  node [
    id 94
    label "post&#281;pek"
  ]
  node [
    id 95
    label "komunikowa&#263;"
  ]
  node [
    id 96
    label "powiada&#263;"
  ]
  node [
    id 97
    label "inform"
  ]
  node [
    id 98
    label "facebook"
  ]
  node [
    id 99
    label "profil"
  ]
  node [
    id 100
    label "wskazywa&#263;"
  ]
  node [
    id 101
    label "poborowy"
  ]
  node [
    id 102
    label "call"
  ]
  node [
    id 103
    label "record"
  ]
  node [
    id 104
    label "wytw&#243;r"
  ]
  node [
    id 105
    label "&#347;wiadectwo"
  ]
  node [
    id 106
    label "zapis"
  ]
  node [
    id 107
    label "fascyku&#322;"
  ]
  node [
    id 108
    label "raport&#243;wka"
  ]
  node [
    id 109
    label "wypis_&#378;r&#243;d&#322;owy"
  ]
  node [
    id 110
    label "artyku&#322;"
  ]
  node [
    id 111
    label "plik"
  ]
  node [
    id 112
    label "writing"
  ]
  node [
    id 113
    label "utw&#243;r"
  ]
  node [
    id 114
    label "dokumentacja"
  ]
  node [
    id 115
    label "parafa"
  ]
  node [
    id 116
    label "sygnatariusz"
  ]
  node [
    id 117
    label "registratura"
  ]
  node [
    id 118
    label "Arabia"
  ]
  node [
    id 119
    label "D&#380;amala"
  ]
  node [
    id 120
    label "Chaszukd&#380;iego"
  ]
  node [
    id 121
    label "Wall"
  ]
  node [
    id 122
    label "Street"
  ]
  node [
    id 123
    label "Journal"
  ]
  node [
    id 124
    label "Mohammad"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 118
  ]
  edge [
    source 3
    target 124
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 52
  ]
  edge [
    source 11
    target 53
  ]
  edge [
    source 11
    target 54
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 55
  ]
  edge [
    source 13
    target 56
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 57
  ]
  edge [
    source 14
    target 58
  ]
  edge [
    source 14
    target 59
  ]
  edge [
    source 14
    target 60
  ]
  edge [
    source 14
    target 61
  ]
  edge [
    source 14
    target 62
  ]
  edge [
    source 14
    target 63
  ]
  edge [
    source 14
    target 64
  ]
  edge [
    source 14
    target 65
  ]
  edge [
    source 14
    target 66
  ]
  edge [
    source 14
    target 67
  ]
  edge [
    source 14
    target 68
  ]
  edge [
    source 14
    target 69
  ]
  edge [
    source 14
    target 70
  ]
  edge [
    source 14
    target 71
  ]
  edge [
    source 14
    target 72
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 75
  ]
  edge [
    source 14
    target 76
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 77
  ]
  edge [
    source 15
    target 78
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 79
  ]
  edge [
    source 16
    target 80
  ]
  edge [
    source 16
    target 81
  ]
  edge [
    source 16
    target 82
  ]
  edge [
    source 16
    target 83
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 84
  ]
  edge [
    source 20
    target 85
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 86
  ]
  edge [
    source 21
    target 87
  ]
  edge [
    source 21
    target 88
  ]
  edge [
    source 21
    target 89
  ]
  edge [
    source 21
    target 90
  ]
  edge [
    source 21
    target 91
  ]
  edge [
    source 21
    target 92
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 93
  ]
  edge [
    source 23
    target 78
  ]
  edge [
    source 23
    target 94
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 95
  ]
  edge [
    source 24
    target 96
  ]
  edge [
    source 24
    target 97
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 98
  ]
  edge [
    source 25
    target 99
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 100
  ]
  edge [
    source 28
    target 101
  ]
  edge [
    source 28
    target 102
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 103
  ]
  edge [
    source 30
    target 104
  ]
  edge [
    source 30
    target 105
  ]
  edge [
    source 30
    target 106
  ]
  edge [
    source 30
    target 107
  ]
  edge [
    source 30
    target 108
  ]
  edge [
    source 30
    target 109
  ]
  edge [
    source 30
    target 110
  ]
  edge [
    source 30
    target 111
  ]
  edge [
    source 30
    target 112
  ]
  edge [
    source 30
    target 113
  ]
  edge [
    source 30
    target 114
  ]
  edge [
    source 30
    target 115
  ]
  edge [
    source 30
    target 116
  ]
  edge [
    source 30
    target 117
  ]
  edge [
    source 118
    target 124
  ]
  edge [
    source 119
    target 120
  ]
  edge [
    source 121
    target 122
  ]
  edge [
    source 121
    target 123
  ]
  edge [
    source 122
    target 123
  ]
]
