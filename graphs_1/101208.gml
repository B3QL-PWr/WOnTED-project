graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.7894736842105263
  density 0.09941520467836257
  graphCliqueNumber 2
  node [
    id 0
    label "promieniowanie"
    origin "text"
  ]
  node [
    id 1
    label "radiation"
  ]
  node [
    id 2
    label "wysy&#322;anie"
  ]
  node [
    id 3
    label "energia"
  ]
  node [
    id 4
    label "radiesteta"
  ]
  node [
    id 5
    label "rozchodzenie_si&#281;"
  ]
  node [
    id 6
    label "oddzia&#322;ywanie"
  ]
  node [
    id 7
    label "aktynometr"
  ]
  node [
    id 8
    label "radiance"
  ]
  node [
    id 9
    label "emanowanie"
  ]
  node [
    id 10
    label "irradiacja"
  ]
  node [
    id 11
    label "narz&#261;d_krytyczny"
  ]
  node [
    id 12
    label "emission"
  ]
  node [
    id 13
    label "radiotherapy"
  ]
  node [
    id 14
    label "zjawisko"
  ]
  node [
    id 15
    label "wypromieniowywanie"
  ]
  node [
    id 16
    label "promie&#324;"
  ]
  node [
    id 17
    label "Roentgen"
  ]
  node [
    id 18
    label "ksi&#261;&#380;&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 18
  ]
]
