graph [
  maxDegree 32
  minDegree 1
  meanDegree 1.9754601226993864
  density 0.012194198288267817
  graphCliqueNumber 2
  node [
    id 0
    label "&#347;wie&#380;y"
    origin "text"
  ]
  node [
    id 1
    label "sprawa"
    origin "text"
  ]
  node [
    id 2
    label "sprzed"
    origin "text"
  ]
  node [
    id 3
    label "godzina"
    origin "text"
  ]
  node [
    id 4
    label "tomasz"
    origin "text"
  ]
  node [
    id 5
    label "sekielski"
    origin "text"
  ]
  node [
    id 6
    label "przygotowywa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "film"
    origin "text"
  ]
  node [
    id 8
    label "pedofilia"
    origin "text"
  ]
  node [
    id 9
    label "polski"
    origin "text"
  ]
  node [
    id 10
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 11
    label "zaczyna&#263;"
    origin "text"
  ]
  node [
    id 12
    label "mie&#263;"
    origin "text"
  ]
  node [
    id 13
    label "pod"
    origin "text"
  ]
  node [
    id 14
    label "g&#243;rka"
    origin "text"
  ]
  node [
    id 15
    label "orze&#378;wiaj&#261;co"
  ]
  node [
    id 16
    label "jasny"
  ]
  node [
    id 17
    label "&#347;wie&#380;o"
  ]
  node [
    id 18
    label "orze&#378;wienie"
  ]
  node [
    id 19
    label "o&#380;ywczy"
  ]
  node [
    id 20
    label "soczysty"
  ]
  node [
    id 21
    label "inny"
  ]
  node [
    id 22
    label "nowotny"
  ]
  node [
    id 23
    label "przyjemny"
  ]
  node [
    id 24
    label "zdrowy"
  ]
  node [
    id 25
    label "&#380;ywy"
  ]
  node [
    id 26
    label "czysty"
  ]
  node [
    id 27
    label "orze&#378;wianie"
  ]
  node [
    id 28
    label "oryginalnie"
  ]
  node [
    id 29
    label "m&#322;ody"
  ]
  node [
    id 30
    label "energiczny"
  ]
  node [
    id 31
    label "nowy"
  ]
  node [
    id 32
    label "dobry"
  ]
  node [
    id 33
    label "rze&#347;ki"
  ]
  node [
    id 34
    label "surowy"
  ]
  node [
    id 35
    label "temat"
  ]
  node [
    id 36
    label "kognicja"
  ]
  node [
    id 37
    label "idea"
  ]
  node [
    id 38
    label "szczeg&#243;&#322;"
  ]
  node [
    id 39
    label "rzecz"
  ]
  node [
    id 40
    label "wydarzenie"
  ]
  node [
    id 41
    label "przes&#322;anka"
  ]
  node [
    id 42
    label "rozprawa"
  ]
  node [
    id 43
    label "object"
  ]
  node [
    id 44
    label "proposition"
  ]
  node [
    id 45
    label "minuta"
  ]
  node [
    id 46
    label "doba"
  ]
  node [
    id 47
    label "czas"
  ]
  node [
    id 48
    label "p&#243;&#322;godzina"
  ]
  node [
    id 49
    label "kwadrans"
  ]
  node [
    id 50
    label "time"
  ]
  node [
    id 51
    label "jednostka_czasu"
  ]
  node [
    id 52
    label "wykonywa&#263;"
  ]
  node [
    id 53
    label "sposobi&#263;"
  ]
  node [
    id 54
    label "arrange"
  ]
  node [
    id 55
    label "pryczy&#263;"
  ]
  node [
    id 56
    label "train"
  ]
  node [
    id 57
    label "robi&#263;"
  ]
  node [
    id 58
    label "wytwarza&#263;"
  ]
  node [
    id 59
    label "szkoli&#263;"
  ]
  node [
    id 60
    label "usposabia&#263;"
  ]
  node [
    id 61
    label "rozbieg&#243;wka"
  ]
  node [
    id 62
    label "block"
  ]
  node [
    id 63
    label "odczula&#263;"
  ]
  node [
    id 64
    label "blik"
  ]
  node [
    id 65
    label "rola"
  ]
  node [
    id 66
    label "trawiarnia"
  ]
  node [
    id 67
    label "b&#322;ona"
  ]
  node [
    id 68
    label "filmoteka"
  ]
  node [
    id 69
    label "sztuka"
  ]
  node [
    id 70
    label "muza"
  ]
  node [
    id 71
    label "odczuli&#263;"
  ]
  node [
    id 72
    label "klatka"
  ]
  node [
    id 73
    label "odczulenie"
  ]
  node [
    id 74
    label "emulsja_fotograficzna"
  ]
  node [
    id 75
    label "animatronika"
  ]
  node [
    id 76
    label "dorobek"
  ]
  node [
    id 77
    label "odczulanie"
  ]
  node [
    id 78
    label "scena"
  ]
  node [
    id 79
    label "czo&#322;&#243;wka"
  ]
  node [
    id 80
    label "ty&#322;&#243;wka"
  ]
  node [
    id 81
    label "napisy"
  ]
  node [
    id 82
    label "photograph"
  ]
  node [
    id 83
    label "&#347;cie&#380;ka_d&#378;wi&#281;kowa"
  ]
  node [
    id 84
    label "postprodukcja"
  ]
  node [
    id 85
    label "sklejarka"
  ]
  node [
    id 86
    label "anamorfoza"
  ]
  node [
    id 87
    label "utw&#243;r_audiowizualny"
  ]
  node [
    id 88
    label "ta&#347;ma"
  ]
  node [
    id 89
    label "b&#322;ona_&#347;wiat&#322;oczu&#322;a"
  ]
  node [
    id 90
    label "uj&#281;cie"
  ]
  node [
    id 91
    label "pedophilia"
  ]
  node [
    id 92
    label "przest&#281;pstwo"
  ]
  node [
    id 93
    label "parafilia"
  ]
  node [
    id 94
    label "lacki"
  ]
  node [
    id 95
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 96
    label "przedmiot"
  ]
  node [
    id 97
    label "sztajer"
  ]
  node [
    id 98
    label "drabant"
  ]
  node [
    id 99
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 100
    label "polak"
  ]
  node [
    id 101
    label "pierogi_ruskie"
  ]
  node [
    id 102
    label "krakowiak"
  ]
  node [
    id 103
    label "Polish"
  ]
  node [
    id 104
    label "j&#281;zyk"
  ]
  node [
    id 105
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 106
    label "oberek"
  ]
  node [
    id 107
    label "po_polsku"
  ]
  node [
    id 108
    label "mazur"
  ]
  node [
    id 109
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 110
    label "chodzony"
  ]
  node [
    id 111
    label "skoczny"
  ]
  node [
    id 112
    label "ryba_po_grecku"
  ]
  node [
    id 113
    label "goniony"
  ]
  node [
    id 114
    label "polsko"
  ]
  node [
    id 115
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 116
    label "zakrystia"
  ]
  node [
    id 117
    label "organizacja_religijna"
  ]
  node [
    id 118
    label "nawa"
  ]
  node [
    id 119
    label "nerwica_eklezjogenna"
  ]
  node [
    id 120
    label "kropielnica"
  ]
  node [
    id 121
    label "prezbiterium"
  ]
  node [
    id 122
    label "wsp&#243;lnota"
  ]
  node [
    id 123
    label "church"
  ]
  node [
    id 124
    label "kruchta"
  ]
  node [
    id 125
    label "Ska&#322;ka"
  ]
  node [
    id 126
    label "kult"
  ]
  node [
    id 127
    label "ub&#322;agalnia"
  ]
  node [
    id 128
    label "dom"
  ]
  node [
    id 129
    label "open"
  ]
  node [
    id 130
    label "odejmowa&#263;"
  ]
  node [
    id 131
    label "mie&#263;_miejsce"
  ]
  node [
    id 132
    label "robi&#263;_pierwszy_krok"
  ]
  node [
    id 133
    label "set_about"
  ]
  node [
    id 134
    label "begin"
  ]
  node [
    id 135
    label "post&#281;powa&#263;"
  ]
  node [
    id 136
    label "bankrupt"
  ]
  node [
    id 137
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 138
    label "czu&#263;"
  ]
  node [
    id 139
    label "need"
  ]
  node [
    id 140
    label "hide"
  ]
  node [
    id 141
    label "support"
  ]
  node [
    id 142
    label "zwy&#380;ka"
  ]
  node [
    id 143
    label "akrobacja_lotnicza"
  ]
  node [
    id 144
    label "nadmiar"
  ]
  node [
    id 145
    label "kozina"
  ]
  node [
    id 146
    label "wzniesienie"
  ]
  node [
    id 147
    label "ozdoba"
  ]
  node [
    id 148
    label "Wawel"
  ]
  node [
    id 149
    label "mocowanie"
  ]
  node [
    id 150
    label "Eskwilin"
  ]
  node [
    id 151
    label "Kapitol"
  ]
  node [
    id 152
    label "Awentyn"
  ]
  node [
    id 153
    label "Kwiryna&#322;"
  ]
  node [
    id 154
    label "Syjon"
  ]
  node [
    id 155
    label "Palatyn"
  ]
  node [
    id 156
    label "tusza"
  ]
  node [
    id 157
    label "bi&#380;uteria"
  ]
  node [
    id 158
    label "ciel&#281;cina"
  ]
  node [
    id 159
    label "baranina"
  ]
  node [
    id 160
    label "mi&#281;so"
  ]
  node [
    id 161
    label "Tomasz"
  ]
  node [
    id 162
    label "Sekielski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 9
    target 100
  ]
  edge [
    source 9
    target 101
  ]
  edge [
    source 9
    target 102
  ]
  edge [
    source 9
    target 103
  ]
  edge [
    source 9
    target 104
  ]
  edge [
    source 9
    target 105
  ]
  edge [
    source 9
    target 106
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 10
    target 118
  ]
  edge [
    source 10
    target 119
  ]
  edge [
    source 10
    target 120
  ]
  edge [
    source 10
    target 121
  ]
  edge [
    source 10
    target 122
  ]
  edge [
    source 10
    target 123
  ]
  edge [
    source 10
    target 124
  ]
  edge [
    source 10
    target 125
  ]
  edge [
    source 10
    target 126
  ]
  edge [
    source 10
    target 127
  ]
  edge [
    source 10
    target 128
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 14
    target 150
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 161
    target 162
  ]
]
