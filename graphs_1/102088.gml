graph [
  maxDegree 23
  minDegree 1
  meanDegree 2
  density 0.020202020202020204
  graphCliqueNumber 2
  node [
    id 0
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 1
    label "boisko"
    origin "text"
  ]
  node [
    id 2
    label "bzura"
    origin "text"
  ]
  node [
    id 3
    label "podejmowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "zespoli&#263;"
    origin "text"
  ]
  node [
    id 5
    label "aleksandr&#243;w"
    origin "text"
  ]
  node [
    id 6
    label "&#322;&#243;dzkie"
    origin "text"
  ]
  node [
    id 7
    label "ten"
    origin "text"
  ]
  node [
    id 8
    label "mecz"
    origin "text"
  ]
  node [
    id 9
    label "zako&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 10
    label "wynik"
    origin "text"
  ]
  node [
    id 11
    label "dla"
    origin "text"
  ]
  node [
    id 12
    label "nasi"
    origin "text"
  ]
  node [
    id 13
    label "pi&#322;karz"
    origin "text"
  ]
  node [
    id 14
    label "zagra&#263;"
    origin "text"
  ]
  node [
    id 15
    label "bardzo"
    origin "text"
  ]
  node [
    id 16
    label "dobrze"
    origin "text"
  ]
  node [
    id 17
    label "doba"
  ]
  node [
    id 18
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 19
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 20
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 21
    label "ziemia"
  ]
  node [
    id 22
    label "pole"
  ]
  node [
    id 23
    label "linia"
  ]
  node [
    id 24
    label "obiekt"
  ]
  node [
    id 25
    label "skrzyd&#322;o"
  ]
  node [
    id 26
    label "aut"
  ]
  node [
    id 27
    label "bojo"
  ]
  node [
    id 28
    label "bojowisko"
  ]
  node [
    id 29
    label "budowla"
  ]
  node [
    id 30
    label "zmienia&#263;"
  ]
  node [
    id 31
    label "reagowa&#263;"
  ]
  node [
    id 32
    label "rise"
  ]
  node [
    id 33
    label "admit"
  ]
  node [
    id 34
    label "drive"
  ]
  node [
    id 35
    label "robi&#263;"
  ]
  node [
    id 36
    label "draw"
  ]
  node [
    id 37
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 38
    label "podnosi&#263;"
  ]
  node [
    id 39
    label "po&#322;&#261;czy&#263;"
  ]
  node [
    id 40
    label "connect"
  ]
  node [
    id 41
    label "okre&#347;lony"
  ]
  node [
    id 42
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 43
    label "obrona"
  ]
  node [
    id 44
    label "gra"
  ]
  node [
    id 45
    label "dwumecz"
  ]
  node [
    id 46
    label "game"
  ]
  node [
    id 47
    label "serw"
  ]
  node [
    id 48
    label "communicate"
  ]
  node [
    id 49
    label "cause"
  ]
  node [
    id 50
    label "zrezygnowa&#263;"
  ]
  node [
    id 51
    label "wytworzy&#263;"
  ]
  node [
    id 52
    label "przesta&#263;"
  ]
  node [
    id 53
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 54
    label "dispose"
  ]
  node [
    id 55
    label "zrobi&#263;"
  ]
  node [
    id 56
    label "typ"
  ]
  node [
    id 57
    label "dzia&#322;anie"
  ]
  node [
    id 58
    label "przyczyna"
  ]
  node [
    id 59
    label "zaokr&#261;gli&#263;"
  ]
  node [
    id 60
    label "liczba_w_dzia&#322;aniu"
  ]
  node [
    id 61
    label "zaokr&#261;glenie"
  ]
  node [
    id 62
    label "event"
  ]
  node [
    id 63
    label "rezultat"
  ]
  node [
    id 64
    label "gracz"
  ]
  node [
    id 65
    label "legionista"
  ]
  node [
    id 66
    label "sportowiec"
  ]
  node [
    id 67
    label "Daniel_Dubicki"
  ]
  node [
    id 68
    label "typify"
  ]
  node [
    id 69
    label "flare"
  ]
  node [
    id 70
    label "zaszczeka&#263;"
  ]
  node [
    id 71
    label "rola"
  ]
  node [
    id 72
    label "wykona&#263;"
  ]
  node [
    id 73
    label "wykorzysta&#263;"
  ]
  node [
    id 74
    label "zacz&#261;&#263;"
  ]
  node [
    id 75
    label "zab&#322;yszcze&#263;"
  ]
  node [
    id 76
    label "zatokowa&#263;"
  ]
  node [
    id 77
    label "zabrzmie&#263;"
  ]
  node [
    id 78
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 79
    label "leave"
  ]
  node [
    id 80
    label "uda&#263;_si&#281;"
  ]
  node [
    id 81
    label "uzewn&#281;trzni&#263;_si&#281;"
  ]
  node [
    id 82
    label "instrument_muzyczny"
  ]
  node [
    id 83
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 84
    label "play"
  ]
  node [
    id 85
    label "sound"
  ]
  node [
    id 86
    label "represent"
  ]
  node [
    id 87
    label "rozegra&#263;"
  ]
  node [
    id 88
    label "w_chuj"
  ]
  node [
    id 89
    label "moralnie"
  ]
  node [
    id 90
    label "wiele"
  ]
  node [
    id 91
    label "lepiej"
  ]
  node [
    id 92
    label "korzystnie"
  ]
  node [
    id 93
    label "pomy&#347;lnie"
  ]
  node [
    id 94
    label "pozytywnie"
  ]
  node [
    id 95
    label "dobry"
  ]
  node [
    id 96
    label "dobroczynnie"
  ]
  node [
    id 97
    label "odpowiednio"
  ]
  node [
    id 98
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 99
    label "skutecznie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 43
  ]
  edge [
    source 8
    target 44
  ]
  edge [
    source 8
    target 45
  ]
  edge [
    source 8
    target 46
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 48
  ]
  edge [
    source 9
    target 49
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 51
  ]
  edge [
    source 9
    target 52
  ]
  edge [
    source 9
    target 53
  ]
  edge [
    source 9
    target 54
  ]
  edge [
    source 9
    target 55
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 64
  ]
  edge [
    source 13
    target 65
  ]
  edge [
    source 13
    target 66
  ]
  edge [
    source 13
    target 67
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 68
  ]
  edge [
    source 14
    target 69
  ]
  edge [
    source 14
    target 70
  ]
  edge [
    source 14
    target 71
  ]
  edge [
    source 14
    target 72
  ]
  edge [
    source 14
    target 73
  ]
  edge [
    source 14
    target 74
  ]
  edge [
    source 14
    target 75
  ]
  edge [
    source 14
    target 76
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 14
    target 78
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 14
    target 55
  ]
  edge [
    source 14
    target 84
  ]
  edge [
    source 14
    target 85
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 88
  ]
  edge [
    source 16
    target 89
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 16
    target 91
  ]
  edge [
    source 16
    target 92
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 94
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
]
