graph [
  maxDegree 71
  minDegree 1
  meanDegree 1.978494623655914
  density 0.021505376344086023
  graphCliqueNumber 2
  node [
    id 0
    label "kuhwa"
    origin "text"
  ]
  node [
    id 1
    label "tyle"
    origin "text"
  ]
  node [
    id 2
    label "lato"
    origin "text"
  ]
  node [
    id 3
    label "&#380;y&#263;"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wiat"
    origin "text"
  ]
  node [
    id 5
    label "dopiero"
    origin "text"
  ]
  node [
    id 6
    label "dzi&#347;"
    origin "text"
  ]
  node [
    id 7
    label "dosta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "wpierdol"
    origin "text"
  ]
  node [
    id 9
    label "wiele"
  ]
  node [
    id 10
    label "konkretnie"
  ]
  node [
    id 11
    label "nieznacznie"
  ]
  node [
    id 12
    label "pora_roku"
  ]
  node [
    id 13
    label "pause"
  ]
  node [
    id 14
    label "stay"
  ]
  node [
    id 15
    label "consist"
  ]
  node [
    id 16
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 17
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 18
    label "istnie&#263;"
  ]
  node [
    id 19
    label "rzeczywisto&#347;&#263;_spo&#322;eczna"
  ]
  node [
    id 20
    label "obszar"
  ]
  node [
    id 21
    label "obiekt_naturalny"
  ]
  node [
    id 22
    label "przedmiot"
  ]
  node [
    id 23
    label "Stary_&#346;wiat"
  ]
  node [
    id 24
    label "grupa"
  ]
  node [
    id 25
    label "stw&#243;r"
  ]
  node [
    id 26
    label "biosfera"
  ]
  node [
    id 27
    label "przyroda_nieo&#380;ywiona"
  ]
  node [
    id 28
    label "rzecz"
  ]
  node [
    id 29
    label "magnetosfera"
  ]
  node [
    id 30
    label "Uk&#322;ad_S&#322;oneczny"
  ]
  node [
    id 31
    label "environment"
  ]
  node [
    id 32
    label "p&#322;aszcz_Ziemi"
  ]
  node [
    id 33
    label "geosfera"
  ]
  node [
    id 34
    label "Nowy_&#346;wiat"
  ]
  node [
    id 35
    label "planeta"
  ]
  node [
    id 36
    label "przejmowa&#263;"
  ]
  node [
    id 37
    label "litosfera"
  ]
  node [
    id 38
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 39
    label "makrokosmos"
  ]
  node [
    id 40
    label "barysfera"
  ]
  node [
    id 41
    label "biota"
  ]
  node [
    id 42
    label "p&#243;&#322;noc"
  ]
  node [
    id 43
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 44
    label "fauna"
  ]
  node [
    id 45
    label "wszechstworzenie"
  ]
  node [
    id 46
    label "geotermia"
  ]
  node [
    id 47
    label "biegun"
  ]
  node [
    id 48
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 49
    label "ekosystem"
  ]
  node [
    id 50
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 51
    label "teren"
  ]
  node [
    id 52
    label "zjawisko"
  ]
  node [
    id 53
    label "p&#243;&#322;kula"
  ]
  node [
    id 54
    label "atmosfera"
  ]
  node [
    id 55
    label "mikrokosmos"
  ]
  node [
    id 56
    label "class"
  ]
  node [
    id 57
    label "po&#322;udnie"
  ]
  node [
    id 58
    label "cia&#322;o_niebieskie"
  ]
  node [
    id 59
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 60
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 61
    label "przejmowanie"
  ]
  node [
    id 62
    label "przestrze&#324;"
  ]
  node [
    id 63
    label "asymilowanie_si&#281;"
  ]
  node [
    id 64
    label "przej&#261;&#263;"
  ]
  node [
    id 65
    label "ekosfera"
  ]
  node [
    id 66
    label "przyroda"
  ]
  node [
    id 67
    label "uk&#322;ad_planetarny"
  ]
  node [
    id 68
    label "ciemna_materia"
  ]
  node [
    id 69
    label "geoida"
  ]
  node [
    id 70
    label "Wsch&#243;d"
  ]
  node [
    id 71
    label "populace"
  ]
  node [
    id 72
    label "Ksi&#281;&#380;yc"
  ]
  node [
    id 73
    label "huczek"
  ]
  node [
    id 74
    label "sfera_gwiazd_sta&#322;ych"
  ]
  node [
    id 75
    label "Ziemia"
  ]
  node [
    id 76
    label "universe"
  ]
  node [
    id 77
    label "ozonosfera"
  ]
  node [
    id 78
    label "rze&#378;ba"
  ]
  node [
    id 79
    label "rzeczywisto&#347;&#263;"
  ]
  node [
    id 80
    label "zagranica"
  ]
  node [
    id 81
    label "hydrosfera"
  ]
  node [
    id 82
    label "woda"
  ]
  node [
    id 83
    label "kuchnia"
  ]
  node [
    id 84
    label "przej&#281;cie"
  ]
  node [
    id 85
    label "czarna_dziura"
  ]
  node [
    id 86
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 87
    label "morze"
  ]
  node [
    id 88
    label "doba"
  ]
  node [
    id 89
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 90
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 91
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 92
    label "&#322;omot"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 92
  ]
]
