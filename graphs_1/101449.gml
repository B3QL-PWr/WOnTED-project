graph [
  maxDegree 18
  minDegree 1
  meanDegree 1.7704918032786885
  density 0.029508196721311476
  graphCliqueNumber 4
  node [
    id 0
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 1
    label "&#347;wiadek"
    origin "text"
  ]
  node [
    id 2
    label "miko&#322;aj"
    origin "text"
  ]
  node [
    id 3
    label "biskup"
    origin "text"
  ]
  node [
    id 4
    label "grabowcu"
    origin "text"
  ]
  node [
    id 5
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 6
    label "zakrystia"
  ]
  node [
    id 7
    label "organizacja_religijna"
  ]
  node [
    id 8
    label "nawa"
  ]
  node [
    id 9
    label "nerwica_eklezjogenna"
  ]
  node [
    id 10
    label "kropielnica"
  ]
  node [
    id 11
    label "prezbiterium"
  ]
  node [
    id 12
    label "wsp&#243;lnota"
  ]
  node [
    id 13
    label "church"
  ]
  node [
    id 14
    label "kruchta"
  ]
  node [
    id 15
    label "Ska&#322;ka"
  ]
  node [
    id 16
    label "kult"
  ]
  node [
    id 17
    label "ub&#322;agalnia"
  ]
  node [
    id 18
    label "dom"
  ]
  node [
    id 19
    label "cz&#322;owiek"
  ]
  node [
    id 20
    label "s&#261;d"
  ]
  node [
    id 21
    label "osoba_fizyczna"
  ]
  node [
    id 22
    label "uczestnik"
  ]
  node [
    id 23
    label "obserwator"
  ]
  node [
    id 24
    label "dru&#380;ba"
  ]
  node [
    id 25
    label "Berkeley"
  ]
  node [
    id 26
    label "prekonizacja"
  ]
  node [
    id 27
    label "sakra"
  ]
  node [
    id 28
    label "pontyfikat"
  ]
  node [
    id 29
    label "&#347;w"
  ]
  node [
    id 30
    label "konsekrowanie"
  ]
  node [
    id 31
    label "episkopat"
  ]
  node [
    id 32
    label "dostojnik_ko&#347;cielny"
  ]
  node [
    id 33
    label "pontyfikalia"
  ]
  node [
    id 34
    label "&#347;wi&#281;ty"
  ]
  node [
    id 35
    label "diecezja"
  ]
  node [
    id 36
    label "zamojsko"
  ]
  node [
    id 37
    label "lubaczowski"
  ]
  node [
    id 38
    label "dekanat"
  ]
  node [
    id 39
    label "grabowieckiego"
  ]
  node [
    id 40
    label "matka"
  ]
  node [
    id 41
    label "bo&#380;y"
  ]
  node [
    id 42
    label "Ziemowit"
  ]
  node [
    id 43
    label "IV"
  ]
  node [
    id 44
    label "Krzysztofa"
  ]
  node [
    id 45
    label "&#346;wi&#281;cicki"
  ]
  node [
    id 46
    label "Fryderyka"
  ]
  node [
    id 47
    label "Libenau"
  ]
  node [
    id 48
    label "Ignacy"
  ]
  node [
    id 49
    label "Kaliniak"
  ]
  node [
    id 50
    label "Adolfa"
  ]
  node [
    id 51
    label "Zdzierski"
  ]
  node [
    id 52
    label "Witolda"
  ]
  node [
    id 53
    label "Skulicz"
  ]
  node [
    id 54
    label "Jerzy"
  ]
  node [
    id 55
    label "leski"
  ]
  node [
    id 56
    label "MB"
  ]
  node [
    id 57
    label "szkaplerzny"
  ]
  node [
    id 58
    label "Jan"
  ]
  node [
    id 59
    label "pawe&#322;"
  ]
  node [
    id 60
    label "ii"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 59
    target 60
  ]
]
