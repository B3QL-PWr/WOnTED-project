graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.13333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "xxx"
    origin "text"
  ]
  node [
    id 1
    label "czym"
    origin "text"
  ]
  node [
    id 2
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 3
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
  ]
  node [
    id 5
    label "uprawi&#263;"
  ]
  node [
    id 6
    label "gotowy"
  ]
  node [
    id 7
    label "might"
  ]
  node [
    id 8
    label "help"
  ]
  node [
    id 9
    label "aid"
  ]
  node [
    id 10
    label "u&#322;atwi&#263;"
  ]
  node [
    id 11
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 12
    label "concur"
  ]
  node [
    id 13
    label "zrobi&#263;"
  ]
  node [
    id 14
    label "zaskutkowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
]
