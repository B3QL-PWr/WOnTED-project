graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.6363636363636365
  density 0.16363636363636364
  graphCliqueNumber 2
  node [
    id 0
    label "mirka"
    origin "text"
  ]
  node [
    id 1
    label "zapyta&#263;by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "wczoraj"
    origin "text"
  ]
  node [
    id 3
    label "szefowa"
    origin "text"
  ]
  node [
    id 4
    label "podwy&#380;ka"
    origin "text"
  ]
  node [
    id 5
    label "doba"
  ]
  node [
    id 6
    label "dawno"
  ]
  node [
    id 7
    label "niedawno"
  ]
  node [
    id 8
    label "wzrost"
  ]
  node [
    id 9
    label "XDDDDD"
  ]
  node [
    id 10
    label "podobno"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 8
  ]
  edge [
    source 9
    target 10
  ]
]
