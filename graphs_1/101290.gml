graph [
  maxDegree 13
  minDegree 1
  meanDegree 2.4
  density 0.07058823529411765
  graphCliqueNumber 6
  node [
    id 0
    label "step"
    origin "text"
  ]
  node [
    id 1
    label "closer"
    origin "text"
  ]
  node [
    id 2
    label "utw&#243;r"
    origin "text"
  ]
  node [
    id 3
    label "formacja_ro&#347;linna"
  ]
  node [
    id 4
    label "biom"
  ]
  node [
    id 5
    label "trawa"
  ]
  node [
    id 6
    label "Abakan"
  ]
  node [
    id 7
    label "krok_taneczny"
  ]
  node [
    id 8
    label "taniec"
  ]
  node [
    id 9
    label "r&#243;wnina"
  ]
  node [
    id 10
    label "teren"
  ]
  node [
    id 11
    label "Wielki_Step"
  ]
  node [
    id 12
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 13
    label "tre&#347;&#263;"
  ]
  node [
    id 14
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 15
    label "obrazowanie"
  ]
  node [
    id 16
    label "part"
  ]
  node [
    id 17
    label "organ"
  ]
  node [
    id 18
    label "komunikat"
  ]
  node [
    id 19
    label "tekst"
  ]
  node [
    id 20
    label "nale&#380;no&#347;&#263;_licencyjna"
  ]
  node [
    id 21
    label "&#347;wiat_przedstawiony"
  ]
  node [
    id 22
    label "element_anatomiczny"
  ]
  node [
    id 23
    label "on"
  ]
  node [
    id 24
    label "Closer"
  ]
  node [
    id 25
    label "How"
  ]
  node [
    id 26
    label "to"
  ]
  node [
    id 27
    label "Dismantle"
  ]
  node [
    id 28
    label "an"
  ]
  node [
    id 29
    label "Atomic"
  ]
  node [
    id 30
    label "bomba"
  ]
  node [
    id 31
    label "Noela"
  ]
  node [
    id 32
    label "Gallaghera"
  ]
  node [
    id 33
    label "b&#243;b"
  ]
  node [
    id 34
    label "Hewsonie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 27
  ]
  edge [
    source 25
    target 28
  ]
  edge [
    source 25
    target 29
  ]
  edge [
    source 25
    target 30
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 29
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 29
  ]
  edge [
    source 27
    target 30
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 33
    target 34
  ]
]
