graph [
  maxDegree 323
  minDegree 1
  meanDegree 1.9941348973607038
  density 0.005865102639296188
  graphCliqueNumber 2
  node [
    id 0
    label "typowy"
    origin "text"
  ]
  node [
    id 1
    label "dzienia"
    origin "text"
  ]
  node [
    id 2
    label "w&#322;a&#347;ciciel"
    origin "text"
  ]
  node [
    id 3
    label "lambo"
    origin "text"
  ]
  node [
    id 4
    label "jak&#380;e"
    origin "text"
  ]
  node [
    id 5
    label "pi&#281;kne"
    origin "text"
  ]
  node [
    id 6
    label "kraj"
    origin "text"
  ]
  node [
    id 7
    label "typowo"
  ]
  node [
    id 8
    label "zwyczajny"
  ]
  node [
    id 9
    label "zwyk&#322;y"
  ]
  node [
    id 10
    label "cz&#281;sty"
  ]
  node [
    id 11
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 12
    label "wykupienie"
  ]
  node [
    id 13
    label "bycie_w_posiadaniu"
  ]
  node [
    id 14
    label "wykupywanie"
  ]
  node [
    id 15
    label "podmiot"
  ]
  node [
    id 16
    label "supersamoch&#243;d"
  ]
  node [
    id 17
    label "Lamborghini"
  ]
  node [
    id 18
    label "samoch&#243;d"
  ]
  node [
    id 19
    label "Skandynawia"
  ]
  node [
    id 20
    label "Filipiny"
  ]
  node [
    id 21
    label "Rwanda"
  ]
  node [
    id 22
    label "Kaukaz"
  ]
  node [
    id 23
    label "Kaszmir"
  ]
  node [
    id 24
    label "Toskania"
  ]
  node [
    id 25
    label "Yorkshire"
  ]
  node [
    id 26
    label "&#321;emkowszczyzna"
  ]
  node [
    id 27
    label "obszar"
  ]
  node [
    id 28
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 29
    label "Monako"
  ]
  node [
    id 30
    label "Amhara"
  ]
  node [
    id 31
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 32
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 33
    label "Lombardia"
  ]
  node [
    id 34
    label "Korea"
  ]
  node [
    id 35
    label "Kalabria"
  ]
  node [
    id 36
    label "Ghana"
  ]
  node [
    id 37
    label "Czarnog&#243;ra"
  ]
  node [
    id 38
    label "Tyrol"
  ]
  node [
    id 39
    label "Malawi"
  ]
  node [
    id 40
    label "Indonezja"
  ]
  node [
    id 41
    label "Bu&#322;garia"
  ]
  node [
    id 42
    label "Nauru"
  ]
  node [
    id 43
    label "Kenia"
  ]
  node [
    id 44
    label "Pamir"
  ]
  node [
    id 45
    label "Kambod&#380;a"
  ]
  node [
    id 46
    label "Lubelszczyzna"
  ]
  node [
    id 47
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 48
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 49
    label "Mali"
  ]
  node [
    id 50
    label "&#379;ywiecczyzna"
  ]
  node [
    id 51
    label "Austria"
  ]
  node [
    id 52
    label "interior"
  ]
  node [
    id 53
    label "Europa_Wschodnia"
  ]
  node [
    id 54
    label "Armenia"
  ]
  node [
    id 55
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 56
    label "Fid&#380;i"
  ]
  node [
    id 57
    label "Tuwalu"
  ]
  node [
    id 58
    label "Zabajkale"
  ]
  node [
    id 59
    label "Etiopia"
  ]
  node [
    id 60
    label "Malta"
  ]
  node [
    id 61
    label "Malezja"
  ]
  node [
    id 62
    label "Kaszuby"
  ]
  node [
    id 63
    label "Bo&#347;nia"
  ]
  node [
    id 64
    label "Noworosja"
  ]
  node [
    id 65
    label "Grenada"
  ]
  node [
    id 66
    label "Tad&#380;ykistan"
  ]
  node [
    id 67
    label "Ba&#322;kany"
  ]
  node [
    id 68
    label "Wehrlen"
  ]
  node [
    id 69
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 70
    label "Anglia"
  ]
  node [
    id 71
    label "Kielecczyzna"
  ]
  node [
    id 72
    label "Rumunia"
  ]
  node [
    id 73
    label "Pomorze_Zachodnie"
  ]
  node [
    id 74
    label "Maroko"
  ]
  node [
    id 75
    label "Bhutan"
  ]
  node [
    id 76
    label "Opolskie"
  ]
  node [
    id 77
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 78
    label "Ko&#322;yma"
  ]
  node [
    id 79
    label "Oksytania"
  ]
  node [
    id 80
    label "S&#322;owacja"
  ]
  node [
    id 81
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 82
    label "Seszele"
  ]
  node [
    id 83
    label "Syjon"
  ]
  node [
    id 84
    label "Kuwejt"
  ]
  node [
    id 85
    label "Arabia_Saudyjska"
  ]
  node [
    id 86
    label "Kociewie"
  ]
  node [
    id 87
    label "Ekwador"
  ]
  node [
    id 88
    label "Kanada"
  ]
  node [
    id 89
    label "ziemia"
  ]
  node [
    id 90
    label "Japonia"
  ]
  node [
    id 91
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 92
    label "Hiszpania"
  ]
  node [
    id 93
    label "Wyspy_Marshalla"
  ]
  node [
    id 94
    label "Botswana"
  ]
  node [
    id 95
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 96
    label "D&#380;ibuti"
  ]
  node [
    id 97
    label "Huculszczyzna"
  ]
  node [
    id 98
    label "Wietnam"
  ]
  node [
    id 99
    label "Egipt"
  ]
  node [
    id 100
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 101
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 102
    label "Burkina_Faso"
  ]
  node [
    id 103
    label "Bawaria"
  ]
  node [
    id 104
    label "Niemcy"
  ]
  node [
    id 105
    label "Khitai"
  ]
  node [
    id 106
    label "Macedonia"
  ]
  node [
    id 107
    label "Albania"
  ]
  node [
    id 108
    label "Madagaskar"
  ]
  node [
    id 109
    label "Bahrajn"
  ]
  node [
    id 110
    label "Jemen"
  ]
  node [
    id 111
    label "Lesoto"
  ]
  node [
    id 112
    label "Maghreb"
  ]
  node [
    id 113
    label "Samoa"
  ]
  node [
    id 114
    label "Andora"
  ]
  node [
    id 115
    label "Bory_Tucholskie"
  ]
  node [
    id 116
    label "Chiny"
  ]
  node [
    id 117
    label "Europa_Zachodnia"
  ]
  node [
    id 118
    label "Cypr"
  ]
  node [
    id 119
    label "Wielka_Brytania"
  ]
  node [
    id 120
    label "Kerala"
  ]
  node [
    id 121
    label "Podhale"
  ]
  node [
    id 122
    label "Kabylia"
  ]
  node [
    id 123
    label "Ukraina"
  ]
  node [
    id 124
    label "Paragwaj"
  ]
  node [
    id 125
    label "Trynidad_i_Tobago"
  ]
  node [
    id 126
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 127
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 128
    label "Ma&#322;opolska"
  ]
  node [
    id 129
    label "Polesie"
  ]
  node [
    id 130
    label "Liguria"
  ]
  node [
    id 131
    label "Libia"
  ]
  node [
    id 132
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 133
    label "&#321;&#243;dzkie"
  ]
  node [
    id 134
    label "Surinam"
  ]
  node [
    id 135
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 136
    label "Palestyna"
  ]
  node [
    id 137
    label "Australia"
  ]
  node [
    id 138
    label "Nigeria"
  ]
  node [
    id 139
    label "Honduras"
  ]
  node [
    id 140
    label "Bojkowszczyzna"
  ]
  node [
    id 141
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 142
    label "Karaiby"
  ]
  node [
    id 143
    label "Bangladesz"
  ]
  node [
    id 144
    label "Peru"
  ]
  node [
    id 145
    label "Kazachstan"
  ]
  node [
    id 146
    label "USA"
  ]
  node [
    id 147
    label "Irak"
  ]
  node [
    id 148
    label "Nepal"
  ]
  node [
    id 149
    label "S&#261;decczyzna"
  ]
  node [
    id 150
    label "Sudan"
  ]
  node [
    id 151
    label "Sand&#380;ak"
  ]
  node [
    id 152
    label "Nadrenia"
  ]
  node [
    id 153
    label "San_Marino"
  ]
  node [
    id 154
    label "Burundi"
  ]
  node [
    id 155
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 156
    label "Dominikana"
  ]
  node [
    id 157
    label "Komory"
  ]
  node [
    id 158
    label "Zakarpacie"
  ]
  node [
    id 159
    label "Gwatemala"
  ]
  node [
    id 160
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 161
    label "Zag&#243;rze"
  ]
  node [
    id 162
    label "Andaluzja"
  ]
  node [
    id 163
    label "granica_pa&#324;stwa"
  ]
  node [
    id 164
    label "Turkiestan"
  ]
  node [
    id 165
    label "Naddniestrze"
  ]
  node [
    id 166
    label "Hercegowina"
  ]
  node [
    id 167
    label "Brunei"
  ]
  node [
    id 168
    label "Iran"
  ]
  node [
    id 169
    label "jednostka_administracyjna"
  ]
  node [
    id 170
    label "Zimbabwe"
  ]
  node [
    id 171
    label "Namibia"
  ]
  node [
    id 172
    label "Meksyk"
  ]
  node [
    id 173
    label "Lotaryngia"
  ]
  node [
    id 174
    label "Kamerun"
  ]
  node [
    id 175
    label "Opolszczyzna"
  ]
  node [
    id 176
    label "Afryka_Wschodnia"
  ]
  node [
    id 177
    label "Szlezwik"
  ]
  node [
    id 178
    label "Somalia"
  ]
  node [
    id 179
    label "Angola"
  ]
  node [
    id 180
    label "Gabon"
  ]
  node [
    id 181
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 182
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 183
    label "Mozambik"
  ]
  node [
    id 184
    label "Tajwan"
  ]
  node [
    id 185
    label "Tunezja"
  ]
  node [
    id 186
    label "Nowa_Zelandia"
  ]
  node [
    id 187
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 188
    label "Podbeskidzie"
  ]
  node [
    id 189
    label "Liban"
  ]
  node [
    id 190
    label "Jordania"
  ]
  node [
    id 191
    label "Tonga"
  ]
  node [
    id 192
    label "Czad"
  ]
  node [
    id 193
    label "Liberia"
  ]
  node [
    id 194
    label "Gwinea"
  ]
  node [
    id 195
    label "Belize"
  ]
  node [
    id 196
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 197
    label "Mazowsze"
  ]
  node [
    id 198
    label "&#321;otwa"
  ]
  node [
    id 199
    label "Syria"
  ]
  node [
    id 200
    label "Benin"
  ]
  node [
    id 201
    label "Afryka_Zachodnia"
  ]
  node [
    id 202
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 203
    label "Dominika"
  ]
  node [
    id 204
    label "Antigua_i_Barbuda"
  ]
  node [
    id 205
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 206
    label "Hanower"
  ]
  node [
    id 207
    label "Galicja"
  ]
  node [
    id 208
    label "Szkocja"
  ]
  node [
    id 209
    label "Walia"
  ]
  node [
    id 210
    label "Afganistan"
  ]
  node [
    id 211
    label "Kiribati"
  ]
  node [
    id 212
    label "W&#322;ochy"
  ]
  node [
    id 213
    label "Szwajcaria"
  ]
  node [
    id 214
    label "Powi&#347;le"
  ]
  node [
    id 215
    label "Sahara_Zachodnia"
  ]
  node [
    id 216
    label "Chorwacja"
  ]
  node [
    id 217
    label "Tajlandia"
  ]
  node [
    id 218
    label "Salwador"
  ]
  node [
    id 219
    label "Bahamy"
  ]
  node [
    id 220
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 221
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 222
    label "Zamojszczyzna"
  ]
  node [
    id 223
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 224
    label "S&#322;owenia"
  ]
  node [
    id 225
    label "Gambia"
  ]
  node [
    id 226
    label "Kujawy"
  ]
  node [
    id 227
    label "Urugwaj"
  ]
  node [
    id 228
    label "Podlasie"
  ]
  node [
    id 229
    label "Zair"
  ]
  node [
    id 230
    label "Erytrea"
  ]
  node [
    id 231
    label "Laponia"
  ]
  node [
    id 232
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 233
    label "Umbria"
  ]
  node [
    id 234
    label "Rosja"
  ]
  node [
    id 235
    label "Uganda"
  ]
  node [
    id 236
    label "Niger"
  ]
  node [
    id 237
    label "Mauritius"
  ]
  node [
    id 238
    label "Turkmenistan"
  ]
  node [
    id 239
    label "Turcja"
  ]
  node [
    id 240
    label "Mezoameryka"
  ]
  node [
    id 241
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 242
    label "Irlandia"
  ]
  node [
    id 243
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 244
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 245
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 246
    label "Gwinea_Bissau"
  ]
  node [
    id 247
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 248
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 249
    label "Kurdystan"
  ]
  node [
    id 250
    label "Belgia"
  ]
  node [
    id 251
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 252
    label "Palau"
  ]
  node [
    id 253
    label "Barbados"
  ]
  node [
    id 254
    label "Chile"
  ]
  node [
    id 255
    label "Wenezuela"
  ]
  node [
    id 256
    label "W&#281;gry"
  ]
  node [
    id 257
    label "Argentyna"
  ]
  node [
    id 258
    label "Kolumbia"
  ]
  node [
    id 259
    label "Kampania"
  ]
  node [
    id 260
    label "Armagnac"
  ]
  node [
    id 261
    label "Sierra_Leone"
  ]
  node [
    id 262
    label "Azerbejd&#380;an"
  ]
  node [
    id 263
    label "Kongo"
  ]
  node [
    id 264
    label "Polinezja"
  ]
  node [
    id 265
    label "Warmia"
  ]
  node [
    id 266
    label "Pakistan"
  ]
  node [
    id 267
    label "Liechtenstein"
  ]
  node [
    id 268
    label "Wielkopolska"
  ]
  node [
    id 269
    label "Nikaragua"
  ]
  node [
    id 270
    label "Senegal"
  ]
  node [
    id 271
    label "brzeg"
  ]
  node [
    id 272
    label "Bordeaux"
  ]
  node [
    id 273
    label "Lauda"
  ]
  node [
    id 274
    label "Indie"
  ]
  node [
    id 275
    label "Mazury"
  ]
  node [
    id 276
    label "Suazi"
  ]
  node [
    id 277
    label "Polska"
  ]
  node [
    id 278
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 279
    label "Algieria"
  ]
  node [
    id 280
    label "Jamajka"
  ]
  node [
    id 281
    label "Timor_Wschodni"
  ]
  node [
    id 282
    label "Oceania"
  ]
  node [
    id 283
    label "Kostaryka"
  ]
  node [
    id 284
    label "Podkarpacie"
  ]
  node [
    id 285
    label "Lasko"
  ]
  node [
    id 286
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 287
    label "Kuba"
  ]
  node [
    id 288
    label "Mauretania"
  ]
  node [
    id 289
    label "Amazonia"
  ]
  node [
    id 290
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 291
    label "Portoryko"
  ]
  node [
    id 292
    label "Brazylia"
  ]
  node [
    id 293
    label "Mo&#322;dawia"
  ]
  node [
    id 294
    label "organizacja"
  ]
  node [
    id 295
    label "Litwa"
  ]
  node [
    id 296
    label "Kirgistan"
  ]
  node [
    id 297
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 298
    label "Izrael"
  ]
  node [
    id 299
    label "Grecja"
  ]
  node [
    id 300
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 301
    label "Kurpie"
  ]
  node [
    id 302
    label "Holandia"
  ]
  node [
    id 303
    label "Sri_Lanka"
  ]
  node [
    id 304
    label "Tonkin"
  ]
  node [
    id 305
    label "Katar"
  ]
  node [
    id 306
    label "Azja_Wschodnia"
  ]
  node [
    id 307
    label "Mikronezja"
  ]
  node [
    id 308
    label "Ukraina_Zachodnia"
  ]
  node [
    id 309
    label "Laos"
  ]
  node [
    id 310
    label "Mongolia"
  ]
  node [
    id 311
    label "Turyngia"
  ]
  node [
    id 312
    label "Malediwy"
  ]
  node [
    id 313
    label "Zambia"
  ]
  node [
    id 314
    label "Baszkiria"
  ]
  node [
    id 315
    label "Tanzania"
  ]
  node [
    id 316
    label "Gujana"
  ]
  node [
    id 317
    label "Apulia"
  ]
  node [
    id 318
    label "Czechy"
  ]
  node [
    id 319
    label "Panama"
  ]
  node [
    id 320
    label "Uzbekistan"
  ]
  node [
    id 321
    label "Gruzja"
  ]
  node [
    id 322
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 323
    label "Serbia"
  ]
  node [
    id 324
    label "Francja"
  ]
  node [
    id 325
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 326
    label "Togo"
  ]
  node [
    id 327
    label "Estonia"
  ]
  node [
    id 328
    label "Indochiny"
  ]
  node [
    id 329
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 330
    label "Oman"
  ]
  node [
    id 331
    label "Boliwia"
  ]
  node [
    id 332
    label "Portugalia"
  ]
  node [
    id 333
    label "Wyspy_Salomona"
  ]
  node [
    id 334
    label "Luksemburg"
  ]
  node [
    id 335
    label "Haiti"
  ]
  node [
    id 336
    label "Biskupizna"
  ]
  node [
    id 337
    label "Lubuskie"
  ]
  node [
    id 338
    label "Birma"
  ]
  node [
    id 339
    label "Rodezja"
  ]
  node [
    id 340
    label "Ameryka_&#321;aci&#324;ska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 19
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 6
    target 143
  ]
  edge [
    source 6
    target 144
  ]
  edge [
    source 6
    target 145
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 6
    target 147
  ]
  edge [
    source 6
    target 148
  ]
  edge [
    source 6
    target 149
  ]
  edge [
    source 6
    target 150
  ]
  edge [
    source 6
    target 151
  ]
  edge [
    source 6
    target 152
  ]
  edge [
    source 6
    target 153
  ]
  edge [
    source 6
    target 154
  ]
  edge [
    source 6
    target 155
  ]
  edge [
    source 6
    target 156
  ]
  edge [
    source 6
    target 157
  ]
  edge [
    source 6
    target 158
  ]
  edge [
    source 6
    target 159
  ]
  edge [
    source 6
    target 160
  ]
  edge [
    source 6
    target 161
  ]
  edge [
    source 6
    target 162
  ]
  edge [
    source 6
    target 163
  ]
  edge [
    source 6
    target 164
  ]
  edge [
    source 6
    target 165
  ]
  edge [
    source 6
    target 166
  ]
  edge [
    source 6
    target 167
  ]
  edge [
    source 6
    target 168
  ]
  edge [
    source 6
    target 169
  ]
  edge [
    source 6
    target 170
  ]
  edge [
    source 6
    target 171
  ]
  edge [
    source 6
    target 172
  ]
  edge [
    source 6
    target 173
  ]
  edge [
    source 6
    target 174
  ]
  edge [
    source 6
    target 175
  ]
  edge [
    source 6
    target 176
  ]
  edge [
    source 6
    target 177
  ]
  edge [
    source 6
    target 178
  ]
  edge [
    source 6
    target 179
  ]
  edge [
    source 6
    target 180
  ]
  edge [
    source 6
    target 181
  ]
  edge [
    source 6
    target 182
  ]
  edge [
    source 6
    target 183
  ]
  edge [
    source 6
    target 184
  ]
  edge [
    source 6
    target 185
  ]
  edge [
    source 6
    target 186
  ]
  edge [
    source 6
    target 187
  ]
  edge [
    source 6
    target 188
  ]
  edge [
    source 6
    target 189
  ]
  edge [
    source 6
    target 190
  ]
  edge [
    source 6
    target 191
  ]
  edge [
    source 6
    target 192
  ]
  edge [
    source 6
    target 193
  ]
  edge [
    source 6
    target 194
  ]
  edge [
    source 6
    target 195
  ]
  edge [
    source 6
    target 196
  ]
  edge [
    source 6
    target 197
  ]
  edge [
    source 6
    target 198
  ]
  edge [
    source 6
    target 199
  ]
  edge [
    source 6
    target 200
  ]
  edge [
    source 6
    target 201
  ]
  edge [
    source 6
    target 202
  ]
  edge [
    source 6
    target 203
  ]
  edge [
    source 6
    target 204
  ]
  edge [
    source 6
    target 205
  ]
  edge [
    source 6
    target 206
  ]
  edge [
    source 6
    target 207
  ]
  edge [
    source 6
    target 208
  ]
  edge [
    source 6
    target 209
  ]
  edge [
    source 6
    target 210
  ]
  edge [
    source 6
    target 211
  ]
  edge [
    source 6
    target 212
  ]
  edge [
    source 6
    target 213
  ]
  edge [
    source 6
    target 214
  ]
  edge [
    source 6
    target 215
  ]
  edge [
    source 6
    target 216
  ]
  edge [
    source 6
    target 217
  ]
  edge [
    source 6
    target 218
  ]
  edge [
    source 6
    target 219
  ]
  edge [
    source 6
    target 220
  ]
  edge [
    source 6
    target 221
  ]
  edge [
    source 6
    target 222
  ]
  edge [
    source 6
    target 223
  ]
  edge [
    source 6
    target 224
  ]
  edge [
    source 6
    target 225
  ]
  edge [
    source 6
    target 226
  ]
  edge [
    source 6
    target 227
  ]
  edge [
    source 6
    target 228
  ]
  edge [
    source 6
    target 229
  ]
  edge [
    source 6
    target 230
  ]
  edge [
    source 6
    target 231
  ]
  edge [
    source 6
    target 232
  ]
  edge [
    source 6
    target 233
  ]
  edge [
    source 6
    target 234
  ]
  edge [
    source 6
    target 235
  ]
  edge [
    source 6
    target 236
  ]
  edge [
    source 6
    target 237
  ]
  edge [
    source 6
    target 238
  ]
  edge [
    source 6
    target 239
  ]
  edge [
    source 6
    target 240
  ]
  edge [
    source 6
    target 241
  ]
  edge [
    source 6
    target 242
  ]
  edge [
    source 6
    target 243
  ]
  edge [
    source 6
    target 244
  ]
  edge [
    source 6
    target 245
  ]
  edge [
    source 6
    target 246
  ]
  edge [
    source 6
    target 247
  ]
  edge [
    source 6
    target 248
  ]
  edge [
    source 6
    target 249
  ]
  edge [
    source 6
    target 250
  ]
  edge [
    source 6
    target 251
  ]
  edge [
    source 6
    target 252
  ]
  edge [
    source 6
    target 253
  ]
  edge [
    source 6
    target 254
  ]
  edge [
    source 6
    target 255
  ]
  edge [
    source 6
    target 256
  ]
  edge [
    source 6
    target 257
  ]
  edge [
    source 6
    target 258
  ]
  edge [
    source 6
    target 259
  ]
  edge [
    source 6
    target 260
  ]
  edge [
    source 6
    target 261
  ]
  edge [
    source 6
    target 262
  ]
  edge [
    source 6
    target 263
  ]
  edge [
    source 6
    target 264
  ]
  edge [
    source 6
    target 265
  ]
  edge [
    source 6
    target 266
  ]
  edge [
    source 6
    target 267
  ]
  edge [
    source 6
    target 268
  ]
  edge [
    source 6
    target 269
  ]
  edge [
    source 6
    target 270
  ]
  edge [
    source 6
    target 271
  ]
  edge [
    source 6
    target 272
  ]
  edge [
    source 6
    target 273
  ]
  edge [
    source 6
    target 274
  ]
  edge [
    source 6
    target 275
  ]
  edge [
    source 6
    target 276
  ]
  edge [
    source 6
    target 277
  ]
  edge [
    source 6
    target 278
  ]
  edge [
    source 6
    target 279
  ]
  edge [
    source 6
    target 280
  ]
  edge [
    source 6
    target 281
  ]
  edge [
    source 6
    target 282
  ]
  edge [
    source 6
    target 283
  ]
  edge [
    source 6
    target 284
  ]
  edge [
    source 6
    target 285
  ]
  edge [
    source 6
    target 286
  ]
  edge [
    source 6
    target 287
  ]
  edge [
    source 6
    target 288
  ]
  edge [
    source 6
    target 289
  ]
  edge [
    source 6
    target 290
  ]
  edge [
    source 6
    target 291
  ]
  edge [
    source 6
    target 292
  ]
  edge [
    source 6
    target 293
  ]
  edge [
    source 6
    target 294
  ]
  edge [
    source 6
    target 295
  ]
  edge [
    source 6
    target 296
  ]
  edge [
    source 6
    target 297
  ]
  edge [
    source 6
    target 298
  ]
  edge [
    source 6
    target 299
  ]
  edge [
    source 6
    target 300
  ]
  edge [
    source 6
    target 301
  ]
  edge [
    source 6
    target 302
  ]
  edge [
    source 6
    target 303
  ]
  edge [
    source 6
    target 304
  ]
  edge [
    source 6
    target 305
  ]
  edge [
    source 6
    target 306
  ]
  edge [
    source 6
    target 307
  ]
  edge [
    source 6
    target 308
  ]
  edge [
    source 6
    target 309
  ]
  edge [
    source 6
    target 310
  ]
  edge [
    source 6
    target 311
  ]
  edge [
    source 6
    target 312
  ]
  edge [
    source 6
    target 313
  ]
  edge [
    source 6
    target 314
  ]
  edge [
    source 6
    target 315
  ]
  edge [
    source 6
    target 316
  ]
  edge [
    source 6
    target 317
  ]
  edge [
    source 6
    target 318
  ]
  edge [
    source 6
    target 319
  ]
  edge [
    source 6
    target 320
  ]
  edge [
    source 6
    target 321
  ]
  edge [
    source 6
    target 322
  ]
  edge [
    source 6
    target 323
  ]
  edge [
    source 6
    target 324
  ]
  edge [
    source 6
    target 325
  ]
  edge [
    source 6
    target 326
  ]
  edge [
    source 6
    target 327
  ]
  edge [
    source 6
    target 328
  ]
  edge [
    source 6
    target 329
  ]
  edge [
    source 6
    target 330
  ]
  edge [
    source 6
    target 331
  ]
  edge [
    source 6
    target 332
  ]
  edge [
    source 6
    target 333
  ]
  edge [
    source 6
    target 334
  ]
  edge [
    source 6
    target 335
  ]
  edge [
    source 6
    target 336
  ]
  edge [
    source 6
    target 337
  ]
  edge [
    source 6
    target 338
  ]
  edge [
    source 6
    target 339
  ]
  edge [
    source 6
    target 340
  ]
]
