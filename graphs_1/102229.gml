graph [
  maxDegree 218
  minDegree 1
  meanDegree 2.146868250539957
  density 0.004646900975194712
  graphCliqueNumber 4
  node [
    id 0
    label "godzina"
    origin "text"
  ]
  node [
    id 1
    label "okre&#347;lenie"
    origin "text"
  ]
  node [
    id 2
    label "transport"
    origin "text"
  ]
  node [
    id 3
    label "mi&#281;dzynarodowy"
    origin "text"
  ]
  node [
    id 4
    label "oznacza&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wszelki"
    origin "text"
  ]
  node [
    id 6
    label "przew&#243;z"
    origin "text"
  ]
  node [
    id 7
    label "statek"
    origin "text"
  ]
  node [
    id 8
    label "morski"
    origin "text"
  ]
  node [
    id 9
    label "lub"
    origin "text"
  ]
  node [
    id 10
    label "powietrzny"
    origin "text"
  ]
  node [
    id 11
    label "&#347;rodek"
    origin "text"
  ]
  node [
    id 12
    label "kolejowy"
    origin "text"
  ]
  node [
    id 13
    label "samochodowy"
    origin "text"
  ]
  node [
    id 14
    label "eksploatowa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "przez"
    origin "text"
  ]
  node [
    id 16
    label "przedsi&#281;biorstwo"
    origin "text"
  ]
  node [
    id 17
    label "umawia&#263;"
    origin "text"
  ]
  node [
    id 18
    label "si&#281;"
    origin "text"
  ]
  node [
    id 19
    label "pa&#324;stwo"
    origin "text"
  ]
  node [
    id 20
    label "wyj&#261;tek"
    origin "text"
  ]
  node [
    id 21
    label "przypadek"
    origin "text"
  ]
  node [
    id 22
    label "gdy"
    origin "text"
  ]
  node [
    id 23
    label "by&#263;"
    origin "text"
  ]
  node [
    id 24
    label "wy&#322;&#261;cznie"
    origin "text"
  ]
  node [
    id 25
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 26
    label "miejsce"
    origin "text"
  ]
  node [
    id 27
    label "po&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 28
    label "druga"
    origin "text"
  ]
  node [
    id 29
    label "minuta"
  ]
  node [
    id 30
    label "doba"
  ]
  node [
    id 31
    label "czas"
  ]
  node [
    id 32
    label "p&#243;&#322;godzina"
  ]
  node [
    id 33
    label "kwadrans"
  ]
  node [
    id 34
    label "time"
  ]
  node [
    id 35
    label "jednostka_czasu"
  ]
  node [
    id 36
    label "zdecydowanie"
  ]
  node [
    id 37
    label "follow-up"
  ]
  node [
    id 38
    label "appointment"
  ]
  node [
    id 39
    label "ustalenie"
  ]
  node [
    id 40
    label "localization"
  ]
  node [
    id 41
    label "denomination"
  ]
  node [
    id 42
    label "wyra&#380;enie"
  ]
  node [
    id 43
    label "ozdobnik"
  ]
  node [
    id 44
    label "przewidzenie"
  ]
  node [
    id 45
    label "term"
  ]
  node [
    id 46
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 47
    label "zawarto&#347;&#263;"
  ]
  node [
    id 48
    label "unos"
  ]
  node [
    id 49
    label "traffic"
  ]
  node [
    id 50
    label "za&#322;adunek"
  ]
  node [
    id 51
    label "gospodarka"
  ]
  node [
    id 52
    label "roz&#322;adunek"
  ]
  node [
    id 53
    label "grupa"
  ]
  node [
    id 54
    label "sprz&#281;t"
  ]
  node [
    id 55
    label "jednoszynowy"
  ]
  node [
    id 56
    label "cedu&#322;a"
  ]
  node [
    id 57
    label "tyfon"
  ]
  node [
    id 58
    label "us&#322;uga"
  ]
  node [
    id 59
    label "prze&#322;adunek"
  ]
  node [
    id 60
    label "komunikacja"
  ]
  node [
    id 61
    label "towar"
  ]
  node [
    id 62
    label "mi&#281;dzynarodowo"
  ]
  node [
    id 63
    label "umi&#281;dzynarodowienie"
  ]
  node [
    id 64
    label "umi&#281;dzynaradawianie"
  ]
  node [
    id 65
    label "okre&#347;la&#263;"
  ]
  node [
    id 66
    label "represent"
  ]
  node [
    id 67
    label "wyraz"
  ]
  node [
    id 68
    label "wskazywa&#263;"
  ]
  node [
    id 69
    label "stanowi&#263;"
  ]
  node [
    id 70
    label "signify"
  ]
  node [
    id 71
    label "set"
  ]
  node [
    id 72
    label "ustala&#263;"
  ]
  node [
    id 73
    label "ka&#380;dy"
  ]
  node [
    id 74
    label "korab"
  ]
  node [
    id 75
    label "zr&#281;bnica"
  ]
  node [
    id 76
    label "odkotwiczenie"
  ]
  node [
    id 77
    label "cumowanie"
  ]
  node [
    id 78
    label "zadokowanie"
  ]
  node [
    id 79
    label "bumsztak"
  ]
  node [
    id 80
    label "zacumowanie"
  ]
  node [
    id 81
    label "dobi&#263;"
  ]
  node [
    id 82
    label "odkotwiczanie"
  ]
  node [
    id 83
    label "kotwica"
  ]
  node [
    id 84
    label "zwodowa&#263;"
  ]
  node [
    id 85
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 86
    label "zakotwiczenie"
  ]
  node [
    id 87
    label "&#347;r&#243;dokr&#281;cie"
  ]
  node [
    id 88
    label "dzi&#243;b"
  ]
  node [
    id 89
    label "w&#281;z&#322;&#243;wka"
  ]
  node [
    id 90
    label "armada"
  ]
  node [
    id 91
    label "grobla"
  ]
  node [
    id 92
    label "kad&#322;ub"
  ]
  node [
    id 93
    label "dobijanie"
  ]
  node [
    id 94
    label "odkotwicza&#263;"
  ]
  node [
    id 95
    label "proporczyk"
  ]
  node [
    id 96
    label "luk"
  ]
  node [
    id 97
    label "odcumowanie"
  ]
  node [
    id 98
    label "kabina"
  ]
  node [
    id 99
    label "skrajnik"
  ]
  node [
    id 100
    label "kotwiczenie"
  ]
  node [
    id 101
    label "zwodowanie"
  ]
  node [
    id 102
    label "szkutnictwo"
  ]
  node [
    id 103
    label "pojazd"
  ]
  node [
    id 104
    label "wodowanie"
  ]
  node [
    id 105
    label "zacumowa&#263;"
  ]
  node [
    id 106
    label "sterownik_automatyczny"
  ]
  node [
    id 107
    label "p&#322;ywa&#263;"
  ]
  node [
    id 108
    label "zadokowa&#263;"
  ]
  node [
    id 109
    label "zakotwiczy&#263;"
  ]
  node [
    id 110
    label "sztormtrap"
  ]
  node [
    id 111
    label "pok&#322;ad"
  ]
  node [
    id 112
    label "kotwiczy&#263;"
  ]
  node [
    id 113
    label "&#380;yroskop"
  ]
  node [
    id 114
    label "odcumowa&#263;"
  ]
  node [
    id 115
    label "dobicie"
  ]
  node [
    id 116
    label "armator"
  ]
  node [
    id 117
    label "odbijacz"
  ]
  node [
    id 118
    label "dziennik_pok&#322;adowy"
  ]
  node [
    id 119
    label "reling"
  ]
  node [
    id 120
    label "flota"
  ]
  node [
    id 121
    label "kabestan"
  ]
  node [
    id 122
    label "nadbud&#243;wka"
  ]
  node [
    id 123
    label "dokowa&#263;"
  ]
  node [
    id 124
    label "cumowa&#263;"
  ]
  node [
    id 125
    label "odkotwiczy&#263;"
  ]
  node [
    id 126
    label "dobija&#263;"
  ]
  node [
    id 127
    label "odcumowywanie"
  ]
  node [
    id 128
    label "ster"
  ]
  node [
    id 129
    label "szkielet_jednostki_p&#322;ywaj&#261;cej"
  ]
  node [
    id 130
    label "odcumowywa&#263;"
  ]
  node [
    id 131
    label "&#347;wiat&#322;o_kotwiczne"
  ]
  node [
    id 132
    label "futr&#243;wka"
  ]
  node [
    id 133
    label "dokowanie"
  ]
  node [
    id 134
    label "trap"
  ]
  node [
    id 135
    label "zaw&#243;r_denny"
  ]
  node [
    id 136
    label "rostra"
  ]
  node [
    id 137
    label "specjalny"
  ]
  node [
    id 138
    label "niebieski"
  ]
  node [
    id 139
    label "nadmorski"
  ]
  node [
    id 140
    label "morsko"
  ]
  node [
    id 141
    label "wodny"
  ]
  node [
    id 142
    label "s&#322;ony"
  ]
  node [
    id 143
    label "zielony"
  ]
  node [
    id 144
    label "przypominaj&#261;cy"
  ]
  node [
    id 145
    label "typowy"
  ]
  node [
    id 146
    label "powietrznie"
  ]
  node [
    id 147
    label "lekki"
  ]
  node [
    id 148
    label "podniebieski"
  ]
  node [
    id 149
    label "podniebnie"
  ]
  node [
    id 150
    label "abstrakcja"
  ]
  node [
    id 151
    label "punkt"
  ]
  node [
    id 152
    label "substancja"
  ]
  node [
    id 153
    label "spos&#243;b"
  ]
  node [
    id 154
    label "chemikalia"
  ]
  node [
    id 155
    label "komunikacyjny"
  ]
  node [
    id 156
    label "motorowy"
  ]
  node [
    id 157
    label "use"
  ]
  node [
    id 158
    label "wyzyskiwa&#263;"
  ]
  node [
    id 159
    label "wydobywa&#263;"
  ]
  node [
    id 160
    label "u&#380;ywa&#263;"
  ]
  node [
    id 161
    label "HP"
  ]
  node [
    id 162
    label "MAC"
  ]
  node [
    id 163
    label "Hortex"
  ]
  node [
    id 164
    label "Baltona"
  ]
  node [
    id 165
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 166
    label "reengineering"
  ]
  node [
    id 167
    label "podmiot_gospodarczy"
  ]
  node [
    id 168
    label "Pewex"
  ]
  node [
    id 169
    label "MAN_SE"
  ]
  node [
    id 170
    label "Orlen"
  ]
  node [
    id 171
    label "zasoby_ludzkie"
  ]
  node [
    id 172
    label "Apeks"
  ]
  node [
    id 173
    label "interes"
  ]
  node [
    id 174
    label "networking"
  ]
  node [
    id 175
    label "zasoby"
  ]
  node [
    id 176
    label "Orbis"
  ]
  node [
    id 177
    label "Google"
  ]
  node [
    id 178
    label "Canon"
  ]
  node [
    id 179
    label "Spo&#322;em"
  ]
  node [
    id 180
    label "agree"
  ]
  node [
    id 181
    label "porozumiewa&#263;_si&#281;"
  ]
  node [
    id 182
    label "kontaktowa&#263;"
  ]
  node [
    id 183
    label "ask"
  ]
  node [
    id 184
    label "Filipiny"
  ]
  node [
    id 185
    label "Rwanda"
  ]
  node [
    id 186
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 187
    label "Monako"
  ]
  node [
    id 188
    label "Korea"
  ]
  node [
    id 189
    label "Ghana"
  ]
  node [
    id 190
    label "Czarnog&#243;ra"
  ]
  node [
    id 191
    label "Malawi"
  ]
  node [
    id 192
    label "Indonezja"
  ]
  node [
    id 193
    label "Bu&#322;garia"
  ]
  node [
    id 194
    label "Nauru"
  ]
  node [
    id 195
    label "Kenia"
  ]
  node [
    id 196
    label "Kambod&#380;a"
  ]
  node [
    id 197
    label "Mali"
  ]
  node [
    id 198
    label "Austria"
  ]
  node [
    id 199
    label "interior"
  ]
  node [
    id 200
    label "Armenia"
  ]
  node [
    id 201
    label "Fid&#380;i"
  ]
  node [
    id 202
    label "Tuwalu"
  ]
  node [
    id 203
    label "Etiopia"
  ]
  node [
    id 204
    label "Malta"
  ]
  node [
    id 205
    label "Malezja"
  ]
  node [
    id 206
    label "Grenada"
  ]
  node [
    id 207
    label "Tad&#380;ykistan"
  ]
  node [
    id 208
    label "Wehrlen"
  ]
  node [
    id 209
    label "para"
  ]
  node [
    id 210
    label "pa&#324;stwo_australijskie"
  ]
  node [
    id 211
    label "Rumunia"
  ]
  node [
    id 212
    label "Maroko"
  ]
  node [
    id 213
    label "Bhutan"
  ]
  node [
    id 214
    label "S&#322;owacja"
  ]
  node [
    id 215
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 216
    label "Seszele"
  ]
  node [
    id 217
    label "Kuwejt"
  ]
  node [
    id 218
    label "Arabia_Saudyjska"
  ]
  node [
    id 219
    label "Ekwador"
  ]
  node [
    id 220
    label "Kanada"
  ]
  node [
    id 221
    label "Japonia"
  ]
  node [
    id 222
    label "ziemia"
  ]
  node [
    id 223
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 224
    label "Hiszpania"
  ]
  node [
    id 225
    label "Wyspy_Marshalla"
  ]
  node [
    id 226
    label "Botswana"
  ]
  node [
    id 227
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 228
    label "D&#380;ibuti"
  ]
  node [
    id 229
    label "Wietnam"
  ]
  node [
    id 230
    label "Egipt"
  ]
  node [
    id 231
    label "Burkina_Faso"
  ]
  node [
    id 232
    label "Niemcy"
  ]
  node [
    id 233
    label "Khitai"
  ]
  node [
    id 234
    label "Macedonia"
  ]
  node [
    id 235
    label "Albania"
  ]
  node [
    id 236
    label "Madagaskar"
  ]
  node [
    id 237
    label "Bahrajn"
  ]
  node [
    id 238
    label "Jemen"
  ]
  node [
    id 239
    label "Lesoto"
  ]
  node [
    id 240
    label "pa&#324;stwo_m&#322;odzi"
  ]
  node [
    id 241
    label "Samoa"
  ]
  node [
    id 242
    label "Andora"
  ]
  node [
    id 243
    label "Chiny"
  ]
  node [
    id 244
    label "Cypr"
  ]
  node [
    id 245
    label "Wielka_Brytania"
  ]
  node [
    id 246
    label "Ukraina"
  ]
  node [
    id 247
    label "Paragwaj"
  ]
  node [
    id 248
    label "Trynidad_i_Tobago"
  ]
  node [
    id 249
    label "Libia"
  ]
  node [
    id 250
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 251
    label "Surinam"
  ]
  node [
    id 252
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 253
    label "Australia"
  ]
  node [
    id 254
    label "Nigeria"
  ]
  node [
    id 255
    label "Honduras"
  ]
  node [
    id 256
    label "Bangladesz"
  ]
  node [
    id 257
    label "Peru"
  ]
  node [
    id 258
    label "Kazachstan"
  ]
  node [
    id 259
    label "pa&#324;stwo_paleotropikalne"
  ]
  node [
    id 260
    label "Irak"
  ]
  node [
    id 261
    label "holoarktyka"
  ]
  node [
    id 262
    label "USA"
  ]
  node [
    id 263
    label "Sudan"
  ]
  node [
    id 264
    label "Nepal"
  ]
  node [
    id 265
    label "San_Marino"
  ]
  node [
    id 266
    label "Burundi"
  ]
  node [
    id 267
    label "Dominikana"
  ]
  node [
    id 268
    label "Komory"
  ]
  node [
    id 269
    label "granica_pa&#324;stwa"
  ]
  node [
    id 270
    label "Gwatemala"
  ]
  node [
    id 271
    label "Antarktis"
  ]
  node [
    id 272
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 273
    label "Brunei"
  ]
  node [
    id 274
    label "Iran"
  ]
  node [
    id 275
    label "Zimbabwe"
  ]
  node [
    id 276
    label "Namibia"
  ]
  node [
    id 277
    label "Meksyk"
  ]
  node [
    id 278
    label "Kamerun"
  ]
  node [
    id 279
    label "zwrot"
  ]
  node [
    id 280
    label "Somalia"
  ]
  node [
    id 281
    label "Angola"
  ]
  node [
    id 282
    label "Gabon"
  ]
  node [
    id 283
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 284
    label "Mozambik"
  ]
  node [
    id 285
    label "Tajwan"
  ]
  node [
    id 286
    label "Tunezja"
  ]
  node [
    id 287
    label "Nowa_Zelandia"
  ]
  node [
    id 288
    label "Liban"
  ]
  node [
    id 289
    label "Jordania"
  ]
  node [
    id 290
    label "Tonga"
  ]
  node [
    id 291
    label "Czad"
  ]
  node [
    id 292
    label "Liberia"
  ]
  node [
    id 293
    label "Gwinea"
  ]
  node [
    id 294
    label "Belize"
  ]
  node [
    id 295
    label "&#321;otwa"
  ]
  node [
    id 296
    label "Syria"
  ]
  node [
    id 297
    label "Benin"
  ]
  node [
    id 298
    label "pa&#324;stwo_oceaniczne"
  ]
  node [
    id 299
    label "pa&#324;stwo_neotropikalne"
  ]
  node [
    id 300
    label "Dominika"
  ]
  node [
    id 301
    label "Antigua_i_Barbuda"
  ]
  node [
    id 302
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 303
    label "Hanower"
  ]
  node [
    id 304
    label "partia"
  ]
  node [
    id 305
    label "Afganistan"
  ]
  node [
    id 306
    label "Kiribati"
  ]
  node [
    id 307
    label "W&#322;ochy"
  ]
  node [
    id 308
    label "Szwajcaria"
  ]
  node [
    id 309
    label "Sahara_Zachodnia"
  ]
  node [
    id 310
    label "Chorwacja"
  ]
  node [
    id 311
    label "Tajlandia"
  ]
  node [
    id 312
    label "Salwador"
  ]
  node [
    id 313
    label "Bahamy"
  ]
  node [
    id 314
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 315
    label "S&#322;owenia"
  ]
  node [
    id 316
    label "Gambia"
  ]
  node [
    id 317
    label "Urugwaj"
  ]
  node [
    id 318
    label "Zair"
  ]
  node [
    id 319
    label "Erytrea"
  ]
  node [
    id 320
    label "Rosja"
  ]
  node [
    id 321
    label "Uganda"
  ]
  node [
    id 322
    label "Niger"
  ]
  node [
    id 323
    label "Mauritius"
  ]
  node [
    id 324
    label "Turkmenistan"
  ]
  node [
    id 325
    label "Turcja"
  ]
  node [
    id 326
    label "Irlandia"
  ]
  node [
    id 327
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 328
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 329
    label "Gwinea_Bissau"
  ]
  node [
    id 330
    label "Belgia"
  ]
  node [
    id 331
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 332
    label "Palau"
  ]
  node [
    id 333
    label "Barbados"
  ]
  node [
    id 334
    label "Chile"
  ]
  node [
    id 335
    label "Wenezuela"
  ]
  node [
    id 336
    label "W&#281;gry"
  ]
  node [
    id 337
    label "Argentyna"
  ]
  node [
    id 338
    label "Kolumbia"
  ]
  node [
    id 339
    label "Sierra_Leone"
  ]
  node [
    id 340
    label "Azerbejd&#380;an"
  ]
  node [
    id 341
    label "Kongo"
  ]
  node [
    id 342
    label "Pakistan"
  ]
  node [
    id 343
    label "Liechtenstein"
  ]
  node [
    id 344
    label "Nikaragua"
  ]
  node [
    id 345
    label "Senegal"
  ]
  node [
    id 346
    label "Indie"
  ]
  node [
    id 347
    label "Suazi"
  ]
  node [
    id 348
    label "Polska"
  ]
  node [
    id 349
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 350
    label "Algieria"
  ]
  node [
    id 351
    label "terytorium"
  ]
  node [
    id 352
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 353
    label "Jamajka"
  ]
  node [
    id 354
    label "Kostaryka"
  ]
  node [
    id 355
    label "Timor_Wschodni"
  ]
  node [
    id 356
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 357
    label "Kuba"
  ]
  node [
    id 358
    label "Mauretania"
  ]
  node [
    id 359
    label "Portoryko"
  ]
  node [
    id 360
    label "Brazylia"
  ]
  node [
    id 361
    label "Mo&#322;dawia"
  ]
  node [
    id 362
    label "organizacja"
  ]
  node [
    id 363
    label "pa&#324;stwo_przyl&#261;dkowe"
  ]
  node [
    id 364
    label "Litwa"
  ]
  node [
    id 365
    label "Kirgistan"
  ]
  node [
    id 366
    label "najbli&#380;sza_rodzina"
  ]
  node [
    id 367
    label "Izrael"
  ]
  node [
    id 368
    label "Grecja"
  ]
  node [
    id 369
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 370
    label "Holandia"
  ]
  node [
    id 371
    label "Sri_Lanka"
  ]
  node [
    id 372
    label "Katar"
  ]
  node [
    id 373
    label "Mikronezja"
  ]
  node [
    id 374
    label "Mongolia"
  ]
  node [
    id 375
    label "Laos"
  ]
  node [
    id 376
    label "Malediwy"
  ]
  node [
    id 377
    label "Zambia"
  ]
  node [
    id 378
    label "Tanzania"
  ]
  node [
    id 379
    label "Gujana"
  ]
  node [
    id 380
    label "Czechy"
  ]
  node [
    id 381
    label "Panama"
  ]
  node [
    id 382
    label "Uzbekistan"
  ]
  node [
    id 383
    label "Gruzja"
  ]
  node [
    id 384
    label "Serbia"
  ]
  node [
    id 385
    label "Francja"
  ]
  node [
    id 386
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 387
    label "Togo"
  ]
  node [
    id 388
    label "Estonia"
  ]
  node [
    id 389
    label "Oman"
  ]
  node [
    id 390
    label "pa&#324;stwo_holantarktyczne"
  ]
  node [
    id 391
    label "Portugalia"
  ]
  node [
    id 392
    label "Boliwia"
  ]
  node [
    id 393
    label "Luksemburg"
  ]
  node [
    id 394
    label "Haiti"
  ]
  node [
    id 395
    label "Wyspy_Salomona"
  ]
  node [
    id 396
    label "Birma"
  ]
  node [
    id 397
    label "Rodezja"
  ]
  node [
    id 398
    label "ortografia"
  ]
  node [
    id 399
    label "sytuacja"
  ]
  node [
    id 400
    label "wyci&#261;g"
  ]
  node [
    id 401
    label "ekscerptor"
  ]
  node [
    id 402
    label "passage"
  ]
  node [
    id 403
    label "urywek"
  ]
  node [
    id 404
    label "leksem"
  ]
  node [
    id 405
    label "cytat"
  ]
  node [
    id 406
    label "pacjent"
  ]
  node [
    id 407
    label "kategoria_gramatyczna"
  ]
  node [
    id 408
    label "schorzenie"
  ]
  node [
    id 409
    label "przeznaczenie"
  ]
  node [
    id 410
    label "zbieg_okoliczno&#347;ci"
  ]
  node [
    id 411
    label "wydarzenie"
  ]
  node [
    id 412
    label "happening"
  ]
  node [
    id 413
    label "przyk&#322;ad"
  ]
  node [
    id 414
    label "si&#281;ga&#263;"
  ]
  node [
    id 415
    label "trwa&#263;"
  ]
  node [
    id 416
    label "obecno&#347;&#263;"
  ]
  node [
    id 417
    label "stan"
  ]
  node [
    id 418
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 419
    label "stand"
  ]
  node [
    id 420
    label "mie&#263;_miejsce"
  ]
  node [
    id 421
    label "uczestniczy&#263;"
  ]
  node [
    id 422
    label "chodzi&#263;"
  ]
  node [
    id 423
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 424
    label "equal"
  ]
  node [
    id 425
    label "wy&#322;&#261;czny"
  ]
  node [
    id 426
    label "cia&#322;o"
  ]
  node [
    id 427
    label "plac"
  ]
  node [
    id 428
    label "cecha"
  ]
  node [
    id 429
    label "uwaga"
  ]
  node [
    id 430
    label "przestrze&#324;"
  ]
  node [
    id 431
    label "status"
  ]
  node [
    id 432
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 433
    label "chwila"
  ]
  node [
    id 434
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 435
    label "rz&#261;d"
  ]
  node [
    id 436
    label "praca"
  ]
  node [
    id 437
    label "location"
  ]
  node [
    id 438
    label "warunek_lokalowy"
  ]
  node [
    id 439
    label "ugo&#347;ci&#263;"
  ]
  node [
    id 440
    label "przygotowa&#263;"
  ]
  node [
    id 441
    label "zmieni&#263;"
  ]
  node [
    id 442
    label "wyr&#243;&#380;ni&#263;"
  ]
  node [
    id 443
    label "pokry&#263;"
  ]
  node [
    id 444
    label "pozostawi&#263;"
  ]
  node [
    id 445
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 446
    label "zacz&#261;&#263;"
  ]
  node [
    id 447
    label "znak"
  ]
  node [
    id 448
    label "return"
  ]
  node [
    id 449
    label "stagger"
  ]
  node [
    id 450
    label "raise"
  ]
  node [
    id 451
    label "plant"
  ]
  node [
    id 452
    label "umie&#347;ci&#263;"
  ]
  node [
    id 453
    label "wear"
  ]
  node [
    id 454
    label "zepsu&#263;"
  ]
  node [
    id 455
    label "oddzia&#322;a&#263;"
  ]
  node [
    id 456
    label "wygra&#263;"
  ]
  node [
    id 457
    label "republika"
  ]
  node [
    id 458
    label "ministerstwo"
  ]
  node [
    id 459
    label "finanse"
  ]
  node [
    id 460
    label "rzeczpospolita"
  ]
  node [
    id 461
    label "polski"
  ]
  node [
    id 462
    label "minister"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 12
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 22
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 8
    target 142
  ]
  edge [
    source 8
    target 143
  ]
  edge [
    source 8
    target 144
  ]
  edge [
    source 8
    target 145
  ]
  edge [
    source 8
    target 12
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 12
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 146
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 14
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 10
    target 27
  ]
  edge [
    source 11
    target 26
  ]
  edge [
    source 11
    target 31
  ]
  edge [
    source 11
    target 150
  ]
  edge [
    source 11
    target 151
  ]
  edge [
    source 11
    target 152
  ]
  edge [
    source 11
    target 153
  ]
  edge [
    source 11
    target 154
  ]
  edge [
    source 11
    target 14
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 25
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 23
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 26
  ]
  edge [
    source 13
    target 27
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 23
  ]
  edge [
    source 14
    target 24
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 14
    target 160
  ]
  edge [
    source 14
    target 27
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 28
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 161
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 16
    target 178
  ]
  edge [
    source 16
    target 179
  ]
  edge [
    source 16
    target 19
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 19
    target 186
  ]
  edge [
    source 19
    target 187
  ]
  edge [
    source 19
    target 188
  ]
  edge [
    source 19
    target 189
  ]
  edge [
    source 19
    target 190
  ]
  edge [
    source 19
    target 191
  ]
  edge [
    source 19
    target 192
  ]
  edge [
    source 19
    target 193
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 19
    target 209
  ]
  edge [
    source 19
    target 210
  ]
  edge [
    source 19
    target 211
  ]
  edge [
    source 19
    target 212
  ]
  edge [
    source 19
    target 213
  ]
  edge [
    source 19
    target 214
  ]
  edge [
    source 19
    target 215
  ]
  edge [
    source 19
    target 216
  ]
  edge [
    source 19
    target 217
  ]
  edge [
    source 19
    target 218
  ]
  edge [
    source 19
    target 219
  ]
  edge [
    source 19
    target 220
  ]
  edge [
    source 19
    target 221
  ]
  edge [
    source 19
    target 222
  ]
  edge [
    source 19
    target 223
  ]
  edge [
    source 19
    target 224
  ]
  edge [
    source 19
    target 225
  ]
  edge [
    source 19
    target 226
  ]
  edge [
    source 19
    target 227
  ]
  edge [
    source 19
    target 228
  ]
  edge [
    source 19
    target 53
  ]
  edge [
    source 19
    target 229
  ]
  edge [
    source 19
    target 230
  ]
  edge [
    source 19
    target 231
  ]
  edge [
    source 19
    target 232
  ]
  edge [
    source 19
    target 233
  ]
  edge [
    source 19
    target 234
  ]
  edge [
    source 19
    target 235
  ]
  edge [
    source 19
    target 236
  ]
  edge [
    source 19
    target 237
  ]
  edge [
    source 19
    target 238
  ]
  edge [
    source 19
    target 239
  ]
  edge [
    source 19
    target 240
  ]
  edge [
    source 19
    target 241
  ]
  edge [
    source 19
    target 242
  ]
  edge [
    source 19
    target 243
  ]
  edge [
    source 19
    target 244
  ]
  edge [
    source 19
    target 245
  ]
  edge [
    source 19
    target 246
  ]
  edge [
    source 19
    target 247
  ]
  edge [
    source 19
    target 248
  ]
  edge [
    source 19
    target 249
  ]
  edge [
    source 19
    target 250
  ]
  edge [
    source 19
    target 251
  ]
  edge [
    source 19
    target 252
  ]
  edge [
    source 19
    target 253
  ]
  edge [
    source 19
    target 254
  ]
  edge [
    source 19
    target 255
  ]
  edge [
    source 19
    target 256
  ]
  edge [
    source 19
    target 257
  ]
  edge [
    source 19
    target 258
  ]
  edge [
    source 19
    target 259
  ]
  edge [
    source 19
    target 260
  ]
  edge [
    source 19
    target 261
  ]
  edge [
    source 19
    target 262
  ]
  edge [
    source 19
    target 263
  ]
  edge [
    source 19
    target 264
  ]
  edge [
    source 19
    target 265
  ]
  edge [
    source 19
    target 266
  ]
  edge [
    source 19
    target 267
  ]
  edge [
    source 19
    target 268
  ]
  edge [
    source 19
    target 269
  ]
  edge [
    source 19
    target 270
  ]
  edge [
    source 19
    target 271
  ]
  edge [
    source 19
    target 272
  ]
  edge [
    source 19
    target 273
  ]
  edge [
    source 19
    target 274
  ]
  edge [
    source 19
    target 275
  ]
  edge [
    source 19
    target 276
  ]
  edge [
    source 19
    target 277
  ]
  edge [
    source 19
    target 278
  ]
  edge [
    source 19
    target 279
  ]
  edge [
    source 19
    target 280
  ]
  edge [
    source 19
    target 281
  ]
  edge [
    source 19
    target 282
  ]
  edge [
    source 19
    target 283
  ]
  edge [
    source 19
    target 284
  ]
  edge [
    source 19
    target 285
  ]
  edge [
    source 19
    target 286
  ]
  edge [
    source 19
    target 287
  ]
  edge [
    source 19
    target 288
  ]
  edge [
    source 19
    target 289
  ]
  edge [
    source 19
    target 290
  ]
  edge [
    source 19
    target 291
  ]
  edge [
    source 19
    target 292
  ]
  edge [
    source 19
    target 293
  ]
  edge [
    source 19
    target 294
  ]
  edge [
    source 19
    target 295
  ]
  edge [
    source 19
    target 296
  ]
  edge [
    source 19
    target 297
  ]
  edge [
    source 19
    target 298
  ]
  edge [
    source 19
    target 299
  ]
  edge [
    source 19
    target 300
  ]
  edge [
    source 19
    target 301
  ]
  edge [
    source 19
    target 302
  ]
  edge [
    source 19
    target 303
  ]
  edge [
    source 19
    target 304
  ]
  edge [
    source 19
    target 305
  ]
  edge [
    source 19
    target 306
  ]
  edge [
    source 19
    target 307
  ]
  edge [
    source 19
    target 308
  ]
  edge [
    source 19
    target 309
  ]
  edge [
    source 19
    target 310
  ]
  edge [
    source 19
    target 311
  ]
  edge [
    source 19
    target 312
  ]
  edge [
    source 19
    target 313
  ]
  edge [
    source 19
    target 314
  ]
  edge [
    source 19
    target 315
  ]
  edge [
    source 19
    target 316
  ]
  edge [
    source 19
    target 317
  ]
  edge [
    source 19
    target 318
  ]
  edge [
    source 19
    target 319
  ]
  edge [
    source 19
    target 320
  ]
  edge [
    source 19
    target 321
  ]
  edge [
    source 19
    target 322
  ]
  edge [
    source 19
    target 323
  ]
  edge [
    source 19
    target 324
  ]
  edge [
    source 19
    target 325
  ]
  edge [
    source 19
    target 326
  ]
  edge [
    source 19
    target 327
  ]
  edge [
    source 19
    target 328
  ]
  edge [
    source 19
    target 329
  ]
  edge [
    source 19
    target 330
  ]
  edge [
    source 19
    target 331
  ]
  edge [
    source 19
    target 332
  ]
  edge [
    source 19
    target 333
  ]
  edge [
    source 19
    target 334
  ]
  edge [
    source 19
    target 335
  ]
  edge [
    source 19
    target 336
  ]
  edge [
    source 19
    target 337
  ]
  edge [
    source 19
    target 338
  ]
  edge [
    source 19
    target 339
  ]
  edge [
    source 19
    target 340
  ]
  edge [
    source 19
    target 341
  ]
  edge [
    source 19
    target 342
  ]
  edge [
    source 19
    target 343
  ]
  edge [
    source 19
    target 344
  ]
  edge [
    source 19
    target 345
  ]
  edge [
    source 19
    target 346
  ]
  edge [
    source 19
    target 347
  ]
  edge [
    source 19
    target 348
  ]
  edge [
    source 19
    target 349
  ]
  edge [
    source 19
    target 350
  ]
  edge [
    source 19
    target 351
  ]
  edge [
    source 19
    target 352
  ]
  edge [
    source 19
    target 353
  ]
  edge [
    source 19
    target 354
  ]
  edge [
    source 19
    target 355
  ]
  edge [
    source 19
    target 356
  ]
  edge [
    source 19
    target 357
  ]
  edge [
    source 19
    target 358
  ]
  edge [
    source 19
    target 359
  ]
  edge [
    source 19
    target 360
  ]
  edge [
    source 19
    target 361
  ]
  edge [
    source 19
    target 362
  ]
  edge [
    source 19
    target 363
  ]
  edge [
    source 19
    target 364
  ]
  edge [
    source 19
    target 365
  ]
  edge [
    source 19
    target 366
  ]
  edge [
    source 19
    target 367
  ]
  edge [
    source 19
    target 368
  ]
  edge [
    source 19
    target 369
  ]
  edge [
    source 19
    target 370
  ]
  edge [
    source 19
    target 371
  ]
  edge [
    source 19
    target 372
  ]
  edge [
    source 19
    target 373
  ]
  edge [
    source 19
    target 374
  ]
  edge [
    source 19
    target 375
  ]
  edge [
    source 19
    target 376
  ]
  edge [
    source 19
    target 377
  ]
  edge [
    source 19
    target 378
  ]
  edge [
    source 19
    target 379
  ]
  edge [
    source 19
    target 380
  ]
  edge [
    source 19
    target 381
  ]
  edge [
    source 19
    target 382
  ]
  edge [
    source 19
    target 383
  ]
  edge [
    source 19
    target 384
  ]
  edge [
    source 19
    target 385
  ]
  edge [
    source 19
    target 386
  ]
  edge [
    source 19
    target 387
  ]
  edge [
    source 19
    target 388
  ]
  edge [
    source 19
    target 389
  ]
  edge [
    source 19
    target 390
  ]
  edge [
    source 19
    target 391
  ]
  edge [
    source 19
    target 392
  ]
  edge [
    source 19
    target 393
  ]
  edge [
    source 19
    target 394
  ]
  edge [
    source 19
    target 395
  ]
  edge [
    source 19
    target 396
  ]
  edge [
    source 19
    target 397
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 398
  ]
  edge [
    source 20
    target 399
  ]
  edge [
    source 20
    target 400
  ]
  edge [
    source 20
    target 401
  ]
  edge [
    source 20
    target 402
  ]
  edge [
    source 20
    target 403
  ]
  edge [
    source 20
    target 404
  ]
  edge [
    source 20
    target 405
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 406
  ]
  edge [
    source 21
    target 407
  ]
  edge [
    source 21
    target 408
  ]
  edge [
    source 21
    target 409
  ]
  edge [
    source 21
    target 410
  ]
  edge [
    source 21
    target 411
  ]
  edge [
    source 21
    target 412
  ]
  edge [
    source 21
    target 413
  ]
  edge [
    source 23
    target 414
  ]
  edge [
    source 23
    target 415
  ]
  edge [
    source 23
    target 416
  ]
  edge [
    source 23
    target 417
  ]
  edge [
    source 23
    target 418
  ]
  edge [
    source 23
    target 419
  ]
  edge [
    source 23
    target 420
  ]
  edge [
    source 23
    target 421
  ]
  edge [
    source 23
    target 422
  ]
  edge [
    source 23
    target 423
  ]
  edge [
    source 23
    target 424
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 425
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 426
  ]
  edge [
    source 26
    target 427
  ]
  edge [
    source 26
    target 428
  ]
  edge [
    source 26
    target 429
  ]
  edge [
    source 26
    target 430
  ]
  edge [
    source 26
    target 431
  ]
  edge [
    source 26
    target 432
  ]
  edge [
    source 26
    target 433
  ]
  edge [
    source 26
    target 434
  ]
  edge [
    source 26
    target 435
  ]
  edge [
    source 26
    target 436
  ]
  edge [
    source 26
    target 437
  ]
  edge [
    source 26
    target 438
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 439
  ]
  edge [
    source 27
    target 440
  ]
  edge [
    source 27
    target 441
  ]
  edge [
    source 27
    target 442
  ]
  edge [
    source 27
    target 443
  ]
  edge [
    source 27
    target 444
  ]
  edge [
    source 27
    target 445
  ]
  edge [
    source 27
    target 446
  ]
  edge [
    source 27
    target 447
  ]
  edge [
    source 27
    target 448
  ]
  edge [
    source 27
    target 449
  ]
  edge [
    source 27
    target 450
  ]
  edge [
    source 27
    target 451
  ]
  edge [
    source 27
    target 452
  ]
  edge [
    source 27
    target 453
  ]
  edge [
    source 27
    target 454
  ]
  edge [
    source 27
    target 455
  ]
  edge [
    source 27
    target 456
  ]
  edge [
    source 207
    target 457
  ]
  edge [
    source 458
    target 459
  ]
  edge [
    source 459
    target 462
  ]
  edge [
    source 460
    target 461
  ]
]
