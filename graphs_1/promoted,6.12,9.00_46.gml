graph [
  maxDegree 24
  minDegree 1
  meanDegree 2
  density 0.015748031496062992
  graphCliqueNumber 2
  node [
    id 0
    label "kiedy"
    origin "text"
  ]
  node [
    id 1
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 2
    label "uwaga"
    origin "text"
  ]
  node [
    id 3
    label "europejski"
    origin "text"
  ]
  node [
    id 4
    label "ekolog"
    origin "text"
  ]
  node [
    id 5
    label "skupiony"
    origin "text"
  ]
  node [
    id 6
    label "by&#322;a"
    origin "text"
  ]
  node [
    id 7
    label "puszcza"
    origin "text"
  ]
  node [
    id 8
    label "bia&#322;owieski"
    origin "text"
  ]
  node [
    id 9
    label "niemcy"
    origin "text"
  ]
  node [
    id 10
    label "podj&#261;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "decyzja"
    origin "text"
  ]
  node [
    id 12
    label "ostateczny"
    origin "text"
  ]
  node [
    id 13
    label "wycinka"
    origin "text"
  ]
  node [
    id 14
    label "reszta"
    origin "text"
  ]
  node [
    id 15
    label "prastary"
    origin "text"
  ]
  node [
    id 16
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 17
    label "tys"
    origin "text"
  ]
  node [
    id 18
    label "lato"
    origin "text"
  ]
  node [
    id 19
    label "las"
    origin "text"
  ]
  node [
    id 20
    label "hambach"
    origin "text"
  ]
  node [
    id 21
    label "du&#380;y"
  ]
  node [
    id 22
    label "jedyny"
  ]
  node [
    id 23
    label "kompletny"
  ]
  node [
    id 24
    label "zdr&#243;w"
  ]
  node [
    id 25
    label "&#380;ywy"
  ]
  node [
    id 26
    label "ca&#322;o"
  ]
  node [
    id 27
    label "pe&#322;ny"
  ]
  node [
    id 28
    label "calu&#347;ko"
  ]
  node [
    id 29
    label "podobny"
  ]
  node [
    id 30
    label "nagana"
  ]
  node [
    id 31
    label "wypowied&#378;"
  ]
  node [
    id 32
    label "stan"
  ]
  node [
    id 33
    label "dzienniczek"
  ]
  node [
    id 34
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 35
    label "wzgl&#261;d"
  ]
  node [
    id 36
    label "gossip"
  ]
  node [
    id 37
    label "upomnienie"
  ]
  node [
    id 38
    label "tekst"
  ]
  node [
    id 39
    label "European"
  ]
  node [
    id 40
    label "po_europejsku"
  ]
  node [
    id 41
    label "charakterystyczny"
  ]
  node [
    id 42
    label "europejsko"
  ]
  node [
    id 43
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 44
    label "typowy"
  ]
  node [
    id 45
    label "zwolennik"
  ]
  node [
    id 46
    label "zieloni"
  ]
  node [
    id 47
    label "biolog"
  ]
  node [
    id 48
    label "dzia&#322;acz"
  ]
  node [
    id 49
    label "biomedyk"
  ]
  node [
    id 50
    label "uwa&#380;ny"
  ]
  node [
    id 51
    label "partnerka"
  ]
  node [
    id 52
    label "ost&#281;p"
  ]
  node [
    id 53
    label "Puszcza_Kozienicka"
  ]
  node [
    id 54
    label "Puszcza_Kampinoska"
  ]
  node [
    id 55
    label "Puszcza_Maria&#324;ska"
  ]
  node [
    id 56
    label "Puszcza_Knyszy&#324;ska"
  ]
  node [
    id 57
    label "Puszcza_Bia&#322;owieska"
  ]
  node [
    id 58
    label "zmieni&#263;"
  ]
  node [
    id 59
    label "zacz&#261;&#263;"
  ]
  node [
    id 60
    label "zareagowa&#263;"
  ]
  node [
    id 61
    label "allude"
  ]
  node [
    id 62
    label "raise"
  ]
  node [
    id 63
    label "wypowiedzie&#263;_si&#281;"
  ]
  node [
    id 64
    label "draw"
  ]
  node [
    id 65
    label "zrobi&#263;"
  ]
  node [
    id 66
    label "dokument"
  ]
  node [
    id 67
    label "resolution"
  ]
  node [
    id 68
    label "zdecydowanie"
  ]
  node [
    id 69
    label "wytw&#243;r"
  ]
  node [
    id 70
    label "podj&#281;cie_decyzji"
  ]
  node [
    id 71
    label "management"
  ]
  node [
    id 72
    label "konieczny"
  ]
  node [
    id 73
    label "skrajny"
  ]
  node [
    id 74
    label "zupe&#322;ny"
  ]
  node [
    id 75
    label "ostatecznie"
  ]
  node [
    id 76
    label "wydarzenie"
  ]
  node [
    id 77
    label "remainder"
  ]
  node [
    id 78
    label "wydanie"
  ]
  node [
    id 79
    label "wyda&#263;"
  ]
  node [
    id 80
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 81
    label "wydawa&#263;"
  ]
  node [
    id 82
    label "pozosta&#322;y"
  ]
  node [
    id 83
    label "kwota"
  ]
  node [
    id 84
    label "staro&#380;ytnie"
  ]
  node [
    id 85
    label "report"
  ]
  node [
    id 86
    label "dodawa&#263;"
  ]
  node [
    id 87
    label "wymienia&#263;"
  ]
  node [
    id 88
    label "okre&#347;la&#263;"
  ]
  node [
    id 89
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 90
    label "dyskalkulia"
  ]
  node [
    id 91
    label "wynagrodzenie"
  ]
  node [
    id 92
    label "admit"
  ]
  node [
    id 93
    label "osi&#261;ga&#263;"
  ]
  node [
    id 94
    label "wyznacza&#263;"
  ]
  node [
    id 95
    label "posiada&#263;"
  ]
  node [
    id 96
    label "mierzy&#263;"
  ]
  node [
    id 97
    label "odlicza&#263;"
  ]
  node [
    id 98
    label "bra&#263;"
  ]
  node [
    id 99
    label "wycenia&#263;"
  ]
  node [
    id 100
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 101
    label "rachowa&#263;"
  ]
  node [
    id 102
    label "tell"
  ]
  node [
    id 103
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 104
    label "policza&#263;"
  ]
  node [
    id 105
    label "count"
  ]
  node [
    id 106
    label "pora_roku"
  ]
  node [
    id 107
    label "chody"
  ]
  node [
    id 108
    label "dno_lasu"
  ]
  node [
    id 109
    label "obr&#281;b"
  ]
  node [
    id 110
    label "&#347;ci&#243;&#322;ka"
  ]
  node [
    id 111
    label "podszyt"
  ]
  node [
    id 112
    label "rewir"
  ]
  node [
    id 113
    label "podrost"
  ]
  node [
    id 114
    label "teren"
  ]
  node [
    id 115
    label "le&#347;nictwo"
  ]
  node [
    id 116
    label "wykarczowanie"
  ]
  node [
    id 117
    label "runo"
  ]
  node [
    id 118
    label "teren_le&#347;ny"
  ]
  node [
    id 119
    label "wykarczowa&#263;"
  ]
  node [
    id 120
    label "mn&#243;stwo"
  ]
  node [
    id 121
    label "nadle&#347;nictwo"
  ]
  node [
    id 122
    label "formacja_ro&#347;linna"
  ]
  node [
    id 123
    label "zalesienie"
  ]
  node [
    id 124
    label "karczowa&#263;"
  ]
  node [
    id 125
    label "wiatro&#322;om"
  ]
  node [
    id 126
    label "karczowanie"
  ]
  node [
    id 127
    label "driada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 67
  ]
  edge [
    source 11
    target 68
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 77
  ]
  edge [
    source 14
    target 78
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 84
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 85
  ]
  edge [
    source 16
    target 86
  ]
  edge [
    source 16
    target 87
  ]
  edge [
    source 16
    target 88
  ]
  edge [
    source 16
    target 89
  ]
  edge [
    source 16
    target 90
  ]
  edge [
    source 16
    target 91
  ]
  edge [
    source 16
    target 92
  ]
  edge [
    source 16
    target 93
  ]
  edge [
    source 16
    target 94
  ]
  edge [
    source 16
    target 95
  ]
  edge [
    source 16
    target 96
  ]
  edge [
    source 16
    target 97
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 101
  ]
  edge [
    source 16
    target 102
  ]
  edge [
    source 16
    target 103
  ]
  edge [
    source 16
    target 104
  ]
  edge [
    source 16
    target 105
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 107
  ]
  edge [
    source 19
    target 108
  ]
  edge [
    source 19
    target 109
  ]
  edge [
    source 19
    target 110
  ]
  edge [
    source 19
    target 111
  ]
  edge [
    source 19
    target 112
  ]
  edge [
    source 19
    target 113
  ]
  edge [
    source 19
    target 114
  ]
  edge [
    source 19
    target 115
  ]
  edge [
    source 19
    target 116
  ]
  edge [
    source 19
    target 117
  ]
  edge [
    source 19
    target 118
  ]
  edge [
    source 19
    target 119
  ]
  edge [
    source 19
    target 120
  ]
  edge [
    source 19
    target 121
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 124
  ]
  edge [
    source 19
    target 125
  ]
  edge [
    source 19
    target 126
  ]
  edge [
    source 19
    target 127
  ]
]
