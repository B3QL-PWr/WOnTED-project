graph [
  maxDegree 19
  minDegree 1
  meanDegree 2
  density 0.018018018018018018
  graphCliqueNumber 3
  node [
    id 0
    label "tur"
    origin "text"
  ]
  node [
    id 1
    label "ozorek"
    origin "text"
  ]
  node [
    id 2
    label "g&#243;rnik"
    origin "text"
  ]
  node [
    id 3
    label "&#322;&#281;czyca"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "mecz"
    origin "text"
  ]
  node [
    id 6
    label "derbowy"
    origin "text"
  ]
  node [
    id 7
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 8
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 9
    label "si&#281;"
    origin "text"
  ]
  node [
    id 10
    label "stadion"
    origin "text"
  ]
  node [
    id 11
    label "przy"
    origin "text"
  ]
  node [
    id 12
    label "ulica"
    origin "text"
  ]
  node [
    id 13
    label "&#322;&#281;czycki"
    origin "text"
  ]
  node [
    id 14
    label "wst&#281;p"
    origin "text"
  ]
  node [
    id 15
    label "przekaza&#263;"
    origin "text"
  ]
  node [
    id 16
    label "cela"
    origin "text"
  ]
  node [
    id 17
    label "statutowy"
    origin "text"
  ]
  node [
    id 18
    label "klub"
    origin "text"
  ]
  node [
    id 19
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 20
    label "byd&#322;o"
  ]
  node [
    id 21
    label "ssak_wymar&#322;y"
  ]
  node [
    id 22
    label "pieczarkowiec"
  ]
  node [
    id 23
    label "ozorkowate"
  ]
  node [
    id 24
    label "saprotrof"
  ]
  node [
    id 25
    label "grzyb"
  ]
  node [
    id 26
    label "paso&#380;yt"
  ]
  node [
    id 27
    label "podroby"
  ]
  node [
    id 28
    label "banknot"
  ]
  node [
    id 29
    label "hawierz"
  ]
  node [
    id 30
    label "robotnik"
  ]
  node [
    id 31
    label "pi&#281;&#263;setz&#322;ot&#243;wka"
  ]
  node [
    id 32
    label "wydobywca"
  ]
  node [
    id 33
    label "rudnik"
  ]
  node [
    id 34
    label "kopalnia"
  ]
  node [
    id 35
    label "Barb&#243;rka"
  ]
  node [
    id 36
    label "si&#281;ga&#263;"
  ]
  node [
    id 37
    label "trwa&#263;"
  ]
  node [
    id 38
    label "obecno&#347;&#263;"
  ]
  node [
    id 39
    label "stan"
  ]
  node [
    id 40
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 41
    label "stand"
  ]
  node [
    id 42
    label "mie&#263;_miejsce"
  ]
  node [
    id 43
    label "uczestniczy&#263;"
  ]
  node [
    id 44
    label "chodzi&#263;"
  ]
  node [
    id 45
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 46
    label "equal"
  ]
  node [
    id 47
    label "obrona"
  ]
  node [
    id 48
    label "gra"
  ]
  node [
    id 49
    label "dwumecz"
  ]
  node [
    id 50
    label "game"
  ]
  node [
    id 51
    label "serw"
  ]
  node [
    id 52
    label "reserve"
  ]
  node [
    id 53
    label "przej&#347;&#263;"
  ]
  node [
    id 54
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 55
    label "obiekt"
  ]
  node [
    id 56
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 57
    label "zgromadzenie"
  ]
  node [
    id 58
    label "korona"
  ]
  node [
    id 59
    label "court"
  ]
  node [
    id 60
    label "trybuna"
  ]
  node [
    id 61
    label "budowla"
  ]
  node [
    id 62
    label "&#347;rodowisko"
  ]
  node [
    id 63
    label "miasteczko"
  ]
  node [
    id 64
    label "streetball"
  ]
  node [
    id 65
    label "pierzeja"
  ]
  node [
    id 66
    label "grupa"
  ]
  node [
    id 67
    label "pas_ruchu"
  ]
  node [
    id 68
    label "jezdnia"
  ]
  node [
    id 69
    label "pas_rozdzielczy"
  ]
  node [
    id 70
    label "droga"
  ]
  node [
    id 71
    label "korona_drogi"
  ]
  node [
    id 72
    label "uk&#322;ad_urbanistyczny"
  ]
  node [
    id 73
    label "chodnik"
  ]
  node [
    id 74
    label "arteria"
  ]
  node [
    id 75
    label "Broadway"
  ]
  node [
    id 76
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 77
    label "wysepka"
  ]
  node [
    id 78
    label "autostrada"
  ]
  node [
    id 79
    label "doj&#347;cie"
  ]
  node [
    id 80
    label "zapowied&#378;"
  ]
  node [
    id 81
    label "evocation"
  ]
  node [
    id 82
    label "g&#322;oska"
  ]
  node [
    id 83
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 84
    label "wymowa"
  ]
  node [
    id 85
    label "pocz&#261;tek"
  ]
  node [
    id 86
    label "utw&#243;r"
  ]
  node [
    id 87
    label "podstawy"
  ]
  node [
    id 88
    label "tekst"
  ]
  node [
    id 89
    label "impart"
  ]
  node [
    id 90
    label "sygna&#322;"
  ]
  node [
    id 91
    label "propagate"
  ]
  node [
    id 92
    label "transfer"
  ]
  node [
    id 93
    label "give"
  ]
  node [
    id 94
    label "wys&#322;a&#263;"
  ]
  node [
    id 95
    label "zrobi&#263;"
  ]
  node [
    id 96
    label "poda&#263;"
  ]
  node [
    id 97
    label "wp&#322;aci&#263;"
  ]
  node [
    id 98
    label "pomieszczenie"
  ]
  node [
    id 99
    label "klasztor"
  ]
  node [
    id 100
    label "programowy"
  ]
  node [
    id 101
    label "regulaminowy"
  ]
  node [
    id 102
    label "society"
  ]
  node [
    id 103
    label "jakobini"
  ]
  node [
    id 104
    label "klubista"
  ]
  node [
    id 105
    label "stowarzyszenie"
  ]
  node [
    id 106
    label "lokal"
  ]
  node [
    id 107
    label "od&#322;am"
  ]
  node [
    id 108
    label "siedziba"
  ]
  node [
    id 109
    label "bar"
  ]
  node [
    id 110
    label "tura"
  ]
  node [
    id 111
    label "&#321;&#281;czyca"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 110
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 111
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 55
  ]
  edge [
    source 10
    target 56
  ]
  edge [
    source 10
    target 57
  ]
  edge [
    source 10
    target 58
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 60
  ]
  edge [
    source 10
    target 61
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 62
  ]
  edge [
    source 12
    target 63
  ]
  edge [
    source 12
    target 64
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 12
    target 66
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 68
  ]
  edge [
    source 12
    target 69
  ]
  edge [
    source 12
    target 70
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 79
  ]
  edge [
    source 14
    target 80
  ]
  edge [
    source 14
    target 81
  ]
  edge [
    source 14
    target 82
  ]
  edge [
    source 14
    target 83
  ]
  edge [
    source 14
    target 84
  ]
  edge [
    source 14
    target 85
  ]
  edge [
    source 14
    target 86
  ]
  edge [
    source 14
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 89
  ]
  edge [
    source 15
    target 90
  ]
  edge [
    source 15
    target 91
  ]
  edge [
    source 15
    target 92
  ]
  edge [
    source 15
    target 93
  ]
  edge [
    source 15
    target 94
  ]
  edge [
    source 15
    target 95
  ]
  edge [
    source 15
    target 96
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 98
  ]
  edge [
    source 16
    target 99
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 100
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 18
    target 102
  ]
  edge [
    source 18
    target 103
  ]
  edge [
    source 18
    target 104
  ]
  edge [
    source 18
    target 105
  ]
  edge [
    source 18
    target 106
  ]
  edge [
    source 18
    target 107
  ]
  edge [
    source 18
    target 108
  ]
  edge [
    source 18
    target 109
  ]
]
