graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.049079754601227
  density 0.012648640460501402
  graphCliqueNumber 3
  node [
    id 0
    label "dziewczyna"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ten"
    origin "text"
  ]
  node [
    id 3
    label "chwila"
    origin "text"
  ]
  node [
    id 4
    label "stan"
    origin "text"
  ]
  node [
    id 5
    label "porusza&#263;"
    origin "text"
  ]
  node [
    id 6
    label "palce"
    origin "text"
  ]
  node [
    id 7
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 8
    label "czemu"
    origin "text"
  ]
  node [
    id 9
    label "da&#263;by&#263;"
    origin "text"
  ]
  node [
    id 10
    label "rada"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "zgada&#263;"
    origin "text"
  ]
  node [
    id 13
    label "kto&#347;"
    origin "text"
  ]
  node [
    id 14
    label "zechcie&#263;"
    origin "text"
  ]
  node [
    id 15
    label "pom&#243;c"
    origin "text"
  ]
  node [
    id 16
    label "uroczy"
    origin "text"
  ]
  node [
    id 17
    label "r&#243;&#380;owy"
    origin "text"
  ]
  node [
    id 18
    label "pasek"
    origin "text"
  ]
  node [
    id 19
    label "kwota"
    origin "text"
  ]
  node [
    id 20
    label "niesamowicie"
    origin "text"
  ]
  node [
    id 21
    label "wdzi&#281;czny"
    origin "text"
  ]
  node [
    id 22
    label "cz&#322;owiek"
  ]
  node [
    id 23
    label "dziewka"
  ]
  node [
    id 24
    label "dziewoja"
  ]
  node [
    id 25
    label "siksa"
  ]
  node [
    id 26
    label "partnerka"
  ]
  node [
    id 27
    label "dziewczynina"
  ]
  node [
    id 28
    label "dziunia"
  ]
  node [
    id 29
    label "sympatia"
  ]
  node [
    id 30
    label "dziewcz&#281;"
  ]
  node [
    id 31
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 32
    label "kora"
  ]
  node [
    id 33
    label "m&#322;&#243;dka"
  ]
  node [
    id 34
    label "dziecina"
  ]
  node [
    id 35
    label "sikorka"
  ]
  node [
    id 36
    label "si&#281;ga&#263;"
  ]
  node [
    id 37
    label "trwa&#263;"
  ]
  node [
    id 38
    label "obecno&#347;&#263;"
  ]
  node [
    id 39
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 40
    label "stand"
  ]
  node [
    id 41
    label "mie&#263;_miejsce"
  ]
  node [
    id 42
    label "uczestniczy&#263;"
  ]
  node [
    id 43
    label "chodzi&#263;"
  ]
  node [
    id 44
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 45
    label "equal"
  ]
  node [
    id 46
    label "okre&#347;lony"
  ]
  node [
    id 47
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 48
    label "czas"
  ]
  node [
    id 49
    label "time"
  ]
  node [
    id 50
    label "Arizona"
  ]
  node [
    id 51
    label "Georgia"
  ]
  node [
    id 52
    label "warstwa"
  ]
  node [
    id 53
    label "jednostka_administracyjna"
  ]
  node [
    id 54
    label "Goa"
  ]
  node [
    id 55
    label "Hawaje"
  ]
  node [
    id 56
    label "Floryda"
  ]
  node [
    id 57
    label "Oklahoma"
  ]
  node [
    id 58
    label "punkt"
  ]
  node [
    id 59
    label "Alaska"
  ]
  node [
    id 60
    label "Alabama"
  ]
  node [
    id 61
    label "wci&#281;cie"
  ]
  node [
    id 62
    label "Oregon"
  ]
  node [
    id 63
    label "poziom"
  ]
  node [
    id 64
    label "Teksas"
  ]
  node [
    id 65
    label "Illinois"
  ]
  node [
    id 66
    label "Jukatan"
  ]
  node [
    id 67
    label "Waszyngton"
  ]
  node [
    id 68
    label "shape"
  ]
  node [
    id 69
    label "Nowy_Meksyk"
  ]
  node [
    id 70
    label "ilo&#347;&#263;"
  ]
  node [
    id 71
    label "state"
  ]
  node [
    id 72
    label "Nowy_York"
  ]
  node [
    id 73
    label "Arakan"
  ]
  node [
    id 74
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 75
    label "Kalifornia"
  ]
  node [
    id 76
    label "wektor"
  ]
  node [
    id 77
    label "Massachusetts"
  ]
  node [
    id 78
    label "miejsce"
  ]
  node [
    id 79
    label "Pensylwania"
  ]
  node [
    id 80
    label "Maryland"
  ]
  node [
    id 81
    label "Michigan"
  ]
  node [
    id 82
    label "Ohio"
  ]
  node [
    id 83
    label "Kansas"
  ]
  node [
    id 84
    label "przestrze&#324;_fazowa"
  ]
  node [
    id 85
    label "Luizjana"
  ]
  node [
    id 86
    label "samopoczucie"
  ]
  node [
    id 87
    label "Wirginia"
  ]
  node [
    id 88
    label "&#347;mier&#263;_cieplna_Wszech&#347;wiata"
  ]
  node [
    id 89
    label "podnosi&#263;"
  ]
  node [
    id 90
    label "meet"
  ]
  node [
    id 91
    label "move"
  ]
  node [
    id 92
    label "act"
  ]
  node [
    id 93
    label "wzbudza&#263;"
  ]
  node [
    id 94
    label "porobi&#263;"
  ]
  node [
    id 95
    label "drive"
  ]
  node [
    id 96
    label "robi&#263;"
  ]
  node [
    id 97
    label "wypowiada&#263;_si&#281;"
  ]
  node [
    id 98
    label "go"
  ]
  node [
    id 99
    label "powodowa&#263;"
  ]
  node [
    id 100
    label "stopa"
  ]
  node [
    id 101
    label "dyskusja"
  ]
  node [
    id 102
    label "grupa"
  ]
  node [
    id 103
    label "Rada_Unii_Europejskiej"
  ]
  node [
    id 104
    label "conference"
  ]
  node [
    id 105
    label "organ"
  ]
  node [
    id 106
    label "zgromadzenie"
  ]
  node [
    id 107
    label "wskaz&#243;wka"
  ]
  node [
    id 108
    label "konsylium"
  ]
  node [
    id 109
    label "Rada_Europy"
  ]
  node [
    id 110
    label "Rada_Europejska"
  ]
  node [
    id 111
    label "posiedzenie"
  ]
  node [
    id 112
    label "znaczenie"
  ]
  node [
    id 113
    label "go&#347;&#263;"
  ]
  node [
    id 114
    label "osoba"
  ]
  node [
    id 115
    label "posta&#263;"
  ]
  node [
    id 116
    label "poczu&#263;"
  ]
  node [
    id 117
    label "desire"
  ]
  node [
    id 118
    label "help"
  ]
  node [
    id 119
    label "aid"
  ]
  node [
    id 120
    label "u&#322;atwi&#263;"
  ]
  node [
    id 121
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 122
    label "concur"
  ]
  node [
    id 123
    label "zrobi&#263;"
  ]
  node [
    id 124
    label "zaskutkowa&#263;"
  ]
  node [
    id 125
    label "sympatyczny"
  ]
  node [
    id 126
    label "uroczny"
  ]
  node [
    id 127
    label "uroczo"
  ]
  node [
    id 128
    label "&#322;adny"
  ]
  node [
    id 129
    label "zar&#243;&#380;owienie_si&#281;"
  ]
  node [
    id 130
    label "zar&#243;&#380;owienie"
  ]
  node [
    id 131
    label "r&#243;&#380;owo"
  ]
  node [
    id 132
    label "r&#243;&#380;owienie"
  ]
  node [
    id 133
    label "weso&#322;y"
  ]
  node [
    id 134
    label "czerwonawy"
  ]
  node [
    id 135
    label "optymistyczny"
  ]
  node [
    id 136
    label "zwi&#261;zek"
  ]
  node [
    id 137
    label "dyktando"
  ]
  node [
    id 138
    label "dodatek"
  ]
  node [
    id 139
    label "obiekt"
  ]
  node [
    id 140
    label "oznaka"
  ]
  node [
    id 141
    label "prevention"
  ]
  node [
    id 142
    label "spekulacja"
  ]
  node [
    id 143
    label "handel"
  ]
  node [
    id 144
    label "zone"
  ]
  node [
    id 145
    label "naszywka"
  ]
  node [
    id 146
    label "us&#322;uga"
  ]
  node [
    id 147
    label "przewi&#261;zka"
  ]
  node [
    id 148
    label "wynosi&#263;"
  ]
  node [
    id 149
    label "limit"
  ]
  node [
    id 150
    label "wynie&#347;&#263;"
  ]
  node [
    id 151
    label "pieni&#261;dze"
  ]
  node [
    id 152
    label "bardzo"
  ]
  node [
    id 153
    label "niesamowito"
  ]
  node [
    id 154
    label "niezwykle"
  ]
  node [
    id 155
    label "niesamowity"
  ]
  node [
    id 156
    label "przyjemny"
  ]
  node [
    id 157
    label "obowi&#261;zany"
  ]
  node [
    id 158
    label "jasny"
  ]
  node [
    id 159
    label "wdzi&#281;cznie"
  ]
  node [
    id 160
    label "przychylny"
  ]
  node [
    id 161
    label "zwinny"
  ]
  node [
    id 162
    label "satysfakcjonuj&#261;cy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 10
    target 106
  ]
  edge [
    source 10
    target 107
  ]
  edge [
    source 10
    target 108
  ]
  edge [
    source 10
    target 109
  ]
  edge [
    source 10
    target 110
  ]
  edge [
    source 10
    target 111
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 116
  ]
  edge [
    source 14
    target 117
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 118
  ]
  edge [
    source 15
    target 119
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 125
  ]
  edge [
    source 16
    target 126
  ]
  edge [
    source 16
    target 127
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 21
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 148
  ]
  edge [
    source 19
    target 149
  ]
  edge [
    source 19
    target 150
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 70
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 152
  ]
  edge [
    source 20
    target 153
  ]
  edge [
    source 20
    target 154
  ]
  edge [
    source 20
    target 155
  ]
  edge [
    source 21
    target 156
  ]
  edge [
    source 21
    target 157
  ]
  edge [
    source 21
    target 128
  ]
  edge [
    source 21
    target 158
  ]
  edge [
    source 21
    target 159
  ]
  edge [
    source 21
    target 160
  ]
  edge [
    source 21
    target 161
  ]
  edge [
    source 21
    target 162
  ]
]
