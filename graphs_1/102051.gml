graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.2474645030425964
  density 0.004568017282606903
  graphCliqueNumber 3
  node [
    id 0
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 1
    label "pisa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "nieprofesjonalny"
    origin "text"
  ]
  node [
    id 3
    label "z&#322;y"
    origin "text"
  ]
  node [
    id 4
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 5
    label "s&#322;owo"
    origin "text"
  ]
  node [
    id 6
    label "te&#380;"
    origin "text"
  ]
  node [
    id 7
    label "by&#263;"
    origin "text"
  ]
  node [
    id 8
    label "dobre"
    origin "text"
  ]
  node [
    id 9
    label "sugerowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "zdefiniowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "ten"
    origin "text"
  ]
  node [
    id 12
    label "warstwa"
    origin "text"
  ]
  node [
    id 13
    label "kulturowy"
    origin "text"
  ]
  node [
    id 14
    label "wystarczy&#263;"
    origin "text"
  ]
  node [
    id 15
    label "negacja"
    origin "text"
  ]
  node [
    id 16
    label "tak"
    origin "text"
  ]
  node [
    id 17
    label "gdyby"
    origin "text"
  ]
  node [
    id 18
    label "nazywa&#263;"
    origin "text"
  ]
  node [
    id 19
    label "telewizja"
    origin "text"
  ]
  node [
    id 20
    label "chwila"
    origin "text"
  ]
  node [
    id 21
    label "powstawa&#263;"
    origin "text"
  ]
  node [
    id 22
    label "wra&#380;enie"
    origin "text"
  ]
  node [
    id 23
    label "cykl"
    origin "text"
  ]
  node [
    id 24
    label "chcie&#263;byby&#263;"
    origin "text"
  ]
  node [
    id 25
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 26
    label "cudzys&#322;&#243;w"
    origin "text"
  ]
  node [
    id 27
    label "poza"
    origin "text"
  ]
  node [
    id 28
    label "tym"
    origin "text"
  ]
  node [
    id 29
    label "moje"
    origin "text"
  ]
  node [
    id 30
    label "du&#380;o"
    origin "text"
  ]
  node [
    id 31
    label "podej&#347;cie"
    origin "text"
  ]
  node [
    id 32
    label "metaforyczny"
    origin "text"
  ]
  node [
    id 33
    label "warto&#347;ciowa&#263;"
    origin "text"
  ]
  node [
    id 34
    label "metafora"
    origin "text"
  ]
  node [
    id 35
    label "dobro"
    origin "text"
  ]
  node [
    id 36
    label "versus"
    origin "text"
  ]
  node [
    id 37
    label "jakby"
    origin "text"
  ]
  node [
    id 38
    label "kultura"
    origin "text"
  ]
  node [
    id 39
    label "dworski"
    origin "text"
  ]
  node [
    id 40
    label "obiektywnie"
    origin "text"
  ]
  node [
    id 41
    label "komercyjny"
    origin "text"
  ]
  node [
    id 42
    label "przecie&#380;"
    origin "text"
  ]
  node [
    id 43
    label "je&#380;eli"
    origin "text"
  ]
  node [
    id 44
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 45
    label "konkretny"
    origin "text"
  ]
  node [
    id 46
    label "uk&#322;ad"
    origin "text"
  ]
  node [
    id 47
    label "odniesienie"
    origin "text"
  ]
  node [
    id 48
    label "sam"
    origin "text"
  ]
  node [
    id 49
    label "dobrowolski"
    origin "text"
  ]
  node [
    id 50
    label "wydawa&#263;"
    origin "text"
  ]
  node [
    id 51
    label "si&#281;"
    origin "text"
  ]
  node [
    id 52
    label "troch&#281;"
    origin "text"
  ]
  node [
    id 53
    label "podejrzany"
    origin "text"
  ]
  node [
    id 54
    label "pierwiastek"
    origin "text"
  ]
  node [
    id 55
    label "na&#347;ladownictwo"
    origin "text"
  ]
  node [
    id 56
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 57
    label "odgrywa&#263;"
    origin "text"
  ]
  node [
    id 58
    label "ch&#322;opski"
    origin "text"
  ]
  node [
    id 59
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 60
    label "rola"
    origin "text"
  ]
  node [
    id 61
    label "ale"
    origin "text"
  ]
  node [
    id 62
    label "jedyna"
    origin "text"
  ]
  node [
    id 63
    label "pewno"
    origin "text"
  ]
  node [
    id 64
    label "dominowa&#263;"
    origin "text"
  ]
  node [
    id 65
    label "si&#281;gn&#261;&#263;"
    origin "text"
  ]
  node [
    id 66
    label "cho&#263;by"
    origin "text"
  ]
  node [
    id 67
    label "ch&#322;op"
    origin "text"
  ]
  node [
    id 68
    label "reymont"
    origin "text"
  ]
  node [
    id 69
    label "tyle"
    origin "text"
  ]
  node [
    id 70
    label "raz"
    origin "text"
  ]
  node [
    id 71
    label "w&#261;tpliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 72
    label "zastrze&#380;enie"
    origin "text"
  ]
  node [
    id 73
    label "natomiast"
    origin "text"
  ]
  node [
    id 74
    label "problem"
    origin "text"
  ]
  node [
    id 75
    label "ciekawy"
    origin "text"
  ]
  node [
    id 76
    label "ozdabia&#263;"
  ]
  node [
    id 77
    label "dysgrafia"
  ]
  node [
    id 78
    label "prasa"
  ]
  node [
    id 79
    label "spell"
  ]
  node [
    id 80
    label "skryba"
  ]
  node [
    id 81
    label "donosi&#263;"
  ]
  node [
    id 82
    label "code"
  ]
  node [
    id 83
    label "tekst"
  ]
  node [
    id 84
    label "dysortografia"
  ]
  node [
    id 85
    label "read"
  ]
  node [
    id 86
    label "tworzy&#263;"
  ]
  node [
    id 87
    label "formu&#322;owa&#263;"
  ]
  node [
    id 88
    label "styl"
  ]
  node [
    id 89
    label "stawia&#263;"
  ]
  node [
    id 90
    label "po_laicku"
  ]
  node [
    id 91
    label "niezawodowy"
  ]
  node [
    id 92
    label "s&#322;aby"
  ]
  node [
    id 93
    label "amatorsko"
  ]
  node [
    id 94
    label "zdenerwowany"
  ]
  node [
    id 95
    label "zez&#322;oszczenie"
  ]
  node [
    id 96
    label "rozz&#322;oszczenie_si&#281;"
  ]
  node [
    id 97
    label "gniewanie"
  ]
  node [
    id 98
    label "niekorzystny"
  ]
  node [
    id 99
    label "niemoralny"
  ]
  node [
    id 100
    label "niegrzeczny"
  ]
  node [
    id 101
    label "pieski"
  ]
  node [
    id 102
    label "negatywny"
  ]
  node [
    id 103
    label "&#378;le"
  ]
  node [
    id 104
    label "syf"
  ]
  node [
    id 105
    label "niew&#322;a&#347;ciwy"
  ]
  node [
    id 106
    label "sierdzisty"
  ]
  node [
    id 107
    label "z&#322;oszczenie"
  ]
  node [
    id 108
    label "rozgniewanie"
  ]
  node [
    id 109
    label "niepomy&#347;lny"
  ]
  node [
    id 110
    label "obietnica"
  ]
  node [
    id 111
    label "bit"
  ]
  node [
    id 112
    label "s&#322;ownictwo"
  ]
  node [
    id 113
    label "jednostka_leksykalna"
  ]
  node [
    id 114
    label "pisanie_si&#281;"
  ]
  node [
    id 115
    label "wykrzyknik"
  ]
  node [
    id 116
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 117
    label "pole_semantyczne"
  ]
  node [
    id 118
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 119
    label "komunikat"
  ]
  node [
    id 120
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 121
    label "wypowiedzenie"
  ]
  node [
    id 122
    label "nag&#322;os"
  ]
  node [
    id 123
    label "wordnet"
  ]
  node [
    id 124
    label "morfem"
  ]
  node [
    id 125
    label "czasownik"
  ]
  node [
    id 126
    label "wyg&#322;os"
  ]
  node [
    id 127
    label "jednostka_informacji"
  ]
  node [
    id 128
    label "si&#281;ga&#263;"
  ]
  node [
    id 129
    label "trwa&#263;"
  ]
  node [
    id 130
    label "obecno&#347;&#263;"
  ]
  node [
    id 131
    label "stan"
  ]
  node [
    id 132
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 133
    label "stand"
  ]
  node [
    id 134
    label "mie&#263;_miejsce"
  ]
  node [
    id 135
    label "uczestniczy&#263;"
  ]
  node [
    id 136
    label "chodzi&#263;"
  ]
  node [
    id 137
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 138
    label "equal"
  ]
  node [
    id 139
    label "render"
  ]
  node [
    id 140
    label "sugestia"
  ]
  node [
    id 141
    label "nasuwa&#263;"
  ]
  node [
    id 142
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 143
    label "podpowiada&#263;"
  ]
  node [
    id 144
    label "hint"
  ]
  node [
    id 145
    label "okre&#347;li&#263;"
  ]
  node [
    id 146
    label "okre&#347;lony"
  ]
  node [
    id 147
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 148
    label "przek&#322;adaniec"
  ]
  node [
    id 149
    label "covering"
  ]
  node [
    id 150
    label "podwarstwa"
  ]
  node [
    id 151
    label "zbi&#243;r"
  ]
  node [
    id 152
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 153
    label "p&#322;aszczyzna"
  ]
  node [
    id 154
    label "kulturowo"
  ]
  node [
    id 155
    label "spowodowa&#263;"
  ]
  node [
    id 156
    label "stan&#261;&#263;"
  ]
  node [
    id 157
    label "dosta&#263;"
  ]
  node [
    id 158
    label "suffice"
  ]
  node [
    id 159
    label "zaspokoi&#263;"
  ]
  node [
    id 160
    label "negation"
  ]
  node [
    id 161
    label "konektyw"
  ]
  node [
    id 162
    label "wypowied&#378;"
  ]
  node [
    id 163
    label "give"
  ]
  node [
    id 164
    label "mieni&#263;"
  ]
  node [
    id 165
    label "okre&#347;la&#263;"
  ]
  node [
    id 166
    label "nadawa&#263;"
  ]
  node [
    id 167
    label "Polsat"
  ]
  node [
    id 168
    label "paj&#281;czarz"
  ]
  node [
    id 169
    label "gadaj&#261;ca_g&#322;owa"
  ]
  node [
    id 170
    label "programowiec"
  ]
  node [
    id 171
    label "technologia"
  ]
  node [
    id 172
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 173
    label "Interwizja"
  ]
  node [
    id 174
    label "BBC"
  ]
  node [
    id 175
    label "ekran"
  ]
  node [
    id 176
    label "redakcja"
  ]
  node [
    id 177
    label "media"
  ]
  node [
    id 178
    label "odbieranie"
  ]
  node [
    id 179
    label "odbiera&#263;"
  ]
  node [
    id 180
    label "odbiornik"
  ]
  node [
    id 181
    label "instytucja"
  ]
  node [
    id 182
    label "du&#380;y_pok&#243;j"
  ]
  node [
    id 183
    label "studio"
  ]
  node [
    id 184
    label "telekomunikacja"
  ]
  node [
    id 185
    label "muza"
  ]
  node [
    id 186
    label "czas"
  ]
  node [
    id 187
    label "time"
  ]
  node [
    id 188
    label "buntowa&#263;_si&#281;"
  ]
  node [
    id 189
    label "rise"
  ]
  node [
    id 190
    label "spring"
  ]
  node [
    id 191
    label "stawa&#263;"
  ]
  node [
    id 192
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 193
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 194
    label "plot"
  ]
  node [
    id 195
    label "podnosi&#263;_si&#281;"
  ]
  node [
    id 196
    label "uwalnia&#263;_si&#281;"
  ]
  node [
    id 197
    label "publish"
  ]
  node [
    id 198
    label "reakcja"
  ]
  node [
    id 199
    label "odczucia"
  ]
  node [
    id 200
    label "czucie"
  ]
  node [
    id 201
    label "poczucie"
  ]
  node [
    id 202
    label "zmys&#322;"
  ]
  node [
    id 203
    label "przeczulica"
  ]
  node [
    id 204
    label "proces"
  ]
  node [
    id 205
    label "zjawisko"
  ]
  node [
    id 206
    label "sekwencja"
  ]
  node [
    id 207
    label "edycja"
  ]
  node [
    id 208
    label "przebieg"
  ]
  node [
    id 209
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 210
    label "okres"
  ]
  node [
    id 211
    label "cykl_p&#322;ciowy"
  ]
  node [
    id 212
    label "cycle"
  ]
  node [
    id 213
    label "owulacja"
  ]
  node [
    id 214
    label "miesi&#261;czka"
  ]
  node [
    id 215
    label "set"
  ]
  node [
    id 216
    label "znak_interpunkcyjny"
  ]
  node [
    id 217
    label "mode"
  ]
  node [
    id 218
    label "gra"
  ]
  node [
    id 219
    label "przesada"
  ]
  node [
    id 220
    label "ustawienie"
  ]
  node [
    id 221
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 222
    label "du&#380;y"
  ]
  node [
    id 223
    label "cz&#281;sto"
  ]
  node [
    id 224
    label "bardzo"
  ]
  node [
    id 225
    label "mocno"
  ]
  node [
    id 226
    label "wiela"
  ]
  node [
    id 227
    label "powaga"
  ]
  node [
    id 228
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 229
    label "cecha"
  ]
  node [
    id 230
    label "droga"
  ]
  node [
    id 231
    label "ploy"
  ]
  node [
    id 232
    label "nastawienie"
  ]
  node [
    id 233
    label "potraktowanie"
  ]
  node [
    id 234
    label "nasi&#261;kni&#281;cie"
  ]
  node [
    id 235
    label "nabranie"
  ]
  node [
    id 236
    label "przeno&#347;nie"
  ]
  node [
    id 237
    label "niedos&#322;owny"
  ]
  node [
    id 238
    label "os&#261;dza&#263;"
  ]
  node [
    id 239
    label "figura_stylistyczna"
  ]
  node [
    id 240
    label "sformu&#322;owanie"
  ]
  node [
    id 241
    label "metaforyka"
  ]
  node [
    id 242
    label "metaphor"
  ]
  node [
    id 243
    label "warto&#347;&#263;"
  ]
  node [
    id 244
    label "dobra"
  ]
  node [
    id 245
    label "kalokagatia"
  ]
  node [
    id 246
    label "krzywa_Engla"
  ]
  node [
    id 247
    label "u&#380;yteczno&#347;&#263;_ca&#322;kowita"
  ]
  node [
    id 248
    label "rzecz"
  ]
  node [
    id 249
    label "despond"
  ]
  node [
    id 250
    label "g&#322;agolica"
  ]
  node [
    id 251
    label "cel"
  ]
  node [
    id 252
    label "go&#322;&#261;bek"
  ]
  node [
    id 253
    label "dobro&#263;"
  ]
  node [
    id 254
    label "litera"
  ]
  node [
    id 255
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 256
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 257
    label "przedmiot"
  ]
  node [
    id 258
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 259
    label "Wsch&#243;d"
  ]
  node [
    id 260
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 261
    label "sztuka"
  ]
  node [
    id 262
    label "religia"
  ]
  node [
    id 263
    label "przejmowa&#263;"
  ]
  node [
    id 264
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 265
    label "makrokosmos"
  ]
  node [
    id 266
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 267
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 268
    label "praca_rolnicza"
  ]
  node [
    id 269
    label "tradycja"
  ]
  node [
    id 270
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 271
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 272
    label "przejmowanie"
  ]
  node [
    id 273
    label "asymilowanie_si&#281;"
  ]
  node [
    id 274
    label "przej&#261;&#263;"
  ]
  node [
    id 275
    label "hodowla"
  ]
  node [
    id 276
    label "brzoskwiniarnia"
  ]
  node [
    id 277
    label "populace"
  ]
  node [
    id 278
    label "konwencja"
  ]
  node [
    id 279
    label "propriety"
  ]
  node [
    id 280
    label "jako&#347;&#263;"
  ]
  node [
    id 281
    label "kuchnia"
  ]
  node [
    id 282
    label "zwyczaj"
  ]
  node [
    id 283
    label "przej&#281;cie"
  ]
  node [
    id 284
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 285
    label "elegancki"
  ]
  node [
    id 286
    label "dwornie"
  ]
  node [
    id 287
    label "grzeczny"
  ]
  node [
    id 288
    label "po_dworsku"
  ]
  node [
    id 289
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 290
    label "faktycznie"
  ]
  node [
    id 291
    label "obiektywny"
  ]
  node [
    id 292
    label "niezale&#380;nie"
  ]
  node [
    id 293
    label "skomercjalizowanie"
  ]
  node [
    id 294
    label "rynkowy"
  ]
  node [
    id 295
    label "komercjalizowanie"
  ]
  node [
    id 296
    label "masowy"
  ]
  node [
    id 297
    label "komercyjnie"
  ]
  node [
    id 298
    label "tre&#347;ciwy"
  ]
  node [
    id 299
    label "&#322;adny"
  ]
  node [
    id 300
    label "jasny"
  ]
  node [
    id 301
    label "ogarni&#281;ty"
  ]
  node [
    id 302
    label "po&#380;ywny"
  ]
  node [
    id 303
    label "skupiony"
  ]
  node [
    id 304
    label "konkretnie"
  ]
  node [
    id 305
    label "posilny"
  ]
  node [
    id 306
    label "abstrakcyjny"
  ]
  node [
    id 307
    label "solidnie"
  ]
  node [
    id 308
    label "niez&#322;y"
  ]
  node [
    id 309
    label "sk&#322;ad"
  ]
  node [
    id 310
    label "zachowanie"
  ]
  node [
    id 311
    label "umowa"
  ]
  node [
    id 312
    label "podsystem"
  ]
  node [
    id 313
    label "zesp&#243;&#322;_urz&#261;dze&#324;"
  ]
  node [
    id 314
    label "przeciwsobno&#347;&#263;"
  ]
  node [
    id 315
    label "system"
  ]
  node [
    id 316
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 317
    label "struktura"
  ]
  node [
    id 318
    label "wi&#281;&#378;"
  ]
  node [
    id 319
    label "zawarcie"
  ]
  node [
    id 320
    label "systemat"
  ]
  node [
    id 321
    label "usenet"
  ]
  node [
    id 322
    label "ONZ"
  ]
  node [
    id 323
    label "o&#347;"
  ]
  node [
    id 324
    label "organ"
  ]
  node [
    id 325
    label "przestawi&#263;"
  ]
  node [
    id 326
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 327
    label "traktat_wersalski"
  ]
  node [
    id 328
    label "rozprz&#261;c"
  ]
  node [
    id 329
    label "cybernetyk"
  ]
  node [
    id 330
    label "cia&#322;o"
  ]
  node [
    id 331
    label "zawrze&#263;"
  ]
  node [
    id 332
    label "konstelacja"
  ]
  node [
    id 333
    label "alliance"
  ]
  node [
    id 334
    label "NATO"
  ]
  node [
    id 335
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 336
    label "treaty"
  ]
  node [
    id 337
    label "od&#322;o&#380;enie"
  ]
  node [
    id 338
    label "skill"
  ]
  node [
    id 339
    label "doznanie"
  ]
  node [
    id 340
    label "gaze"
  ]
  node [
    id 341
    label "deference"
  ]
  node [
    id 342
    label "dochrapanie_si&#281;"
  ]
  node [
    id 343
    label "dostarczenie"
  ]
  node [
    id 344
    label "mention"
  ]
  node [
    id 345
    label "bearing"
  ]
  node [
    id 346
    label "po&#380;yczenie"
  ]
  node [
    id 347
    label "uzyskanie"
  ]
  node [
    id 348
    label "oddanie"
  ]
  node [
    id 349
    label "powi&#261;zanie"
  ]
  node [
    id 350
    label "sklep"
  ]
  node [
    id 351
    label "impart"
  ]
  node [
    id 352
    label "panna_na_wydaniu"
  ]
  node [
    id 353
    label "surrender"
  ]
  node [
    id 354
    label "zwraca&#263;_si&#281;"
  ]
  node [
    id 355
    label "train"
  ]
  node [
    id 356
    label "wytwarza&#263;"
  ]
  node [
    id 357
    label "dawa&#263;"
  ]
  node [
    id 358
    label "zapach"
  ]
  node [
    id 359
    label "wprowadza&#263;"
  ]
  node [
    id 360
    label "ujawnia&#263;"
  ]
  node [
    id 361
    label "wydawnictwo"
  ]
  node [
    id 362
    label "powierza&#263;"
  ]
  node [
    id 363
    label "produkcja"
  ]
  node [
    id 364
    label "denuncjowa&#263;"
  ]
  node [
    id 365
    label "plon"
  ]
  node [
    id 366
    label "reszta"
  ]
  node [
    id 367
    label "robi&#263;"
  ]
  node [
    id 368
    label "placard"
  ]
  node [
    id 369
    label "tajemnica"
  ]
  node [
    id 370
    label "wiano"
  ]
  node [
    id 371
    label "kojarzy&#263;"
  ]
  node [
    id 372
    label "d&#378;wi&#281;k"
  ]
  node [
    id 373
    label "podawa&#263;"
  ]
  node [
    id 374
    label "s&#261;d"
  ]
  node [
    id 375
    label "nieprzejrzysty"
  ]
  node [
    id 376
    label "pos&#261;dzanie"
  ]
  node [
    id 377
    label "podejrzanie"
  ]
  node [
    id 378
    label "podmiot"
  ]
  node [
    id 379
    label "niepewny"
  ]
  node [
    id 380
    label "root"
  ]
  node [
    id 381
    label "substancja_chemiczna"
  ]
  node [
    id 382
    label "liczba"
  ]
  node [
    id 383
    label "sk&#322;adnik"
  ]
  node [
    id 384
    label "model"
  ]
  node [
    id 385
    label "kopia"
  ]
  node [
    id 386
    label "imitacja"
  ]
  node [
    id 387
    label "uprawi&#263;"
  ]
  node [
    id 388
    label "gotowy"
  ]
  node [
    id 389
    label "might"
  ]
  node [
    id 390
    label "wykonywa&#263;"
  ]
  node [
    id 391
    label "prezentowa&#263;"
  ]
  node [
    id 392
    label "deal"
  ]
  node [
    id 393
    label "play"
  ]
  node [
    id 394
    label "rozgrywa&#263;"
  ]
  node [
    id 395
    label "m&#281;ski"
  ]
  node [
    id 396
    label "typowy"
  ]
  node [
    id 397
    label "po_ch&#322;opsku"
  ]
  node [
    id 398
    label "jako&#347;"
  ]
  node [
    id 399
    label "charakterystyczny"
  ]
  node [
    id 400
    label "jako_tako"
  ]
  node [
    id 401
    label "dziwny"
  ]
  node [
    id 402
    label "przyzwoity"
  ]
  node [
    id 403
    label "pole"
  ]
  node [
    id 404
    label "znaczenie"
  ]
  node [
    id 405
    label "ziemia"
  ]
  node [
    id 406
    label "zastosowanie"
  ]
  node [
    id 407
    label "zreinterpretowa&#263;"
  ]
  node [
    id 408
    label "zreinterpretowanie"
  ]
  node [
    id 409
    label "function"
  ]
  node [
    id 410
    label "zagranie"
  ]
  node [
    id 411
    label "p&#322;osa"
  ]
  node [
    id 412
    label "plik"
  ]
  node [
    id 413
    label "reinterpretowanie"
  ]
  node [
    id 414
    label "uprawienie"
  ]
  node [
    id 415
    label "gra&#263;"
  ]
  node [
    id 416
    label "radlina"
  ]
  node [
    id 417
    label "ustawi&#263;"
  ]
  node [
    id 418
    label "irygowa&#263;"
  ]
  node [
    id 419
    label "wrench"
  ]
  node [
    id 420
    label "irygowanie"
  ]
  node [
    id 421
    label "dialog"
  ]
  node [
    id 422
    label "zagon"
  ]
  node [
    id 423
    label "scenariusz"
  ]
  node [
    id 424
    label "zagra&#263;"
  ]
  node [
    id 425
    label "kszta&#322;t"
  ]
  node [
    id 426
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 427
    label "czyn"
  ]
  node [
    id 428
    label "gospodarstwo"
  ]
  node [
    id 429
    label "reinterpretowa&#263;"
  ]
  node [
    id 430
    label "granie"
  ]
  node [
    id 431
    label "wykonywanie"
  ]
  node [
    id 432
    label "aktorstwo"
  ]
  node [
    id 433
    label "kostium"
  ]
  node [
    id 434
    label "posta&#263;"
  ]
  node [
    id 435
    label "piwo"
  ]
  node [
    id 436
    label "kobieta"
  ]
  node [
    id 437
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 438
    label "control"
  ]
  node [
    id 439
    label "allude"
  ]
  node [
    id 440
    label "dotrze&#263;"
  ]
  node [
    id 441
    label "appreciation"
  ]
  node [
    id 442
    label "u&#380;y&#263;"
  ]
  node [
    id 443
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 444
    label "seize"
  ]
  node [
    id 445
    label "fall_upon"
  ]
  node [
    id 446
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 447
    label "skorzysta&#263;"
  ]
  node [
    id 448
    label "cz&#322;owiek"
  ]
  node [
    id 449
    label "partner"
  ]
  node [
    id 450
    label "cham"
  ]
  node [
    id 451
    label "ch&#322;opstwo"
  ]
  node [
    id 452
    label "przedstawiciel"
  ]
  node [
    id 453
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 454
    label "rolnik"
  ]
  node [
    id 455
    label "uw&#322;aszcza&#263;"
  ]
  node [
    id 456
    label "w&#322;o&#347;cianin"
  ]
  node [
    id 457
    label "uw&#322;aszczanie"
  ]
  node [
    id 458
    label "bamber"
  ]
  node [
    id 459
    label "m&#261;&#380;"
  ]
  node [
    id 460
    label "uw&#322;aszczy&#263;"
  ]
  node [
    id 461
    label "facet"
  ]
  node [
    id 462
    label "prawo_wychodu"
  ]
  node [
    id 463
    label "wiele"
  ]
  node [
    id 464
    label "nieznacznie"
  ]
  node [
    id 465
    label "uderzenie"
  ]
  node [
    id 466
    label "cios"
  ]
  node [
    id 467
    label "w&#261;tpienie"
  ]
  node [
    id 468
    label "wytw&#243;r"
  ]
  node [
    id 469
    label "question"
  ]
  node [
    id 470
    label "uprzedzenie"
  ]
  node [
    id 471
    label "wym&#243;wienie"
  ]
  node [
    id 472
    label "restriction"
  ]
  node [
    id 473
    label "zapewnienie"
  ]
  node [
    id 474
    label "condition"
  ]
  node [
    id 475
    label "trudno&#347;&#263;"
  ]
  node [
    id 476
    label "sprawa"
  ]
  node [
    id 477
    label "ambaras"
  ]
  node [
    id 478
    label "problemat"
  ]
  node [
    id 479
    label "pierepa&#322;ka"
  ]
  node [
    id 480
    label "obstruction"
  ]
  node [
    id 481
    label "problematyka"
  ]
  node [
    id 482
    label "jajko_Kolumba"
  ]
  node [
    id 483
    label "subiekcja"
  ]
  node [
    id 484
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 485
    label "swoisty"
  ]
  node [
    id 486
    label "interesowanie"
  ]
  node [
    id 487
    label "nietuzinkowy"
  ]
  node [
    id 488
    label "ciekawie"
  ]
  node [
    id 489
    label "indagator"
  ]
  node [
    id 490
    label "interesuj&#261;cy"
  ]
  node [
    id 491
    label "intryguj&#261;cy"
  ]
  node [
    id 492
    label "ch&#281;tny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 76
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 3
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 94
  ]
  edge [
    source 3
    target 95
  ]
  edge [
    source 3
    target 96
  ]
  edge [
    source 3
    target 97
  ]
  edge [
    source 3
    target 98
  ]
  edge [
    source 3
    target 99
  ]
  edge [
    source 3
    target 100
  ]
  edge [
    source 3
    target 101
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 9
  ]
  edge [
    source 5
    target 110
  ]
  edge [
    source 5
    target 111
  ]
  edge [
    source 5
    target 112
  ]
  edge [
    source 5
    target 113
  ]
  edge [
    source 5
    target 114
  ]
  edge [
    source 5
    target 115
  ]
  edge [
    source 5
    target 116
  ]
  edge [
    source 5
    target 117
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 145
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 11
    target 147
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 46
  ]
  edge [
    source 12
    target 65
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 14
    target 159
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 36
  ]
  edge [
    source 16
    target 37
  ]
  edge [
    source 16
    target 42
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 32
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 168
  ]
  edge [
    source 19
    target 169
  ]
  edge [
    source 19
    target 170
  ]
  edge [
    source 19
    target 171
  ]
  edge [
    source 19
    target 172
  ]
  edge [
    source 19
    target 173
  ]
  edge [
    source 19
    target 174
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 177
  ]
  edge [
    source 19
    target 178
  ]
  edge [
    source 19
    target 179
  ]
  edge [
    source 19
    target 180
  ]
  edge [
    source 19
    target 181
  ]
  edge [
    source 19
    target 182
  ]
  edge [
    source 19
    target 183
  ]
  edge [
    source 19
    target 184
  ]
  edge [
    source 19
    target 185
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 70
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 198
  ]
  edge [
    source 22
    target 199
  ]
  edge [
    source 22
    target 200
  ]
  edge [
    source 22
    target 201
  ]
  edge [
    source 22
    target 202
  ]
  edge [
    source 22
    target 203
  ]
  edge [
    source 22
    target 204
  ]
  edge [
    source 22
    target 205
  ]
  edge [
    source 22
    target 57
  ]
  edge [
    source 22
    target 64
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 206
  ]
  edge [
    source 23
    target 186
  ]
  edge [
    source 23
    target 207
  ]
  edge [
    source 23
    target 208
  ]
  edge [
    source 23
    target 209
  ]
  edge [
    source 23
    target 210
  ]
  edge [
    source 23
    target 211
  ]
  edge [
    source 23
    target 212
  ]
  edge [
    source 23
    target 213
  ]
  edge [
    source 23
    target 214
  ]
  edge [
    source 23
    target 215
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 216
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 217
  ]
  edge [
    source 27
    target 218
  ]
  edge [
    source 27
    target 219
  ]
  edge [
    source 27
    target 220
  ]
  edge [
    source 27
    target 221
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 31
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 222
  ]
  edge [
    source 30
    target 223
  ]
  edge [
    source 30
    target 224
  ]
  edge [
    source 30
    target 225
  ]
  edge [
    source 30
    target 226
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 227
  ]
  edge [
    source 31
    target 228
  ]
  edge [
    source 31
    target 229
  ]
  edge [
    source 31
    target 230
  ]
  edge [
    source 31
    target 231
  ]
  edge [
    source 31
    target 232
  ]
  edge [
    source 31
    target 233
  ]
  edge [
    source 31
    target 234
  ]
  edge [
    source 31
    target 235
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 236
  ]
  edge [
    source 32
    target 237
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 44
  ]
  edge [
    source 33
    target 45
  ]
  edge [
    source 33
    target 238
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 239
  ]
  edge [
    source 34
    target 240
  ]
  edge [
    source 34
    target 241
  ]
  edge [
    source 34
    target 242
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 243
  ]
  edge [
    source 35
    target 244
  ]
  edge [
    source 35
    target 245
  ]
  edge [
    source 35
    target 246
  ]
  edge [
    source 35
    target 247
  ]
  edge [
    source 35
    target 248
  ]
  edge [
    source 35
    target 249
  ]
  edge [
    source 35
    target 250
  ]
  edge [
    source 35
    target 251
  ]
  edge [
    source 35
    target 252
  ]
  edge [
    source 35
    target 253
  ]
  edge [
    source 35
    target 254
  ]
  edge [
    source 35
    target 255
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 57
  ]
  edge [
    source 38
    target 58
  ]
  edge [
    source 38
    target 256
  ]
  edge [
    source 38
    target 257
  ]
  edge [
    source 38
    target 258
  ]
  edge [
    source 38
    target 259
  ]
  edge [
    source 38
    target 248
  ]
  edge [
    source 38
    target 260
  ]
  edge [
    source 38
    target 261
  ]
  edge [
    source 38
    target 262
  ]
  edge [
    source 38
    target 263
  ]
  edge [
    source 38
    target 264
  ]
  edge [
    source 38
    target 265
  ]
  edge [
    source 38
    target 266
  ]
  edge [
    source 38
    target 267
  ]
  edge [
    source 38
    target 205
  ]
  edge [
    source 38
    target 268
  ]
  edge [
    source 38
    target 269
  ]
  edge [
    source 38
    target 270
  ]
  edge [
    source 38
    target 271
  ]
  edge [
    source 38
    target 272
  ]
  edge [
    source 38
    target 229
  ]
  edge [
    source 38
    target 273
  ]
  edge [
    source 38
    target 274
  ]
  edge [
    source 38
    target 275
  ]
  edge [
    source 38
    target 276
  ]
  edge [
    source 38
    target 277
  ]
  edge [
    source 38
    target 278
  ]
  edge [
    source 38
    target 279
  ]
  edge [
    source 38
    target 280
  ]
  edge [
    source 38
    target 281
  ]
  edge [
    source 38
    target 282
  ]
  edge [
    source 38
    target 283
  ]
  edge [
    source 38
    target 284
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 285
  ]
  edge [
    source 39
    target 286
  ]
  edge [
    source 39
    target 287
  ]
  edge [
    source 39
    target 288
  ]
  edge [
    source 39
    target 289
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 290
  ]
  edge [
    source 40
    target 291
  ]
  edge [
    source 40
    target 292
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 293
  ]
  edge [
    source 41
    target 294
  ]
  edge [
    source 41
    target 295
  ]
  edge [
    source 41
    target 296
  ]
  edge [
    source 41
    target 297
  ]
  edge [
    source 42
    target 61
  ]
  edge [
    source 42
    target 62
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 47
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 298
  ]
  edge [
    source 45
    target 299
  ]
  edge [
    source 45
    target 300
  ]
  edge [
    source 45
    target 146
  ]
  edge [
    source 45
    target 301
  ]
  edge [
    source 45
    target 59
  ]
  edge [
    source 45
    target 302
  ]
  edge [
    source 45
    target 303
  ]
  edge [
    source 45
    target 304
  ]
  edge [
    source 45
    target 305
  ]
  edge [
    source 45
    target 306
  ]
  edge [
    source 45
    target 307
  ]
  edge [
    source 45
    target 308
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 309
  ]
  edge [
    source 46
    target 310
  ]
  edge [
    source 46
    target 311
  ]
  edge [
    source 46
    target 312
  ]
  edge [
    source 46
    target 313
  ]
  edge [
    source 46
    target 314
  ]
  edge [
    source 46
    target 315
  ]
  edge [
    source 46
    target 316
  ]
  edge [
    source 46
    target 209
  ]
  edge [
    source 46
    target 317
  ]
  edge [
    source 46
    target 318
  ]
  edge [
    source 46
    target 319
  ]
  edge [
    source 46
    target 320
  ]
  edge [
    source 46
    target 321
  ]
  edge [
    source 46
    target 322
  ]
  edge [
    source 46
    target 323
  ]
  edge [
    source 46
    target 324
  ]
  edge [
    source 46
    target 325
  ]
  edge [
    source 46
    target 326
  ]
  edge [
    source 46
    target 327
  ]
  edge [
    source 46
    target 328
  ]
  edge [
    source 46
    target 329
  ]
  edge [
    source 46
    target 330
  ]
  edge [
    source 46
    target 331
  ]
  edge [
    source 46
    target 332
  ]
  edge [
    source 46
    target 333
  ]
  edge [
    source 46
    target 151
  ]
  edge [
    source 46
    target 334
  ]
  edge [
    source 46
    target 335
  ]
  edge [
    source 46
    target 336
  ]
  edge [
    source 46
    target 65
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 162
  ]
  edge [
    source 47
    target 337
  ]
  edge [
    source 47
    target 338
  ]
  edge [
    source 47
    target 339
  ]
  edge [
    source 47
    target 340
  ]
  edge [
    source 47
    target 341
  ]
  edge [
    source 47
    target 342
  ]
  edge [
    source 47
    target 343
  ]
  edge [
    source 47
    target 344
  ]
  edge [
    source 47
    target 345
  ]
  edge [
    source 47
    target 346
  ]
  edge [
    source 47
    target 251
  ]
  edge [
    source 47
    target 347
  ]
  edge [
    source 47
    target 348
  ]
  edge [
    source 47
    target 349
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 350
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 351
  ]
  edge [
    source 50
    target 352
  ]
  edge [
    source 50
    target 353
  ]
  edge [
    source 50
    target 354
  ]
  edge [
    source 50
    target 355
  ]
  edge [
    source 50
    target 163
  ]
  edge [
    source 50
    target 356
  ]
  edge [
    source 50
    target 357
  ]
  edge [
    source 50
    target 358
  ]
  edge [
    source 50
    target 359
  ]
  edge [
    source 50
    target 360
  ]
  edge [
    source 50
    target 361
  ]
  edge [
    source 50
    target 362
  ]
  edge [
    source 50
    target 363
  ]
  edge [
    source 50
    target 364
  ]
  edge [
    source 50
    target 134
  ]
  edge [
    source 50
    target 365
  ]
  edge [
    source 50
    target 366
  ]
  edge [
    source 50
    target 367
  ]
  edge [
    source 50
    target 368
  ]
  edge [
    source 50
    target 369
  ]
  edge [
    source 50
    target 370
  ]
  edge [
    source 50
    target 371
  ]
  edge [
    source 50
    target 372
  ]
  edge [
    source 50
    target 373
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 68
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 374
  ]
  edge [
    source 53
    target 375
  ]
  edge [
    source 53
    target 376
  ]
  edge [
    source 53
    target 377
  ]
  edge [
    source 53
    target 378
  ]
  edge [
    source 53
    target 379
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 380
  ]
  edge [
    source 54
    target 381
  ]
  edge [
    source 54
    target 382
  ]
  edge [
    source 54
    target 124
  ]
  edge [
    source 54
    target 383
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 384
  ]
  edge [
    source 55
    target 385
  ]
  edge [
    source 55
    target 386
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 387
  ]
  edge [
    source 56
    target 388
  ]
  edge [
    source 56
    target 389
  ]
  edge [
    source 57
    target 390
  ]
  edge [
    source 57
    target 391
  ]
  edge [
    source 57
    target 392
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 57
    target 393
  ]
  edge [
    source 57
    target 394
  ]
  edge [
    source 57
    target 64
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 395
  ]
  edge [
    source 58
    target 396
  ]
  edge [
    source 58
    target 397
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 398
  ]
  edge [
    source 59
    target 399
  ]
  edge [
    source 59
    target 75
  ]
  edge [
    source 59
    target 400
  ]
  edge [
    source 59
    target 401
  ]
  edge [
    source 59
    target 308
  ]
  edge [
    source 59
    target 402
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 403
  ]
  edge [
    source 60
    target 404
  ]
  edge [
    source 60
    target 405
  ]
  edge [
    source 60
    target 309
  ]
  edge [
    source 60
    target 406
  ]
  edge [
    source 60
    target 407
  ]
  edge [
    source 60
    target 408
  ]
  edge [
    source 60
    target 409
  ]
  edge [
    source 60
    target 410
  ]
  edge [
    source 60
    target 411
  ]
  edge [
    source 60
    target 412
  ]
  edge [
    source 60
    target 251
  ]
  edge [
    source 60
    target 413
  ]
  edge [
    source 60
    target 83
  ]
  edge [
    source 60
    target 390
  ]
  edge [
    source 60
    target 387
  ]
  edge [
    source 60
    target 414
  ]
  edge [
    source 60
    target 415
  ]
  edge [
    source 60
    target 416
  ]
  edge [
    source 60
    target 417
  ]
  edge [
    source 60
    target 418
  ]
  edge [
    source 60
    target 419
  ]
  edge [
    source 60
    target 420
  ]
  edge [
    source 60
    target 421
  ]
  edge [
    source 60
    target 422
  ]
  edge [
    source 60
    target 423
  ]
  edge [
    source 60
    target 424
  ]
  edge [
    source 60
    target 425
  ]
  edge [
    source 60
    target 426
  ]
  edge [
    source 60
    target 220
  ]
  edge [
    source 60
    target 427
  ]
  edge [
    source 60
    target 428
  ]
  edge [
    source 60
    target 429
  ]
  edge [
    source 60
    target 430
  ]
  edge [
    source 60
    target 431
  ]
  edge [
    source 60
    target 432
  ]
  edge [
    source 60
    target 433
  ]
  edge [
    source 60
    target 434
  ]
  edge [
    source 61
    target 435
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 62
    target 436
  ]
  edge [
    source 62
    target 437
  ]
  edge [
    source 63
    target 64
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 438
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 439
  ]
  edge [
    source 65
    target 440
  ]
  edge [
    source 65
    target 441
  ]
  edge [
    source 65
    target 442
  ]
  edge [
    source 65
    target 443
  ]
  edge [
    source 65
    target 444
  ]
  edge [
    source 65
    target 445
  ]
  edge [
    source 65
    target 446
  ]
  edge [
    source 65
    target 447
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 67
    target 68
  ]
  edge [
    source 67
    target 448
  ]
  edge [
    source 67
    target 449
  ]
  edge [
    source 67
    target 450
  ]
  edge [
    source 67
    target 451
  ]
  edge [
    source 67
    target 452
  ]
  edge [
    source 67
    target 453
  ]
  edge [
    source 67
    target 454
  ]
  edge [
    source 67
    target 455
  ]
  edge [
    source 67
    target 456
  ]
  edge [
    source 67
    target 457
  ]
  edge [
    source 67
    target 458
  ]
  edge [
    source 67
    target 459
  ]
  edge [
    source 67
    target 460
  ]
  edge [
    source 67
    target 461
  ]
  edge [
    source 67
    target 462
  ]
  edge [
    source 68
    target 69
  ]
  edge [
    source 69
    target 70
  ]
  edge [
    source 69
    target 463
  ]
  edge [
    source 69
    target 304
  ]
  edge [
    source 69
    target 464
  ]
  edge [
    source 70
    target 71
  ]
  edge [
    source 70
    target 465
  ]
  edge [
    source 70
    target 466
  ]
  edge [
    source 70
    target 187
  ]
  edge [
    source 71
    target 72
  ]
  edge [
    source 71
    target 467
  ]
  edge [
    source 71
    target 162
  ]
  edge [
    source 71
    target 468
  ]
  edge [
    source 71
    target 469
  ]
  edge [
    source 72
    target 73
  ]
  edge [
    source 72
    target 470
  ]
  edge [
    source 72
    target 162
  ]
  edge [
    source 72
    target 471
  ]
  edge [
    source 72
    target 472
  ]
  edge [
    source 72
    target 311
  ]
  edge [
    source 72
    target 473
  ]
  edge [
    source 72
    target 469
  ]
  edge [
    source 72
    target 474
  ]
  edge [
    source 73
    target 74
  ]
  edge [
    source 74
    target 475
  ]
  edge [
    source 74
    target 476
  ]
  edge [
    source 74
    target 477
  ]
  edge [
    source 74
    target 478
  ]
  edge [
    source 74
    target 479
  ]
  edge [
    source 74
    target 480
  ]
  edge [
    source 74
    target 481
  ]
  edge [
    source 74
    target 482
  ]
  edge [
    source 74
    target 483
  ]
  edge [
    source 74
    target 484
  ]
  edge [
    source 75
    target 485
  ]
  edge [
    source 75
    target 448
  ]
  edge [
    source 75
    target 486
  ]
  edge [
    source 75
    target 487
  ]
  edge [
    source 75
    target 488
  ]
  edge [
    source 75
    target 489
  ]
  edge [
    source 75
    target 490
  ]
  edge [
    source 75
    target 401
  ]
  edge [
    source 75
    target 491
  ]
  edge [
    source 75
    target 492
  ]
]
