graph [
  maxDegree 38
  minDegree 1
  meanDegree 2.0756302521008405
  density 0.008757933553168102
  graphCliqueNumber 3
  node [
    id 0
    label "niezale&#380;nie"
    origin "text"
  ]
  node [
    id 1
    label "postaw"
    origin "text"
  ]
  node [
    id 2
    label "wi&#281;kszo&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "respondent"
    origin "text"
  ]
  node [
    id 4
    label "popiera&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wprowadzenie"
    origin "text"
  ]
  node [
    id 6
    label "zakaz"
    origin "text"
  ]
  node [
    id 7
    label "palenie"
    origin "text"
  ]
  node [
    id 8
    label "miejsce"
    origin "text"
  ]
  node [
    id 9
    label "publiczny"
    origin "text"
  ]
  node [
    id 10
    label "gdy"
    origin "text"
  ]
  node [
    id 11
    label "by&#263;"
    origin "text"
  ]
  node [
    id 12
    label "zdecydowanie"
    origin "text"
  ]
  node [
    id 13
    label "przeciwny"
    origin "text"
  ]
  node [
    id 14
    label "ogranicza&#263;"
    origin "text"
  ]
  node [
    id 15
    label "wolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "palacz"
    origin "text"
  ]
  node [
    id 17
    label "ciekawy"
    origin "text"
  ]
  node [
    id 18
    label "ostatni"
    origin "text"
  ]
  node [
    id 19
    label "dwa"
    origin "text"
  ]
  node [
    id 20
    label "lato"
    origin "text"
  ]
  node [
    id 21
    label "grupa"
    origin "text"
  ]
  node [
    id 22
    label "nieznacznie"
    origin "text"
  ]
  node [
    id 23
    label "si&#281;"
    origin "text"
  ]
  node [
    id 24
    label "powi&#281;kszy&#263;"
    origin "text"
  ]
  node [
    id 25
    label "uby&#263;"
    origin "text"
  ]
  node [
    id 26
    label "zwolennik"
    origin "text"
  ]
  node [
    id 27
    label "polak"
    origin "text"
  ]
  node [
    id 28
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 29
    label "papieros"
    origin "text"
  ]
  node [
    id 30
    label "niezale&#380;ny"
  ]
  node [
    id 31
    label "jednostka"
  ]
  node [
    id 32
    label "majority"
  ]
  node [
    id 33
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 34
    label "odpowied&#378;"
  ]
  node [
    id 35
    label "badany"
  ]
  node [
    id 36
    label "uzasadnia&#263;"
  ]
  node [
    id 37
    label "pomaga&#263;"
  ]
  node [
    id 38
    label "unbosom"
  ]
  node [
    id 39
    label "rynek"
  ]
  node [
    id 40
    label "issue"
  ]
  node [
    id 41
    label "evocation"
  ]
  node [
    id 42
    label "wst&#281;p"
  ]
  node [
    id 43
    label "nuklearyzacja"
  ]
  node [
    id 44
    label "umo&#380;liwienie"
  ]
  node [
    id 45
    label "zacz&#281;cie"
  ]
  node [
    id 46
    label "wpisanie"
  ]
  node [
    id 47
    label "zapoznanie"
  ]
  node [
    id 48
    label "zrobienie"
  ]
  node [
    id 49
    label "czynno&#347;&#263;"
  ]
  node [
    id 50
    label "entrance"
  ]
  node [
    id 51
    label "wej&#347;cie"
  ]
  node [
    id 52
    label "podstawy"
  ]
  node [
    id 53
    label "spowodowanie"
  ]
  node [
    id 54
    label "zak&#322;&#243;cenie"
  ]
  node [
    id 55
    label "w&#322;&#261;czenie"
  ]
  node [
    id 56
    label "doprowadzenie"
  ]
  node [
    id 57
    label "przewietrzenie"
  ]
  node [
    id 58
    label "deduction"
  ]
  node [
    id 59
    label "umieszczenie"
  ]
  node [
    id 60
    label "rozporz&#261;dzenie"
  ]
  node [
    id 61
    label "polecenie"
  ]
  node [
    id 62
    label "kadzenie"
  ]
  node [
    id 63
    label "robienie"
  ]
  node [
    id 64
    label "strzelanie"
  ]
  node [
    id 65
    label "wypalenie"
  ]
  node [
    id 66
    label "przygotowywanie"
  ]
  node [
    id 67
    label "wypalanie"
  ]
  node [
    id 68
    label "pykni&#281;cie"
  ]
  node [
    id 69
    label "popalenie"
  ]
  node [
    id 70
    label "emergency"
  ]
  node [
    id 71
    label "rozpalanie"
  ]
  node [
    id 72
    label "na&#322;&#243;g"
  ]
  node [
    id 73
    label "incineration"
  ]
  node [
    id 74
    label "dra&#380;nienie"
  ]
  node [
    id 75
    label "paliwo"
  ]
  node [
    id 76
    label "psucie"
  ]
  node [
    id 77
    label "jaranie"
  ]
  node [
    id 78
    label "zu&#380;ywanie"
  ]
  node [
    id 79
    label "podpalanie"
  ]
  node [
    id 80
    label "palenie_si&#281;"
  ]
  node [
    id 81
    label "ogie&#324;"
  ]
  node [
    id 82
    label "grzanie"
  ]
  node [
    id 83
    label "bolenie"
  ]
  node [
    id 84
    label "burn"
  ]
  node [
    id 85
    label "fajka"
  ]
  node [
    id 86
    label "dowcip"
  ]
  node [
    id 87
    label "dopalenie"
  ]
  node [
    id 88
    label "dokuczanie"
  ]
  node [
    id 89
    label "podtrzymywanie"
  ]
  node [
    id 90
    label "napalenie"
  ]
  node [
    id 91
    label "cygaro"
  ]
  node [
    id 92
    label "burning"
  ]
  node [
    id 93
    label "niszczenie"
  ]
  node [
    id 94
    label "powodowanie"
  ]
  node [
    id 95
    label "cia&#322;o"
  ]
  node [
    id 96
    label "plac"
  ]
  node [
    id 97
    label "cecha"
  ]
  node [
    id 98
    label "uwaga"
  ]
  node [
    id 99
    label "przestrze&#324;"
  ]
  node [
    id 100
    label "status"
  ]
  node [
    id 101
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 102
    label "chwila"
  ]
  node [
    id 103
    label "rz&#261;d"
  ]
  node [
    id 104
    label "praca"
  ]
  node [
    id 105
    label "location"
  ]
  node [
    id 106
    label "warunek_lokalowy"
  ]
  node [
    id 107
    label "jawny"
  ]
  node [
    id 108
    label "upublicznienie"
  ]
  node [
    id 109
    label "upublicznianie"
  ]
  node [
    id 110
    label "publicznie"
  ]
  node [
    id 111
    label "si&#281;ga&#263;"
  ]
  node [
    id 112
    label "trwa&#263;"
  ]
  node [
    id 113
    label "obecno&#347;&#263;"
  ]
  node [
    id 114
    label "stan"
  ]
  node [
    id 115
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 116
    label "stand"
  ]
  node [
    id 117
    label "mie&#263;_miejsce"
  ]
  node [
    id 118
    label "uczestniczy&#263;"
  ]
  node [
    id 119
    label "chodzi&#263;"
  ]
  node [
    id 120
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 121
    label "equal"
  ]
  node [
    id 122
    label "zdecydowany"
  ]
  node [
    id 123
    label "oddzia&#322;anie"
  ]
  node [
    id 124
    label "resoluteness"
  ]
  node [
    id 125
    label "decyzja"
  ]
  node [
    id 126
    label "pewnie"
  ]
  node [
    id 127
    label "rozstrzygni&#281;cie_si&#281;"
  ]
  node [
    id 128
    label "podj&#281;cie"
  ]
  node [
    id 129
    label "zauwa&#380;alnie"
  ]
  node [
    id 130
    label "judgment"
  ]
  node [
    id 131
    label "inny"
  ]
  node [
    id 132
    label "odmienny"
  ]
  node [
    id 133
    label "po_przeciwnej_stronie"
  ]
  node [
    id 134
    label "odwrotnie"
  ]
  node [
    id 135
    label "przeciwnie"
  ]
  node [
    id 136
    label "odwrotno&#347;&#263;"
  ]
  node [
    id 137
    label "niech&#281;tny"
  ]
  node [
    id 138
    label "wytycza&#263;"
  ]
  node [
    id 139
    label "bound"
  ]
  node [
    id 140
    label "wi&#281;zienie"
  ]
  node [
    id 141
    label "suppress"
  ]
  node [
    id 142
    label "environment"
  ]
  node [
    id 143
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 144
    label "zmniejsza&#263;"
  ]
  node [
    id 145
    label "stanowi&#263;"
  ]
  node [
    id 146
    label "absolutno&#347;&#263;"
  ]
  node [
    id 147
    label "uwi&#281;zi&#263;"
  ]
  node [
    id 148
    label "freedom"
  ]
  node [
    id 149
    label "niezale&#380;no&#347;&#263;"
  ]
  node [
    id 150
    label "uwi&#281;zienie"
  ]
  node [
    id 151
    label "nieokre&#347;lono&#347;&#263;"
  ]
  node [
    id 152
    label "robotnik"
  ]
  node [
    id 153
    label "pal&#261;cy"
  ]
  node [
    id 154
    label "na&#322;ogowiec"
  ]
  node [
    id 155
    label "konsument"
  ]
  node [
    id 156
    label "swoisty"
  ]
  node [
    id 157
    label "cz&#322;owiek"
  ]
  node [
    id 158
    label "interesowanie"
  ]
  node [
    id 159
    label "nietuzinkowy"
  ]
  node [
    id 160
    label "ciekawie"
  ]
  node [
    id 161
    label "indagator"
  ]
  node [
    id 162
    label "interesuj&#261;cy"
  ]
  node [
    id 163
    label "dziwny"
  ]
  node [
    id 164
    label "intryguj&#261;cy"
  ]
  node [
    id 165
    label "ch&#281;tny"
  ]
  node [
    id 166
    label "kolejny"
  ]
  node [
    id 167
    label "istota_&#380;ywa"
  ]
  node [
    id 168
    label "najgorszy"
  ]
  node [
    id 169
    label "aktualny"
  ]
  node [
    id 170
    label "ostatnio"
  ]
  node [
    id 171
    label "niedawno"
  ]
  node [
    id 172
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 173
    label "sko&#324;czony"
  ]
  node [
    id 174
    label "poprzedni"
  ]
  node [
    id 175
    label "pozosta&#322;y"
  ]
  node [
    id 176
    label "w&#261;tpliwy"
  ]
  node [
    id 177
    label "pora_roku"
  ]
  node [
    id 178
    label "odm&#322;adza&#263;"
  ]
  node [
    id 179
    label "asymilowa&#263;"
  ]
  node [
    id 180
    label "cz&#261;steczka"
  ]
  node [
    id 181
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 182
    label "egzemplarz"
  ]
  node [
    id 183
    label "formacja_geologiczna"
  ]
  node [
    id 184
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 185
    label "harcerze_starsi"
  ]
  node [
    id 186
    label "liga"
  ]
  node [
    id 187
    label "Terranie"
  ]
  node [
    id 188
    label "&#346;wietliki"
  ]
  node [
    id 189
    label "pakiet_klimatyczny"
  ]
  node [
    id 190
    label "oddzia&#322;"
  ]
  node [
    id 191
    label "stage_set"
  ]
  node [
    id 192
    label "Entuzjastki"
  ]
  node [
    id 193
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 194
    label "odm&#322;odzenie"
  ]
  node [
    id 195
    label "type"
  ]
  node [
    id 196
    label "category"
  ]
  node [
    id 197
    label "asymilowanie"
  ]
  node [
    id 198
    label "specgrupa"
  ]
  node [
    id 199
    label "odm&#322;adzanie"
  ]
  node [
    id 200
    label "gromada"
  ]
  node [
    id 201
    label "Eurogrupa"
  ]
  node [
    id 202
    label "jednostka_systematyczna"
  ]
  node [
    id 203
    label "kompozycja"
  ]
  node [
    id 204
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 205
    label "zbi&#243;r"
  ]
  node [
    id 206
    label "nieistotnie"
  ]
  node [
    id 207
    label "nieznaczny"
  ]
  node [
    id 208
    label "zmieni&#263;"
  ]
  node [
    id 209
    label "ascend"
  ]
  node [
    id 210
    label "zabrakn&#261;&#263;"
  ]
  node [
    id 211
    label "straci&#263;"
  ]
  node [
    id 212
    label "become"
  ]
  node [
    id 213
    label "polski"
  ]
  node [
    id 214
    label "system"
  ]
  node [
    id 215
    label "s&#261;d"
  ]
  node [
    id 216
    label "wytw&#243;r"
  ]
  node [
    id 217
    label "istota"
  ]
  node [
    id 218
    label "thinking"
  ]
  node [
    id 219
    label "idea"
  ]
  node [
    id 220
    label "political_orientation"
  ]
  node [
    id 221
    label "pomys&#322;"
  ]
  node [
    id 222
    label "szko&#322;a"
  ]
  node [
    id 223
    label "umys&#322;"
  ]
  node [
    id 224
    label "fantomatyka"
  ]
  node [
    id 225
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 226
    label "p&#322;&#243;d"
  ]
  node [
    id 227
    label "filtr"
  ]
  node [
    id 228
    label "u&#380;ywka"
  ]
  node [
    id 229
    label "dulec"
  ]
  node [
    id 230
    label "bibu&#322;ka"
  ]
  node [
    id 231
    label "szlug"
  ]
  node [
    id 232
    label "zapalenie"
  ]
  node [
    id 233
    label "wyr&#243;b_tytoniowy"
  ]
  node [
    id 234
    label "trafika"
  ]
  node [
    id 235
    label "smoke"
  ]
  node [
    id 236
    label "pali&#263;"
  ]
  node [
    id 237
    label "odpali&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 33
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 107
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 48
  ]
  edge [
    source 12
    target 122
  ]
  edge [
    source 12
    target 123
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 124
  ]
  edge [
    source 12
    target 125
  ]
  edge [
    source 12
    target 126
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 138
  ]
  edge [
    source 14
    target 139
  ]
  edge [
    source 14
    target 140
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 97
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 17
    target 165
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 178
  ]
  edge [
    source 21
    target 179
  ]
  edge [
    source 21
    target 180
  ]
  edge [
    source 21
    target 181
  ]
  edge [
    source 21
    target 182
  ]
  edge [
    source 21
    target 183
  ]
  edge [
    source 21
    target 184
  ]
  edge [
    source 21
    target 185
  ]
  edge [
    source 21
    target 186
  ]
  edge [
    source 21
    target 187
  ]
  edge [
    source 21
    target 188
  ]
  edge [
    source 21
    target 189
  ]
  edge [
    source 21
    target 190
  ]
  edge [
    source 21
    target 191
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 21
    target 201
  ]
  edge [
    source 21
    target 202
  ]
  edge [
    source 21
    target 203
  ]
  edge [
    source 21
    target 204
  ]
  edge [
    source 21
    target 205
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 206
  ]
  edge [
    source 22
    target 207
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 208
  ]
  edge [
    source 24
    target 209
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 210
  ]
  edge [
    source 25
    target 211
  ]
  edge [
    source 25
    target 212
  ]
  edge [
    source 26
    target 157
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 213
  ]
  edge [
    source 28
    target 214
  ]
  edge [
    source 28
    target 215
  ]
  edge [
    source 28
    target 216
  ]
  edge [
    source 28
    target 217
  ]
  edge [
    source 28
    target 218
  ]
  edge [
    source 28
    target 219
  ]
  edge [
    source 28
    target 220
  ]
  edge [
    source 28
    target 221
  ]
  edge [
    source 28
    target 222
  ]
  edge [
    source 28
    target 223
  ]
  edge [
    source 28
    target 224
  ]
  edge [
    source 28
    target 225
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 29
    target 227
  ]
  edge [
    source 29
    target 228
  ]
  edge [
    source 29
    target 229
  ]
  edge [
    source 29
    target 230
  ]
  edge [
    source 29
    target 231
  ]
  edge [
    source 29
    target 232
  ]
  edge [
    source 29
    target 233
  ]
  edge [
    source 29
    target 234
  ]
  edge [
    source 29
    target 235
  ]
  edge [
    source 29
    target 236
  ]
  edge [
    source 29
    target 237
  ]
]
