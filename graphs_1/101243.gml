graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.07971014492753623
  graphCliqueNumber 2
  node [
    id 0
    label "pierog"
    origin "text"
  ]
  node [
    id 1
    label "ruski"
    origin "text"
  ]
  node [
    id 2
    label "pierogi"
  ]
  node [
    id 3
    label "nadzienie"
  ]
  node [
    id 4
    label "po_rosyjsku"
  ]
  node [
    id 5
    label "toporny"
  ]
  node [
    id 6
    label "serowy"
  ]
  node [
    id 7
    label "zbytkowny"
  ]
  node [
    id 8
    label "j&#281;zyk"
  ]
  node [
    id 9
    label "ostentacyjny"
  ]
  node [
    id 10
    label "byle_jaki"
  ]
  node [
    id 11
    label "wielkoruski"
  ]
  node [
    id 12
    label "tandetny"
  ]
  node [
    id 13
    label "kacapski"
  ]
  node [
    id 14
    label "Russian"
  ]
  node [
    id 15
    label "wulgarny"
  ]
  node [
    id 16
    label "j&#281;zyki_wschodnios&#322;owia&#324;skie"
  ]
  node [
    id 17
    label "po_rusku"
  ]
  node [
    id 18
    label "ziemniaczany"
  ]
  node [
    id 19
    label "j&#281;zyk_s&#322;owia&#324;ski"
  ]
  node [
    id 20
    label "rusek"
  ]
  node [
    id 21
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 22
    label "rusy"
  ]
  node [
    id 23
    label "czerwony"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 22
    target 23
  ]
]
