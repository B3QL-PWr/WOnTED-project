graph [
  maxDegree 46
  minDegree 1
  meanDegree 2
  density 0.020618556701030927
  graphCliqueNumber 2
  node [
    id 0
    label "j&#281;zyk"
    origin "text"
  ]
  node [
    id 1
    label "opis"
    origin "text"
  ]
  node [
    id 2
    label "wymagania"
    origin "text"
  ]
  node [
    id 3
    label "funkcjonalny"
    origin "text"
  ]
  node [
    id 4
    label "system"
    origin "text"
  ]
  node [
    id 5
    label "informatyczny"
    origin "text"
  ]
  node [
    id 6
    label "pisa&#263;"
  ]
  node [
    id 7
    label "przet&#322;umaczy&#263;"
  ]
  node [
    id 8
    label "czynno&#347;&#263;_psychiczna"
  ]
  node [
    id 9
    label "ssanie"
  ]
  node [
    id 10
    label "po_koroniarsku"
  ]
  node [
    id 11
    label "przedmiot"
  ]
  node [
    id 12
    label "but"
  ]
  node [
    id 13
    label "m&#243;wienie"
  ]
  node [
    id 14
    label "rozumie&#263;"
  ]
  node [
    id 15
    label "formacja_geologiczna"
  ]
  node [
    id 16
    label "rozumienie"
  ]
  node [
    id 17
    label "m&#243;wi&#263;"
  ]
  node [
    id 18
    label "gramatyka"
  ]
  node [
    id 19
    label "pype&#263;"
  ]
  node [
    id 20
    label "makroglosja"
  ]
  node [
    id 21
    label "kawa&#322;ek"
  ]
  node [
    id 22
    label "artykulator"
  ]
  node [
    id 23
    label "kultura_duchowa"
  ]
  node [
    id 24
    label "j&#281;zyk_geograficzny"
  ]
  node [
    id 25
    label "jama_ustna"
  ]
  node [
    id 26
    label "spos&#243;b"
  ]
  node [
    id 27
    label "rodzina_j&#281;zykowa"
  ]
  node [
    id 28
    label "przet&#322;umaczenie"
  ]
  node [
    id 29
    label "t&#322;umaczenie"
  ]
  node [
    id 30
    label "language"
  ]
  node [
    id 31
    label "jeniec"
  ]
  node [
    id 32
    label "organ"
  ]
  node [
    id 33
    label "cz&#281;&#347;&#263;_mowy"
  ]
  node [
    id 34
    label "pismo"
  ]
  node [
    id 35
    label "formalizowanie"
  ]
  node [
    id 36
    label "fonetyka"
  ]
  node [
    id 37
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 38
    label "wokalizm"
  ]
  node [
    id 39
    label "liza&#263;"
  ]
  node [
    id 40
    label "s&#322;ownictwo"
  ]
  node [
    id 41
    label "napisa&#263;"
  ]
  node [
    id 42
    label "formalizowa&#263;"
  ]
  node [
    id 43
    label "natural_language"
  ]
  node [
    id 44
    label "&#347;rodek_ekspresji_j&#281;zykowej"
  ]
  node [
    id 45
    label "stylik"
  ]
  node [
    id 46
    label "konsonantyzm"
  ]
  node [
    id 47
    label "urz&#261;dzenie"
  ]
  node [
    id 48
    label "ssa&#263;"
  ]
  node [
    id 49
    label "kod"
  ]
  node [
    id 50
    label "lizanie"
  ]
  node [
    id 51
    label "exposition"
  ]
  node [
    id 52
    label "czynno&#347;&#263;"
  ]
  node [
    id 53
    label "wypowied&#378;"
  ]
  node [
    id 54
    label "obja&#347;nienie"
  ]
  node [
    id 55
    label "czynny"
  ]
  node [
    id 56
    label "funkcjonalnie"
  ]
  node [
    id 57
    label "praktyczny"
  ]
  node [
    id 58
    label "model"
  ]
  node [
    id 59
    label "sk&#322;ad"
  ]
  node [
    id 60
    label "zachowanie"
  ]
  node [
    id 61
    label "podstawa"
  ]
  node [
    id 62
    label "porz&#261;dek"
  ]
  node [
    id 63
    label "Android"
  ]
  node [
    id 64
    label "przyn&#281;ta"
  ]
  node [
    id 65
    label "jednostka_geologiczna"
  ]
  node [
    id 66
    label "metoda"
  ]
  node [
    id 67
    label "podsystem"
  ]
  node [
    id 68
    label "p&#322;&#243;d"
  ]
  node [
    id 69
    label "oprzyrz&#261;dowanie"
  ]
  node [
    id 70
    label "s&#261;d"
  ]
  node [
    id 71
    label "rozprz&#281;&#380;enie"
  ]
  node [
    id 72
    label "wielozadaniowo&#347;&#263;"
  ]
  node [
    id 73
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 74
    label "j&#261;dro"
  ]
  node [
    id 75
    label "eratem"
  ]
  node [
    id 76
    label "ryba"
  ]
  node [
    id 77
    label "pulpit"
  ]
  node [
    id 78
    label "struktura"
  ]
  node [
    id 79
    label "oddzia&#322;"
  ]
  node [
    id 80
    label "usenet"
  ]
  node [
    id 81
    label "o&#347;"
  ]
  node [
    id 82
    label "oprogramowanie"
  ]
  node [
    id 83
    label "regu&#322;a_Sarrusa"
  ]
  node [
    id 84
    label "poj&#281;cie"
  ]
  node [
    id 85
    label "w&#281;dkarstwo"
  ]
  node [
    id 86
    label "odpowied&#378;_impulsowa"
  ]
  node [
    id 87
    label "Leopard"
  ]
  node [
    id 88
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 89
    label "systemik"
  ]
  node [
    id 90
    label "rozprz&#261;c"
  ]
  node [
    id 91
    label "cybernetyk"
  ]
  node [
    id 92
    label "konstelacja"
  ]
  node [
    id 93
    label "doktryna"
  ]
  node [
    id 94
    label "net"
  ]
  node [
    id 95
    label "zbi&#243;r"
  ]
  node [
    id 96
    label "method"
  ]
  node [
    id 97
    label "systemat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
]
