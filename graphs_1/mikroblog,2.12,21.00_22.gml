graph [
  maxDegree 33
  minDegree 1
  meanDegree 2.0246913580246915
  density 0.025308641975308643
  graphCliqueNumber 3
  node [
    id 0
    label "kto"
    origin "text"
  ]
  node [
    id 1
    label "pami&#281;ta&#263;"
    origin "text"
  ]
  node [
    id 2
    label "dawa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "plus"
    origin "text"
  ]
  node [
    id 4
    label "inny"
    origin "text"
  ]
  node [
    id 5
    label "przewija&#263;"
    origin "text"
  ]
  node [
    id 6
    label "daleko"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;ga&#263;"
  ]
  node [
    id 8
    label "zna&#263;"
  ]
  node [
    id 9
    label "troska&#263;_si&#281;"
  ]
  node [
    id 10
    label "zachowywa&#263;"
  ]
  node [
    id 11
    label "chowa&#263;"
  ]
  node [
    id 12
    label "think"
  ]
  node [
    id 13
    label "pilnowa&#263;"
  ]
  node [
    id 14
    label "robi&#263;"
  ]
  node [
    id 15
    label "recall"
  ]
  node [
    id 16
    label "echo"
  ]
  node [
    id 17
    label "take_care"
  ]
  node [
    id 18
    label "render"
  ]
  node [
    id 19
    label "hold"
  ]
  node [
    id 20
    label "surrender"
  ]
  node [
    id 21
    label "traktowa&#263;"
  ]
  node [
    id 22
    label "dostarcza&#263;"
  ]
  node [
    id 23
    label "tender"
  ]
  node [
    id 24
    label "train"
  ]
  node [
    id 25
    label "give"
  ]
  node [
    id 26
    label "umieszcza&#263;"
  ]
  node [
    id 27
    label "nalewa&#263;"
  ]
  node [
    id 28
    label "przeznacza&#263;"
  ]
  node [
    id 29
    label "p&#322;aci&#263;"
  ]
  node [
    id 30
    label "umo&#380;liwia&#263;"
  ]
  node [
    id 31
    label "wyst&#281;powa&#263;"
  ]
  node [
    id 32
    label "powierza&#263;"
  ]
  node [
    id 33
    label "hold_out"
  ]
  node [
    id 34
    label "udost&#281;pnia&#263;"
  ]
  node [
    id 35
    label "wyrzeka&#263;_si&#281;"
  ]
  node [
    id 36
    label "mie&#263;_miejsce"
  ]
  node [
    id 37
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 38
    label "t&#322;uc"
  ]
  node [
    id 39
    label "wpiernicza&#263;"
  ]
  node [
    id 40
    label "przekazywa&#263;"
  ]
  node [
    id 41
    label "puszcza&#263;_si&#281;"
  ]
  node [
    id 42
    label "zezwala&#263;"
  ]
  node [
    id 43
    label "rap"
  ]
  node [
    id 44
    label "obiecywa&#263;"
  ]
  node [
    id 45
    label "&#322;adowa&#263;"
  ]
  node [
    id 46
    label "odst&#281;powa&#263;"
  ]
  node [
    id 47
    label "exsert"
  ]
  node [
    id 48
    label "warto&#347;&#263;"
  ]
  node [
    id 49
    label "wabik"
  ]
  node [
    id 50
    label "rewaluowa&#263;"
  ]
  node [
    id 51
    label "korzy&#347;&#263;"
  ]
  node [
    id 52
    label "dodawanie"
  ]
  node [
    id 53
    label "rewaluowanie"
  ]
  node [
    id 54
    label "stopie&#324;"
  ]
  node [
    id 55
    label "ocena"
  ]
  node [
    id 56
    label "zrewaluowa&#263;"
  ]
  node [
    id 57
    label "liczba"
  ]
  node [
    id 58
    label "znak_matematyczny"
  ]
  node [
    id 59
    label "strona"
  ]
  node [
    id 60
    label "zrewaluowanie"
  ]
  node [
    id 61
    label "kolejny"
  ]
  node [
    id 62
    label "inaczej"
  ]
  node [
    id 63
    label "r&#243;&#380;ny"
  ]
  node [
    id 64
    label "inszy"
  ]
  node [
    id 65
    label "osobno"
  ]
  node [
    id 66
    label "przebiera&#263;"
  ]
  node [
    id 67
    label "owija&#263;"
  ]
  node [
    id 68
    label "swathe"
  ]
  node [
    id 69
    label "kaseta"
  ]
  node [
    id 70
    label "powodowa&#263;"
  ]
  node [
    id 71
    label "pielucha"
  ]
  node [
    id 72
    label "dawno"
  ]
  node [
    id 73
    label "nisko"
  ]
  node [
    id 74
    label "nieobecnie"
  ]
  node [
    id 75
    label "daleki"
  ]
  node [
    id 76
    label "het"
  ]
  node [
    id 77
    label "wysoko"
  ]
  node [
    id 78
    label "du&#380;o"
  ]
  node [
    id 79
    label "znacznie"
  ]
  node [
    id 80
    label "g&#322;&#281;boko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
]
