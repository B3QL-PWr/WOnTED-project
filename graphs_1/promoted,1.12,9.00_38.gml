graph [
  maxDegree 24
  minDegree 1
  meanDegree 2
  density 0.02197802197802198
  graphCliqueNumber 2
  node [
    id 0
    label "trzeba"
    origin "text"
  ]
  node [
    id 1
    label "zdawa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "siebie"
    origin "text"
  ]
  node [
    id 3
    label "sprawa"
    origin "text"
  ]
  node [
    id 4
    label "skala"
    origin "text"
  ]
  node [
    id 5
    label "europa"
    origin "text"
  ]
  node [
    id 6
    label "kredyt"
    origin "text"
  ]
  node [
    id 7
    label "mieszkaniowy"
    origin "text"
  ]
  node [
    id 8
    label "polska"
    origin "text"
  ]
  node [
    id 9
    label "nadal"
    origin "text"
  ]
  node [
    id 10
    label "cechowa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "do&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "wysoki"
    origin "text"
  ]
  node [
    id 14
    label "oprocentowanie"
    origin "text"
  ]
  node [
    id 15
    label "trza"
  ]
  node [
    id 16
    label "necessity"
  ]
  node [
    id 17
    label "zalicza&#263;"
  ]
  node [
    id 18
    label "przyst&#281;powa&#263;"
  ]
  node [
    id 19
    label "opowiada&#263;"
  ]
  node [
    id 20
    label "render"
  ]
  node [
    id 21
    label "impart"
  ]
  node [
    id 22
    label "oddawa&#263;"
  ]
  node [
    id 23
    label "zostawia&#263;"
  ]
  node [
    id 24
    label "sk&#322;ada&#263;"
  ]
  node [
    id 25
    label "convey"
  ]
  node [
    id 26
    label "powierza&#263;"
  ]
  node [
    id 27
    label "bequeath"
  ]
  node [
    id 28
    label "temat"
  ]
  node [
    id 29
    label "kognicja"
  ]
  node [
    id 30
    label "idea"
  ]
  node [
    id 31
    label "szczeg&#243;&#322;"
  ]
  node [
    id 32
    label "rzecz"
  ]
  node [
    id 33
    label "wydarzenie"
  ]
  node [
    id 34
    label "przes&#322;anka"
  ]
  node [
    id 35
    label "rozprawa"
  ]
  node [
    id 36
    label "object"
  ]
  node [
    id 37
    label "proposition"
  ]
  node [
    id 38
    label "przedzia&#322;"
  ]
  node [
    id 39
    label "przymiar"
  ]
  node [
    id 40
    label "podzia&#322;ka"
  ]
  node [
    id 41
    label "proporcja"
  ]
  node [
    id 42
    label "tetrachord"
  ]
  node [
    id 43
    label "scale"
  ]
  node [
    id 44
    label "dominanta"
  ]
  node [
    id 45
    label "rejestr"
  ]
  node [
    id 46
    label "sfera"
  ]
  node [
    id 47
    label "struktura"
  ]
  node [
    id 48
    label "kreska"
  ]
  node [
    id 49
    label "zero"
  ]
  node [
    id 50
    label "interwa&#322;"
  ]
  node [
    id 51
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 52
    label "subdominanta"
  ]
  node [
    id 53
    label "dziedzina"
  ]
  node [
    id 54
    label "masztab"
  ]
  node [
    id 55
    label "part"
  ]
  node [
    id 56
    label "podzakres"
  ]
  node [
    id 57
    label "zbi&#243;r"
  ]
  node [
    id 58
    label "wielko&#347;&#263;"
  ]
  node [
    id 59
    label "jednostka"
  ]
  node [
    id 60
    label "aktywa"
  ]
  node [
    id 61
    label "borg"
  ]
  node [
    id 62
    label "odsetki"
  ]
  node [
    id 63
    label "konsolidacja"
  ]
  node [
    id 64
    label "arrozacja"
  ]
  node [
    id 65
    label "d&#322;ug"
  ]
  node [
    id 66
    label "sp&#322;ata"
  ]
  node [
    id 67
    label "rata"
  ]
  node [
    id 68
    label "zapa&#347;&#263;_kredytowa"
  ]
  node [
    id 69
    label "konto"
  ]
  node [
    id 70
    label "pasywa"
  ]
  node [
    id 71
    label "zobowi&#261;zanie"
  ]
  node [
    id 72
    label "linia_kredytowa"
  ]
  node [
    id 73
    label "mark"
  ]
  node [
    id 74
    label "oznacza&#263;"
  ]
  node [
    id 75
    label "warto&#347;ciowy"
  ]
  node [
    id 76
    label "du&#380;y"
  ]
  node [
    id 77
    label "wysoce"
  ]
  node [
    id 78
    label "daleki"
  ]
  node [
    id 79
    label "znaczny"
  ]
  node [
    id 80
    label "wysoko"
  ]
  node [
    id 81
    label "szczytnie"
  ]
  node [
    id 82
    label "wznios&#322;y"
  ]
  node [
    id 83
    label "wyrafinowany"
  ]
  node [
    id 84
    label "z_wysoka"
  ]
  node [
    id 85
    label "chwalebny"
  ]
  node [
    id 86
    label "uprzywilejowany"
  ]
  node [
    id 87
    label "niepo&#347;ledni"
  ]
  node [
    id 88
    label "doch&#243;d"
  ]
  node [
    id 89
    label "pastime"
  ]
  node [
    id 90
    label "baza_odsetkowa"
  ]
  node [
    id 91
    label "powi&#281;kszenie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 14
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 75
  ]
  edge [
    source 13
    target 76
  ]
  edge [
    source 13
    target 77
  ]
  edge [
    source 13
    target 78
  ]
  edge [
    source 13
    target 79
  ]
  edge [
    source 13
    target 80
  ]
  edge [
    source 13
    target 81
  ]
  edge [
    source 13
    target 82
  ]
  edge [
    source 13
    target 83
  ]
  edge [
    source 13
    target 84
  ]
  edge [
    source 13
    target 85
  ]
  edge [
    source 13
    target 86
  ]
  edge [
    source 13
    target 87
  ]
  edge [
    source 14
    target 88
  ]
  edge [
    source 14
    target 89
  ]
  edge [
    source 14
    target 90
  ]
  edge [
    source 14
    target 91
  ]
]
