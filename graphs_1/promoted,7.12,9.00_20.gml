graph [
  maxDegree 39
  minDegree 1
  meanDegree 1.9705882352941178
  density 0.029411764705882353
  graphCliqueNumber 2
  node [
    id 0
    label "zrobi&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "swoje"
    origin "text"
  ]
  node [
    id 2
    label "czas"
    origin "text"
  ]
  node [
    id 3
    label "taki"
    origin "text"
  ]
  node [
    id 4
    label "tiny"
    origin "text"
  ]
  node [
    id 5
    label "portal"
    origin "text"
  ]
  node [
    id 6
    label "pikio"
    origin "text"
  ]
  node [
    id 7
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 8
    label "https"
    origin "text"
  ]
  node [
    id 9
    label "czasokres"
  ]
  node [
    id 10
    label "trawienie"
  ]
  node [
    id 11
    label "kategoria_gramatyczna"
  ]
  node [
    id 12
    label "period"
  ]
  node [
    id 13
    label "odczyt"
  ]
  node [
    id 14
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 15
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 16
    label "chwila"
  ]
  node [
    id 17
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 18
    label "poprzedzenie"
  ]
  node [
    id 19
    label "koniugacja"
  ]
  node [
    id 20
    label "dzieje"
  ]
  node [
    id 21
    label "poprzedzi&#263;"
  ]
  node [
    id 22
    label "przep&#322;ywanie"
  ]
  node [
    id 23
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 24
    label "odwlekanie_si&#281;"
  ]
  node [
    id 25
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 26
    label "Zeitgeist"
  ]
  node [
    id 27
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 28
    label "okres_czasu"
  ]
  node [
    id 29
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 30
    label "pochodzi&#263;"
  ]
  node [
    id 31
    label "schy&#322;ek"
  ]
  node [
    id 32
    label "czwarty_wymiar"
  ]
  node [
    id 33
    label "chronometria"
  ]
  node [
    id 34
    label "poprzedzanie"
  ]
  node [
    id 35
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 36
    label "pogoda"
  ]
  node [
    id 37
    label "zegar"
  ]
  node [
    id 38
    label "trawi&#263;"
  ]
  node [
    id 39
    label "pochodzenie"
  ]
  node [
    id 40
    label "poprzedza&#263;"
  ]
  node [
    id 41
    label "time_period"
  ]
  node [
    id 42
    label "rachuba_czasu"
  ]
  node [
    id 43
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 44
    label "czasoprzestrze&#324;"
  ]
  node [
    id 45
    label "laba"
  ]
  node [
    id 46
    label "okre&#347;lony"
  ]
  node [
    id 47
    label "jaki&#347;"
  ]
  node [
    id 48
    label "obramienie"
  ]
  node [
    id 49
    label "forum"
  ]
  node [
    id 50
    label "serwis_internetowy"
  ]
  node [
    id 51
    label "wej&#347;cie"
  ]
  node [
    id 52
    label "Onet"
  ]
  node [
    id 53
    label "archiwolta"
  ]
  node [
    id 54
    label "zorganizowa&#263;"
  ]
  node [
    id 55
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 56
    label "przerobi&#263;"
  ]
  node [
    id 57
    label "wystylizowa&#263;"
  ]
  node [
    id 58
    label "cause"
  ]
  node [
    id 59
    label "wydali&#263;"
  ]
  node [
    id 60
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 61
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 62
    label "post&#261;pi&#263;"
  ]
  node [
    id 63
    label "appoint"
  ]
  node [
    id 64
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 65
    label "nabra&#263;"
  ]
  node [
    id 66
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 67
    label "make"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
]
