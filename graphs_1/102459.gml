graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.0816326530612246
  density 0.010675039246467817
  graphCliqueNumber 3
  node [
    id 0
    label "maj"
    origin "text"
  ]
  node [
    id 1
    label "godz"
    origin "text"
  ]
  node [
    id 2
    label "okazja"
    origin "text"
  ]
  node [
    id 3
    label "rocznica"
    origin "text"
  ]
  node [
    id 4
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 5
    label "urodziny"
    origin "text"
  ]
  node [
    id 6
    label "jana"
    origin "text"
  ]
  node [
    id 7
    label "pawe&#322;"
    origin "text"
  ]
  node [
    id 8
    label "czytelnia"
    origin "text"
  ]
  node [
    id 9
    label "miejski"
    origin "text"
  ]
  node [
    id 10
    label "biblioteka"
    origin "text"
  ]
  node [
    id 11
    label "publiczny"
    origin "text"
  ]
  node [
    id 12
    label "ozorkowie"
    origin "text"
  ]
  node [
    id 13
    label "przy"
    origin "text"
  ]
  node [
    id 14
    label "ula"
    origin "text"
  ]
  node [
    id 15
    label "listopadowy"
    origin "text"
  ]
  node [
    id 16
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 17
    label "si&#281;"
    origin "text"
  ]
  node [
    id 18
    label "koncert"
    origin "text"
  ]
  node [
    id 19
    label "pi&#261;tek"
    origin "text"
  ]
  node [
    id 20
    label "droga"
    origin "text"
  ]
  node [
    id 21
    label "&#347;wi&#281;to&#347;&#263;"
    origin "text"
  ]
  node [
    id 22
    label "wykonanie"
    origin "text"
  ]
  node [
    id 23
    label "zesp&#243;&#322;"
    origin "text"
  ]
  node [
    id 24
    label "marcato"
    origin "text"
  ]
  node [
    id 25
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 26
    label "podstawowy"
    origin "text"
  ]
  node [
    id 27
    label "zaprasza&#263;"
    origin "text"
  ]
  node [
    id 28
    label "&#346;wi&#281;to_Pracy"
  ]
  node [
    id 29
    label "miesi&#261;c"
  ]
  node [
    id 30
    label "atrakcyjny"
  ]
  node [
    id 31
    label "oferta"
  ]
  node [
    id 32
    label "adeptness"
  ]
  node [
    id 33
    label "okazka"
  ]
  node [
    id 34
    label "wydarzenie"
  ]
  node [
    id 35
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 36
    label "podw&#243;zka"
  ]
  node [
    id 37
    label "autostop"
  ]
  node [
    id 38
    label "sytuacja"
  ]
  node [
    id 39
    label "termin"
  ]
  node [
    id 40
    label "obchody"
  ]
  node [
    id 41
    label "defenestracja"
  ]
  node [
    id 42
    label "kres"
  ]
  node [
    id 43
    label "agonia"
  ]
  node [
    id 44
    label "&#380;ycie"
  ]
  node [
    id 45
    label "szeol"
  ]
  node [
    id 46
    label "mogi&#322;a"
  ]
  node [
    id 47
    label "pogrzeb"
  ]
  node [
    id 48
    label "istota_nadprzyrodzona"
  ]
  node [
    id 49
    label "&#380;a&#322;oba"
  ]
  node [
    id 50
    label "pogrzebanie"
  ]
  node [
    id 51
    label "upadek"
  ]
  node [
    id 52
    label "zabicie"
  ]
  node [
    id 53
    label "kres_&#380;ycia"
  ]
  node [
    id 54
    label "impreza"
  ]
  node [
    id 55
    label "jubileusz"
  ]
  node [
    id 56
    label "&#347;wi&#281;to"
  ]
  node [
    id 57
    label "pocz&#261;tek"
  ]
  node [
    id 58
    label "pomieszczenie"
  ]
  node [
    id 59
    label "informatorium"
  ]
  node [
    id 60
    label "miejsko"
  ]
  node [
    id 61
    label "miastowy"
  ]
  node [
    id 62
    label "typowy"
  ]
  node [
    id 63
    label "pok&#243;j"
  ]
  node [
    id 64
    label "rewers"
  ]
  node [
    id 65
    label "kolekcja"
  ]
  node [
    id 66
    label "czytelnik"
  ]
  node [
    id 67
    label "budynek"
  ]
  node [
    id 68
    label "zbi&#243;r"
  ]
  node [
    id 69
    label "programowanie"
  ]
  node [
    id 70
    label "ksi&#281;gozbi&#243;r_podr&#281;czny"
  ]
  node [
    id 71
    label "library"
  ]
  node [
    id 72
    label "instytucja"
  ]
  node [
    id 73
    label "jawny"
  ]
  node [
    id 74
    label "upublicznienie"
  ]
  node [
    id 75
    label "upublicznianie"
  ]
  node [
    id 76
    label "publicznie"
  ]
  node [
    id 77
    label "reserve"
  ]
  node [
    id 78
    label "przej&#347;&#263;"
  ]
  node [
    id 79
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 80
    label "p&#322;acz"
  ]
  node [
    id 81
    label "wyst&#281;p"
  ]
  node [
    id 82
    label "performance"
  ]
  node [
    id 83
    label "bogactwo"
  ]
  node [
    id 84
    label "mn&#243;stwo"
  ]
  node [
    id 85
    label "utw&#243;r"
  ]
  node [
    id 86
    label "show"
  ]
  node [
    id 87
    label "pokaz"
  ]
  node [
    id 88
    label "zjawisko"
  ]
  node [
    id 89
    label "szale&#324;stwo"
  ]
  node [
    id 90
    label "dzie&#324;_powszedni"
  ]
  node [
    id 91
    label "Wielki_Pi&#261;tek"
  ]
  node [
    id 92
    label "tydzie&#324;"
  ]
  node [
    id 93
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 94
    label "journey"
  ]
  node [
    id 95
    label "podbieg"
  ]
  node [
    id 96
    label "bezsilnikowy"
  ]
  node [
    id 97
    label "przyjaci&#243;&#322;ka"
  ]
  node [
    id 98
    label "wylot"
  ]
  node [
    id 99
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 100
    label "drogowskaz"
  ]
  node [
    id 101
    label "nawierzchnia"
  ]
  node [
    id 102
    label "turystyka"
  ]
  node [
    id 103
    label "budowla"
  ]
  node [
    id 104
    label "spos&#243;b"
  ]
  node [
    id 105
    label "passage"
  ]
  node [
    id 106
    label "marszrutyzacja"
  ]
  node [
    id 107
    label "zbior&#243;wka"
  ]
  node [
    id 108
    label "rajza"
  ]
  node [
    id 109
    label "ekskursja"
  ]
  node [
    id 110
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 111
    label "ruch"
  ]
  node [
    id 112
    label "trasa"
  ]
  node [
    id 113
    label "wyb&#243;j"
  ]
  node [
    id 114
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 115
    label "ekwipunek"
  ]
  node [
    id 116
    label "korona_drogi"
  ]
  node [
    id 117
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 118
    label "pobocze"
  ]
  node [
    id 119
    label "niezwyk&#322;o&#347;&#263;"
  ]
  node [
    id 120
    label "co&#347;"
  ]
  node [
    id 121
    label "holiness"
  ]
  node [
    id 122
    label "Had&#380;ar"
  ]
  node [
    id 123
    label "u&#347;wi&#281;canie"
  ]
  node [
    id 124
    label "u&#347;wi&#281;cenie"
  ]
  node [
    id 125
    label "zrobienie"
  ]
  node [
    id 126
    label "czynno&#347;&#263;"
  ]
  node [
    id 127
    label "fabrication"
  ]
  node [
    id 128
    label "ziszczenie_si&#281;"
  ]
  node [
    id 129
    label "pojawienie_si&#281;"
  ]
  node [
    id 130
    label "dzie&#322;o"
  ]
  node [
    id 131
    label "production"
  ]
  node [
    id 132
    label "completion"
  ]
  node [
    id 133
    label "realizacja"
  ]
  node [
    id 134
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 135
    label "whole"
  ]
  node [
    id 136
    label "odm&#322;adza&#263;"
  ]
  node [
    id 137
    label "zabudowania"
  ]
  node [
    id 138
    label "odm&#322;odzenie"
  ]
  node [
    id 139
    label "zespolik"
  ]
  node [
    id 140
    label "skupienie"
  ]
  node [
    id 141
    label "schorzenie"
  ]
  node [
    id 142
    label "grupa"
  ]
  node [
    id 143
    label "Depeche_Mode"
  ]
  node [
    id 144
    label "Mazowsze"
  ]
  node [
    id 145
    label "ro&#347;lina"
  ]
  node [
    id 146
    label "The_Beatles"
  ]
  node [
    id 147
    label "group"
  ]
  node [
    id 148
    label "&#346;wietliki"
  ]
  node [
    id 149
    label "odm&#322;adzanie"
  ]
  node [
    id 150
    label "batch"
  ]
  node [
    id 151
    label "Mickiewicz"
  ]
  node [
    id 152
    label "czas"
  ]
  node [
    id 153
    label "szkolenie"
  ]
  node [
    id 154
    label "przepisa&#263;"
  ]
  node [
    id 155
    label "lesson"
  ]
  node [
    id 156
    label "praktyka"
  ]
  node [
    id 157
    label "metoda"
  ]
  node [
    id 158
    label "niepokalanki"
  ]
  node [
    id 159
    label "kara"
  ]
  node [
    id 160
    label "zda&#263;"
  ]
  node [
    id 161
    label "form"
  ]
  node [
    id 162
    label "kwalifikacje"
  ]
  node [
    id 163
    label "system"
  ]
  node [
    id 164
    label "przepisanie"
  ]
  node [
    id 165
    label "sztuba"
  ]
  node [
    id 166
    label "wiedza"
  ]
  node [
    id 167
    label "stopek"
  ]
  node [
    id 168
    label "school"
  ]
  node [
    id 169
    label "absolwent"
  ]
  node [
    id 170
    label "urszulanki"
  ]
  node [
    id 171
    label "gabinet"
  ]
  node [
    id 172
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 173
    label "ideologia"
  ]
  node [
    id 174
    label "lekcja"
  ]
  node [
    id 175
    label "muzyka"
  ]
  node [
    id 176
    label "podr&#281;cznik"
  ]
  node [
    id 177
    label "zdanie"
  ]
  node [
    id 178
    label "siedziba"
  ]
  node [
    id 179
    label "sekretariat"
  ]
  node [
    id 180
    label "nauka"
  ]
  node [
    id 181
    label "do&#347;wiadczenie"
  ]
  node [
    id 182
    label "tablica"
  ]
  node [
    id 183
    label "teren_szko&#322;y"
  ]
  node [
    id 184
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 185
    label "skolaryzacja"
  ]
  node [
    id 186
    label "&#322;awa_szkolna"
  ]
  node [
    id 187
    label "klasa"
  ]
  node [
    id 188
    label "pocz&#261;tkowy"
  ]
  node [
    id 189
    label "podstawowo"
  ]
  node [
    id 190
    label "najwa&#380;niejszy"
  ]
  node [
    id 191
    label "niezaawansowany"
  ]
  node [
    id 192
    label "invite"
  ]
  node [
    id 193
    label "ask"
  ]
  node [
    id 194
    label "oferowa&#263;"
  ]
  node [
    id 195
    label "zwraca&#263;_si&#281;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 10
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 11
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 59
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 15
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 77
  ]
  edge [
    source 16
    target 78
  ]
  edge [
    source 16
    target 79
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 80
  ]
  edge [
    source 18
    target 81
  ]
  edge [
    source 18
    target 82
  ]
  edge [
    source 18
    target 83
  ]
  edge [
    source 18
    target 84
  ]
  edge [
    source 18
    target 85
  ]
  edge [
    source 18
    target 86
  ]
  edge [
    source 18
    target 87
  ]
  edge [
    source 18
    target 88
  ]
  edge [
    source 18
    target 89
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 90
  ]
  edge [
    source 19
    target 91
  ]
  edge [
    source 19
    target 92
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 93
  ]
  edge [
    source 20
    target 94
  ]
  edge [
    source 20
    target 95
  ]
  edge [
    source 20
    target 96
  ]
  edge [
    source 20
    target 97
  ]
  edge [
    source 20
    target 98
  ]
  edge [
    source 20
    target 99
  ]
  edge [
    source 20
    target 100
  ]
  edge [
    source 20
    target 101
  ]
  edge [
    source 20
    target 102
  ]
  edge [
    source 20
    target 103
  ]
  edge [
    source 20
    target 104
  ]
  edge [
    source 20
    target 105
  ]
  edge [
    source 20
    target 106
  ]
  edge [
    source 20
    target 107
  ]
  edge [
    source 20
    target 108
  ]
  edge [
    source 20
    target 109
  ]
  edge [
    source 20
    target 110
  ]
  edge [
    source 20
    target 111
  ]
  edge [
    source 20
    target 112
  ]
  edge [
    source 20
    target 113
  ]
  edge [
    source 20
    target 114
  ]
  edge [
    source 20
    target 115
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 119
  ]
  edge [
    source 21
    target 120
  ]
  edge [
    source 21
    target 121
  ]
  edge [
    source 21
    target 122
  ]
  edge [
    source 21
    target 123
  ]
  edge [
    source 21
    target 124
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 125
  ]
  edge [
    source 22
    target 126
  ]
  edge [
    source 22
    target 127
  ]
  edge [
    source 22
    target 128
  ]
  edge [
    source 22
    target 129
  ]
  edge [
    source 22
    target 130
  ]
  edge [
    source 22
    target 131
  ]
  edge [
    source 22
    target 132
  ]
  edge [
    source 22
    target 133
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 134
  ]
  edge [
    source 23
    target 135
  ]
  edge [
    source 23
    target 136
  ]
  edge [
    source 23
    target 137
  ]
  edge [
    source 23
    target 138
  ]
  edge [
    source 23
    target 139
  ]
  edge [
    source 23
    target 140
  ]
  edge [
    source 23
    target 141
  ]
  edge [
    source 23
    target 142
  ]
  edge [
    source 23
    target 143
  ]
  edge [
    source 23
    target 144
  ]
  edge [
    source 23
    target 145
  ]
  edge [
    source 23
    target 68
  ]
  edge [
    source 23
    target 146
  ]
  edge [
    source 23
    target 147
  ]
  edge [
    source 23
    target 148
  ]
  edge [
    source 23
    target 149
  ]
  edge [
    source 23
    target 150
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 151
  ]
  edge [
    source 25
    target 152
  ]
  edge [
    source 25
    target 153
  ]
  edge [
    source 25
    target 154
  ]
  edge [
    source 25
    target 155
  ]
  edge [
    source 25
    target 142
  ]
  edge [
    source 25
    target 156
  ]
  edge [
    source 25
    target 157
  ]
  edge [
    source 25
    target 158
  ]
  edge [
    source 25
    target 159
  ]
  edge [
    source 25
    target 160
  ]
  edge [
    source 25
    target 161
  ]
  edge [
    source 25
    target 162
  ]
  edge [
    source 25
    target 163
  ]
  edge [
    source 25
    target 164
  ]
  edge [
    source 25
    target 165
  ]
  edge [
    source 25
    target 166
  ]
  edge [
    source 25
    target 167
  ]
  edge [
    source 25
    target 168
  ]
  edge [
    source 25
    target 169
  ]
  edge [
    source 25
    target 170
  ]
  edge [
    source 25
    target 171
  ]
  edge [
    source 25
    target 172
  ]
  edge [
    source 25
    target 173
  ]
  edge [
    source 25
    target 174
  ]
  edge [
    source 25
    target 175
  ]
  edge [
    source 25
    target 176
  ]
  edge [
    source 25
    target 177
  ]
  edge [
    source 25
    target 178
  ]
  edge [
    source 25
    target 179
  ]
  edge [
    source 25
    target 180
  ]
  edge [
    source 25
    target 181
  ]
  edge [
    source 25
    target 182
  ]
  edge [
    source 25
    target 183
  ]
  edge [
    source 25
    target 72
  ]
  edge [
    source 25
    target 184
  ]
  edge [
    source 25
    target 185
  ]
  edge [
    source 25
    target 186
  ]
  edge [
    source 25
    target 187
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 188
  ]
  edge [
    source 26
    target 189
  ]
  edge [
    source 26
    target 190
  ]
  edge [
    source 26
    target 191
  ]
  edge [
    source 27
    target 192
  ]
  edge [
    source 27
    target 193
  ]
  edge [
    source 27
    target 194
  ]
  edge [
    source 27
    target 195
  ]
]
