graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9047619047619047
  density 0.09523809523809523
  graphCliqueNumber 2
  node [
    id 0
    label "mandat"
    origin "text"
  ]
  node [
    id 1
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 2
    label "dokument"
  ]
  node [
    id 3
    label "pe&#322;nomocnictwo"
  ]
  node [
    id 4
    label "kara_pieni&#281;&#380;na"
  ]
  node [
    id 5
    label "commission"
  ]
  node [
    id 6
    label "szlachetny"
  ]
  node [
    id 7
    label "metaliczny"
  ]
  node [
    id 8
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 9
    label "z&#322;ocenie"
  ]
  node [
    id 10
    label "grosz"
  ]
  node [
    id 11
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 12
    label "utytu&#322;owany"
  ]
  node [
    id 13
    label "poz&#322;ocenie"
  ]
  node [
    id 14
    label "Polska"
  ]
  node [
    id 15
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 16
    label "wspania&#322;y"
  ]
  node [
    id 17
    label "doskona&#322;y"
  ]
  node [
    id 18
    label "kochany"
  ]
  node [
    id 19
    label "jednostka_monetarna"
  ]
  node [
    id 20
    label "z&#322;oci&#347;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
]
