graph [
  maxDegree 23
  minDegree 1
  meanDegree 2
  density 0.03389830508474576
  graphCliqueNumber 2
  node [
    id 0
    label "ulubiony"
    origin "text"
  ]
  node [
    id 1
    label "rozrywka"
    origin "text"
  ]
  node [
    id 2
    label "moi"
    origin "text"
  ]
  node [
    id 3
    label "kot"
    origin "text"
  ]
  node [
    id 4
    label "ogl&#261;da&#263;"
    origin "text"
  ]
  node [
    id 5
    label "wirowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "b&#281;ben"
    origin "text"
  ]
  node [
    id 7
    label "pralka"
    origin "text"
  ]
  node [
    id 8
    label "pokazkota"
    origin "text"
  ]
  node [
    id 9
    label "duszekitygrysek"
    origin "text"
  ]
  node [
    id 10
    label "wyj&#261;tkowy"
  ]
  node [
    id 11
    label "faworytny"
  ]
  node [
    id 12
    label "czasoumilacz"
  ]
  node [
    id 13
    label "game"
  ]
  node [
    id 14
    label "odpoczynek"
  ]
  node [
    id 15
    label "zamiaucze&#263;"
  ]
  node [
    id 16
    label "pierwszoklasista"
  ]
  node [
    id 17
    label "fala"
  ]
  node [
    id 18
    label "miaucze&#263;"
  ]
  node [
    id 19
    label "kabanos"
  ]
  node [
    id 20
    label "kotowate"
  ]
  node [
    id 21
    label "miaukni&#281;cie"
  ]
  node [
    id 22
    label "samiec"
  ]
  node [
    id 23
    label "otrz&#281;siny"
  ]
  node [
    id 24
    label "miauczenie"
  ]
  node [
    id 25
    label "czworon&#243;g"
  ]
  node [
    id 26
    label "ssak_drapie&#380;ny"
  ]
  node [
    id 27
    label "rekrut"
  ]
  node [
    id 28
    label "zaj&#261;c"
  ]
  node [
    id 29
    label "kotwica"
  ]
  node [
    id 30
    label "trackball"
  ]
  node [
    id 31
    label "felinoterapia"
  ]
  node [
    id 32
    label "odk&#322;aczacz"
  ]
  node [
    id 33
    label "zamiauczenie"
  ]
  node [
    id 34
    label "notice"
  ]
  node [
    id 35
    label "zapoznawa&#263;_si&#281;"
  ]
  node [
    id 36
    label "styka&#263;_si&#281;"
  ]
  node [
    id 37
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 38
    label "wirzy&#263;_si&#281;"
  ]
  node [
    id 39
    label "mie&#263;_miejsce"
  ]
  node [
    id 40
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 41
    label "porusza&#263;_si&#281;"
  ]
  node [
    id 42
    label "whirl"
  ]
  node [
    id 43
    label "okr&#281;ca&#263;_si&#281;"
  ]
  node [
    id 44
    label "wirzy&#263;"
  ]
  node [
    id 45
    label "rusza&#263;"
  ]
  node [
    id 46
    label "szpulka"
  ]
  node [
    id 47
    label "kopu&#322;a"
  ]
  node [
    id 48
    label "podstawa"
  ]
  node [
    id 49
    label "dziecko"
  ]
  node [
    id 50
    label "cylinder"
  ]
  node [
    id 51
    label "maszyna"
  ]
  node [
    id 52
    label "naci&#261;g_perkusyjny"
  ]
  node [
    id 53
    label "d&#378;wig"
  ]
  node [
    id 54
    label "brzuszysko"
  ]
  node [
    id 55
    label "membranofon"
  ]
  node [
    id 56
    label "sprz&#281;t_AGD"
  ]
  node [
    id 57
    label "zawarto&#347;&#263;"
  ]
  node [
    id 58
    label "urz&#261;dzenie_domowe"
  ]
  node [
    id 59
    label "ilo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 8
    target 9
  ]
]
