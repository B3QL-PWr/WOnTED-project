graph [
  maxDegree 2
  minDegree 1
  meanDegree 1.4285714285714286
  density 0.10989010989010989
  graphCliqueNumber 3
  node [
    id 0
    label "michai&#322;"
    origin "text"
  ]
  node [
    id 1
    label "iwanow"
    origin "text"
  ]
  node [
    id 2
    label "Michai&#322;"
  ]
  node [
    id 3
    label "Iwanow"
  ]
  node [
    id 4
    label "salto"
  ]
  node [
    id 5
    label "Lake"
  ]
  node [
    id 6
    label "city"
  ]
  node [
    id 7
    label "zimowy"
  ]
  node [
    id 8
    label "igrzysko"
  ]
  node [
    id 9
    label "olimpijski"
  ]
  node [
    id 10
    label "Johanna"
  ]
  node [
    id 11
    label "M&#252;hlegga"
  ]
  node [
    id 12
    label "puchar"
  ]
  node [
    id 13
    label "&#347;wiat"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 12
    target 13
  ]
]
