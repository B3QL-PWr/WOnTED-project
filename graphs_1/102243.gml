graph [
  maxDegree 33
  minDegree 1
  meanDegree 9.01639344262295
  density 0.15027322404371585
  graphCliqueNumber 20
  node [
    id 0
    label "dziennik"
    origin "text"
  ]
  node [
    id 1
    label "ustawa"
    origin "text"
  ]
  node [
    id 2
    label "marco"
    origin "text"
  ]
  node [
    id 3
    label "poz"
    origin "text"
  ]
  node [
    id 4
    label "spis"
  ]
  node [
    id 5
    label "sheet"
  ]
  node [
    id 6
    label "gazeta"
  ]
  node [
    id 7
    label "diariusz"
  ]
  node [
    id 8
    label "pami&#281;tnik"
  ]
  node [
    id 9
    label "journal"
  ]
  node [
    id 10
    label "ksi&#281;ga"
  ]
  node [
    id 11
    label "program_informacyjny"
  ]
  node [
    id 12
    label "Karta_Nauczyciela"
  ]
  node [
    id 13
    label "marc&#243;wka"
  ]
  node [
    id 14
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 15
    label "akt"
  ]
  node [
    id 16
    label "przej&#347;&#263;"
  ]
  node [
    id 17
    label "charter"
  ]
  node [
    id 18
    label "przej&#347;cie"
  ]
  node [
    id 19
    label "ustawi&#263;"
  ]
  node [
    id 20
    label "trybuna&#322;"
  ]
  node [
    id 21
    label "konstytucyjny"
  ]
  node [
    id 22
    label "Ewa"
  ]
  node [
    id 23
    label "&#321;&#281;towska"
  ]
  node [
    id 24
    label "Marian"
  ]
  node [
    id 25
    label "grzybowski"
  ]
  node [
    id 26
    label "Wies&#322;awa"
  ]
  node [
    id 27
    label "Johann"
  ]
  node [
    id 28
    label "Biruta"
  ]
  node [
    id 29
    label "Lewaszkiewicz"
  ]
  node [
    id 30
    label "Petrykowska"
  ]
  node [
    id 31
    label "zdyba&#263;"
  ]
  node [
    id 32
    label "Krzysztofa"
  ]
  node [
    id 33
    label "Zalecki"
  ]
  node [
    id 34
    label "prokurator"
  ]
  node [
    id 35
    label "generalny"
  ]
  node [
    id 36
    label "polski"
  ]
  node [
    id 37
    label "organizacja"
  ]
  node [
    id 38
    label "pracodawca"
  ]
  node [
    id 39
    label "osoba"
  ]
  node [
    id 40
    label "niepe&#322;nosprawny"
  ]
  node [
    id 41
    label "zeszyt"
  ]
  node [
    id 42
    label "dzie&#324;"
  ]
  node [
    id 43
    label "27"
  ]
  node [
    id 44
    label "sierpie&#324;"
  ]
  node [
    id 45
    label "1997"
  ]
  node [
    id 46
    label "rok"
  ]
  node [
    id 47
    label "ojciec"
  ]
  node [
    id 48
    label "rehabilitacja"
  ]
  node [
    id 49
    label "zawodowy"
  ]
  node [
    id 50
    label "i"
  ]
  node [
    id 51
    label "spo&#322;eczny"
  ]
  node [
    id 52
    label "oraz"
  ]
  node [
    id 53
    label "zatrudnia&#263;"
  ]
  node [
    id 54
    label "u"
  ]
  node [
    id 55
    label "20"
  ]
  node [
    id 56
    label "grudzie&#324;"
  ]
  node [
    id 57
    label "2002"
  ]
  node [
    id 58
    label "zmiana"
  ]
  node [
    id 59
    label "niekt&#243;ry"
  ]
  node [
    id 60
    label "inny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 1
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 19
    target 41
  ]
  edge [
    source 19
    target 42
  ]
  edge [
    source 19
    target 55
  ]
  edge [
    source 19
    target 56
  ]
  edge [
    source 19
    target 57
  ]
  edge [
    source 19
    target 46
  ]
  edge [
    source 19
    target 47
  ]
  edge [
    source 19
    target 58
  ]
  edge [
    source 19
    target 48
  ]
  edge [
    source 19
    target 49
  ]
  edge [
    source 19
    target 50
  ]
  edge [
    source 19
    target 51
  ]
  edge [
    source 19
    target 52
  ]
  edge [
    source 19
    target 53
  ]
  edge [
    source 19
    target 39
  ]
  edge [
    source 19
    target 40
  ]
  edge [
    source 19
    target 59
  ]
  edge [
    source 19
    target 60
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 30
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 38
  ]
  edge [
    source 36
    target 39
  ]
  edge [
    source 36
    target 40
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 39
  ]
  edge [
    source 37
    target 40
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 40
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 41
  ]
  edge [
    source 39
    target 42
  ]
  edge [
    source 39
    target 43
  ]
  edge [
    source 39
    target 44
  ]
  edge [
    source 39
    target 45
  ]
  edge [
    source 39
    target 46
  ]
  edge [
    source 39
    target 47
  ]
  edge [
    source 39
    target 48
  ]
  edge [
    source 39
    target 49
  ]
  edge [
    source 39
    target 50
  ]
  edge [
    source 39
    target 51
  ]
  edge [
    source 39
    target 52
  ]
  edge [
    source 39
    target 53
  ]
  edge [
    source 39
    target 55
  ]
  edge [
    source 39
    target 56
  ]
  edge [
    source 39
    target 57
  ]
  edge [
    source 39
    target 58
  ]
  edge [
    source 39
    target 59
  ]
  edge [
    source 39
    target 60
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 42
  ]
  edge [
    source 40
    target 43
  ]
  edge [
    source 40
    target 44
  ]
  edge [
    source 40
    target 45
  ]
  edge [
    source 40
    target 46
  ]
  edge [
    source 40
    target 47
  ]
  edge [
    source 40
    target 48
  ]
  edge [
    source 40
    target 49
  ]
  edge [
    source 40
    target 50
  ]
  edge [
    source 40
    target 51
  ]
  edge [
    source 40
    target 52
  ]
  edge [
    source 40
    target 53
  ]
  edge [
    source 40
    target 55
  ]
  edge [
    source 40
    target 56
  ]
  edge [
    source 40
    target 57
  ]
  edge [
    source 40
    target 58
  ]
  edge [
    source 40
    target 59
  ]
  edge [
    source 40
    target 60
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 43
  ]
  edge [
    source 41
    target 44
  ]
  edge [
    source 41
    target 45
  ]
  edge [
    source 41
    target 46
  ]
  edge [
    source 41
    target 47
  ]
  edge [
    source 41
    target 48
  ]
  edge [
    source 41
    target 49
  ]
  edge [
    source 41
    target 50
  ]
  edge [
    source 41
    target 51
  ]
  edge [
    source 41
    target 52
  ]
  edge [
    source 41
    target 53
  ]
  edge [
    source 41
    target 55
  ]
  edge [
    source 41
    target 56
  ]
  edge [
    source 41
    target 57
  ]
  edge [
    source 41
    target 58
  ]
  edge [
    source 41
    target 59
  ]
  edge [
    source 41
    target 60
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 42
    target 45
  ]
  edge [
    source 42
    target 46
  ]
  edge [
    source 42
    target 47
  ]
  edge [
    source 42
    target 48
  ]
  edge [
    source 42
    target 49
  ]
  edge [
    source 42
    target 50
  ]
  edge [
    source 42
    target 51
  ]
  edge [
    source 42
    target 52
  ]
  edge [
    source 42
    target 53
  ]
  edge [
    source 42
    target 55
  ]
  edge [
    source 42
    target 56
  ]
  edge [
    source 42
    target 57
  ]
  edge [
    source 42
    target 58
  ]
  edge [
    source 42
    target 59
  ]
  edge [
    source 42
    target 60
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 45
  ]
  edge [
    source 43
    target 46
  ]
  edge [
    source 43
    target 47
  ]
  edge [
    source 43
    target 48
  ]
  edge [
    source 43
    target 49
  ]
  edge [
    source 43
    target 50
  ]
  edge [
    source 43
    target 51
  ]
  edge [
    source 43
    target 52
  ]
  edge [
    source 43
    target 53
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 46
  ]
  edge [
    source 44
    target 47
  ]
  edge [
    source 44
    target 48
  ]
  edge [
    source 44
    target 49
  ]
  edge [
    source 44
    target 50
  ]
  edge [
    source 44
    target 51
  ]
  edge [
    source 44
    target 52
  ]
  edge [
    source 44
    target 53
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 47
  ]
  edge [
    source 45
    target 48
  ]
  edge [
    source 45
    target 49
  ]
  edge [
    source 45
    target 50
  ]
  edge [
    source 45
    target 51
  ]
  edge [
    source 45
    target 52
  ]
  edge [
    source 45
    target 53
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 48
  ]
  edge [
    source 46
    target 49
  ]
  edge [
    source 46
    target 50
  ]
  edge [
    source 46
    target 51
  ]
  edge [
    source 46
    target 52
  ]
  edge [
    source 46
    target 53
  ]
  edge [
    source 46
    target 55
  ]
  edge [
    source 46
    target 56
  ]
  edge [
    source 46
    target 57
  ]
  edge [
    source 46
    target 58
  ]
  edge [
    source 46
    target 59
  ]
  edge [
    source 46
    target 60
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 47
    target 50
  ]
  edge [
    source 47
    target 51
  ]
  edge [
    source 47
    target 52
  ]
  edge [
    source 47
    target 53
  ]
  edge [
    source 47
    target 55
  ]
  edge [
    source 47
    target 56
  ]
  edge [
    source 47
    target 57
  ]
  edge [
    source 47
    target 58
  ]
  edge [
    source 47
    target 47
  ]
  edge [
    source 47
    target 59
  ]
  edge [
    source 47
    target 60
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 50
  ]
  edge [
    source 48
    target 51
  ]
  edge [
    source 48
    target 52
  ]
  edge [
    source 48
    target 53
  ]
  edge [
    source 48
    target 55
  ]
  edge [
    source 48
    target 56
  ]
  edge [
    source 48
    target 57
  ]
  edge [
    source 48
    target 58
  ]
  edge [
    source 48
    target 59
  ]
  edge [
    source 48
    target 60
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 51
  ]
  edge [
    source 49
    target 52
  ]
  edge [
    source 49
    target 53
  ]
  edge [
    source 49
    target 55
  ]
  edge [
    source 49
    target 56
  ]
  edge [
    source 49
    target 57
  ]
  edge [
    source 49
    target 58
  ]
  edge [
    source 49
    target 59
  ]
  edge [
    source 49
    target 60
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 52
  ]
  edge [
    source 50
    target 53
  ]
  edge [
    source 50
    target 55
  ]
  edge [
    source 50
    target 56
  ]
  edge [
    source 50
    target 57
  ]
  edge [
    source 50
    target 58
  ]
  edge [
    source 50
    target 59
  ]
  edge [
    source 50
    target 60
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 53
  ]
  edge [
    source 51
    target 55
  ]
  edge [
    source 51
    target 56
  ]
  edge [
    source 51
    target 57
  ]
  edge [
    source 51
    target 58
  ]
  edge [
    source 51
    target 59
  ]
  edge [
    source 51
    target 60
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 55
  ]
  edge [
    source 52
    target 56
  ]
  edge [
    source 52
    target 57
  ]
  edge [
    source 52
    target 58
  ]
  edge [
    source 52
    target 52
  ]
  edge [
    source 52
    target 59
  ]
  edge [
    source 52
    target 60
  ]
  edge [
    source 53
    target 55
  ]
  edge [
    source 53
    target 56
  ]
  edge [
    source 53
    target 57
  ]
  edge [
    source 53
    target 58
  ]
  edge [
    source 53
    target 59
  ]
  edge [
    source 53
    target 60
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 57
  ]
  edge [
    source 55
    target 58
  ]
  edge [
    source 55
    target 59
  ]
  edge [
    source 55
    target 60
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 56
    target 58
  ]
  edge [
    source 56
    target 59
  ]
  edge [
    source 56
    target 60
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 58
    target 58
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 59
    target 60
  ]
]
