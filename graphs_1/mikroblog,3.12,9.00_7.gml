graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.8181818181818181
  density 0.18181818181818182
  graphCliqueNumber 2
  node [
    id 0
    label "mie&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 2
    label "w&#322;os"
    origin "text"
  ]
  node [
    id 3
    label "daleki"
  ]
  node [
    id 4
    label "d&#322;ugo"
  ]
  node [
    id 5
    label "ruch"
  ]
  node [
    id 6
    label "cebulka"
  ]
  node [
    id 7
    label "wytw&#243;r"
  ]
  node [
    id 8
    label "powierzchnia"
  ]
  node [
    id 9
    label "mieszek_w&#322;osowy"
  ]
  node [
    id 10
    label "tkanina"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
]
