graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.040201005025126
  density 0.010304045479924877
  graphCliqueNumber 2
  node [
    id 0
    label "prezentowa&#263;"
    origin "text"
  ]
  node [
    id 1
    label "gigachada"
    origin "text"
  ]
  node [
    id 2
    label "po&#347;r&#243;d"
    origin "text"
  ]
  node [
    id 3
    label "sernik"
    origin "text"
  ]
  node [
    id 4
    label "zero"
    origin "text"
  ]
  node [
    id 5
    label "rodzynek"
    origin "text"
  ]
  node [
    id 6
    label "wpakowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "tona"
    origin "text"
  ]
  node [
    id 8
    label "bia&#322;y"
    origin "text"
  ]
  node [
    id 9
    label "czekolada"
    origin "text"
  ]
  node [
    id 10
    label "sp&#243;d"
    origin "text"
  ]
  node [
    id 11
    label "majestatyczny"
    origin "text"
  ]
  node [
    id 12
    label "serniczek"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "czekoladowy"
    origin "text"
  ]
  node [
    id 15
    label "herbatnik"
    origin "text"
  ]
  node [
    id 16
    label "domowy"
    origin "text"
  ]
  node [
    id 17
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 18
    label "mi&#322;o&#347;&#263;"
    origin "text"
  ]
  node [
    id 19
    label "przez"
    origin "text"
  ]
  node [
    id 20
    label "rodzicielka"
    origin "text"
  ]
  node [
    id 21
    label "gotujzwykopem"
    origin "text"
  ]
  node [
    id 22
    label "slodkijezu"
    origin "text"
  ]
  node [
    id 23
    label "kuchnia"
    origin "text"
  ]
  node [
    id 24
    label "zapoznawa&#263;"
  ]
  node [
    id 25
    label "przedstawia&#263;"
  ]
  node [
    id 26
    label "present"
  ]
  node [
    id 27
    label "gra&#263;"
  ]
  node [
    id 28
    label "uprzedza&#263;"
  ]
  node [
    id 29
    label "represent"
  ]
  node [
    id 30
    label "program"
  ]
  node [
    id 31
    label "wyra&#380;a&#263;"
  ]
  node [
    id 32
    label "attest"
  ]
  node [
    id 33
    label "display"
  ]
  node [
    id 34
    label "cheesecake"
  ]
  node [
    id 35
    label "seryna"
  ]
  node [
    id 36
    label "spi&#380;arnia"
  ]
  node [
    id 37
    label "aminokwas"
  ]
  node [
    id 38
    label "fosfoproteina"
  ]
  node [
    id 39
    label "glikoproteina"
  ]
  node [
    id 40
    label "ciasto"
  ]
  node [
    id 41
    label "miernota"
  ]
  node [
    id 42
    label "g&#243;wno"
  ]
  node [
    id 43
    label "love"
  ]
  node [
    id 44
    label "podzia&#322;ka"
  ]
  node [
    id 45
    label "liczba"
  ]
  node [
    id 46
    label "cyfra"
  ]
  node [
    id 47
    label "punkt"
  ]
  node [
    id 48
    label "ilo&#347;&#263;"
  ]
  node [
    id 49
    label "brak"
  ]
  node [
    id 50
    label "ciura"
  ]
  node [
    id 51
    label "bakalie"
  ]
  node [
    id 52
    label "susz"
  ]
  node [
    id 53
    label "winogrono"
  ]
  node [
    id 54
    label "stuff"
  ]
  node [
    id 55
    label "entail"
  ]
  node [
    id 56
    label "sprowadzi&#263;"
  ]
  node [
    id 57
    label "w&#322;&#261;czy&#263;"
  ]
  node [
    id 58
    label "w&#322;o&#380;y&#263;"
  ]
  node [
    id 59
    label "wepchn&#261;&#263;"
  ]
  node [
    id 60
    label "kilogram"
  ]
  node [
    id 61
    label "kilotona"
  ]
  node [
    id 62
    label "metryczna_jednostka_masy"
  ]
  node [
    id 63
    label "cz&#322;owiek"
  ]
  node [
    id 64
    label "jasny"
  ]
  node [
    id 65
    label "typ_orientalny"
  ]
  node [
    id 66
    label "bezbarwny"
  ]
  node [
    id 67
    label "blady"
  ]
  node [
    id 68
    label "bierka_szachowa"
  ]
  node [
    id 69
    label "bia&#322;osk&#243;ry"
  ]
  node [
    id 70
    label "czysty"
  ]
  node [
    id 71
    label "Rosjanin"
  ]
  node [
    id 72
    label "siwy"
  ]
  node [
    id 73
    label "jasnosk&#243;ry"
  ]
  node [
    id 74
    label "bia&#322;as"
  ]
  node [
    id 75
    label "&#347;nie&#380;nie"
  ]
  node [
    id 76
    label "bia&#322;e"
  ]
  node [
    id 77
    label "nacjonalista"
  ]
  node [
    id 78
    label "bia&#322;a_n&#281;dza"
  ]
  node [
    id 79
    label "bia&#322;y_taniec"
  ]
  node [
    id 80
    label "konserwatysta"
  ]
  node [
    id 81
    label "dzia&#322;acz"
  ]
  node [
    id 82
    label "medyczny"
  ]
  node [
    id 83
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 84
    label "&#347;nie&#380;no"
  ]
  node [
    id 85
    label "bia&#322;o"
  ]
  node [
    id 86
    label "carat"
  ]
  node [
    id 87
    label "Polak"
  ]
  node [
    id 88
    label "bia&#322;y_murzyn"
  ]
  node [
    id 89
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 90
    label "dobry"
  ]
  node [
    id 91
    label "libera&#322;"
  ]
  node [
    id 92
    label "konszowa&#263;"
  ]
  node [
    id 93
    label "konszowanie"
  ]
  node [
    id 94
    label "nap&#243;j"
  ]
  node [
    id 95
    label "Wedel"
  ]
  node [
    id 96
    label "substancja"
  ]
  node [
    id 97
    label "wyr&#243;b_cukierniczy"
  ]
  node [
    id 98
    label "afrodyzjak"
  ]
  node [
    id 99
    label "mikstura"
  ]
  node [
    id 100
    label "d&#243;&#322;"
  ]
  node [
    id 101
    label "strona"
  ]
  node [
    id 102
    label "placek"
  ]
  node [
    id 103
    label "bielizna"
  ]
  node [
    id 104
    label "majestatycznie"
  ]
  node [
    id 105
    label "powa&#380;ny"
  ]
  node [
    id 106
    label "baronial"
  ]
  node [
    id 107
    label "pot&#281;&#380;nie"
  ]
  node [
    id 108
    label "dostojny"
  ]
  node [
    id 109
    label "okaza&#322;y"
  ]
  node [
    id 110
    label "si&#281;ga&#263;"
  ]
  node [
    id 111
    label "trwa&#263;"
  ]
  node [
    id 112
    label "obecno&#347;&#263;"
  ]
  node [
    id 113
    label "stan"
  ]
  node [
    id 114
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 115
    label "stand"
  ]
  node [
    id 116
    label "mie&#263;_miejsce"
  ]
  node [
    id 117
    label "uczestniczy&#263;"
  ]
  node [
    id 118
    label "chodzi&#263;"
  ]
  node [
    id 119
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 120
    label "equal"
  ]
  node [
    id 121
    label "s&#322;odki"
  ]
  node [
    id 122
    label "aromatyczny"
  ]
  node [
    id 123
    label "czekoladowo"
  ]
  node [
    id 124
    label "ciep&#322;y"
  ]
  node [
    id 125
    label "ciemnobr&#261;zowy"
  ]
  node [
    id 126
    label "ciastko"
  ]
  node [
    id 127
    label "alkoholik"
  ]
  node [
    id 128
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 129
    label "domowo"
  ]
  node [
    id 130
    label "budynkowy"
  ]
  node [
    id 131
    label "zorganizowa&#263;"
  ]
  node [
    id 132
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 133
    label "przerobi&#263;"
  ]
  node [
    id 134
    label "wystylizowa&#263;"
  ]
  node [
    id 135
    label "cause"
  ]
  node [
    id 136
    label "wydali&#263;"
  ]
  node [
    id 137
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 138
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 139
    label "post&#261;pi&#263;"
  ]
  node [
    id 140
    label "appoint"
  ]
  node [
    id 141
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 142
    label "nabra&#263;"
  ]
  node [
    id 143
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 144
    label "make"
  ]
  node [
    id 145
    label "tendency"
  ]
  node [
    id 146
    label "erotyka"
  ]
  node [
    id 147
    label "serce"
  ]
  node [
    id 148
    label "podniecanie"
  ]
  node [
    id 149
    label "feblik"
  ]
  node [
    id 150
    label "wzw&#243;d"
  ]
  node [
    id 151
    label "droga"
  ]
  node [
    id 152
    label "ukochanie"
  ]
  node [
    id 153
    label "rozmna&#380;anie"
  ]
  node [
    id 154
    label "po&#380;&#261;danie"
  ]
  node [
    id 155
    label "imisja"
  ]
  node [
    id 156
    label "po&#380;ycie"
  ]
  node [
    id 157
    label "pozycja_misjonarska"
  ]
  node [
    id 158
    label "podnieci&#263;"
  ]
  node [
    id 159
    label "podnieca&#263;"
  ]
  node [
    id 160
    label "na_je&#378;d&#378;ca"
  ]
  node [
    id 161
    label "wi&#281;&#378;"
  ]
  node [
    id 162
    label "czynno&#347;&#263;"
  ]
  node [
    id 163
    label "afekt"
  ]
  node [
    id 164
    label "wsp&#243;&#322;&#380;ycie"
  ]
  node [
    id 165
    label "gra_wst&#281;pna"
  ]
  node [
    id 166
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 167
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 168
    label "numer"
  ]
  node [
    id 169
    label "ruch_frykcyjny"
  ]
  node [
    id 170
    label "baraszki"
  ]
  node [
    id 171
    label "zakochanie"
  ]
  node [
    id 172
    label "na_pieska"
  ]
  node [
    id 173
    label "emocja"
  ]
  node [
    id 174
    label "z&#322;&#261;czenie"
  ]
  node [
    id 175
    label "seks"
  ]
  node [
    id 176
    label "podniecenie"
  ]
  node [
    id 177
    label "drogi"
  ]
  node [
    id 178
    label "zajawka"
  ]
  node [
    id 179
    label "matczysko"
  ]
  node [
    id 180
    label "macierz"
  ]
  node [
    id 181
    label "przodkini"
  ]
  node [
    id 182
    label "Matka_Boska"
  ]
  node [
    id 183
    label "macocha"
  ]
  node [
    id 184
    label "matka_zast&#281;pcza"
  ]
  node [
    id 185
    label "stara"
  ]
  node [
    id 186
    label "rodzice"
  ]
  node [
    id 187
    label "rodzic"
  ]
  node [
    id 188
    label "st&#243;&#322;_kuchenny"
  ]
  node [
    id 189
    label "zlewozmywak"
  ]
  node [
    id 190
    label "tajniki"
  ]
  node [
    id 191
    label "jedzenie"
  ]
  node [
    id 192
    label "instytucja"
  ]
  node [
    id 193
    label "gotowa&#263;"
  ]
  node [
    id 194
    label "kultura"
  ]
  node [
    id 195
    label "zaplecze"
  ]
  node [
    id 196
    label "&#378;r&#243;d&#322;o"
  ]
  node [
    id 197
    label "pomieszczenie"
  ]
  node [
    id 198
    label "zaj&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 9
    target 99
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 128
  ]
  edge [
    source 16
    target 129
  ]
  edge [
    source 16
    target 130
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 17
    target 140
  ]
  edge [
    source 17
    target 141
  ]
  edge [
    source 17
    target 142
  ]
  edge [
    source 17
    target 143
  ]
  edge [
    source 17
    target 144
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 63
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 18
    target 152
  ]
  edge [
    source 18
    target 153
  ]
  edge [
    source 18
    target 154
  ]
  edge [
    source 18
    target 155
  ]
  edge [
    source 18
    target 156
  ]
  edge [
    source 18
    target 157
  ]
  edge [
    source 18
    target 158
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 160
  ]
  edge [
    source 18
    target 161
  ]
  edge [
    source 18
    target 162
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 43
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 188
  ]
  edge [
    source 23
    target 189
  ]
  edge [
    source 23
    target 190
  ]
  edge [
    source 23
    target 191
  ]
  edge [
    source 23
    target 192
  ]
  edge [
    source 23
    target 193
  ]
  edge [
    source 23
    target 194
  ]
  edge [
    source 23
    target 195
  ]
  edge [
    source 23
    target 196
  ]
  edge [
    source 23
    target 197
  ]
  edge [
    source 23
    target 198
  ]
]
