graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.96
  density 0.04
  graphCliqueNumber 2
  node [
    id 0
    label "imienie"
    origin "text"
  ]
  node [
    id 1
    label "rzeczpospolita"
    origin "text"
  ]
  node [
    id 2
    label "polski"
    origin "text"
  ]
  node [
    id 3
    label "Buriacja"
  ]
  node [
    id 4
    label "Abchazja"
  ]
  node [
    id 5
    label "Kabardo-Ba&#322;karia"
  ]
  node [
    id 6
    label "Inguszetia"
  ]
  node [
    id 7
    label "Nachiczewan"
  ]
  node [
    id 8
    label "Karaka&#322;pacja"
  ]
  node [
    id 9
    label "Jakucja"
  ]
  node [
    id 10
    label "Singapur"
  ]
  node [
    id 11
    label "Karelia"
  ]
  node [
    id 12
    label "Komi"
  ]
  node [
    id 13
    label "Tatarstan"
  ]
  node [
    id 14
    label "Chakasja"
  ]
  node [
    id 15
    label "Dagestan"
  ]
  node [
    id 16
    label "Mordowia"
  ]
  node [
    id 17
    label "Ka&#322;mucja"
  ]
  node [
    id 18
    label "Karaczajo-Czerkiesja"
  ]
  node [
    id 19
    label "Republika_Cisalpi&#324;ska"
  ]
  node [
    id 20
    label "Baszkiria"
  ]
  node [
    id 21
    label "pa&#324;stwo_demokratyczne"
  ]
  node [
    id 22
    label "Mari_El"
  ]
  node [
    id 23
    label "Ad&#380;aria"
  ]
  node [
    id 24
    label "Czuwaszja"
  ]
  node [
    id 25
    label "Tuwa"
  ]
  node [
    id 26
    label "Czeczenia"
  ]
  node [
    id 27
    label "Udmurcja"
  ]
  node [
    id 28
    label "pa&#324;stwo"
  ]
  node [
    id 29
    label "lacki"
  ]
  node [
    id 30
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 31
    label "przedmiot"
  ]
  node [
    id 32
    label "sztajer"
  ]
  node [
    id 33
    label "drabant"
  ]
  node [
    id 34
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 35
    label "polak"
  ]
  node [
    id 36
    label "pierogi_ruskie"
  ]
  node [
    id 37
    label "krakowiak"
  ]
  node [
    id 38
    label "Polish"
  ]
  node [
    id 39
    label "j&#281;zyk"
  ]
  node [
    id 40
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 41
    label "oberek"
  ]
  node [
    id 42
    label "po_polsku"
  ]
  node [
    id 43
    label "mazur"
  ]
  node [
    id 44
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 45
    label "chodzony"
  ]
  node [
    id 46
    label "skoczny"
  ]
  node [
    id 47
    label "ryba_po_grecku"
  ]
  node [
    id 48
    label "goniony"
  ]
  node [
    id 49
    label "polsko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
]
