graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.058823529411764705
  graphCliqueNumber 2
  node [
    id 0
    label "listopad"
    origin "text"
  ]
  node [
    id 1
    label "centrum"
    origin "text"
  ]
  node [
    id 2
    label "radom"
    origin "text"
  ]
  node [
    id 3
    label "pojawi&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "ma&#322;a"
    origin "text"
  ]
  node [
    id 6
    label "rze&#378;ba"
    origin "text"
  ]
  node [
    id 7
    label "br&#261;z"
    origin "text"
  ]
  node [
    id 8
    label "miesi&#261;c"
  ]
  node [
    id 9
    label "miejsce"
  ]
  node [
    id 10
    label "centroprawica"
  ]
  node [
    id 11
    label "core"
  ]
  node [
    id 12
    label "Hollywood"
  ]
  node [
    id 13
    label "blok"
  ]
  node [
    id 14
    label "centrolew"
  ]
  node [
    id 15
    label "sejm"
  ]
  node [
    id 16
    label "punkt"
  ]
  node [
    id 17
    label "o&#347;rodek"
  ]
  node [
    id 18
    label "dziewczynka"
  ]
  node [
    id 19
    label "dziewczyna"
  ]
  node [
    id 20
    label "bozzetto"
  ]
  node [
    id 21
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 22
    label "Ziemia"
  ]
  node [
    id 23
    label "rze&#378;biarstwo"
  ]
  node [
    id 24
    label "planacja"
  ]
  node [
    id 25
    label "plastyka"
  ]
  node [
    id 26
    label "ukszta&#322;towanie_terenu"
  ]
  node [
    id 27
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 28
    label "relief"
  ]
  node [
    id 29
    label "stop"
  ]
  node [
    id 30
    label "metal_kolorowy"
  ]
  node [
    id 31
    label "tangent"
  ]
  node [
    id 32
    label "medal"
  ]
  node [
    id 33
    label "kolor"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
]
