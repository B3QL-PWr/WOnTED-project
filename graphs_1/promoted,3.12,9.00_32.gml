graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8333333333333333
  density 0.16666666666666666
  graphCliqueNumber 2
  node [
    id 0
    label "marek"
    origin "text"
  ]
  node [
    id 1
    label "zd&#261;&#380;y&#263;"
    origin "text"
  ]
  node [
    id 2
    label "spier"
    origin "text"
  ]
  node [
    id 3
    label "li&#263;"
    origin "text"
  ]
  node [
    id 4
    label "selerowate"
  ]
  node [
    id 5
    label "bylina"
  ]
  node [
    id 6
    label "hygrofit"
  ]
  node [
    id 7
    label "spieszy&#263;_si&#281;"
  ]
  node [
    id 8
    label "po&#347;pie&#263;"
  ]
  node [
    id 9
    label "zjawi&#263;_si&#281;"
  ]
  node [
    id 10
    label "sko&#324;czy&#263;"
  ]
  node [
    id 11
    label "utrzyma&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
]
