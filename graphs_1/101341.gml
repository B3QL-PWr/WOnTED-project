graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.8
  density 0.09473684210526316
  graphCliqueNumber 2
  node [
    id 0
    label "kardyna&#322;ek"
    origin "text"
  ]
  node [
    id 1
    label "chi&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "karpiowate"
  ]
  node [
    id 3
    label "ryba"
  ]
  node [
    id 4
    label "kitajski"
  ]
  node [
    id 5
    label "j&#281;zyk"
  ]
  node [
    id 6
    label "kaczka_po_peki&#324;sku"
  ]
  node [
    id 7
    label "dziwaczny"
  ]
  node [
    id 8
    label "tandetny"
  ]
  node [
    id 9
    label "makroj&#281;zyk"
  ]
  node [
    id 10
    label "chi&#324;sko"
  ]
  node [
    id 11
    label "po_chi&#324;sku"
  ]
  node [
    id 12
    label "j&#281;zyk_chi&#324;sko-tybeta&#324;ski"
  ]
  node [
    id 13
    label "azjatycki"
  ]
  node [
    id 14
    label "lipny"
  ]
  node [
    id 15
    label "go"
  ]
  node [
    id 16
    label "niedrogi"
  ]
  node [
    id 17
    label "dalekowschodni"
  ]
  node [
    id 18
    label "g&#243;ry"
  ]
  node [
    id 19
    label "Po&#322;udniowochi&#324;skie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 18
    target 19
  ]
]
