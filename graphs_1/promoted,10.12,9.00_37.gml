graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.9759036144578312
  density 0.024096385542168676
  graphCliqueNumber 3
  node [
    id 0
    label "blisko"
    origin "text"
  ]
  node [
    id 1
    label "po&#322;a"
    origin "text"
  ]
  node [
    id 2
    label "rok"
    origin "text"
  ]
  node [
    id 3
    label "trwa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "&#347;ledztwo"
    origin "text"
  ]
  node [
    id 5
    label "sprawa"
    origin "text"
  ]
  node [
    id 6
    label "&#347;mier&#263;"
    origin "text"
  ]
  node [
    id 7
    label "szef"
    origin "text"
  ]
  node [
    id 8
    label "lubuski"
    origin "text"
  ]
  node [
    id 9
    label "antyterrorysta"
    origin "text"
  ]
  node [
    id 10
    label "adam"
    origin "text"
  ]
  node [
    id 11
    label "pawlak"
    origin "text"
  ]
  node [
    id 12
    label "o&#347;rodek"
    origin "text"
  ]
  node [
    id 13
    label "&#347;winouj&#347;cie"
    origin "text"
  ]
  node [
    id 14
    label "dok&#322;adnie"
  ]
  node [
    id 15
    label "bliski"
  ]
  node [
    id 16
    label "silnie"
  ]
  node [
    id 17
    label "tu&#380;_tu&#380;"
  ]
  node [
    id 18
    label "str&#243;j"
  ]
  node [
    id 19
    label "fragment"
  ]
  node [
    id 20
    label "stulecie"
  ]
  node [
    id 21
    label "kalendarz"
  ]
  node [
    id 22
    label "czas"
  ]
  node [
    id 23
    label "pora_roku"
  ]
  node [
    id 24
    label "cykl_astronomiczny"
  ]
  node [
    id 25
    label "p&#243;&#322;rocze"
  ]
  node [
    id 26
    label "grupa"
  ]
  node [
    id 27
    label "kwarta&#322;"
  ]
  node [
    id 28
    label "kurs"
  ]
  node [
    id 29
    label "jubileusz"
  ]
  node [
    id 30
    label "miesi&#261;c"
  ]
  node [
    id 31
    label "lata"
  ]
  node [
    id 32
    label "martwy_sezon"
  ]
  node [
    id 33
    label "zostawa&#263;"
  ]
  node [
    id 34
    label "upiera&#263;_si&#281;"
  ]
  node [
    id 35
    label "pozostawa&#263;"
  ]
  node [
    id 36
    label "stand"
  ]
  node [
    id 37
    label "adhere"
  ]
  node [
    id 38
    label "istnie&#263;"
  ]
  node [
    id 39
    label "detektyw"
  ]
  node [
    id 40
    label "wydarzenie"
  ]
  node [
    id 41
    label "examination"
  ]
  node [
    id 42
    label "temat"
  ]
  node [
    id 43
    label "kognicja"
  ]
  node [
    id 44
    label "idea"
  ]
  node [
    id 45
    label "szczeg&#243;&#322;"
  ]
  node [
    id 46
    label "rzecz"
  ]
  node [
    id 47
    label "przes&#322;anka"
  ]
  node [
    id 48
    label "rozprawa"
  ]
  node [
    id 49
    label "object"
  ]
  node [
    id 50
    label "proposition"
  ]
  node [
    id 51
    label "defenestracja"
  ]
  node [
    id 52
    label "kres"
  ]
  node [
    id 53
    label "agonia"
  ]
  node [
    id 54
    label "&#380;ycie"
  ]
  node [
    id 55
    label "szeol"
  ]
  node [
    id 56
    label "mogi&#322;a"
  ]
  node [
    id 57
    label "pogrzeb"
  ]
  node [
    id 58
    label "istota_nadprzyrodzona"
  ]
  node [
    id 59
    label "&#380;a&#322;oba"
  ]
  node [
    id 60
    label "pogrzebanie"
  ]
  node [
    id 61
    label "upadek"
  ]
  node [
    id 62
    label "zabicie"
  ]
  node [
    id 63
    label "kres_&#380;ycia"
  ]
  node [
    id 64
    label "cz&#322;owiek"
  ]
  node [
    id 65
    label "kierowa&#263;"
  ]
  node [
    id 66
    label "zwrot"
  ]
  node [
    id 67
    label "kierownictwo"
  ]
  node [
    id 68
    label "pryncypa&#322;"
  ]
  node [
    id 69
    label "antyterroryzm"
  ]
  node [
    id 70
    label "szturmowiec"
  ]
  node [
    id 71
    label "miejsce"
  ]
  node [
    id 72
    label "Hollywood"
  ]
  node [
    id 73
    label "zal&#261;&#380;ek"
  ]
  node [
    id 74
    label "otoczenie"
  ]
  node [
    id 75
    label "o&#347;rodkowy_uk&#322;ad_nerwowy"
  ]
  node [
    id 76
    label "&#347;rodek"
  ]
  node [
    id 77
    label "center"
  ]
  node [
    id 78
    label "instytucja"
  ]
  node [
    id 79
    label "skupisko"
  ]
  node [
    id 80
    label "warunki"
  ]
  node [
    id 81
    label "Adam"
  ]
  node [
    id 82
    label "Pawlak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 71
  ]
  edge [
    source 12
    target 72
  ]
  edge [
    source 12
    target 73
  ]
  edge [
    source 12
    target 74
  ]
  edge [
    source 12
    target 75
  ]
  edge [
    source 12
    target 76
  ]
  edge [
    source 12
    target 77
  ]
  edge [
    source 12
    target 78
  ]
  edge [
    source 12
    target 79
  ]
  edge [
    source 12
    target 80
  ]
  edge [
    source 81
    target 82
  ]
]
