graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.13333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 1
    label "krystyn"
    origin "text"
  ]
  node [
    id 2
    label "skowro&#324;ska"
    origin "text"
  ]
  node [
    id 3
    label "dyplomata"
  ]
  node [
    id 4
    label "wys&#322;annik"
  ]
  node [
    id 5
    label "przedstawiciel"
  ]
  node [
    id 6
    label "kurier_dyplomatyczny"
  ]
  node [
    id 7
    label "ablegat"
  ]
  node [
    id 8
    label "klubista"
  ]
  node [
    id 9
    label "Miko&#322;ajczyk"
  ]
  node [
    id 10
    label "Korwin"
  ]
  node [
    id 11
    label "parlamentarzysta"
  ]
  node [
    id 12
    label "dyscyplina_partyjna"
  ]
  node [
    id 13
    label "izba_ni&#380;sza"
  ]
  node [
    id 14
    label "poselstwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
]
