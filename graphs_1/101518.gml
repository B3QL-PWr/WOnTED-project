graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.878787878787879
  density 0.058712121212121215
  graphCliqueNumber 2
  node [
    id 0
    label "jeszcze"
    origin "text"
  ]
  node [
    id 1
    label "tym"
    origin "text"
  ]
  node [
    id 2
    label "&#322;atwo"
    origin "text"
  ]
  node [
    id 3
    label "salusia"
    origin "text"
  ]
  node [
    id 4
    label "zrobi&#263;"
    origin "text"
  ]
  node [
    id 5
    label "zdo&#322;a&#263;"
    origin "text"
  ]
  node [
    id 6
    label "ci&#261;gle"
  ]
  node [
    id 7
    label "&#322;atwy"
  ]
  node [
    id 8
    label "szybko"
  ]
  node [
    id 9
    label "&#322;atwie"
  ]
  node [
    id 10
    label "prosto"
  ]
  node [
    id 11
    label "snadnie"
  ]
  node [
    id 12
    label "przyjemnie"
  ]
  node [
    id 13
    label "&#322;acno"
  ]
  node [
    id 14
    label "zorganizowa&#263;"
  ]
  node [
    id 15
    label "zaj&#261;&#263;_si&#281;"
  ]
  node [
    id 16
    label "przerobi&#263;"
  ]
  node [
    id 17
    label "wystylizowa&#263;"
  ]
  node [
    id 18
    label "cause"
  ]
  node [
    id 19
    label "wydali&#263;"
  ]
  node [
    id 20
    label "przypiecz&#281;towa&#263;"
  ]
  node [
    id 21
    label "urz&#261;dzi&#263;"
  ]
  node [
    id 22
    label "post&#261;pi&#263;"
  ]
  node [
    id 23
    label "appoint"
  ]
  node [
    id 24
    label "zrz&#261;dzi&#263;"
  ]
  node [
    id 25
    label "nabra&#263;"
  ]
  node [
    id 26
    label "wcieli&#263;_si&#281;"
  ]
  node [
    id 27
    label "make"
  ]
  node [
    id 28
    label "plan"
  ]
  node [
    id 29
    label "zdole&#263;"
  ]
  node [
    id 30
    label "poradzi&#263;_sobie"
  ]
  node [
    id 31
    label "Onufry"
  ]
  node [
    id 32
    label "Cydzika"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 31
    target 32
  ]
]
