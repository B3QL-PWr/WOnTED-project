graph [
  maxDegree 36
  minDegree 1
  meanDegree 2
  density 0.015503875968992248
  graphCliqueNumber 3
  node [
    id 0
    label "alkohol"
    origin "text"
  ]
  node [
    id 1
    label "burda"
    origin "text"
  ]
  node [
    id 2
    label "wizja"
    origin "text"
  ]
  node [
    id 3
    label "nawet"
    origin "text"
  ]
  node [
    id 4
    label "nak&#322;ania&#263;"
    origin "text"
  ]
  node [
    id 5
    label "pedofilia"
    origin "text"
  ]
  node [
    id 6
    label "ciemno"
    origin "text"
  ]
  node [
    id 7
    label "strona"
    origin "text"
  ]
  node [
    id 8
    label "polski"
    origin "text"
  ]
  node [
    id 9
    label "internet"
    origin "text"
  ]
  node [
    id 10
    label "gorzelnia_rolnicza"
  ]
  node [
    id 11
    label "upija&#263;"
  ]
  node [
    id 12
    label "szk&#322;o"
  ]
  node [
    id 13
    label "spirytualia"
  ]
  node [
    id 14
    label "nap&#243;j"
  ]
  node [
    id 15
    label "wypicie"
  ]
  node [
    id 16
    label "poniewierca"
  ]
  node [
    id 17
    label "rozgrzewacz"
  ]
  node [
    id 18
    label "upajanie"
  ]
  node [
    id 19
    label "piwniczka"
  ]
  node [
    id 20
    label "najebka"
  ]
  node [
    id 21
    label "grupa_hydroksylowa"
  ]
  node [
    id 22
    label "le&#380;akownia"
  ]
  node [
    id 23
    label "g&#322;owa"
  ]
  node [
    id 24
    label "upi&#263;"
  ]
  node [
    id 25
    label "upojenie"
  ]
  node [
    id 26
    label "likwor"
  ]
  node [
    id 27
    label "u&#380;ywka"
  ]
  node [
    id 28
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 29
    label "alko"
  ]
  node [
    id 30
    label "picie"
  ]
  node [
    id 31
    label "scene"
  ]
  node [
    id 32
    label "zaj&#347;cie"
  ]
  node [
    id 33
    label "konflikt"
  ]
  node [
    id 34
    label "projekcja"
  ]
  node [
    id 35
    label "idea"
  ]
  node [
    id 36
    label "obraz"
  ]
  node [
    id 37
    label "przywidzenie"
  ]
  node [
    id 38
    label "widok"
  ]
  node [
    id 39
    label "przeplot"
  ]
  node [
    id 40
    label "ostro&#347;&#263;"
  ]
  node [
    id 41
    label "ziarno"
  ]
  node [
    id 42
    label "u&#322;uda"
  ]
  node [
    id 43
    label "zach&#281;ca&#263;"
  ]
  node [
    id 44
    label "act"
  ]
  node [
    id 45
    label "pedophilia"
  ]
  node [
    id 46
    label "przest&#281;pstwo"
  ]
  node [
    id 47
    label "parafilia"
  ]
  node [
    id 48
    label "ciemnota"
  ]
  node [
    id 49
    label "ciemniej"
  ]
  node [
    id 50
    label "noktowizja"
  ]
  node [
    id 51
    label "&#263;ma"
  ]
  node [
    id 52
    label "Ereb"
  ]
  node [
    id 53
    label "&#378;le"
  ]
  node [
    id 54
    label "pomrok"
  ]
  node [
    id 55
    label "ciemny"
  ]
  node [
    id 56
    label "sowie_oczy"
  ]
  node [
    id 57
    label "zjawisko"
  ]
  node [
    id 58
    label "cloud"
  ]
  node [
    id 59
    label "skr&#281;canie"
  ]
  node [
    id 60
    label "voice"
  ]
  node [
    id 61
    label "forma"
  ]
  node [
    id 62
    label "skr&#281;ci&#263;"
  ]
  node [
    id 63
    label "kartka"
  ]
  node [
    id 64
    label "orientowa&#263;"
  ]
  node [
    id 65
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 66
    label "powierzchnia"
  ]
  node [
    id 67
    label "plik"
  ]
  node [
    id 68
    label "bok"
  ]
  node [
    id 69
    label "pagina"
  ]
  node [
    id 70
    label "orientowanie"
  ]
  node [
    id 71
    label "fragment"
  ]
  node [
    id 72
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 73
    label "s&#261;d"
  ]
  node [
    id 74
    label "skr&#281;ca&#263;"
  ]
  node [
    id 75
    label "g&#243;ra"
  ]
  node [
    id 76
    label "serwis_internetowy"
  ]
  node [
    id 77
    label "orientacja"
  ]
  node [
    id 78
    label "linia"
  ]
  node [
    id 79
    label "skr&#281;cenie"
  ]
  node [
    id 80
    label "layout"
  ]
  node [
    id 81
    label "zorientowa&#263;"
  ]
  node [
    id 82
    label "zorientowanie"
  ]
  node [
    id 83
    label "obiekt"
  ]
  node [
    id 84
    label "podmiot"
  ]
  node [
    id 85
    label "ty&#322;"
  ]
  node [
    id 86
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 87
    label "logowanie"
  ]
  node [
    id 88
    label "adres_internetowy"
  ]
  node [
    id 89
    label "uj&#281;cie"
  ]
  node [
    id 90
    label "prz&#243;d"
  ]
  node [
    id 91
    label "posta&#263;"
  ]
  node [
    id 92
    label "lacki"
  ]
  node [
    id 93
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 94
    label "przedmiot"
  ]
  node [
    id 95
    label "sztajer"
  ]
  node [
    id 96
    label "drabant"
  ]
  node [
    id 97
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 98
    label "polak"
  ]
  node [
    id 99
    label "pierogi_ruskie"
  ]
  node [
    id 100
    label "krakowiak"
  ]
  node [
    id 101
    label "Polish"
  ]
  node [
    id 102
    label "j&#281;zyk"
  ]
  node [
    id 103
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 104
    label "oberek"
  ]
  node [
    id 105
    label "po_polsku"
  ]
  node [
    id 106
    label "mazur"
  ]
  node [
    id 107
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 108
    label "chodzony"
  ]
  node [
    id 109
    label "skoczny"
  ]
  node [
    id 110
    label "ryba_po_grecku"
  ]
  node [
    id 111
    label "goniony"
  ]
  node [
    id 112
    label "polsko"
  ]
  node [
    id 113
    label "us&#322;uga_internetowa"
  ]
  node [
    id 114
    label "biznes_elektroniczny"
  ]
  node [
    id 115
    label "punkt_dost&#281;pu"
  ]
  node [
    id 116
    label "hipertekst"
  ]
  node [
    id 117
    label "gra_sieciowa"
  ]
  node [
    id 118
    label "mem"
  ]
  node [
    id 119
    label "e-hazard"
  ]
  node [
    id 120
    label "sie&#263;_komputerowa"
  ]
  node [
    id 121
    label "media"
  ]
  node [
    id 122
    label "podcast"
  ]
  node [
    id 123
    label "netbook"
  ]
  node [
    id 124
    label "provider"
  ]
  node [
    id 125
    label "cyberprzestrze&#324;"
  ]
  node [
    id 126
    label "grooming"
  ]
  node [
    id 127
    label "mi&#281;dzyinnymi"
  ]
  node [
    id 128
    label "Google"
  ]
  node [
    id 129
    label "Polska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 8
    target 93
  ]
  edge [
    source 8
    target 94
  ]
  edge [
    source 8
    target 95
  ]
  edge [
    source 8
    target 96
  ]
  edge [
    source 8
    target 97
  ]
  edge [
    source 8
    target 98
  ]
  edge [
    source 8
    target 99
  ]
  edge [
    source 8
    target 100
  ]
  edge [
    source 8
    target 101
  ]
  edge [
    source 8
    target 102
  ]
  edge [
    source 8
    target 103
  ]
  edge [
    source 8
    target 104
  ]
  edge [
    source 8
    target 105
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 8
    target 108
  ]
  edge [
    source 8
    target 109
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 8
    target 111
  ]
  edge [
    source 8
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 127
    target 128
  ]
  edge [
    source 127
    target 129
  ]
  edge [
    source 128
    target 129
  ]
]
