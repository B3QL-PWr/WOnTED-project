graph [
  maxDegree 324
  minDegree 1
  meanDegree 2
  density 0.004807692307692308
  graphCliqueNumber 3
  node [
    id 0
    label "nowy"
    origin "text"
  ]
  node [
    id 1
    label "ambasador"
    origin "text"
  ]
  node [
    id 2
    label "izrael"
    origin "text"
  ]
  node [
    id 3
    label "ukraina"
    origin "text"
  ]
  node [
    id 4
    label "nazwa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "narodowy"
    origin "text"
  ]
  node [
    id 6
    label "bohater"
    origin "text"
  ]
  node [
    id 7
    label "kraj"
    origin "text"
  ]
  node [
    id 8
    label "historycznie"
    origin "text"
  ]
  node [
    id 9
    label "horror"
    origin "text"
  ]
  node [
    id 10
    label "dla"
    origin "text"
  ]
  node [
    id 11
    label "&#380;yd"
    origin "text"
  ]
  node [
    id 12
    label "informowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "telegraficzny"
    origin "text"
  ]
  node [
    id 14
    label "jta"
    origin "text"
  ]
  node [
    id 15
    label "cz&#322;owiek"
  ]
  node [
    id 16
    label "nowotny"
  ]
  node [
    id 17
    label "drugi"
  ]
  node [
    id 18
    label "kolejny"
  ]
  node [
    id 19
    label "bie&#380;&#261;cy"
  ]
  node [
    id 20
    label "nowo"
  ]
  node [
    id 21
    label "narybek"
  ]
  node [
    id 22
    label "wsp&#243;&#322;czesny"
  ]
  node [
    id 23
    label "obcy"
  ]
  node [
    id 24
    label "dyplomata"
  ]
  node [
    id 25
    label "zwolennik"
  ]
  node [
    id 26
    label "chor&#261;&#380;y"
  ]
  node [
    id 27
    label "tuba"
  ]
  node [
    id 28
    label "przedstawiciel"
  ]
  node [
    id 29
    label "hay_fever"
  ]
  node [
    id 30
    label "rozsiewca"
  ]
  node [
    id 31
    label "popularyzator"
  ]
  node [
    id 32
    label "broadcast"
  ]
  node [
    id 33
    label "okre&#347;li&#263;"
  ]
  node [
    id 34
    label "nada&#263;"
  ]
  node [
    id 35
    label "nacjonalistyczny"
  ]
  node [
    id 36
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 37
    label "narodowo"
  ]
  node [
    id 38
    label "wa&#380;ny"
  ]
  node [
    id 39
    label "bohaterski"
  ]
  node [
    id 40
    label "Zgredek"
  ]
  node [
    id 41
    label "Herkules"
  ]
  node [
    id 42
    label "Casanova"
  ]
  node [
    id 43
    label "Borewicz"
  ]
  node [
    id 44
    label "Don_Juan"
  ]
  node [
    id 45
    label "posta&#263;_mitologiczna"
  ]
  node [
    id 46
    label "Winnetou"
  ]
  node [
    id 47
    label "niezwyci&#281;&#380;ony"
  ]
  node [
    id 48
    label "Messi"
  ]
  node [
    id 49
    label "Herkules_Poirot"
  ]
  node [
    id 50
    label "cz&#322;owiek_wielkiego_ducha"
  ]
  node [
    id 51
    label "Szwejk"
  ]
  node [
    id 52
    label "Sherlock_Holmes"
  ]
  node [
    id 53
    label "Fantastyczna_Czw&#243;rka"
  ]
  node [
    id 54
    label "Hamlet"
  ]
  node [
    id 55
    label "Asterix"
  ]
  node [
    id 56
    label "Quasimodo"
  ]
  node [
    id 57
    label "Wallenrod"
  ]
  node [
    id 58
    label "Don_Kiszot"
  ]
  node [
    id 59
    label "uczestnik"
  ]
  node [
    id 60
    label "&#347;mia&#322;ek"
  ]
  node [
    id 61
    label "Harry_Potter"
  ]
  node [
    id 62
    label "podmiot"
  ]
  node [
    id 63
    label "Achilles"
  ]
  node [
    id 64
    label "Werter"
  ]
  node [
    id 65
    label "Mario"
  ]
  node [
    id 66
    label "posta&#263;"
  ]
  node [
    id 67
    label "Skandynawia"
  ]
  node [
    id 68
    label "Filipiny"
  ]
  node [
    id 69
    label "Rwanda"
  ]
  node [
    id 70
    label "Kaukaz"
  ]
  node [
    id 71
    label "Kaszmir"
  ]
  node [
    id 72
    label "Toskania"
  ]
  node [
    id 73
    label "Yorkshire"
  ]
  node [
    id 74
    label "&#321;emkowszczyzna"
  ]
  node [
    id 75
    label "obszar"
  ]
  node [
    id 76
    label "Sudan_Po&#322;udniowy"
  ]
  node [
    id 77
    label "Monako"
  ]
  node [
    id 78
    label "Amhara"
  ]
  node [
    id 79
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 80
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 81
    label "Lombardia"
  ]
  node [
    id 82
    label "Korea"
  ]
  node [
    id 83
    label "Kalabria"
  ]
  node [
    id 84
    label "Ghana"
  ]
  node [
    id 85
    label "Czarnog&#243;ra"
  ]
  node [
    id 86
    label "Tyrol"
  ]
  node [
    id 87
    label "Malawi"
  ]
  node [
    id 88
    label "Indonezja"
  ]
  node [
    id 89
    label "Bu&#322;garia"
  ]
  node [
    id 90
    label "Nauru"
  ]
  node [
    id 91
    label "Kenia"
  ]
  node [
    id 92
    label "Pamir"
  ]
  node [
    id 93
    label "Kambod&#380;a"
  ]
  node [
    id 94
    label "Lubelszczyzna"
  ]
  node [
    id 95
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 96
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 97
    label "Mali"
  ]
  node [
    id 98
    label "&#379;ywiecczyzna"
  ]
  node [
    id 99
    label "Austria"
  ]
  node [
    id 100
    label "interior"
  ]
  node [
    id 101
    label "Europa_Wschodnia"
  ]
  node [
    id 102
    label "Armenia"
  ]
  node [
    id 103
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 104
    label "Fid&#380;i"
  ]
  node [
    id 105
    label "Tuwalu"
  ]
  node [
    id 106
    label "Zabajkale"
  ]
  node [
    id 107
    label "Etiopia"
  ]
  node [
    id 108
    label "Malta"
  ]
  node [
    id 109
    label "Malezja"
  ]
  node [
    id 110
    label "Kaszuby"
  ]
  node [
    id 111
    label "Bo&#347;nia"
  ]
  node [
    id 112
    label "Noworosja"
  ]
  node [
    id 113
    label "Grenada"
  ]
  node [
    id 114
    label "Tad&#380;ykistan"
  ]
  node [
    id 115
    label "Ba&#322;kany"
  ]
  node [
    id 116
    label "Wehrlen"
  ]
  node [
    id 117
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 118
    label "Anglia"
  ]
  node [
    id 119
    label "Kielecczyzna"
  ]
  node [
    id 120
    label "Rumunia"
  ]
  node [
    id 121
    label "Pomorze_Zachodnie"
  ]
  node [
    id 122
    label "Maroko"
  ]
  node [
    id 123
    label "Bhutan"
  ]
  node [
    id 124
    label "Opolskie"
  ]
  node [
    id 125
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 126
    label "Ko&#322;yma"
  ]
  node [
    id 127
    label "Oksytania"
  ]
  node [
    id 128
    label "S&#322;owacja"
  ]
  node [
    id 129
    label "Kr&#243;lestwo_Kongresowe"
  ]
  node [
    id 130
    label "Seszele"
  ]
  node [
    id 131
    label "Syjon"
  ]
  node [
    id 132
    label "Kuwejt"
  ]
  node [
    id 133
    label "Arabia_Saudyjska"
  ]
  node [
    id 134
    label "Kociewie"
  ]
  node [
    id 135
    label "Ekwador"
  ]
  node [
    id 136
    label "Kanada"
  ]
  node [
    id 137
    label "ziemia"
  ]
  node [
    id 138
    label "Japonia"
  ]
  node [
    id 139
    label "Kraj_Nadwi&#347;la&#324;ski"
  ]
  node [
    id 140
    label "Hiszpania"
  ]
  node [
    id 141
    label "Wyspy_Marshalla"
  ]
  node [
    id 142
    label "Botswana"
  ]
  node [
    id 143
    label "Bo&#347;nia_i_Hercegowina"
  ]
  node [
    id 144
    label "D&#380;ibuti"
  ]
  node [
    id 145
    label "Huculszczyzna"
  ]
  node [
    id 146
    label "Wietnam"
  ]
  node [
    id 147
    label "Egipt"
  ]
  node [
    id 148
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 149
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 150
    label "Burkina_Faso"
  ]
  node [
    id 151
    label "Bawaria"
  ]
  node [
    id 152
    label "Niemcy"
  ]
  node [
    id 153
    label "Khitai"
  ]
  node [
    id 154
    label "Macedonia"
  ]
  node [
    id 155
    label "Albania"
  ]
  node [
    id 156
    label "Madagaskar"
  ]
  node [
    id 157
    label "Bahrajn"
  ]
  node [
    id 158
    label "Jemen"
  ]
  node [
    id 159
    label "Lesoto"
  ]
  node [
    id 160
    label "Maghreb"
  ]
  node [
    id 161
    label "Samoa"
  ]
  node [
    id 162
    label "Andora"
  ]
  node [
    id 163
    label "Bory_Tucholskie"
  ]
  node [
    id 164
    label "Chiny"
  ]
  node [
    id 165
    label "Europa_Zachodnia"
  ]
  node [
    id 166
    label "Cypr"
  ]
  node [
    id 167
    label "Wielka_Brytania"
  ]
  node [
    id 168
    label "Kerala"
  ]
  node [
    id 169
    label "Podhale"
  ]
  node [
    id 170
    label "Kabylia"
  ]
  node [
    id 171
    label "Ukraina"
  ]
  node [
    id 172
    label "Paragwaj"
  ]
  node [
    id 173
    label "Trynidad_i_Tobago"
  ]
  node [
    id 174
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 175
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 176
    label "Ma&#322;opolska"
  ]
  node [
    id 177
    label "Polesie"
  ]
  node [
    id 178
    label "Liguria"
  ]
  node [
    id 179
    label "Libia"
  ]
  node [
    id 180
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 181
    label "&#321;&#243;dzkie"
  ]
  node [
    id 182
    label "Surinam"
  ]
  node [
    id 183
    label "Republika_Zielonego_Przyl&#261;dka"
  ]
  node [
    id 184
    label "Palestyna"
  ]
  node [
    id 185
    label "Australia"
  ]
  node [
    id 186
    label "Nigeria"
  ]
  node [
    id 187
    label "Honduras"
  ]
  node [
    id 188
    label "Bojkowszczyzna"
  ]
  node [
    id 189
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 190
    label "Karaiby"
  ]
  node [
    id 191
    label "Bangladesz"
  ]
  node [
    id 192
    label "Peru"
  ]
  node [
    id 193
    label "Kazachstan"
  ]
  node [
    id 194
    label "USA"
  ]
  node [
    id 195
    label "Irak"
  ]
  node [
    id 196
    label "Nepal"
  ]
  node [
    id 197
    label "S&#261;decczyzna"
  ]
  node [
    id 198
    label "Sudan"
  ]
  node [
    id 199
    label "Sand&#380;ak"
  ]
  node [
    id 200
    label "Nadrenia"
  ]
  node [
    id 201
    label "San_Marino"
  ]
  node [
    id 202
    label "Burundi"
  ]
  node [
    id 203
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 204
    label "Dominikana"
  ]
  node [
    id 205
    label "Komory"
  ]
  node [
    id 206
    label "Zakarpacie"
  ]
  node [
    id 207
    label "Gwatemala"
  ]
  node [
    id 208
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 209
    label "Zag&#243;rze"
  ]
  node [
    id 210
    label "Andaluzja"
  ]
  node [
    id 211
    label "granica_pa&#324;stwa"
  ]
  node [
    id 212
    label "Turkiestan"
  ]
  node [
    id 213
    label "Naddniestrze"
  ]
  node [
    id 214
    label "Hercegowina"
  ]
  node [
    id 215
    label "Brunei"
  ]
  node [
    id 216
    label "Iran"
  ]
  node [
    id 217
    label "jednostka_administracyjna"
  ]
  node [
    id 218
    label "Zimbabwe"
  ]
  node [
    id 219
    label "Namibia"
  ]
  node [
    id 220
    label "Meksyk"
  ]
  node [
    id 221
    label "Lotaryngia"
  ]
  node [
    id 222
    label "Kamerun"
  ]
  node [
    id 223
    label "Opolszczyzna"
  ]
  node [
    id 224
    label "Afryka_Wschodnia"
  ]
  node [
    id 225
    label "Szlezwik"
  ]
  node [
    id 226
    label "Somalia"
  ]
  node [
    id 227
    label "Angola"
  ]
  node [
    id 228
    label "Gabon"
  ]
  node [
    id 229
    label "Korea_Po&#322;udniowa"
  ]
  node [
    id 230
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 231
    label "Mozambik"
  ]
  node [
    id 232
    label "Tajwan"
  ]
  node [
    id 233
    label "Tunezja"
  ]
  node [
    id 234
    label "Nowa_Zelandia"
  ]
  node [
    id 235
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 236
    label "Podbeskidzie"
  ]
  node [
    id 237
    label "Liban"
  ]
  node [
    id 238
    label "Jordania"
  ]
  node [
    id 239
    label "Tonga"
  ]
  node [
    id 240
    label "Czad"
  ]
  node [
    id 241
    label "Liberia"
  ]
  node [
    id 242
    label "Gwinea"
  ]
  node [
    id 243
    label "Belize"
  ]
  node [
    id 244
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 245
    label "Mazowsze"
  ]
  node [
    id 246
    label "&#321;otwa"
  ]
  node [
    id 247
    label "Syria"
  ]
  node [
    id 248
    label "Benin"
  ]
  node [
    id 249
    label "Afryka_Zachodnia"
  ]
  node [
    id 250
    label "Kraj_zli&#324;ski"
  ]
  node [
    id 251
    label "Dominika"
  ]
  node [
    id 252
    label "Antigua_i_Barbuda"
  ]
  node [
    id 253
    label "Bia&#322;oru&#347;"
  ]
  node [
    id 254
    label "Hanower"
  ]
  node [
    id 255
    label "Galicja"
  ]
  node [
    id 256
    label "Szkocja"
  ]
  node [
    id 257
    label "Walia"
  ]
  node [
    id 258
    label "Afganistan"
  ]
  node [
    id 259
    label "Kiribati"
  ]
  node [
    id 260
    label "W&#322;ochy"
  ]
  node [
    id 261
    label "Szwajcaria"
  ]
  node [
    id 262
    label "Powi&#347;le"
  ]
  node [
    id 263
    label "Sahara_Zachodnia"
  ]
  node [
    id 264
    label "Chorwacja"
  ]
  node [
    id 265
    label "Tajlandia"
  ]
  node [
    id 266
    label "Salwador"
  ]
  node [
    id 267
    label "Bahamy"
  ]
  node [
    id 268
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 269
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 270
    label "Zamojszczyzna"
  ]
  node [
    id 271
    label "Ludowa_Republika_Kampuczy"
  ]
  node [
    id 272
    label "S&#322;owenia"
  ]
  node [
    id 273
    label "Gambia"
  ]
  node [
    id 274
    label "Kujawy"
  ]
  node [
    id 275
    label "Urugwaj"
  ]
  node [
    id 276
    label "Podlasie"
  ]
  node [
    id 277
    label "Zair"
  ]
  node [
    id 278
    label "Erytrea"
  ]
  node [
    id 279
    label "Laponia"
  ]
  node [
    id 280
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 281
    label "Umbria"
  ]
  node [
    id 282
    label "Rosja"
  ]
  node [
    id 283
    label "Uganda"
  ]
  node [
    id 284
    label "Niger"
  ]
  node [
    id 285
    label "Mauritius"
  ]
  node [
    id 286
    label "Turkmenistan"
  ]
  node [
    id 287
    label "Turcja"
  ]
  node [
    id 288
    label "Mezoameryka"
  ]
  node [
    id 289
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 290
    label "Irlandia"
  ]
  node [
    id 291
    label "Republika_&#346;rodkowoafryka&#324;ska"
  ]
  node [
    id 292
    label "Republika_Po&#322;udniowej_Afryki"
  ]
  node [
    id 293
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 294
    label "Gwinea_Bissau"
  ]
  node [
    id 295
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 296
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 297
    label "Kurdystan"
  ]
  node [
    id 298
    label "Belgia"
  ]
  node [
    id 299
    label "Gwinea_R&#243;wnikowa"
  ]
  node [
    id 300
    label "Palau"
  ]
  node [
    id 301
    label "Barbados"
  ]
  node [
    id 302
    label "Chile"
  ]
  node [
    id 303
    label "Wenezuela"
  ]
  node [
    id 304
    label "W&#281;gry"
  ]
  node [
    id 305
    label "Argentyna"
  ]
  node [
    id 306
    label "Kolumbia"
  ]
  node [
    id 307
    label "Kampania"
  ]
  node [
    id 308
    label "Armagnac"
  ]
  node [
    id 309
    label "Sierra_Leone"
  ]
  node [
    id 310
    label "Azerbejd&#380;an"
  ]
  node [
    id 311
    label "Kongo"
  ]
  node [
    id 312
    label "Polinezja"
  ]
  node [
    id 313
    label "Warmia"
  ]
  node [
    id 314
    label "Pakistan"
  ]
  node [
    id 315
    label "Liechtenstein"
  ]
  node [
    id 316
    label "Wielkopolska"
  ]
  node [
    id 317
    label "Nikaragua"
  ]
  node [
    id 318
    label "Senegal"
  ]
  node [
    id 319
    label "brzeg"
  ]
  node [
    id 320
    label "Bordeaux"
  ]
  node [
    id 321
    label "Lauda"
  ]
  node [
    id 322
    label "Indie"
  ]
  node [
    id 323
    label "Mazury"
  ]
  node [
    id 324
    label "Suazi"
  ]
  node [
    id 325
    label "Polska"
  ]
  node [
    id 326
    label "Kr&#243;lestwo_Niderland&#243;w"
  ]
  node [
    id 327
    label "Algieria"
  ]
  node [
    id 328
    label "Jamajka"
  ]
  node [
    id 329
    label "Timor_Wschodni"
  ]
  node [
    id 330
    label "Oceania"
  ]
  node [
    id 331
    label "Kostaryka"
  ]
  node [
    id 332
    label "Podkarpacie"
  ]
  node [
    id 333
    label "Lasko"
  ]
  node [
    id 334
    label "Papua-Nowa_Gwinea"
  ]
  node [
    id 335
    label "Kuba"
  ]
  node [
    id 336
    label "Mauretania"
  ]
  node [
    id 337
    label "Amazonia"
  ]
  node [
    id 338
    label "barwy_pa&#324;stwowe"
  ]
  node [
    id 339
    label "Portoryko"
  ]
  node [
    id 340
    label "Brazylia"
  ]
  node [
    id 341
    label "Mo&#322;dawia"
  ]
  node [
    id 342
    label "organizacja"
  ]
  node [
    id 343
    label "Litwa"
  ]
  node [
    id 344
    label "Kirgistan"
  ]
  node [
    id 345
    label "Demokratyczna_Kampucza"
  ]
  node [
    id 346
    label "Izrael"
  ]
  node [
    id 347
    label "Grecja"
  ]
  node [
    id 348
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 349
    label "Kurpie"
  ]
  node [
    id 350
    label "Holandia"
  ]
  node [
    id 351
    label "Sri_Lanka"
  ]
  node [
    id 352
    label "Tonkin"
  ]
  node [
    id 353
    label "Katar"
  ]
  node [
    id 354
    label "Azja_Wschodnia"
  ]
  node [
    id 355
    label "Mikronezja"
  ]
  node [
    id 356
    label "Ukraina_Zachodnia"
  ]
  node [
    id 357
    label "Laos"
  ]
  node [
    id 358
    label "Mongolia"
  ]
  node [
    id 359
    label "Turyngia"
  ]
  node [
    id 360
    label "Malediwy"
  ]
  node [
    id 361
    label "Zambia"
  ]
  node [
    id 362
    label "Baszkiria"
  ]
  node [
    id 363
    label "Tanzania"
  ]
  node [
    id 364
    label "Gujana"
  ]
  node [
    id 365
    label "Apulia"
  ]
  node [
    id 366
    label "Czechy"
  ]
  node [
    id 367
    label "Panama"
  ]
  node [
    id 368
    label "Uzbekistan"
  ]
  node [
    id 369
    label "Gruzja"
  ]
  node [
    id 370
    label "nawa_pa&#324;stwowa"
  ]
  node [
    id 371
    label "Serbia"
  ]
  node [
    id 372
    label "Francja"
  ]
  node [
    id 373
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 374
    label "Togo"
  ]
  node [
    id 375
    label "Estonia"
  ]
  node [
    id 376
    label "Indochiny"
  ]
  node [
    id 377
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 378
    label "Oman"
  ]
  node [
    id 379
    label "Boliwia"
  ]
  node [
    id 380
    label "Portugalia"
  ]
  node [
    id 381
    label "Wyspy_Salomona"
  ]
  node [
    id 382
    label "Luksemburg"
  ]
  node [
    id 383
    label "Haiti"
  ]
  node [
    id 384
    label "Biskupizna"
  ]
  node [
    id 385
    label "Lubuskie"
  ]
  node [
    id 386
    label "Birma"
  ]
  node [
    id 387
    label "Rodezja"
  ]
  node [
    id 388
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 389
    label "historyczny"
  ]
  node [
    id 390
    label "pierwotnie"
  ]
  node [
    id 391
    label "dawniej"
  ]
  node [
    id 392
    label "zgodnie"
  ]
  node [
    id 393
    label "makabra"
  ]
  node [
    id 394
    label "powie&#347;&#263;_fantastyczna"
  ]
  node [
    id 395
    label "miscarriage"
  ]
  node [
    id 396
    label "dramat"
  ]
  node [
    id 397
    label "film_fabularny"
  ]
  node [
    id 398
    label "chciwiec"
  ]
  node [
    id 399
    label "judenrat"
  ]
  node [
    id 400
    label "materialista"
  ]
  node [
    id 401
    label "sk&#261;piarz"
  ]
  node [
    id 402
    label "wyznawca"
  ]
  node [
    id 403
    label "&#379;ydziak"
  ]
  node [
    id 404
    label "sk&#261;py"
  ]
  node [
    id 405
    label "szmonces"
  ]
  node [
    id 406
    label "monoteista"
  ]
  node [
    id 407
    label "mosiek"
  ]
  node [
    id 408
    label "komunikowa&#263;"
  ]
  node [
    id 409
    label "powiada&#263;"
  ]
  node [
    id 410
    label "inform"
  ]
  node [
    id 411
    label "zwi&#281;z&#322;y"
  ]
  node [
    id 412
    label "telegraficznie"
  ]
  node [
    id 413
    label "&#380;ydowski"
  ]
  node [
    id 414
    label "agencja"
  ]
  node [
    id 415
    label "Joel"
  ]
  node [
    id 416
    label "Lion"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 15
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 6
    target 59
  ]
  edge [
    source 6
    target 60
  ]
  edge [
    source 6
    target 61
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 7
    target 85
  ]
  edge [
    source 7
    target 86
  ]
  edge [
    source 7
    target 87
  ]
  edge [
    source 7
    target 88
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 7
    target 147
  ]
  edge [
    source 7
    target 148
  ]
  edge [
    source 7
    target 149
  ]
  edge [
    source 7
    target 150
  ]
  edge [
    source 7
    target 151
  ]
  edge [
    source 7
    target 152
  ]
  edge [
    source 7
    target 153
  ]
  edge [
    source 7
    target 154
  ]
  edge [
    source 7
    target 155
  ]
  edge [
    source 7
    target 156
  ]
  edge [
    source 7
    target 157
  ]
  edge [
    source 7
    target 158
  ]
  edge [
    source 7
    target 159
  ]
  edge [
    source 7
    target 160
  ]
  edge [
    source 7
    target 161
  ]
  edge [
    source 7
    target 162
  ]
  edge [
    source 7
    target 163
  ]
  edge [
    source 7
    target 164
  ]
  edge [
    source 7
    target 165
  ]
  edge [
    source 7
    target 166
  ]
  edge [
    source 7
    target 167
  ]
  edge [
    source 7
    target 168
  ]
  edge [
    source 7
    target 169
  ]
  edge [
    source 7
    target 170
  ]
  edge [
    source 7
    target 171
  ]
  edge [
    source 7
    target 172
  ]
  edge [
    source 7
    target 173
  ]
  edge [
    source 7
    target 174
  ]
  edge [
    source 7
    target 175
  ]
  edge [
    source 7
    target 176
  ]
  edge [
    source 7
    target 177
  ]
  edge [
    source 7
    target 178
  ]
  edge [
    source 7
    target 179
  ]
  edge [
    source 7
    target 180
  ]
  edge [
    source 7
    target 181
  ]
  edge [
    source 7
    target 182
  ]
  edge [
    source 7
    target 183
  ]
  edge [
    source 7
    target 184
  ]
  edge [
    source 7
    target 185
  ]
  edge [
    source 7
    target 186
  ]
  edge [
    source 7
    target 187
  ]
  edge [
    source 7
    target 188
  ]
  edge [
    source 7
    target 189
  ]
  edge [
    source 7
    target 190
  ]
  edge [
    source 7
    target 191
  ]
  edge [
    source 7
    target 192
  ]
  edge [
    source 7
    target 193
  ]
  edge [
    source 7
    target 194
  ]
  edge [
    source 7
    target 195
  ]
  edge [
    source 7
    target 196
  ]
  edge [
    source 7
    target 197
  ]
  edge [
    source 7
    target 198
  ]
  edge [
    source 7
    target 199
  ]
  edge [
    source 7
    target 200
  ]
  edge [
    source 7
    target 201
  ]
  edge [
    source 7
    target 202
  ]
  edge [
    source 7
    target 203
  ]
  edge [
    source 7
    target 204
  ]
  edge [
    source 7
    target 205
  ]
  edge [
    source 7
    target 206
  ]
  edge [
    source 7
    target 207
  ]
  edge [
    source 7
    target 208
  ]
  edge [
    source 7
    target 209
  ]
  edge [
    source 7
    target 210
  ]
  edge [
    source 7
    target 211
  ]
  edge [
    source 7
    target 212
  ]
  edge [
    source 7
    target 213
  ]
  edge [
    source 7
    target 214
  ]
  edge [
    source 7
    target 215
  ]
  edge [
    source 7
    target 216
  ]
  edge [
    source 7
    target 217
  ]
  edge [
    source 7
    target 218
  ]
  edge [
    source 7
    target 219
  ]
  edge [
    source 7
    target 220
  ]
  edge [
    source 7
    target 221
  ]
  edge [
    source 7
    target 222
  ]
  edge [
    source 7
    target 223
  ]
  edge [
    source 7
    target 224
  ]
  edge [
    source 7
    target 225
  ]
  edge [
    source 7
    target 226
  ]
  edge [
    source 7
    target 227
  ]
  edge [
    source 7
    target 228
  ]
  edge [
    source 7
    target 229
  ]
  edge [
    source 7
    target 230
  ]
  edge [
    source 7
    target 231
  ]
  edge [
    source 7
    target 232
  ]
  edge [
    source 7
    target 233
  ]
  edge [
    source 7
    target 234
  ]
  edge [
    source 7
    target 235
  ]
  edge [
    source 7
    target 236
  ]
  edge [
    source 7
    target 237
  ]
  edge [
    source 7
    target 238
  ]
  edge [
    source 7
    target 239
  ]
  edge [
    source 7
    target 240
  ]
  edge [
    source 7
    target 241
  ]
  edge [
    source 7
    target 242
  ]
  edge [
    source 7
    target 243
  ]
  edge [
    source 7
    target 244
  ]
  edge [
    source 7
    target 245
  ]
  edge [
    source 7
    target 246
  ]
  edge [
    source 7
    target 247
  ]
  edge [
    source 7
    target 248
  ]
  edge [
    source 7
    target 249
  ]
  edge [
    source 7
    target 250
  ]
  edge [
    source 7
    target 251
  ]
  edge [
    source 7
    target 252
  ]
  edge [
    source 7
    target 253
  ]
  edge [
    source 7
    target 254
  ]
  edge [
    source 7
    target 255
  ]
  edge [
    source 7
    target 256
  ]
  edge [
    source 7
    target 257
  ]
  edge [
    source 7
    target 258
  ]
  edge [
    source 7
    target 259
  ]
  edge [
    source 7
    target 260
  ]
  edge [
    source 7
    target 261
  ]
  edge [
    source 7
    target 262
  ]
  edge [
    source 7
    target 263
  ]
  edge [
    source 7
    target 264
  ]
  edge [
    source 7
    target 265
  ]
  edge [
    source 7
    target 266
  ]
  edge [
    source 7
    target 267
  ]
  edge [
    source 7
    target 268
  ]
  edge [
    source 7
    target 269
  ]
  edge [
    source 7
    target 270
  ]
  edge [
    source 7
    target 271
  ]
  edge [
    source 7
    target 272
  ]
  edge [
    source 7
    target 273
  ]
  edge [
    source 7
    target 274
  ]
  edge [
    source 7
    target 275
  ]
  edge [
    source 7
    target 276
  ]
  edge [
    source 7
    target 277
  ]
  edge [
    source 7
    target 278
  ]
  edge [
    source 7
    target 279
  ]
  edge [
    source 7
    target 280
  ]
  edge [
    source 7
    target 281
  ]
  edge [
    source 7
    target 282
  ]
  edge [
    source 7
    target 283
  ]
  edge [
    source 7
    target 284
  ]
  edge [
    source 7
    target 285
  ]
  edge [
    source 7
    target 286
  ]
  edge [
    source 7
    target 287
  ]
  edge [
    source 7
    target 288
  ]
  edge [
    source 7
    target 289
  ]
  edge [
    source 7
    target 290
  ]
  edge [
    source 7
    target 291
  ]
  edge [
    source 7
    target 292
  ]
  edge [
    source 7
    target 293
  ]
  edge [
    source 7
    target 294
  ]
  edge [
    source 7
    target 295
  ]
  edge [
    source 7
    target 296
  ]
  edge [
    source 7
    target 297
  ]
  edge [
    source 7
    target 298
  ]
  edge [
    source 7
    target 299
  ]
  edge [
    source 7
    target 300
  ]
  edge [
    source 7
    target 301
  ]
  edge [
    source 7
    target 302
  ]
  edge [
    source 7
    target 303
  ]
  edge [
    source 7
    target 304
  ]
  edge [
    source 7
    target 305
  ]
  edge [
    source 7
    target 306
  ]
  edge [
    source 7
    target 307
  ]
  edge [
    source 7
    target 308
  ]
  edge [
    source 7
    target 309
  ]
  edge [
    source 7
    target 310
  ]
  edge [
    source 7
    target 311
  ]
  edge [
    source 7
    target 312
  ]
  edge [
    source 7
    target 313
  ]
  edge [
    source 7
    target 314
  ]
  edge [
    source 7
    target 315
  ]
  edge [
    source 7
    target 316
  ]
  edge [
    source 7
    target 317
  ]
  edge [
    source 7
    target 318
  ]
  edge [
    source 7
    target 319
  ]
  edge [
    source 7
    target 320
  ]
  edge [
    source 7
    target 321
  ]
  edge [
    source 7
    target 322
  ]
  edge [
    source 7
    target 323
  ]
  edge [
    source 7
    target 324
  ]
  edge [
    source 7
    target 325
  ]
  edge [
    source 7
    target 326
  ]
  edge [
    source 7
    target 327
  ]
  edge [
    source 7
    target 328
  ]
  edge [
    source 7
    target 329
  ]
  edge [
    source 7
    target 330
  ]
  edge [
    source 7
    target 331
  ]
  edge [
    source 7
    target 332
  ]
  edge [
    source 7
    target 333
  ]
  edge [
    source 7
    target 334
  ]
  edge [
    source 7
    target 335
  ]
  edge [
    source 7
    target 336
  ]
  edge [
    source 7
    target 337
  ]
  edge [
    source 7
    target 338
  ]
  edge [
    source 7
    target 339
  ]
  edge [
    source 7
    target 340
  ]
  edge [
    source 7
    target 341
  ]
  edge [
    source 7
    target 342
  ]
  edge [
    source 7
    target 343
  ]
  edge [
    source 7
    target 344
  ]
  edge [
    source 7
    target 345
  ]
  edge [
    source 7
    target 346
  ]
  edge [
    source 7
    target 347
  ]
  edge [
    source 7
    target 348
  ]
  edge [
    source 7
    target 349
  ]
  edge [
    source 7
    target 350
  ]
  edge [
    source 7
    target 351
  ]
  edge [
    source 7
    target 352
  ]
  edge [
    source 7
    target 353
  ]
  edge [
    source 7
    target 354
  ]
  edge [
    source 7
    target 355
  ]
  edge [
    source 7
    target 356
  ]
  edge [
    source 7
    target 357
  ]
  edge [
    source 7
    target 358
  ]
  edge [
    source 7
    target 359
  ]
  edge [
    source 7
    target 360
  ]
  edge [
    source 7
    target 361
  ]
  edge [
    source 7
    target 362
  ]
  edge [
    source 7
    target 363
  ]
  edge [
    source 7
    target 364
  ]
  edge [
    source 7
    target 365
  ]
  edge [
    source 7
    target 366
  ]
  edge [
    source 7
    target 367
  ]
  edge [
    source 7
    target 368
  ]
  edge [
    source 7
    target 369
  ]
  edge [
    source 7
    target 370
  ]
  edge [
    source 7
    target 371
  ]
  edge [
    source 7
    target 372
  ]
  edge [
    source 7
    target 373
  ]
  edge [
    source 7
    target 374
  ]
  edge [
    source 7
    target 375
  ]
  edge [
    source 7
    target 376
  ]
  edge [
    source 7
    target 377
  ]
  edge [
    source 7
    target 378
  ]
  edge [
    source 7
    target 379
  ]
  edge [
    source 7
    target 380
  ]
  edge [
    source 7
    target 381
  ]
  edge [
    source 7
    target 382
  ]
  edge [
    source 7
    target 383
  ]
  edge [
    source 7
    target 384
  ]
  edge [
    source 7
    target 385
  ]
  edge [
    source 7
    target 386
  ]
  edge [
    source 7
    target 387
  ]
  edge [
    source 7
    target 388
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 389
  ]
  edge [
    source 8
    target 390
  ]
  edge [
    source 8
    target 391
  ]
  edge [
    source 8
    target 392
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 393
  ]
  edge [
    source 9
    target 394
  ]
  edge [
    source 9
    target 395
  ]
  edge [
    source 9
    target 396
  ]
  edge [
    source 9
    target 397
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 398
  ]
  edge [
    source 11
    target 399
  ]
  edge [
    source 11
    target 400
  ]
  edge [
    source 11
    target 401
  ]
  edge [
    source 11
    target 402
  ]
  edge [
    source 11
    target 403
  ]
  edge [
    source 11
    target 404
  ]
  edge [
    source 11
    target 405
  ]
  edge [
    source 11
    target 406
  ]
  edge [
    source 11
    target 407
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 408
  ]
  edge [
    source 12
    target 409
  ]
  edge [
    source 12
    target 410
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 411
  ]
  edge [
    source 13
    target 412
  ]
  edge [
    source 13
    target 413
  ]
  edge [
    source 13
    target 414
  ]
  edge [
    source 413
    target 414
  ]
  edge [
    source 415
    target 416
  ]
]
