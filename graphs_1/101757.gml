graph [
  maxDegree 30
  minDegree 1
  meanDegree 2.1485148514851486
  density 0.010689128614353973
  graphCliqueNumber 5
  node [
    id 0
    label "microsoft"
    origin "text"
  ]
  node [
    id 1
    label "raz"
    origin "text"
  ]
  node [
    id 2
    label "kolejny"
    origin "text"
  ]
  node [
    id 3
    label "udawa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "ugi&#261;&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "pod"
    origin "text"
  ]
  node [
    id 7
    label "nacisk"
    origin "text"
  ]
  node [
    id 8
    label "komisja"
    origin "text"
  ]
  node [
    id 9
    label "europejski"
    origin "text"
  ]
  node [
    id 10
    label "og&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 11
    label "zasada"
    origin "text"
  ]
  node [
    id 12
    label "interoperacyjno&#347;&#263;"
    origin "text"
  ]
  node [
    id 13
    label "obiecywa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "wolno&#347;&#263;"
    origin "text"
  ]
  node [
    id 15
    label "r&#243;wno&#347;&#263;"
    origin "text"
  ]
  node [
    id 16
    label "braterstwo"
    origin "text"
  ]
  node [
    id 17
    label "s&#322;owo"
    origin "text"
  ]
  node [
    id 18
    label "otwarty"
    origin "text"
  ]
  node [
    id 19
    label "powt&#243;rzy&#263;"
    origin "text"
  ]
  node [
    id 20
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 21
    label "czterdzie&#347;ci"
    origin "text"
  ]
  node [
    id 22
    label "otwarto&#347;&#263;"
    origin "text"
  ]
  node [
    id 23
    label "wed&#322;ug"
    origin "text"
  ]
  node [
    id 24
    label "oznacza&#263;"
    origin "text"
  ]
  node [
    id 25
    label "pierwsza"
    origin "text"
  ]
  node [
    id 26
    label "korzysta&#263;"
    origin "text"
  ]
  node [
    id 27
    label "protok&#243;&#322;"
    origin "text"
  ]
  node [
    id 28
    label "trzeba"
    origin "text"
  ]
  node [
    id 29
    label "p&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 30
    label "chwila"
  ]
  node [
    id 31
    label "uderzenie"
  ]
  node [
    id 32
    label "cios"
  ]
  node [
    id 33
    label "time"
  ]
  node [
    id 34
    label "inny"
  ]
  node [
    id 35
    label "nast&#281;pnie"
  ]
  node [
    id 36
    label "kt&#243;ry&#347;"
  ]
  node [
    id 37
    label "kolejno"
  ]
  node [
    id 38
    label "nastopny"
  ]
  node [
    id 39
    label "dally"
  ]
  node [
    id 40
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 41
    label "kopiowa&#263;"
  ]
  node [
    id 42
    label "robi&#263;"
  ]
  node [
    id 43
    label "sztuczny"
  ]
  node [
    id 44
    label "mock"
  ]
  node [
    id 45
    label "tone"
  ]
  node [
    id 46
    label "pochyli&#263;"
  ]
  node [
    id 47
    label "przygn&#281;bi&#263;"
  ]
  node [
    id 48
    label "crush"
  ]
  node [
    id 49
    label "zjawisko"
  ]
  node [
    id 50
    label "force"
  ]
  node [
    id 51
    label "uwaga"
  ]
  node [
    id 52
    label "wp&#322;yw"
  ]
  node [
    id 53
    label "obrady"
  ]
  node [
    id 54
    label "zesp&#243;&#322;"
  ]
  node [
    id 55
    label "organ"
  ]
  node [
    id 56
    label "Komisja_Europejska"
  ]
  node [
    id 57
    label "podkomisja"
  ]
  node [
    id 58
    label "European"
  ]
  node [
    id 59
    label "po_europejsku"
  ]
  node [
    id 60
    label "charakterystyczny"
  ]
  node [
    id 61
    label "europejsko"
  ]
  node [
    id 62
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 63
    label "typowy"
  ]
  node [
    id 64
    label "announce"
  ]
  node [
    id 65
    label "publikowa&#263;"
  ]
  node [
    id 66
    label "obwo&#322;ywa&#263;"
  ]
  node [
    id 67
    label "post"
  ]
  node [
    id 68
    label "podawa&#263;"
  ]
  node [
    id 69
    label "obserwacja"
  ]
  node [
    id 70
    label "moralno&#347;&#263;"
  ]
  node [
    id 71
    label "podstawa"
  ]
  node [
    id 72
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 73
    label "umowa"
  ]
  node [
    id 74
    label "dominion"
  ]
  node [
    id 75
    label "qualification"
  ]
  node [
    id 76
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 77
    label "opis"
  ]
  node [
    id 78
    label "regu&#322;a_Allena"
  ]
  node [
    id 79
    label "normalizacja"
  ]
  node [
    id 80
    label "regu&#322;a_Glogera"
  ]
  node [
    id 81
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 82
    label "standard"
  ]
  node [
    id 83
    label "base"
  ]
  node [
    id 84
    label "substancja"
  ]
  node [
    id 85
    label "spos&#243;b"
  ]
  node [
    id 86
    label "prawid&#322;o"
  ]
  node [
    id 87
    label "prawo_Mendla"
  ]
  node [
    id 88
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 89
    label "criterion"
  ]
  node [
    id 90
    label "twierdzenie"
  ]
  node [
    id 91
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 92
    label "prawo"
  ]
  node [
    id 93
    label "occupation"
  ]
  node [
    id 94
    label "zasada_d'Alemberta"
  ]
  node [
    id 95
    label "udost&#281;pnianie"
  ]
  node [
    id 96
    label "spos&#243;b_dzia&#322;ania"
  ]
  node [
    id 97
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 98
    label "pledge"
  ]
  node [
    id 99
    label "harbinger"
  ]
  node [
    id 100
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 101
    label "absolutno&#347;&#263;"
  ]
  node [
    id 102
    label "obecno&#347;&#263;"
  ]
  node [
    id 103
    label "uwi&#281;zi&#263;"
  ]
  node [
    id 104
    label "cecha"
  ]
  node [
    id 105
    label "freedom"
  ]
  node [
    id 106
    label "niezale&#380;no&#347;&#263;"
  ]
  node [
    id 107
    label "uwi&#281;zienie"
  ]
  node [
    id 108
    label "nieokre&#347;lono&#347;&#263;"
  ]
  node [
    id 109
    label "spok&#243;j"
  ]
  node [
    id 110
    label "sprawiedliwo&#347;&#263;"
  ]
  node [
    id 111
    label "g&#322;adko&#347;&#263;"
  ]
  node [
    id 112
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 113
    label "poj&#281;cie"
  ]
  node [
    id 114
    label "podobie&#324;stwo"
  ]
  node [
    id 115
    label "emocja"
  ]
  node [
    id 116
    label "przyja&#378;&#324;"
  ]
  node [
    id 117
    label "ma&#322;&#380;e&#324;stwo"
  ]
  node [
    id 118
    label "brotherhood"
  ]
  node [
    id 119
    label "wi&#281;&#378;"
  ]
  node [
    id 120
    label "obietnica"
  ]
  node [
    id 121
    label "bit"
  ]
  node [
    id 122
    label "s&#322;ownictwo"
  ]
  node [
    id 123
    label "jednostka_leksykalna"
  ]
  node [
    id 124
    label "pisanie_si&#281;"
  ]
  node [
    id 125
    label "wykrzyknik"
  ]
  node [
    id 126
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 127
    label "pole_semantyczne"
  ]
  node [
    id 128
    label "&#347;r&#243;dg&#322;os"
  ]
  node [
    id 129
    label "komunikat"
  ]
  node [
    id 130
    label "pisa&#263;_si&#281;"
  ]
  node [
    id 131
    label "wypowiedzenie"
  ]
  node [
    id 132
    label "nag&#322;os"
  ]
  node [
    id 133
    label "wordnet"
  ]
  node [
    id 134
    label "morfem"
  ]
  node [
    id 135
    label "czasownik"
  ]
  node [
    id 136
    label "wyg&#322;os"
  ]
  node [
    id 137
    label "jednostka_informacji"
  ]
  node [
    id 138
    label "ewidentny"
  ]
  node [
    id 139
    label "bezpo&#347;redni"
  ]
  node [
    id 140
    label "otwarcie"
  ]
  node [
    id 141
    label "nieograniczony"
  ]
  node [
    id 142
    label "zdecydowany"
  ]
  node [
    id 143
    label "gotowy"
  ]
  node [
    id 144
    label "aktualny"
  ]
  node [
    id 145
    label "prostoduszny"
  ]
  node [
    id 146
    label "jawnie"
  ]
  node [
    id 147
    label "otworzysty"
  ]
  node [
    id 148
    label "dost&#281;pny"
  ]
  node [
    id 149
    label "publiczny"
  ]
  node [
    id 150
    label "aktywny"
  ]
  node [
    id 151
    label "kontaktowy"
  ]
  node [
    id 152
    label "impart"
  ]
  node [
    id 153
    label "podszkoli&#263;_si&#281;"
  ]
  node [
    id 154
    label "reprise"
  ]
  node [
    id 155
    label "zrobi&#263;"
  ]
  node [
    id 156
    label "poda&#263;"
  ]
  node [
    id 157
    label "proceed"
  ]
  node [
    id 158
    label "catch"
  ]
  node [
    id 159
    label "pozosta&#263;"
  ]
  node [
    id 160
    label "osta&#263;_si&#281;"
  ]
  node [
    id 161
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 162
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 163
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 164
    label "change"
  ]
  node [
    id 165
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 166
    label "gotowo&#347;&#263;"
  ]
  node [
    id 167
    label "frankness"
  ]
  node [
    id 168
    label "jawno&#347;&#263;"
  ]
  node [
    id 169
    label "rozmiar"
  ]
  node [
    id 170
    label "nieograniczono&#347;&#263;"
  ]
  node [
    id 171
    label "kontaktowo&#347;&#263;"
  ]
  node [
    id 172
    label "posta&#263;"
  ]
  node [
    id 173
    label "prostoduszno&#347;&#263;"
  ]
  node [
    id 174
    label "by&#263;"
  ]
  node [
    id 175
    label "okre&#347;la&#263;"
  ]
  node [
    id 176
    label "represent"
  ]
  node [
    id 177
    label "wyraz"
  ]
  node [
    id 178
    label "wskazywa&#263;"
  ]
  node [
    id 179
    label "stanowi&#263;"
  ]
  node [
    id 180
    label "signify"
  ]
  node [
    id 181
    label "set"
  ]
  node [
    id 182
    label "ustala&#263;"
  ]
  node [
    id 183
    label "godzina"
  ]
  node [
    id 184
    label "use"
  ]
  node [
    id 185
    label "uzyskiwa&#263;"
  ]
  node [
    id 186
    label "u&#380;ywa&#263;"
  ]
  node [
    id 187
    label "komunikacja_zintegrowana"
  ]
  node [
    id 188
    label "akt"
  ]
  node [
    id 189
    label "relacja"
  ]
  node [
    id 190
    label "etykieta"
  ]
  node [
    id 191
    label "trza"
  ]
  node [
    id 192
    label "necessity"
  ]
  node [
    id 193
    label "buli&#263;"
  ]
  node [
    id 194
    label "osi&#261;ga&#263;"
  ]
  node [
    id 195
    label "give"
  ]
  node [
    id 196
    label "wydawa&#263;"
  ]
  node [
    id 197
    label "pay"
  ]
  node [
    id 198
    label "internet"
  ]
  node [
    id 199
    label "Explorer"
  ]
  node [
    id 200
    label "nasz"
  ]
  node [
    id 201
    label "klasa"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
  edge [
    source 9
    target 58
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 9
    target 62
  ]
  edge [
    source 9
    target 63
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 16
  ]
  edge [
    source 11
    target 69
  ]
  edge [
    source 11
    target 70
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 72
  ]
  edge [
    source 11
    target 73
  ]
  edge [
    source 11
    target 74
  ]
  edge [
    source 11
    target 75
  ]
  edge [
    source 11
    target 76
  ]
  edge [
    source 11
    target 77
  ]
  edge [
    source 11
    target 78
  ]
  edge [
    source 11
    target 79
  ]
  edge [
    source 11
    target 80
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 27
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 25
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 13
    target 25
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 101
  ]
  edge [
    source 14
    target 102
  ]
  edge [
    source 14
    target 103
  ]
  edge [
    source 14
    target 104
  ]
  edge [
    source 14
    target 105
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 14
    target 107
  ]
  edge [
    source 14
    target 108
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 15
    target 112
  ]
  edge [
    source 15
    target 113
  ]
  edge [
    source 15
    target 114
  ]
  edge [
    source 15
    target 19
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 20
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 120
  ]
  edge [
    source 17
    target 121
  ]
  edge [
    source 17
    target 122
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 17
    target 132
  ]
  edge [
    source 17
    target 133
  ]
  edge [
    source 17
    target 134
  ]
  edge [
    source 17
    target 135
  ]
  edge [
    source 17
    target 136
  ]
  edge [
    source 17
    target 137
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 26
  ]
  edge [
    source 18
    target 27
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 18
    target 139
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 18
    target 151
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 153
  ]
  edge [
    source 19
    target 154
  ]
  edge [
    source 19
    target 155
  ]
  edge [
    source 19
    target 156
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 157
  ]
  edge [
    source 20
    target 158
  ]
  edge [
    source 20
    target 159
  ]
  edge [
    source 20
    target 160
  ]
  edge [
    source 20
    target 161
  ]
  edge [
    source 20
    target 162
  ]
  edge [
    source 20
    target 163
  ]
  edge [
    source 20
    target 164
  ]
  edge [
    source 20
    target 165
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 166
  ]
  edge [
    source 22
    target 167
  ]
  edge [
    source 22
    target 104
  ]
  edge [
    source 22
    target 168
  ]
  edge [
    source 22
    target 169
  ]
  edge [
    source 22
    target 170
  ]
  edge [
    source 22
    target 171
  ]
  edge [
    source 22
    target 172
  ]
  edge [
    source 22
    target 173
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 174
  ]
  edge [
    source 24
    target 175
  ]
  edge [
    source 24
    target 176
  ]
  edge [
    source 24
    target 177
  ]
  edge [
    source 24
    target 178
  ]
  edge [
    source 24
    target 179
  ]
  edge [
    source 24
    target 180
  ]
  edge [
    source 24
    target 181
  ]
  edge [
    source 24
    target 182
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 183
  ]
  edge [
    source 26
    target 184
  ]
  edge [
    source 26
    target 185
  ]
  edge [
    source 26
    target 186
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 187
  ]
  edge [
    source 27
    target 188
  ]
  edge [
    source 27
    target 189
  ]
  edge [
    source 27
    target 190
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 191
  ]
  edge [
    source 28
    target 192
  ]
  edge [
    source 29
    target 193
  ]
  edge [
    source 29
    target 194
  ]
  edge [
    source 29
    target 195
  ]
  edge [
    source 29
    target 196
  ]
  edge [
    source 29
    target 197
  ]
  edge [
    source 198
    target 199
  ]
  edge [
    source 200
    target 201
  ]
]
