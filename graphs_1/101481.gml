graph [
  maxDegree 1
  minDegree 1
  meanDegree 1
  density 0.2
  graphCliqueNumber 2
  node [
    id 0
    label "jacek"
    origin "text"
  ]
  node [
    id 1
    label "sobieszcza&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "Jacek"
  ]
  node [
    id 3
    label "Sobieszcza&#324;ski"
  ]
  node [
    id 4
    label "Maciej"
  ]
  node [
    id 5
    label "nowak"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
]
