graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.75
  density 0.11666666666666667
  graphCliqueNumber 3
  node [
    id 0
    label "zygmunt"
    origin "text"
  ]
  node [
    id 1
    label "hohenzollern"
    origin "text"
  ]
  node [
    id 2
    label "arcybiskup"
    origin "text"
  ]
  node [
    id 3
    label "magdeburski"
    origin "text"
  ]
  node [
    id 4
    label "ordynariusz"
  ]
  node [
    id 5
    label "Zygmunt"
  ]
  node [
    id 6
    label "Hohenzollern"
  ]
  node [
    id 7
    label "Joachima"
  ]
  node [
    id 8
    label "ii"
  ]
  node [
    id 9
    label "i"
  ]
  node [
    id 10
    label "stary"
  ]
  node [
    id 11
    label "Jadwiga"
  ]
  node [
    id 12
    label "Jagiellonka"
  ]
  node [
    id 13
    label "Juliusz"
  ]
  node [
    id 14
    label "iii"
  ]
  node [
    id 15
    label "august"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 9
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 8
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 15
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 13
    target 14
  ]
]
