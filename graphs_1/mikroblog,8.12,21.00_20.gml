graph [
  maxDegree 10
  minDegree 1
  meanDegree 1.894736842105263
  density 0.10526315789473684
  graphCliqueNumber 2
  node [
    id 0
    label "dobra"
    origin "text"
  ]
  node [
    id 1
    label "snookerowy"
    origin "text"
  ]
  node [
    id 2
    label "&#347;wir"
    origin "text"
  ]
  node [
    id 3
    label "frymark"
  ]
  node [
    id 4
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 5
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 6
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 7
    label "commodity"
  ]
  node [
    id 8
    label "mienie"
  ]
  node [
    id 9
    label "Wilko"
  ]
  node [
    id 10
    label "jednostka_monetarna"
  ]
  node [
    id 11
    label "centym"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "&#347;wiergot"
  ]
  node [
    id 14
    label "odchylenie"
  ]
  node [
    id 15
    label "zakr&#281;t"
  ]
  node [
    id 16
    label "nienormalny"
  ]
  node [
    id 17
    label "orygina&#322;"
  ]
  node [
    id 18
    label "krejzol"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
]
