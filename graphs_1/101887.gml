graph [
  maxDegree 29
  minDegree 1
  meanDegree 2.129032258064516
  density 0.0062618595825426945
  graphCliqueNumber 3
  node [
    id 0
    label "kwestionariusz"
    origin "text"
  ]
  node [
    id 1
    label "sk&#322;ada&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "prosty"
    origin "text"
  ]
  node [
    id 4
    label "test"
    origin "text"
  ]
  node [
    id 5
    label "odpami&#281;tywania"
    origin "text"
  ]
  node [
    id 6
    label "dwa"
    origin "text"
  ]
  node [
    id 7
    label "pytanie"
    origin "text"
  ]
  node [
    id 8
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 9
    label "temat"
    origin "text"
  ]
  node [
    id 10
    label "problem"
    origin "text"
  ]
  node [
    id 11
    label "samodzielny"
    origin "text"
  ]
  node [
    id 12
    label "zapami&#281;tywa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "przyjmowa&#263;"
    origin "text"
  ]
  node [
    id 14
    label "lek"
    origin "text"
  ]
  node [
    id 15
    label "list"
    origin "text"
  ]
  node [
    id 16
    label "sprawa"
    origin "text"
  ]
  node [
    id 17
    label "za&#322;atwienie"
    origin "text"
  ]
  node [
    id 18
    label "doktor"
    origin "text"
  ]
  node [
    id 19
    label "howard"
    origin "text"
  ]
  node [
    id 20
    label "fillit"
    origin "text"
  ]
  node [
    id 21
    label "instytut"
    origin "text"
  ]
  node [
    id 22
    label "starzenie"
    origin "text"
  ]
  node [
    id 23
    label "nowa"
    origin "text"
  ]
  node [
    id 24
    label "jork"
    origin "text"
  ]
  node [
    id 25
    label "kierowa&#263;"
    origin "text"
  ]
  node [
    id 26
    label "praca"
    origin "text"
  ]
  node [
    id 27
    label "nad"
    origin "text"
  ]
  node [
    id 28
    label "budowa"
    origin "text"
  ]
  node [
    id 29
    label "przekonywa&#263;"
    origin "text"
  ]
  node [
    id 30
    label "wykonywa&#263;"
    origin "text"
  ]
  node [
    id 31
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 32
    label "r&#243;wnie&#380;"
    origin "text"
  ]
  node [
    id 33
    label "cz&#322;onek"
    origin "text"
  ]
  node [
    id 34
    label "tzw"
    origin "text"
  ]
  node [
    id 35
    label "pomocniczy"
    origin "text"
  ]
  node [
    id 36
    label "s&#322;u&#380;ba"
    origin "text"
  ]
  node [
    id 37
    label "medyczny"
    origin "text"
  ]
  node [
    id 38
    label "znacznie"
    origin "text"
  ]
  node [
    id 39
    label "obni&#380;a&#263;"
    origin "text"
  ]
  node [
    id 40
    label "koszt"
    origin "text"
  ]
  node [
    id 41
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 42
    label "kafeteria"
  ]
  node [
    id 43
    label "formularz"
  ]
  node [
    id 44
    label "render"
  ]
  node [
    id 45
    label "zmienia&#263;"
  ]
  node [
    id 46
    label "zestaw"
  ]
  node [
    id 47
    label "train"
  ]
  node [
    id 48
    label "uk&#322;ada&#263;"
  ]
  node [
    id 49
    label "dzieli&#263;"
  ]
  node [
    id 50
    label "set"
  ]
  node [
    id 51
    label "przywraca&#263;"
  ]
  node [
    id 52
    label "dawa&#263;"
  ]
  node [
    id 53
    label "przyk&#322;ada&#263;"
  ]
  node [
    id 54
    label "k&#322;a&#347;&#263;"
  ]
  node [
    id 55
    label "zbiera&#263;"
  ]
  node [
    id 56
    label "convey"
  ]
  node [
    id 57
    label "opracowywa&#263;"
  ]
  node [
    id 58
    label "oszcz&#281;dza&#263;"
  ]
  node [
    id 59
    label "publicize"
  ]
  node [
    id 60
    label "przekazywa&#263;"
  ]
  node [
    id 61
    label "&#322;&#261;czy&#263;"
  ]
  node [
    id 62
    label "scala&#263;"
  ]
  node [
    id 63
    label "oddawa&#263;"
  ]
  node [
    id 64
    label "&#322;atwy"
  ]
  node [
    id 65
    label "prostowanie_si&#281;"
  ]
  node [
    id 66
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 67
    label "rozprostowanie"
  ]
  node [
    id 68
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 69
    label "prostoduszny"
  ]
  node [
    id 70
    label "naturalny"
  ]
  node [
    id 71
    label "naiwny"
  ]
  node [
    id 72
    label "cios"
  ]
  node [
    id 73
    label "prostowanie"
  ]
  node [
    id 74
    label "niepozorny"
  ]
  node [
    id 75
    label "zwyk&#322;y"
  ]
  node [
    id 76
    label "prosto"
  ]
  node [
    id 77
    label "po_prostu"
  ]
  node [
    id 78
    label "skromny"
  ]
  node [
    id 79
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 80
    label "do&#347;wiadczenie"
  ]
  node [
    id 81
    label "arkusz"
  ]
  node [
    id 82
    label "sprawdzian"
  ]
  node [
    id 83
    label "quiz"
  ]
  node [
    id 84
    label "przechodzi&#263;"
  ]
  node [
    id 85
    label "przechodzenie"
  ]
  node [
    id 86
    label "badanie"
  ]
  node [
    id 87
    label "narz&#281;dzie"
  ]
  node [
    id 88
    label "sytuacja"
  ]
  node [
    id 89
    label "zadanie"
  ]
  node [
    id 90
    label "wypowied&#378;"
  ]
  node [
    id 91
    label "problemat"
  ]
  node [
    id 92
    label "rozpytywanie"
  ]
  node [
    id 93
    label "przes&#322;uchiwanie"
  ]
  node [
    id 94
    label "wypytanie"
  ]
  node [
    id 95
    label "zwracanie_si&#281;"
  ]
  node [
    id 96
    label "wypowiedzenie"
  ]
  node [
    id 97
    label "wywo&#322;ywanie"
  ]
  node [
    id 98
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 99
    label "problematyka"
  ]
  node [
    id 100
    label "question"
  ]
  node [
    id 101
    label "sprawdzanie"
  ]
  node [
    id 102
    label "odpowiadanie"
  ]
  node [
    id 103
    label "survey"
  ]
  node [
    id 104
    label "odpowiada&#263;"
  ]
  node [
    id 105
    label "egzaminowanie"
  ]
  node [
    id 106
    label "czyj&#347;"
  ]
  node [
    id 107
    label "m&#261;&#380;"
  ]
  node [
    id 108
    label "fraza"
  ]
  node [
    id 109
    label "forma"
  ]
  node [
    id 110
    label "melodia"
  ]
  node [
    id 111
    label "rzecz"
  ]
  node [
    id 112
    label "zbacza&#263;"
  ]
  node [
    id 113
    label "entity"
  ]
  node [
    id 114
    label "omawia&#263;"
  ]
  node [
    id 115
    label "topik"
  ]
  node [
    id 116
    label "wyraz_pochodny"
  ]
  node [
    id 117
    label "om&#243;wi&#263;"
  ]
  node [
    id 118
    label "omawianie"
  ]
  node [
    id 119
    label "w&#261;tek"
  ]
  node [
    id 120
    label "forum"
  ]
  node [
    id 121
    label "cecha"
  ]
  node [
    id 122
    label "zboczenie"
  ]
  node [
    id 123
    label "zbaczanie"
  ]
  node [
    id 124
    label "tre&#347;&#263;"
  ]
  node [
    id 125
    label "tematyka"
  ]
  node [
    id 126
    label "istota"
  ]
  node [
    id 127
    label "otoczka"
  ]
  node [
    id 128
    label "zboczy&#263;"
  ]
  node [
    id 129
    label "om&#243;wienie"
  ]
  node [
    id 130
    label "trudno&#347;&#263;"
  ]
  node [
    id 131
    label "ambaras"
  ]
  node [
    id 132
    label "pierepa&#322;ka"
  ]
  node [
    id 133
    label "obstruction"
  ]
  node [
    id 134
    label "jajko_Kolumba"
  ]
  node [
    id 135
    label "subiekcja"
  ]
  node [
    id 136
    label "sobieradzki"
  ]
  node [
    id 137
    label "autonomicznie"
  ]
  node [
    id 138
    label "osobny"
  ]
  node [
    id 139
    label "indywidualny"
  ]
  node [
    id 140
    label "niepodleg&#322;y"
  ]
  node [
    id 141
    label "sw&#243;j"
  ]
  node [
    id 142
    label "samodzielnie"
  ]
  node [
    id 143
    label "odr&#281;bny"
  ]
  node [
    id 144
    label "w&#322;asny"
  ]
  node [
    id 145
    label "zobowi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 146
    label "poch&#322;ania&#263;"
  ]
  node [
    id 147
    label "dostarcza&#263;"
  ]
  node [
    id 148
    label "umieszcza&#263;"
  ]
  node [
    id 149
    label "uznawa&#263;"
  ]
  node [
    id 150
    label "swallow"
  ]
  node [
    id 151
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 152
    label "admit"
  ]
  node [
    id 153
    label "fall"
  ]
  node [
    id 154
    label "undertake"
  ]
  node [
    id 155
    label "dopuszcza&#263;"
  ]
  node [
    id 156
    label "wyprawia&#263;"
  ]
  node [
    id 157
    label "robi&#263;"
  ]
  node [
    id 158
    label "wpuszcza&#263;"
  ]
  node [
    id 159
    label "close"
  ]
  node [
    id 160
    label "przyjmowanie"
  ]
  node [
    id 161
    label "obiera&#263;"
  ]
  node [
    id 162
    label "pracowa&#263;"
  ]
  node [
    id 163
    label "bra&#263;"
  ]
  node [
    id 164
    label "zgadza&#263;_si&#281;"
  ]
  node [
    id 165
    label "odbiera&#263;"
  ]
  node [
    id 166
    label "stawa&#263;_si&#281;"
  ]
  node [
    id 167
    label "naszprycowa&#263;"
  ]
  node [
    id 168
    label "Albania"
  ]
  node [
    id 169
    label "tonizowa&#263;"
  ]
  node [
    id 170
    label "medicine"
  ]
  node [
    id 171
    label "szprycowa&#263;"
  ]
  node [
    id 172
    label "przepisanie"
  ]
  node [
    id 173
    label "przepisa&#263;"
  ]
  node [
    id 174
    label "tonizowanie"
  ]
  node [
    id 175
    label "szprycowanie"
  ]
  node [
    id 176
    label "naszprycowanie"
  ]
  node [
    id 177
    label "jednostka_monetarna"
  ]
  node [
    id 178
    label "apteczka"
  ]
  node [
    id 179
    label "substancja"
  ]
  node [
    id 180
    label "spos&#243;b"
  ]
  node [
    id 181
    label "quindarka"
  ]
  node [
    id 182
    label "li&#347;&#263;"
  ]
  node [
    id 183
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 184
    label "poczta"
  ]
  node [
    id 185
    label "epistolografia"
  ]
  node [
    id 186
    label "przesy&#322;ka"
  ]
  node [
    id 187
    label "poczta_elektroniczna"
  ]
  node [
    id 188
    label "znaczek_pocztowy"
  ]
  node [
    id 189
    label "kognicja"
  ]
  node [
    id 190
    label "idea"
  ]
  node [
    id 191
    label "szczeg&#243;&#322;"
  ]
  node [
    id 192
    label "wydarzenie"
  ]
  node [
    id 193
    label "przes&#322;anka"
  ]
  node [
    id 194
    label "rozprawa"
  ]
  node [
    id 195
    label "object"
  ]
  node [
    id 196
    label "proposition"
  ]
  node [
    id 197
    label "skrzywdzenie"
  ]
  node [
    id 198
    label "spe&#322;nienie"
  ]
  node [
    id 199
    label "poza&#322;atwianie"
  ]
  node [
    id 200
    label "bargain"
  ]
  node [
    id 201
    label "return"
  ]
  node [
    id 202
    label "wyrz&#261;dzenie"
  ]
  node [
    id 203
    label "pozabijanie"
  ]
  node [
    id 204
    label "killing"
  ]
  node [
    id 205
    label "uzyskanie"
  ]
  node [
    id 206
    label "zabicie"
  ]
  node [
    id 207
    label "bycie_w_posiadaniu"
  ]
  node [
    id 208
    label "pozyskanie"
  ]
  node [
    id 209
    label "doktorant"
  ]
  node [
    id 210
    label "doktoryzowanie_si&#281;"
  ]
  node [
    id 211
    label "pracownik"
  ]
  node [
    id 212
    label "pracownik_naukowy"
  ]
  node [
    id 213
    label "stopie&#324;_naukowy"
  ]
  node [
    id 214
    label "plac&#243;wka"
  ]
  node [
    id 215
    label "institute"
  ]
  node [
    id 216
    label "&#379;ydowski_Instytut_Historyczny"
  ]
  node [
    id 217
    label "Ossolineum"
  ]
  node [
    id 218
    label "instytucja"
  ]
  node [
    id 219
    label "plac&#243;wka_naukowa"
  ]
  node [
    id 220
    label "gwiazda"
  ]
  node [
    id 221
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 222
    label "pies_do_towarzystwa"
  ]
  node [
    id 223
    label "terier"
  ]
  node [
    id 224
    label "pies_pokojowy"
  ]
  node [
    id 225
    label "pies_miniaturowy"
  ]
  node [
    id 226
    label "pies_ozdobny"
  ]
  node [
    id 227
    label "&#347;rodek_lokomocji"
  ]
  node [
    id 228
    label "control"
  ]
  node [
    id 229
    label "ustawia&#263;"
  ]
  node [
    id 230
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 231
    label "motywowa&#263;"
  ]
  node [
    id 232
    label "order"
  ]
  node [
    id 233
    label "administrowa&#263;"
  ]
  node [
    id 234
    label "manipulate"
  ]
  node [
    id 235
    label "give"
  ]
  node [
    id 236
    label "indicate"
  ]
  node [
    id 237
    label "przeznacza&#263;"
  ]
  node [
    id 238
    label "oddzia&#322;ywa&#263;"
  ]
  node [
    id 239
    label "match"
  ]
  node [
    id 240
    label "sterowa&#263;"
  ]
  node [
    id 241
    label "wysy&#322;a&#263;"
  ]
  node [
    id 242
    label "zwierzchnik"
  ]
  node [
    id 243
    label "zarz&#261;dza&#263;"
  ]
  node [
    id 244
    label "stosunek_pracy"
  ]
  node [
    id 245
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 246
    label "benedykty&#324;ski"
  ]
  node [
    id 247
    label "pracowanie"
  ]
  node [
    id 248
    label "zaw&#243;d"
  ]
  node [
    id 249
    label "kierownictwo"
  ]
  node [
    id 250
    label "zmiana"
  ]
  node [
    id 251
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 252
    label "wytw&#243;r"
  ]
  node [
    id 253
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 254
    label "tynkarski"
  ]
  node [
    id 255
    label "czynnik_produkcji"
  ]
  node [
    id 256
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 257
    label "zobowi&#261;zanie"
  ]
  node [
    id 258
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 259
    label "czynno&#347;&#263;"
  ]
  node [
    id 260
    label "tyrka"
  ]
  node [
    id 261
    label "siedziba"
  ]
  node [
    id 262
    label "poda&#380;_pracy"
  ]
  node [
    id 263
    label "miejsce"
  ]
  node [
    id 264
    label "zak&#322;ad"
  ]
  node [
    id 265
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 266
    label "najem"
  ]
  node [
    id 267
    label "figura"
  ]
  node [
    id 268
    label "wjazd"
  ]
  node [
    id 269
    label "struktura"
  ]
  node [
    id 270
    label "konstrukcja"
  ]
  node [
    id 271
    label "r&#243;w"
  ]
  node [
    id 272
    label "kreacja"
  ]
  node [
    id 273
    label "posesja"
  ]
  node [
    id 274
    label "dokumentacja_powykonawcza"
  ]
  node [
    id 275
    label "organ"
  ]
  node [
    id 276
    label "mechanika"
  ]
  node [
    id 277
    label "zwierz&#281;"
  ]
  node [
    id 278
    label "miejsce_pracy"
  ]
  node [
    id 279
    label "constitution"
  ]
  node [
    id 280
    label "argue"
  ]
  node [
    id 281
    label "nak&#322;ania&#263;"
  ]
  node [
    id 282
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 283
    label "work"
  ]
  node [
    id 284
    label "muzyka"
  ]
  node [
    id 285
    label "rola"
  ]
  node [
    id 286
    label "create"
  ]
  node [
    id 287
    label "wytwarza&#263;"
  ]
  node [
    id 288
    label "by&#263;"
  ]
  node [
    id 289
    label "uprawi&#263;"
  ]
  node [
    id 290
    label "gotowy"
  ]
  node [
    id 291
    label "might"
  ]
  node [
    id 292
    label "cz&#322;owiek"
  ]
  node [
    id 293
    label "cia&#322;o"
  ]
  node [
    id 294
    label "organizacja"
  ]
  node [
    id 295
    label "przedstawiciel"
  ]
  node [
    id 296
    label "shaft"
  ]
  node [
    id 297
    label "podmiot"
  ]
  node [
    id 298
    label "fiut"
  ]
  node [
    id 299
    label "przyrodzenie"
  ]
  node [
    id 300
    label "wchodzenie"
  ]
  node [
    id 301
    label "grupa"
  ]
  node [
    id 302
    label "ptaszek"
  ]
  node [
    id 303
    label "wej&#347;cie"
  ]
  node [
    id 304
    label "narz&#261;d_kopulacyjny"
  ]
  node [
    id 305
    label "element_anatomiczny"
  ]
  node [
    id 306
    label "pomocniczo"
  ]
  node [
    id 307
    label "dodatkowy"
  ]
  node [
    id 308
    label "service"
  ]
  node [
    id 309
    label "ZOMO"
  ]
  node [
    id 310
    label "czworak"
  ]
  node [
    id 311
    label "zesp&#243;&#322;"
  ]
  node [
    id 312
    label "s&#322;u&#380;&#261;ca"
  ]
  node [
    id 313
    label "s&#322;u&#380;&#261;cy"
  ]
  node [
    id 314
    label "wys&#322;uga"
  ]
  node [
    id 315
    label "specjalny"
  ]
  node [
    id 316
    label "praktyczny"
  ]
  node [
    id 317
    label "paramedyczny"
  ]
  node [
    id 318
    label "zgodny"
  ]
  node [
    id 319
    label "profilowy"
  ]
  node [
    id 320
    label "leczniczy"
  ]
  node [
    id 321
    label "lekarsko"
  ]
  node [
    id 322
    label "bia&#322;y"
  ]
  node [
    id 323
    label "medycznie"
  ]
  node [
    id 324
    label "specjalistyczny"
  ]
  node [
    id 325
    label "znaczny"
  ]
  node [
    id 326
    label "zauwa&#380;alnie"
  ]
  node [
    id 327
    label "brzmie&#263;"
  ]
  node [
    id 328
    label "write_out"
  ]
  node [
    id 329
    label "bate"
  ]
  node [
    id 330
    label "zmniejsza&#263;"
  ]
  node [
    id 331
    label "nak&#322;ad"
  ]
  node [
    id 332
    label "rachunek_koszt&#243;w"
  ]
  node [
    id 333
    label "sumpt"
  ]
  node [
    id 334
    label "wydatek"
  ]
  node [
    id 335
    label "u&#380;ywa&#263;"
  ]
  node [
    id 336
    label "Howard"
  ]
  node [
    id 337
    label "Fillit"
  ]
  node [
    id 338
    label "starzy&#263;"
  ]
  node [
    id 339
    label "nowy"
  ]
  node [
    id 340
    label "Jork"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 1
    target 61
  ]
  edge [
    source 1
    target 62
  ]
  edge [
    source 1
    target 63
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 3
    target 75
  ]
  edge [
    source 3
    target 76
  ]
  edge [
    source 3
    target 77
  ]
  edge [
    source 3
    target 78
  ]
  edge [
    source 3
    target 79
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 89
  ]
  edge [
    source 7
    target 90
  ]
  edge [
    source 7
    target 91
  ]
  edge [
    source 7
    target 92
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 93
  ]
  edge [
    source 7
    target 94
  ]
  edge [
    source 7
    target 95
  ]
  edge [
    source 7
    target 96
  ]
  edge [
    source 7
    target 97
  ]
  edge [
    source 7
    target 98
  ]
  edge [
    source 7
    target 99
  ]
  edge [
    source 7
    target 100
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 106
  ]
  edge [
    source 8
    target 107
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 108
  ]
  edge [
    source 9
    target 109
  ]
  edge [
    source 9
    target 110
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 9
    target 112
  ]
  edge [
    source 9
    target 113
  ]
  edge [
    source 9
    target 114
  ]
  edge [
    source 9
    target 115
  ]
  edge [
    source 9
    target 116
  ]
  edge [
    source 9
    target 117
  ]
  edge [
    source 9
    target 118
  ]
  edge [
    source 9
    target 119
  ]
  edge [
    source 9
    target 120
  ]
  edge [
    source 9
    target 121
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 16
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 130
  ]
  edge [
    source 10
    target 16
  ]
  edge [
    source 10
    target 131
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 132
  ]
  edge [
    source 10
    target 133
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 134
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 11
    target 140
  ]
  edge [
    source 11
    target 141
  ]
  edge [
    source 11
    target 142
  ]
  edge [
    source 11
    target 143
  ]
  edge [
    source 11
    target 144
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 167
  ]
  edge [
    source 14
    target 168
  ]
  edge [
    source 14
    target 169
  ]
  edge [
    source 14
    target 170
  ]
  edge [
    source 14
    target 171
  ]
  edge [
    source 14
    target 172
  ]
  edge [
    source 14
    target 173
  ]
  edge [
    source 14
    target 174
  ]
  edge [
    source 14
    target 175
  ]
  edge [
    source 14
    target 176
  ]
  edge [
    source 14
    target 177
  ]
  edge [
    source 14
    target 178
  ]
  edge [
    source 14
    target 179
  ]
  edge [
    source 14
    target 180
  ]
  edge [
    source 14
    target 181
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 182
  ]
  edge [
    source 15
    target 183
  ]
  edge [
    source 15
    target 184
  ]
  edge [
    source 15
    target 185
  ]
  edge [
    source 15
    target 186
  ]
  edge [
    source 15
    target 187
  ]
  edge [
    source 15
    target 188
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 189
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 16
    target 192
  ]
  edge [
    source 16
    target 193
  ]
  edge [
    source 16
    target 194
  ]
  edge [
    source 16
    target 195
  ]
  edge [
    source 16
    target 196
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 197
  ]
  edge [
    source 17
    target 198
  ]
  edge [
    source 17
    target 199
  ]
  edge [
    source 17
    target 200
  ]
  edge [
    source 17
    target 201
  ]
  edge [
    source 17
    target 202
  ]
  edge [
    source 17
    target 203
  ]
  edge [
    source 17
    target 204
  ]
  edge [
    source 17
    target 205
  ]
  edge [
    source 17
    target 206
  ]
  edge [
    source 17
    target 207
  ]
  edge [
    source 17
    target 50
  ]
  edge [
    source 17
    target 208
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 209
  ]
  edge [
    source 18
    target 210
  ]
  edge [
    source 18
    target 211
  ]
  edge [
    source 18
    target 212
  ]
  edge [
    source 18
    target 213
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 214
  ]
  edge [
    source 21
    target 215
  ]
  edge [
    source 21
    target 216
  ]
  edge [
    source 21
    target 217
  ]
  edge [
    source 21
    target 218
  ]
  edge [
    source 21
    target 219
  ]
  edge [
    source 21
    target 338
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 221
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 222
  ]
  edge [
    source 24
    target 223
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 24
    target 225
  ]
  edge [
    source 24
    target 226
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 227
  ]
  edge [
    source 25
    target 228
  ]
  edge [
    source 25
    target 229
  ]
  edge [
    source 25
    target 230
  ]
  edge [
    source 25
    target 231
  ]
  edge [
    source 25
    target 232
  ]
  edge [
    source 25
    target 233
  ]
  edge [
    source 25
    target 234
  ]
  edge [
    source 25
    target 235
  ]
  edge [
    source 25
    target 236
  ]
  edge [
    source 25
    target 237
  ]
  edge [
    source 25
    target 238
  ]
  edge [
    source 25
    target 239
  ]
  edge [
    source 25
    target 240
  ]
  edge [
    source 25
    target 241
  ]
  edge [
    source 25
    target 242
  ]
  edge [
    source 25
    target 243
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 244
  ]
  edge [
    source 26
    target 245
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 26
    target 248
  ]
  edge [
    source 26
    target 249
  ]
  edge [
    source 26
    target 250
  ]
  edge [
    source 26
    target 251
  ]
  edge [
    source 26
    target 252
  ]
  edge [
    source 26
    target 253
  ]
  edge [
    source 26
    target 254
  ]
  edge [
    source 26
    target 255
  ]
  edge [
    source 26
    target 256
  ]
  edge [
    source 26
    target 257
  ]
  edge [
    source 26
    target 258
  ]
  edge [
    source 26
    target 259
  ]
  edge [
    source 26
    target 260
  ]
  edge [
    source 26
    target 162
  ]
  edge [
    source 26
    target 261
  ]
  edge [
    source 26
    target 262
  ]
  edge [
    source 26
    target 263
  ]
  edge [
    source 26
    target 264
  ]
  edge [
    source 26
    target 265
  ]
  edge [
    source 26
    target 266
  ]
  edge [
    source 26
    target 28
  ]
  edge [
    source 26
    target 30
  ]
  edge [
    source 26
    target 36
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 28
    target 267
  ]
  edge [
    source 28
    target 268
  ]
  edge [
    source 28
    target 269
  ]
  edge [
    source 28
    target 270
  ]
  edge [
    source 28
    target 271
  ]
  edge [
    source 28
    target 272
  ]
  edge [
    source 28
    target 273
  ]
  edge [
    source 28
    target 121
  ]
  edge [
    source 28
    target 274
  ]
  edge [
    source 28
    target 275
  ]
  edge [
    source 28
    target 276
  ]
  edge [
    source 28
    target 277
  ]
  edge [
    source 28
    target 278
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 29
    target 280
  ]
  edge [
    source 29
    target 238
  ]
  edge [
    source 29
    target 281
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 282
  ]
  edge [
    source 30
    target 283
  ]
  edge [
    source 30
    target 157
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 30
    target 285
  ]
  edge [
    source 30
    target 286
  ]
  edge [
    source 30
    target 287
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 288
  ]
  edge [
    source 31
    target 289
  ]
  edge [
    source 31
    target 290
  ]
  edge [
    source 31
    target 291
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 292
  ]
  edge [
    source 33
    target 293
  ]
  edge [
    source 33
    target 294
  ]
  edge [
    source 33
    target 295
  ]
  edge [
    source 33
    target 296
  ]
  edge [
    source 33
    target 297
  ]
  edge [
    source 33
    target 298
  ]
  edge [
    source 33
    target 299
  ]
  edge [
    source 33
    target 300
  ]
  edge [
    source 33
    target 301
  ]
  edge [
    source 33
    target 302
  ]
  edge [
    source 33
    target 275
  ]
  edge [
    source 33
    target 303
  ]
  edge [
    source 33
    target 304
  ]
  edge [
    source 33
    target 305
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 306
  ]
  edge [
    source 35
    target 307
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 308
  ]
  edge [
    source 36
    target 309
  ]
  edge [
    source 36
    target 310
  ]
  edge [
    source 36
    target 311
  ]
  edge [
    source 36
    target 312
  ]
  edge [
    source 36
    target 218
  ]
  edge [
    source 36
    target 313
  ]
  edge [
    source 36
    target 314
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 315
  ]
  edge [
    source 37
    target 316
  ]
  edge [
    source 37
    target 317
  ]
  edge [
    source 37
    target 318
  ]
  edge [
    source 37
    target 319
  ]
  edge [
    source 37
    target 320
  ]
  edge [
    source 37
    target 321
  ]
  edge [
    source 37
    target 322
  ]
  edge [
    source 37
    target 323
  ]
  edge [
    source 37
    target 324
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 325
  ]
  edge [
    source 38
    target 326
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 45
  ]
  edge [
    source 39
    target 327
  ]
  edge [
    source 39
    target 328
  ]
  edge [
    source 39
    target 329
  ]
  edge [
    source 39
    target 330
  ]
  edge [
    source 39
    target 153
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 331
  ]
  edge [
    source 40
    target 332
  ]
  edge [
    source 40
    target 333
  ]
  edge [
    source 40
    target 334
  ]
  edge [
    source 41
    target 335
  ]
  edge [
    source 336
    target 337
  ]
  edge [
    source 339
    target 340
  ]
]
