graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.92
  density 0.08
  graphCliqueNumber 2
  node [
    id 0
    label "brawo"
    origin "text"
  ]
  node [
    id 1
    label "doktor"
    origin "text"
  ]
  node [
    id 2
    label "religa"
    origin "text"
  ]
  node [
    id 3
    label "by&#263;by"
    origin "text"
  ]
  node [
    id 4
    label "dumny"
    origin "text"
  ]
  node [
    id 5
    label "kolega"
    origin "text"
  ]
  node [
    id 6
    label "acclaim"
  ]
  node [
    id 7
    label "aprobata"
  ]
  node [
    id 8
    label "doktorant"
  ]
  node [
    id 9
    label "doktoryzowanie_si&#281;"
  ]
  node [
    id 10
    label "pracownik"
  ]
  node [
    id 11
    label "pracownik_naukowy"
  ]
  node [
    id 12
    label "stopie&#324;_naukowy"
  ]
  node [
    id 13
    label "godny"
  ]
  node [
    id 14
    label "zadowolony"
  ]
  node [
    id 15
    label "dumnie"
  ]
  node [
    id 16
    label "pewny"
  ]
  node [
    id 17
    label "dostojny"
  ]
  node [
    id 18
    label "kumplowanie_si&#281;"
  ]
  node [
    id 19
    label "znajomy"
  ]
  node [
    id 20
    label "konfrater"
  ]
  node [
    id 21
    label "towarzysz"
  ]
  node [
    id 22
    label "zakolegowanie_si&#281;"
  ]
  node [
    id 23
    label "partner"
  ]
  node [
    id 24
    label "ziom"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
]
