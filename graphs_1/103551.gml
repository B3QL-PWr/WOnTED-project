graph [
  maxDegree 28
  minDegree 1
  meanDegree 1.9310344827586208
  density 0.06896551724137931
  graphCliqueNumber 2
  node [
    id 0
    label "scena"
    origin "text"
  ]
  node [
    id 1
    label "proscenium"
  ]
  node [
    id 2
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 3
    label "Teatr_&#321;a&#378;nia_Nowa"
  ]
  node [
    id 4
    label "stadium"
  ]
  node [
    id 5
    label "antyteatr"
  ]
  node [
    id 6
    label "sztuka"
  ]
  node [
    id 7
    label "fragment"
  ]
  node [
    id 8
    label "sznurownia"
  ]
  node [
    id 9
    label "widzownia"
  ]
  node [
    id 10
    label "kiesze&#324;"
  ]
  node [
    id 11
    label "teren"
  ]
  node [
    id 12
    label "horyzont"
  ]
  node [
    id 13
    label "podest"
  ]
  node [
    id 14
    label "przedstawia&#263;"
  ]
  node [
    id 15
    label "podwy&#380;szenie"
  ]
  node [
    id 16
    label "budka_suflera"
  ]
  node [
    id 17
    label "akt"
  ]
  node [
    id 18
    label "przedstawianie"
  ]
  node [
    id 19
    label "wydarzenie"
  ]
  node [
    id 20
    label "kurtyna"
  ]
  node [
    id 21
    label "epizod"
  ]
  node [
    id 22
    label "sphere"
  ]
  node [
    id 23
    label "przedstawienie"
  ]
  node [
    id 24
    label "nadscenie"
  ]
  node [
    id 25
    label "film"
  ]
  node [
    id 26
    label "k&#322;&#243;tnia"
  ]
  node [
    id 27
    label "instytucja"
  ]
  node [
    id 28
    label "dramaturgy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
]
