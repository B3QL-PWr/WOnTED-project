graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9655172413793103
  density 0.034482758620689655
  graphCliqueNumber 2
  node [
    id 0
    label "jaki"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;c"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 4
    label "letni"
    origin "text"
  ]
  node [
    id 5
    label "facet"
    origin "text"
  ]
  node [
    id 6
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 7
    label "prawa"
    origin "text"
  ]
  node [
    id 8
    label "jazda"
    origin "text"
  ]
  node [
    id 9
    label "uprawi&#263;"
  ]
  node [
    id 10
    label "gotowy"
  ]
  node [
    id 11
    label "might"
  ]
  node [
    id 12
    label "si&#281;ga&#263;"
  ]
  node [
    id 13
    label "trwa&#263;"
  ]
  node [
    id 14
    label "obecno&#347;&#263;"
  ]
  node [
    id 15
    label "stan"
  ]
  node [
    id 16
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "stand"
  ]
  node [
    id 18
    label "mie&#263;_miejsce"
  ]
  node [
    id 19
    label "uczestniczy&#263;"
  ]
  node [
    id 20
    label "chodzi&#263;"
  ]
  node [
    id 21
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 22
    label "equal"
  ]
  node [
    id 23
    label "strona"
  ]
  node [
    id 24
    label "przyczyna"
  ]
  node [
    id 25
    label "matuszka"
  ]
  node [
    id 26
    label "geneza"
  ]
  node [
    id 27
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 28
    label "czynnik"
  ]
  node [
    id 29
    label "poci&#261;ganie"
  ]
  node [
    id 30
    label "rezultat"
  ]
  node [
    id 31
    label "uprz&#261;&#380;"
  ]
  node [
    id 32
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 33
    label "subject"
  ]
  node [
    id 34
    label "nijaki"
  ]
  node [
    id 35
    label "sezonowy"
  ]
  node [
    id 36
    label "letnio"
  ]
  node [
    id 37
    label "s&#322;oneczny"
  ]
  node [
    id 38
    label "weso&#322;y"
  ]
  node [
    id 39
    label "oboj&#281;tny"
  ]
  node [
    id 40
    label "latowy"
  ]
  node [
    id 41
    label "ciep&#322;y"
  ]
  node [
    id 42
    label "typowy"
  ]
  node [
    id 43
    label "cz&#322;owiek"
  ]
  node [
    id 44
    label "bratek"
  ]
  node [
    id 45
    label "czyj&#347;"
  ]
  node [
    id 46
    label "m&#261;&#380;"
  ]
  node [
    id 47
    label "sport"
  ]
  node [
    id 48
    label "szwadron"
  ]
  node [
    id 49
    label "formacja"
  ]
  node [
    id 50
    label "chor&#261;giew"
  ]
  node [
    id 51
    label "ruch"
  ]
  node [
    id 52
    label "wykrzyknik"
  ]
  node [
    id 53
    label "heca"
  ]
  node [
    id 54
    label "journey"
  ]
  node [
    id 55
    label "cavalry"
  ]
  node [
    id 56
    label "szale&#324;stwo"
  ]
  node [
    id 57
    label "awantura"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 47
  ]
  edge [
    source 8
    target 48
  ]
  edge [
    source 8
    target 49
  ]
  edge [
    source 8
    target 50
  ]
  edge [
    source 8
    target 51
  ]
  edge [
    source 8
    target 52
  ]
  edge [
    source 8
    target 53
  ]
  edge [
    source 8
    target 54
  ]
  edge [
    source 8
    target 55
  ]
  edge [
    source 8
    target 56
  ]
  edge [
    source 8
    target 57
  ]
]
