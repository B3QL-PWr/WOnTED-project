graph [
  maxDegree 3
  minDegree 1
  meanDegree 1.5
  density 0.13636363636363635
  graphCliqueNumber 3
  node [
    id 0
    label "robin"
    origin "text"
  ]
  node [
    id 1
    label "haase"
    origin "text"
  ]
  node [
    id 2
    label "Robin"
  ]
  node [
    id 3
    label "Haase"
  ]
  node [
    id 4
    label "puchar"
  ]
  node [
    id 5
    label "Davis"
  ]
  node [
    id 6
    label "juan"
  ]
  node [
    id 7
    label "Pablo"
  ]
  node [
    id 8
    label "Brzezicki"
  ]
  node [
    id 9
    label "Guzman"
  ]
  node [
    id 10
    label "Rogierem"
  ]
  node [
    id 11
    label "Wassenem"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 10
    target 11
  ]
]
