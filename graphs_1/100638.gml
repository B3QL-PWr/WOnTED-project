graph [
  maxDegree 28
  minDegree 1
  meanDegree 2
  density 0.05405405405405406
  graphCliqueNumber 3
  node [
    id 0
    label "koran"
    origin "text"
  ]
  node [
    id 1
    label "jasno&#347;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "poranek"
    origin "text"
  ]
  node [
    id 3
    label "&#347;wieci&#263;"
  ]
  node [
    id 4
    label "klarowno&#347;&#263;"
  ]
  node [
    id 5
    label "wpa&#347;&#263;"
  ]
  node [
    id 6
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 7
    label "ton"
  ]
  node [
    id 8
    label "promieniowanie_optyczne"
  ]
  node [
    id 9
    label "przy&#263;mienie"
  ]
  node [
    id 10
    label "wpada&#263;"
  ]
  node [
    id 11
    label "light"
  ]
  node [
    id 12
    label "zjawisko"
  ]
  node [
    id 13
    label "przeb&#322;yskiwanie"
  ]
  node [
    id 14
    label "przy&#263;miewa&#263;"
  ]
  node [
    id 15
    label "polish"
  ]
  node [
    id 16
    label "przy&#263;miewanie"
  ]
  node [
    id 17
    label "fotokataliza"
  ]
  node [
    id 18
    label "ja&#347;nia"
  ]
  node [
    id 19
    label "jednoznaczno&#347;&#263;"
  ]
  node [
    id 20
    label "wpadni&#281;cie"
  ]
  node [
    id 21
    label "&#347;wiecenie"
  ]
  node [
    id 22
    label "pogodno&#347;&#263;"
  ]
  node [
    id 23
    label "wpadanie"
  ]
  node [
    id 24
    label "promie&#324;"
  ]
  node [
    id 25
    label "przy&#263;mi&#263;"
  ]
  node [
    id 26
    label "kontrast"
  ]
  node [
    id 27
    label "przeb&#322;yskiwa&#263;"
  ]
  node [
    id 28
    label "podkurek"
  ]
  node [
    id 29
    label "blady_&#347;wit"
  ]
  node [
    id 30
    label "dzie&#324;"
  ]
  node [
    id 31
    label "Koran"
  ]
  node [
    id 32
    label "Surah"
  ]
  node [
    id 33
    label "Ada"
  ]
  node [
    id 34
    label "Duh&#226;"
  ]
  node [
    id 35
    label "s&#322;o&#324;ce"
  ]
  node [
    id 36
    label "na"
  ]
  node [
    id 37
    label "wsch&#243;d"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 34
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 37
  ]
  edge [
    source 36
    target 37
  ]
]
