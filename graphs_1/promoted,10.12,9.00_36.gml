graph [
  maxDegree 64
  minDegree 1
  meanDegree 2.027586206896552
  density 0.014080459770114942
  graphCliqueNumber 3
  node [
    id 0
    label "karetka"
    origin "text"
  ]
  node [
    id 1
    label "wie&#378;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "krak&#243;w"
    origin "text"
  ]
  node [
    id 3
    label "serce"
    origin "text"
  ]
  node [
    id 4
    label "przeszczep"
    origin "text"
  ]
  node [
    id 5
    label "zobaczy&#263;"
    origin "text"
  ]
  node [
    id 6
    label "robil"
    origin "text"
  ]
  node [
    id 7
    label "czas"
    origin "text"
  ]
  node [
    id 8
    label "kierowca"
    origin "text"
  ]
  node [
    id 9
    label "bus"
    origin "text"
  ]
  node [
    id 10
    label "samoch&#243;d"
  ]
  node [
    id 11
    label "transportowa&#263;"
  ]
  node [
    id 12
    label "carry"
  ]
  node [
    id 13
    label "cz&#322;owiek"
  ]
  node [
    id 14
    label "courage"
  ]
  node [
    id 15
    label "mi&#281;sie&#324;"
  ]
  node [
    id 16
    label "kompleks"
  ]
  node [
    id 17
    label "sfera_afektywna"
  ]
  node [
    id 18
    label "heart"
  ]
  node [
    id 19
    label "sumienie"
  ]
  node [
    id 20
    label "przedsionek"
  ]
  node [
    id 21
    label "entity"
  ]
  node [
    id 22
    label "nastawienie"
  ]
  node [
    id 23
    label "punkt"
  ]
  node [
    id 24
    label "kompleksja"
  ]
  node [
    id 25
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 26
    label "kier"
  ]
  node [
    id 27
    label "systol"
  ]
  node [
    id 28
    label "pikawa"
  ]
  node [
    id 29
    label "power"
  ]
  node [
    id 30
    label "zastawka"
  ]
  node [
    id 31
    label "psychika"
  ]
  node [
    id 32
    label "wola"
  ]
  node [
    id 33
    label "komora"
  ]
  node [
    id 34
    label "struna_&#347;ci&#281;gnista"
  ]
  node [
    id 35
    label "zapalno&#347;&#263;"
  ]
  node [
    id 36
    label "wsierdzie"
  ]
  node [
    id 37
    label "mi&#281;sie&#324;_sercowy"
  ]
  node [
    id 38
    label "podekscytowanie"
  ]
  node [
    id 39
    label "przed&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 40
    label "strunowiec"
  ]
  node [
    id 41
    label "favor"
  ]
  node [
    id 42
    label "dusza"
  ]
  node [
    id 43
    label "fizjonomia"
  ]
  node [
    id 44
    label "przegroda_mi&#281;dzykomorowa"
  ]
  node [
    id 45
    label "uk&#322;ad_przedsionkowo-komorowy"
  ]
  node [
    id 46
    label "nie&#347;wiadomo&#347;&#263;"
  ]
  node [
    id 47
    label "charakter"
  ]
  node [
    id 48
    label "mikrokosmos"
  ]
  node [
    id 49
    label "dzwon"
  ]
  node [
    id 50
    label "koniuszek_serca"
  ]
  node [
    id 51
    label "pozytywno&#347;&#263;"
  ]
  node [
    id 52
    label "passion"
  ]
  node [
    id 53
    label "cecha"
  ]
  node [
    id 54
    label "pulsowa&#263;"
  ]
  node [
    id 55
    label "ego"
  ]
  node [
    id 56
    label "organ"
  ]
  node [
    id 57
    label "m&#243;zg_emocjonalny"
  ]
  node [
    id 58
    label "w&#281;ze&#322;_zatokowo-przedsionkowy"
  ]
  node [
    id 59
    label "kardiografia"
  ]
  node [
    id 60
    label "osobowo&#347;&#263;"
  ]
  node [
    id 61
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 62
    label "kszta&#322;t"
  ]
  node [
    id 63
    label "karta"
  ]
  node [
    id 64
    label "zatoka_wie&#324;cowa"
  ]
  node [
    id 65
    label "dobro&#263;"
  ]
  node [
    id 66
    label "podroby"
  ]
  node [
    id 67
    label "mi&#281;sie&#324;_brodawkowaty"
  ]
  node [
    id 68
    label "pulsowanie"
  ]
  node [
    id 69
    label "deformowa&#263;"
  ]
  node [
    id 70
    label "&#347;r&#243;dsierdzie"
  ]
  node [
    id 71
    label "seksualno&#347;&#263;"
  ]
  node [
    id 72
    label "deformowanie"
  ]
  node [
    id 73
    label "uk&#322;ad_krwiono&#347;ny"
  ]
  node [
    id 74
    label "elektrokardiografia"
  ]
  node [
    id 75
    label "przyj&#261;&#263;_si&#281;"
  ]
  node [
    id 76
    label "dawca"
  ]
  node [
    id 77
    label "biorca"
  ]
  node [
    id 78
    label "tkanka"
  ]
  node [
    id 79
    label "przyjmowa&#263;_si&#281;"
  ]
  node [
    id 80
    label "przyjmowanie_si&#281;"
  ]
  node [
    id 81
    label "graft"
  ]
  node [
    id 82
    label "operacja"
  ]
  node [
    id 83
    label "spotka&#263;"
  ]
  node [
    id 84
    label "wyobrazi&#263;_sobie"
  ]
  node [
    id 85
    label "peek"
  ]
  node [
    id 86
    label "dowiedzie&#263;_si&#281;"
  ]
  node [
    id 87
    label "spoziera&#263;"
  ]
  node [
    id 88
    label "obejrze&#263;"
  ]
  node [
    id 89
    label "sp&#243;jrze&#263;"
  ]
  node [
    id 90
    label "postrzec"
  ]
  node [
    id 91
    label "spot"
  ]
  node [
    id 92
    label "go_steady"
  ]
  node [
    id 93
    label "pojrze&#263;"
  ]
  node [
    id 94
    label "popatrze&#263;"
  ]
  node [
    id 95
    label "u&#347;wiadomi&#263;_sobie"
  ]
  node [
    id 96
    label "pogl&#261;dn&#261;&#263;"
  ]
  node [
    id 97
    label "see"
  ]
  node [
    id 98
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 99
    label "dostrzec"
  ]
  node [
    id 100
    label "zinterpretowa&#263;"
  ]
  node [
    id 101
    label "przekona&#263;_si&#281;"
  ]
  node [
    id 102
    label "znale&#378;&#263;"
  ]
  node [
    id 103
    label "cognizance"
  ]
  node [
    id 104
    label "czasokres"
  ]
  node [
    id 105
    label "trawienie"
  ]
  node [
    id 106
    label "kategoria_gramatyczna"
  ]
  node [
    id 107
    label "period"
  ]
  node [
    id 108
    label "odczyt"
  ]
  node [
    id 109
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 110
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 111
    label "chwila"
  ]
  node [
    id 112
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 113
    label "poprzedzenie"
  ]
  node [
    id 114
    label "koniugacja"
  ]
  node [
    id 115
    label "dzieje"
  ]
  node [
    id 116
    label "poprzedzi&#263;"
  ]
  node [
    id 117
    label "przep&#322;ywanie"
  ]
  node [
    id 118
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 119
    label "odwlekanie_si&#281;"
  ]
  node [
    id 120
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 121
    label "Zeitgeist"
  ]
  node [
    id 122
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 123
    label "okres_czasu"
  ]
  node [
    id 124
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 125
    label "pochodzi&#263;"
  ]
  node [
    id 126
    label "schy&#322;ek"
  ]
  node [
    id 127
    label "czwarty_wymiar"
  ]
  node [
    id 128
    label "chronometria"
  ]
  node [
    id 129
    label "poprzedzanie"
  ]
  node [
    id 130
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 131
    label "pogoda"
  ]
  node [
    id 132
    label "zegar"
  ]
  node [
    id 133
    label "trawi&#263;"
  ]
  node [
    id 134
    label "pochodzenie"
  ]
  node [
    id 135
    label "poprzedza&#263;"
  ]
  node [
    id 136
    label "time_period"
  ]
  node [
    id 137
    label "rachuba_czasu"
  ]
  node [
    id 138
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 139
    label "czasoprzestrze&#324;"
  ]
  node [
    id 140
    label "laba"
  ]
  node [
    id 141
    label "transportowiec"
  ]
  node [
    id 142
    label "autobus"
  ]
  node [
    id 143
    label "buspas"
  ]
  node [
    id 144
    label "znak_informacyjny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 3
    target 69
  ]
  edge [
    source 3
    target 70
  ]
  edge [
    source 3
    target 71
  ]
  edge [
    source 3
    target 72
  ]
  edge [
    source 3
    target 73
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 83
  ]
  edge [
    source 5
    target 84
  ]
  edge [
    source 5
    target 85
  ]
  edge [
    source 5
    target 86
  ]
  edge [
    source 5
    target 87
  ]
  edge [
    source 5
    target 88
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 7
    target 124
  ]
  edge [
    source 7
    target 125
  ]
  edge [
    source 7
    target 126
  ]
  edge [
    source 7
    target 127
  ]
  edge [
    source 7
    target 128
  ]
  edge [
    source 7
    target 129
  ]
  edge [
    source 7
    target 130
  ]
  edge [
    source 7
    target 131
  ]
  edge [
    source 7
    target 132
  ]
  edge [
    source 7
    target 133
  ]
  edge [
    source 7
    target 134
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 7
    target 136
  ]
  edge [
    source 7
    target 137
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 144
  ]
]
