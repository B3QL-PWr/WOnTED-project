graph [
  maxDegree 5
  minDegree 1
  meanDegree 2.2
  density 0.24444444444444444
  graphCliqueNumber 4
  node [
    id 0
    label "wita&#263;"
    origin "text"
  ]
  node [
    id 1
    label "pozdrawia&#263;"
  ]
  node [
    id 2
    label "&#347;wi&#281;towa&#263;"
  ]
  node [
    id 3
    label "greet"
  ]
  node [
    id 4
    label "uzewn&#281;trznia&#263;_si&#281;"
  ]
  node [
    id 5
    label "welcome"
  ]
  node [
    id 6
    label "m&#243;j"
  ]
  node [
    id 7
    label "tata"
  ]
  node [
    id 8
    label "lvl"
  ]
  node [
    id 9
    label "70"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 8
  ]
  edge [
    source 6
    target 9
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
]
