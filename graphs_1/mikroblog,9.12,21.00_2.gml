graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.8181818181818181
  density 0.18181818181818182
  graphCliqueNumber 2
  node [
    id 0
    label "geniusz"
    origin "text"
  ]
  node [
    id 1
    label "zbrodnia"
    origin "text"
  ]
  node [
    id 2
    label "zlodzieje"
    origin "text"
  ]
  node [
    id 3
    label "doskona&#322;o&#347;&#263;"
  ]
  node [
    id 4
    label "nadcz&#322;owiek"
  ]
  node [
    id 5
    label "duch_opieku&#324;czy"
  ]
  node [
    id 6
    label "m&#243;zg"
  ]
  node [
    id 7
    label "talent"
  ]
  node [
    id 8
    label "crime"
  ]
  node [
    id 9
    label "przest&#281;pstwo"
  ]
  node [
    id 10
    label "post&#281;pek"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
]
