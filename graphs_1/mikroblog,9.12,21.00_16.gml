graph [
  maxDegree 25
  minDegree 1
  meanDegree 1.9545454545454546
  density 0.045454545454545456
  graphCliqueNumber 2
  node [
    id 0
    label "tera&#263;"
    origin "text"
  ]
  node [
    id 1
    label "ko&#347;ci&#243;&#322;"
    origin "text"
  ]
  node [
    id 2
    label "p&#243;j&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "hehe"
    origin "text"
  ]
  node [
    id 4
    label "heheszki"
    origin "text"
  ]
  node [
    id 5
    label "gownowpis"
    origin "text"
  ]
  node [
    id 6
    label "marnowa&#263;"
  ]
  node [
    id 7
    label "&#347;wi&#261;tynia"
  ]
  node [
    id 8
    label "zakrystia"
  ]
  node [
    id 9
    label "organizacja_religijna"
  ]
  node [
    id 10
    label "nawa"
  ]
  node [
    id 11
    label "nerwica_eklezjogenna"
  ]
  node [
    id 12
    label "prezbiterium"
  ]
  node [
    id 13
    label "kropielnica"
  ]
  node [
    id 14
    label "wsp&#243;lnota"
  ]
  node [
    id 15
    label "church"
  ]
  node [
    id 16
    label "kruchta"
  ]
  node [
    id 17
    label "Ska&#322;ka"
  ]
  node [
    id 18
    label "kult"
  ]
  node [
    id 19
    label "ub&#322;agalnia"
  ]
  node [
    id 20
    label "dom"
  ]
  node [
    id 21
    label "opu&#347;ci&#263;"
  ]
  node [
    id 22
    label "poruszy&#263;_si&#281;"
  ]
  node [
    id 23
    label "proceed"
  ]
  node [
    id 24
    label "powie&#347;&#263;_si&#281;"
  ]
  node [
    id 25
    label "popsu&#263;_si&#281;"
  ]
  node [
    id 26
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 27
    label "kopn&#261;&#263;_si&#281;"
  ]
  node [
    id 28
    label "zacz&#261;&#263;"
  ]
  node [
    id 29
    label "zmieni&#263;"
  ]
  node [
    id 30
    label "zosta&#263;"
  ]
  node [
    id 31
    label "sail"
  ]
  node [
    id 32
    label "leave"
  ]
  node [
    id 33
    label "uda&#263;_si&#281;"
  ]
  node [
    id 34
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 35
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 36
    label "zrobi&#263;"
  ]
  node [
    id 37
    label "wyp&#322;yn&#261;&#263;"
  ]
  node [
    id 38
    label "sprzeda&#263;_si&#281;"
  ]
  node [
    id 39
    label "przyj&#261;&#263;"
  ]
  node [
    id 40
    label "potoczy&#263;_si&#281;"
  ]
  node [
    id 41
    label "become"
  ]
  node [
    id 42
    label "play_along"
  ]
  node [
    id 43
    label "travel"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
