graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.262295081967213
  density 0.005310551835603787
  graphCliqueNumber 4
  node [
    id 0
    label "junior"
    origin "text"
  ]
  node [
    id 1
    label "bzura"
    origin "text"
  ]
  node [
    id 2
    label "jak"
    origin "text"
  ]
  node [
    id 3
    label "raz"
    origin "text"
  ]
  node [
    id 4
    label "zajmowa&#263;"
    origin "text"
  ]
  node [
    id 5
    label "miejsce"
    origin "text"
  ]
  node [
    id 6
    label "grupa"
    origin "text"
  ]
  node [
    id 7
    label "bardzo"
    origin "text"
  ]
  node [
    id 8
    label "mks"
    origin "text"
  ]
  node [
    id 9
    label "przegra&#263;"
    origin "text"
  ]
  node [
    id 10
    label "tylko"
    origin "text"
  ]
  node [
    id 11
    label "wyjazd"
    origin "text"
  ]
  node [
    id 12
    label "uks"
    origin "text"
  ]
  node [
    id 13
    label "sms"
    origin "text"
  ]
  node [
    id 14
    label "&#322;&#243;d&#378;"
    origin "text"
  ]
  node [
    id 15
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 16
    label "mecz"
    origin "text"
  ]
  node [
    id 17
    label "pojecha&#263;"
    origin "text"
  ]
  node [
    id 18
    label "by&#263;"
    origin "text"
  ]
  node [
    id 19
    label "spowodowa&#263;"
    origin "text"
  ]
  node [
    id 20
    label "choroba"
    origin "text"
  ]
  node [
    id 21
    label "pozosta&#322;y"
    origin "text"
  ]
  node [
    id 22
    label "zawodnik"
    origin "text"
  ]
  node [
    id 23
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 24
    label "ksi&#261;dz"
    origin "text"
  ]
  node [
    id 25
    label "wifama"
    origin "text"
  ]
  node [
    id 26
    label "walczy&#263;"
    origin "text"
  ]
  node [
    id 27
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 28
    label "klasa"
    origin "text"
  ]
  node [
    id 29
    label "rozgrywkowy"
    origin "text"
  ]
  node [
    id 30
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 31
    label "mistrz"
    origin "text"
  ]
  node [
    id 32
    label "wojew&#243;dztwo"
    origin "text"
  ]
  node [
    id 33
    label "zako&#324;czy&#263;"
    origin "text"
  ]
  node [
    id 34
    label "si&#281;"
    origin "text"
  ]
  node [
    id 35
    label "zwyci&#281;stwo"
    origin "text"
  ]
  node [
    id 36
    label "gospodarz"
    origin "text"
  ]
  node [
    id 37
    label "cieszy&#263;"
    origin "text"
  ]
  node [
    id 38
    label "ostatnio"
    origin "text"
  ]
  node [
    id 39
    label "wifam&#261;"
    origin "text"
  ]
  node [
    id 40
    label "podopieczny"
    origin "text"
  ]
  node [
    id 41
    label "grzegorz"
    origin "text"
  ]
  node [
    id 42
    label "paw&#322;owski"
    origin "text"
  ]
  node [
    id 43
    label "wygra&#263;"
    origin "text"
  ]
  node [
    id 44
    label "fina&#322;"
    origin "text"
  ]
  node [
    id 45
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 46
    label "m&#322;odzik"
    origin "text"
  ]
  node [
    id 47
    label "gdzie"
    origin "text"
  ]
  node [
    id 48
    label "zaj&#261;&#263;"
    origin "text"
  ]
  node [
    id 49
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 50
    label "zosta&#263;"
    origin "text"
  ]
  node [
    id 51
    label "dru&#380;yna"
    origin "text"
  ]
  node [
    id 52
    label "polska"
    origin "text"
  ]
  node [
    id 53
    label "wielki"
    origin "text"
  ]
  node [
    id 54
    label "osi&#261;gni&#281;cie"
    origin "text"
  ]
  node [
    id 55
    label "m&#322;ode"
    origin "text"
  ]
  node [
    id 56
    label "siatkarz"
    origin "text"
  ]
  node [
    id 57
    label "cz&#322;owiek"
  ]
  node [
    id 58
    label "potomek"
  ]
  node [
    id 59
    label "m&#322;odzieniec"
  ]
  node [
    id 60
    label "byd&#322;o"
  ]
  node [
    id 61
    label "zobo"
  ]
  node [
    id 62
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 63
    label "yakalo"
  ]
  node [
    id 64
    label "dzo"
  ]
  node [
    id 65
    label "chwila"
  ]
  node [
    id 66
    label "uderzenie"
  ]
  node [
    id 67
    label "cios"
  ]
  node [
    id 68
    label "time"
  ]
  node [
    id 69
    label "return"
  ]
  node [
    id 70
    label "dostarcza&#263;"
  ]
  node [
    id 71
    label "anektowa&#263;"
  ]
  node [
    id 72
    label "trwa&#263;"
  ]
  node [
    id 73
    label "pali&#263;_si&#281;"
  ]
  node [
    id 74
    label "lokowa&#263;_si&#281;"
  ]
  node [
    id 75
    label "korzysta&#263;"
  ]
  node [
    id 76
    label "fill"
  ]
  node [
    id 77
    label "aim"
  ]
  node [
    id 78
    label "rozciekawia&#263;"
  ]
  node [
    id 79
    label "zadawa&#263;"
  ]
  node [
    id 80
    label "robi&#263;"
  ]
  node [
    id 81
    label "do"
  ]
  node [
    id 82
    label "klasyfikacja"
  ]
  node [
    id 83
    label "rozprzestrzenia&#263;_si&#281;"
  ]
  node [
    id 84
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 85
    label "bra&#263;"
  ]
  node [
    id 86
    label "obejmowa&#263;"
  ]
  node [
    id 87
    label "sake"
  ]
  node [
    id 88
    label "sytuowa&#263;_si&#281;"
  ]
  node [
    id 89
    label "schorzenie"
  ]
  node [
    id 90
    label "zabiera&#263;"
  ]
  node [
    id 91
    label "komornik"
  ]
  node [
    id 92
    label "prosecute"
  ]
  node [
    id 93
    label "topographic_point"
  ]
  node [
    id 94
    label "powodowa&#263;"
  ]
  node [
    id 95
    label "cia&#322;o"
  ]
  node [
    id 96
    label "plac"
  ]
  node [
    id 97
    label "cecha"
  ]
  node [
    id 98
    label "uwaga"
  ]
  node [
    id 99
    label "przestrze&#324;"
  ]
  node [
    id 100
    label "status"
  ]
  node [
    id 101
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 102
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 103
    label "rz&#261;d"
  ]
  node [
    id 104
    label "praca"
  ]
  node [
    id 105
    label "location"
  ]
  node [
    id 106
    label "warunek_lokalowy"
  ]
  node [
    id 107
    label "odm&#322;adza&#263;"
  ]
  node [
    id 108
    label "asymilowa&#263;"
  ]
  node [
    id 109
    label "cz&#261;steczka"
  ]
  node [
    id 110
    label "uk&#322;ad_okresowy_pierwiastk&#243;w_chemicznych"
  ]
  node [
    id 111
    label "egzemplarz"
  ]
  node [
    id 112
    label "formacja_geologiczna"
  ]
  node [
    id 113
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 114
    label "harcerze_starsi"
  ]
  node [
    id 115
    label "liga"
  ]
  node [
    id 116
    label "Terranie"
  ]
  node [
    id 117
    label "&#346;wietliki"
  ]
  node [
    id 118
    label "pakiet_klimatyczny"
  ]
  node [
    id 119
    label "oddzia&#322;"
  ]
  node [
    id 120
    label "stage_set"
  ]
  node [
    id 121
    label "Entuzjastki"
  ]
  node [
    id 122
    label "odm&#322;odzi&#263;"
  ]
  node [
    id 123
    label "odm&#322;odzenie"
  ]
  node [
    id 124
    label "type"
  ]
  node [
    id 125
    label "category"
  ]
  node [
    id 126
    label "asymilowanie"
  ]
  node [
    id 127
    label "specgrupa"
  ]
  node [
    id 128
    label "odm&#322;adzanie"
  ]
  node [
    id 129
    label "gromada"
  ]
  node [
    id 130
    label "Eurogrupa"
  ]
  node [
    id 131
    label "jednostka_systematyczna"
  ]
  node [
    id 132
    label "kompozycja"
  ]
  node [
    id 133
    label "zwi&#261;zek_organiczny"
  ]
  node [
    id 134
    label "zbi&#243;r"
  ]
  node [
    id 135
    label "w_chuj"
  ]
  node [
    id 136
    label "play"
  ]
  node [
    id 137
    label "ponie&#347;&#263;"
  ]
  node [
    id 138
    label "podr&#243;&#380;"
  ]
  node [
    id 139
    label "digression"
  ]
  node [
    id 140
    label "SMS"
  ]
  node [
    id 141
    label "regaty"
  ]
  node [
    id 142
    label "statek"
  ]
  node [
    id 143
    label "spalin&#243;wka"
  ]
  node [
    id 144
    label "pok&#322;ad"
  ]
  node [
    id 145
    label "ster"
  ]
  node [
    id 146
    label "kratownica"
  ]
  node [
    id 147
    label "pojazd_niemechaniczny"
  ]
  node [
    id 148
    label "drzewce"
  ]
  node [
    id 149
    label "obrona"
  ]
  node [
    id 150
    label "gra"
  ]
  node [
    id 151
    label "dwumecz"
  ]
  node [
    id 152
    label "game"
  ]
  node [
    id 153
    label "serw"
  ]
  node [
    id 154
    label "odby&#263;"
  ]
  node [
    id 155
    label "przesun&#261;&#263;"
  ]
  node [
    id 156
    label "napa&#347;&#263;"
  ]
  node [
    id 157
    label "uda&#263;_si&#281;"
  ]
  node [
    id 158
    label "przemie&#347;ci&#263;_si&#281;"
  ]
  node [
    id 159
    label "poci&#261;gn&#261;&#263;"
  ]
  node [
    id 160
    label "drive"
  ]
  node [
    id 161
    label "odjecha&#263;"
  ]
  node [
    id 162
    label "ride"
  ]
  node [
    id 163
    label "ruszy&#263;"
  ]
  node [
    id 164
    label "skorzysta&#263;"
  ]
  node [
    id 165
    label "si&#281;ga&#263;"
  ]
  node [
    id 166
    label "obecno&#347;&#263;"
  ]
  node [
    id 167
    label "stan"
  ]
  node [
    id 168
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 169
    label "stand"
  ]
  node [
    id 170
    label "mie&#263;_miejsce"
  ]
  node [
    id 171
    label "uczestniczy&#263;"
  ]
  node [
    id 172
    label "chodzi&#263;"
  ]
  node [
    id 173
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 174
    label "equal"
  ]
  node [
    id 175
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 176
    label "act"
  ]
  node [
    id 177
    label "ognisko"
  ]
  node [
    id 178
    label "odezwanie_si&#281;"
  ]
  node [
    id 179
    label "zaostrzy&#263;_si&#281;"
  ]
  node [
    id 180
    label "z&#322;&#243;g_wapniowy"
  ]
  node [
    id 181
    label "zaostrzanie_si&#281;"
  ]
  node [
    id 182
    label "przypadek"
  ]
  node [
    id 183
    label "zajmowanie"
  ]
  node [
    id 184
    label "badanie_histopatologiczne"
  ]
  node [
    id 185
    label "zaostrzenie_si&#281;"
  ]
  node [
    id 186
    label "atakowanie"
  ]
  node [
    id 187
    label "odezwa&#263;_si&#281;"
  ]
  node [
    id 188
    label "bol&#261;czka"
  ]
  node [
    id 189
    label "remisja"
  ]
  node [
    id 190
    label "grupa_ryzyka"
  ]
  node [
    id 191
    label "atakowa&#263;"
  ]
  node [
    id 192
    label "kryzys"
  ]
  node [
    id 193
    label "nabawienie_si&#281;"
  ]
  node [
    id 194
    label "chor&#243;bka"
  ]
  node [
    id 195
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 196
    label "ubezpieczenie_chorobowe"
  ]
  node [
    id 197
    label "inkubacja"
  ]
  node [
    id 198
    label "powalenie"
  ]
  node [
    id 199
    label "cholera"
  ]
  node [
    id 200
    label "nabawianie_si&#281;"
  ]
  node [
    id 201
    label "odzywa&#263;_si&#281;"
  ]
  node [
    id 202
    label "odzywanie_si&#281;"
  ]
  node [
    id 203
    label "diagnoza"
  ]
  node [
    id 204
    label "zaostrza&#263;_si&#281;"
  ]
  node [
    id 205
    label "powali&#263;"
  ]
  node [
    id 206
    label "zaburzenie"
  ]
  node [
    id 207
    label "inny"
  ]
  node [
    id 208
    label "&#380;ywy"
  ]
  node [
    id 209
    label "facet"
  ]
  node [
    id 210
    label "czo&#322;&#243;wka"
  ]
  node [
    id 211
    label "lista_startowa"
  ]
  node [
    id 212
    label "uczestnik"
  ]
  node [
    id 213
    label "orygina&#322;"
  ]
  node [
    id 214
    label "sportowiec"
  ]
  node [
    id 215
    label "zi&#243;&#322;ko"
  ]
  node [
    id 216
    label "silny"
  ]
  node [
    id 217
    label "wa&#380;nie"
  ]
  node [
    id 218
    label "eksponowany"
  ]
  node [
    id 219
    label "istotnie"
  ]
  node [
    id 220
    label "znaczny"
  ]
  node [
    id 221
    label "dobry"
  ]
  node [
    id 222
    label "wynios&#322;y"
  ]
  node [
    id 223
    label "dono&#347;ny"
  ]
  node [
    id 224
    label "klecha"
  ]
  node [
    id 225
    label "eklezjasta"
  ]
  node [
    id 226
    label "rozgrzeszanie"
  ]
  node [
    id 227
    label "duszpasterstwo"
  ]
  node [
    id 228
    label "rozgrzesza&#263;"
  ]
  node [
    id 229
    label "kap&#322;an"
  ]
  node [
    id 230
    label "ksi&#281;&#380;a"
  ]
  node [
    id 231
    label "duchowny"
  ]
  node [
    id 232
    label "kol&#281;da"
  ]
  node [
    id 233
    label "seminarzysta"
  ]
  node [
    id 234
    label "pasterz"
  ]
  node [
    id 235
    label "zmaga&#263;_si&#281;"
  ]
  node [
    id 236
    label "cope"
  ]
  node [
    id 237
    label "contend"
  ]
  node [
    id 238
    label "zawody"
  ]
  node [
    id 239
    label "opowiada&#263;_si&#281;"
  ]
  node [
    id 240
    label "dzia&#322;a&#263;"
  ]
  node [
    id 241
    label "wrestle"
  ]
  node [
    id 242
    label "sprzeciwia&#263;_si&#281;"
  ]
  node [
    id 243
    label "my&#347;lenie"
  ]
  node [
    id 244
    label "argue"
  ]
  node [
    id 245
    label "stara&#263;_si&#281;"
  ]
  node [
    id 246
    label "fight"
  ]
  node [
    id 247
    label "opiera&#263;_si&#281;"
  ]
  node [
    id 248
    label "jaki&#347;"
  ]
  node [
    id 249
    label "typ"
  ]
  node [
    id 250
    label "warstwa"
  ]
  node [
    id 251
    label "znak_jako&#347;ci"
  ]
  node [
    id 252
    label "przedmiot"
  ]
  node [
    id 253
    label "przepisa&#263;"
  ]
  node [
    id 254
    label "pomoc"
  ]
  node [
    id 255
    label "arrangement"
  ]
  node [
    id 256
    label "wagon"
  ]
  node [
    id 257
    label "form"
  ]
  node [
    id 258
    label "zaleta"
  ]
  node [
    id 259
    label "poziom"
  ]
  node [
    id 260
    label "dziennik_lekcyjny"
  ]
  node [
    id 261
    label "&#347;rodowisko"
  ]
  node [
    id 262
    label "atak"
  ]
  node [
    id 263
    label "przepisanie"
  ]
  node [
    id 264
    label "szko&#322;a"
  ]
  node [
    id 265
    label "class"
  ]
  node [
    id 266
    label "organizacja"
  ]
  node [
    id 267
    label "promocja"
  ]
  node [
    id 268
    label "&#322;awka"
  ]
  node [
    id 269
    label "kurs"
  ]
  node [
    id 270
    label "botanika"
  ]
  node [
    id 271
    label "sala"
  ]
  node [
    id 272
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 273
    label "obiekt"
  ]
  node [
    id 274
    label "Ekwici"
  ]
  node [
    id 275
    label "fakcja"
  ]
  node [
    id 276
    label "tablica"
  ]
  node [
    id 277
    label "programowanie_obiektowe"
  ]
  node [
    id 278
    label "wykrzyknik"
  ]
  node [
    id 279
    label "mecz_mistrzowski"
  ]
  node [
    id 280
    label "jako&#347;&#263;"
  ]
  node [
    id 281
    label "rezerwa"
  ]
  node [
    id 282
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 283
    label "podtytu&#322;"
  ]
  node [
    id 284
    label "debit"
  ]
  node [
    id 285
    label "szata_graficzna"
  ]
  node [
    id 286
    label "elevation"
  ]
  node [
    id 287
    label "wyda&#263;"
  ]
  node [
    id 288
    label "nadtytu&#322;"
  ]
  node [
    id 289
    label "tytulatura"
  ]
  node [
    id 290
    label "nazwa"
  ]
  node [
    id 291
    label "redaktor"
  ]
  node [
    id 292
    label "wydawa&#263;"
  ]
  node [
    id 293
    label "druk"
  ]
  node [
    id 294
    label "mianowaniec"
  ]
  node [
    id 295
    label "poster"
  ]
  node [
    id 296
    label "publikacja"
  ]
  node [
    id 297
    label "miszczu"
  ]
  node [
    id 298
    label "rzemie&#347;lnik"
  ]
  node [
    id 299
    label "znakomito&#347;&#263;"
  ]
  node [
    id 300
    label "doradca"
  ]
  node [
    id 301
    label "werkmistrz"
  ]
  node [
    id 302
    label "majstersztyk"
  ]
  node [
    id 303
    label "zwyci&#281;zca"
  ]
  node [
    id 304
    label "agent"
  ]
  node [
    id 305
    label "kozak"
  ]
  node [
    id 306
    label "autorytet"
  ]
  node [
    id 307
    label "zwierzchnik"
  ]
  node [
    id 308
    label "Towia&#324;ski"
  ]
  node [
    id 309
    label "jednostka_administracyjna"
  ]
  node [
    id 310
    label "makroregion"
  ]
  node [
    id 311
    label "powiat"
  ]
  node [
    id 312
    label "wojew&#243;dztwo_&#322;&#243;dzkie"
  ]
  node [
    id 313
    label "mikroregion"
  ]
  node [
    id 314
    label "wojew&#243;dztwo_&#347;wi&#281;tokrzyskie"
  ]
  node [
    id 315
    label "pa&#324;stwo"
  ]
  node [
    id 316
    label "communicate"
  ]
  node [
    id 317
    label "cause"
  ]
  node [
    id 318
    label "zrezygnowa&#263;"
  ]
  node [
    id 319
    label "wytworzy&#263;"
  ]
  node [
    id 320
    label "przesta&#263;"
  ]
  node [
    id 321
    label "ukszta&#322;towa&#263;"
  ]
  node [
    id 322
    label "dispose"
  ]
  node [
    id 323
    label "zrobi&#263;"
  ]
  node [
    id 324
    label "puchar"
  ]
  node [
    id 325
    label "beat"
  ]
  node [
    id 326
    label "sukces"
  ]
  node [
    id 327
    label "conquest"
  ]
  node [
    id 328
    label "poradzenie_sobie"
  ]
  node [
    id 329
    label "organizm"
  ]
  node [
    id 330
    label "opiekun"
  ]
  node [
    id 331
    label "g&#322;owa_domu"
  ]
  node [
    id 332
    label "rolnik"
  ]
  node [
    id 333
    label "wie&#347;niak"
  ]
  node [
    id 334
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 335
    label "zarz&#261;dca"
  ]
  node [
    id 336
    label "m&#261;&#380;"
  ]
  node [
    id 337
    label "organizator"
  ]
  node [
    id 338
    label "amuse"
  ]
  node [
    id 339
    label "pie&#347;ci&#263;"
  ]
  node [
    id 340
    label "nape&#322;nia&#263;"
  ]
  node [
    id 341
    label "wzbudza&#263;"
  ]
  node [
    id 342
    label "raczy&#263;"
  ]
  node [
    id 343
    label "porusza&#263;"
  ]
  node [
    id 344
    label "szczerzy&#263;"
  ]
  node [
    id 345
    label "zaspokaja&#263;"
  ]
  node [
    id 346
    label "ostatni"
  ]
  node [
    id 347
    label "poprzednio"
  ]
  node [
    id 348
    label "aktualnie"
  ]
  node [
    id 349
    label "score"
  ]
  node [
    id 350
    label "okaza&#263;_si&#281;"
  ]
  node [
    id 351
    label "zwojowa&#263;"
  ]
  node [
    id 352
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 353
    label "leave"
  ]
  node [
    id 354
    label "znie&#347;&#263;"
  ]
  node [
    id 355
    label "zagwarantowa&#263;"
  ]
  node [
    id 356
    label "instrument_muzyczny"
  ]
  node [
    id 357
    label "zagra&#263;"
  ]
  node [
    id 358
    label "net_income"
  ]
  node [
    id 359
    label "koniec"
  ]
  node [
    id 360
    label "conclusion"
  ]
  node [
    id 361
    label "coating"
  ]
  node [
    id 362
    label "runda"
  ]
  node [
    id 363
    label "championship"
  ]
  node [
    id 364
    label "Formu&#322;a_1"
  ]
  node [
    id 365
    label "stopie&#324;_harcerski"
  ]
  node [
    id 366
    label "harcerz"
  ]
  node [
    id 367
    label "g&#243;wniarz"
  ]
  node [
    id 368
    label "dziecko"
  ]
  node [
    id 369
    label "ch&#322;opta&#347;"
  ]
  node [
    id 370
    label "beniaminek"
  ]
  node [
    id 371
    label "zwierz&#281;"
  ]
  node [
    id 372
    label "niepe&#322;noletni"
  ]
  node [
    id 373
    label "go&#322;ow&#261;s"
  ]
  node [
    id 374
    label "dostarczy&#263;"
  ]
  node [
    id 375
    label "wype&#322;ni&#263;"
  ]
  node [
    id 376
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 377
    label "obj&#261;&#263;"
  ]
  node [
    id 378
    label "zada&#263;"
  ]
  node [
    id 379
    label "sorb"
  ]
  node [
    id 380
    label "interest"
  ]
  node [
    id 381
    label "usytuowa&#263;_si&#281;"
  ]
  node [
    id 382
    label "wzi&#261;&#263;"
  ]
  node [
    id 383
    label "employment"
  ]
  node [
    id 384
    label "zapanowa&#263;"
  ]
  node [
    id 385
    label "bankrupt"
  ]
  node [
    id 386
    label "zabra&#263;"
  ]
  node [
    id 387
    label "ulokowa&#263;_si&#281;"
  ]
  node [
    id 388
    label "seize"
  ]
  node [
    id 389
    label "wzbudzi&#263;"
  ]
  node [
    id 390
    label "rozciekawi&#263;"
  ]
  node [
    id 391
    label "p&#243;&#378;ny"
  ]
  node [
    id 392
    label "proceed"
  ]
  node [
    id 393
    label "catch"
  ]
  node [
    id 394
    label "pozosta&#263;"
  ]
  node [
    id 395
    label "osta&#263;_si&#281;"
  ]
  node [
    id 396
    label "zmieni&#263;_si&#281;"
  ]
  node [
    id 397
    label "zachowa&#263;_si&#281;"
  ]
  node [
    id 398
    label "change"
  ]
  node [
    id 399
    label "oddali&#263;_si&#281;"
  ]
  node [
    id 400
    label "whole"
  ]
  node [
    id 401
    label "szczep"
  ]
  node [
    id 402
    label "dublet"
  ]
  node [
    id 403
    label "pluton"
  ]
  node [
    id 404
    label "formacja"
  ]
  node [
    id 405
    label "zesp&#243;&#322;"
  ]
  node [
    id 406
    label "force"
  ]
  node [
    id 407
    label "zast&#281;p"
  ]
  node [
    id 408
    label "pododdzia&#322;"
  ]
  node [
    id 409
    label "dupny"
  ]
  node [
    id 410
    label "wysoce"
  ]
  node [
    id 411
    label "wyj&#261;tkowy"
  ]
  node [
    id 412
    label "wybitny"
  ]
  node [
    id 413
    label "prawdziwy"
  ]
  node [
    id 414
    label "nieprzeci&#281;tny"
  ]
  node [
    id 415
    label "zas&#322;u&#380;ony"
  ]
  node [
    id 416
    label "dotarcie"
  ]
  node [
    id 417
    label "dochrapanie_si&#281;"
  ]
  node [
    id 418
    label "skill"
  ]
  node [
    id 419
    label "accomplishment"
  ]
  node [
    id 420
    label "zdarzenie_si&#281;"
  ]
  node [
    id 421
    label "uzyskanie"
  ]
  node [
    id 422
    label "zaawansowanie"
  ]
  node [
    id 423
    label "potomstwo"
  ]
  node [
    id 424
    label "m&#322;odziak"
  ]
  node [
    id 425
    label "fledgling"
  ]
  node [
    id 426
    label "pi&#322;karz"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 3
    target 66
  ]
  edge [
    source 3
    target 67
  ]
  edge [
    source 3
    target 68
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 5
    target 99
  ]
  edge [
    source 5
    target 100
  ]
  edge [
    source 5
    target 101
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 102
  ]
  edge [
    source 5
    target 103
  ]
  edge [
    source 5
    target 104
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 7
    target 135
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 136
  ]
  edge [
    source 9
    target 137
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 24
  ]
  edge [
    source 10
    target 37
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 138
  ]
  edge [
    source 11
    target 139
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 25
  ]
  edge [
    source 14
    target 141
  ]
  edge [
    source 14
    target 142
  ]
  edge [
    source 14
    target 143
  ]
  edge [
    source 14
    target 144
  ]
  edge [
    source 14
    target 145
  ]
  edge [
    source 14
    target 146
  ]
  edge [
    source 14
    target 147
  ]
  edge [
    source 14
    target 148
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 55
  ]
  edge [
    source 15
    target 30
  ]
  edge [
    source 16
    target 18
  ]
  edge [
    source 16
    target 24
  ]
  edge [
    source 16
    target 32
  ]
  edge [
    source 16
    target 33
  ]
  edge [
    source 16
    target 149
  ]
  edge [
    source 16
    target 150
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 154
  ]
  edge [
    source 17
    target 155
  ]
  edge [
    source 17
    target 156
  ]
  edge [
    source 17
    target 157
  ]
  edge [
    source 17
    target 158
  ]
  edge [
    source 17
    target 159
  ]
  edge [
    source 17
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 17
    target 163
  ]
  edge [
    source 17
    target 164
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 52
  ]
  edge [
    source 18
    target 53
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 72
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 175
  ]
  edge [
    source 19
    target 176
  ]
  edge [
    source 19
    target 48
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 177
  ]
  edge [
    source 20
    target 178
  ]
  edge [
    source 20
    target 179
  ]
  edge [
    source 20
    target 180
  ]
  edge [
    source 20
    target 181
  ]
  edge [
    source 20
    target 182
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
  edge [
    source 20
    target 186
  ]
  edge [
    source 20
    target 187
  ]
  edge [
    source 20
    target 188
  ]
  edge [
    source 20
    target 189
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 192
  ]
  edge [
    source 20
    target 193
  ]
  edge [
    source 20
    target 194
  ]
  edge [
    source 20
    target 195
  ]
  edge [
    source 20
    target 196
  ]
  edge [
    source 20
    target 197
  ]
  edge [
    source 20
    target 198
  ]
  edge [
    source 20
    target 199
  ]
  edge [
    source 20
    target 200
  ]
  edge [
    source 20
    target 201
  ]
  edge [
    source 20
    target 202
  ]
  edge [
    source 20
    target 203
  ]
  edge [
    source 20
    target 204
  ]
  edge [
    source 20
    target 205
  ]
  edge [
    source 20
    target 206
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 207
  ]
  edge [
    source 21
    target 208
  ]
  edge [
    source 21
    target 35
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 209
  ]
  edge [
    source 22
    target 210
  ]
  edge [
    source 22
    target 211
  ]
  edge [
    source 22
    target 212
  ]
  edge [
    source 22
    target 213
  ]
  edge [
    source 22
    target 214
  ]
  edge [
    source 22
    target 215
  ]
  edge [
    source 22
    target 46
  ]
  edge [
    source 22
    target 36
  ]
  edge [
    source 23
    target 216
  ]
  edge [
    source 23
    target 217
  ]
  edge [
    source 23
    target 218
  ]
  edge [
    source 23
    target 219
  ]
  edge [
    source 23
    target 220
  ]
  edge [
    source 23
    target 221
  ]
  edge [
    source 23
    target 222
  ]
  edge [
    source 23
    target 223
  ]
  edge [
    source 23
    target 53
  ]
  edge [
    source 23
    target 31
  ]
  edge [
    source 23
    target 45
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 224
  ]
  edge [
    source 24
    target 225
  ]
  edge [
    source 24
    target 226
  ]
  edge [
    source 24
    target 227
  ]
  edge [
    source 24
    target 228
  ]
  edge [
    source 24
    target 229
  ]
  edge [
    source 24
    target 230
  ]
  edge [
    source 24
    target 231
  ]
  edge [
    source 24
    target 232
  ]
  edge [
    source 24
    target 233
  ]
  edge [
    source 24
    target 234
  ]
  edge [
    source 24
    target 37
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 235
  ]
  edge [
    source 26
    target 236
  ]
  edge [
    source 26
    target 237
  ]
  edge [
    source 26
    target 238
  ]
  edge [
    source 26
    target 239
  ]
  edge [
    source 26
    target 240
  ]
  edge [
    source 26
    target 241
  ]
  edge [
    source 26
    target 242
  ]
  edge [
    source 26
    target 80
  ]
  edge [
    source 26
    target 243
  ]
  edge [
    source 26
    target 244
  ]
  edge [
    source 26
    target 245
  ]
  edge [
    source 26
    target 246
  ]
  edge [
    source 26
    target 247
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 248
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 249
  ]
  edge [
    source 28
    target 250
  ]
  edge [
    source 28
    target 251
  ]
  edge [
    source 28
    target 252
  ]
  edge [
    source 28
    target 253
  ]
  edge [
    source 28
    target 254
  ]
  edge [
    source 28
    target 255
  ]
  edge [
    source 28
    target 256
  ]
  edge [
    source 28
    target 257
  ]
  edge [
    source 28
    target 258
  ]
  edge [
    source 28
    target 259
  ]
  edge [
    source 28
    target 260
  ]
  edge [
    source 28
    target 261
  ]
  edge [
    source 28
    target 262
  ]
  edge [
    source 28
    target 263
  ]
  edge [
    source 28
    target 264
  ]
  edge [
    source 28
    target 265
  ]
  edge [
    source 28
    target 266
  ]
  edge [
    source 28
    target 149
  ]
  edge [
    source 28
    target 124
  ]
  edge [
    source 28
    target 267
  ]
  edge [
    source 28
    target 268
  ]
  edge [
    source 28
    target 269
  ]
  edge [
    source 28
    target 270
  ]
  edge [
    source 28
    target 271
  ]
  edge [
    source 28
    target 272
  ]
  edge [
    source 28
    target 129
  ]
  edge [
    source 28
    target 273
  ]
  edge [
    source 28
    target 274
  ]
  edge [
    source 28
    target 275
  ]
  edge [
    source 28
    target 276
  ]
  edge [
    source 28
    target 277
  ]
  edge [
    source 28
    target 278
  ]
  edge [
    source 28
    target 131
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 134
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 28
    target 281
  ]
  edge [
    source 28
    target 282
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 283
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 30
    target 285
  ]
  edge [
    source 30
    target 286
  ]
  edge [
    source 30
    target 287
  ]
  edge [
    source 30
    target 288
  ]
  edge [
    source 30
    target 289
  ]
  edge [
    source 30
    target 290
  ]
  edge [
    source 30
    target 291
  ]
  edge [
    source 30
    target 292
  ]
  edge [
    source 30
    target 293
  ]
  edge [
    source 30
    target 294
  ]
  edge [
    source 30
    target 295
  ]
  edge [
    source 30
    target 296
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 297
  ]
  edge [
    source 31
    target 298
  ]
  edge [
    source 31
    target 299
  ]
  edge [
    source 31
    target 300
  ]
  edge [
    source 31
    target 301
  ]
  edge [
    source 31
    target 302
  ]
  edge [
    source 31
    target 303
  ]
  edge [
    source 31
    target 304
  ]
  edge [
    source 31
    target 305
  ]
  edge [
    source 31
    target 306
  ]
  edge [
    source 31
    target 307
  ]
  edge [
    source 31
    target 308
  ]
  edge [
    source 31
    target 45
  ]
  edge [
    source 32
    target 309
  ]
  edge [
    source 32
    target 310
  ]
  edge [
    source 32
    target 311
  ]
  edge [
    source 32
    target 312
  ]
  edge [
    source 32
    target 313
  ]
  edge [
    source 32
    target 314
  ]
  edge [
    source 32
    target 315
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 316
  ]
  edge [
    source 33
    target 317
  ]
  edge [
    source 33
    target 318
  ]
  edge [
    source 33
    target 319
  ]
  edge [
    source 33
    target 320
  ]
  edge [
    source 33
    target 321
  ]
  edge [
    source 33
    target 322
  ]
  edge [
    source 33
    target 323
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 324
  ]
  edge [
    source 35
    target 325
  ]
  edge [
    source 35
    target 326
  ]
  edge [
    source 35
    target 327
  ]
  edge [
    source 35
    target 328
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 329
  ]
  edge [
    source 36
    target 330
  ]
  edge [
    source 36
    target 331
  ]
  edge [
    source 36
    target 332
  ]
  edge [
    source 36
    target 333
  ]
  edge [
    source 36
    target 334
  ]
  edge [
    source 36
    target 335
  ]
  edge [
    source 36
    target 336
  ]
  edge [
    source 36
    target 337
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 338
  ]
  edge [
    source 37
    target 339
  ]
  edge [
    source 37
    target 340
  ]
  edge [
    source 37
    target 341
  ]
  edge [
    source 37
    target 342
  ]
  edge [
    source 37
    target 343
  ]
  edge [
    source 37
    target 344
  ]
  edge [
    source 37
    target 345
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 346
  ]
  edge [
    source 38
    target 347
  ]
  edge [
    source 38
    target 348
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 40
    target 57
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 349
  ]
  edge [
    source 43
    target 350
  ]
  edge [
    source 43
    target 351
  ]
  edge [
    source 43
    target 352
  ]
  edge [
    source 43
    target 353
  ]
  edge [
    source 43
    target 354
  ]
  edge [
    source 43
    target 355
  ]
  edge [
    source 43
    target 356
  ]
  edge [
    source 43
    target 357
  ]
  edge [
    source 43
    target 175
  ]
  edge [
    source 43
    target 323
  ]
  edge [
    source 43
    target 358
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 359
  ]
  edge [
    source 44
    target 360
  ]
  edge [
    source 44
    target 361
  ]
  edge [
    source 44
    target 362
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 45
    target 363
  ]
  edge [
    source 45
    target 364
  ]
  edge [
    source 45
    target 238
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 57
  ]
  edge [
    source 46
    target 329
  ]
  edge [
    source 46
    target 365
  ]
  edge [
    source 46
    target 55
  ]
  edge [
    source 46
    target 366
  ]
  edge [
    source 46
    target 367
  ]
  edge [
    source 46
    target 368
  ]
  edge [
    source 46
    target 369
  ]
  edge [
    source 46
    target 370
  ]
  edge [
    source 46
    target 371
  ]
  edge [
    source 46
    target 372
  ]
  edge [
    source 46
    target 373
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 48
    target 374
  ]
  edge [
    source 48
    target 375
  ]
  edge [
    source 48
    target 71
  ]
  edge [
    source 48
    target 376
  ]
  edge [
    source 48
    target 377
  ]
  edge [
    source 48
    target 378
  ]
  edge [
    source 48
    target 379
  ]
  edge [
    source 48
    target 380
  ]
  edge [
    source 48
    target 164
  ]
  edge [
    source 48
    target 381
  ]
  edge [
    source 48
    target 382
  ]
  edge [
    source 48
    target 383
  ]
  edge [
    source 48
    target 384
  ]
  edge [
    source 48
    target 81
  ]
  edge [
    source 48
    target 82
  ]
  edge [
    source 48
    target 175
  ]
  edge [
    source 48
    target 385
  ]
  edge [
    source 48
    target 386
  ]
  edge [
    source 48
    target 387
  ]
  edge [
    source 48
    target 91
  ]
  edge [
    source 48
    target 92
  ]
  edge [
    source 48
    target 388
  ]
  edge [
    source 48
    target 93
  ]
  edge [
    source 48
    target 389
  ]
  edge [
    source 48
    target 390
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 391
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 392
  ]
  edge [
    source 50
    target 393
  ]
  edge [
    source 50
    target 394
  ]
  edge [
    source 50
    target 395
  ]
  edge [
    source 50
    target 396
  ]
  edge [
    source 50
    target 397
  ]
  edge [
    source 50
    target 175
  ]
  edge [
    source 50
    target 398
  ]
  edge [
    source 50
    target 399
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 400
  ]
  edge [
    source 51
    target 401
  ]
  edge [
    source 51
    target 402
  ]
  edge [
    source 51
    target 403
  ]
  edge [
    source 51
    target 404
  ]
  edge [
    source 51
    target 405
  ]
  edge [
    source 51
    target 406
  ]
  edge [
    source 51
    target 407
  ]
  edge [
    source 51
    target 408
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 409
  ]
  edge [
    source 53
    target 410
  ]
  edge [
    source 53
    target 411
  ]
  edge [
    source 53
    target 412
  ]
  edge [
    source 53
    target 220
  ]
  edge [
    source 53
    target 413
  ]
  edge [
    source 53
    target 414
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 415
  ]
  edge [
    source 54
    target 416
  ]
  edge [
    source 54
    target 326
  ]
  edge [
    source 54
    target 417
  ]
  edge [
    source 54
    target 418
  ]
  edge [
    source 54
    target 419
  ]
  edge [
    source 54
    target 176
  ]
  edge [
    source 54
    target 420
  ]
  edge [
    source 54
    target 421
  ]
  edge [
    source 54
    target 422
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 55
    target 423
  ]
  edge [
    source 55
    target 329
  ]
  edge [
    source 55
    target 424
  ]
  edge [
    source 55
    target 371
  ]
  edge [
    source 55
    target 425
  ]
  edge [
    source 56
    target 426
  ]
]
