graph [
  maxDegree 42
  minDegree 1
  meanDegree 1.9801980198019802
  density 0.019801980198019802
  graphCliqueNumber 2
  node [
    id 0
    label "mieszkaniec"
    origin "text"
  ]
  node [
    id 1
    label "zi&#281;bice"
    origin "text"
  ]
  node [
    id 2
    label "musza"
    origin "text"
  ]
  node [
    id 3
    label "dop&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 4
    label "rachunek"
    origin "text"
  ]
  node [
    id 5
    label "woda"
    origin "text"
  ]
  node [
    id 6
    label "kilka"
    origin "text"
  ]
  node [
    id 7
    label "tysi&#261;c"
    origin "text"
  ]
  node [
    id 8
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 9
    label "pow&#243;d"
    origin "text"
  ]
  node [
    id 10
    label "niekontrolowany"
    origin "text"
  ]
  node [
    id 11
    label "wyciek"
    origin "text"
  ]
  node [
    id 12
    label "cz&#322;owiek"
  ]
  node [
    id 13
    label "ludno&#347;&#263;"
  ]
  node [
    id 14
    label "zwierz&#281;"
  ]
  node [
    id 15
    label "zap&#322;aci&#263;"
  ]
  node [
    id 16
    label "spis"
  ]
  node [
    id 17
    label "wytw&#243;r"
  ]
  node [
    id 18
    label "check"
  ]
  node [
    id 19
    label "count"
  ]
  node [
    id 20
    label "mienie"
  ]
  node [
    id 21
    label "nale&#380;no&#347;&#263;"
  ]
  node [
    id 22
    label "wypowied&#378;"
  ]
  node [
    id 23
    label "obiekt_naturalny"
  ]
  node [
    id 24
    label "bicie"
  ]
  node [
    id 25
    label "wysi&#281;k"
  ]
  node [
    id 26
    label "pustka"
  ]
  node [
    id 27
    label "woda_s&#322;odka"
  ]
  node [
    id 28
    label "p&#322;ycizna"
  ]
  node [
    id 29
    label "ciecz"
  ]
  node [
    id 30
    label "spi&#281;trza&#263;"
  ]
  node [
    id 31
    label "uj&#281;cie_wody"
  ]
  node [
    id 32
    label "chlasta&#263;"
  ]
  node [
    id 33
    label "spi&#281;trzy&#263;"
  ]
  node [
    id 34
    label "nap&#243;j"
  ]
  node [
    id 35
    label "bombast"
  ]
  node [
    id 36
    label "water"
  ]
  node [
    id 37
    label "kryptodepresja"
  ]
  node [
    id 38
    label "wodnik"
  ]
  node [
    id 39
    label "pojazd"
  ]
  node [
    id 40
    label "&#347;r&#243;dl&#261;dowy"
  ]
  node [
    id 41
    label "fala"
  ]
  node [
    id 42
    label "Waruna"
  ]
  node [
    id 43
    label "nap&#243;j_bezalkoholowy"
  ]
  node [
    id 44
    label "zrzut"
  ]
  node [
    id 45
    label "dotleni&#263;"
  ]
  node [
    id 46
    label "utylizator"
  ]
  node [
    id 47
    label "przyroda"
  ]
  node [
    id 48
    label "uci&#261;g"
  ]
  node [
    id 49
    label "wybrze&#380;e"
  ]
  node [
    id 50
    label "nabranie"
  ]
  node [
    id 51
    label "p&#322;yn_ustrojowy"
  ]
  node [
    id 52
    label "chlastanie"
  ]
  node [
    id 53
    label "klarownik"
  ]
  node [
    id 54
    label "przybrze&#380;e"
  ]
  node [
    id 55
    label "deklamacja"
  ]
  node [
    id 56
    label "spi&#281;trzenie"
  ]
  node [
    id 57
    label "przybieranie"
  ]
  node [
    id 58
    label "nabra&#263;"
  ]
  node [
    id 59
    label "tlenek"
  ]
  node [
    id 60
    label "spi&#281;trzanie"
  ]
  node [
    id 61
    label "l&#243;d"
  ]
  node [
    id 62
    label "&#347;ledziowate"
  ]
  node [
    id 63
    label "ryba"
  ]
  node [
    id 64
    label "tauzen"
  ]
  node [
    id 65
    label "musik"
  ]
  node [
    id 66
    label "molarity"
  ]
  node [
    id 67
    label "licytacja"
  ]
  node [
    id 68
    label "patyk"
  ]
  node [
    id 69
    label "liczba"
  ]
  node [
    id 70
    label "gra_w_karty"
  ]
  node [
    id 71
    label "kwota"
  ]
  node [
    id 72
    label "szlachetny"
  ]
  node [
    id 73
    label "metaliczny"
  ]
  node [
    id 74
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 75
    label "z&#322;ocenie"
  ]
  node [
    id 76
    label "grosz"
  ]
  node [
    id 77
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 78
    label "utytu&#322;owany"
  ]
  node [
    id 79
    label "poz&#322;ocenie"
  ]
  node [
    id 80
    label "Polska"
  ]
  node [
    id 81
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 82
    label "wspania&#322;y"
  ]
  node [
    id 83
    label "doskona&#322;y"
  ]
  node [
    id 84
    label "kochany"
  ]
  node [
    id 85
    label "jednostka_monetarna"
  ]
  node [
    id 86
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 87
    label "strona"
  ]
  node [
    id 88
    label "przyczyna"
  ]
  node [
    id 89
    label "matuszka"
  ]
  node [
    id 90
    label "geneza"
  ]
  node [
    id 91
    label "z&#322;o&#380;enie_si&#281;"
  ]
  node [
    id 92
    label "czynnik"
  ]
  node [
    id 93
    label "poci&#261;ganie"
  ]
  node [
    id 94
    label "rezultat"
  ]
  node [
    id 95
    label "uprz&#261;&#380;"
  ]
  node [
    id 96
    label "poci&#261;gni&#281;cie"
  ]
  node [
    id 97
    label "subject"
  ]
  node [
    id 98
    label "magnetic_field"
  ]
  node [
    id 99
    label "zjawisko"
  ]
  node [
    id 100
    label "oznaka"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 62
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 66
  ]
  edge [
    source 7
    target 67
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
]
