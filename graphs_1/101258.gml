graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.9047619047619047
  density 0.09523809523809523
  graphCliqueNumber 3
  node [
    id 0
    label "trasa"
    origin "text"
  ]
  node [
    id 1
    label "siekierkowski"
    origin "text"
  ]
  node [
    id 2
    label "warszawa"
    origin "text"
  ]
  node [
    id 3
    label "marszrutyzacja"
  ]
  node [
    id 4
    label "b&#322;&#261;dzi&#263;"
  ]
  node [
    id 5
    label "w&#281;ze&#322;"
  ]
  node [
    id 6
    label "przebieg"
  ]
  node [
    id 7
    label "odleg&#322;o&#347;&#263;"
  ]
  node [
    id 8
    label "droga"
  ]
  node [
    id 9
    label "infrastruktura"
  ]
  node [
    id 10
    label "b&#322;&#261;dzenie"
  ]
  node [
    id 11
    label "podbieg"
  ]
  node [
    id 12
    label "Warszawa"
  ]
  node [
    id 13
    label "samoch&#243;d"
  ]
  node [
    id 14
    label "fastback"
  ]
  node [
    id 15
    label "obwodnica"
  ]
  node [
    id 16
    label "etapowy"
  ]
  node [
    id 17
    label "&#322;azienkowski"
  ]
  node [
    id 18
    label "Praga"
  ]
  node [
    id 19
    label "po&#322;udnie"
  ]
  node [
    id 20
    label "most"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 17
    target 20
  ]
  edge [
    source 18
    target 19
  ]
]
