graph [
  maxDegree 6
  minDegree 1
  meanDegree 2.142857142857143
  density 0.16483516483516483
  graphCliqueNumber 4
  node [
    id 0
    label "lam"
    origin "text"
  ]
  node [
    id 1
    label "religioznawstwo"
    origin "text"
  ]
  node [
    id 2
    label "soteriologia"
  ]
  node [
    id 3
    label "religiologia"
  ]
  node [
    id 4
    label "psychologia_religii"
  ]
  node [
    id 5
    label "nauka_humanistyczna"
  ]
  node [
    id 6
    label "demonologia"
  ]
  node [
    id 7
    label "Dziamjang"
  ]
  node [
    id 8
    label "Czientse"
  ]
  node [
    id 9
    label "&#321;angpo"
  ]
  node [
    id 10
    label "Jamgon"
  ]
  node [
    id 11
    label "Kongtrul"
  ]
  node [
    id 12
    label "Lodr&#246;"
  ]
  node [
    id 13
    label "Thaye"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 9
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 12
  ]
  edge [
    source 10
    target 13
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 13
  ]
  edge [
    source 12
    target 13
  ]
]
