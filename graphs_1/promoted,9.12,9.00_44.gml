graph [
  maxDegree 27
  minDegree 1
  meanDegree 2.054054054054054
  density 0.028137726767863754
  graphCliqueNumber 2
  node [
    id 0
    label "dziwny"
    origin "text"
  ]
  node [
    id 1
    label "uczucie"
    origin "text"
  ]
  node [
    id 2
    label "siedzie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "s&#322;ucha&#263;"
    origin "text"
  ]
  node [
    id 4
    label "odg&#322;os"
    origin "text"
  ]
  node [
    id 5
    label "inny"
    origin "text"
  ]
  node [
    id 6
    label "planet"
    origin "text"
  ]
  node [
    id 7
    label "dziwy"
  ]
  node [
    id 8
    label "dziwnie"
  ]
  node [
    id 9
    label "zareagowanie"
  ]
  node [
    id 10
    label "wpa&#347;&#263;"
  ]
  node [
    id 11
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 12
    label "opanowanie"
  ]
  node [
    id 13
    label "d&#322;awi&#263;"
  ]
  node [
    id 14
    label "wpada&#263;"
  ]
  node [
    id 15
    label "os&#322;upienie"
  ]
  node [
    id 16
    label "zmys&#322;"
  ]
  node [
    id 17
    label "zaanga&#380;owanie"
  ]
  node [
    id 18
    label "smell"
  ]
  node [
    id 19
    label "zdarzenie_si&#281;"
  ]
  node [
    id 20
    label "ostygn&#261;&#263;"
  ]
  node [
    id 21
    label "afekt"
  ]
  node [
    id 22
    label "stan"
  ]
  node [
    id 23
    label "iskrzy&#263;"
  ]
  node [
    id 24
    label "afekcja"
  ]
  node [
    id 25
    label "przeczulica"
  ]
  node [
    id 26
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 27
    label "czucie"
  ]
  node [
    id 28
    label "doznanie"
  ]
  node [
    id 29
    label "emocja"
  ]
  node [
    id 30
    label "ogrom"
  ]
  node [
    id 31
    label "stygn&#261;&#263;"
  ]
  node [
    id 32
    label "poczucie"
  ]
  node [
    id 33
    label "temperatura"
  ]
  node [
    id 34
    label "trwa&#263;"
  ]
  node [
    id 35
    label "sit"
  ]
  node [
    id 36
    label "pause"
  ]
  node [
    id 37
    label "ptak"
  ]
  node [
    id 38
    label "garowa&#263;"
  ]
  node [
    id 39
    label "mieszka&#263;"
  ]
  node [
    id 40
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 41
    label "znajdowa&#263;_si&#281;"
  ]
  node [
    id 42
    label "przebywa&#263;"
  ]
  node [
    id 43
    label "brood"
  ]
  node [
    id 44
    label "zwierz&#281;"
  ]
  node [
    id 45
    label "doprowadza&#263;"
  ]
  node [
    id 46
    label "spoczywa&#263;"
  ]
  node [
    id 47
    label "tkwi&#263;"
  ]
  node [
    id 48
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 49
    label "continue"
  ]
  node [
    id 50
    label "podporz&#261;dkowywa&#263;_si&#281;"
  ]
  node [
    id 51
    label "lubi&#263;"
  ]
  node [
    id 52
    label "odbiera&#263;"
  ]
  node [
    id 53
    label "wybiera&#263;"
  ]
  node [
    id 54
    label "odtwarza&#263;"
  ]
  node [
    id 55
    label "nat&#281;&#380;a&#263;"
  ]
  node [
    id 56
    label "resonance"
  ]
  node [
    id 57
    label "brzmienie"
  ]
  node [
    id 58
    label "wydanie"
  ]
  node [
    id 59
    label "onomatopeja"
  ]
  node [
    id 60
    label "wpadanie"
  ]
  node [
    id 61
    label "wyda&#263;"
  ]
  node [
    id 62
    label "sound"
  ]
  node [
    id 63
    label "note"
  ]
  node [
    id 64
    label "wydawa&#263;"
  ]
  node [
    id 65
    label "d&#378;wi&#281;k"
  ]
  node [
    id 66
    label "zjawisko"
  ]
  node [
    id 67
    label "wpadni&#281;cie"
  ]
  node [
    id 68
    label "kolejny"
  ]
  node [
    id 69
    label "inaczej"
  ]
  node [
    id 70
    label "r&#243;&#380;ny"
  ]
  node [
    id 71
    label "inszy"
  ]
  node [
    id 72
    label "osobno"
  ]
  node [
    id 73
    label "narz&#281;dzie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 1
    target 32
  ]
  edge [
    source 1
    target 33
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 10
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 6
    target 73
  ]
]
