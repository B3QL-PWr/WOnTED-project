graph [
  maxDegree 36
  minDegree 1
  meanDegree 2.0217391304347827
  density 0.022216913521261348
  graphCliqueNumber 3
  node [
    id 0
    label "rozmowa"
    origin "text"
  ]
  node [
    id 1
    label "mec"
    origin "text"
  ]
  node [
    id 2
    label "monika"
    origin "text"
  ]
  node [
    id 3
    label "brzozowska"
    origin "text"
  ]
  node [
    id 4
    label "pasieka"
    origin "text"
  ]
  node [
    id 5
    label "pe&#322;nomocnik"
    origin "text"
  ]
  node [
    id 6
    label "strona"
    origin "text"
  ]
  node [
    id 7
    label "oskar&#380;aj&#261;cy"
    origin "text"
  ]
  node [
    id 8
    label "niemiec"
    origin "text"
  ]
  node [
    id 9
    label "obra&#380;a&#263;"
    origin "text"
  ]
  node [
    id 10
    label "polski"
    origin "text"
  ]
  node [
    id 11
    label "pracownik"
    origin "text"
  ]
  node [
    id 12
    label "discussion"
  ]
  node [
    id 13
    label "czynno&#347;&#263;"
  ]
  node [
    id 14
    label "odpowied&#378;"
  ]
  node [
    id 15
    label "rozhowor"
  ]
  node [
    id 16
    label "cisza"
  ]
  node [
    id 17
    label "pasiecznik"
  ]
  node [
    id 18
    label "pasieczysko"
  ]
  node [
    id 19
    label "gospodarstwo"
  ]
  node [
    id 20
    label "cz&#322;owiek"
  ]
  node [
    id 21
    label "zast&#281;pca"
  ]
  node [
    id 22
    label "substytuowanie"
  ]
  node [
    id 23
    label "substytuowa&#263;"
  ]
  node [
    id 24
    label "skr&#281;canie"
  ]
  node [
    id 25
    label "voice"
  ]
  node [
    id 26
    label "forma"
  ]
  node [
    id 27
    label "internet"
  ]
  node [
    id 28
    label "skr&#281;ci&#263;"
  ]
  node [
    id 29
    label "kartka"
  ]
  node [
    id 30
    label "orientowa&#263;"
  ]
  node [
    id 31
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 32
    label "powierzchnia"
  ]
  node [
    id 33
    label "plik"
  ]
  node [
    id 34
    label "bok"
  ]
  node [
    id 35
    label "pagina"
  ]
  node [
    id 36
    label "orientowanie"
  ]
  node [
    id 37
    label "fragment"
  ]
  node [
    id 38
    label "trzyma&#263;_si&#281;"
  ]
  node [
    id 39
    label "s&#261;d"
  ]
  node [
    id 40
    label "skr&#281;ca&#263;"
  ]
  node [
    id 41
    label "g&#243;ra"
  ]
  node [
    id 42
    label "serwis_internetowy"
  ]
  node [
    id 43
    label "orientacja"
  ]
  node [
    id 44
    label "linia"
  ]
  node [
    id 45
    label "skr&#281;cenie"
  ]
  node [
    id 46
    label "layout"
  ]
  node [
    id 47
    label "zorientowa&#263;"
  ]
  node [
    id 48
    label "zorientowanie"
  ]
  node [
    id 49
    label "obiekt"
  ]
  node [
    id 50
    label "podmiot"
  ]
  node [
    id 51
    label "ty&#322;"
  ]
  node [
    id 52
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 53
    label "logowanie"
  ]
  node [
    id 54
    label "adres_internetowy"
  ]
  node [
    id 55
    label "uj&#281;cie"
  ]
  node [
    id 56
    label "prz&#243;d"
  ]
  node [
    id 57
    label "posta&#263;"
  ]
  node [
    id 58
    label "niemiecki"
  ]
  node [
    id 59
    label "zachowywa&#263;_si&#281;"
  ]
  node [
    id 60
    label "narusza&#263;"
  ]
  node [
    id 61
    label "mistreat"
  ]
  node [
    id 62
    label "lacki"
  ]
  node [
    id 63
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 64
    label "przedmiot"
  ]
  node [
    id 65
    label "sztajer"
  ]
  node [
    id 66
    label "drabant"
  ]
  node [
    id 67
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 68
    label "polak"
  ]
  node [
    id 69
    label "pierogi_ruskie"
  ]
  node [
    id 70
    label "krakowiak"
  ]
  node [
    id 71
    label "Polish"
  ]
  node [
    id 72
    label "j&#281;zyk"
  ]
  node [
    id 73
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 74
    label "oberek"
  ]
  node [
    id 75
    label "po_polsku"
  ]
  node [
    id 76
    label "mazur"
  ]
  node [
    id 77
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 78
    label "chodzony"
  ]
  node [
    id 79
    label "skoczny"
  ]
  node [
    id 80
    label "ryba_po_grecku"
  ]
  node [
    id 81
    label "goniony"
  ]
  node [
    id 82
    label "polsko"
  ]
  node [
    id 83
    label "cz&#322;owiek_pracy"
  ]
  node [
    id 84
    label "delegowa&#263;"
  ]
  node [
    id 85
    label "s&#322;u&#380;ba_cywilna"
  ]
  node [
    id 86
    label "pracu&#347;"
  ]
  node [
    id 87
    label "delegowanie"
  ]
  node [
    id 88
    label "r&#281;ka"
  ]
  node [
    id 89
    label "salariat"
  ]
  node [
    id 90
    label "Monika"
  ]
  node [
    id 91
    label "Brzozowska"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 24
  ]
  edge [
    source 6
    target 25
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 6
    target 29
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
  edge [
    source 6
    target 43
  ]
  edge [
    source 6
    target 44
  ]
  edge [
    source 6
    target 45
  ]
  edge [
    source 6
    target 46
  ]
  edge [
    source 6
    target 47
  ]
  edge [
    source 6
    target 48
  ]
  edge [
    source 6
    target 49
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 58
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 59
  ]
  edge [
    source 9
    target 60
  ]
  edge [
    source 9
    target 61
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 62
  ]
  edge [
    source 10
    target 63
  ]
  edge [
    source 10
    target 64
  ]
  edge [
    source 10
    target 65
  ]
  edge [
    source 10
    target 66
  ]
  edge [
    source 10
    target 67
  ]
  edge [
    source 10
    target 68
  ]
  edge [
    source 10
    target 69
  ]
  edge [
    source 10
    target 70
  ]
  edge [
    source 10
    target 71
  ]
  edge [
    source 10
    target 72
  ]
  edge [
    source 10
    target 73
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 90
    target 91
  ]
]
