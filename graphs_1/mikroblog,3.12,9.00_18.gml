graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.8823529411764706
  density 0.11764705882352941
  graphCliqueNumber 2
  node [
    id 0
    label "tak"
    origin "text"
  ]
  node [
    id 1
    label "by&#263;"
    origin "text"
  ]
  node [
    id 2
    label "rolnikszukazony"
    origin "text"
  ]
  node [
    id 3
    label "heheszki"
    origin "text"
  ]
  node [
    id 4
    label "poteznygej"
    origin "text"
  ]
  node [
    id 5
    label "humorobrazkowy"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;ga&#263;"
  ]
  node [
    id 7
    label "trwa&#263;"
  ]
  node [
    id 8
    label "obecno&#347;&#263;"
  ]
  node [
    id 9
    label "stan"
  ]
  node [
    id 10
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 11
    label "stand"
  ]
  node [
    id 12
    label "mie&#263;_miejsce"
  ]
  node [
    id 13
    label "uczestniczy&#263;"
  ]
  node [
    id 14
    label "chodzi&#263;"
  ]
  node [
    id 15
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 16
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
]
