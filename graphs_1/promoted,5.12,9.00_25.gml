graph [
  maxDegree 45
  minDegree 1
  meanDegree 1.9813084112149533
  density 0.018691588785046728
  graphCliqueNumber 2
  node [
    id 0
    label "wynagrodzenie"
    origin "text"
  ]
  node [
    id 1
    label "minimalny"
    origin "text"
  ]
  node [
    id 2
    label "wzrosn&#261;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "z&#322;oty"
    origin "text"
  ]
  node [
    id 4
    label "by&#263;"
    origin "text"
  ]
  node [
    id 5
    label "najwy&#380;szy"
    origin "text"
  ]
  node [
    id 6
    label "historia"
    origin "text"
  ]
  node [
    id 7
    label "liczenie"
  ]
  node [
    id 8
    label "return"
  ]
  node [
    id 9
    label "doch&#243;d"
  ]
  node [
    id 10
    label "zap&#322;ata"
  ]
  node [
    id 11
    label "wynagrodzenie_brutto"
  ]
  node [
    id 12
    label "ryzyko_niewygas&#322;e"
  ]
  node [
    id 13
    label "koszt_rodzajowy"
  ]
  node [
    id 14
    label "zado&#347;&#263;uczynienie"
  ]
  node [
    id 15
    label "danie"
  ]
  node [
    id 16
    label "policzenie"
  ]
  node [
    id 17
    label "policzy&#263;"
  ]
  node [
    id 18
    label "liczy&#263;"
  ]
  node [
    id 19
    label "refund"
  ]
  node [
    id 20
    label "bud&#380;et_domowy"
  ]
  node [
    id 21
    label "pay"
  ]
  node [
    id 22
    label "ordynaria"
  ]
  node [
    id 23
    label "graniczny"
  ]
  node [
    id 24
    label "minimalnie"
  ]
  node [
    id 25
    label "minimalizowanie"
  ]
  node [
    id 26
    label "zminimalizowanie"
  ]
  node [
    id 27
    label "pojawi&#263;_si&#281;"
  ]
  node [
    id 28
    label "sta&#263;_si&#281;"
  ]
  node [
    id 29
    label "zrodzi&#263;_si&#281;"
  ]
  node [
    id 30
    label "rozwin&#261;&#263;_si&#281;"
  ]
  node [
    id 31
    label "rise"
  ]
  node [
    id 32
    label "increase"
  ]
  node [
    id 33
    label "urosn&#261;&#263;"
  ]
  node [
    id 34
    label "narosn&#261;&#263;"
  ]
  node [
    id 35
    label "wzbi&#263;_si&#281;"
  ]
  node [
    id 36
    label "zrobi&#263;_si&#281;"
  ]
  node [
    id 37
    label "szlachetny"
  ]
  node [
    id 38
    label "metaliczny"
  ]
  node [
    id 39
    label "oz&#322;acanie_si&#281;"
  ]
  node [
    id 40
    label "z&#322;ocenie"
  ]
  node [
    id 41
    label "grosz"
  ]
  node [
    id 42
    label "oz&#322;ocenie_si&#281;"
  ]
  node [
    id 43
    label "utytu&#322;owany"
  ]
  node [
    id 44
    label "poz&#322;ocenie"
  ]
  node [
    id 45
    label "Polska"
  ]
  node [
    id 46
    label "&#380;&#243;&#322;ty"
  ]
  node [
    id 47
    label "wspania&#322;y"
  ]
  node [
    id 48
    label "doskona&#322;y"
  ]
  node [
    id 49
    label "kochany"
  ]
  node [
    id 50
    label "jednostka_monetarna"
  ]
  node [
    id 51
    label "z&#322;oci&#347;cie"
  ]
  node [
    id 52
    label "si&#281;ga&#263;"
  ]
  node [
    id 53
    label "trwa&#263;"
  ]
  node [
    id 54
    label "obecno&#347;&#263;"
  ]
  node [
    id 55
    label "stan"
  ]
  node [
    id 56
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 57
    label "stand"
  ]
  node [
    id 58
    label "mie&#263;_miejsce"
  ]
  node [
    id 59
    label "uczestniczy&#263;"
  ]
  node [
    id 60
    label "chodzi&#263;"
  ]
  node [
    id 61
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 62
    label "equal"
  ]
  node [
    id 63
    label "report"
  ]
  node [
    id 64
    label "rozmowy_w_Magdalence"
  ]
  node [
    id 65
    label "wypowied&#378;"
  ]
  node [
    id 66
    label "neografia"
  ]
  node [
    id 67
    label "przedmiot"
  ]
  node [
    id 68
    label "papirologia"
  ]
  node [
    id 69
    label "historia_gospodarcza"
  ]
  node [
    id 70
    label "przebiec"
  ]
  node [
    id 71
    label "hista"
  ]
  node [
    id 72
    label "nauka_humanistyczna"
  ]
  node [
    id 73
    label "filigranistyka"
  ]
  node [
    id 74
    label "dyplomatyka"
  ]
  node [
    id 75
    label "annalistyka"
  ]
  node [
    id 76
    label "historyka"
  ]
  node [
    id 77
    label "heraldyka"
  ]
  node [
    id 78
    label "fabu&#322;a"
  ]
  node [
    id 79
    label "muzealnictwo"
  ]
  node [
    id 80
    label "varsavianistyka"
  ]
  node [
    id 81
    label "prezentyzm"
  ]
  node [
    id 82
    label "mediewistyka"
  ]
  node [
    id 83
    label "przebiegni&#281;cie"
  ]
  node [
    id 84
    label "charakter"
  ]
  node [
    id 85
    label "paleografia"
  ]
  node [
    id 86
    label "genealogia"
  ]
  node [
    id 87
    label "czynno&#347;&#263;"
  ]
  node [
    id 88
    label "prozopografia"
  ]
  node [
    id 89
    label "motyw"
  ]
  node [
    id 90
    label "nautologia"
  ]
  node [
    id 91
    label "przesz&#322;o&#347;&#263;"
  ]
  node [
    id 92
    label "epoka"
  ]
  node [
    id 93
    label "numizmatyka"
  ]
  node [
    id 94
    label "ruralistyka"
  ]
  node [
    id 95
    label "epigrafika"
  ]
  node [
    id 96
    label "&#378;r&#243;d&#322;oznawstwo"
  ]
  node [
    id 97
    label "historiografia"
  ]
  node [
    id 98
    label "bizantynistyka"
  ]
  node [
    id 99
    label "weksylologia"
  ]
  node [
    id 100
    label "kierunek"
  ]
  node [
    id 101
    label "ikonografia"
  ]
  node [
    id 102
    label "chronologia"
  ]
  node [
    id 103
    label "archiwistyka"
  ]
  node [
    id 104
    label "sfragistyka"
  ]
  node [
    id 105
    label "zabytkoznawstwo"
  ]
  node [
    id 106
    label "historia_sztuki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 63
  ]
  edge [
    source 6
    target 64
  ]
  edge [
    source 6
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 6
    target 83
  ]
  edge [
    source 6
    target 84
  ]
  edge [
    source 6
    target 85
  ]
  edge [
    source 6
    target 86
  ]
  edge [
    source 6
    target 87
  ]
  edge [
    source 6
    target 88
  ]
  edge [
    source 6
    target 89
  ]
  edge [
    source 6
    target 90
  ]
  edge [
    source 6
    target 91
  ]
  edge [
    source 6
    target 92
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
]
