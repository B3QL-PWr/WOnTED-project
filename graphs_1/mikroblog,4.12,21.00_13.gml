graph [
  maxDegree 29
  minDegree 1
  meanDegree 1.9753086419753085
  density 0.024691358024691357
  graphCliqueNumber 2
  node [
    id 0
    label "kiedy"
    origin "text"
  ]
  node [
    id 1
    label "robi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ziomek"
    origin "text"
  ]
  node [
    id 3
    label "trening"
    origin "text"
  ]
  node [
    id 4
    label "si&#322;ownia"
    origin "text"
  ]
  node [
    id 5
    label "wej&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "rekord"
    origin "text"
  ]
  node [
    id 7
    label "siad"
    origin "text"
  ]
  node [
    id 8
    label "tentegowa&#263;"
  ]
  node [
    id 9
    label "urz&#261;dza&#263;"
  ]
  node [
    id 10
    label "give"
  ]
  node [
    id 11
    label "zrz&#261;dza&#263;"
  ]
  node [
    id 12
    label "czyni&#263;"
  ]
  node [
    id 13
    label "wciela&#263;_si&#281;"
  ]
  node [
    id 14
    label "post&#281;powa&#263;"
  ]
  node [
    id 15
    label "wydala&#263;"
  ]
  node [
    id 16
    label "oszukiwa&#263;"
  ]
  node [
    id 17
    label "organizowa&#263;"
  ]
  node [
    id 18
    label "na&#347;ladowa&#263;"
  ]
  node [
    id 19
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 20
    label "work"
  ]
  node [
    id 21
    label "przerabia&#263;"
  ]
  node [
    id 22
    label "stylizowa&#263;"
  ]
  node [
    id 23
    label "falowa&#263;"
  ]
  node [
    id 24
    label "act"
  ]
  node [
    id 25
    label "peddle"
  ]
  node [
    id 26
    label "ukazywa&#263;"
  ]
  node [
    id 27
    label "przypiecz&#281;towywa&#263;"
  ]
  node [
    id 28
    label "praca"
  ]
  node [
    id 29
    label "cz&#322;onek"
  ]
  node [
    id 30
    label "sw&#243;j"
  ]
  node [
    id 31
    label "ziomkostwo"
  ]
  node [
    id 32
    label "swojak"
  ]
  node [
    id 33
    label "sp&#243;&#322;ziomek"
  ]
  node [
    id 34
    label "kolega"
  ]
  node [
    id 35
    label "doskonalenie"
  ]
  node [
    id 36
    label "training"
  ]
  node [
    id 37
    label "zgrupowanie"
  ]
  node [
    id 38
    label "ruch"
  ]
  node [
    id 39
    label "&#263;wiczenie"
  ]
  node [
    id 40
    label "obw&#243;d"
  ]
  node [
    id 41
    label "warsztat"
  ]
  node [
    id 42
    label "pakowa&#263;"
  ]
  node [
    id 43
    label "kulturysta"
  ]
  node [
    id 44
    label "obiekt"
  ]
  node [
    id 45
    label "budowla"
  ]
  node [
    id 46
    label "zak&#322;ad_komunalny"
  ]
  node [
    id 47
    label "pakernia"
  ]
  node [
    id 48
    label "dosta&#263;_si&#281;"
  ]
  node [
    id 49
    label "get"
  ]
  node [
    id 50
    label "pozna&#263;"
  ]
  node [
    id 51
    label "spotka&#263;"
  ]
  node [
    id 52
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 53
    label "przenikn&#261;&#263;"
  ]
  node [
    id 54
    label "submit"
  ]
  node [
    id 55
    label "nast&#261;pi&#263;"
  ]
  node [
    id 56
    label "zag&#322;&#281;bi&#263;_si&#281;"
  ]
  node [
    id 57
    label "ascend"
  ]
  node [
    id 58
    label "intervene"
  ]
  node [
    id 59
    label "zacz&#261;&#263;"
  ]
  node [
    id 60
    label "catch"
  ]
  node [
    id 61
    label "doj&#347;&#263;"
  ]
  node [
    id 62
    label "wnikn&#261;&#263;"
  ]
  node [
    id 63
    label "przekroczy&#263;"
  ]
  node [
    id 64
    label "zaistnie&#263;"
  ]
  node [
    id 65
    label "sta&#263;_si&#281;"
  ]
  node [
    id 66
    label "wzi&#261;&#263;"
  ]
  node [
    id 67
    label "drapn&#261;&#263;_si&#281;"
  ]
  node [
    id 68
    label "z&#322;oi&#263;"
  ]
  node [
    id 69
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 70
    label "w&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 71
    label "wznie&#347;&#263;_si&#281;"
  ]
  node [
    id 72
    label "move"
  ]
  node [
    id 73
    label "become"
  ]
  node [
    id 74
    label "przy&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 75
    label "record"
  ]
  node [
    id 76
    label "osi&#261;gni&#281;cie"
  ]
  node [
    id 77
    label "baza_danych"
  ]
  node [
    id 78
    label "dane"
  ]
  node [
    id 79
    label "szczyt"
  ]
  node [
    id 80
    label "pozycja"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 5
    target 72
  ]
  edge [
    source 5
    target 73
  ]
  edge [
    source 5
    target 74
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 7
    target 80
  ]
]
