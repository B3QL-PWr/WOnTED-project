graph [
  maxDegree 16
  minDegree 1
  meanDegree 1.9411764705882353
  density 0.058823529411764705
  graphCliqueNumber 2
  node [
    id 0
    label "je&#347;li"
    origin "text"
  ]
  node [
    id 1
    label "chocia&#380;"
    origin "text"
  ]
  node [
    id 2
    label "raz"
    origin "text"
  ]
  node [
    id 3
    label "ogl&#261;da&#263;by&#263;"
    origin "text"
  ]
  node [
    id 4
    label "niemiecki"
    origin "text"
  ]
  node [
    id 5
    label "viv&#281;"
    origin "text"
  ]
  node [
    id 6
    label "ten"
    origin "text"
  ]
  node [
    id 7
    label "wpis"
    origin "text"
  ]
  node [
    id 8
    label "chwila"
  ]
  node [
    id 9
    label "uderzenie"
  ]
  node [
    id 10
    label "cios"
  ]
  node [
    id 11
    label "time"
  ]
  node [
    id 12
    label "szwabski"
  ]
  node [
    id 13
    label "po_niemiecku"
  ]
  node [
    id 14
    label "niemiec"
  ]
  node [
    id 15
    label "cenar"
  ]
  node [
    id 16
    label "j&#281;zyk"
  ]
  node [
    id 17
    label "europejski"
  ]
  node [
    id 18
    label "German"
  ]
  node [
    id 19
    label "pionier"
  ]
  node [
    id 20
    label "niemiecko"
  ]
  node [
    id 21
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 22
    label "zachodnioeuropejski"
  ]
  node [
    id 23
    label "strudel"
  ]
  node [
    id 24
    label "junkers"
  ]
  node [
    id 25
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 26
    label "okre&#347;lony"
  ]
  node [
    id 27
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 28
    label "czynno&#347;&#263;"
  ]
  node [
    id 29
    label "entrance"
  ]
  node [
    id 30
    label "inscription"
  ]
  node [
    id 31
    label "akt"
  ]
  node [
    id 32
    label "op&#322;ata"
  ]
  node [
    id 33
    label "tekst"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 4
    target 16
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 18
  ]
  edge [
    source 4
    target 19
  ]
  edge [
    source 4
    target 20
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 22
  ]
  edge [
    source 4
    target 23
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 4
    target 25
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 26
  ]
  edge [
    source 6
    target 27
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
]
