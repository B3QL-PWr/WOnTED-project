graph [
  maxDegree 23
  minDegree 1
  meanDegree 2
  density 0.017094017094017096
  graphCliqueNumber 2
  node [
    id 0
    label "dawno"
    origin "text"
  ]
  node [
    id 1
    label "temu"
    origin "text"
  ]
  node [
    id 2
    label "kiedy"
    origin "text"
  ]
  node [
    id 3
    label "niewybredny"
    origin "text"
  ]
  node [
    id 4
    label "&#380;art"
    origin "text"
  ]
  node [
    id 5
    label "niepoprawny"
    origin "text"
  ]
  node [
    id 6
    label "politycznie"
    origin "text"
  ]
  node [
    id 7
    label "podtekst"
    origin "text"
  ]
  node [
    id 8
    label "doprowadza&#263;"
    origin "text"
  ]
  node [
    id 9
    label "opinia"
    origin "text"
  ]
  node [
    id 10
    label "publiczny"
    origin "text"
  ]
  node [
    id 11
    label "szewski"
    origin "text"
  ]
  node [
    id 12
    label "pasja"
    origin "text"
  ]
  node [
    id 13
    label "lato"
    origin "text"
  ]
  node [
    id 14
    label "osiemdziesi&#261;ty"
    origin "text"
  ]
  node [
    id 15
    label "triumf"
    origin "text"
  ]
  node [
    id 16
    label "trio"
    origin "text"
  ]
  node [
    id 17
    label "znana"
    origin "text"
  ]
  node [
    id 18
    label "bran&#380;a"
    origin "text"
  ]
  node [
    id 19
    label "jako"
    origin "text"
  ]
  node [
    id 20
    label "zaz"
    origin "text"
  ]
  node [
    id 21
    label "dawny"
  ]
  node [
    id 22
    label "ongi&#347;"
  ]
  node [
    id 23
    label "dawnie"
  ]
  node [
    id 24
    label "wcze&#347;niej"
  ]
  node [
    id 25
    label "d&#322;ugotrwale"
  ]
  node [
    id 26
    label "niewybrednie"
  ]
  node [
    id 27
    label "niewymagaj&#261;cy"
  ]
  node [
    id 28
    label "pospolity"
  ]
  node [
    id 29
    label "niewymy&#347;lny"
  ]
  node [
    id 30
    label "gryps"
  ]
  node [
    id 31
    label "koncept"
  ]
  node [
    id 32
    label "spalenie"
  ]
  node [
    id 33
    label "turn"
  ]
  node [
    id 34
    label "palenie"
  ]
  node [
    id 35
    label "finfa"
  ]
  node [
    id 36
    label "g&#243;wno"
  ]
  node [
    id 37
    label "szpas"
  ]
  node [
    id 38
    label "szczeg&#243;&#322;"
  ]
  node [
    id 39
    label "furda"
  ]
  node [
    id 40
    label "opowiadanie"
  ]
  node [
    id 41
    label "pomys&#322;"
  ]
  node [
    id 42
    label "dokazywanie"
  ]
  node [
    id 43
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 44
    label "anecdote"
  ]
  node [
    id 45
    label "banalny"
  ]
  node [
    id 46
    label "czyn"
  ]
  node [
    id 47
    label "raptularz"
  ]
  node [
    id 48
    label "cyrk"
  ]
  node [
    id 49
    label "humor"
  ]
  node [
    id 50
    label "sofcik"
  ]
  node [
    id 51
    label "niepoprawnie"
  ]
  node [
    id 52
    label "nieuleczalny"
  ]
  node [
    id 53
    label "nies&#322;uszny"
  ]
  node [
    id 54
    label "z&#322;y"
  ]
  node [
    id 55
    label "niestosowny"
  ]
  node [
    id 56
    label "nieprawid&#322;owo"
  ]
  node [
    id 57
    label "polityczny"
  ]
  node [
    id 58
    label "znaczenie"
  ]
  node [
    id 59
    label "wykonywa&#263;"
  ]
  node [
    id 60
    label "moderate"
  ]
  node [
    id 61
    label "wprowadza&#263;"
  ]
  node [
    id 62
    label "wzbudza&#263;"
  ]
  node [
    id 63
    label "deliver"
  ]
  node [
    id 64
    label "rig"
  ]
  node [
    id 65
    label "message"
  ]
  node [
    id 66
    label "powodowa&#263;"
  ]
  node [
    id 67
    label "prowadzi&#263;"
  ]
  node [
    id 68
    label "dokument"
  ]
  node [
    id 69
    label "rehabilitowanie_si&#281;"
  ]
  node [
    id 70
    label "reputacja"
  ]
  node [
    id 71
    label "prowadzi&#263;_si&#281;"
  ]
  node [
    id 72
    label "rehabilitowa&#263;_si&#281;"
  ]
  node [
    id 73
    label "cecha"
  ]
  node [
    id 74
    label "zrehabilitowanie_si&#281;"
  ]
  node [
    id 75
    label "informacja"
  ]
  node [
    id 76
    label "appraisal"
  ]
  node [
    id 77
    label "ekspertyza"
  ]
  node [
    id 78
    label "zrehabilitowa&#263;_si&#281;"
  ]
  node [
    id 79
    label "pogl&#261;d"
  ]
  node [
    id 80
    label "kryterium"
  ]
  node [
    id 81
    label "wielko&#347;&#263;"
  ]
  node [
    id 82
    label "jawny"
  ]
  node [
    id 83
    label "upublicznienie"
  ]
  node [
    id 84
    label "upublicznianie"
  ]
  node [
    id 85
    label "publicznie"
  ]
  node [
    id 86
    label "specjalny"
  ]
  node [
    id 87
    label "tendency"
  ]
  node [
    id 88
    label "nabo&#380;e&#324;stwo"
  ]
  node [
    id 89
    label "nami&#281;tny"
  ]
  node [
    id 90
    label "Jezus_Chrystus"
  ]
  node [
    id 91
    label "z&#322;o&#347;&#263;"
  ]
  node [
    id 92
    label "feblik"
  ]
  node [
    id 93
    label "lunacy"
  ]
  node [
    id 94
    label "dzie&#322;o"
  ]
  node [
    id 95
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 96
    label "dedication"
  ]
  node [
    id 97
    label "fondness"
  ]
  node [
    id 98
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 99
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 100
    label "utw&#243;r"
  ]
  node [
    id 101
    label "pie&#347;&#324;_pasyjna"
  ]
  node [
    id 102
    label "kurwica"
  ]
  node [
    id 103
    label "zaj&#281;cie"
  ]
  node [
    id 104
    label "zajawka"
  ]
  node [
    id 105
    label "pora_roku"
  ]
  node [
    id 106
    label "puchar"
  ]
  node [
    id 107
    label "sukces"
  ]
  node [
    id 108
    label "conquest"
  ]
  node [
    id 109
    label "partia"
  ]
  node [
    id 110
    label "zesp&#243;&#322;"
  ]
  node [
    id 111
    label "tr&#243;jka"
  ]
  node [
    id 112
    label "dziedzina"
  ]
  node [
    id 113
    label "David"
  ]
  node [
    id 114
    label "Zucker"
  ]
  node [
    id 115
    label "Jim"
  ]
  node [
    id 116
    label "Abrahams"
  ]
  node [
    id 117
    label "Jerry"
  ]
  edge [
    source 0
    target 0
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 59
  ]
  edge [
    source 8
    target 60
  ]
  edge [
    source 8
    target 61
  ]
  edge [
    source 8
    target 62
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 50
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 105
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 106
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 109
  ]
  edge [
    source 16
    target 110
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 16
    target 111
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 112
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 113
    target 114
  ]
  edge [
    source 114
    target 117
  ]
  edge [
    source 115
    target 116
  ]
]
