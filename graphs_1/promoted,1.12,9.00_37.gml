graph [
  maxDegree 19
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.04242424242424243
  graphCliqueNumber 2
  node [
    id 0
    label "amerykanin"
    origin "text"
  ]
  node [
    id 1
    label "coraz"
    origin "text"
  ]
  node [
    id 2
    label "bardziej"
    origin "text"
  ]
  node [
    id 3
    label "u&#347;wiadamia&#263;"
    origin "text"
  ]
  node [
    id 4
    label "siebie"
    origin "text"
  ]
  node [
    id 5
    label "okienko"
    origin "text"
  ]
  node [
    id 6
    label "si&#281;"
    origin "text"
  ]
  node [
    id 7
    label "zamyka&#263;"
    origin "text"
  ]
  node [
    id 8
    label "explain"
  ]
  node [
    id 9
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 10
    label "informowa&#263;"
  ]
  node [
    id 11
    label "czas_wolny"
  ]
  node [
    id 12
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 13
    label "wype&#322;nienie"
  ]
  node [
    id 14
    label "otw&#243;r"
  ]
  node [
    id 15
    label "poczta"
  ]
  node [
    id 16
    label "wype&#322;nianie"
  ]
  node [
    id 17
    label "ekran"
  ]
  node [
    id 18
    label "urz&#261;d"
  ]
  node [
    id 19
    label "rubryka"
  ]
  node [
    id 20
    label "program"
  ]
  node [
    id 21
    label "interfejs"
  ]
  node [
    id 22
    label "menad&#380;er_okien"
  ]
  node [
    id 23
    label "pulpit"
  ]
  node [
    id 24
    label "tabela"
  ]
  node [
    id 25
    label "ram&#243;wka"
  ]
  node [
    id 26
    label "pozycja"
  ]
  node [
    id 27
    label "okno"
  ]
  node [
    id 28
    label "ko&#324;czy&#263;"
  ]
  node [
    id 29
    label "ukrywa&#263;"
  ]
  node [
    id 30
    label "zawiera&#263;"
  ]
  node [
    id 31
    label "sk&#322;ada&#263;"
  ]
  node [
    id 32
    label "suspend"
  ]
  node [
    id 33
    label "unieruchamia&#263;"
  ]
  node [
    id 34
    label "umieszcza&#263;"
  ]
  node [
    id 35
    label "rozwi&#261;zywa&#263;"
  ]
  node [
    id 36
    label "instytucja"
  ]
  node [
    id 37
    label "blokowa&#263;"
  ]
  node [
    id 38
    label "ujmowa&#263;"
  ]
  node [
    id 39
    label "exsert"
  ]
  node [
    id 40
    label "lock"
  ]
  node [
    id 41
    label "bogdan"
  ]
  node [
    id 42
    label "g&#243;ralczyk"
  ]
  node [
    id 43
    label "Grzegorz"
  ]
  node [
    id 44
    label "Sroczy&#324;ski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 11
  ]
  edge [
    source 5
    target 12
  ]
  edge [
    source 5
    target 13
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 5
    target 15
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 28
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 43
    target 44
  ]
]
