graph [
  maxDegree 48
  minDegree 1
  meanDegree 1.9441340782122905
  density 0.010922101562990396
  graphCliqueNumber 4
  node [
    id 0
    label "g&#322;os"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "pan"
    origin "text"
  ]
  node [
    id 3
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 4
    label "piotr"
    origin "text"
  ]
  node [
    id 5
    label "babinetz"
    origin "text"
  ]
  node [
    id 6
    label "prawo"
    origin "text"
  ]
  node [
    id 7
    label "sprawiedliwo&#347;&#263;"
    origin "text"
  ]
  node [
    id 8
    label "opinion"
  ]
  node [
    id 9
    label "wypowied&#378;"
  ]
  node [
    id 10
    label "zmatowienie"
  ]
  node [
    id 11
    label "wpa&#347;&#263;"
  ]
  node [
    id 12
    label "grupa"
  ]
  node [
    id 13
    label "wokal"
  ]
  node [
    id 14
    label "note"
  ]
  node [
    id 15
    label "wydawa&#263;"
  ]
  node [
    id 16
    label "nakaz"
  ]
  node [
    id 17
    label "regestr"
  ]
  node [
    id 18
    label "&#347;piewak_operowy"
  ]
  node [
    id 19
    label "matowie&#263;"
  ]
  node [
    id 20
    label "wpada&#263;"
  ]
  node [
    id 21
    label "stanowisko"
  ]
  node [
    id 22
    label "zjawisko"
  ]
  node [
    id 23
    label "mutacja"
  ]
  node [
    id 24
    label "partia"
  ]
  node [
    id 25
    label "&#347;piewak"
  ]
  node [
    id 26
    label "emisja"
  ]
  node [
    id 27
    label "brzmienie"
  ]
  node [
    id 28
    label "zmatowie&#263;"
  ]
  node [
    id 29
    label "wydanie"
  ]
  node [
    id 30
    label "zesp&#243;&#322;"
  ]
  node [
    id 31
    label "wyda&#263;"
  ]
  node [
    id 32
    label "zdolno&#347;&#263;"
  ]
  node [
    id 33
    label "decyzja"
  ]
  node [
    id 34
    label "wpadni&#281;cie"
  ]
  node [
    id 35
    label "linia_melodyczna"
  ]
  node [
    id 36
    label "wpadanie"
  ]
  node [
    id 37
    label "onomatopeja"
  ]
  node [
    id 38
    label "sound"
  ]
  node [
    id 39
    label "matowienie"
  ]
  node [
    id 40
    label "ch&#243;rzysta"
  ]
  node [
    id 41
    label "d&#378;wi&#281;k"
  ]
  node [
    id 42
    label "foniatra"
  ]
  node [
    id 43
    label "&#347;piewaczka"
  ]
  node [
    id 44
    label "czyj&#347;"
  ]
  node [
    id 45
    label "m&#261;&#380;"
  ]
  node [
    id 46
    label "cz&#322;owiek"
  ]
  node [
    id 47
    label "profesor"
  ]
  node [
    id 48
    label "kszta&#322;ciciel"
  ]
  node [
    id 49
    label "jegomo&#347;&#263;"
  ]
  node [
    id 50
    label "zwrot"
  ]
  node [
    id 51
    label "pracodawca"
  ]
  node [
    id 52
    label "rz&#261;dzenie"
  ]
  node [
    id 53
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 54
    label "ch&#322;opina"
  ]
  node [
    id 55
    label "bratek"
  ]
  node [
    id 56
    label "opiekun"
  ]
  node [
    id 57
    label "doros&#322;y"
  ]
  node [
    id 58
    label "preceptor"
  ]
  node [
    id 59
    label "Midas"
  ]
  node [
    id 60
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 61
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 62
    label "murza"
  ]
  node [
    id 63
    label "ojciec"
  ]
  node [
    id 64
    label "androlog"
  ]
  node [
    id 65
    label "pupil"
  ]
  node [
    id 66
    label "efendi"
  ]
  node [
    id 67
    label "nabab"
  ]
  node [
    id 68
    label "w&#322;odarz"
  ]
  node [
    id 69
    label "szkolnik"
  ]
  node [
    id 70
    label "pedagog"
  ]
  node [
    id 71
    label "popularyzator"
  ]
  node [
    id 72
    label "andropauza"
  ]
  node [
    id 73
    label "gra_w_karty"
  ]
  node [
    id 74
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 75
    label "Mieszko_I"
  ]
  node [
    id 76
    label "bogaty"
  ]
  node [
    id 77
    label "samiec"
  ]
  node [
    id 78
    label "przyw&#243;dca"
  ]
  node [
    id 79
    label "pa&#324;stwo"
  ]
  node [
    id 80
    label "belfer"
  ]
  node [
    id 81
    label "dyplomata"
  ]
  node [
    id 82
    label "wys&#322;annik"
  ]
  node [
    id 83
    label "przedstawiciel"
  ]
  node [
    id 84
    label "kurier_dyplomatyczny"
  ]
  node [
    id 85
    label "ablegat"
  ]
  node [
    id 86
    label "klubista"
  ]
  node [
    id 87
    label "Miko&#322;ajczyk"
  ]
  node [
    id 88
    label "Korwin"
  ]
  node [
    id 89
    label "parlamentarzysta"
  ]
  node [
    id 90
    label "dyscyplina_partyjna"
  ]
  node [
    id 91
    label "izba_ni&#380;sza"
  ]
  node [
    id 92
    label "poselstwo"
  ]
  node [
    id 93
    label "obserwacja"
  ]
  node [
    id 94
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 95
    label "nauka_prawa"
  ]
  node [
    id 96
    label "dominion"
  ]
  node [
    id 97
    label "normatywizm"
  ]
  node [
    id 98
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 99
    label "qualification"
  ]
  node [
    id 100
    label "opis"
  ]
  node [
    id 101
    label "regu&#322;a_Allena"
  ]
  node [
    id 102
    label "normalizacja"
  ]
  node [
    id 103
    label "kazuistyka"
  ]
  node [
    id 104
    label "regu&#322;a_Glogera"
  ]
  node [
    id 105
    label "kultura_duchowa"
  ]
  node [
    id 106
    label "prawo_karne"
  ]
  node [
    id 107
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 108
    label "standard"
  ]
  node [
    id 109
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 110
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 111
    label "struktura"
  ]
  node [
    id 112
    label "szko&#322;a"
  ]
  node [
    id 113
    label "prawo_karne_procesowe"
  ]
  node [
    id 114
    label "prawo_Mendla"
  ]
  node [
    id 115
    label "przepis"
  ]
  node [
    id 116
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 117
    label "criterion"
  ]
  node [
    id 118
    label "kanonistyka"
  ]
  node [
    id 119
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 120
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 121
    label "wykonawczy"
  ]
  node [
    id 122
    label "twierdzenie"
  ]
  node [
    id 123
    label "judykatura"
  ]
  node [
    id 124
    label "legislacyjnie"
  ]
  node [
    id 125
    label "umocowa&#263;"
  ]
  node [
    id 126
    label "podmiot"
  ]
  node [
    id 127
    label "procesualistyka"
  ]
  node [
    id 128
    label "kierunek"
  ]
  node [
    id 129
    label "kryminologia"
  ]
  node [
    id 130
    label "kryminalistyka"
  ]
  node [
    id 131
    label "cywilistyka"
  ]
  node [
    id 132
    label "law"
  ]
  node [
    id 133
    label "zasada_d'Alemberta"
  ]
  node [
    id 134
    label "jurisprudence"
  ]
  node [
    id 135
    label "zasada"
  ]
  node [
    id 136
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 137
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 138
    label "konsekwencja"
  ]
  node [
    id 139
    label "punishment"
  ]
  node [
    id 140
    label "cecha"
  ]
  node [
    id 141
    label "roboty_przymusowe"
  ]
  node [
    id 142
    label "nemezis"
  ]
  node [
    id 143
    label "righteousness"
  ]
  node [
    id 144
    label "Piotr"
  ]
  node [
    id 145
    label "Babinetz"
  ]
  node [
    id 146
    label "i"
  ]
  node [
    id 147
    label "Mariusz"
  ]
  node [
    id 148
    label "Kami&#324;ski"
  ]
  node [
    id 149
    label "liga"
  ]
  node [
    id 150
    label "republika&#324;ski"
  ]
  node [
    id 151
    label "unia"
  ]
  node [
    id 152
    label "wolno&#347;&#263;"
  ]
  node [
    id 153
    label "konstytucja"
  ]
  node [
    id 154
    label "kwietniowy"
  ]
  node [
    id 155
    label "rz&#261;d"
  ]
  node [
    id 156
    label "polski"
  ]
  node [
    id 157
    label "na"
  ]
  node [
    id 158
    label "uchod&#378;stwo"
  ]
  node [
    id 159
    label "urz&#261;d"
  ]
  node [
    id 160
    label "bezpiecze&#324;stwo"
  ]
  node [
    id 161
    label "informacja"
  ]
  node [
    id 162
    label "wojskowy"
  ]
  node [
    id 163
    label "s&#322;u&#380;ba"
  ]
  node [
    id 164
    label "wewn&#281;trzny"
  ]
  node [
    id 165
    label "zwi&#261;zkowy"
  ]
  node [
    id 166
    label "sowiecki"
  ]
  node [
    id 167
    label "zwi&#261;zek"
  ]
  node [
    id 168
    label "platforma"
  ]
  node [
    id 169
    label "obywatelski"
  ]
  node [
    id 170
    label "J&#243;zefa"
  ]
  node [
    id 171
    label "Franczaka"
  ]
  node [
    id 172
    label "Jerzy"
  ]
  node [
    id 173
    label "Szmajdzi&#324;ski"
  ]
  node [
    id 174
    label "Franciszka"
  ]
  node [
    id 175
    label "Stefaniuk"
  ]
  node [
    id 176
    label "polskie"
  ]
  node [
    id 177
    label "stronnictwo"
  ]
  node [
    id 178
    label "ludowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 2
    target 48
  ]
  edge [
    source 2
    target 49
  ]
  edge [
    source 2
    target 50
  ]
  edge [
    source 2
    target 51
  ]
  edge [
    source 2
    target 52
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 53
  ]
  edge [
    source 2
    target 54
  ]
  edge [
    source 2
    target 55
  ]
  edge [
    source 2
    target 56
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 58
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 81
  ]
  edge [
    source 3
    target 82
  ]
  edge [
    source 3
    target 83
  ]
  edge [
    source 3
    target 84
  ]
  edge [
    source 3
    target 85
  ]
  edge [
    source 3
    target 86
  ]
  edge [
    source 3
    target 87
  ]
  edge [
    source 3
    target 88
  ]
  edge [
    source 3
    target 89
  ]
  edge [
    source 3
    target 90
  ]
  edge [
    source 3
    target 91
  ]
  edge [
    source 3
    target 92
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 93
  ]
  edge [
    source 6
    target 94
  ]
  edge [
    source 6
    target 95
  ]
  edge [
    source 6
    target 96
  ]
  edge [
    source 6
    target 97
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 6
    target 102
  ]
  edge [
    source 6
    target 103
  ]
  edge [
    source 6
    target 104
  ]
  edge [
    source 6
    target 105
  ]
  edge [
    source 6
    target 106
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 6
    target 110
  ]
  edge [
    source 6
    target 111
  ]
  edge [
    source 6
    target 112
  ]
  edge [
    source 6
    target 113
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 6
    target 118
  ]
  edge [
    source 6
    target 119
  ]
  edge [
    source 6
    target 120
  ]
  edge [
    source 6
    target 121
  ]
  edge [
    source 6
    target 122
  ]
  edge [
    source 6
    target 123
  ]
  edge [
    source 6
    target 124
  ]
  edge [
    source 6
    target 125
  ]
  edge [
    source 6
    target 126
  ]
  edge [
    source 6
    target 127
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 146
  ]
  edge [
    source 7
    target 138
  ]
  edge [
    source 7
    target 139
  ]
  edge [
    source 7
    target 140
  ]
  edge [
    source 7
    target 141
  ]
  edge [
    source 7
    target 142
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 146
  ]
  edge [
    source 144
    target 145
  ]
  edge [
    source 147
    target 148
  ]
  edge [
    source 149
    target 150
  ]
  edge [
    source 151
    target 152
  ]
  edge [
    source 153
    target 154
  ]
  edge [
    source 155
    target 156
  ]
  edge [
    source 155
    target 157
  ]
  edge [
    source 155
    target 158
  ]
  edge [
    source 156
    target 157
  ]
  edge [
    source 156
    target 158
  ]
  edge [
    source 157
    target 158
  ]
  edge [
    source 159
    target 160
  ]
  edge [
    source 160
    target 163
  ]
  edge [
    source 161
    target 162
  ]
  edge [
    source 162
    target 163
  ]
  edge [
    source 162
    target 164
  ]
  edge [
    source 163
    target 164
  ]
  edge [
    source 165
    target 166
  ]
  edge [
    source 166
    target 167
  ]
  edge [
    source 168
    target 169
  ]
  edge [
    source 170
    target 171
  ]
  edge [
    source 172
    target 173
  ]
  edge [
    source 172
    target 174
  ]
  edge [
    source 172
    target 175
  ]
  edge [
    source 174
    target 175
  ]
  edge [
    source 176
    target 177
  ]
  edge [
    source 176
    target 178
  ]
  edge [
    source 177
    target 178
  ]
]
