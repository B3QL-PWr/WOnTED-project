graph [
  maxDegree 20
  minDegree 1
  meanDegree 1.9692307692307693
  density 0.03076923076923077
  graphCliqueNumber 2
  node [
    id 0
    label "formu&#322;a"
    origin "text"
  ]
  node [
    id 1
    label "pokaza&#263;"
    origin "text"
  ]
  node [
    id 2
    label "prawdopodobie&#324;stwo"
    origin "text"
  ]
  node [
    id 3
    label "wyprzedzenie"
    origin "text"
  ]
  node [
    id 4
    label "nowa"
    origin "text"
  ]
  node [
    id 5
    label "grafika"
    origin "text"
  ]
  node [
    id 6
    label "telewizyjny"
    origin "text"
  ]
  node [
    id 7
    label "sezon"
    origin "text"
  ]
  node [
    id 8
    label "rule"
  ]
  node [
    id 9
    label "obyczajowo&#347;&#263;"
  ]
  node [
    id 10
    label "zapis"
  ]
  node [
    id 11
    label "formularz"
  ]
  node [
    id 12
    label "sformu&#322;owanie"
  ]
  node [
    id 13
    label "kultura"
  ]
  node [
    id 14
    label "kultura_duchowa"
  ]
  node [
    id 15
    label "ceremony"
  ]
  node [
    id 16
    label "wz&#243;r_Wilcoxa"
  ]
  node [
    id 17
    label "spowodowa&#263;"
  ]
  node [
    id 18
    label "wyrazi&#263;"
  ]
  node [
    id 19
    label "da&#263;_w_ko&#347;&#263;"
  ]
  node [
    id 20
    label "przedstawi&#263;"
  ]
  node [
    id 21
    label "testify"
  ]
  node [
    id 22
    label "indicate"
  ]
  node [
    id 23
    label "przeszkoli&#263;"
  ]
  node [
    id 24
    label "udowodni&#263;"
  ]
  node [
    id 25
    label "poinformowa&#263;"
  ]
  node [
    id 26
    label "poda&#263;"
  ]
  node [
    id 27
    label "point"
  ]
  node [
    id 28
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 29
    label "poj&#281;cie"
  ]
  node [
    id 30
    label "zrobienie"
  ]
  node [
    id 31
    label "przemieszczenie_si&#281;"
  ]
  node [
    id 32
    label "advance"
  ]
  node [
    id 33
    label "przygotowanie"
  ]
  node [
    id 34
    label "warunki"
  ]
  node [
    id 35
    label "progress"
  ]
  node [
    id 36
    label "gwiazda"
  ]
  node [
    id 37
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 38
    label "wygl&#261;d"
  ]
  node [
    id 39
    label "rysunek"
  ]
  node [
    id 40
    label "rytownictwo"
  ]
  node [
    id 41
    label "graphics"
  ]
  node [
    id 42
    label "persona"
  ]
  node [
    id 43
    label "wydawnictwo"
  ]
  node [
    id 44
    label "ilustracja"
  ]
  node [
    id 45
    label "plastyka"
  ]
  node [
    id 46
    label "wektoryzacja"
  ]
  node [
    id 47
    label "picture"
  ]
  node [
    id 48
    label "liternictwo"
  ]
  node [
    id 49
    label "illustration"
  ]
  node [
    id 50
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 51
    label "sztycharstwo"
  ]
  node [
    id 52
    label "dziedzina_informatyki"
  ]
  node [
    id 53
    label "spos&#243;b"
  ]
  node [
    id 54
    label "styl"
  ]
  node [
    id 55
    label "typografia"
  ]
  node [
    id 56
    label "specjalny"
  ]
  node [
    id 57
    label "medialny"
  ]
  node [
    id 58
    label "telewizyjnie"
  ]
  node [
    id 59
    label "season"
  ]
  node [
    id 60
    label "serial"
  ]
  node [
    id 61
    label "seria"
  ]
  node [
    id 62
    label "czas"
  ]
  node [
    id 63
    label "rok"
  ]
  node [
    id 64
    label "1"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
]
