graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9130434782608696
  density 0.08695652173913043
  graphCliqueNumber 2
  node [
    id 0
    label "wysoce"
    origin "text"
  ]
  node [
    id 1
    label "plusowany"
    origin "text"
  ]
  node [
    id 2
    label "obrazek"
    origin "text"
  ]
  node [
    id 3
    label "wygrywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "wysoki"
  ]
  node [
    id 5
    label "intensywnie"
  ]
  node [
    id 6
    label "wielki"
  ]
  node [
    id 7
    label "druk_ulotny"
  ]
  node [
    id 8
    label "opowiadanie"
  ]
  node [
    id 9
    label "rysunek"
  ]
  node [
    id 10
    label "picture"
  ]
  node [
    id 11
    label "zagwarantowywa&#263;"
  ]
  node [
    id 12
    label "mie&#263;_miejsce"
  ]
  node [
    id 13
    label "znosi&#263;"
  ]
  node [
    id 14
    label "gra&#263;"
  ]
  node [
    id 15
    label "osi&#261;ga&#263;"
  ]
  node [
    id 16
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 17
    label "robi&#263;"
  ]
  node [
    id 18
    label "strike"
  ]
  node [
    id 19
    label "instrument_muzyczny"
  ]
  node [
    id 20
    label "muzykowa&#263;"
  ]
  node [
    id 21
    label "play"
  ]
  node [
    id 22
    label "net_income"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
  edge [
    source 3
    target 13
  ]
  edge [
    source 3
    target 14
  ]
  edge [
    source 3
    target 15
  ]
  edge [
    source 3
    target 16
  ]
  edge [
    source 3
    target 17
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 3
    target 19
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
]
