graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.9130434782608696
  density 0.08695652173913043
  graphCliqueNumber 2
  node [
    id 0
    label "znalezisko"
    origin "text"
  ]
  node [
    id 1
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 2
    label "niepracuj&#261;ca"
    origin "text"
  ]
  node [
    id 3
    label "polek"
    origin "text"
  ]
  node [
    id 4
    label "okaza&#263;"
    origin "text"
  ]
  node [
    id 5
    label "si&#281;"
    origin "text"
  ]
  node [
    id 6
    label "fake"
    origin "text"
  ]
  node [
    id 7
    label "news"
    origin "text"
  ]
  node [
    id 8
    label "przedmiot"
  ]
  node [
    id 9
    label "bargain"
  ]
  node [
    id 10
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 11
    label "tycze&#263;"
  ]
  node [
    id 12
    label "pokaza&#263;"
  ]
  node [
    id 13
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 14
    label "testify"
  ]
  node [
    id 15
    label "give"
  ]
  node [
    id 16
    label "system"
  ]
  node [
    id 17
    label "nowostka"
  ]
  node [
    id 18
    label "wiadomo&#347;&#263;"
  ]
  node [
    id 19
    label "doniesienie"
  ]
  node [
    id 20
    label "informacja"
  ]
  node [
    id 21
    label "message"
  ]
  node [
    id 22
    label "nius"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 16
  ]
  edge [
    source 7
    target 17
  ]
  edge [
    source 7
    target 18
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 21
  ]
  edge [
    source 7
    target 22
  ]
]
