graph [
  maxDegree 6
  minDegree 1
  meanDegree 1.8666666666666667
  density 0.13333333333333333
  graphCliqueNumber 2
  node [
    id 0
    label "by&#263;by&#263;"
    origin "text"
  ]
  node [
    id 1
    label "szok"
    origin "text"
  ]
  node [
    id 2
    label "przyjecha&#263;by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "ukraina"
    origin "text"
  ]
  node [
    id 4
    label "d&#322;ugi"
    origin "text"
  ]
  node [
    id 5
    label "pobyt"
    origin "text"
  ]
  node [
    id 6
    label "polska"
    origin "text"
  ]
  node [
    id 7
    label "shock_absorber"
  ]
  node [
    id 8
    label "wstrz&#261;s"
  ]
  node [
    id 9
    label "zaskoczenie"
  ]
  node [
    id 10
    label "prze&#380;ycie"
  ]
  node [
    id 11
    label "daleki"
  ]
  node [
    id 12
    label "d&#322;ugo"
  ]
  node [
    id 13
    label "ruch"
  ]
  node [
    id 14
    label "obecno&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 11
  ]
  edge [
    source 4
    target 12
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 14
  ]
]
