graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.0408163265306123
  density 0.01397819401733296
  graphCliqueNumber 3
  node [
    id 0
    label "mo&#380;na"
    origin "text"
  ]
  node [
    id 1
    label "uzmys&#322;owi&#263;"
    origin "text"
  ]
  node [
    id 2
    label "siebie"
    origin "text"
  ]
  node [
    id 3
    label "jak"
    origin "text"
  ]
  node [
    id 4
    label "ruch"
    origin "text"
  ]
  node [
    id 5
    label "panowa&#263;"
    origin "text"
  ]
  node [
    id 6
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 7
    label "szlak"
    origin "text"
  ]
  node [
    id 8
    label "handlowy"
    origin "text"
  ]
  node [
    id 9
    label "te&#380;"
    origin "text"
  ]
  node [
    id 10
    label "zauwa&#380;y&#263;"
    origin "text"
  ]
  node [
    id 11
    label "rola"
    origin "text"
  ]
  node [
    id 12
    label "odgrywa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "na_przyk&#322;ad"
    origin "text"
  ]
  node [
    id 14
    label "niemcy"
    origin "text"
  ]
  node [
    id 15
    label "holandia"
    origin "text"
  ]
  node [
    id 16
    label "transport"
    origin "text"
  ]
  node [
    id 17
    label "rzeczny"
    origin "text"
  ]
  node [
    id 18
    label "free"
  ]
  node [
    id 19
    label "u&#347;wiadomi&#263;"
  ]
  node [
    id 20
    label "attest"
  ]
  node [
    id 21
    label "byd&#322;o"
  ]
  node [
    id 22
    label "zobo"
  ]
  node [
    id 23
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 24
    label "yakalo"
  ]
  node [
    id 25
    label "dzo"
  ]
  node [
    id 26
    label "manewr"
  ]
  node [
    id 27
    label "model"
  ]
  node [
    id 28
    label "movement"
  ]
  node [
    id 29
    label "apraksja"
  ]
  node [
    id 30
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 31
    label "Solidarno&#347;&#263;"
  ]
  node [
    id 32
    label "poruszenie"
  ]
  node [
    id 33
    label "commercial_enterprise"
  ]
  node [
    id 34
    label "dyssypacja_energii"
  ]
  node [
    id 35
    label "zmiana"
  ]
  node [
    id 36
    label "utrzymanie"
  ]
  node [
    id 37
    label "utrzyma&#263;"
  ]
  node [
    id 38
    label "komunikacja"
  ]
  node [
    id 39
    label "tumult"
  ]
  node [
    id 40
    label "kr&#243;tki"
  ]
  node [
    id 41
    label "drift"
  ]
  node [
    id 42
    label "utrzymywa&#263;"
  ]
  node [
    id 43
    label "stopek"
  ]
  node [
    id 44
    label "kanciasty"
  ]
  node [
    id 45
    label "d&#322;ugi"
  ]
  node [
    id 46
    label "zjawisko"
  ]
  node [
    id 47
    label "utrzymywanie"
  ]
  node [
    id 48
    label "czynno&#347;&#263;"
  ]
  node [
    id 49
    label "myk"
  ]
  node [
    id 50
    label "Czerwony_Krzy&#380;"
  ]
  node [
    id 51
    label "wydarzenie"
  ]
  node [
    id 52
    label "taktyka"
  ]
  node [
    id 53
    label "move"
  ]
  node [
    id 54
    label "natural_process"
  ]
  node [
    id 55
    label "lokomocja"
  ]
  node [
    id 56
    label "mechanika"
  ]
  node [
    id 57
    label "proces"
  ]
  node [
    id 58
    label "strumie&#324;"
  ]
  node [
    id 59
    label "aktywno&#347;&#263;"
  ]
  node [
    id 60
    label "travel"
  ]
  node [
    id 61
    label "przewa&#380;a&#263;"
  ]
  node [
    id 62
    label "kontrolowa&#263;"
  ]
  node [
    id 63
    label "control"
  ]
  node [
    id 64
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 65
    label "kierowa&#263;"
  ]
  node [
    id 66
    label "manipulate"
  ]
  node [
    id 67
    label "dominowa&#263;"
  ]
  node [
    id 68
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 69
    label "istnie&#263;"
  ]
  node [
    id 70
    label "silny"
  ]
  node [
    id 71
    label "wa&#380;nie"
  ]
  node [
    id 72
    label "eksponowany"
  ]
  node [
    id 73
    label "istotnie"
  ]
  node [
    id 74
    label "znaczny"
  ]
  node [
    id 75
    label "dobry"
  ]
  node [
    id 76
    label "wynios&#322;y"
  ]
  node [
    id 77
    label "dono&#347;ny"
  ]
  node [
    id 78
    label "w&#281;ze&#322;"
  ]
  node [
    id 79
    label "ozdoba"
  ]
  node [
    id 80
    label "droga"
  ]
  node [
    id 81
    label "infrastruktura"
  ]
  node [
    id 82
    label "wz&#243;r"
  ]
  node [
    id 83
    label "crisscross"
  ]
  node [
    id 84
    label "handlowo"
  ]
  node [
    id 85
    label "zobaczy&#263;"
  ]
  node [
    id 86
    label "zwr&#243;ci&#263;_uwag&#281;"
  ]
  node [
    id 87
    label "notice"
  ]
  node [
    id 88
    label "cognizance"
  ]
  node [
    id 89
    label "pole"
  ]
  node [
    id 90
    label "znaczenie"
  ]
  node [
    id 91
    label "ziemia"
  ]
  node [
    id 92
    label "sk&#322;ad"
  ]
  node [
    id 93
    label "zastosowanie"
  ]
  node [
    id 94
    label "zreinterpretowa&#263;"
  ]
  node [
    id 95
    label "zreinterpretowanie"
  ]
  node [
    id 96
    label "function"
  ]
  node [
    id 97
    label "zagranie"
  ]
  node [
    id 98
    label "p&#322;osa"
  ]
  node [
    id 99
    label "plik"
  ]
  node [
    id 100
    label "cel"
  ]
  node [
    id 101
    label "reinterpretowanie"
  ]
  node [
    id 102
    label "tekst"
  ]
  node [
    id 103
    label "wykonywa&#263;"
  ]
  node [
    id 104
    label "uprawi&#263;"
  ]
  node [
    id 105
    label "uprawienie"
  ]
  node [
    id 106
    label "gra&#263;"
  ]
  node [
    id 107
    label "radlina"
  ]
  node [
    id 108
    label "ustawi&#263;"
  ]
  node [
    id 109
    label "irygowa&#263;"
  ]
  node [
    id 110
    label "wrench"
  ]
  node [
    id 111
    label "irygowanie"
  ]
  node [
    id 112
    label "dialog"
  ]
  node [
    id 113
    label "zagon"
  ]
  node [
    id 114
    label "scenariusz"
  ]
  node [
    id 115
    label "zagra&#263;"
  ]
  node [
    id 116
    label "kszta&#322;t"
  ]
  node [
    id 117
    label "pierwszoplanowo&#347;&#263;"
  ]
  node [
    id 118
    label "ustawienie"
  ]
  node [
    id 119
    label "czyn"
  ]
  node [
    id 120
    label "gospodarstwo"
  ]
  node [
    id 121
    label "reinterpretowa&#263;"
  ]
  node [
    id 122
    label "granie"
  ]
  node [
    id 123
    label "wykonywanie"
  ]
  node [
    id 124
    label "aktorstwo"
  ]
  node [
    id 125
    label "kostium"
  ]
  node [
    id 126
    label "posta&#263;"
  ]
  node [
    id 127
    label "prezentowa&#263;"
  ]
  node [
    id 128
    label "deal"
  ]
  node [
    id 129
    label "play"
  ]
  node [
    id 130
    label "rozgrywa&#263;"
  ]
  node [
    id 131
    label "uk&#322;ad_komunikacyjny"
  ]
  node [
    id 132
    label "zawarto&#347;&#263;"
  ]
  node [
    id 133
    label "unos"
  ]
  node [
    id 134
    label "traffic"
  ]
  node [
    id 135
    label "za&#322;adunek"
  ]
  node [
    id 136
    label "gospodarka"
  ]
  node [
    id 137
    label "roz&#322;adunek"
  ]
  node [
    id 138
    label "grupa"
  ]
  node [
    id 139
    label "sprz&#281;t"
  ]
  node [
    id 140
    label "jednoszynowy"
  ]
  node [
    id 141
    label "cedu&#322;a"
  ]
  node [
    id 142
    label "tyfon"
  ]
  node [
    id 143
    label "us&#322;uga"
  ]
  node [
    id 144
    label "prze&#322;adunek"
  ]
  node [
    id 145
    label "towar"
  ]
  node [
    id 146
    label "wodny"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 26
  ]
  edge [
    source 4
    target 27
  ]
  edge [
    source 4
    target 28
  ]
  edge [
    source 4
    target 29
  ]
  edge [
    source 4
    target 30
  ]
  edge [
    source 4
    target 31
  ]
  edge [
    source 4
    target 32
  ]
  edge [
    source 4
    target 33
  ]
  edge [
    source 4
    target 34
  ]
  edge [
    source 4
    target 35
  ]
  edge [
    source 4
    target 36
  ]
  edge [
    source 4
    target 37
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 14
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 11
    target 91
  ]
  edge [
    source 11
    target 92
  ]
  edge [
    source 11
    target 93
  ]
  edge [
    source 11
    target 94
  ]
  edge [
    source 11
    target 95
  ]
  edge [
    source 11
    target 96
  ]
  edge [
    source 11
    target 97
  ]
  edge [
    source 11
    target 98
  ]
  edge [
    source 11
    target 99
  ]
  edge [
    source 11
    target 100
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 127
  ]
  edge [
    source 12
    target 128
  ]
  edge [
    source 12
    target 129
  ]
  edge [
    source 12
    target 130
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 131
  ]
  edge [
    source 16
    target 132
  ]
  edge [
    source 16
    target 133
  ]
  edge [
    source 16
    target 134
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 16
    target 138
  ]
  edge [
    source 16
    target 139
  ]
  edge [
    source 16
    target 140
  ]
  edge [
    source 16
    target 141
  ]
  edge [
    source 16
    target 142
  ]
  edge [
    source 16
    target 143
  ]
  edge [
    source 16
    target 144
  ]
  edge [
    source 16
    target 38
  ]
  edge [
    source 16
    target 145
  ]
  edge [
    source 17
    target 146
  ]
]
