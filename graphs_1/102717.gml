graph [
  maxDegree 135
  minDegree 1
  meanDegree 2.041322314049587
  density 0.008470217070745174
  graphCliqueNumber 3
  node [
    id 0
    label "filler"
    origin "text"
  ]
  node [
    id 1
    label "dobra"
    origin "text"
  ]
  node [
    id 2
    label "wiedzie&#263;"
    origin "text"
  ]
  node [
    id 3
    label "pan"
    origin "text"
  ]
  node [
    id 4
    label "tak"
    origin "text"
  ]
  node [
    id 5
    label "jeszcze"
    origin "text"
  ]
  node [
    id 6
    label "interesowa&#263;"
    origin "text"
  ]
  node [
    id 7
    label "cholera"
    origin "text"
  ]
  node [
    id 8
    label "tylko"
    origin "text"
  ]
  node [
    id 9
    label "czy"
    origin "text"
  ]
  node [
    id 10
    label "cokolwiek"
    origin "text"
  ]
  node [
    id 11
    label "tam"
    origin "text"
  ]
  node [
    id 12
    label "jecha&#263;"
    origin "text"
  ]
  node [
    id 13
    label "region"
    origin "text"
  ]
  node [
    id 14
    label "kobyla&#324;ski"
    origin "text"
  ]
  node [
    id 15
    label "korzenny"
    origin "text"
  ]
  node [
    id 16
    label "spiralna"
    origin "text"
  ]
  node [
    id 17
    label "drobna_jednostka_monetarna"
  ]
  node [
    id 18
    label "drzewo_owocowe"
  ]
  node [
    id 19
    label "frymark"
  ]
  node [
    id 20
    label "Wyspy_&#346;wi&#281;tego_Tomasza_i_Ksi&#261;&#380;&#281;ca"
  ]
  node [
    id 21
    label "ca&#322;o&#347;&#263;"
  ]
  node [
    id 22
    label "nieruchomo&#347;&#263;"
  ]
  node [
    id 23
    label "commodity"
  ]
  node [
    id 24
    label "mienie"
  ]
  node [
    id 25
    label "Wilko"
  ]
  node [
    id 26
    label "jednostka_monetarna"
  ]
  node [
    id 27
    label "centym"
  ]
  node [
    id 28
    label "cognizance"
  ]
  node [
    id 29
    label "cz&#322;owiek"
  ]
  node [
    id 30
    label "profesor"
  ]
  node [
    id 31
    label "kszta&#322;ciciel"
  ]
  node [
    id 32
    label "jegomo&#347;&#263;"
  ]
  node [
    id 33
    label "zwrot"
  ]
  node [
    id 34
    label "pracodawca"
  ]
  node [
    id 35
    label "rz&#261;dzenie"
  ]
  node [
    id 36
    label "m&#261;&#380;"
  ]
  node [
    id 37
    label "uk&#322;ad_rozrodczy_m&#281;ski"
  ]
  node [
    id 38
    label "ch&#322;opina"
  ]
  node [
    id 39
    label "bratek"
  ]
  node [
    id 40
    label "opiekun"
  ]
  node [
    id 41
    label "doros&#322;y"
  ]
  node [
    id 42
    label "preceptor"
  ]
  node [
    id 43
    label "Midas"
  ]
  node [
    id 44
    label "w&#322;a&#347;ciciel"
  ]
  node [
    id 45
    label "rz&#261;dz&#261;cy"
  ]
  node [
    id 46
    label "murza"
  ]
  node [
    id 47
    label "ojciec"
  ]
  node [
    id 48
    label "androlog"
  ]
  node [
    id 49
    label "pupil"
  ]
  node [
    id 50
    label "efendi"
  ]
  node [
    id 51
    label "nabab"
  ]
  node [
    id 52
    label "w&#322;odarz"
  ]
  node [
    id 53
    label "szkolnik"
  ]
  node [
    id 54
    label "pedagog"
  ]
  node [
    id 55
    label "popularyzator"
  ]
  node [
    id 56
    label "andropauza"
  ]
  node [
    id 57
    label "gra_w_karty"
  ]
  node [
    id 58
    label "pracownik_umys&#322;owy"
  ]
  node [
    id 59
    label "Mieszko_I"
  ]
  node [
    id 60
    label "bogaty"
  ]
  node [
    id 61
    label "samiec"
  ]
  node [
    id 62
    label "przyw&#243;dca"
  ]
  node [
    id 63
    label "pa&#324;stwo"
  ]
  node [
    id 64
    label "belfer"
  ]
  node [
    id 65
    label "ci&#261;gle"
  ]
  node [
    id 66
    label "rozciekawia&#263;"
  ]
  node [
    id 67
    label "sake"
  ]
  node [
    id 68
    label "fury"
  ]
  node [
    id 69
    label "istota_&#380;ywa"
  ]
  node [
    id 70
    label "choroba_bakteryjna"
  ]
  node [
    id 71
    label "gniew"
  ]
  node [
    id 72
    label "wyzwisko"
  ]
  node [
    id 73
    label "chor&#243;bka"
  ]
  node [
    id 74
    label "przecinkowiec_cholery"
  ]
  node [
    id 75
    label "charakternik"
  ]
  node [
    id 76
    label "choroba_zara&#378;liwa"
  ]
  node [
    id 77
    label "przekle&#324;stwo"
  ]
  node [
    id 78
    label "choroba"
  ]
  node [
    id 79
    label "holender"
  ]
  node [
    id 80
    label "skurczybyk"
  ]
  node [
    id 81
    label "cholewa"
  ]
  node [
    id 82
    label "ilo&#347;&#263;"
  ]
  node [
    id 83
    label "co&#347;"
  ]
  node [
    id 84
    label "tu"
  ]
  node [
    id 85
    label "proceed"
  ]
  node [
    id 86
    label "napada&#263;"
  ]
  node [
    id 87
    label "wyz&#322;o&#347;liwia&#263;_si&#281;"
  ]
  node [
    id 88
    label "wy&#347;miewa&#263;"
  ]
  node [
    id 89
    label "wykonywa&#263;"
  ]
  node [
    id 90
    label "udawa&#263;_si&#281;"
  ]
  node [
    id 91
    label "czu&#263;"
  ]
  node [
    id 92
    label "overdrive"
  ]
  node [
    id 93
    label "&#347;mierdzie&#263;"
  ]
  node [
    id 94
    label "ride"
  ]
  node [
    id 95
    label "korzysta&#263;"
  ]
  node [
    id 96
    label "go"
  ]
  node [
    id 97
    label "prowadzi&#263;"
  ]
  node [
    id 98
    label "continue"
  ]
  node [
    id 99
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 100
    label "drive"
  ]
  node [
    id 101
    label "kontynuowa&#263;"
  ]
  node [
    id 102
    label "rozk&#322;ad_jazdy"
  ]
  node [
    id 103
    label "odbywa&#263;"
  ]
  node [
    id 104
    label "przesuwa&#263;_si&#281;"
  ]
  node [
    id 105
    label "carry"
  ]
  node [
    id 106
    label "Skandynawia"
  ]
  node [
    id 107
    label "Yorkshire"
  ]
  node [
    id 108
    label "Kaukaz"
  ]
  node [
    id 109
    label "Kaszmir"
  ]
  node [
    id 110
    label "Podbeskidzie"
  ]
  node [
    id 111
    label "Toskania"
  ]
  node [
    id 112
    label "&#321;emkowszczyzna"
  ]
  node [
    id 113
    label "obszar"
  ]
  node [
    id 114
    label "Amhara"
  ]
  node [
    id 115
    label "Ameryka_&#346;rodkowa"
  ]
  node [
    id 116
    label "Lombardia"
  ]
  node [
    id 117
    label "okr&#281;g"
  ]
  node [
    id 118
    label "Chiny_Wschodnie"
  ]
  node [
    id 119
    label "Kalabria"
  ]
  node [
    id 120
    label "Tyrol"
  ]
  node [
    id 121
    label "Pamir"
  ]
  node [
    id 122
    label "Lubelszczyzna"
  ]
  node [
    id 123
    label "Pomorze_&#346;rodkowe"
  ]
  node [
    id 124
    label "Europa_&#346;rodkowa"
  ]
  node [
    id 125
    label "&#379;ywiecczyzna"
  ]
  node [
    id 126
    label "Europa_Wschodnia"
  ]
  node [
    id 127
    label "Europa_P&#243;&#322;nocna"
  ]
  node [
    id 128
    label "Chiny_Zachodnie"
  ]
  node [
    id 129
    label "Zabajkale"
  ]
  node [
    id 130
    label "Kaszuby"
  ]
  node [
    id 131
    label "Noworosja"
  ]
  node [
    id 132
    label "Bo&#347;nia"
  ]
  node [
    id 133
    label "Ba&#322;kany"
  ]
  node [
    id 134
    label "Zag&#322;&#281;bie_D&#261;browskie"
  ]
  node [
    id 135
    label "Anglia"
  ]
  node [
    id 136
    label "Kielecczyzna"
  ]
  node [
    id 137
    label "Krajina"
  ]
  node [
    id 138
    label "Pomorze_Zachodnie"
  ]
  node [
    id 139
    label "Opolskie"
  ]
  node [
    id 140
    label "Daleki_Wsch&#243;d"
  ]
  node [
    id 141
    label "Ko&#322;yma"
  ]
  node [
    id 142
    label "Oksytania"
  ]
  node [
    id 143
    label "country"
  ]
  node [
    id 144
    label "Syjon"
  ]
  node [
    id 145
    label "Kociewie"
  ]
  node [
    id 146
    label "Huculszczyzna"
  ]
  node [
    id 147
    label "Afryka_Po&#322;udniowa"
  ]
  node [
    id 148
    label "Azja_&#346;rodkowa"
  ]
  node [
    id 149
    label "Bawaria"
  ]
  node [
    id 150
    label "Maghreb"
  ]
  node [
    id 151
    label "Bory_Tucholskie"
  ]
  node [
    id 152
    label "Europa_Zachodnia"
  ]
  node [
    id 153
    label "Kerala"
  ]
  node [
    id 154
    label "Burgundia"
  ]
  node [
    id 155
    label "Podhale"
  ]
  node [
    id 156
    label "Kabylia"
  ]
  node [
    id 157
    label "Europa_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 158
    label "Zawo&#322;&#380;e"
  ]
  node [
    id 159
    label "Ma&#322;opolska"
  ]
  node [
    id 160
    label "Polesie"
  ]
  node [
    id 161
    label "Liguria"
  ]
  node [
    id 162
    label "&#321;&#243;dzkie"
  ]
  node [
    id 163
    label "Palestyna"
  ]
  node [
    id 164
    label "Bojkowszczyzna"
  ]
  node [
    id 165
    label "Europa_Po&#322;udniowa"
  ]
  node [
    id 166
    label "Karaiby"
  ]
  node [
    id 167
    label "S&#261;decczyzna"
  ]
  node [
    id 168
    label "Sand&#380;ak"
  ]
  node [
    id 169
    label "Nadrenia"
  ]
  node [
    id 170
    label "Pomorze_Gda&#324;skie"
  ]
  node [
    id 171
    label "Zakarpacie"
  ]
  node [
    id 172
    label "Afryka_&#346;rodkowa"
  ]
  node [
    id 173
    label "Zag&#243;rze"
  ]
  node [
    id 174
    label "Andaluzja"
  ]
  node [
    id 175
    label "&#379;mud&#378;"
  ]
  node [
    id 176
    label "Turkiestan"
  ]
  node [
    id 177
    label "Naddniestrze"
  ]
  node [
    id 178
    label "Hercegowina"
  ]
  node [
    id 179
    label "Opolszczyzna"
  ]
  node [
    id 180
    label "jednostka_administracyjna"
  ]
  node [
    id 181
    label "Lotaryngia"
  ]
  node [
    id 182
    label "Afryka_Wschodnia"
  ]
  node [
    id 183
    label "Szlezwik"
  ]
  node [
    id 184
    label "G&#243;rny_&#346;l&#261;sk"
  ]
  node [
    id 185
    label "&#346;wi&#281;tokrzyskie"
  ]
  node [
    id 186
    label "Mazowsze"
  ]
  node [
    id 187
    label "Afryka_P&#243;&#322;nocna"
  ]
  node [
    id 188
    label "Afryka_Zachodnia"
  ]
  node [
    id 189
    label "Galicja"
  ]
  node [
    id 190
    label "Szkocja"
  ]
  node [
    id 191
    label "Walia"
  ]
  node [
    id 192
    label "Powi&#347;le"
  ]
  node [
    id 193
    label "Azja_Po&#322;udniowo-Wschodnia"
  ]
  node [
    id 194
    label "Zamojszczyzna"
  ]
  node [
    id 195
    label "Bliski_Wsch&#243;d"
  ]
  node [
    id 196
    label "Kujawy"
  ]
  node [
    id 197
    label "Podlasie"
  ]
  node [
    id 198
    label "Laponia"
  ]
  node [
    id 199
    label "Umbria"
  ]
  node [
    id 200
    label "Mezoameryka"
  ]
  node [
    id 201
    label "Azja_P&#243;&#322;nocna"
  ]
  node [
    id 202
    label "&#346;l&#261;sk_Cieszy&#324;ski"
  ]
  node [
    id 203
    label "Europa_&#346;rodkowo-Wschodnia"
  ]
  node [
    id 204
    label "Kaukaz_Po&#322;udniowy"
  ]
  node [
    id 205
    label "Kraina"
  ]
  node [
    id 206
    label "Kurdystan"
  ]
  node [
    id 207
    label "Flandria"
  ]
  node [
    id 208
    label "Kampania"
  ]
  node [
    id 209
    label "Armagnac"
  ]
  node [
    id 210
    label "Polinezja"
  ]
  node [
    id 211
    label "Warmia"
  ]
  node [
    id 212
    label "Wielkopolska"
  ]
  node [
    id 213
    label "Bordeaux"
  ]
  node [
    id 214
    label "Lauda"
  ]
  node [
    id 215
    label "Mazury"
  ]
  node [
    id 216
    label "Podkarpacie"
  ]
  node [
    id 217
    label "Oceania"
  ]
  node [
    id 218
    label "Lasko"
  ]
  node [
    id 219
    label "Amazonia"
  ]
  node [
    id 220
    label "podregion"
  ]
  node [
    id 221
    label "Dolny_&#346;l&#261;sk"
  ]
  node [
    id 222
    label "Kurpie"
  ]
  node [
    id 223
    label "Tonkin"
  ]
  node [
    id 224
    label "Azja_Wschodnia"
  ]
  node [
    id 225
    label "Mikronezja"
  ]
  node [
    id 226
    label "Ukraina_Zachodnia"
  ]
  node [
    id 227
    label "Azja_Po&#322;udniowa"
  ]
  node [
    id 228
    label "subregion"
  ]
  node [
    id 229
    label "Turyngia"
  ]
  node [
    id 230
    label "Baszkiria"
  ]
  node [
    id 231
    label "Apulia"
  ]
  node [
    id 232
    label "Azja_Po&#322;udniowo-Zachodnia"
  ]
  node [
    id 233
    label "Afryka_P&#243;&#322;nocno-Wschodnia"
  ]
  node [
    id 234
    label "Indochiny"
  ]
  node [
    id 235
    label "Lubuskie"
  ]
  node [
    id 236
    label "Biskupizna"
  ]
  node [
    id 237
    label "Bia&#322;ostocczyzna"
  ]
  node [
    id 238
    label "Ameryka_&#321;aci&#324;ska"
  ]
  node [
    id 239
    label "korzennie"
  ]
  node [
    id 240
    label "aromatyczny"
  ]
  node [
    id 241
    label "ciep&#322;y"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
  edge [
    source 1
    target 22
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 6
    target 66
  ]
  edge [
    source 6
    target 67
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 68
  ]
  edge [
    source 7
    target 69
  ]
  edge [
    source 7
    target 70
  ]
  edge [
    source 7
    target 71
  ]
  edge [
    source 7
    target 72
  ]
  edge [
    source 7
    target 73
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 12
    target 85
  ]
  edge [
    source 12
    target 86
  ]
  edge [
    source 12
    target 87
  ]
  edge [
    source 12
    target 88
  ]
  edge [
    source 12
    target 89
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 106
  ]
  edge [
    source 13
    target 107
  ]
  edge [
    source 13
    target 108
  ]
  edge [
    source 13
    target 109
  ]
  edge [
    source 13
    target 110
  ]
  edge [
    source 13
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 121
  ]
  edge [
    source 13
    target 122
  ]
  edge [
    source 13
    target 123
  ]
  edge [
    source 13
    target 124
  ]
  edge [
    source 13
    target 125
  ]
  edge [
    source 13
    target 126
  ]
  edge [
    source 13
    target 127
  ]
  edge [
    source 13
    target 128
  ]
  edge [
    source 13
    target 129
  ]
  edge [
    source 13
    target 130
  ]
  edge [
    source 13
    target 131
  ]
  edge [
    source 13
    target 132
  ]
  edge [
    source 13
    target 133
  ]
  edge [
    source 13
    target 134
  ]
  edge [
    source 13
    target 135
  ]
  edge [
    source 13
    target 136
  ]
  edge [
    source 13
    target 137
  ]
  edge [
    source 13
    target 138
  ]
  edge [
    source 13
    target 139
  ]
  edge [
    source 13
    target 140
  ]
  edge [
    source 13
    target 141
  ]
  edge [
    source 13
    target 142
  ]
  edge [
    source 13
    target 143
  ]
  edge [
    source 13
    target 144
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 154
  ]
  edge [
    source 13
    target 155
  ]
  edge [
    source 13
    target 156
  ]
  edge [
    source 13
    target 157
  ]
  edge [
    source 13
    target 158
  ]
  edge [
    source 13
    target 159
  ]
  edge [
    source 13
    target 160
  ]
  edge [
    source 13
    target 161
  ]
  edge [
    source 13
    target 162
  ]
  edge [
    source 13
    target 163
  ]
  edge [
    source 13
    target 164
  ]
  edge [
    source 13
    target 165
  ]
  edge [
    source 13
    target 166
  ]
  edge [
    source 13
    target 167
  ]
  edge [
    source 13
    target 168
  ]
  edge [
    source 13
    target 169
  ]
  edge [
    source 13
    target 170
  ]
  edge [
    source 13
    target 171
  ]
  edge [
    source 13
    target 172
  ]
  edge [
    source 13
    target 173
  ]
  edge [
    source 13
    target 174
  ]
  edge [
    source 13
    target 175
  ]
  edge [
    source 13
    target 176
  ]
  edge [
    source 13
    target 177
  ]
  edge [
    source 13
    target 178
  ]
  edge [
    source 13
    target 179
  ]
  edge [
    source 13
    target 180
  ]
  edge [
    source 13
    target 181
  ]
  edge [
    source 13
    target 182
  ]
  edge [
    source 13
    target 183
  ]
  edge [
    source 13
    target 184
  ]
  edge [
    source 13
    target 185
  ]
  edge [
    source 13
    target 186
  ]
  edge [
    source 13
    target 187
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 199
  ]
  edge [
    source 13
    target 200
  ]
  edge [
    source 13
    target 201
  ]
  edge [
    source 13
    target 202
  ]
  edge [
    source 13
    target 203
  ]
  edge [
    source 13
    target 204
  ]
  edge [
    source 13
    target 205
  ]
  edge [
    source 13
    target 206
  ]
  edge [
    source 13
    target 207
  ]
  edge [
    source 13
    target 208
  ]
  edge [
    source 13
    target 209
  ]
  edge [
    source 13
    target 210
  ]
  edge [
    source 13
    target 211
  ]
  edge [
    source 13
    target 212
  ]
  edge [
    source 13
    target 213
  ]
  edge [
    source 13
    target 214
  ]
  edge [
    source 13
    target 215
  ]
  edge [
    source 13
    target 216
  ]
  edge [
    source 13
    target 217
  ]
  edge [
    source 13
    target 218
  ]
  edge [
    source 13
    target 219
  ]
  edge [
    source 13
    target 220
  ]
  edge [
    source 13
    target 221
  ]
  edge [
    source 13
    target 222
  ]
  edge [
    source 13
    target 223
  ]
  edge [
    source 13
    target 224
  ]
  edge [
    source 13
    target 225
  ]
  edge [
    source 13
    target 226
  ]
  edge [
    source 13
    target 227
  ]
  edge [
    source 13
    target 228
  ]
  edge [
    source 13
    target 229
  ]
  edge [
    source 13
    target 230
  ]
  edge [
    source 13
    target 231
  ]
  edge [
    source 13
    target 232
  ]
  edge [
    source 13
    target 233
  ]
  edge [
    source 13
    target 234
  ]
  edge [
    source 13
    target 235
  ]
  edge [
    source 13
    target 236
  ]
  edge [
    source 13
    target 237
  ]
  edge [
    source 13
    target 238
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 239
  ]
  edge [
    source 15
    target 240
  ]
  edge [
    source 15
    target 241
  ]
]
