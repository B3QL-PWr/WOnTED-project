graph [
  maxDegree 49
  minDegree 1
  meanDegree 2.162962962962963
  density 0.005353868720205354
  graphCliqueNumber 3
  node [
    id 0
    label "czas"
    origin "text"
  ]
  node [
    id 1
    label "mija&#263;"
    origin "text"
  ]
  node [
    id 2
    label "p&#281;dzi&#263;"
    origin "text"
  ]
  node [
    id 3
    label "elektron"
    origin "text"
  ]
  node [
    id 4
    label "orbita"
    origin "text"
  ]
  node [
    id 5
    label "atom"
    origin "text"
  ]
  node [
    id 6
    label "zegar"
    origin "text"
  ]
  node [
    id 7
    label "sarkofag"
    origin "text"
  ]
  node [
    id 8
    label "urodzony"
    origin "text"
  ]
  node [
    id 9
    label "jaki"
    origin "text"
  ]
  node [
    id 10
    label "zna&#263;"
    origin "text"
  ]
  node [
    id 11
    label "alienor"
    origin "text"
  ]
  node [
    id 12
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 13
    label "istnie&#263;"
    origin "text"
  ]
  node [
    id 14
    label "wielki"
    origin "text"
  ]
  node [
    id 15
    label "kap&#322;an"
    origin "text"
  ]
  node [
    id 16
    label "sto"
    origin "text"
  ]
  node [
    id 17
    label "lata"
    origin "text"
  ]
  node [
    id 18
    label "&#380;y&#322;a"
    origin "text"
  ]
  node [
    id 19
    label "osiemdziesi&#261;t"
    origin "text"
  ]
  node [
    id 20
    label "matrona"
    origin "text"
  ]
  node [
    id 21
    label "siostra"
    origin "text"
  ]
  node [
    id 22
    label "inany"
    origin "text"
  ]
  node [
    id 23
    label "pi&#281;&#263;dziesi&#261;t"
    origin "text"
  ]
  node [
    id 24
    label "nikt"
    origin "text"
  ]
  node [
    id 25
    label "koga"
    origin "text"
  ]
  node [
    id 26
    label "kocha&#263;"
    origin "text"
  ]
  node [
    id 27
    label "kima"
    origin "text"
  ]
  node [
    id 28
    label "dzieli&#263;"
    origin "text"
  ]
  node [
    id 29
    label "my&#347;l"
    origin "text"
  ]
  node [
    id 30
    label "uczucie"
    origin "text"
  ]
  node [
    id 31
    label "ani"
    origin "text"
  ]
  node [
    id 32
    label "ojciec"
    origin "text"
  ]
  node [
    id 33
    label "enmaker"
    origin "text"
  ]
  node [
    id 34
    label "ukochana"
    origin "text"
  ]
  node [
    id 35
    label "matka"
    origin "text"
  ]
  node [
    id 36
    label "rodzona"
    origin "text"
  ]
  node [
    id 37
    label "brat"
    origin "text"
  ]
  node [
    id 38
    label "pozbawi&#263;"
    origin "text"
  ]
  node [
    id 39
    label "prawo"
    origin "text"
  ]
  node [
    id 40
    label "polityk"
    origin "text"
  ]
  node [
    id 41
    label "czasokres"
  ]
  node [
    id 42
    label "trawienie"
  ]
  node [
    id 43
    label "kategoria_gramatyczna"
  ]
  node [
    id 44
    label "period"
  ]
  node [
    id 45
    label "odczyt"
  ]
  node [
    id 46
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 47
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 48
    label "chwila"
  ]
  node [
    id 49
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 50
    label "poprzedzenie"
  ]
  node [
    id 51
    label "koniugacja"
  ]
  node [
    id 52
    label "dzieje"
  ]
  node [
    id 53
    label "poprzedzi&#263;"
  ]
  node [
    id 54
    label "przep&#322;ywanie"
  ]
  node [
    id 55
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 56
    label "odwlekanie_si&#281;"
  ]
  node [
    id 57
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 58
    label "Zeitgeist"
  ]
  node [
    id 59
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 60
    label "okres_czasu"
  ]
  node [
    id 61
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 62
    label "pochodzi&#263;"
  ]
  node [
    id 63
    label "schy&#322;ek"
  ]
  node [
    id 64
    label "czwarty_wymiar"
  ]
  node [
    id 65
    label "chronometria"
  ]
  node [
    id 66
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 67
    label "poprzedzanie"
  ]
  node [
    id 68
    label "pogoda"
  ]
  node [
    id 69
    label "pochodzenie"
  ]
  node [
    id 70
    label "poprzedza&#263;"
  ]
  node [
    id 71
    label "trawi&#263;"
  ]
  node [
    id 72
    label "time_period"
  ]
  node [
    id 73
    label "rachuba_czasu"
  ]
  node [
    id 74
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 75
    label "czasoprzestrze&#324;"
  ]
  node [
    id 76
    label "laba"
  ]
  node [
    id 77
    label "omija&#263;"
  ]
  node [
    id 78
    label "trwa&#263;"
  ]
  node [
    id 79
    label "przemieszcza&#263;_si&#281;"
  ]
  node [
    id 80
    label "proceed"
  ]
  node [
    id 81
    label "przestawa&#263;"
  ]
  node [
    id 82
    label "przechodzi&#263;"
  ]
  node [
    id 83
    label "base_on_balls"
  ]
  node [
    id 84
    label "go"
  ]
  node [
    id 85
    label "produkowa&#263;"
  ]
  node [
    id 86
    label "sp&#281;dza&#263;"
  ]
  node [
    id 87
    label "lecie&#263;"
  ]
  node [
    id 88
    label "gania&#263;"
  ]
  node [
    id 89
    label "rush"
  ]
  node [
    id 90
    label "run"
  ]
  node [
    id 91
    label "pop&#281;dza&#263;"
  ]
  node [
    id 92
    label "gorzelnik"
  ]
  node [
    id 93
    label "pobudza&#263;"
  ]
  node [
    id 94
    label "meliniarz"
  ]
  node [
    id 95
    label "chyba&#263;"
  ]
  node [
    id 96
    label "zapieprza&#263;"
  ]
  node [
    id 97
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 98
    label "spieszy&#263;_si&#281;"
  ]
  node [
    id 99
    label "d&#261;&#380;y&#263;"
  ]
  node [
    id 100
    label "powodowa&#263;"
  ]
  node [
    id 101
    label "bimbrownik"
  ]
  node [
    id 102
    label "delokalizacja_elektron&#243;w"
  ]
  node [
    id 103
    label "stop"
  ]
  node [
    id 104
    label "lepton"
  ]
  node [
    id 105
    label "wi&#261;zanie_kowalencyjne"
  ]
  node [
    id 106
    label "rodnik"
  ]
  node [
    id 107
    label "gaz_Fermiego"
  ]
  node [
    id 108
    label "electron"
  ]
  node [
    id 109
    label "pow&#322;oka_elektronowa"
  ]
  node [
    id 110
    label "apogeum"
  ]
  node [
    id 111
    label "aphelium"
  ]
  node [
    id 112
    label "ruchy_planet"
  ]
  node [
    id 113
    label "w&#281;ze&#322;"
  ]
  node [
    id 114
    label "oczod&#243;&#322;"
  ]
  node [
    id 115
    label "tor"
  ]
  node [
    id 116
    label "punkt_przyziemny"
  ]
  node [
    id 117
    label "punkt_przys&#322;oneczny"
  ]
  node [
    id 118
    label "rdze&#324;_atomowy"
  ]
  node [
    id 119
    label "diadochia"
  ]
  node [
    id 120
    label "cz&#261;steczka"
  ]
  node [
    id 121
    label "masa_atomowa"
  ]
  node [
    id 122
    label "pierwiastek"
  ]
  node [
    id 123
    label "cz&#261;stka_elementarna"
  ]
  node [
    id 124
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 125
    label "j&#261;dro_atomowe"
  ]
  node [
    id 126
    label "mikrokosmos"
  ]
  node [
    id 127
    label "liczba_atomowa"
  ]
  node [
    id 128
    label "kurant"
  ]
  node [
    id 129
    label "sp&#243;&#378;nianie_si&#281;"
  ]
  node [
    id 130
    label "czasomierz"
  ]
  node [
    id 131
    label "werk"
  ]
  node [
    id 132
    label "bicie"
  ]
  node [
    id 133
    label "cyferblat"
  ]
  node [
    id 134
    label "&#347;pieszenie_si&#281;"
  ]
  node [
    id 135
    label "sp&#243;&#378;nia&#263;_si&#281;"
  ]
  node [
    id 136
    label "tyka&#263;"
  ]
  node [
    id 137
    label "godzinnik"
  ]
  node [
    id 138
    label "urz&#261;dzenie"
  ]
  node [
    id 139
    label "kotwica"
  ]
  node [
    id 140
    label "nabicie"
  ]
  node [
    id 141
    label "tykn&#261;&#263;"
  ]
  node [
    id 142
    label "wahad&#322;o"
  ]
  node [
    id 143
    label "trumna"
  ]
  node [
    id 144
    label "gr&#243;b"
  ]
  node [
    id 145
    label "nekrofag"
  ]
  node [
    id 146
    label "wysoko_urodzony"
  ]
  node [
    id 147
    label "zawo&#322;any"
  ]
  node [
    id 148
    label "wiedzie&#263;"
  ]
  node [
    id 149
    label "cognizance"
  ]
  node [
    id 150
    label "stand"
  ]
  node [
    id 151
    label "dupny"
  ]
  node [
    id 152
    label "wysoce"
  ]
  node [
    id 153
    label "wyj&#261;tkowy"
  ]
  node [
    id 154
    label "wybitny"
  ]
  node [
    id 155
    label "znaczny"
  ]
  node [
    id 156
    label "prawdziwy"
  ]
  node [
    id 157
    label "wa&#380;ny"
  ]
  node [
    id 158
    label "nieprzeci&#281;tny"
  ]
  node [
    id 159
    label "cz&#322;owiek"
  ]
  node [
    id 160
    label "zwolennik"
  ]
  node [
    id 161
    label "tuba"
  ]
  node [
    id 162
    label "chor&#261;&#380;y"
  ]
  node [
    id 163
    label "klecha"
  ]
  node [
    id 164
    label "eklezjasta"
  ]
  node [
    id 165
    label "duszpasterstwo"
  ]
  node [
    id 166
    label "rozgrzesza&#263;"
  ]
  node [
    id 167
    label "duchowny"
  ]
  node [
    id 168
    label "ksi&#281;&#380;a"
  ]
  node [
    id 169
    label "kol&#281;da"
  ]
  node [
    id 170
    label "seminarzysta"
  ]
  node [
    id 171
    label "wyznawca"
  ]
  node [
    id 172
    label "rozsiewca"
  ]
  node [
    id 173
    label "rozgrzeszanie"
  ]
  node [
    id 174
    label "popularyzator"
  ]
  node [
    id 175
    label "pasterz"
  ]
  node [
    id 176
    label "summer"
  ]
  node [
    id 177
    label "formacja_geologiczna"
  ]
  node [
    id 178
    label "vein"
  ]
  node [
    id 179
    label "chciwiec"
  ]
  node [
    id 180
    label "dost&#281;p_do&#380;ylny"
  ]
  node [
    id 181
    label "wymagaj&#261;cy"
  ]
  node [
    id 182
    label "lina"
  ]
  node [
    id 183
    label "materialista"
  ]
  node [
    id 184
    label "naczynie"
  ]
  node [
    id 185
    label "okrutnik"
  ]
  node [
    id 186
    label "sk&#261;piarz"
  ]
  node [
    id 187
    label "przew&#243;d"
  ]
  node [
    id 188
    label "sk&#261;py"
  ]
  node [
    id 189
    label "atleta"
  ]
  node [
    id 190
    label "m&#281;&#380;atka"
  ]
  node [
    id 191
    label "stateczny"
  ]
  node [
    id 192
    label "kornet"
  ]
  node [
    id 193
    label "wyznawczyni"
  ]
  node [
    id 194
    label "pigu&#322;a"
  ]
  node [
    id 195
    label "rodze&#324;stwo"
  ]
  node [
    id 196
    label "czepek"
  ]
  node [
    id 197
    label "krewna"
  ]
  node [
    id 198
    label "siostrzyca"
  ]
  node [
    id 199
    label "pingwin"
  ]
  node [
    id 200
    label "siora"
  ]
  node [
    id 201
    label "anestetysta"
  ]
  node [
    id 202
    label "miernota"
  ]
  node [
    id 203
    label "ciura"
  ]
  node [
    id 204
    label "nef"
  ]
  node [
    id 205
    label "statek_handlowy"
  ]
  node [
    id 206
    label "&#380;aglowiec"
  ]
  node [
    id 207
    label "kasztel"
  ]
  node [
    id 208
    label "hulk"
  ]
  node [
    id 209
    label "like"
  ]
  node [
    id 210
    label "czu&#263;"
  ]
  node [
    id 211
    label "chowa&#263;"
  ]
  node [
    id 212
    label "mi&#322;owa&#263;"
  ]
  node [
    id 213
    label "sen"
  ]
  node [
    id 214
    label "kszta&#322;townik"
  ]
  node [
    id 215
    label "cover"
  ]
  node [
    id 216
    label "assign"
  ]
  node [
    id 217
    label "sk&#322;&#243;ca&#263;"
  ]
  node [
    id 218
    label "wyodr&#281;bnia&#263;"
  ]
  node [
    id 219
    label "share"
  ]
  node [
    id 220
    label "divide"
  ]
  node [
    id 221
    label "sprawowa&#263;"
  ]
  node [
    id 222
    label "deal"
  ]
  node [
    id 223
    label "iloraz"
  ]
  node [
    id 224
    label "robi&#263;"
  ]
  node [
    id 225
    label "posiada&#263;"
  ]
  node [
    id 226
    label "korzysta&#263;"
  ]
  node [
    id 227
    label "liczy&#263;"
  ]
  node [
    id 228
    label "rozdawa&#263;"
  ]
  node [
    id 229
    label "digest"
  ]
  node [
    id 230
    label "system"
  ]
  node [
    id 231
    label "s&#261;d"
  ]
  node [
    id 232
    label "wytw&#243;r"
  ]
  node [
    id 233
    label "istota"
  ]
  node [
    id 234
    label "thinking"
  ]
  node [
    id 235
    label "idea"
  ]
  node [
    id 236
    label "political_orientation"
  ]
  node [
    id 237
    label "pomys&#322;"
  ]
  node [
    id 238
    label "szko&#322;a"
  ]
  node [
    id 239
    label "umys&#322;"
  ]
  node [
    id 240
    label "fantomatyka"
  ]
  node [
    id 241
    label "t&#322;oczenie_si&#281;"
  ]
  node [
    id 242
    label "p&#322;&#243;d"
  ]
  node [
    id 243
    label "zareagowanie"
  ]
  node [
    id 244
    label "wpa&#347;&#263;"
  ]
  node [
    id 245
    label "mi&#322;o&#347;&#263;"
  ]
  node [
    id 246
    label "opanowanie"
  ]
  node [
    id 247
    label "d&#322;awi&#263;"
  ]
  node [
    id 248
    label "wpada&#263;"
  ]
  node [
    id 249
    label "os&#322;upienie"
  ]
  node [
    id 250
    label "zmys&#322;"
  ]
  node [
    id 251
    label "zaanga&#380;owanie"
  ]
  node [
    id 252
    label "smell"
  ]
  node [
    id 253
    label "zdarzenie_si&#281;"
  ]
  node [
    id 254
    label "ostygn&#261;&#263;"
  ]
  node [
    id 255
    label "afekt"
  ]
  node [
    id 256
    label "stan"
  ]
  node [
    id 257
    label "iskrzy&#263;"
  ]
  node [
    id 258
    label "afekcja"
  ]
  node [
    id 259
    label "przeczulica"
  ]
  node [
    id 260
    label "ow&#322;adni&#281;ty"
  ]
  node [
    id 261
    label "czucie"
  ]
  node [
    id 262
    label "doznanie"
  ]
  node [
    id 263
    label "emocja"
  ]
  node [
    id 264
    label "ogrom"
  ]
  node [
    id 265
    label "stygn&#261;&#263;"
  ]
  node [
    id 266
    label "poczucie"
  ]
  node [
    id 267
    label "temperatura"
  ]
  node [
    id 268
    label "pomys&#322;odawca"
  ]
  node [
    id 269
    label "kszta&#322;ciciel"
  ]
  node [
    id 270
    label "tworzyciel"
  ]
  node [
    id 271
    label "ojczym"
  ]
  node [
    id 272
    label "m&#281;&#380;czyzna"
  ]
  node [
    id 273
    label "stary"
  ]
  node [
    id 274
    label "samiec"
  ]
  node [
    id 275
    label "papa"
  ]
  node [
    id 276
    label "&#347;w"
  ]
  node [
    id 277
    label "podmiot_czynno&#347;ci_tw&#243;rczych"
  ]
  node [
    id 278
    label "zakonnik"
  ]
  node [
    id 279
    label "kuwada"
  ]
  node [
    id 280
    label "przodek"
  ]
  node [
    id 281
    label "wykonawca"
  ]
  node [
    id 282
    label "rodzice"
  ]
  node [
    id 283
    label "rodzic"
  ]
  node [
    id 284
    label "ptaszyna"
  ]
  node [
    id 285
    label "umi&#322;owana"
  ]
  node [
    id 286
    label "kochanka"
  ]
  node [
    id 287
    label "kochanie"
  ]
  node [
    id 288
    label "Dulcynea"
  ]
  node [
    id 289
    label "wybranka"
  ]
  node [
    id 290
    label "Matka_Boska"
  ]
  node [
    id 291
    label "matka_zast&#281;pcza"
  ]
  node [
    id 292
    label "stara"
  ]
  node [
    id 293
    label "matczysko"
  ]
  node [
    id 294
    label "ro&#347;lina"
  ]
  node [
    id 295
    label "pi&#322;ka_palantowa"
  ]
  node [
    id 296
    label "gracz"
  ]
  node [
    id 297
    label "zawodnik"
  ]
  node [
    id 298
    label "macierz"
  ]
  node [
    id 299
    label "owad"
  ]
  node [
    id 300
    label "przyczyna"
  ]
  node [
    id 301
    label "macocha"
  ]
  node [
    id 302
    label "dwa_ognie"
  ]
  node [
    id 303
    label "staruszka"
  ]
  node [
    id 304
    label "kom&#243;rka_jajowa"
  ]
  node [
    id 305
    label "rozsadnik"
  ]
  node [
    id 306
    label "zakonnica"
  ]
  node [
    id 307
    label "obiekt"
  ]
  node [
    id 308
    label "samica"
  ]
  node [
    id 309
    label "przodkini"
  ]
  node [
    id 310
    label "cz&#322;onek"
  ]
  node [
    id 311
    label "mnich"
  ]
  node [
    id 312
    label "r&#243;wniacha"
  ]
  node [
    id 313
    label "zwrot"
  ]
  node [
    id 314
    label "bratanie_si&#281;"
  ]
  node [
    id 315
    label "zbratanie_si&#281;"
  ]
  node [
    id 316
    label "sw&#243;j"
  ]
  node [
    id 317
    label "pobratymiec"
  ]
  node [
    id 318
    label "przyjaciel"
  ]
  node [
    id 319
    label "krewny"
  ]
  node [
    id 320
    label "stryj"
  ]
  node [
    id 321
    label "zakon"
  ]
  node [
    id 322
    label "br"
  ]
  node [
    id 323
    label "bractwo"
  ]
  node [
    id 324
    label "deprive"
  ]
  node [
    id 325
    label "spowodowa&#263;"
  ]
  node [
    id 326
    label "obserwacja"
  ]
  node [
    id 327
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 328
    label "nauka_prawa"
  ]
  node [
    id 329
    label "dominion"
  ]
  node [
    id 330
    label "normatywizm"
  ]
  node [
    id 331
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 332
    label "qualification"
  ]
  node [
    id 333
    label "opis"
  ]
  node [
    id 334
    label "regu&#322;a_Allena"
  ]
  node [
    id 335
    label "normalizacja"
  ]
  node [
    id 336
    label "kazuistyka"
  ]
  node [
    id 337
    label "regu&#322;a_Glogera"
  ]
  node [
    id 338
    label "kultura_duchowa"
  ]
  node [
    id 339
    label "prawo_karne"
  ]
  node [
    id 340
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 341
    label "standard"
  ]
  node [
    id 342
    label "nauka_spo&#322;eczna"
  ]
  node [
    id 343
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 344
    label "struktura"
  ]
  node [
    id 345
    label "prawo_karne_procesowe"
  ]
  node [
    id 346
    label "prawo_Mendla"
  ]
  node [
    id 347
    label "przepis"
  ]
  node [
    id 348
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 349
    label "criterion"
  ]
  node [
    id 350
    label "kanonistyka"
  ]
  node [
    id 351
    label "wymiar_sprawiedliwo&#347;ci"
  ]
  node [
    id 352
    label "pods&#261;dno&#347;&#263;"
  ]
  node [
    id 353
    label "wykonawczy"
  ]
  node [
    id 354
    label "twierdzenie"
  ]
  node [
    id 355
    label "judykatura"
  ]
  node [
    id 356
    label "legislacyjnie"
  ]
  node [
    id 357
    label "umocowa&#263;"
  ]
  node [
    id 358
    label "podmiot"
  ]
  node [
    id 359
    label "procesualistyka"
  ]
  node [
    id 360
    label "kierunek"
  ]
  node [
    id 361
    label "kryminologia"
  ]
  node [
    id 362
    label "kryminalistyka"
  ]
  node [
    id 363
    label "cywilistyka"
  ]
  node [
    id 364
    label "law"
  ]
  node [
    id 365
    label "zasada_d'Alemberta"
  ]
  node [
    id 366
    label "jurisprudence"
  ]
  node [
    id 367
    label "zasada"
  ]
  node [
    id 368
    label "dogmatyzm_prawniczy"
  ]
  node [
    id 369
    label "ga&#322;&#261;&#378;_prawa"
  ]
  node [
    id 370
    label "J&#281;drzejewicz"
  ]
  node [
    id 371
    label "Sto&#322;ypin"
  ]
  node [
    id 372
    label "Nixon"
  ]
  node [
    id 373
    label "Perykles"
  ]
  node [
    id 374
    label "bezpartyjny"
  ]
  node [
    id 375
    label "Gomu&#322;ka"
  ]
  node [
    id 376
    label "Gorbaczow"
  ]
  node [
    id 377
    label "Borel"
  ]
  node [
    id 378
    label "Katon"
  ]
  node [
    id 379
    label "McCarthy"
  ]
  node [
    id 380
    label "Gierek"
  ]
  node [
    id 381
    label "Naser"
  ]
  node [
    id 382
    label "Goebbels"
  ]
  node [
    id 383
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 384
    label "Jaros&#322;aw_Kaczy&#324;ski"
  ]
  node [
    id 385
    label "de_Gaulle"
  ]
  node [
    id 386
    label "Vladimir_Me&#269;iar"
  ]
  node [
    id 387
    label "Bre&#380;niew"
  ]
  node [
    id 388
    label "Juliusz_Cezar"
  ]
  node [
    id 389
    label "Bierut"
  ]
  node [
    id 390
    label "Kuro&#324;"
  ]
  node [
    id 391
    label "Arafat"
  ]
  node [
    id 392
    label "Fidel_Castro"
  ]
  node [
    id 393
    label "Moczar"
  ]
  node [
    id 394
    label "Miko&#322;ajczyk"
  ]
  node [
    id 395
    label "Korwin"
  ]
  node [
    id 396
    label "dzia&#322;acz"
  ]
  node [
    id 397
    label "Winston_Churchill"
  ]
  node [
    id 398
    label "Leszek_Miller"
  ]
  node [
    id 399
    label "Ziobro"
  ]
  node [
    id 400
    label "Mao"
  ]
  node [
    id 401
    label "Chruszczow"
  ]
  node [
    id 402
    label "Putin"
  ]
  node [
    id 403
    label "Falandysz"
  ]
  node [
    id 404
    label "Metternich"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 77
  ]
  edge [
    source 1
    target 78
  ]
  edge [
    source 1
    target 79
  ]
  edge [
    source 1
    target 80
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 85
  ]
  edge [
    source 2
    target 86
  ]
  edge [
    source 2
    target 80
  ]
  edge [
    source 2
    target 87
  ]
  edge [
    source 2
    target 88
  ]
  edge [
    source 2
    target 89
  ]
  edge [
    source 2
    target 90
  ]
  edge [
    source 2
    target 91
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 57
  ]
  edge [
    source 2
    target 84
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 2
    target 100
  ]
  edge [
    source 2
    target 101
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 102
  ]
  edge [
    source 3
    target 103
  ]
  edge [
    source 3
    target 104
  ]
  edge [
    source 3
    target 105
  ]
  edge [
    source 3
    target 106
  ]
  edge [
    source 3
    target 107
  ]
  edge [
    source 3
    target 108
  ]
  edge [
    source 3
    target 5
  ]
  edge [
    source 3
    target 109
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 4
    target 114
  ]
  edge [
    source 4
    target 115
  ]
  edge [
    source 4
    target 116
  ]
  edge [
    source 4
    target 117
  ]
  edge [
    source 4
    target 7
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 118
  ]
  edge [
    source 5
    target 119
  ]
  edge [
    source 5
    target 120
  ]
  edge [
    source 5
    target 121
  ]
  edge [
    source 5
    target 122
  ]
  edge [
    source 5
    target 123
  ]
  edge [
    source 5
    target 124
  ]
  edge [
    source 5
    target 125
  ]
  edge [
    source 5
    target 126
  ]
  edge [
    source 5
    target 127
  ]
  edge [
    source 5
    target 109
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 128
  ]
  edge [
    source 6
    target 129
  ]
  edge [
    source 6
    target 130
  ]
  edge [
    source 6
    target 131
  ]
  edge [
    source 6
    target 132
  ]
  edge [
    source 6
    target 133
  ]
  edge [
    source 6
    target 134
  ]
  edge [
    source 6
    target 135
  ]
  edge [
    source 6
    target 136
  ]
  edge [
    source 6
    target 137
  ]
  edge [
    source 6
    target 138
  ]
  edge [
    source 6
    target 139
  ]
  edge [
    source 6
    target 140
  ]
  edge [
    source 6
    target 141
  ]
  edge [
    source 6
    target 142
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 143
  ]
  edge [
    source 7
    target 144
  ]
  edge [
    source 7
    target 145
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 146
  ]
  edge [
    source 8
    target 147
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 25
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 17
  ]
  edge [
    source 12
    target 18
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 151
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 14
    target 158
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 19
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 23
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 20
  ]
  edge [
    source 18
    target 24
  ]
  edge [
    source 18
    target 159
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 18
    target 183
  ]
  edge [
    source 18
    target 184
  ]
  edge [
    source 18
    target 185
  ]
  edge [
    source 18
    target 186
  ]
  edge [
    source 18
    target 187
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 190
  ]
  edge [
    source 20
    target 191
  ]
  edge [
    source 20
    target 30
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 36
  ]
  edge [
    source 21
    target 37
  ]
  edge [
    source 21
    target 192
  ]
  edge [
    source 21
    target 193
  ]
  edge [
    source 21
    target 194
  ]
  edge [
    source 21
    target 195
  ]
  edge [
    source 21
    target 196
  ]
  edge [
    source 21
    target 197
  ]
  edge [
    source 21
    target 198
  ]
  edge [
    source 21
    target 199
  ]
  edge [
    source 21
    target 200
  ]
  edge [
    source 21
    target 201
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 202
  ]
  edge [
    source 24
    target 203
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 204
  ]
  edge [
    source 25
    target 205
  ]
  edge [
    source 25
    target 206
  ]
  edge [
    source 25
    target 207
  ]
  edge [
    source 25
    target 208
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 209
  ]
  edge [
    source 26
    target 210
  ]
  edge [
    source 26
    target 211
  ]
  edge [
    source 26
    target 212
  ]
  edge [
    source 26
    target 33
  ]
  edge [
    source 26
    target 39
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 213
  ]
  edge [
    source 27
    target 214
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 215
  ]
  edge [
    source 28
    target 216
  ]
  edge [
    source 28
    target 217
  ]
  edge [
    source 28
    target 218
  ]
  edge [
    source 28
    target 219
  ]
  edge [
    source 28
    target 220
  ]
  edge [
    source 28
    target 221
  ]
  edge [
    source 28
    target 222
  ]
  edge [
    source 28
    target 223
  ]
  edge [
    source 28
    target 224
  ]
  edge [
    source 28
    target 225
  ]
  edge [
    source 28
    target 226
  ]
  edge [
    source 28
    target 227
  ]
  edge [
    source 28
    target 100
  ]
  edge [
    source 28
    target 228
  ]
  edge [
    source 28
    target 229
  ]
  edge [
    source 28
    target 34
  ]
  edge [
    source 28
    target 40
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 230
  ]
  edge [
    source 29
    target 231
  ]
  edge [
    source 29
    target 232
  ]
  edge [
    source 29
    target 233
  ]
  edge [
    source 29
    target 234
  ]
  edge [
    source 29
    target 235
  ]
  edge [
    source 29
    target 236
  ]
  edge [
    source 29
    target 237
  ]
  edge [
    source 29
    target 238
  ]
  edge [
    source 29
    target 239
  ]
  edge [
    source 29
    target 240
  ]
  edge [
    source 29
    target 241
  ]
  edge [
    source 29
    target 242
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 243
  ]
  edge [
    source 30
    target 244
  ]
  edge [
    source 30
    target 245
  ]
  edge [
    source 30
    target 246
  ]
  edge [
    source 30
    target 247
  ]
  edge [
    source 30
    target 248
  ]
  edge [
    source 30
    target 249
  ]
  edge [
    source 30
    target 250
  ]
  edge [
    source 30
    target 251
  ]
  edge [
    source 30
    target 252
  ]
  edge [
    source 30
    target 253
  ]
  edge [
    source 30
    target 254
  ]
  edge [
    source 30
    target 255
  ]
  edge [
    source 30
    target 256
  ]
  edge [
    source 30
    target 257
  ]
  edge [
    source 30
    target 258
  ]
  edge [
    source 30
    target 259
  ]
  edge [
    source 30
    target 260
  ]
  edge [
    source 30
    target 261
  ]
  edge [
    source 30
    target 262
  ]
  edge [
    source 30
    target 263
  ]
  edge [
    source 30
    target 264
  ]
  edge [
    source 30
    target 265
  ]
  edge [
    source 30
    target 266
  ]
  edge [
    source 30
    target 267
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 31
    target 34
  ]
  edge [
    source 31
    target 35
  ]
  edge [
    source 31
    target 36
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 268
  ]
  edge [
    source 32
    target 269
  ]
  edge [
    source 32
    target 270
  ]
  edge [
    source 32
    target 271
  ]
  edge [
    source 32
    target 272
  ]
  edge [
    source 32
    target 273
  ]
  edge [
    source 32
    target 274
  ]
  edge [
    source 32
    target 275
  ]
  edge [
    source 32
    target 276
  ]
  edge [
    source 32
    target 277
  ]
  edge [
    source 32
    target 278
  ]
  edge [
    source 32
    target 279
  ]
  edge [
    source 32
    target 280
  ]
  edge [
    source 32
    target 281
  ]
  edge [
    source 32
    target 282
  ]
  edge [
    source 32
    target 283
  ]
  edge [
    source 32
    target 35
  ]
  edge [
    source 33
    target 39
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 284
  ]
  edge [
    source 34
    target 285
  ]
  edge [
    source 34
    target 286
  ]
  edge [
    source 34
    target 287
  ]
  edge [
    source 34
    target 288
  ]
  edge [
    source 34
    target 289
  ]
  edge [
    source 34
    target 40
  ]
  edge [
    source 35
    target 290
  ]
  edge [
    source 35
    target 291
  ]
  edge [
    source 35
    target 292
  ]
  edge [
    source 35
    target 283
  ]
  edge [
    source 35
    target 293
  ]
  edge [
    source 35
    target 294
  ]
  edge [
    source 35
    target 295
  ]
  edge [
    source 35
    target 296
  ]
  edge [
    source 35
    target 297
  ]
  edge [
    source 35
    target 298
  ]
  edge [
    source 35
    target 299
  ]
  edge [
    source 35
    target 300
  ]
  edge [
    source 35
    target 301
  ]
  edge [
    source 35
    target 302
  ]
  edge [
    source 35
    target 303
  ]
  edge [
    source 35
    target 304
  ]
  edge [
    source 35
    target 305
  ]
  edge [
    source 35
    target 306
  ]
  edge [
    source 35
    target 307
  ]
  edge [
    source 35
    target 308
  ]
  edge [
    source 35
    target 309
  ]
  edge [
    source 35
    target 282
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 159
  ]
  edge [
    source 37
    target 310
  ]
  edge [
    source 37
    target 311
  ]
  edge [
    source 37
    target 312
  ]
  edge [
    source 37
    target 313
  ]
  edge [
    source 37
    target 314
  ]
  edge [
    source 37
    target 315
  ]
  edge [
    source 37
    target 316
  ]
  edge [
    source 37
    target 276
  ]
  edge [
    source 37
    target 317
  ]
  edge [
    source 37
    target 318
  ]
  edge [
    source 37
    target 319
  ]
  edge [
    source 37
    target 171
  ]
  edge [
    source 37
    target 320
  ]
  edge [
    source 37
    target 321
  ]
  edge [
    source 37
    target 322
  ]
  edge [
    source 37
    target 195
  ]
  edge [
    source 37
    target 323
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 324
  ]
  edge [
    source 38
    target 325
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 326
  ]
  edge [
    source 39
    target 327
  ]
  edge [
    source 39
    target 328
  ]
  edge [
    source 39
    target 329
  ]
  edge [
    source 39
    target 330
  ]
  edge [
    source 39
    target 331
  ]
  edge [
    source 39
    target 332
  ]
  edge [
    source 39
    target 333
  ]
  edge [
    source 39
    target 334
  ]
  edge [
    source 39
    target 335
  ]
  edge [
    source 39
    target 336
  ]
  edge [
    source 39
    target 337
  ]
  edge [
    source 39
    target 338
  ]
  edge [
    source 39
    target 339
  ]
  edge [
    source 39
    target 340
  ]
  edge [
    source 39
    target 341
  ]
  edge [
    source 39
    target 342
  ]
  edge [
    source 39
    target 343
  ]
  edge [
    source 39
    target 344
  ]
  edge [
    source 39
    target 238
  ]
  edge [
    source 39
    target 345
  ]
  edge [
    source 39
    target 346
  ]
  edge [
    source 39
    target 347
  ]
  edge [
    source 39
    target 348
  ]
  edge [
    source 39
    target 349
  ]
  edge [
    source 39
    target 350
  ]
  edge [
    source 39
    target 351
  ]
  edge [
    source 39
    target 352
  ]
  edge [
    source 39
    target 353
  ]
  edge [
    source 39
    target 354
  ]
  edge [
    source 39
    target 355
  ]
  edge [
    source 39
    target 356
  ]
  edge [
    source 39
    target 357
  ]
  edge [
    source 39
    target 358
  ]
  edge [
    source 39
    target 359
  ]
  edge [
    source 39
    target 360
  ]
  edge [
    source 39
    target 361
  ]
  edge [
    source 39
    target 362
  ]
  edge [
    source 39
    target 363
  ]
  edge [
    source 39
    target 364
  ]
  edge [
    source 39
    target 365
  ]
  edge [
    source 39
    target 366
  ]
  edge [
    source 39
    target 367
  ]
  edge [
    source 39
    target 368
  ]
  edge [
    source 39
    target 369
  ]
  edge [
    source 40
    target 370
  ]
  edge [
    source 40
    target 371
  ]
  edge [
    source 40
    target 372
  ]
  edge [
    source 40
    target 373
  ]
  edge [
    source 40
    target 374
  ]
  edge [
    source 40
    target 375
  ]
  edge [
    source 40
    target 376
  ]
  edge [
    source 40
    target 377
  ]
  edge [
    source 40
    target 378
  ]
  edge [
    source 40
    target 379
  ]
  edge [
    source 40
    target 380
  ]
  edge [
    source 40
    target 381
  ]
  edge [
    source 40
    target 382
  ]
  edge [
    source 40
    target 383
  ]
  edge [
    source 40
    target 384
  ]
  edge [
    source 40
    target 385
  ]
  edge [
    source 40
    target 386
  ]
  edge [
    source 40
    target 387
  ]
  edge [
    source 40
    target 388
  ]
  edge [
    source 40
    target 389
  ]
  edge [
    source 40
    target 390
  ]
  edge [
    source 40
    target 391
  ]
  edge [
    source 40
    target 392
  ]
  edge [
    source 40
    target 393
  ]
  edge [
    source 40
    target 394
  ]
  edge [
    source 40
    target 395
  ]
  edge [
    source 40
    target 396
  ]
  edge [
    source 40
    target 397
  ]
  edge [
    source 40
    target 398
  ]
  edge [
    source 40
    target 399
  ]
  edge [
    source 40
    target 400
  ]
  edge [
    source 40
    target 401
  ]
  edge [
    source 40
    target 402
  ]
  edge [
    source 40
    target 403
  ]
  edge [
    source 40
    target 404
  ]
]
