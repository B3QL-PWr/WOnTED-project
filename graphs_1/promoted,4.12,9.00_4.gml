graph [
  maxDegree 42
  minDegree 1
  meanDegree 2.021857923497268
  density 0.011109109469765208
  graphCliqueNumber 2
  node [
    id 0
    label "prezydent"
    origin "text"
  ]
  node [
    id 1
    label "zaj&#261;&#263;"
    origin "text"
  ]
  node [
    id 2
    label "miejsce"
    origin "text"
  ]
  node [
    id 3
    label "druga"
    origin "text"
  ]
  node [
    id 4
    label "klasa"
    origin "text"
  ]
  node [
    id 5
    label "kiedy"
    origin "text"
  ]
  node [
    id 6
    label "okaza&#263;"
    origin "text"
  ]
  node [
    id 7
    label "si&#281;"
    origin "text"
  ]
  node [
    id 8
    label "sk&#322;ad"
    origin "text"
  ]
  node [
    id 9
    label "poci&#261;g"
    origin "text"
  ]
  node [
    id 10
    label "pkp"
    origin "text"
  ]
  node [
    id 11
    label "intercity"
    origin "text"
  ]
  node [
    id 12
    label "relacja"
    origin "text"
  ]
  node [
    id 13
    label "wiede&#324;"
    origin "text"
  ]
  node [
    id 14
    label "katowice"
    origin "text"
  ]
  node [
    id 15
    label "brakowa&#263;"
    origin "text"
  ]
  node [
    id 16
    label "zarezerwowa&#263;"
    origin "text"
  ]
  node [
    id 17
    label "wagon"
    origin "text"
  ]
  node [
    id 18
    label "Jelcyn"
  ]
  node [
    id 19
    label "Roosevelt"
  ]
  node [
    id 20
    label "Clinton"
  ]
  node [
    id 21
    label "dostojnik"
  ]
  node [
    id 22
    label "Nixon"
  ]
  node [
    id 23
    label "Tito"
  ]
  node [
    id 24
    label "de_Gaulle"
  ]
  node [
    id 25
    label "gruba_ryba"
  ]
  node [
    id 26
    label "Gorbaczow"
  ]
  node [
    id 27
    label "Wa&#322;&#281;sa"
  ]
  node [
    id 28
    label "Putin"
  ]
  node [
    id 29
    label "Naser"
  ]
  node [
    id 30
    label "samorz&#261;dowiec"
  ]
  node [
    id 31
    label "Kemal"
  ]
  node [
    id 32
    label "g&#322;owa_pa&#324;stwa"
  ]
  node [
    id 33
    label "zwierzchnik"
  ]
  node [
    id 34
    label "Bierut"
  ]
  node [
    id 35
    label "dostarczy&#263;"
  ]
  node [
    id 36
    label "wype&#322;ni&#263;"
  ]
  node [
    id 37
    label "anektowa&#263;"
  ]
  node [
    id 38
    label "rozprzestrzeni&#263;_si&#281;"
  ]
  node [
    id 39
    label "obj&#261;&#263;"
  ]
  node [
    id 40
    label "zada&#263;"
  ]
  node [
    id 41
    label "sorb"
  ]
  node [
    id 42
    label "interest"
  ]
  node [
    id 43
    label "skorzysta&#263;"
  ]
  node [
    id 44
    label "usytuowa&#263;_si&#281;"
  ]
  node [
    id 45
    label "wzi&#261;&#263;"
  ]
  node [
    id 46
    label "employment"
  ]
  node [
    id 47
    label "zapanowa&#263;"
  ]
  node [
    id 48
    label "do"
  ]
  node [
    id 49
    label "klasyfikacja"
  ]
  node [
    id 50
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 51
    label "bankrupt"
  ]
  node [
    id 52
    label "zabra&#263;"
  ]
  node [
    id 53
    label "spowodowa&#263;"
  ]
  node [
    id 54
    label "ulokowa&#263;_si&#281;"
  ]
  node [
    id 55
    label "komornik"
  ]
  node [
    id 56
    label "prosecute"
  ]
  node [
    id 57
    label "seize"
  ]
  node [
    id 58
    label "topographic_point"
  ]
  node [
    id 59
    label "wzbudzi&#263;"
  ]
  node [
    id 60
    label "rozciekawi&#263;"
  ]
  node [
    id 61
    label "cia&#322;o"
  ]
  node [
    id 62
    label "plac"
  ]
  node [
    id 63
    label "cecha"
  ]
  node [
    id 64
    label "uwaga"
  ]
  node [
    id 65
    label "przestrze&#324;"
  ]
  node [
    id 66
    label "status"
  ]
  node [
    id 67
    label "obj&#281;to&#347;&#263;"
  ]
  node [
    id 68
    label "chwila"
  ]
  node [
    id 69
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 70
    label "rz&#261;d"
  ]
  node [
    id 71
    label "praca"
  ]
  node [
    id 72
    label "location"
  ]
  node [
    id 73
    label "warunek_lokalowy"
  ]
  node [
    id 74
    label "godzina"
  ]
  node [
    id 75
    label "typ"
  ]
  node [
    id 76
    label "warstwa"
  ]
  node [
    id 77
    label "znak_jako&#347;ci"
  ]
  node [
    id 78
    label "przedmiot"
  ]
  node [
    id 79
    label "przepisa&#263;"
  ]
  node [
    id 80
    label "grupa"
  ]
  node [
    id 81
    label "pomoc"
  ]
  node [
    id 82
    label "arrangement"
  ]
  node [
    id 83
    label "form"
  ]
  node [
    id 84
    label "zaleta"
  ]
  node [
    id 85
    label "poziom"
  ]
  node [
    id 86
    label "dziennik_lekcyjny"
  ]
  node [
    id 87
    label "&#347;rodowisko"
  ]
  node [
    id 88
    label "atak"
  ]
  node [
    id 89
    label "przepisanie"
  ]
  node [
    id 90
    label "szko&#322;a"
  ]
  node [
    id 91
    label "class"
  ]
  node [
    id 92
    label "organizacja"
  ]
  node [
    id 93
    label "obrona"
  ]
  node [
    id 94
    label "type"
  ]
  node [
    id 95
    label "promocja"
  ]
  node [
    id 96
    label "&#322;awka"
  ]
  node [
    id 97
    label "kurs"
  ]
  node [
    id 98
    label "botanika"
  ]
  node [
    id 99
    label "sala"
  ]
  node [
    id 100
    label "spo&#322;ecze&#324;stwo"
  ]
  node [
    id 101
    label "gromada"
  ]
  node [
    id 102
    label "obiekt"
  ]
  node [
    id 103
    label "Ekwici"
  ]
  node [
    id 104
    label "fakcja"
  ]
  node [
    id 105
    label "tablica"
  ]
  node [
    id 106
    label "programowanie_obiektowe"
  ]
  node [
    id 107
    label "wykrzyknik"
  ]
  node [
    id 108
    label "jednostka_systematyczna"
  ]
  node [
    id 109
    label "mecz_mistrzowski"
  ]
  node [
    id 110
    label "zbi&#243;r"
  ]
  node [
    id 111
    label "jako&#347;&#263;"
  ]
  node [
    id 112
    label "rezerwa"
  ]
  node [
    id 113
    label "ligowy_system_rozgrywek"
  ]
  node [
    id 114
    label "pokaza&#263;"
  ]
  node [
    id 115
    label "zdradzi&#263;_si&#281;"
  ]
  node [
    id 116
    label "testify"
  ]
  node [
    id 117
    label "give"
  ]
  node [
    id 118
    label "pole"
  ]
  node [
    id 119
    label "fabryka"
  ]
  node [
    id 120
    label "blokada"
  ]
  node [
    id 121
    label "pas"
  ]
  node [
    id 122
    label "pomieszczenie"
  ]
  node [
    id 123
    label "set"
  ]
  node [
    id 124
    label "constitution"
  ]
  node [
    id 125
    label "tekst"
  ]
  node [
    id 126
    label "struktura"
  ]
  node [
    id 127
    label "basic"
  ]
  node [
    id 128
    label "rank_and_file"
  ]
  node [
    id 129
    label "tabulacja"
  ]
  node [
    id 130
    label "hurtownia"
  ]
  node [
    id 131
    label "sklep"
  ]
  node [
    id 132
    label "&#347;wiat&#322;o"
  ]
  node [
    id 133
    label "zesp&#243;&#322;"
  ]
  node [
    id 134
    label "syf"
  ]
  node [
    id 135
    label "w&#322;a&#347;ciwo&#347;&#263;_chemiczna"
  ]
  node [
    id 136
    label "obr&#243;bka"
  ]
  node [
    id 137
    label "sk&#322;adnik"
  ]
  node [
    id 138
    label "lokomotywa"
  ]
  node [
    id 139
    label "pojazd_kolejowy"
  ]
  node [
    id 140
    label "kolej"
  ]
  node [
    id 141
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 142
    label "tender"
  ]
  node [
    id 143
    label "cug"
  ]
  node [
    id 144
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 145
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 146
    label "wypowied&#378;"
  ]
  node [
    id 147
    label "message"
  ]
  node [
    id 148
    label "podzbi&#243;r"
  ]
  node [
    id 149
    label "iloczyn_kartezja&#324;ski"
  ]
  node [
    id 150
    label "ustosunkowywa&#263;"
  ]
  node [
    id 151
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 152
    label "bratnia_dusza"
  ]
  node [
    id 153
    label "zwi&#261;zanie"
  ]
  node [
    id 154
    label "ustosunkowanie"
  ]
  node [
    id 155
    label "ustosunkowywanie"
  ]
  node [
    id 156
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 157
    label "zwi&#261;za&#263;"
  ]
  node [
    id 158
    label "ustosunkowa&#263;"
  ]
  node [
    id 159
    label "niezgodno&#347;&#263;"
  ]
  node [
    id 160
    label "korespondent"
  ]
  node [
    id 161
    label "marriage"
  ]
  node [
    id 162
    label "wi&#261;zanie"
  ]
  node [
    id 163
    label "trasa"
  ]
  node [
    id 164
    label "zwi&#261;zek"
  ]
  node [
    id 165
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 166
    label "zale&#380;no&#347;&#263;"
  ]
  node [
    id 167
    label "sprawko"
  ]
  node [
    id 168
    label "przebiera&#263;"
  ]
  node [
    id 169
    label "lack"
  ]
  node [
    id 170
    label "przegl&#261;da&#263;"
  ]
  node [
    id 171
    label "sprawdza&#263;"
  ]
  node [
    id 172
    label "utylizowa&#263;"
  ]
  node [
    id 173
    label "aim"
  ]
  node [
    id 174
    label "zachowa&#263;"
  ]
  node [
    id 175
    label "wym&#243;wi&#263;"
  ]
  node [
    id 176
    label "condition"
  ]
  node [
    id 177
    label "zapewni&#263;"
  ]
  node [
    id 178
    label "czo&#322;ownica"
  ]
  node [
    id 179
    label "tramwaj"
  ]
  node [
    id 180
    label "karton"
  ]
  node [
    id 181
    label "harmonijka"
  ]
  node [
    id 182
    label "PKP"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 35
  ]
  edge [
    source 1
    target 36
  ]
  edge [
    source 1
    target 37
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 1
    target 39
  ]
  edge [
    source 1
    target 40
  ]
  edge [
    source 1
    target 41
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 1
    target 59
  ]
  edge [
    source 1
    target 60
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 2
    target 64
  ]
  edge [
    source 2
    target 65
  ]
  edge [
    source 2
    target 66
  ]
  edge [
    source 2
    target 67
  ]
  edge [
    source 2
    target 68
  ]
  edge [
    source 2
    target 69
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 74
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 17
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 4
    target 97
  ]
  edge [
    source 4
    target 98
  ]
  edge [
    source 4
    target 99
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 4
    target 105
  ]
  edge [
    source 4
    target 106
  ]
  edge [
    source 4
    target 107
  ]
  edge [
    source 4
    target 108
  ]
  edge [
    source 4
    target 109
  ]
  edge [
    source 4
    target 110
  ]
  edge [
    source 4
    target 111
  ]
  edge [
    source 4
    target 112
  ]
  edge [
    source 4
    target 113
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 114
  ]
  edge [
    source 6
    target 115
  ]
  edge [
    source 6
    target 116
  ]
  edge [
    source 6
    target 117
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 8
    target 122
  ]
  edge [
    source 8
    target 123
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 138
  ]
  edge [
    source 9
    target 139
  ]
  edge [
    source 9
    target 140
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 17
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 182
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 12
    target 150
  ]
  edge [
    source 12
    target 151
  ]
  edge [
    source 12
    target 152
  ]
  edge [
    source 12
    target 153
  ]
  edge [
    source 12
    target 154
  ]
  edge [
    source 12
    target 155
  ]
  edge [
    source 12
    target 156
  ]
  edge [
    source 12
    target 157
  ]
  edge [
    source 12
    target 158
  ]
  edge [
    source 12
    target 159
  ]
  edge [
    source 12
    target 160
  ]
  edge [
    source 12
    target 161
  ]
  edge [
    source 12
    target 162
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 16
    target 176
  ]
  edge [
    source 16
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
]
