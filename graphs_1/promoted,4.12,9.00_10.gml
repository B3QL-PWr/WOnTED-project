graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9130434782608696
  density 0.08695652173913043
  graphCliqueNumber 2
  node [
    id 0
    label "sytuacja"
    origin "text"
  ]
  node [
    id 1
    label "kierowca"
    origin "text"
  ]
  node [
    id 2
    label "by&#263;"
    origin "text"
  ]
  node [
    id 3
    label "niepozazdroszczenie"
    origin "text"
  ]
  node [
    id 4
    label "szczeg&#243;&#322;"
  ]
  node [
    id 5
    label "motyw"
  ]
  node [
    id 6
    label "okoliczno&#347;&#263;"
  ]
  node [
    id 7
    label "state"
  ]
  node [
    id 8
    label "realia"
  ]
  node [
    id 9
    label "warunki"
  ]
  node [
    id 10
    label "cz&#322;owiek"
  ]
  node [
    id 11
    label "transportowiec"
  ]
  node [
    id 12
    label "si&#281;ga&#263;"
  ]
  node [
    id 13
    label "trwa&#263;"
  ]
  node [
    id 14
    label "obecno&#347;&#263;"
  ]
  node [
    id 15
    label "stan"
  ]
  node [
    id 16
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 17
    label "stand"
  ]
  node [
    id 18
    label "mie&#263;_miejsce"
  ]
  node [
    id 19
    label "uczestniczy&#263;"
  ]
  node [
    id 20
    label "chodzi&#263;"
  ]
  node [
    id 21
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 22
    label "equal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
]
