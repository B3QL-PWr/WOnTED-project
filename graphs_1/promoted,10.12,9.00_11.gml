graph [
  maxDegree 7
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 1
    label "m&#243;j"
    origin "text"
  ]
  node [
    id 2
    label "swoje"
    origin "text"
  ]
  node [
    id 3
    label "pomys&#322;"
    origin "text"
  ]
  node [
    id 4
    label "jaki&#347;"
  ]
  node [
    id 5
    label "czyj&#347;"
  ]
  node [
    id 6
    label "m&#261;&#380;"
  ]
  node [
    id 7
    label "system"
  ]
  node [
    id 8
    label "wytw&#243;r"
  ]
  node [
    id 9
    label "idea"
  ]
  node [
    id 10
    label "ukra&#347;&#263;"
  ]
  node [
    id 11
    label "ukradzenie"
  ]
  node [
    id 12
    label "pocz&#261;tki"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
]
