graph [
  maxDegree 41
  minDegree 1
  meanDegree 2.075313807531381
  density 0.008719805913997397
  graphCliqueNumber 3
  node [
    id 0
    label "polska"
    origin "text"
  ]
  node [
    id 1
    label "ukaza&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "nowa"
    origin "text"
  ]
  node [
    id 4
    label "ksi&#261;&#380;ek"
    origin "text"
  ]
  node [
    id 5
    label "lawrence"
    origin "text"
  ]
  node [
    id 6
    label "aby"
    origin "text"
  ]
  node [
    id 7
    label "sztuka"
    origin "text"
  ]
  node [
    id 8
    label "biznes"
    origin "text"
  ]
  node [
    id 9
    label "rozkwita&#263;"
    origin "text"
  ]
  node [
    id 10
    label "hybrydowy"
    origin "text"
  ]
  node [
    id 11
    label "gospodarka"
    origin "text"
  ]
  node [
    id 12
    label "by&#263;"
    origin "text"
  ]
  node [
    id 13
    label "ju&#380;"
    origin "text"
  ]
  node [
    id 14
    label "druga"
    origin "text"
  ]
  node [
    id 15
    label "wyda&#263;"
    origin "text"
  ]
  node [
    id 16
    label "przez"
    origin "text"
  ]
  node [
    id 17
    label "wydawnictwo"
    origin "text"
  ]
  node [
    id 18
    label "akademicki"
    origin "text"
  ]
  node [
    id 19
    label "profesjonalny"
    origin "text"
  ]
  node [
    id 20
    label "kultura"
    origin "text"
  ]
  node [
    id 21
    label "lata"
    origin "text"
  ]
  node [
    id 22
    label "temu"
    origin "text"
  ]
  node [
    id 23
    label "pokaza&#263;"
  ]
  node [
    id 24
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 25
    label "unwrap"
  ]
  node [
    id 26
    label "gwiazda"
  ]
  node [
    id 27
    label "gwiazda_podw&#243;jna"
  ]
  node [
    id 28
    label "troch&#281;"
  ]
  node [
    id 29
    label "theatrical_performance"
  ]
  node [
    id 30
    label "cz&#322;owiek"
  ]
  node [
    id 31
    label "sprawno&#347;&#263;"
  ]
  node [
    id 32
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 33
    label "ods&#322;ona"
  ]
  node [
    id 34
    label "przedmiot"
  ]
  node [
    id 35
    label "Faust"
  ]
  node [
    id 36
    label "jednostka"
  ]
  node [
    id 37
    label "fortel"
  ]
  node [
    id 38
    label "environment"
  ]
  node [
    id 39
    label "rola"
  ]
  node [
    id 40
    label "dzie&#322;o_sztuki"
  ]
  node [
    id 41
    label "utw&#243;r"
  ]
  node [
    id 42
    label "egzemplarz"
  ]
  node [
    id 43
    label "realizacja"
  ]
  node [
    id 44
    label "turn"
  ]
  node [
    id 45
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 46
    label "scenografia"
  ]
  node [
    id 47
    label "kultura_duchowa"
  ]
  node [
    id 48
    label "ilo&#347;&#263;"
  ]
  node [
    id 49
    label "pokaz"
  ]
  node [
    id 50
    label "przedstawia&#263;"
  ]
  node [
    id 51
    label "pr&#243;bowa&#263;"
  ]
  node [
    id 52
    label "pr&#243;bowanie"
  ]
  node [
    id 53
    label "kobieta"
  ]
  node [
    id 54
    label "scena"
  ]
  node [
    id 55
    label "przedstawianie"
  ]
  node [
    id 56
    label "scenariusz"
  ]
  node [
    id 57
    label "didaskalia"
  ]
  node [
    id 58
    label "Apollo"
  ]
  node [
    id 59
    label "czyn"
  ]
  node [
    id 60
    label "przedstawienie"
  ]
  node [
    id 61
    label "head"
  ]
  node [
    id 62
    label "telewizyjno&#347;&#263;"
  ]
  node [
    id 63
    label "przedstawi&#263;"
  ]
  node [
    id 64
    label "ambala&#380;"
  ]
  node [
    id 65
    label "towar"
  ]
  node [
    id 66
    label "rynek"
  ]
  node [
    id 67
    label "Hortex"
  ]
  node [
    id 68
    label "MAC"
  ]
  node [
    id 69
    label "korzy&#347;&#263;"
  ]
  node [
    id 70
    label "reengineering"
  ]
  node [
    id 71
    label "podmiot_gospodarczy"
  ]
  node [
    id 72
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 73
    label "Google"
  ]
  node [
    id 74
    label "networking"
  ]
  node [
    id 75
    label "interes"
  ]
  node [
    id 76
    label "zasoby_ludzkie"
  ]
  node [
    id 77
    label "przedsi&#281;wzi&#281;cie"
  ]
  node [
    id 78
    label "Canon"
  ]
  node [
    id 79
    label "object"
  ]
  node [
    id 80
    label "HP"
  ]
  node [
    id 81
    label "Baltona"
  ]
  node [
    id 82
    label "Pewex"
  ]
  node [
    id 83
    label "MAN_SE"
  ]
  node [
    id 84
    label "Apeks"
  ]
  node [
    id 85
    label "zasoby"
  ]
  node [
    id 86
    label "Orbis"
  ]
  node [
    id 87
    label "Spo&#322;em"
  ]
  node [
    id 88
    label "sprawa"
  ]
  node [
    id 89
    label "przedsi&#281;biorczo&#347;&#263;"
  ]
  node [
    id 90
    label "Orlen"
  ]
  node [
    id 91
    label "zmienia&#263;_si&#281;"
  ]
  node [
    id 92
    label "zapala&#263;_si&#281;"
  ]
  node [
    id 93
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 94
    label "&#347;wita&#263;"
  ]
  node [
    id 95
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 96
    label "rozwija&#263;_si&#281;"
  ]
  node [
    id 97
    label "doro&#347;le&#263;"
  ]
  node [
    id 98
    label "blow"
  ]
  node [
    id 99
    label "zr&#243;&#380;nicowany"
  ]
  node [
    id 100
    label "hybrydowo"
  ]
  node [
    id 101
    label "pole"
  ]
  node [
    id 102
    label "szkolnictwo"
  ]
  node [
    id 103
    label "przemys&#322;"
  ]
  node [
    id 104
    label "gospodarka_wodna"
  ]
  node [
    id 105
    label "fabryka"
  ]
  node [
    id 106
    label "rolnictwo"
  ]
  node [
    id 107
    label "gospodarka_le&#347;na"
  ]
  node [
    id 108
    label "gospodarowa&#263;"
  ]
  node [
    id 109
    label "sektor_prywatny"
  ]
  node [
    id 110
    label "obronno&#347;&#263;"
  ]
  node [
    id 111
    label "obora"
  ]
  node [
    id 112
    label "mieszkalnictwo"
  ]
  node [
    id 113
    label "sektor_publiczny"
  ]
  node [
    id 114
    label "czerwona_strefa"
  ]
  node [
    id 115
    label "struktura"
  ]
  node [
    id 116
    label "stodo&#322;a"
  ]
  node [
    id 117
    label "rybo&#322;&#243;wstwo"
  ]
  node [
    id 118
    label "produkowanie"
  ]
  node [
    id 119
    label "gospodarowanie"
  ]
  node [
    id 120
    label "agregat_ekonomiczny"
  ]
  node [
    id 121
    label "sch&#322;adza&#263;"
  ]
  node [
    id 122
    label "spichlerz"
  ]
  node [
    id 123
    label "inwentarz"
  ]
  node [
    id 124
    label "transport"
  ]
  node [
    id 125
    label "sch&#322;odzenie"
  ]
  node [
    id 126
    label "s&#322;u&#380;ba_zdrowia"
  ]
  node [
    id 127
    label "miejsce_pracy"
  ]
  node [
    id 128
    label "wytw&#243;rnia"
  ]
  node [
    id 129
    label "farmaceutyka"
  ]
  node [
    id 130
    label "bud&#380;et&#243;wka"
  ]
  node [
    id 131
    label "skarbowo&#347;&#263;"
  ]
  node [
    id 132
    label "administracja"
  ]
  node [
    id 133
    label "sch&#322;adzanie"
  ]
  node [
    id 134
    label "bankowo&#347;&#263;"
  ]
  node [
    id 135
    label "zasada"
  ]
  node [
    id 136
    label "sch&#322;odzi&#263;"
  ]
  node [
    id 137
    label "regulacja_cen"
  ]
  node [
    id 138
    label "si&#281;ga&#263;"
  ]
  node [
    id 139
    label "trwa&#263;"
  ]
  node [
    id 140
    label "obecno&#347;&#263;"
  ]
  node [
    id 141
    label "stan"
  ]
  node [
    id 142
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 143
    label "stand"
  ]
  node [
    id 144
    label "mie&#263;_miejsce"
  ]
  node [
    id 145
    label "uczestniczy&#263;"
  ]
  node [
    id 146
    label "chodzi&#263;"
  ]
  node [
    id 147
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 148
    label "equal"
  ]
  node [
    id 149
    label "godzina"
  ]
  node [
    id 150
    label "impart"
  ]
  node [
    id 151
    label "panna_na_wydaniu"
  ]
  node [
    id 152
    label "translate"
  ]
  node [
    id 153
    label "give"
  ]
  node [
    id 154
    label "pieni&#261;dze"
  ]
  node [
    id 155
    label "supply"
  ]
  node [
    id 156
    label "wprowadzi&#263;"
  ]
  node [
    id 157
    label "da&#263;"
  ]
  node [
    id 158
    label "zapach"
  ]
  node [
    id 159
    label "powierzy&#263;"
  ]
  node [
    id 160
    label "produkcja"
  ]
  node [
    id 161
    label "poda&#263;"
  ]
  node [
    id 162
    label "skojarzy&#263;"
  ]
  node [
    id 163
    label "dress"
  ]
  node [
    id 164
    label "plon"
  ]
  node [
    id 165
    label "ujawni&#263;"
  ]
  node [
    id 166
    label "reszta"
  ]
  node [
    id 167
    label "zwr&#243;ci&#263;_si&#281;"
  ]
  node [
    id 168
    label "zadenuncjowa&#263;"
  ]
  node [
    id 169
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 170
    label "zrobi&#263;"
  ]
  node [
    id 171
    label "tajemnica"
  ]
  node [
    id 172
    label "wiano"
  ]
  node [
    id 173
    label "gor&#261;cy_pieni&#261;dz"
  ]
  node [
    id 174
    label "wytworzy&#263;"
  ]
  node [
    id 175
    label "d&#378;wi&#281;k"
  ]
  node [
    id 176
    label "picture"
  ]
  node [
    id 177
    label "debit"
  ]
  node [
    id 178
    label "szata_graficzna"
  ]
  node [
    id 179
    label "firma"
  ]
  node [
    id 180
    label "redakcja"
  ]
  node [
    id 181
    label "Pa&#324;stwowy_Instytut_Wydawniczy"
  ]
  node [
    id 182
    label "wydawa&#263;"
  ]
  node [
    id 183
    label "redaktor"
  ]
  node [
    id 184
    label "druk"
  ]
  node [
    id 185
    label "Sp&#243;&#322;dzielnia_Wydawnicza_&#34;Czytelnik&#34;"
  ]
  node [
    id 186
    label "poster"
  ]
  node [
    id 187
    label "publikacja"
  ]
  node [
    id 188
    label "prawid&#322;owy"
  ]
  node [
    id 189
    label "tradycyjny"
  ]
  node [
    id 190
    label "intelektualny"
  ]
  node [
    id 191
    label "akademiczny"
  ]
  node [
    id 192
    label "szkolny"
  ]
  node [
    id 193
    label "po_akademicku"
  ]
  node [
    id 194
    label "naukowy"
  ]
  node [
    id 195
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 196
    label "akademicko"
  ]
  node [
    id 197
    label "teoretyczny"
  ]
  node [
    id 198
    label "studencki"
  ]
  node [
    id 199
    label "odpowiedni"
  ]
  node [
    id 200
    label "specjalny"
  ]
  node [
    id 201
    label "kompetentny"
  ]
  node [
    id 202
    label "rzetelny"
  ]
  node [
    id 203
    label "trained"
  ]
  node [
    id 204
    label "porz&#261;dny"
  ]
  node [
    id 205
    label "fachowy"
  ]
  node [
    id 206
    label "ch&#322;odny"
  ]
  node [
    id 207
    label "profesjonalnie"
  ]
  node [
    id 208
    label "zawodowo"
  ]
  node [
    id 209
    label "Wsch&#243;d"
  ]
  node [
    id 210
    label "rzecz"
  ]
  node [
    id 211
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 212
    label "religia"
  ]
  node [
    id 213
    label "przejmowa&#263;"
  ]
  node [
    id 214
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 215
    label "makrokosmos"
  ]
  node [
    id 216
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 217
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 218
    label "zjawisko"
  ]
  node [
    id 219
    label "praca_rolnicza"
  ]
  node [
    id 220
    label "tradycja"
  ]
  node [
    id 221
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 222
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 223
    label "przejmowanie"
  ]
  node [
    id 224
    label "cecha"
  ]
  node [
    id 225
    label "asymilowanie_si&#281;"
  ]
  node [
    id 226
    label "przej&#261;&#263;"
  ]
  node [
    id 227
    label "hodowla"
  ]
  node [
    id 228
    label "brzoskwiniarnia"
  ]
  node [
    id 229
    label "populace"
  ]
  node [
    id 230
    label "konwencja"
  ]
  node [
    id 231
    label "propriety"
  ]
  node [
    id 232
    label "jako&#347;&#263;"
  ]
  node [
    id 233
    label "kuchnia"
  ]
  node [
    id 234
    label "zwyczaj"
  ]
  node [
    id 235
    label "przej&#281;cie"
  ]
  node [
    id 236
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 237
    label "summer"
  ]
  node [
    id 238
    label "czas"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 15
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 28
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 29
  ]
  edge [
    source 7
    target 30
  ]
  edge [
    source 7
    target 31
  ]
  edge [
    source 7
    target 32
  ]
  edge [
    source 7
    target 33
  ]
  edge [
    source 7
    target 34
  ]
  edge [
    source 7
    target 35
  ]
  edge [
    source 7
    target 36
  ]
  edge [
    source 7
    target 37
  ]
  edge [
    source 7
    target 38
  ]
  edge [
    source 7
    target 39
  ]
  edge [
    source 7
    target 40
  ]
  edge [
    source 7
    target 41
  ]
  edge [
    source 7
    target 42
  ]
  edge [
    source 7
    target 43
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 45
  ]
  edge [
    source 7
    target 46
  ]
  edge [
    source 7
    target 47
  ]
  edge [
    source 7
    target 48
  ]
  edge [
    source 7
    target 49
  ]
  edge [
    source 7
    target 50
  ]
  edge [
    source 7
    target 51
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
  edge [
    source 7
    target 20
  ]
  edge [
    source 7
    target 65
  ]
  edge [
    source 7
    target 19
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 8
    target 73
  ]
  edge [
    source 8
    target 74
  ]
  edge [
    source 8
    target 75
  ]
  edge [
    source 8
    target 76
  ]
  edge [
    source 8
    target 77
  ]
  edge [
    source 8
    target 78
  ]
  edge [
    source 8
    target 79
  ]
  edge [
    source 8
    target 80
  ]
  edge [
    source 8
    target 81
  ]
  edge [
    source 8
    target 82
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 9
    target 92
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 9
    target 95
  ]
  edge [
    source 9
    target 96
  ]
  edge [
    source 9
    target 97
  ]
  edge [
    source 9
    target 98
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 66
  ]
  edge [
    source 11
    target 101
  ]
  edge [
    source 11
    target 102
  ]
  edge [
    source 11
    target 103
  ]
  edge [
    source 11
    target 104
  ]
  edge [
    source 11
    target 105
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 11
    target 113
  ]
  edge [
    source 11
    target 114
  ]
  edge [
    source 11
    target 115
  ]
  edge [
    source 11
    target 116
  ]
  edge [
    source 11
    target 117
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 137
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 12
    target 145
  ]
  edge [
    source 12
    target 146
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 149
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 15
    target 151
  ]
  edge [
    source 15
    target 152
  ]
  edge [
    source 15
    target 153
  ]
  edge [
    source 15
    target 154
  ]
  edge [
    source 15
    target 155
  ]
  edge [
    source 15
    target 156
  ]
  edge [
    source 15
    target 157
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 17
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 15
    target 162
  ]
  edge [
    source 15
    target 163
  ]
  edge [
    source 15
    target 164
  ]
  edge [
    source 15
    target 165
  ]
  edge [
    source 15
    target 166
  ]
  edge [
    source 15
    target 167
  ]
  edge [
    source 15
    target 168
  ]
  edge [
    source 15
    target 169
  ]
  edge [
    source 15
    target 170
  ]
  edge [
    source 15
    target 171
  ]
  edge [
    source 15
    target 172
  ]
  edge [
    source 15
    target 173
  ]
  edge [
    source 15
    target 174
  ]
  edge [
    source 15
    target 175
  ]
  edge [
    source 15
    target 176
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 188
  ]
  edge [
    source 18
    target 189
  ]
  edge [
    source 18
    target 190
  ]
  edge [
    source 18
    target 191
  ]
  edge [
    source 18
    target 192
  ]
  edge [
    source 18
    target 193
  ]
  edge [
    source 18
    target 194
  ]
  edge [
    source 18
    target 195
  ]
  edge [
    source 18
    target 196
  ]
  edge [
    source 18
    target 197
  ]
  edge [
    source 18
    target 198
  ]
  edge [
    source 18
    target 199
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 19
    target 201
  ]
  edge [
    source 19
    target 202
  ]
  edge [
    source 19
    target 203
  ]
  edge [
    source 19
    target 204
  ]
  edge [
    source 19
    target 205
  ]
  edge [
    source 19
    target 206
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 207
  ]
  edge [
    source 19
    target 208
  ]
  edge [
    source 20
    target 32
  ]
  edge [
    source 20
    target 34
  ]
  edge [
    source 20
    target 72
  ]
  edge [
    source 20
    target 209
  ]
  edge [
    source 20
    target 210
  ]
  edge [
    source 20
    target 211
  ]
  edge [
    source 20
    target 212
  ]
  edge [
    source 20
    target 213
  ]
  edge [
    source 20
    target 214
  ]
  edge [
    source 20
    target 215
  ]
  edge [
    source 20
    target 216
  ]
  edge [
    source 20
    target 217
  ]
  edge [
    source 20
    target 218
  ]
  edge [
    source 20
    target 219
  ]
  edge [
    source 20
    target 220
  ]
  edge [
    source 20
    target 221
  ]
  edge [
    source 20
    target 222
  ]
  edge [
    source 20
    target 223
  ]
  edge [
    source 20
    target 224
  ]
  edge [
    source 20
    target 225
  ]
  edge [
    source 20
    target 226
  ]
  edge [
    source 20
    target 227
  ]
  edge [
    source 20
    target 228
  ]
  edge [
    source 20
    target 229
  ]
  edge [
    source 20
    target 230
  ]
  edge [
    source 20
    target 231
  ]
  edge [
    source 20
    target 232
  ]
  edge [
    source 20
    target 233
  ]
  edge [
    source 20
    target 234
  ]
  edge [
    source 20
    target 235
  ]
  edge [
    source 20
    target 236
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 237
  ]
  edge [
    source 21
    target 238
  ]
]
