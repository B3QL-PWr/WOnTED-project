graph [
  maxDegree 20
  minDegree 1
  meanDegree 2.25
  density 0.03571428571428571
  graphCliqueNumber 5
  node [
    id 0
    label "mistrzostwa"
    origin "text"
  ]
  node [
    id 1
    label "azja"
    origin "text"
  ]
  node [
    id 2
    label "lekkoatletyka"
    origin "text"
  ]
  node [
    id 3
    label "skok"
    origin "text"
  ]
  node [
    id 4
    label "tyczka"
    origin "text"
  ]
  node [
    id 5
    label "kobieta"
    origin "text"
  ]
  node [
    id 6
    label "championship"
  ]
  node [
    id 7
    label "Formu&#322;a_1"
  ]
  node [
    id 8
    label "zawody"
  ]
  node [
    id 9
    label "sport"
  ]
  node [
    id 10
    label "tr&#243;jskok"
  ]
  node [
    id 11
    label "skok_o_tyczce"
  ]
  node [
    id 12
    label "dziesi&#281;ciob&#243;j"
  ]
  node [
    id 13
    label "skok_wzwy&#380;"
  ]
  node [
    id 14
    label "rzut_oszczepem"
  ]
  node [
    id 15
    label "bieg"
  ]
  node [
    id 16
    label "skok_w_dal"
  ]
  node [
    id 17
    label "ch&#243;d"
  ]
  node [
    id 18
    label "pchni&#281;cie_kul&#261;"
  ]
  node [
    id 19
    label "rzut_m&#322;otem"
  ]
  node [
    id 20
    label "wybicie"
  ]
  node [
    id 21
    label "konkurencja"
  ]
  node [
    id 22
    label "derail"
  ]
  node [
    id 23
    label "ptak"
  ]
  node [
    id 24
    label "ruch"
  ]
  node [
    id 25
    label "l&#261;dowanie"
  ]
  node [
    id 26
    label "&#322;apa"
  ]
  node [
    id 27
    label "struktura_anatomiczna"
  ]
  node [
    id 28
    label "stroke"
  ]
  node [
    id 29
    label "gimnastyka_przyrz&#261;dowa"
  ]
  node [
    id 30
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 31
    label "zmiana"
  ]
  node [
    id 32
    label "caper"
  ]
  node [
    id 33
    label "zaj&#261;c"
  ]
  node [
    id 34
    label "naskok"
  ]
  node [
    id 35
    label "napad"
  ]
  node [
    id 36
    label "ko&#347;&#263;_skokowa"
  ]
  node [
    id 37
    label "noga"
  ]
  node [
    id 38
    label "cz&#322;owiek"
  ]
  node [
    id 39
    label "skoki"
  ]
  node [
    id 40
    label "chor&#261;giewka"
  ]
  node [
    id 41
    label "patyk"
  ]
  node [
    id 42
    label "palik"
  ]
  node [
    id 43
    label "sprz&#281;t_sportowy"
  ]
  node [
    id 44
    label "pr&#281;t"
  ]
  node [
    id 45
    label "przekwitanie"
  ]
  node [
    id 46
    label "m&#281;&#380;yna"
  ]
  node [
    id 47
    label "babka"
  ]
  node [
    id 48
    label "samica"
  ]
  node [
    id 49
    label "doros&#322;y"
  ]
  node [
    id 50
    label "ulec"
  ]
  node [
    id 51
    label "uleganie"
  ]
  node [
    id 52
    label "partnerka"
  ]
  node [
    id 53
    label "&#380;ona"
  ]
  node [
    id 54
    label "ulega&#263;"
  ]
  node [
    id 55
    label "uk&#322;ad_rozrodczy_&#380;e&#324;ski"
  ]
  node [
    id 56
    label "pa&#324;stwo"
  ]
  node [
    id 57
    label "ulegni&#281;cie"
  ]
  node [
    id 58
    label "menopauza"
  ]
  node [
    id 59
    label "&#322;ono"
  ]
  node [
    id 60
    label "mistrzostwo"
  ]
  node [
    id 61
    label "Azja"
  ]
  node [
    id 62
    label "wyspa"
  ]
  node [
    id 63
    label "XVIII"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 2
    target 62
  ]
  edge [
    source 2
    target 63
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 21
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 62
  ]
  edge [
    source 60
    target 63
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 63
  ]
  edge [
    source 62
    target 63
  ]
]
