graph [
  maxDegree 11
  minDegree 1
  meanDegree 1.7777777777777777
  density 0.10457516339869281
  graphCliqueNumber 2
  node [
    id 0
    label "herb"
    origin "text"
  ]
  node [
    id 1
    label "gmin"
    origin "text"
  ]
  node [
    id 2
    label "lutut&#243;w"
    origin "text"
  ]
  node [
    id 3
    label "trzymacz"
  ]
  node [
    id 4
    label "symbol"
  ]
  node [
    id 5
    label "tarcza_herbowa"
  ]
  node [
    id 6
    label "korona_rangowa"
  ]
  node [
    id 7
    label "heraldyka"
  ]
  node [
    id 8
    label "blazonowa&#263;"
  ]
  node [
    id 9
    label "blazonowanie"
  ]
  node [
    id 10
    label "klejnot_herbowy"
  ]
  node [
    id 11
    label "barwy"
  ]
  node [
    id 12
    label "znak"
  ]
  node [
    id 13
    label "stan_trzeci"
  ]
  node [
    id 14
    label "stan"
  ]
  node [
    id 15
    label "gminno&#347;&#263;"
  ]
  node [
    id 16
    label "muzeum"
  ]
  node [
    id 17
    label "narodowy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 16
    target 17
  ]
]
