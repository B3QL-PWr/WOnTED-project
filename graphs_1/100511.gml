graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.8
  density 0.2
  graphCliqueNumber 2
  node [
    id 0
    label "stadion"
    origin "text"
  ]
  node [
    id 1
    label "punjab"
    origin "text"
  ]
  node [
    id 2
    label "obiekt"
  ]
  node [
    id 3
    label "jednostka_d&#322;ugo&#347;ci"
  ]
  node [
    id 4
    label "zgromadzenie"
  ]
  node [
    id 5
    label "korona"
  ]
  node [
    id 6
    label "court"
  ]
  node [
    id 7
    label "trybuna"
  ]
  node [
    id 8
    label "budowla"
  ]
  node [
    id 9
    label "Punjab"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
]
