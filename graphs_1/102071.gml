graph [
  maxDegree 23
  minDegree 1
  meanDegree 2.1572052401746724
  density 0.009461426491994178
  graphCliqueNumber 3
  node [
    id 0
    label "chyba"
    origin "text"
  ]
  node [
    id 1
    label "pierwsza"
    origin "text"
  ]
  node [
    id 2
    label "dow&#243;d"
    origin "text"
  ]
  node [
    id 3
    label "model"
    origin "text"
  ]
  node [
    id 4
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 5
    label "oprze&#263;"
    origin "text"
  ]
  node [
    id 6
    label "by&#263;"
    origin "text"
  ]
  node [
    id 7
    label "wikipedia"
    origin "text"
  ]
  node [
    id 8
    label "dzia&#322;o"
    origin "text"
  ]
  node [
    id 9
    label "ca&#322;kiem"
    origin "text"
  ]
  node [
    id 10
    label "dobrze"
    origin "text"
  ]
  node [
    id 11
    label "s&#322;ynny"
    origin "text"
  ]
  node [
    id 12
    label "raport"
    origin "text"
  ]
  node [
    id 13
    label "wynika&#263;"
    origin "text"
  ]
  node [
    id 14
    label "b&#322;&#261;d"
    origin "text"
  ]
  node [
    id 15
    label "sieciowy"
    origin "text"
  ]
  node [
    id 16
    label "encyklopedia"
    origin "text"
  ]
  node [
    id 17
    label "zbli&#380;ony"
    origin "text"
  ]
  node [
    id 18
    label "liczba"
    origin "text"
  ]
  node [
    id 19
    label "pomy&#322;ka"
    origin "text"
  ]
  node [
    id 20
    label "encyclopedia"
    origin "text"
  ]
  node [
    id 21
    label "britannica"
    origin "text"
  ]
  node [
    id 22
    label "teraz"
    origin "text"
  ]
  node [
    id 23
    label "sam"
    origin "text"
  ]
  node [
    id 24
    label "upodabnia&#263;"
    origin "text"
  ]
  node [
    id 25
    label "si&#281;"
    origin "text"
  ]
  node [
    id 26
    label "wikipedii"
    origin "text"
  ]
  node [
    id 27
    label "jak"
    origin "text"
  ]
  node [
    id 28
    label "donosi&#263;"
    origin "text"
  ]
  node [
    id 29
    label "cho&#263;"
    origin "text"
  ]
  node [
    id 30
    label "wszyscy"
    origin "text"
  ]
  node [
    id 31
    label "zg&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 32
    label "propozycja"
    origin "text"
  ]
  node [
    id 33
    label "has&#322;o"
    origin "text"
  ]
  node [
    id 34
    label "umie&#347;ci&#263;"
    origin "text"
  ]
  node [
    id 35
    label "liczy&#263;"
    origin "text"
  ]
  node [
    id 36
    label "siebie"
    origin "text"
  ]
  node [
    id 37
    label "lata"
    origin "text"
  ]
  node [
    id 38
    label "godzina"
  ]
  node [
    id 39
    label "dokument"
  ]
  node [
    id 40
    label "forsing"
  ]
  node [
    id 41
    label "certificate"
  ]
  node [
    id 42
    label "rewizja"
  ]
  node [
    id 43
    label "argument"
  ]
  node [
    id 44
    label "act"
  ]
  node [
    id 45
    label "rzecz"
  ]
  node [
    id 46
    label "&#347;rodek"
  ]
  node [
    id 47
    label "uzasadnienie"
  ]
  node [
    id 48
    label "typ"
  ]
  node [
    id 49
    label "cz&#322;owiek"
  ]
  node [
    id 50
    label "pozowa&#263;"
  ]
  node [
    id 51
    label "ideal"
  ]
  node [
    id 52
    label "matryca"
  ]
  node [
    id 53
    label "imitacja"
  ]
  node [
    id 54
    label "ruch"
  ]
  node [
    id 55
    label "motif"
  ]
  node [
    id 56
    label "pozowanie"
  ]
  node [
    id 57
    label "wz&#243;r"
  ]
  node [
    id 58
    label "miniatura"
  ]
  node [
    id 59
    label "prezenter"
  ]
  node [
    id 60
    label "facet"
  ]
  node [
    id 61
    label "orygina&#322;"
  ]
  node [
    id 62
    label "mildew"
  ]
  node [
    id 63
    label "spos&#243;b"
  ]
  node [
    id 64
    label "zi&#243;&#322;ko"
  ]
  node [
    id 65
    label "adaptation"
  ]
  node [
    id 66
    label "establish"
  ]
  node [
    id 67
    label "zaczerpn&#261;&#263;"
  ]
  node [
    id 68
    label "recline"
  ]
  node [
    id 69
    label "podstawa"
  ]
  node [
    id 70
    label "ustawi&#263;"
  ]
  node [
    id 71
    label "osnowa&#263;"
  ]
  node [
    id 72
    label "si&#281;ga&#263;"
  ]
  node [
    id 73
    label "trwa&#263;"
  ]
  node [
    id 74
    label "obecno&#347;&#263;"
  ]
  node [
    id 75
    label "stan"
  ]
  node [
    id 76
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 77
    label "stand"
  ]
  node [
    id 78
    label "mie&#263;_miejsce"
  ]
  node [
    id 79
    label "uczestniczy&#263;"
  ]
  node [
    id 80
    label "chodzi&#263;"
  ]
  node [
    id 81
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 82
    label "equal"
  ]
  node [
    id 83
    label "bateria"
  ]
  node [
    id 84
    label "laweta"
  ]
  node [
    id 85
    label "bro&#324;_artyleryjska"
  ]
  node [
    id 86
    label "bro&#324;"
  ]
  node [
    id 87
    label "oporopowrotnik"
  ]
  node [
    id 88
    label "przedmuchiwacz"
  ]
  node [
    id 89
    label "artyleria"
  ]
  node [
    id 90
    label "waln&#261;&#263;"
  ]
  node [
    id 91
    label "bateria_artylerii"
  ]
  node [
    id 92
    label "cannon"
  ]
  node [
    id 93
    label "wniwecz"
  ]
  node [
    id 94
    label "zupe&#322;ny"
  ]
  node [
    id 95
    label "moralnie"
  ]
  node [
    id 96
    label "wiele"
  ]
  node [
    id 97
    label "lepiej"
  ]
  node [
    id 98
    label "korzystnie"
  ]
  node [
    id 99
    label "pomy&#347;lnie"
  ]
  node [
    id 100
    label "pozytywnie"
  ]
  node [
    id 101
    label "dobry"
  ]
  node [
    id 102
    label "dobroczynnie"
  ]
  node [
    id 103
    label "odpowiednio"
  ]
  node [
    id 104
    label "w&#322;a&#347;ciwie"
  ]
  node [
    id 105
    label "skutecznie"
  ]
  node [
    id 106
    label "ws&#322;awianie_si&#281;"
  ]
  node [
    id 107
    label "ws&#322;awienie"
  ]
  node [
    id 108
    label "ws&#322;awienie_si&#281;"
  ]
  node [
    id 109
    label "wielki"
  ]
  node [
    id 110
    label "znany"
  ]
  node [
    id 111
    label "os&#322;awiony"
  ]
  node [
    id 112
    label "ws&#322;awianie"
  ]
  node [
    id 113
    label "raport_Beveridge'a"
  ]
  node [
    id 114
    label "relacja"
  ]
  node [
    id 115
    label "raport_Fischlera"
  ]
  node [
    id 116
    label "statement"
  ]
  node [
    id 117
    label "okazywa&#263;_si&#281;"
  ]
  node [
    id 118
    label "rise"
  ]
  node [
    id 119
    label "appear"
  ]
  node [
    id 120
    label "rodzi&#263;_si&#281;"
  ]
  node [
    id 121
    label "baseball"
  ]
  node [
    id 122
    label "czyn"
  ]
  node [
    id 123
    label "error"
  ]
  node [
    id 124
    label "pomyli&#263;_si&#281;"
  ]
  node [
    id 125
    label "pomylenie_si&#281;"
  ]
  node [
    id 126
    label "mniemanie"
  ]
  node [
    id 127
    label "byk"
  ]
  node [
    id 128
    label "nieprawid&#322;owo&#347;&#263;"
  ]
  node [
    id 129
    label "rezultat"
  ]
  node [
    id 130
    label "elektroniczny"
  ]
  node [
    id 131
    label "sieciowo"
  ]
  node [
    id 132
    label "internetowo"
  ]
  node [
    id 133
    label "netowy"
  ]
  node [
    id 134
    label "typowy"
  ]
  node [
    id 135
    label "tezaurus"
  ]
  node [
    id 136
    label "artyku&#322;_has&#322;owy"
  ]
  node [
    id 137
    label "publikacja_encyklopedyczna"
  ]
  node [
    id 138
    label "bliski"
  ]
  node [
    id 139
    label "niedaleki"
  ]
  node [
    id 140
    label "kategoria"
  ]
  node [
    id 141
    label "kategoria_gramatyczna"
  ]
  node [
    id 142
    label "kwadrat_magiczny"
  ]
  node [
    id 143
    label "cecha"
  ]
  node [
    id 144
    label "grupa"
  ]
  node [
    id 145
    label "wyra&#380;enie"
  ]
  node [
    id 146
    label "pierwiastek"
  ]
  node [
    id 147
    label "rozmiar"
  ]
  node [
    id 148
    label "number"
  ]
  node [
    id 149
    label "poj&#281;cie"
  ]
  node [
    id 150
    label "koniugacja"
  ]
  node [
    id 151
    label "po&#322;&#261;czenie"
  ]
  node [
    id 152
    label "faux_pas"
  ]
  node [
    id 153
    label "chwila"
  ]
  node [
    id 154
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 155
    label "sklep"
  ]
  node [
    id 156
    label "dopasowywa&#263;"
  ]
  node [
    id 157
    label "assimilate"
  ]
  node [
    id 158
    label "byd&#322;o"
  ]
  node [
    id 159
    label "zobo"
  ]
  node [
    id 160
    label "ssak_kr&#281;torogi"
  ]
  node [
    id 161
    label "yakalo"
  ]
  node [
    id 162
    label "dzo"
  ]
  node [
    id 163
    label "zu&#380;y&#263;"
  ]
  node [
    id 164
    label "get"
  ]
  node [
    id 165
    label "render"
  ]
  node [
    id 166
    label "ci&#261;&#380;a"
  ]
  node [
    id 167
    label "informowa&#263;"
  ]
  node [
    id 168
    label "zanosi&#263;"
  ]
  node [
    id 169
    label "introduce"
  ]
  node [
    id 170
    label "do&#322;&#261;cza&#263;"
  ]
  node [
    id 171
    label "spill_the_beans"
  ]
  node [
    id 172
    label "give"
  ]
  node [
    id 173
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 174
    label "inform"
  ]
  node [
    id 175
    label "przeby&#263;"
  ]
  node [
    id 176
    label "report"
  ]
  node [
    id 177
    label "write"
  ]
  node [
    id 178
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 179
    label "pomys&#322;"
  ]
  node [
    id 180
    label "proposal"
  ]
  node [
    id 181
    label "ochrona"
  ]
  node [
    id 182
    label "sztuka_dla_sztuki"
  ]
  node [
    id 183
    label "dost&#281;p"
  ]
  node [
    id 184
    label "przes&#322;anie"
  ]
  node [
    id 185
    label "definicja"
  ]
  node [
    id 186
    label "idea"
  ]
  node [
    id 187
    label "sygna&#322;"
  ]
  node [
    id 188
    label "kwalifikator"
  ]
  node [
    id 189
    label "artyku&#322;"
  ]
  node [
    id 190
    label "powiedzenie"
  ]
  node [
    id 191
    label "guide_word"
  ]
  node [
    id 192
    label "rozwi&#261;zanie"
  ]
  node [
    id 193
    label "solicitation"
  ]
  node [
    id 194
    label "krzy&#380;&#243;wka"
  ]
  node [
    id 195
    label "kod"
  ]
  node [
    id 196
    label "pozycja"
  ]
  node [
    id 197
    label "leksem"
  ]
  node [
    id 198
    label "zrobi&#263;"
  ]
  node [
    id 199
    label "okre&#347;li&#263;"
  ]
  node [
    id 200
    label "uplasowa&#263;"
  ]
  node [
    id 201
    label "umieszcza&#263;"
  ]
  node [
    id 202
    label "wpierniczy&#263;"
  ]
  node [
    id 203
    label "pomie&#347;ci&#263;"
  ]
  node [
    id 204
    label "zmieni&#263;"
  ]
  node [
    id 205
    label "put"
  ]
  node [
    id 206
    label "set"
  ]
  node [
    id 207
    label "dodawa&#263;"
  ]
  node [
    id 208
    label "wymienia&#263;"
  ]
  node [
    id 209
    label "okre&#347;la&#263;"
  ]
  node [
    id 210
    label "spodziewa&#263;_si&#281;"
  ]
  node [
    id 211
    label "dyskalkulia"
  ]
  node [
    id 212
    label "wynagrodzenie"
  ]
  node [
    id 213
    label "admit"
  ]
  node [
    id 214
    label "osi&#261;ga&#263;"
  ]
  node [
    id 215
    label "wyznacza&#263;"
  ]
  node [
    id 216
    label "posiada&#263;"
  ]
  node [
    id 217
    label "mierzy&#263;"
  ]
  node [
    id 218
    label "odlicza&#263;"
  ]
  node [
    id 219
    label "bra&#263;"
  ]
  node [
    id 220
    label "wycenia&#263;"
  ]
  node [
    id 221
    label "dzia&#322;anie_matematyczne"
  ]
  node [
    id 222
    label "rachowa&#263;"
  ]
  node [
    id 223
    label "tell"
  ]
  node [
    id 224
    label "uwzgl&#281;dnia&#263;"
  ]
  node [
    id 225
    label "policza&#263;"
  ]
  node [
    id 226
    label "count"
  ]
  node [
    id 227
    label "summer"
  ]
  node [
    id 228
    label "czas"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 38
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 2
    target 43
  ]
  edge [
    source 2
    target 44
  ]
  edge [
    source 2
    target 45
  ]
  edge [
    source 2
    target 46
  ]
  edge [
    source 2
    target 47
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 3
    target 52
  ]
  edge [
    source 3
    target 53
  ]
  edge [
    source 3
    target 54
  ]
  edge [
    source 3
    target 55
  ]
  edge [
    source 3
    target 56
  ]
  edge [
    source 3
    target 57
  ]
  edge [
    source 3
    target 58
  ]
  edge [
    source 3
    target 59
  ]
  edge [
    source 3
    target 60
  ]
  edge [
    source 3
    target 61
  ]
  edge [
    source 3
    target 62
  ]
  edge [
    source 3
    target 63
  ]
  edge [
    source 3
    target 64
  ]
  edge [
    source 3
    target 65
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 68
  ]
  edge [
    source 5
    target 69
  ]
  edge [
    source 5
    target 70
  ]
  edge [
    source 5
    target 71
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 10
  ]
  edge [
    source 6
    target 11
  ]
  edge [
    source 6
    target 16
  ]
  edge [
    source 6
    target 17
  ]
  edge [
    source 6
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 6
    target 74
  ]
  edge [
    source 6
    target 75
  ]
  edge [
    source 6
    target 76
  ]
  edge [
    source 6
    target 77
  ]
  edge [
    source 6
    target 78
  ]
  edge [
    source 6
    target 79
  ]
  edge [
    source 6
    target 80
  ]
  edge [
    source 6
    target 81
  ]
  edge [
    source 6
    target 82
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 12
  ]
  edge [
    source 7
    target 11
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 83
  ]
  edge [
    source 8
    target 84
  ]
  edge [
    source 8
    target 85
  ]
  edge [
    source 8
    target 86
  ]
  edge [
    source 8
    target 87
  ]
  edge [
    source 8
    target 88
  ]
  edge [
    source 8
    target 89
  ]
  edge [
    source 8
    target 90
  ]
  edge [
    source 8
    target 91
  ]
  edge [
    source 8
    target 92
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 93
  ]
  edge [
    source 9
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 10
    target 101
  ]
  edge [
    source 10
    target 102
  ]
  edge [
    source 10
    target 103
  ]
  edge [
    source 10
    target 104
  ]
  edge [
    source 10
    target 105
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 106
  ]
  edge [
    source 11
    target 107
  ]
  edge [
    source 11
    target 108
  ]
  edge [
    source 11
    target 109
  ]
  edge [
    source 11
    target 110
  ]
  edge [
    source 11
    target 111
  ]
  edge [
    source 11
    target 112
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 113
  ]
  edge [
    source 12
    target 114
  ]
  edge [
    source 12
    target 115
  ]
  edge [
    source 12
    target 116
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 13
    target 118
  ]
  edge [
    source 13
    target 119
  ]
  edge [
    source 13
    target 120
  ]
  edge [
    source 13
    target 21
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 121
  ]
  edge [
    source 14
    target 122
  ]
  edge [
    source 14
    target 123
  ]
  edge [
    source 14
    target 124
  ]
  edge [
    source 14
    target 125
  ]
  edge [
    source 14
    target 126
  ]
  edge [
    source 14
    target 127
  ]
  edge [
    source 14
    target 128
  ]
  edge [
    source 14
    target 129
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 16
    target 37
  ]
  edge [
    source 16
    target 135
  ]
  edge [
    source 16
    target 136
  ]
  edge [
    source 16
    target 137
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 138
  ]
  edge [
    source 17
    target 139
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 140
  ]
  edge [
    source 18
    target 141
  ]
  edge [
    source 18
    target 142
  ]
  edge [
    source 18
    target 143
  ]
  edge [
    source 18
    target 144
  ]
  edge [
    source 18
    target 145
  ]
  edge [
    source 18
    target 146
  ]
  edge [
    source 18
    target 147
  ]
  edge [
    source 18
    target 148
  ]
  edge [
    source 18
    target 149
  ]
  edge [
    source 18
    target 150
  ]
  edge [
    source 19
    target 122
  ]
  edge [
    source 19
    target 123
  ]
  edge [
    source 19
    target 151
  ]
  edge [
    source 19
    target 152
  ]
  edge [
    source 19
    target 129
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 23
  ]
  edge [
    source 21
    target 24
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 153
  ]
  edge [
    source 22
    target 154
  ]
  edge [
    source 23
    target 155
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 156
  ]
  edge [
    source 24
    target 157
  ]
  edge [
    source 24
    target 26
  ]
  edge [
    source 24
    target 31
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 31
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 158
  ]
  edge [
    source 27
    target 159
  ]
  edge [
    source 27
    target 160
  ]
  edge [
    source 27
    target 161
  ]
  edge [
    source 27
    target 162
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 163
  ]
  edge [
    source 28
    target 164
  ]
  edge [
    source 28
    target 165
  ]
  edge [
    source 28
    target 166
  ]
  edge [
    source 28
    target 167
  ]
  edge [
    source 28
    target 168
  ]
  edge [
    source 28
    target 169
  ]
  edge [
    source 28
    target 170
  ]
  edge [
    source 28
    target 171
  ]
  edge [
    source 28
    target 172
  ]
  edge [
    source 28
    target 173
  ]
  edge [
    source 28
    target 174
  ]
  edge [
    source 28
    target 175
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 176
  ]
  edge [
    source 31
    target 177
  ]
  edge [
    source 31
    target 178
  ]
  edge [
    source 31
    target 167
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 179
  ]
  edge [
    source 32
    target 180
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 181
  ]
  edge [
    source 33
    target 182
  ]
  edge [
    source 33
    target 183
  ]
  edge [
    source 33
    target 184
  ]
  edge [
    source 33
    target 185
  ]
  edge [
    source 33
    target 186
  ]
  edge [
    source 33
    target 187
  ]
  edge [
    source 33
    target 188
  ]
  edge [
    source 33
    target 145
  ]
  edge [
    source 33
    target 189
  ]
  edge [
    source 33
    target 190
  ]
  edge [
    source 33
    target 136
  ]
  edge [
    source 33
    target 191
  ]
  edge [
    source 33
    target 192
  ]
  edge [
    source 33
    target 193
  ]
  edge [
    source 33
    target 194
  ]
  edge [
    source 33
    target 195
  ]
  edge [
    source 33
    target 196
  ]
  edge [
    source 33
    target 197
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 198
  ]
  edge [
    source 34
    target 199
  ]
  edge [
    source 34
    target 200
  ]
  edge [
    source 34
    target 201
  ]
  edge [
    source 34
    target 202
  ]
  edge [
    source 34
    target 203
  ]
  edge [
    source 34
    target 204
  ]
  edge [
    source 34
    target 205
  ]
  edge [
    source 34
    target 206
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 176
  ]
  edge [
    source 35
    target 207
  ]
  edge [
    source 35
    target 208
  ]
  edge [
    source 35
    target 209
  ]
  edge [
    source 35
    target 210
  ]
  edge [
    source 35
    target 211
  ]
  edge [
    source 35
    target 212
  ]
  edge [
    source 35
    target 213
  ]
  edge [
    source 35
    target 214
  ]
  edge [
    source 35
    target 215
  ]
  edge [
    source 35
    target 216
  ]
  edge [
    source 35
    target 217
  ]
  edge [
    source 35
    target 218
  ]
  edge [
    source 35
    target 219
  ]
  edge [
    source 35
    target 220
  ]
  edge [
    source 35
    target 221
  ]
  edge [
    source 35
    target 222
  ]
  edge [
    source 35
    target 223
  ]
  edge [
    source 35
    target 224
  ]
  edge [
    source 35
    target 225
  ]
  edge [
    source 35
    target 226
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 37
    target 227
  ]
  edge [
    source 37
    target 228
  ]
]
