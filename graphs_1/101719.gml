graph [
  maxDegree 25
  minDegree 1
  meanDegree 1.9692307692307693
  density 0.03076923076923077
  graphCliqueNumber 2
  node [
    id 0
    label "polski"
    origin "text"
  ]
  node [
    id 1
    label "fansuberzy"
    origin "text"
  ]
  node [
    id 2
    label "anime"
    origin "text"
  ]
  node [
    id 3
    label "anna"
    origin "text"
  ]
  node [
    id 4
    label "koralewsk&#261;"
    origin "text"
  ]
  node [
    id 5
    label "temat"
    origin "text"
  ]
  node [
    id 6
    label "fan"
    origin "text"
  ]
  node [
    id 7
    label "t&#322;umacz"
    origin "text"
  ]
  node [
    id 8
    label "lacki"
  ]
  node [
    id 9
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 10
    label "przedmiot"
  ]
  node [
    id 11
    label "sztajer"
  ]
  node [
    id 12
    label "drabant"
  ]
  node [
    id 13
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 14
    label "polak"
  ]
  node [
    id 15
    label "pierogi_ruskie"
  ]
  node [
    id 16
    label "krakowiak"
  ]
  node [
    id 17
    label "Polish"
  ]
  node [
    id 18
    label "j&#281;zyk"
  ]
  node [
    id 19
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 20
    label "oberek"
  ]
  node [
    id 21
    label "po_polsku"
  ]
  node [
    id 22
    label "mazur"
  ]
  node [
    id 23
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 24
    label "chodzony"
  ]
  node [
    id 25
    label "skoczny"
  ]
  node [
    id 26
    label "ryba_po_grecku"
  ]
  node [
    id 27
    label "goniony"
  ]
  node [
    id 28
    label "polsko"
  ]
  node [
    id 29
    label "otaku"
  ]
  node [
    id 30
    label "Cowboy_Bebop"
  ]
  node [
    id 31
    label "film"
  ]
  node [
    id 32
    label "fraza"
  ]
  node [
    id 33
    label "forma"
  ]
  node [
    id 34
    label "melodia"
  ]
  node [
    id 35
    label "rzecz"
  ]
  node [
    id 36
    label "zbacza&#263;"
  ]
  node [
    id 37
    label "entity"
  ]
  node [
    id 38
    label "omawia&#263;"
  ]
  node [
    id 39
    label "topik"
  ]
  node [
    id 40
    label "wyraz_pochodny"
  ]
  node [
    id 41
    label "om&#243;wi&#263;"
  ]
  node [
    id 42
    label "omawianie"
  ]
  node [
    id 43
    label "w&#261;tek"
  ]
  node [
    id 44
    label "forum"
  ]
  node [
    id 45
    label "cecha"
  ]
  node [
    id 46
    label "zboczenie"
  ]
  node [
    id 47
    label "zbaczanie"
  ]
  node [
    id 48
    label "tre&#347;&#263;"
  ]
  node [
    id 49
    label "tematyka"
  ]
  node [
    id 50
    label "sprawa"
  ]
  node [
    id 51
    label "istota"
  ]
  node [
    id 52
    label "otoczka"
  ]
  node [
    id 53
    label "zboczy&#263;"
  ]
  node [
    id 54
    label "om&#243;wienie"
  ]
  node [
    id 55
    label "mi&#322;o&#347;nik"
  ]
  node [
    id 56
    label "fan_club"
  ]
  node [
    id 57
    label "fandom"
  ]
  node [
    id 58
    label "przek&#322;adowca"
  ]
  node [
    id 59
    label "przek&#322;adacz"
  ]
  node [
    id 60
    label "Jan_Czeczot"
  ]
  node [
    id 61
    label "aplikacja"
  ]
  node [
    id 62
    label "interpretator"
  ]
  node [
    id 63
    label "Jakub_Wujek"
  ]
  node [
    id 64
    label "pracownik_umys&#322;owy"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 0
    target 11
  ]
  edge [
    source 0
    target 12
  ]
  edge [
    source 0
    target 13
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
  edge [
    source 5
    target 41
  ]
  edge [
    source 5
    target 42
  ]
  edge [
    source 5
    target 43
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 5
    target 51
  ]
  edge [
    source 5
    target 52
  ]
  edge [
    source 5
    target 53
  ]
  edge [
    source 5
    target 54
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 7
    target 64
  ]
]
