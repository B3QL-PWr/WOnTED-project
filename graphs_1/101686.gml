graph [
  maxDegree 28
  minDegree 1
  meanDegree 2.199248120300752
  density 0.0041417102077226965
  graphCliqueNumber 3
  node [
    id 0
    label "koniec"
    origin "text"
  ]
  node [
    id 1
    label "jeden"
    origin "text"
  ]
  node [
    id 2
    label "sprawa"
    origin "text"
  ]
  node [
    id 3
    label "jako"
    origin "text"
  ]
  node [
    id 4
    label "przyk&#322;ad"
    origin "text"
  ]
  node [
    id 5
    label "trzeba"
    origin "text"
  ]
  node [
    id 6
    label "u&#347;wiadamia&#263;"
    origin "text"
  ]
  node [
    id 7
    label "spo&#322;ecze&#324;stwo"
    origin "text"
  ]
  node [
    id 8
    label "leczenie"
    origin "text"
  ]
  node [
    id 9
    label "kosztowa&#263;"
    origin "text"
  ]
  node [
    id 10
    label "wprowadzi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "nasa"
    origin "text"
  ]
  node [
    id 12
    label "zasada"
    origin "text"
  ]
  node [
    id 13
    label "by&#263;"
    origin "text"
  ]
  node [
    id 14
    label "rzecz"
    origin "text"
  ]
  node [
    id 15
    label "mo&#380;e"
    origin "text"
  ]
  node [
    id 16
    label "niewielki"
    origin "text"
  ]
  node [
    id 17
    label "ale"
    origin "text"
  ]
  node [
    id 18
    label "wa&#380;ny"
    origin "text"
  ]
  node [
    id 19
    label "przy"
    origin "text"
  ]
  node [
    id 20
    label "ka&#380;dy"
    origin "text"
  ]
  node [
    id 21
    label "wypis"
    origin "text"
  ]
  node [
    id 22
    label "szpital"
    origin "text"
  ]
  node [
    id 23
    label "pacjent"
    origin "text"
  ]
  node [
    id 24
    label "otrzymywa&#263;"
    origin "text"
  ]
  node [
    id 25
    label "informacja"
    origin "text"
  ]
  node [
    id 26
    label "ile"
    origin "text"
  ]
  node [
    id 27
    label "fundusz"
    origin "text"
  ]
  node [
    id 28
    label "zap&#322;aci&#263;"
    origin "text"
  ]
  node [
    id 29
    label "pobyt"
    origin "text"
  ]
  node [
    id 30
    label "dostawa&#263;"
    origin "text"
  ]
  node [
    id 31
    label "potwierdzenie"
    origin "text"
  ]
  node [
    id 32
    label "tym"
    origin "text"
  ]
  node [
    id 33
    label "pewien"
    origin "text"
  ]
  node [
    id 34
    label "spos&#243;b"
    origin "text"
  ]
  node [
    id 35
    label "trafia&#263;"
    origin "text"
  ]
  node [
    id 36
    label "&#347;wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 37
    label "gor&#261;co"
    origin "text"
  ]
  node [
    id 38
    label "namawia&#263;"
    origin "text"
  ]
  node [
    id 39
    label "skorzysta&#263;"
    origin "text"
  ]
  node [
    id 40
    label "tychy"
    origin "text"
  ]
  node [
    id 41
    label "pozytywny"
    origin "text"
  ]
  node [
    id 42
    label "do&#347;wiadczenie"
    origin "text"
  ]
  node [
    id 43
    label "jednocze&#347;nie"
    origin "text"
  ]
  node [
    id 44
    label "s&#261;dzi&#263;"
    origin "text"
  ]
  node [
    id 45
    label "imienie"
    origin "text"
  ]
  node [
    id 46
    label "ca&#322;y"
    origin "text"
  ]
  node [
    id 47
    label "klub"
    origin "text"
  ]
  node [
    id 48
    label "deklarowa&#263;"
    origin "text"
  ]
  node [
    id 49
    label "aktywny"
    origin "text"
  ]
  node [
    id 50
    label "praca"
    origin "text"
  ]
  node [
    id 51
    label "pose&#322;"
    origin "text"
  ]
  node [
    id 52
    label "polski"
    origin "text"
  ]
  node [
    id 53
    label "stronnictwo"
    origin "text"
  ]
  node [
    id 54
    label "ludowy"
    origin "text"
  ]
  node [
    id 55
    label "nad"
    origin "text"
  ]
  node [
    id 56
    label "wszyscy"
    origin "text"
  ]
  node [
    id 57
    label "przed&#322;o&#380;y&#263;"
    origin "text"
  ]
  node [
    id 58
    label "projekt"
    origin "text"
  ]
  node [
    id 59
    label "ustawa"
    origin "text"
  ]
  node [
    id 60
    label "dzi&#281;kowa&#263;"
    origin "text"
  ]
  node [
    id 61
    label "pi&#281;knie"
    origin "text"
  ]
  node [
    id 62
    label "oklask"
    origin "text"
  ]
  node [
    id 63
    label "defenestracja"
  ]
  node [
    id 64
    label "szereg"
  ]
  node [
    id 65
    label "dzia&#322;anie"
  ]
  node [
    id 66
    label "miejsce"
  ]
  node [
    id 67
    label "ostatnie_podrygi"
  ]
  node [
    id 68
    label "kres"
  ]
  node [
    id 69
    label "agonia"
  ]
  node [
    id 70
    label "visitation"
  ]
  node [
    id 71
    label "szeol"
  ]
  node [
    id 72
    label "mogi&#322;a"
  ]
  node [
    id 73
    label "chwila"
  ]
  node [
    id 74
    label "wydarzenie"
  ]
  node [
    id 75
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 76
    label "pogrzebanie"
  ]
  node [
    id 77
    label "punkt"
  ]
  node [
    id 78
    label "&#380;a&#322;oba"
  ]
  node [
    id 79
    label "zabicie"
  ]
  node [
    id 80
    label "kres_&#380;ycia"
  ]
  node [
    id 81
    label "kieliszek"
  ]
  node [
    id 82
    label "shot"
  ]
  node [
    id 83
    label "niezr&#243;&#380;nicowany"
  ]
  node [
    id 84
    label "ujednolicenie_si&#281;"
  ]
  node [
    id 85
    label "jaki&#347;"
  ]
  node [
    id 86
    label "jednolicie"
  ]
  node [
    id 87
    label "ujednolicanie_si&#281;"
  ]
  node [
    id 88
    label "w&#243;dka"
  ]
  node [
    id 89
    label "ten"
  ]
  node [
    id 90
    label "ujednolicenie"
  ]
  node [
    id 91
    label "jednakowy"
  ]
  node [
    id 92
    label "temat"
  ]
  node [
    id 93
    label "kognicja"
  ]
  node [
    id 94
    label "idea"
  ]
  node [
    id 95
    label "szczeg&#243;&#322;"
  ]
  node [
    id 96
    label "przes&#322;anka"
  ]
  node [
    id 97
    label "rozprawa"
  ]
  node [
    id 98
    label "object"
  ]
  node [
    id 99
    label "proposition"
  ]
  node [
    id 100
    label "cz&#322;owiek"
  ]
  node [
    id 101
    label "czyn"
  ]
  node [
    id 102
    label "przedstawiciel"
  ]
  node [
    id 103
    label "ilustracja"
  ]
  node [
    id 104
    label "fakt"
  ]
  node [
    id 105
    label "trza"
  ]
  node [
    id 106
    label "necessity"
  ]
  node [
    id 107
    label "explain"
  ]
  node [
    id 108
    label "t&#322;umaczy&#263;"
  ]
  node [
    id 109
    label "informowa&#263;"
  ]
  node [
    id 110
    label "pole"
  ]
  node [
    id 111
    label "kastowo&#347;&#263;"
  ]
  node [
    id 112
    label "ludzie_pracy"
  ]
  node [
    id 113
    label "community"
  ]
  node [
    id 114
    label "status"
  ]
  node [
    id 115
    label "cywilizacja"
  ]
  node [
    id 116
    label "pozaklasowy"
  ]
  node [
    id 117
    label "aspo&#322;eczny"
  ]
  node [
    id 118
    label "uwarstwienie"
  ]
  node [
    id 119
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 120
    label "elita"
  ]
  node [
    id 121
    label "spo&#322;eczno&#347;&#263;"
  ]
  node [
    id 122
    label "wspo&#322;ecze&#324;stwo"
  ]
  node [
    id 123
    label "klasa"
  ]
  node [
    id 124
    label "homeopata"
  ]
  node [
    id 125
    label "zabieg"
  ]
  node [
    id 126
    label "sanatoryjny"
  ]
  node [
    id 127
    label "&#322;agodzenie"
  ]
  node [
    id 128
    label "alopata"
  ]
  node [
    id 129
    label "odwykowy"
  ]
  node [
    id 130
    label "zdiagnozowanie"
  ]
  node [
    id 131
    label "nauka_medyczna"
  ]
  node [
    id 132
    label "wizyta"
  ]
  node [
    id 133
    label "pomaganie"
  ]
  node [
    id 134
    label "plombowanie"
  ]
  node [
    id 135
    label "uzdrawianie"
  ]
  node [
    id 136
    label "opatrywanie"
  ]
  node [
    id 137
    label "wizytowanie"
  ]
  node [
    id 138
    label "opatrzenie"
  ]
  node [
    id 139
    label "medication"
  ]
  node [
    id 140
    label "opieka_medyczna"
  ]
  node [
    id 141
    label "cena"
  ]
  node [
    id 142
    label "try"
  ]
  node [
    id 143
    label "essay"
  ]
  node [
    id 144
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 145
    label "doznawa&#263;"
  ]
  node [
    id 146
    label "savor"
  ]
  node [
    id 147
    label "wej&#347;&#263;"
  ]
  node [
    id 148
    label "rynek"
  ]
  node [
    id 149
    label "zacz&#261;&#263;"
  ]
  node [
    id 150
    label "zej&#347;&#263;"
  ]
  node [
    id 151
    label "spowodowa&#263;"
  ]
  node [
    id 152
    label "wpisa&#263;"
  ]
  node [
    id 153
    label "insert"
  ]
  node [
    id 154
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 155
    label "testify"
  ]
  node [
    id 156
    label "indicate"
  ]
  node [
    id 157
    label "zapozna&#263;"
  ]
  node [
    id 158
    label "umie&#347;ci&#263;"
  ]
  node [
    id 159
    label "zak&#322;&#243;ci&#263;"
  ]
  node [
    id 160
    label "zrobi&#263;"
  ]
  node [
    id 161
    label "doprowadzi&#263;"
  ]
  node [
    id 162
    label "picture"
  ]
  node [
    id 163
    label "obserwacja"
  ]
  node [
    id 164
    label "moralno&#347;&#263;"
  ]
  node [
    id 165
    label "podstawa"
  ]
  node [
    id 166
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 167
    label "umowa"
  ]
  node [
    id 168
    label "dominion"
  ]
  node [
    id 169
    label "qualification"
  ]
  node [
    id 170
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 171
    label "opis"
  ]
  node [
    id 172
    label "regu&#322;a_Allena"
  ]
  node [
    id 173
    label "normalizacja"
  ]
  node [
    id 174
    label "regu&#322;a_Glogera"
  ]
  node [
    id 175
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 176
    label "standard"
  ]
  node [
    id 177
    label "base"
  ]
  node [
    id 178
    label "substancja"
  ]
  node [
    id 179
    label "prawid&#322;o"
  ]
  node [
    id 180
    label "prawo_Mendla"
  ]
  node [
    id 181
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 182
    label "criterion"
  ]
  node [
    id 183
    label "twierdzenie"
  ]
  node [
    id 184
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 185
    label "prawo"
  ]
  node [
    id 186
    label "occupation"
  ]
  node [
    id 187
    label "zasada_d'Alemberta"
  ]
  node [
    id 188
    label "si&#281;ga&#263;"
  ]
  node [
    id 189
    label "trwa&#263;"
  ]
  node [
    id 190
    label "obecno&#347;&#263;"
  ]
  node [
    id 191
    label "stan"
  ]
  node [
    id 192
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 193
    label "stand"
  ]
  node [
    id 194
    label "mie&#263;_miejsce"
  ]
  node [
    id 195
    label "uczestniczy&#263;"
  ]
  node [
    id 196
    label "chodzi&#263;"
  ]
  node [
    id 197
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 198
    label "equal"
  ]
  node [
    id 199
    label "obiekt"
  ]
  node [
    id 200
    label "istota"
  ]
  node [
    id 201
    label "wpada&#263;"
  ]
  node [
    id 202
    label "wpa&#347;&#263;"
  ]
  node [
    id 203
    label "przedmiot"
  ]
  node [
    id 204
    label "wpadanie"
  ]
  node [
    id 205
    label "kultura"
  ]
  node [
    id 206
    label "przyroda"
  ]
  node [
    id 207
    label "mienie"
  ]
  node [
    id 208
    label "wpadni&#281;cie"
  ]
  node [
    id 209
    label "ma&#322;o"
  ]
  node [
    id 210
    label "nielicznie"
  ]
  node [
    id 211
    label "ma&#322;y"
  ]
  node [
    id 212
    label "niewa&#380;ny"
  ]
  node [
    id 213
    label "piwo"
  ]
  node [
    id 214
    label "silny"
  ]
  node [
    id 215
    label "wa&#380;nie"
  ]
  node [
    id 216
    label "eksponowany"
  ]
  node [
    id 217
    label "istotnie"
  ]
  node [
    id 218
    label "znaczny"
  ]
  node [
    id 219
    label "dobry"
  ]
  node [
    id 220
    label "wynios&#322;y"
  ]
  node [
    id 221
    label "dono&#347;ny"
  ]
  node [
    id 222
    label "dokument"
  ]
  node [
    id 223
    label "edukt"
  ]
  node [
    id 224
    label "wyimek"
  ]
  node [
    id 225
    label "notatka"
  ]
  node [
    id 226
    label "wyci&#261;g"
  ]
  node [
    id 227
    label "szpitalnictwo"
  ]
  node [
    id 228
    label "oddzia&#322;_septyczny"
  ]
  node [
    id 229
    label "klinicysta"
  ]
  node [
    id 230
    label "zabieg&#243;wka"
  ]
  node [
    id 231
    label "izba_chorych"
  ]
  node [
    id 232
    label "&#322;&#243;&#380;eczko_nadziei"
  ]
  node [
    id 233
    label "blok_operacyjny"
  ]
  node [
    id 234
    label "instytucja"
  ]
  node [
    id 235
    label "centrum_urazowe"
  ]
  node [
    id 236
    label "plac&#243;wka_s&#322;u&#380;by_zdrowia"
  ]
  node [
    id 237
    label "kostnica"
  ]
  node [
    id 238
    label "oddzia&#322;"
  ]
  node [
    id 239
    label "sala_chorych"
  ]
  node [
    id 240
    label "od&#322;&#261;czenie"
  ]
  node [
    id 241
    label "przymus_bezpo&#347;redni"
  ]
  node [
    id 242
    label "od&#322;&#261;czy&#263;"
  ]
  node [
    id 243
    label "od&#322;&#261;czanie"
  ]
  node [
    id 244
    label "klient"
  ]
  node [
    id 245
    label "chory"
  ]
  node [
    id 246
    label "szpitalnik"
  ]
  node [
    id 247
    label "od&#322;&#261;cza&#263;"
  ]
  node [
    id 248
    label "przypadek"
  ]
  node [
    id 249
    label "piel&#281;gniarz"
  ]
  node [
    id 250
    label "wytwarza&#263;"
  ]
  node [
    id 251
    label "take"
  ]
  node [
    id 252
    label "return"
  ]
  node [
    id 253
    label "doj&#347;cie"
  ]
  node [
    id 254
    label "doj&#347;&#263;"
  ]
  node [
    id 255
    label "powzi&#261;&#263;"
  ]
  node [
    id 256
    label "wiedza"
  ]
  node [
    id 257
    label "sygna&#322;"
  ]
  node [
    id 258
    label "obiegni&#281;cie"
  ]
  node [
    id 259
    label "obieganie"
  ]
  node [
    id 260
    label "obiec"
  ]
  node [
    id 261
    label "dane"
  ]
  node [
    id 262
    label "obiega&#263;"
  ]
  node [
    id 263
    label "publikacja"
  ]
  node [
    id 264
    label "powzi&#281;cie"
  ]
  node [
    id 265
    label "uruchomienie"
  ]
  node [
    id 266
    label "nap&#322;yn&#261;&#263;"
  ]
  node [
    id 267
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 268
    label "supernadz&#243;r"
  ]
  node [
    id 269
    label "absolutorium"
  ]
  node [
    id 270
    label "nap&#322;yni&#281;cie"
  ]
  node [
    id 271
    label "podupada&#263;"
  ]
  node [
    id 272
    label "nap&#322;ywanie"
  ]
  node [
    id 273
    label "podupadanie"
  ]
  node [
    id 274
    label "kwestor"
  ]
  node [
    id 275
    label "uruchamia&#263;"
  ]
  node [
    id 276
    label "proces_koncentracji_kapita&#322;u"
  ]
  node [
    id 277
    label "uruchamianie"
  ]
  node [
    id 278
    label "czynnik_produkcji"
  ]
  node [
    id 279
    label "wy&#322;oi&#263;"
  ]
  node [
    id 280
    label "osi&#261;gn&#261;&#263;"
  ]
  node [
    id 281
    label "wyda&#263;"
  ]
  node [
    id 282
    label "pay"
  ]
  node [
    id 283
    label "zabuli&#263;"
  ]
  node [
    id 284
    label "uzyskiwa&#263;"
  ]
  node [
    id 285
    label "wystarcza&#263;"
  ]
  node [
    id 286
    label "range"
  ]
  node [
    id 287
    label "winnings"
  ]
  node [
    id 288
    label "opanowywa&#263;"
  ]
  node [
    id 289
    label "kupowa&#263;"
  ]
  node [
    id 290
    label "nabywa&#263;"
  ]
  node [
    id 291
    label "bra&#263;"
  ]
  node [
    id 292
    label "obskakiwa&#263;"
  ]
  node [
    id 293
    label "sanction"
  ]
  node [
    id 294
    label "certificate"
  ]
  node [
    id 295
    label "przy&#347;wiadczenie"
  ]
  node [
    id 296
    label "o&#347;wiadczenie"
  ]
  node [
    id 297
    label "stwierdzenie"
  ]
  node [
    id 298
    label "kontrasygnowanie"
  ]
  node [
    id 299
    label "zgodzenie_si&#281;"
  ]
  node [
    id 300
    label "upewnienie_si&#281;"
  ]
  node [
    id 301
    label "wierzenie"
  ]
  node [
    id 302
    label "mo&#380;liwy"
  ]
  node [
    id 303
    label "ufanie"
  ]
  node [
    id 304
    label "spokojny"
  ]
  node [
    id 305
    label "upewnianie_si&#281;"
  ]
  node [
    id 306
    label "model"
  ]
  node [
    id 307
    label "zbi&#243;r"
  ]
  node [
    id 308
    label "tryb"
  ]
  node [
    id 309
    label "narz&#281;dzie"
  ]
  node [
    id 310
    label "nature"
  ]
  node [
    id 311
    label "dostawa&#263;_si&#281;"
  ]
  node [
    id 312
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 313
    label "dolatywa&#263;"
  ]
  node [
    id 314
    label "happen"
  ]
  node [
    id 315
    label "zjawia&#263;_si&#281;"
  ]
  node [
    id 316
    label "hit"
  ]
  node [
    id 317
    label "spotyka&#263;"
  ]
  node [
    id 318
    label "znajdowa&#263;"
  ]
  node [
    id 319
    label "dosi&#281;ga&#263;"
  ]
  node [
    id 320
    label "pocisk"
  ]
  node [
    id 321
    label "dociera&#263;"
  ]
  node [
    id 322
    label "dopasowywa&#263;_si&#281;"
  ]
  node [
    id 323
    label "psychika"
  ]
  node [
    id 324
    label "psychoanaliza"
  ]
  node [
    id 325
    label "ekstraspekcja"
  ]
  node [
    id 326
    label "zemdle&#263;"
  ]
  node [
    id 327
    label "conscience"
  ]
  node [
    id 328
    label "Freud"
  ]
  node [
    id 329
    label "feeling"
  ]
  node [
    id 330
    label "ardor"
  ]
  node [
    id 331
    label "szkodliwie"
  ]
  node [
    id 332
    label "gor&#261;cy"
  ]
  node [
    id 333
    label "seksownie"
  ]
  node [
    id 334
    label "serdecznie"
  ]
  node [
    id 335
    label "g&#322;&#281;boko"
  ]
  node [
    id 336
    label "ciep&#322;o"
  ]
  node [
    id 337
    label "war"
  ]
  node [
    id 338
    label "przekonywa&#263;"
  ]
  node [
    id 339
    label "prompt"
  ]
  node [
    id 340
    label "u&#380;y&#263;"
  ]
  node [
    id 341
    label "uzyska&#263;"
  ]
  node [
    id 342
    label "utilize"
  ]
  node [
    id 343
    label "przyjemny"
  ]
  node [
    id 344
    label "po&#380;&#261;dany"
  ]
  node [
    id 345
    label "dobrze"
  ]
  node [
    id 346
    label "fajny"
  ]
  node [
    id 347
    label "pozytywnie"
  ]
  node [
    id 348
    label "dodatnio"
  ]
  node [
    id 349
    label "zbadanie"
  ]
  node [
    id 350
    label "skill"
  ]
  node [
    id 351
    label "wy&#347;wiadczenie"
  ]
  node [
    id 352
    label "znawstwo"
  ]
  node [
    id 353
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 354
    label "poczucie"
  ]
  node [
    id 355
    label "spotkanie"
  ]
  node [
    id 356
    label "do&#347;wiadczanie"
  ]
  node [
    id 357
    label "badanie"
  ]
  node [
    id 358
    label "assay"
  ]
  node [
    id 359
    label "obserwowanie"
  ]
  node [
    id 360
    label "checkup"
  ]
  node [
    id 361
    label "potraktowanie"
  ]
  node [
    id 362
    label "szko&#322;a"
  ]
  node [
    id 363
    label "eksperiencja"
  ]
  node [
    id 364
    label "simultaneously"
  ]
  node [
    id 365
    label "coincidentally"
  ]
  node [
    id 366
    label "synchronously"
  ]
  node [
    id 367
    label "concurrently"
  ]
  node [
    id 368
    label "jednoczesny"
  ]
  node [
    id 369
    label "wsp&#243;&#322;rz&#261;dzi&#263;"
  ]
  node [
    id 370
    label "hold"
  ]
  node [
    id 371
    label "s&#281;dziowa&#263;"
  ]
  node [
    id 372
    label "my&#347;le&#263;"
  ]
  node [
    id 373
    label "sprawowa&#263;"
  ]
  node [
    id 374
    label "os&#261;dza&#263;"
  ]
  node [
    id 375
    label "robi&#263;"
  ]
  node [
    id 376
    label "zas&#261;dza&#263;"
  ]
  node [
    id 377
    label "powodowa&#263;"
  ]
  node [
    id 378
    label "deliver"
  ]
  node [
    id 379
    label "du&#380;y"
  ]
  node [
    id 380
    label "jedyny"
  ]
  node [
    id 381
    label "kompletny"
  ]
  node [
    id 382
    label "zdr&#243;w"
  ]
  node [
    id 383
    label "&#380;ywy"
  ]
  node [
    id 384
    label "ca&#322;o"
  ]
  node [
    id 385
    label "pe&#322;ny"
  ]
  node [
    id 386
    label "calu&#347;ko"
  ]
  node [
    id 387
    label "podobny"
  ]
  node [
    id 388
    label "society"
  ]
  node [
    id 389
    label "jakobini"
  ]
  node [
    id 390
    label "klubista"
  ]
  node [
    id 391
    label "stowarzyszenie"
  ]
  node [
    id 392
    label "lokal"
  ]
  node [
    id 393
    label "od&#322;am"
  ]
  node [
    id 394
    label "siedziba"
  ]
  node [
    id 395
    label "bar"
  ]
  node [
    id 396
    label "zapewnia&#263;"
  ]
  node [
    id 397
    label "poda&#263;"
  ]
  node [
    id 398
    label "obiecywa&#263;"
  ]
  node [
    id 399
    label "obieca&#263;"
  ]
  node [
    id 400
    label "bespeak"
  ]
  node [
    id 401
    label "sign"
  ]
  node [
    id 402
    label "podawa&#263;"
  ]
  node [
    id 403
    label "zapewni&#263;"
  ]
  node [
    id 404
    label "czynny"
  ]
  node [
    id 405
    label "uczynnienie"
  ]
  node [
    id 406
    label "czynnie"
  ]
  node [
    id 407
    label "realny"
  ]
  node [
    id 408
    label "aktywnie"
  ]
  node [
    id 409
    label "ciekawy"
  ]
  node [
    id 410
    label "intensywny"
  ]
  node [
    id 411
    label "faktyczny"
  ]
  node [
    id 412
    label "istotny"
  ]
  node [
    id 413
    label "zdolny"
  ]
  node [
    id 414
    label "uczynnianie"
  ]
  node [
    id 415
    label "stosunek_pracy"
  ]
  node [
    id 416
    label "zatrudnienie_si&#281;"
  ]
  node [
    id 417
    label "benedykty&#324;ski"
  ]
  node [
    id 418
    label "pracowanie"
  ]
  node [
    id 419
    label "zaw&#243;d"
  ]
  node [
    id 420
    label "kierownictwo"
  ]
  node [
    id 421
    label "zmiana"
  ]
  node [
    id 422
    label "wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 423
    label "wytw&#243;r"
  ]
  node [
    id 424
    label "zaanga&#380;owa&#263;_si&#281;"
  ]
  node [
    id 425
    label "tynkarski"
  ]
  node [
    id 426
    label "marchwiane_r&#281;ce"
  ]
  node [
    id 427
    label "zobowi&#261;zanie"
  ]
  node [
    id 428
    label "zmianowo&#347;&#263;"
  ]
  node [
    id 429
    label "czynno&#347;&#263;"
  ]
  node [
    id 430
    label "tyrka"
  ]
  node [
    id 431
    label "pracowa&#263;"
  ]
  node [
    id 432
    label "poda&#380;_pracy"
  ]
  node [
    id 433
    label "zak&#322;ad"
  ]
  node [
    id 434
    label "zatrudni&#263;_si&#281;"
  ]
  node [
    id 435
    label "najem"
  ]
  node [
    id 436
    label "dyplomata"
  ]
  node [
    id 437
    label "wys&#322;annik"
  ]
  node [
    id 438
    label "kurier_dyplomatyczny"
  ]
  node [
    id 439
    label "ablegat"
  ]
  node [
    id 440
    label "Miko&#322;ajczyk"
  ]
  node [
    id 441
    label "Korwin"
  ]
  node [
    id 442
    label "parlamentarzysta"
  ]
  node [
    id 443
    label "dyscyplina_partyjna"
  ]
  node [
    id 444
    label "izba_ni&#380;sza"
  ]
  node [
    id 445
    label "poselstwo"
  ]
  node [
    id 446
    label "lacki"
  ]
  node [
    id 447
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 448
    label "sztajer"
  ]
  node [
    id 449
    label "drabant"
  ]
  node [
    id 450
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 451
    label "polak"
  ]
  node [
    id 452
    label "pierogi_ruskie"
  ]
  node [
    id 453
    label "krakowiak"
  ]
  node [
    id 454
    label "Polish"
  ]
  node [
    id 455
    label "j&#281;zyk"
  ]
  node [
    id 456
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 457
    label "oberek"
  ]
  node [
    id 458
    label "po_polsku"
  ]
  node [
    id 459
    label "mazur"
  ]
  node [
    id 460
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 461
    label "chodzony"
  ]
  node [
    id 462
    label "skoczny"
  ]
  node [
    id 463
    label "ryba_po_grecku"
  ]
  node [
    id 464
    label "goniony"
  ]
  node [
    id 465
    label "polsko"
  ]
  node [
    id 466
    label "SLD"
  ]
  node [
    id 467
    label "Partia_Republika&#324;ska"
  ]
  node [
    id 468
    label "m&#322;odzie&#380;&#243;wka"
  ]
  node [
    id 469
    label "ZChN"
  ]
  node [
    id 470
    label "Wigowie"
  ]
  node [
    id 471
    label "egzekutywa"
  ]
  node [
    id 472
    label "unit"
  ]
  node [
    id 473
    label "blok"
  ]
  node [
    id 474
    label "Razem"
  ]
  node [
    id 475
    label "partia"
  ]
  node [
    id 476
    label "organizacja"
  ]
  node [
    id 477
    label "si&#322;a"
  ]
  node [
    id 478
    label "PiS"
  ]
  node [
    id 479
    label "Polska_Partia_Socjalistyczna"
  ]
  node [
    id 480
    label "Bund"
  ]
  node [
    id 481
    label "AWS"
  ]
  node [
    id 482
    label "Polska_Zjednoczona_Partia_Robotnicza"
  ]
  node [
    id 483
    label "Kuomintang"
  ]
  node [
    id 484
    label "Jakobici"
  ]
  node [
    id 485
    label "PSL"
  ]
  node [
    id 486
    label "Federali&#347;ci"
  ]
  node [
    id 487
    label "ZSL"
  ]
  node [
    id 488
    label "PPR"
  ]
  node [
    id 489
    label "Komunistyczna_Partia_Polski"
  ]
  node [
    id 490
    label "PO"
  ]
  node [
    id 491
    label "wiejski"
  ]
  node [
    id 492
    label "folk"
  ]
  node [
    id 493
    label "publiczny"
  ]
  node [
    id 494
    label "etniczny"
  ]
  node [
    id 495
    label "ludowo"
  ]
  node [
    id 496
    label "express"
  ]
  node [
    id 497
    label "translate"
  ]
  node [
    id 498
    label "zaproponowa&#263;"
  ]
  node [
    id 499
    label "wyja&#347;ni&#263;"
  ]
  node [
    id 500
    label "wyst&#261;pi&#263;"
  ]
  node [
    id 501
    label "device"
  ]
  node [
    id 502
    label "program_u&#380;ytkowy"
  ]
  node [
    id 503
    label "intencja"
  ]
  node [
    id 504
    label "agreement"
  ]
  node [
    id 505
    label "pomys&#322;"
  ]
  node [
    id 506
    label "maksymalna_awaria_projektowa"
  ]
  node [
    id 507
    label "plan"
  ]
  node [
    id 508
    label "dokumentacja"
  ]
  node [
    id 509
    label "Karta_Nauczyciela"
  ]
  node [
    id 510
    label "marc&#243;wka"
  ]
  node [
    id 511
    label "wi&#281;kszo&#347;&#263;_kwalifikowana"
  ]
  node [
    id 512
    label "akt"
  ]
  node [
    id 513
    label "przej&#347;&#263;"
  ]
  node [
    id 514
    label "charter"
  ]
  node [
    id 515
    label "przej&#347;cie"
  ]
  node [
    id 516
    label "odmawia&#263;"
  ]
  node [
    id 517
    label "wycofywa&#263;_si&#281;"
  ]
  node [
    id 518
    label "sk&#322;ada&#263;"
  ]
  node [
    id 519
    label "thank"
  ]
  node [
    id 520
    label "odwdzi&#281;cza&#263;_si&#281;"
  ]
  node [
    id 521
    label "wyra&#380;a&#263;"
  ]
  node [
    id 522
    label "etykieta"
  ]
  node [
    id 523
    label "szlachetny"
  ]
  node [
    id 524
    label "pi&#281;kny"
  ]
  node [
    id 525
    label "okazale"
  ]
  node [
    id 526
    label "&#378;le"
  ]
  node [
    id 527
    label "wspaniale"
  ]
  node [
    id 528
    label "skandalicznie"
  ]
  node [
    id 529
    label "beautifully"
  ]
  node [
    id 530
    label "oklaski"
  ]
  node [
    id 531
    label "kla&#347;ni&#281;cie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 0
    target 66
  ]
  edge [
    source 0
    target 67
  ]
  edge [
    source 0
    target 68
  ]
  edge [
    source 0
    target 69
  ]
  edge [
    source 0
    target 70
  ]
  edge [
    source 0
    target 71
  ]
  edge [
    source 0
    target 72
  ]
  edge [
    source 0
    target 73
  ]
  edge [
    source 0
    target 74
  ]
  edge [
    source 0
    target 75
  ]
  edge [
    source 0
    target 76
  ]
  edge [
    source 0
    target 77
  ]
  edge [
    source 0
    target 78
  ]
  edge [
    source 0
    target 79
  ]
  edge [
    source 0
    target 80
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 81
  ]
  edge [
    source 1
    target 82
  ]
  edge [
    source 1
    target 83
  ]
  edge [
    source 1
    target 84
  ]
  edge [
    source 1
    target 85
  ]
  edge [
    source 1
    target 86
  ]
  edge [
    source 1
    target 87
  ]
  edge [
    source 1
    target 88
  ]
  edge [
    source 1
    target 89
  ]
  edge [
    source 1
    target 90
  ]
  edge [
    source 1
    target 91
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 92
  ]
  edge [
    source 2
    target 93
  ]
  edge [
    source 2
    target 94
  ]
  edge [
    source 2
    target 95
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 96
  ]
  edge [
    source 2
    target 97
  ]
  edge [
    source 2
    target 98
  ]
  edge [
    source 2
    target 99
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 100
  ]
  edge [
    source 4
    target 101
  ]
  edge [
    source 4
    target 102
  ]
  edge [
    source 4
    target 103
  ]
  edge [
    source 4
    target 104
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 105
  ]
  edge [
    source 5
    target 106
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 107
  ]
  edge [
    source 6
    target 108
  ]
  edge [
    source 6
    target 109
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 7
    target 116
  ]
  edge [
    source 7
    target 117
  ]
  edge [
    source 7
    target 118
  ]
  edge [
    source 7
    target 119
  ]
  edge [
    source 7
    target 120
  ]
  edge [
    source 7
    target 121
  ]
  edge [
    source 7
    target 122
  ]
  edge [
    source 7
    target 123
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 13
  ]
  edge [
    source 8
    target 28
  ]
  edge [
    source 8
    target 17
  ]
  edge [
    source 8
    target 124
  ]
  edge [
    source 8
    target 125
  ]
  edge [
    source 8
    target 126
  ]
  edge [
    source 8
    target 127
  ]
  edge [
    source 8
    target 128
  ]
  edge [
    source 8
    target 129
  ]
  edge [
    source 8
    target 130
  ]
  edge [
    source 8
    target 131
  ]
  edge [
    source 8
    target 132
  ]
  edge [
    source 8
    target 133
  ]
  edge [
    source 8
    target 134
  ]
  edge [
    source 8
    target 135
  ]
  edge [
    source 8
    target 136
  ]
  edge [
    source 8
    target 137
  ]
  edge [
    source 8
    target 138
  ]
  edge [
    source 8
    target 139
  ]
  edge [
    source 8
    target 140
  ]
  edge [
    source 8
    target 39
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 13
  ]
  edge [
    source 9
    target 141
  ]
  edge [
    source 9
    target 142
  ]
  edge [
    source 9
    target 143
  ]
  edge [
    source 9
    target 144
  ]
  edge [
    source 9
    target 145
  ]
  edge [
    source 9
    target 146
  ]
  edge [
    source 9
    target 38
  ]
  edge [
    source 9
    target 41
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 147
  ]
  edge [
    source 10
    target 148
  ]
  edge [
    source 10
    target 149
  ]
  edge [
    source 10
    target 150
  ]
  edge [
    source 10
    target 151
  ]
  edge [
    source 10
    target 152
  ]
  edge [
    source 10
    target 153
  ]
  edge [
    source 10
    target 154
  ]
  edge [
    source 10
    target 155
  ]
  edge [
    source 10
    target 156
  ]
  edge [
    source 10
    target 157
  ]
  edge [
    source 10
    target 158
  ]
  edge [
    source 10
    target 159
  ]
  edge [
    source 10
    target 160
  ]
  edge [
    source 10
    target 161
  ]
  edge [
    source 10
    target 162
  ]
  edge [
    source 10
    target 42
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 163
  ]
  edge [
    source 12
    target 164
  ]
  edge [
    source 12
    target 165
  ]
  edge [
    source 12
    target 166
  ]
  edge [
    source 12
    target 167
  ]
  edge [
    source 12
    target 168
  ]
  edge [
    source 12
    target 169
  ]
  edge [
    source 12
    target 170
  ]
  edge [
    source 12
    target 171
  ]
  edge [
    source 12
    target 172
  ]
  edge [
    source 12
    target 173
  ]
  edge [
    source 12
    target 174
  ]
  edge [
    source 12
    target 175
  ]
  edge [
    source 12
    target 176
  ]
  edge [
    source 12
    target 177
  ]
  edge [
    source 12
    target 178
  ]
  edge [
    source 12
    target 34
  ]
  edge [
    source 12
    target 179
  ]
  edge [
    source 12
    target 180
  ]
  edge [
    source 12
    target 181
  ]
  edge [
    source 12
    target 182
  ]
  edge [
    source 12
    target 183
  ]
  edge [
    source 12
    target 184
  ]
  edge [
    source 12
    target 185
  ]
  edge [
    source 12
    target 186
  ]
  edge [
    source 12
    target 187
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 33
  ]
  edge [
    source 13
    target 188
  ]
  edge [
    source 13
    target 189
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 192
  ]
  edge [
    source 13
    target 193
  ]
  edge [
    source 13
    target 194
  ]
  edge [
    source 13
    target 195
  ]
  edge [
    source 13
    target 196
  ]
  edge [
    source 13
    target 197
  ]
  edge [
    source 13
    target 198
  ]
  edge [
    source 13
    target 30
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 199
  ]
  edge [
    source 14
    target 92
  ]
  edge [
    source 14
    target 200
  ]
  edge [
    source 14
    target 201
  ]
  edge [
    source 14
    target 202
  ]
  edge [
    source 14
    target 203
  ]
  edge [
    source 14
    target 204
  ]
  edge [
    source 14
    target 205
  ]
  edge [
    source 14
    target 206
  ]
  edge [
    source 14
    target 207
  ]
  edge [
    source 14
    target 98
  ]
  edge [
    source 14
    target 208
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 209
  ]
  edge [
    source 16
    target 210
  ]
  edge [
    source 16
    target 211
  ]
  edge [
    source 16
    target 212
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 28
  ]
  edge [
    source 17
    target 213
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 214
  ]
  edge [
    source 18
    target 215
  ]
  edge [
    source 18
    target 216
  ]
  edge [
    source 18
    target 217
  ]
  edge [
    source 18
    target 218
  ]
  edge [
    source 18
    target 219
  ]
  edge [
    source 18
    target 220
  ]
  edge [
    source 18
    target 221
  ]
  edge [
    source 18
    target 49
  ]
  edge [
    source 18
    target 37
  ]
  edge [
    source 18
    target 43
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 85
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 222
  ]
  edge [
    source 21
    target 223
  ]
  edge [
    source 21
    target 224
  ]
  edge [
    source 21
    target 225
  ]
  edge [
    source 21
    target 226
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 32
  ]
  edge [
    source 22
    target 227
  ]
  edge [
    source 22
    target 228
  ]
  edge [
    source 22
    target 229
  ]
  edge [
    source 22
    target 230
  ]
  edge [
    source 22
    target 231
  ]
  edge [
    source 22
    target 232
  ]
  edge [
    source 22
    target 233
  ]
  edge [
    source 22
    target 234
  ]
  edge [
    source 22
    target 235
  ]
  edge [
    source 22
    target 236
  ]
  edge [
    source 22
    target 237
  ]
  edge [
    source 22
    target 238
  ]
  edge [
    source 22
    target 239
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 240
  ]
  edge [
    source 23
    target 100
  ]
  edge [
    source 23
    target 241
  ]
  edge [
    source 23
    target 242
  ]
  edge [
    source 23
    target 243
  ]
  edge [
    source 23
    target 244
  ]
  edge [
    source 23
    target 245
  ]
  edge [
    source 23
    target 246
  ]
  edge [
    source 23
    target 247
  ]
  edge [
    source 23
    target 248
  ]
  edge [
    source 23
    target 249
  ]
  edge [
    source 23
    target 40
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 250
  ]
  edge [
    source 24
    target 251
  ]
  edge [
    source 24
    target 30
  ]
  edge [
    source 24
    target 252
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 253
  ]
  edge [
    source 25
    target 254
  ]
  edge [
    source 25
    target 255
  ]
  edge [
    source 25
    target 256
  ]
  edge [
    source 25
    target 257
  ]
  edge [
    source 25
    target 258
  ]
  edge [
    source 25
    target 259
  ]
  edge [
    source 25
    target 260
  ]
  edge [
    source 25
    target 261
  ]
  edge [
    source 25
    target 262
  ]
  edge [
    source 25
    target 77
  ]
  edge [
    source 25
    target 263
  ]
  edge [
    source 25
    target 264
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 265
  ]
  edge [
    source 27
    target 266
  ]
  edge [
    source 27
    target 267
  ]
  edge [
    source 27
    target 268
  ]
  edge [
    source 27
    target 269
  ]
  edge [
    source 27
    target 270
  ]
  edge [
    source 27
    target 271
  ]
  edge [
    source 27
    target 272
  ]
  edge [
    source 27
    target 273
  ]
  edge [
    source 27
    target 274
  ]
  edge [
    source 27
    target 275
  ]
  edge [
    source 27
    target 207
  ]
  edge [
    source 27
    target 276
  ]
  edge [
    source 27
    target 277
  ]
  edge [
    source 27
    target 234
  ]
  edge [
    source 27
    target 278
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 279
  ]
  edge [
    source 28
    target 280
  ]
  edge [
    source 28
    target 281
  ]
  edge [
    source 28
    target 162
  ]
  edge [
    source 28
    target 282
  ]
  edge [
    source 28
    target 283
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 190
  ]
  edge [
    source 29
    target 36
  ]
  edge [
    source 30
    target 31
  ]
  edge [
    source 30
    target 188
  ]
  edge [
    source 30
    target 284
  ]
  edge [
    source 30
    target 285
  ]
  edge [
    source 30
    target 286
  ]
  edge [
    source 30
    target 287
  ]
  edge [
    source 30
    target 194
  ]
  edge [
    source 30
    target 288
  ]
  edge [
    source 30
    target 289
  ]
  edge [
    source 30
    target 290
  ]
  edge [
    source 30
    target 291
  ]
  edge [
    source 30
    target 292
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 31
    target 222
  ]
  edge [
    source 31
    target 293
  ]
  edge [
    source 31
    target 294
  ]
  edge [
    source 31
    target 295
  ]
  edge [
    source 31
    target 296
  ]
  edge [
    source 31
    target 297
  ]
  edge [
    source 31
    target 298
  ]
  edge [
    source 31
    target 299
  ]
  edge [
    source 31
    target 33
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 33
    target 300
  ]
  edge [
    source 33
    target 301
  ]
  edge [
    source 33
    target 302
  ]
  edge [
    source 33
    target 303
  ]
  edge [
    source 33
    target 85
  ]
  edge [
    source 33
    target 304
  ]
  edge [
    source 33
    target 305
  ]
  edge [
    source 34
    target 35
  ]
  edge [
    source 34
    target 306
  ]
  edge [
    source 34
    target 307
  ]
  edge [
    source 34
    target 308
  ]
  edge [
    source 34
    target 309
  ]
  edge [
    source 34
    target 310
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 311
  ]
  edge [
    source 35
    target 201
  ]
  edge [
    source 35
    target 312
  ]
  edge [
    source 35
    target 313
  ]
  edge [
    source 35
    target 314
  ]
  edge [
    source 35
    target 315
  ]
  edge [
    source 35
    target 316
  ]
  edge [
    source 35
    target 317
  ]
  edge [
    source 35
    target 156
  ]
  edge [
    source 35
    target 318
  ]
  edge [
    source 35
    target 319
  ]
  edge [
    source 35
    target 320
  ]
  edge [
    source 35
    target 321
  ]
  edge [
    source 35
    target 322
  ]
  edge [
    source 36
    target 37
  ]
  edge [
    source 36
    target 191
  ]
  edge [
    source 36
    target 323
  ]
  edge [
    source 36
    target 324
  ]
  edge [
    source 36
    target 256
  ]
  edge [
    source 36
    target 325
  ]
  edge [
    source 36
    target 326
  ]
  edge [
    source 36
    target 327
  ]
  edge [
    source 36
    target 328
  ]
  edge [
    source 36
    target 329
  ]
  edge [
    source 37
    target 38
  ]
  edge [
    source 37
    target 330
  ]
  edge [
    source 37
    target 331
  ]
  edge [
    source 37
    target 332
  ]
  edge [
    source 37
    target 333
  ]
  edge [
    source 37
    target 334
  ]
  edge [
    source 37
    target 335
  ]
  edge [
    source 37
    target 336
  ]
  edge [
    source 37
    target 337
  ]
  edge [
    source 37
    target 61
  ]
  edge [
    source 37
    target 43
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 38
    target 338
  ]
  edge [
    source 38
    target 339
  ]
  edge [
    source 38
    target 41
  ]
  edge [
    source 39
    target 40
  ]
  edge [
    source 39
    target 160
  ]
  edge [
    source 39
    target 340
  ]
  edge [
    source 39
    target 341
  ]
  edge [
    source 39
    target 342
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 41
    target 42
  ]
  edge [
    source 41
    target 343
  ]
  edge [
    source 41
    target 344
  ]
  edge [
    source 41
    target 345
  ]
  edge [
    source 41
    target 346
  ]
  edge [
    source 41
    target 347
  ]
  edge [
    source 41
    target 348
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 349
  ]
  edge [
    source 42
    target 350
  ]
  edge [
    source 42
    target 351
  ]
  edge [
    source 42
    target 352
  ]
  edge [
    source 42
    target 256
  ]
  edge [
    source 42
    target 353
  ]
  edge [
    source 42
    target 354
  ]
  edge [
    source 42
    target 355
  ]
  edge [
    source 42
    target 356
  ]
  edge [
    source 42
    target 74
  ]
  edge [
    source 42
    target 357
  ]
  edge [
    source 42
    target 358
  ]
  edge [
    source 42
    target 359
  ]
  edge [
    source 42
    target 360
  ]
  edge [
    source 42
    target 361
  ]
  edge [
    source 42
    target 362
  ]
  edge [
    source 42
    target 363
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 43
    target 364
  ]
  edge [
    source 43
    target 365
  ]
  edge [
    source 43
    target 366
  ]
  edge [
    source 43
    target 367
  ]
  edge [
    source 43
    target 368
  ]
  edge [
    source 44
    target 45
  ]
  edge [
    source 44
    target 369
  ]
  edge [
    source 44
    target 370
  ]
  edge [
    source 44
    target 371
  ]
  edge [
    source 44
    target 372
  ]
  edge [
    source 44
    target 373
  ]
  edge [
    source 44
    target 374
  ]
  edge [
    source 44
    target 375
  ]
  edge [
    source 44
    target 376
  ]
  edge [
    source 44
    target 377
  ]
  edge [
    source 44
    target 378
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 46
    target 47
  ]
  edge [
    source 46
    target 379
  ]
  edge [
    source 46
    target 380
  ]
  edge [
    source 46
    target 381
  ]
  edge [
    source 46
    target 382
  ]
  edge [
    source 46
    target 383
  ]
  edge [
    source 46
    target 384
  ]
  edge [
    source 46
    target 385
  ]
  edge [
    source 46
    target 386
  ]
  edge [
    source 46
    target 387
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 388
  ]
  edge [
    source 47
    target 389
  ]
  edge [
    source 47
    target 390
  ]
  edge [
    source 47
    target 391
  ]
  edge [
    source 47
    target 392
  ]
  edge [
    source 47
    target 393
  ]
  edge [
    source 47
    target 394
  ]
  edge [
    source 47
    target 395
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 48
    target 396
  ]
  edge [
    source 48
    target 397
  ]
  edge [
    source 48
    target 398
  ]
  edge [
    source 48
    target 399
  ]
  edge [
    source 48
    target 400
  ]
  edge [
    source 48
    target 401
  ]
  edge [
    source 48
    target 402
  ]
  edge [
    source 48
    target 403
  ]
  edge [
    source 49
    target 50
  ]
  edge [
    source 49
    target 404
  ]
  edge [
    source 49
    target 405
  ]
  edge [
    source 49
    target 65
  ]
  edge [
    source 49
    target 406
  ]
  edge [
    source 49
    target 407
  ]
  edge [
    source 49
    target 408
  ]
  edge [
    source 49
    target 409
  ]
  edge [
    source 49
    target 410
  ]
  edge [
    source 49
    target 411
  ]
  edge [
    source 49
    target 412
  ]
  edge [
    source 49
    target 413
  ]
  edge [
    source 49
    target 414
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 50
    target 415
  ]
  edge [
    source 50
    target 416
  ]
  edge [
    source 50
    target 417
  ]
  edge [
    source 50
    target 418
  ]
  edge [
    source 50
    target 419
  ]
  edge [
    source 50
    target 420
  ]
  edge [
    source 50
    target 421
  ]
  edge [
    source 50
    target 422
  ]
  edge [
    source 50
    target 423
  ]
  edge [
    source 50
    target 424
  ]
  edge [
    source 50
    target 425
  ]
  edge [
    source 50
    target 278
  ]
  edge [
    source 50
    target 426
  ]
  edge [
    source 50
    target 427
  ]
  edge [
    source 50
    target 428
  ]
  edge [
    source 50
    target 429
  ]
  edge [
    source 50
    target 430
  ]
  edge [
    source 50
    target 431
  ]
  edge [
    source 50
    target 394
  ]
  edge [
    source 50
    target 432
  ]
  edge [
    source 50
    target 66
  ]
  edge [
    source 50
    target 433
  ]
  edge [
    source 50
    target 434
  ]
  edge [
    source 50
    target 435
  ]
  edge [
    source 51
    target 52
  ]
  edge [
    source 51
    target 436
  ]
  edge [
    source 51
    target 437
  ]
  edge [
    source 51
    target 102
  ]
  edge [
    source 51
    target 438
  ]
  edge [
    source 51
    target 439
  ]
  edge [
    source 51
    target 390
  ]
  edge [
    source 51
    target 440
  ]
  edge [
    source 51
    target 441
  ]
  edge [
    source 51
    target 442
  ]
  edge [
    source 51
    target 443
  ]
  edge [
    source 51
    target 444
  ]
  edge [
    source 51
    target 445
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 52
    target 446
  ]
  edge [
    source 52
    target 447
  ]
  edge [
    source 52
    target 203
  ]
  edge [
    source 52
    target 448
  ]
  edge [
    source 52
    target 449
  ]
  edge [
    source 52
    target 450
  ]
  edge [
    source 52
    target 451
  ]
  edge [
    source 52
    target 452
  ]
  edge [
    source 52
    target 453
  ]
  edge [
    source 52
    target 454
  ]
  edge [
    source 52
    target 455
  ]
  edge [
    source 52
    target 456
  ]
  edge [
    source 52
    target 457
  ]
  edge [
    source 52
    target 458
  ]
  edge [
    source 52
    target 459
  ]
  edge [
    source 52
    target 460
  ]
  edge [
    source 52
    target 461
  ]
  edge [
    source 52
    target 462
  ]
  edge [
    source 52
    target 463
  ]
  edge [
    source 52
    target 464
  ]
  edge [
    source 52
    target 465
  ]
  edge [
    source 53
    target 54
  ]
  edge [
    source 53
    target 466
  ]
  edge [
    source 53
    target 467
  ]
  edge [
    source 53
    target 468
  ]
  edge [
    source 53
    target 469
  ]
  edge [
    source 53
    target 470
  ]
  edge [
    source 53
    target 471
  ]
  edge [
    source 53
    target 472
  ]
  edge [
    source 53
    target 473
  ]
  edge [
    source 53
    target 474
  ]
  edge [
    source 53
    target 475
  ]
  edge [
    source 53
    target 476
  ]
  edge [
    source 53
    target 477
  ]
  edge [
    source 53
    target 478
  ]
  edge [
    source 53
    target 479
  ]
  edge [
    source 53
    target 480
  ]
  edge [
    source 53
    target 481
  ]
  edge [
    source 53
    target 482
  ]
  edge [
    source 53
    target 483
  ]
  edge [
    source 53
    target 484
  ]
  edge [
    source 53
    target 485
  ]
  edge [
    source 53
    target 486
  ]
  edge [
    source 53
    target 487
  ]
  edge [
    source 53
    target 488
  ]
  edge [
    source 53
    target 489
  ]
  edge [
    source 53
    target 490
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 491
  ]
  edge [
    source 54
    target 492
  ]
  edge [
    source 54
    target 493
  ]
  edge [
    source 54
    target 494
  ]
  edge [
    source 54
    target 495
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 56
    target 57
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 496
  ]
  edge [
    source 57
    target 497
  ]
  edge [
    source 57
    target 498
  ]
  edge [
    source 57
    target 499
  ]
  edge [
    source 57
    target 500
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 222
  ]
  edge [
    source 58
    target 501
  ]
  edge [
    source 58
    target 502
  ]
  edge [
    source 58
    target 503
  ]
  edge [
    source 58
    target 504
  ]
  edge [
    source 58
    target 505
  ]
  edge [
    source 58
    target 506
  ]
  edge [
    source 58
    target 507
  ]
  edge [
    source 58
    target 508
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 59
    target 509
  ]
  edge [
    source 59
    target 510
  ]
  edge [
    source 59
    target 511
  ]
  edge [
    source 59
    target 512
  ]
  edge [
    source 59
    target 513
  ]
  edge [
    source 59
    target 514
  ]
  edge [
    source 59
    target 515
  ]
  edge [
    source 60
    target 61
  ]
  edge [
    source 60
    target 516
  ]
  edge [
    source 60
    target 517
  ]
  edge [
    source 60
    target 518
  ]
  edge [
    source 60
    target 519
  ]
  edge [
    source 60
    target 520
  ]
  edge [
    source 60
    target 521
  ]
  edge [
    source 60
    target 522
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 61
    target 523
  ]
  edge [
    source 61
    target 524
  ]
  edge [
    source 61
    target 525
  ]
  edge [
    source 61
    target 345
  ]
  edge [
    source 61
    target 526
  ]
  edge [
    source 61
    target 527
  ]
  edge [
    source 61
    target 528
  ]
  edge [
    source 61
    target 529
  ]
  edge [
    source 62
    target 530
  ]
  edge [
    source 62
    target 531
  ]
]
