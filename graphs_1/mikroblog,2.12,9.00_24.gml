graph [
  maxDegree 21
  minDegree 1
  meanDegree 1.9090909090909092
  density 0.09090909090909091
  graphCliqueNumber 2
  node [
    id 0
    label "poznan"
    origin "text"
  ]
  node [
    id 1
    label "fotografia"
    origin "text"
  ]
  node [
    id 2
    label "bezcieniowy"
  ]
  node [
    id 3
    label "wyretuszowanie"
  ]
  node [
    id 4
    label "przedstawienie"
  ]
  node [
    id 5
    label "fota"
  ]
  node [
    id 6
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 7
    label "podlew"
  ]
  node [
    id 8
    label "ziarno"
  ]
  node [
    id 9
    label "obraz"
  ]
  node [
    id 10
    label "legitymacja"
  ]
  node [
    id 11
    label "przepa&#322;"
  ]
  node [
    id 12
    label "fotogaleria"
  ]
  node [
    id 13
    label "winieta"
  ]
  node [
    id 14
    label "retuszowanie"
  ]
  node [
    id 15
    label "monid&#322;o"
  ]
  node [
    id 16
    label "talbotypia"
  ]
  node [
    id 17
    label "wyretuszowa&#263;"
  ]
  node [
    id 18
    label "fototeka"
  ]
  node [
    id 19
    label "photograph"
  ]
  node [
    id 20
    label "ekspozycja"
  ]
  node [
    id 21
    label "retuszowa&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 3
  ]
  edge [
    source 1
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 1
    target 20
  ]
  edge [
    source 1
    target 21
  ]
]
