graph [
  maxDegree 40
  minDegree 1
  meanDegree 2.440677966101695
  density 0.010385863685539128
  graphCliqueNumber 8
  node [
    id 0
    label "szko&#322;a"
    origin "text"
  ]
  node [
    id 1
    label "podstawowy"
    origin "text"
  ]
  node [
    id 2
    label "pr&#243;bowa&#263;"
    origin "text"
  ]
  node [
    id 3
    label "swoje"
    origin "text"
  ]
  node [
    id 4
    label "si&#322;a"
    origin "text"
  ]
  node [
    id 5
    label "k&#243;&#322;ka"
    origin "text"
  ]
  node [
    id 6
    label "teatralny"
    origin "text"
  ]
  node [
    id 7
    label "dziennikarski"
    origin "text"
  ]
  node [
    id 8
    label "j&#281;zykowy"
    origin "text"
  ]
  node [
    id 9
    label "p&#243;&#378;no"
    origin "text"
  ]
  node [
    id 10
    label "przyj&#347;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "radio"
    origin "text"
  ]
  node [
    id 12
    label "&#322;&#243;d&#378;"
    origin "text"
  ]
  node [
    id 13
    label "lista"
    origin "text"
  ]
  node [
    id 14
    label "przeboje"
    origin "text"
  ]
  node [
    id 15
    label "adam"
    origin "text"
  ]
  node [
    id 16
    label "ko&#322;aci&#324;ski"
    origin "text"
  ]
  node [
    id 17
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 18
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 19
    label "pozna&#263;"
    origin "text"
  ]
  node [
    id 20
    label "podszewka"
    origin "text"
  ]
  node [
    id 21
    label "zainteresowania"
    origin "text"
  ]
  node [
    id 22
    label "humanistyczny"
    origin "text"
  ]
  node [
    id 23
    label "przeszkodzi&#263;"
    origin "text"
  ]
  node [
    id 24
    label "uko&#324;czenie"
    origin "text"
  ]
  node [
    id 25
    label "studia"
    origin "text"
  ]
  node [
    id 26
    label "ekonomiczny"
    origin "text"
  ]
  node [
    id 27
    label "Mickiewicz"
  ]
  node [
    id 28
    label "czas"
  ]
  node [
    id 29
    label "szkolenie"
  ]
  node [
    id 30
    label "przepisa&#263;"
  ]
  node [
    id 31
    label "lesson"
  ]
  node [
    id 32
    label "grupa"
  ]
  node [
    id 33
    label "praktyka"
  ]
  node [
    id 34
    label "metoda"
  ]
  node [
    id 35
    label "niepokalanki"
  ]
  node [
    id 36
    label "kara"
  ]
  node [
    id 37
    label "zda&#263;"
  ]
  node [
    id 38
    label "form"
  ]
  node [
    id 39
    label "kwalifikacje"
  ]
  node [
    id 40
    label "system"
  ]
  node [
    id 41
    label "przepisanie"
  ]
  node [
    id 42
    label "sztuba"
  ]
  node [
    id 43
    label "wiedza"
  ]
  node [
    id 44
    label "stopek"
  ]
  node [
    id 45
    label "school"
  ]
  node [
    id 46
    label "absolwent"
  ]
  node [
    id 47
    label "urszulanki"
  ]
  node [
    id 48
    label "gabinet"
  ]
  node [
    id 49
    label "zesp&#243;&#322;_szk&#243;&#322;"
  ]
  node [
    id 50
    label "ideologia"
  ]
  node [
    id 51
    label "lekcja"
  ]
  node [
    id 52
    label "muzyka"
  ]
  node [
    id 53
    label "podr&#281;cznik"
  ]
  node [
    id 54
    label "zdanie"
  ]
  node [
    id 55
    label "siedziba"
  ]
  node [
    id 56
    label "sekretariat"
  ]
  node [
    id 57
    label "nauka"
  ]
  node [
    id 58
    label "do&#347;wiadczenie"
  ]
  node [
    id 59
    label "tablica"
  ]
  node [
    id 60
    label "teren_szko&#322;y"
  ]
  node [
    id 61
    label "instytucja"
  ]
  node [
    id 62
    label "plac&#243;wka_o&#347;wiatowo-wychowawcza"
  ]
  node [
    id 63
    label "skolaryzacja"
  ]
  node [
    id 64
    label "&#322;awa_szkolna"
  ]
  node [
    id 65
    label "klasa"
  ]
  node [
    id 66
    label "pocz&#261;tkowy"
  ]
  node [
    id 67
    label "podstawowo"
  ]
  node [
    id 68
    label "najwa&#380;niejszy"
  ]
  node [
    id 69
    label "niezaawansowany"
  ]
  node [
    id 70
    label "przygotowywa&#263;_si&#281;"
  ]
  node [
    id 71
    label "feel"
  ]
  node [
    id 72
    label "przedstawienie"
  ]
  node [
    id 73
    label "try"
  ]
  node [
    id 74
    label "zajmowa&#263;_si&#281;"
  ]
  node [
    id 75
    label "pos&#322;ugiwa&#263;_si&#281;"
  ]
  node [
    id 76
    label "spo&#380;ywa&#263;"
  ]
  node [
    id 77
    label "sprawdza&#263;"
  ]
  node [
    id 78
    label "stara&#263;_si&#281;"
  ]
  node [
    id 79
    label "kosztowa&#263;"
  ]
  node [
    id 80
    label "wojsko"
  ]
  node [
    id 81
    label "magnitude"
  ]
  node [
    id 82
    label "energia"
  ]
  node [
    id 83
    label "capacity"
  ]
  node [
    id 84
    label "wuchta"
  ]
  node [
    id 85
    label "cecha"
  ]
  node [
    id 86
    label "parametr"
  ]
  node [
    id 87
    label "moment_si&#322;y"
  ]
  node [
    id 88
    label "przemoc"
  ]
  node [
    id 89
    label "zdolno&#347;&#263;"
  ]
  node [
    id 90
    label "mn&#243;stwo"
  ]
  node [
    id 91
    label "wektorowa_wielko&#347;&#263;_fizyczna"
  ]
  node [
    id 92
    label "rozwi&#261;zanie"
  ]
  node [
    id 93
    label "zbiorowo&#347;&#263;"
  ]
  node [
    id 94
    label "potencja"
  ]
  node [
    id 95
    label "zjawisko"
  ]
  node [
    id 96
    label "zaleta"
  ]
  node [
    id 97
    label "przyrz&#261;d_gimnastyczny"
  ]
  node [
    id 98
    label "nadmierny"
  ]
  node [
    id 99
    label "nienaturalny"
  ]
  node [
    id 100
    label "teatralnie"
  ]
  node [
    id 101
    label "zawodowy"
  ]
  node [
    id 102
    label "rzetelny"
  ]
  node [
    id 103
    label "tre&#347;ciwy"
  ]
  node [
    id 104
    label "po_dziennikarsku"
  ]
  node [
    id 105
    label "dziennikarsko"
  ]
  node [
    id 106
    label "obiektywny"
  ]
  node [
    id 107
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 108
    label "wzorowy"
  ]
  node [
    id 109
    label "typowy"
  ]
  node [
    id 110
    label "komunikacyjny"
  ]
  node [
    id 111
    label "p&#243;&#378;ny"
  ]
  node [
    id 112
    label "sta&#263;_si&#281;"
  ]
  node [
    id 113
    label "zaistnie&#263;"
  ]
  node [
    id 114
    label "doj&#347;&#263;"
  ]
  node [
    id 115
    label "become"
  ]
  node [
    id 116
    label "line_up"
  ]
  node [
    id 117
    label "przyby&#263;"
  ]
  node [
    id 118
    label "uk&#322;ad"
  ]
  node [
    id 119
    label "paj&#281;czarz"
  ]
  node [
    id 120
    label "fala_radiowa"
  ]
  node [
    id 121
    label "spot"
  ]
  node [
    id 122
    label "programowiec"
  ]
  node [
    id 123
    label "urz&#261;dzenie_RTV"
  ]
  node [
    id 124
    label "eliminator"
  ]
  node [
    id 125
    label "studio"
  ]
  node [
    id 126
    label "radiola"
  ]
  node [
    id 127
    label "redakcja"
  ]
  node [
    id 128
    label "odbieranie"
  ]
  node [
    id 129
    label "dyskryminator"
  ]
  node [
    id 130
    label "odbiera&#263;"
  ]
  node [
    id 131
    label "odbiornik"
  ]
  node [
    id 132
    label "media"
  ]
  node [
    id 133
    label "urz&#261;dzenie_radiowe"
  ]
  node [
    id 134
    label "stacja"
  ]
  node [
    id 135
    label "radiolinia"
  ]
  node [
    id 136
    label "radiofonia"
  ]
  node [
    id 137
    label "regaty"
  ]
  node [
    id 138
    label "statek"
  ]
  node [
    id 139
    label "spalin&#243;wka"
  ]
  node [
    id 140
    label "pok&#322;ad"
  ]
  node [
    id 141
    label "ster"
  ]
  node [
    id 142
    label "kratownica"
  ]
  node [
    id 143
    label "pojazd_niemechaniczny"
  ]
  node [
    id 144
    label "drzewce"
  ]
  node [
    id 145
    label "wyliczanka"
  ]
  node [
    id 146
    label "catalog"
  ]
  node [
    id 147
    label "stock"
  ]
  node [
    id 148
    label "figurowa&#263;"
  ]
  node [
    id 149
    label "zbi&#243;r"
  ]
  node [
    id 150
    label "book"
  ]
  node [
    id 151
    label "pozycja"
  ]
  node [
    id 152
    label "tekst"
  ]
  node [
    id 153
    label "sumariusz"
  ]
  node [
    id 154
    label "komplikacja"
  ]
  node [
    id 155
    label "k&#322;opot"
  ]
  node [
    id 156
    label "bonanza"
  ]
  node [
    id 157
    label "przyswoi&#263;"
  ]
  node [
    id 158
    label "zetkn&#261;&#263;_si&#281;"
  ]
  node [
    id 159
    label "teach"
  ]
  node [
    id 160
    label "zrozumie&#263;"
  ]
  node [
    id 161
    label "umo&#380;liwi&#263;"
  ]
  node [
    id 162
    label "rozr&#243;&#380;ni&#263;"
  ]
  node [
    id 163
    label "topographic_point"
  ]
  node [
    id 164
    label "experience"
  ]
  node [
    id 165
    label "zdarzy&#263;_si&#281;"
  ]
  node [
    id 166
    label "domy&#347;li&#263;_si&#281;"
  ]
  node [
    id 167
    label "visualize"
  ]
  node [
    id 168
    label "str&#243;j"
  ]
  node [
    id 169
    label "tkanina"
  ]
  node [
    id 170
    label "sp&#243;d"
  ]
  node [
    id 171
    label "podszycie"
  ]
  node [
    id 172
    label "tendency"
  ]
  node [
    id 173
    label "feblik"
  ]
  node [
    id 174
    label "sk&#322;onno&#347;&#263;"
  ]
  node [
    id 175
    label "wyr&#380;n&#261;&#263;"
  ]
  node [
    id 176
    label "zajawka"
  ]
  node [
    id 177
    label "humanistycznie"
  ]
  node [
    id 178
    label "intervene"
  ]
  node [
    id 179
    label "utrudni&#263;"
  ]
  node [
    id 180
    label "dzia&#322;anie"
  ]
  node [
    id 181
    label "zrobienie"
  ]
  node [
    id 182
    label "zako&#324;czenie"
  ]
  node [
    id 183
    label "uczenie_si&#281;"
  ]
  node [
    id 184
    label "termination"
  ]
  node [
    id 185
    label "completion"
  ]
  node [
    id 186
    label "badanie"
  ]
  node [
    id 187
    label "ekonomicznie"
  ]
  node [
    id 188
    label "korzystny"
  ]
  node [
    id 189
    label "oszcz&#281;dny"
  ]
  node [
    id 190
    label "przeb&#243;j"
  ]
  node [
    id 191
    label "Adam"
  ]
  node [
    id 192
    label "Edyta"
  ]
  node [
    id 193
    label "flis"
  ]
  node [
    id 194
    label "&#347;wiatowy"
  ]
  node [
    id 195
    label "przegl&#261;d"
  ]
  node [
    id 196
    label "folklor"
  ]
  node [
    id 197
    label "&#8222;"
  ]
  node [
    id 198
    label "integracja"
  ]
  node [
    id 199
    label "&#8221;"
  ]
  node [
    id 200
    label "wyspa"
  ]
  node [
    id 201
    label "fundacja"
  ]
  node [
    id 202
    label "nowy"
  ]
  node [
    id 203
    label "medium"
  ]
  node [
    id 204
    label "internetowy"
  ]
  node [
    id 205
    label "wirtualny"
  ]
  node [
    id 206
    label "konkurs"
  ]
  node [
    id 207
    label "chopinowski"
  ]
  node [
    id 208
    label "&#8211;"
  ]
  node [
    id 209
    label "Chopin"
  ]
  node [
    id 210
    label "Garage"
  ]
  node [
    id 211
    label "bando"
  ]
  node [
    id 212
    label "Marcin"
  ]
  node [
    id 213
    label "grudzie&#324;"
  ]
  node [
    id 214
    label "m&#322;odzie&#380;owy"
  ]
  node [
    id 215
    label "akcja"
  ]
  node [
    id 216
    label "multimedialny"
  ]
  node [
    id 217
    label "stowarzyszy&#263;"
  ]
  node [
    id 218
    label "m&#322;odzi"
  ]
  node [
    id 219
    label "dziennikarz"
  ]
  node [
    id 220
    label "polisa"
  ]
  node [
    id 221
    label "unia"
  ]
  node [
    id 222
    label "polski"
  ]
  node [
    id 223
    label "PKO"
  ]
  node [
    id 224
    label "b&#322;ogos&#322;awionej&#160;pami&#281;ci"
  ]
  node [
    id 225
    label "ergo"
  ]
  node [
    id 226
    label "Hestii"
  ]
  node [
    id 227
    label "Micha&#322;"
  ]
  node [
    id 228
    label "Horbulewicz"
  ]
  node [
    id 229
    label "pod"
  ]
  node [
    id 230
    label "wiatr"
  ]
  node [
    id 231
    label "plus"
  ]
  node [
    id 232
    label "Bydgoszcz"
  ]
  node [
    id 233
    label "TVP"
  ]
  node [
    id 234
    label "monitor"
  ]
  node [
    id 235
    label "wiadomo&#347;&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 0
    target 42
  ]
  edge [
    source 0
    target 43
  ]
  edge [
    source 0
    target 44
  ]
  edge [
    source 0
    target 45
  ]
  edge [
    source 0
    target 46
  ]
  edge [
    source 0
    target 47
  ]
  edge [
    source 0
    target 48
  ]
  edge [
    source 0
    target 49
  ]
  edge [
    source 0
    target 50
  ]
  edge [
    source 0
    target 51
  ]
  edge [
    source 0
    target 52
  ]
  edge [
    source 0
    target 53
  ]
  edge [
    source 0
    target 54
  ]
  edge [
    source 0
    target 55
  ]
  edge [
    source 0
    target 56
  ]
  edge [
    source 0
    target 57
  ]
  edge [
    source 0
    target 58
  ]
  edge [
    source 0
    target 59
  ]
  edge [
    source 0
    target 60
  ]
  edge [
    source 0
    target 61
  ]
  edge [
    source 0
    target 62
  ]
  edge [
    source 0
    target 63
  ]
  edge [
    source 0
    target 64
  ]
  edge [
    source 0
    target 65
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 66
  ]
  edge [
    source 1
    target 67
  ]
  edge [
    source 1
    target 68
  ]
  edge [
    source 1
    target 69
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 70
  ]
  edge [
    source 2
    target 71
  ]
  edge [
    source 2
    target 72
  ]
  edge [
    source 2
    target 73
  ]
  edge [
    source 2
    target 74
  ]
  edge [
    source 2
    target 75
  ]
  edge [
    source 2
    target 76
  ]
  edge [
    source 2
    target 77
  ]
  edge [
    source 2
    target 78
  ]
  edge [
    source 2
    target 79
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 4
    target 89
  ]
  edge [
    source 4
    target 90
  ]
  edge [
    source 4
    target 91
  ]
  edge [
    source 4
    target 92
  ]
  edge [
    source 4
    target 93
  ]
  edge [
    source 4
    target 94
  ]
  edge [
    source 4
    target 95
  ]
  edge [
    source 4
    target 96
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 98
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 101
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 110
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 111
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 112
  ]
  edge [
    source 10
    target 113
  ]
  edge [
    source 10
    target 28
  ]
  edge [
    source 10
    target 114
  ]
  edge [
    source 10
    target 115
  ]
  edge [
    source 10
    target 116
  ]
  edge [
    source 10
    target 117
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 19
  ]
  edge [
    source 11
    target 20
  ]
  edge [
    source 11
    target 118
  ]
  edge [
    source 11
    target 119
  ]
  edge [
    source 11
    target 120
  ]
  edge [
    source 11
    target 121
  ]
  edge [
    source 11
    target 122
  ]
  edge [
    source 11
    target 123
  ]
  edge [
    source 11
    target 124
  ]
  edge [
    source 11
    target 125
  ]
  edge [
    source 11
    target 126
  ]
  edge [
    source 11
    target 127
  ]
  edge [
    source 11
    target 128
  ]
  edge [
    source 11
    target 129
  ]
  edge [
    source 11
    target 130
  ]
  edge [
    source 11
    target 131
  ]
  edge [
    source 11
    target 132
  ]
  edge [
    source 11
    target 133
  ]
  edge [
    source 11
    target 134
  ]
  edge [
    source 11
    target 135
  ]
  edge [
    source 11
    target 136
  ]
  edge [
    source 11
    target 231
  ]
  edge [
    source 11
    target 232
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 137
  ]
  edge [
    source 12
    target 138
  ]
  edge [
    source 12
    target 139
  ]
  edge [
    source 12
    target 140
  ]
  edge [
    source 12
    target 141
  ]
  edge [
    source 12
    target 142
  ]
  edge [
    source 12
    target 143
  ]
  edge [
    source 12
    target 144
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 145
  ]
  edge [
    source 13
    target 146
  ]
  edge [
    source 13
    target 147
  ]
  edge [
    source 13
    target 148
  ]
  edge [
    source 13
    target 149
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 13
    target 152
  ]
  edge [
    source 13
    target 153
  ]
  edge [
    source 13
    target 22
  ]
  edge [
    source 13
    target 190
  ]
  edge [
    source 13
    target 191
  ]
  edge [
    source 13
    target 16
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 190
  ]
  edge [
    source 16
    target 191
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 19
    target 157
  ]
  edge [
    source 19
    target 71
  ]
  edge [
    source 19
    target 158
  ]
  edge [
    source 19
    target 159
  ]
  edge [
    source 19
    target 160
  ]
  edge [
    source 19
    target 161
  ]
  edge [
    source 19
    target 162
  ]
  edge [
    source 19
    target 163
  ]
  edge [
    source 19
    target 164
  ]
  edge [
    source 19
    target 165
  ]
  edge [
    source 19
    target 166
  ]
  edge [
    source 19
    target 167
  ]
  edge [
    source 19
    target 194
  ]
  edge [
    source 19
    target 195
  ]
  edge [
    source 19
    target 196
  ]
  edge [
    source 19
    target 197
  ]
  edge [
    source 19
    target 198
  ]
  edge [
    source 19
    target 199
  ]
  edge [
    source 19
    target 200
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 168
  ]
  edge [
    source 20
    target 169
  ]
  edge [
    source 20
    target 170
  ]
  edge [
    source 20
    target 171
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 172
  ]
  edge [
    source 21
    target 173
  ]
  edge [
    source 21
    target 174
  ]
  edge [
    source 21
    target 175
  ]
  edge [
    source 21
    target 176
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 22
    target 177
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 23
    target 178
  ]
  edge [
    source 23
    target 179
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 180
  ]
  edge [
    source 24
    target 181
  ]
  edge [
    source 24
    target 182
  ]
  edge [
    source 24
    target 183
  ]
  edge [
    source 24
    target 184
  ]
  edge [
    source 24
    target 185
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 186
  ]
  edge [
    source 25
    target 57
  ]
  edge [
    source 26
    target 187
  ]
  edge [
    source 26
    target 188
  ]
  edge [
    source 26
    target 189
  ]
  edge [
    source 190
    target 191
  ]
  edge [
    source 192
    target 193
  ]
  edge [
    source 194
    target 195
  ]
  edge [
    source 194
    target 196
  ]
  edge [
    source 194
    target 197
  ]
  edge [
    source 194
    target 198
  ]
  edge [
    source 194
    target 199
  ]
  edge [
    source 194
    target 200
  ]
  edge [
    source 195
    target 196
  ]
  edge [
    source 195
    target 197
  ]
  edge [
    source 195
    target 198
  ]
  edge [
    source 195
    target 199
  ]
  edge [
    source 195
    target 200
  ]
  edge [
    source 196
    target 197
  ]
  edge [
    source 196
    target 198
  ]
  edge [
    source 196
    target 199
  ]
  edge [
    source 196
    target 200
  ]
  edge [
    source 197
    target 198
  ]
  edge [
    source 197
    target 199
  ]
  edge [
    source 197
    target 200
  ]
  edge [
    source 197
    target 217
  ]
  edge [
    source 197
    target 218
  ]
  edge [
    source 197
    target 219
  ]
  edge [
    source 197
    target 220
  ]
  edge [
    source 198
    target 199
  ]
  edge [
    source 198
    target 200
  ]
  edge [
    source 199
    target 200
  ]
  edge [
    source 199
    target 217
  ]
  edge [
    source 199
    target 218
  ]
  edge [
    source 199
    target 219
  ]
  edge [
    source 199
    target 220
  ]
  edge [
    source 201
    target 202
  ]
  edge [
    source 201
    target 203
  ]
  edge [
    source 202
    target 203
  ]
  edge [
    source 204
    target 205
  ]
  edge [
    source 204
    target 206
  ]
  edge [
    source 204
    target 207
  ]
  edge [
    source 204
    target 208
  ]
  edge [
    source 204
    target 209
  ]
  edge [
    source 204
    target 210
  ]
  edge [
    source 204
    target 211
  ]
  edge [
    source 205
    target 206
  ]
  edge [
    source 205
    target 207
  ]
  edge [
    source 205
    target 208
  ]
  edge [
    source 205
    target 209
  ]
  edge [
    source 205
    target 210
  ]
  edge [
    source 205
    target 211
  ]
  edge [
    source 206
    target 207
  ]
  edge [
    source 206
    target 208
  ]
  edge [
    source 206
    target 209
  ]
  edge [
    source 206
    target 210
  ]
  edge [
    source 206
    target 211
  ]
  edge [
    source 207
    target 208
  ]
  edge [
    source 207
    target 209
  ]
  edge [
    source 207
    target 210
  ]
  edge [
    source 207
    target 211
  ]
  edge [
    source 208
    target 209
  ]
  edge [
    source 208
    target 210
  ]
  edge [
    source 208
    target 211
  ]
  edge [
    source 209
    target 210
  ]
  edge [
    source 209
    target 211
  ]
  edge [
    source 210
    target 211
  ]
  edge [
    source 212
    target 213
  ]
  edge [
    source 214
    target 215
  ]
  edge [
    source 214
    target 216
  ]
  edge [
    source 215
    target 216
  ]
  edge [
    source 217
    target 218
  ]
  edge [
    source 217
    target 219
  ]
  edge [
    source 217
    target 220
  ]
  edge [
    source 218
    target 219
  ]
  edge [
    source 218
    target 220
  ]
  edge [
    source 219
    target 220
  ]
  edge [
    source 221
    target 222
  ]
  edge [
    source 223
    target 224
  ]
  edge [
    source 225
    target 226
  ]
  edge [
    source 227
    target 228
  ]
  edge [
    source 229
    target 230
  ]
  edge [
    source 231
    target 232
  ]
  edge [
    source 232
    target 233
  ]
  edge [
    source 234
    target 235
  ]
]
