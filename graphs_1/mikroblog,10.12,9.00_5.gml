graph [
  maxDegree 8
  minDegree 1
  meanDegree 1.8461538461538463
  density 0.15384615384615385
  graphCliqueNumber 2
  node [
    id 0
    label "teraz"
    origin "text"
  ]
  node [
    id 1
    label "musza"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "ukrywa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "chwila"
  ]
  node [
    id 5
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 6
    label "report"
  ]
  node [
    id 7
    label "zachowywa&#263;"
  ]
  node [
    id 8
    label "zawiera&#263;"
  ]
  node [
    id 9
    label "cache"
  ]
  node [
    id 10
    label "meliniarz"
  ]
  node [
    id 11
    label "suppress"
  ]
  node [
    id 12
    label "umieszcza&#263;"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 0
    target 5
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 6
  ]
  edge [
    source 3
    target 7
  ]
  edge [
    source 3
    target 8
  ]
  edge [
    source 3
    target 9
  ]
  edge [
    source 3
    target 10
  ]
  edge [
    source 3
    target 11
  ]
  edge [
    source 3
    target 12
  ]
]
