graph [
  maxDegree 15
  minDegree 1
  meanDegree 1.8095238095238095
  density 0.09047619047619047
  graphCliqueNumber 2
  node [
    id 0
    label "spawalnictwo"
    origin "text"
  ]
  node [
    id 1
    label "budownictwo"
    origin "text"
  ]
  node [
    id 2
    label "przemys&#322;"
  ]
  node [
    id 3
    label "stopiwo"
  ]
  node [
    id 4
    label "welding"
  ]
  node [
    id 5
    label "styl_architektoniczny"
  ]
  node [
    id 6
    label "sztuki_pi&#281;kne"
  ]
  node [
    id 7
    label "murarstwo"
  ]
  node [
    id 8
    label "labirynt"
  ]
  node [
    id 9
    label "zgarniacz"
  ]
  node [
    id 10
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 11
    label "lateryt"
  ]
  node [
    id 12
    label "architektura_rezydencjonalna"
  ]
  node [
    id 13
    label "wymian"
  ]
  node [
    id 14
    label "og&#243;lnobudowlany"
  ]
  node [
    id 15
    label "iniekcyjny"
  ]
  node [
    id 16
    label "computer_architecture"
  ]
  node [
    id 17
    label "rozpierak"
  ]
  node [
    id 18
    label "nauka"
  ]
  node [
    id 19
    label "Stefan"
  ]
  node [
    id 20
    label "bry&#322;a"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 2
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
  edge [
    source 1
    target 8
  ]
  edge [
    source 1
    target 9
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 1
    target 11
  ]
  edge [
    source 1
    target 12
  ]
  edge [
    source 1
    target 13
  ]
  edge [
    source 1
    target 14
  ]
  edge [
    source 1
    target 15
  ]
  edge [
    source 1
    target 16
  ]
  edge [
    source 1
    target 17
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 19
    target 20
  ]
]
