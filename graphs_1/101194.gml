graph [
  maxDegree 36
  minDegree 1
  meanDegree 1.9428571428571428
  density 0.028157349896480333
  graphCliqueNumber 4
  node [
    id 0
    label "antoni"
    origin "text"
  ]
  node [
    id 1
    label "urba&#324;ski"
    origin "text"
  ]
  node [
    id 2
    label "polityk"
    origin "text"
  ]
  node [
    id 3
    label "J&#281;drzejewicz"
  ]
  node [
    id 4
    label "Sto&#322;ypin"
  ]
  node [
    id 5
    label "Nixon"
  ]
  node [
    id 6
    label "Perykles"
  ]
  node [
    id 7
    label "bezpartyjny"
  ]
  node [
    id 8
    label "Gomu&#322;ka"
  ]
  node [
    id 9
    label "Gorbaczow"
  ]
  node [
    id 10
    label "Borel"
  ]
  node [
    id 11
    label "Katon"
  ]
  node [
    id 12
    label "McCarthy"
  ]
  node [
    id 13
    label "Gierek"
  ]
  node [
    id 14
    label "Naser"
  ]
  node [
    id 15
    label "Goebbels"
  ]
  node [
    id 16
    label "Ko&#322;&#322;&#261;taj"
  ]
  node [
    id 17
    label "Jaros&#322;aw_Kaczy&#324;ski"
  ]
  node [
    id 18
    label "de_Gaulle"
  ]
  node [
    id 19
    label "Vladimir_Me&#269;iar"
  ]
  node [
    id 20
    label "Bre&#380;niew"
  ]
  node [
    id 21
    label "Juliusz_Cezar"
  ]
  node [
    id 22
    label "Bierut"
  ]
  node [
    id 23
    label "Kuro&#324;"
  ]
  node [
    id 24
    label "Arafat"
  ]
  node [
    id 25
    label "Fidel_Castro"
  ]
  node [
    id 26
    label "Moczar"
  ]
  node [
    id 27
    label "Miko&#322;ajczyk"
  ]
  node [
    id 28
    label "Korwin"
  ]
  node [
    id 29
    label "dzia&#322;acz"
  ]
  node [
    id 30
    label "Winston_Churchill"
  ]
  node [
    id 31
    label "Leszek_Miller"
  ]
  node [
    id 32
    label "Ziobro"
  ]
  node [
    id 33
    label "Mao"
  ]
  node [
    id 34
    label "Chruszczow"
  ]
  node [
    id 35
    label "Putin"
  ]
  node [
    id 36
    label "Falandysz"
  ]
  node [
    id 37
    label "Metternich"
  ]
  node [
    id 38
    label "Antoni"
  ]
  node [
    id 39
    label "Urba&#324;ski"
  ]
  node [
    id 40
    label "d&#261;b"
  ]
  node [
    id 41
    label "szlachecki"
  ]
  node [
    id 42
    label "krajowy"
  ]
  node [
    id 43
    label "rada"
  ]
  node [
    id 44
    label "narodowy"
  ]
  node [
    id 45
    label "sejm"
  ]
  node [
    id 46
    label "ustawodawczy"
  ]
  node [
    id 47
    label "wydzia&#322;"
  ]
  node [
    id 48
    label "Prawno"
  ]
  node [
    id 49
    label "ekonomiczny"
  ]
  node [
    id 50
    label "uniwersytet"
  ]
  node [
    id 51
    label "pozna&#324;ski"
  ]
  node [
    id 52
    label "akademia"
  ]
  node [
    id 53
    label "handlowy"
  ]
  node [
    id 54
    label "ii"
  ]
  node [
    id 55
    label "wojna"
  ]
  node [
    id 56
    label "&#347;wiatowy"
  ]
  node [
    id 57
    label "pozna&#324;sko"
  ]
  node [
    id 58
    label "warszawski"
  ]
  node [
    id 59
    label "towarzystwo"
  ]
  node [
    id 60
    label "ubezpieczeniowy"
  ]
  node [
    id 61
    label "pa&#324;stwowy"
  ]
  node [
    id 62
    label "centrala"
  ]
  node [
    id 63
    label "rzemie&#347;lniczy"
  ]
  node [
    id 64
    label "centralny"
  ]
  node [
    id 65
    label "zwi&#261;zek"
  ]
  node [
    id 66
    label "sp&#243;&#322;dzielczo&#347;&#263;"
  ]
  node [
    id 67
    label "praca"
  ]
  node [
    id 68
    label "Franciszka"
  ]
  node [
    id 69
    label "Ma&#324;kowski"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 4
  ]
  edge [
    source 2
    target 5
  ]
  edge [
    source 2
    target 6
  ]
  edge [
    source 2
    target 7
  ]
  edge [
    source 2
    target 8
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 2
    target 28
  ]
  edge [
    source 2
    target 29
  ]
  edge [
    source 2
    target 30
  ]
  edge [
    source 2
    target 31
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 38
    target 39
  ]
  edge [
    source 40
    target 41
  ]
  edge [
    source 42
    target 43
  ]
  edge [
    source 42
    target 44
  ]
  edge [
    source 43
    target 44
  ]
  edge [
    source 45
    target 46
  ]
  edge [
    source 47
    target 48
  ]
  edge [
    source 47
    target 49
  ]
  edge [
    source 48
    target 49
  ]
  edge [
    source 50
    target 51
  ]
  edge [
    source 52
    target 53
  ]
  edge [
    source 53
    target 61
  ]
  edge [
    source 53
    target 62
  ]
  edge [
    source 54
    target 55
  ]
  edge [
    source 54
    target 56
  ]
  edge [
    source 55
    target 56
  ]
  edge [
    source 57
    target 58
  ]
  edge [
    source 57
    target 59
  ]
  edge [
    source 57
    target 60
  ]
  edge [
    source 58
    target 59
  ]
  edge [
    source 58
    target 60
  ]
  edge [
    source 59
    target 60
  ]
  edge [
    source 61
    target 62
  ]
  edge [
    source 62
    target 63
  ]
  edge [
    source 64
    target 65
  ]
  edge [
    source 64
    target 66
  ]
  edge [
    source 64
    target 67
  ]
  edge [
    source 65
    target 66
  ]
  edge [
    source 65
    target 67
  ]
  edge [
    source 66
    target 67
  ]
  edge [
    source 68
    target 69
  ]
]
