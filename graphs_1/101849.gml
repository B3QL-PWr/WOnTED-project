graph [
  maxDegree 34
  minDegree 1
  meanDegree 2.096774193548387
  density 0.011333914559721011
  graphCliqueNumber 3
  node [
    id 0
    label "creative"
    origin "text"
  ]
  node [
    id 1
    label "commons"
    origin "text"
  ]
  node [
    id 2
    label "polska"
    origin "text"
  ]
  node [
    id 3
    label "ko&#322;o"
    origin "text"
  ]
  node [
    id 4
    label "naukowy"
    origin "text"
  ]
  node [
    id 5
    label "w&#322;asno&#347;&#263;"
    origin "text"
  ]
  node [
    id 6
    label "intelektualny"
    origin "text"
  ]
  node [
    id 7
    label "wydzia&#322;"
    origin "text"
  ]
  node [
    id 8
    label "prawa"
    origin "text"
  ]
  node [
    id 9
    label "administracja"
    origin "text"
  ]
  node [
    id 10
    label "uniwersytet"
    origin "text"
  ]
  node [
    id 11
    label "warszawskie"
    origin "text"
  ]
  node [
    id 12
    label "organizowa&#263;"
    origin "text"
  ]
  node [
    id 13
    label "konferencja"
    origin "text"
  ]
  node [
    id 14
    label "zatytu&#322;owa&#263;"
    origin "text"
  ]
  node [
    id 15
    label "kultura"
    origin "text"
  ]
  node [
    id 16
    label "by&#263;"
    origin "text"
  ]
  node [
    id 17
    label "legalny"
    origin "text"
  ]
  node [
    id 18
    label "polski"
    origin "text"
  ]
  node [
    id 19
    label "prawie"
    origin "text"
  ]
  node [
    id 20
    label "autorski"
    origin "text"
  ]
  node [
    id 21
    label "odcinek_ko&#322;a"
  ]
  node [
    id 22
    label "whip"
  ]
  node [
    id 23
    label "przedmiot"
  ]
  node [
    id 24
    label "grupa"
  ]
  node [
    id 25
    label "zabawa"
  ]
  node [
    id 26
    label "gang"
  ]
  node [
    id 27
    label "obr&#281;cz"
  ]
  node [
    id 28
    label "pi"
  ]
  node [
    id 29
    label "zwolnica"
  ]
  node [
    id 30
    label "piasta"
  ]
  node [
    id 31
    label "pojazd"
  ]
  node [
    id 32
    label "&#322;amanie"
  ]
  node [
    id 33
    label "o&#347;"
  ]
  node [
    id 34
    label "okr&#261;g"
  ]
  node [
    id 35
    label "figura_ograniczona"
  ]
  node [
    id 36
    label "stowarzyszenie"
  ]
  node [
    id 37
    label "figura_geometryczna"
  ]
  node [
    id 38
    label "kolokwium"
  ]
  node [
    id 39
    label "sphere"
  ]
  node [
    id 40
    label "p&#243;&#322;kole"
  ]
  node [
    id 41
    label "lap"
  ]
  node [
    id 42
    label "podwozie"
  ]
  node [
    id 43
    label "sejmik"
  ]
  node [
    id 44
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 45
    label "&#322;ama&#263;"
  ]
  node [
    id 46
    label "narz&#281;dzie_tortur"
  ]
  node [
    id 47
    label "specjalny"
  ]
  node [
    id 48
    label "edukacyjnie"
  ]
  node [
    id 49
    label "skomplikowany"
  ]
  node [
    id 50
    label "zgodny"
  ]
  node [
    id 51
    label "naukowo"
  ]
  node [
    id 52
    label "scjentyficzny"
  ]
  node [
    id 53
    label "teoretyczny"
  ]
  node [
    id 54
    label "specjalistyczny"
  ]
  node [
    id 55
    label "rodowo&#347;&#263;"
  ]
  node [
    id 56
    label "prawo_rzeczowe"
  ]
  node [
    id 57
    label "possession"
  ]
  node [
    id 58
    label "stan"
  ]
  node [
    id 59
    label "dobra"
  ]
  node [
    id 60
    label "charakterystyka"
  ]
  node [
    id 61
    label "wyci&#261;gni&#281;cie"
  ]
  node [
    id 62
    label "patent"
  ]
  node [
    id 63
    label "mienie"
  ]
  node [
    id 64
    label "przej&#347;&#263;"
  ]
  node [
    id 65
    label "attribute"
  ]
  node [
    id 66
    label "wyci&#261;gn&#261;&#263;"
  ]
  node [
    id 67
    label "przej&#347;cie"
  ]
  node [
    id 68
    label "g&#322;&#281;boki"
  ]
  node [
    id 69
    label "inteligentny"
  ]
  node [
    id 70
    label "wznios&#322;y"
  ]
  node [
    id 71
    label "umys&#322;owy"
  ]
  node [
    id 72
    label "intelektualnie"
  ]
  node [
    id 73
    label "my&#347;l&#261;cy"
  ]
  node [
    id 74
    label "podsekcja"
  ]
  node [
    id 75
    label "whole"
  ]
  node [
    id 76
    label "relation"
  ]
  node [
    id 77
    label "politechnika"
  ]
  node [
    id 78
    label "katedra"
  ]
  node [
    id 79
    label "urz&#261;d"
  ]
  node [
    id 80
    label "jednostka_organizacyjna"
  ]
  node [
    id 81
    label "insourcing"
  ]
  node [
    id 82
    label "ministerstwo"
  ]
  node [
    id 83
    label "miejsce_pracy"
  ]
  node [
    id 84
    label "dzia&#322;"
  ]
  node [
    id 85
    label "zarz&#261;d"
  ]
  node [
    id 86
    label "gospodarka"
  ]
  node [
    id 87
    label "petent"
  ]
  node [
    id 88
    label "biuro"
  ]
  node [
    id 89
    label "dziekanat"
  ]
  node [
    id 90
    label "struktura"
  ]
  node [
    id 91
    label "siedziba"
  ]
  node [
    id 92
    label "ku&#378;nia"
  ]
  node [
    id 93
    label "Harvard"
  ]
  node [
    id 94
    label "uczelnia"
  ]
  node [
    id 95
    label "Sorbona"
  ]
  node [
    id 96
    label "Uniwersytet_Oksfordzki"
  ]
  node [
    id 97
    label "Stanford"
  ]
  node [
    id 98
    label "Princeton"
  ]
  node [
    id 99
    label "academy"
  ]
  node [
    id 100
    label "szko&#322;a_wy&#380;sza"
  ]
  node [
    id 101
    label "planowa&#263;"
  ]
  node [
    id 102
    label "dostosowywa&#263;"
  ]
  node [
    id 103
    label "pozyskiwa&#263;"
  ]
  node [
    id 104
    label "wprowadza&#263;"
  ]
  node [
    id 105
    label "treat"
  ]
  node [
    id 106
    label "przygotowywa&#263;"
  ]
  node [
    id 107
    label "create"
  ]
  node [
    id 108
    label "ensnare"
  ]
  node [
    id 109
    label "tworzy&#263;"
  ]
  node [
    id 110
    label "standard"
  ]
  node [
    id 111
    label "skupia&#263;"
  ]
  node [
    id 112
    label "konferencyjka"
  ]
  node [
    id 113
    label "Poczdam"
  ]
  node [
    id 114
    label "conference"
  ]
  node [
    id 115
    label "spotkanie"
  ]
  node [
    id 116
    label "grusza_pospolita"
  ]
  node [
    id 117
    label "Ja&#322;ta"
  ]
  node [
    id 118
    label "nazwa&#263;"
  ]
  node [
    id 119
    label "title"
  ]
  node [
    id 120
    label "tw&#243;rczo&#347;&#263;"
  ]
  node [
    id 121
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 122
    label "Wsch&#243;d"
  ]
  node [
    id 123
    label "rzecz"
  ]
  node [
    id 124
    label "grzeczno&#347;&#263;"
  ]
  node [
    id 125
    label "sztuka"
  ]
  node [
    id 126
    label "religia"
  ]
  node [
    id 127
    label "przejmowa&#263;"
  ]
  node [
    id 128
    label "asymilowa&#263;_si&#281;"
  ]
  node [
    id 129
    label "makrokosmos"
  ]
  node [
    id 130
    label "Staro&#380;ytny_Egipt"
  ]
  node [
    id 131
    label "zasymilowanie_si&#281;"
  ]
  node [
    id 132
    label "zjawisko"
  ]
  node [
    id 133
    label "praca_rolnicza"
  ]
  node [
    id 134
    label "tradycja"
  ]
  node [
    id 135
    label "kultura_janis&#322;awicka"
  ]
  node [
    id 136
    label "zasymilowa&#263;_si&#281;"
  ]
  node [
    id 137
    label "przejmowanie"
  ]
  node [
    id 138
    label "cecha"
  ]
  node [
    id 139
    label "asymilowanie_si&#281;"
  ]
  node [
    id 140
    label "przej&#261;&#263;"
  ]
  node [
    id 141
    label "hodowla"
  ]
  node [
    id 142
    label "brzoskwiniarnia"
  ]
  node [
    id 143
    label "populace"
  ]
  node [
    id 144
    label "konwencja"
  ]
  node [
    id 145
    label "propriety"
  ]
  node [
    id 146
    label "jako&#347;&#263;"
  ]
  node [
    id 147
    label "kuchnia"
  ]
  node [
    id 148
    label "zwyczaj"
  ]
  node [
    id 149
    label "przej&#281;cie"
  ]
  node [
    id 150
    label "ro&#347;linno&#347;&#263;"
  ]
  node [
    id 151
    label "si&#281;ga&#263;"
  ]
  node [
    id 152
    label "trwa&#263;"
  ]
  node [
    id 153
    label "obecno&#347;&#263;"
  ]
  node [
    id 154
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 155
    label "stand"
  ]
  node [
    id 156
    label "mie&#263;_miejsce"
  ]
  node [
    id 157
    label "uczestniczy&#263;"
  ]
  node [
    id 158
    label "chodzi&#263;"
  ]
  node [
    id 159
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 160
    label "equal"
  ]
  node [
    id 161
    label "gajny"
  ]
  node [
    id 162
    label "legalnie"
  ]
  node [
    id 163
    label "lacki"
  ]
  node [
    id 164
    label "j&#281;zyk_lechicki"
  ]
  node [
    id 165
    label "sztajer"
  ]
  node [
    id 166
    label "drabant"
  ]
  node [
    id 167
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 168
    label "polak"
  ]
  node [
    id 169
    label "pierogi_ruskie"
  ]
  node [
    id 170
    label "krakowiak"
  ]
  node [
    id 171
    label "Polish"
  ]
  node [
    id 172
    label "j&#281;zyk"
  ]
  node [
    id 173
    label "fasolka_po_breto&#324;sku"
  ]
  node [
    id 174
    label "oberek"
  ]
  node [
    id 175
    label "po_polsku"
  ]
  node [
    id 176
    label "mazur"
  ]
  node [
    id 177
    label "s&#322;owia&#324;ski"
  ]
  node [
    id 178
    label "chodzony"
  ]
  node [
    id 179
    label "skoczny"
  ]
  node [
    id 180
    label "ryba_po_grecku"
  ]
  node [
    id 181
    label "goniony"
  ]
  node [
    id 182
    label "polsko"
  ]
  node [
    id 183
    label "w&#322;asny"
  ]
  node [
    id 184
    label "oryginalny"
  ]
  node [
    id 185
    label "autorsko"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 18
  ]
  edge [
    source 1
    target 19
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 3
    target 24
  ]
  edge [
    source 3
    target 25
  ]
  edge [
    source 3
    target 26
  ]
  edge [
    source 3
    target 27
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 3
    target 38
  ]
  edge [
    source 3
    target 39
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 13
  ]
  edge [
    source 4
    target 14
  ]
  edge [
    source 4
    target 47
  ]
  edge [
    source 4
    target 48
  ]
  edge [
    source 4
    target 6
  ]
  edge [
    source 4
    target 49
  ]
  edge [
    source 4
    target 50
  ]
  edge [
    source 4
    target 51
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 55
  ]
  edge [
    source 5
    target 56
  ]
  edge [
    source 5
    target 57
  ]
  edge [
    source 5
    target 58
  ]
  edge [
    source 5
    target 59
  ]
  edge [
    source 5
    target 60
  ]
  edge [
    source 5
    target 61
  ]
  edge [
    source 5
    target 62
  ]
  edge [
    source 5
    target 63
  ]
  edge [
    source 5
    target 64
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 14
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 74
  ]
  edge [
    source 7
    target 75
  ]
  edge [
    source 7
    target 76
  ]
  edge [
    source 7
    target 77
  ]
  edge [
    source 7
    target 78
  ]
  edge [
    source 7
    target 79
  ]
  edge [
    source 7
    target 10
  ]
  edge [
    source 7
    target 80
  ]
  edge [
    source 7
    target 81
  ]
  edge [
    source 7
    target 82
  ]
  edge [
    source 7
    target 83
  ]
  edge [
    source 7
    target 84
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 9
    target 86
  ]
  edge [
    source 9
    target 87
  ]
  edge [
    source 9
    target 88
  ]
  edge [
    source 9
    target 89
  ]
  edge [
    source 9
    target 90
  ]
  edge [
    source 9
    target 91
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 10
    target 96
  ]
  edge [
    source 10
    target 97
  ]
  edge [
    source 10
    target 98
  ]
  edge [
    source 10
    target 99
  ]
  edge [
    source 10
    target 100
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 12
    target 106
  ]
  edge [
    source 12
    target 107
  ]
  edge [
    source 12
    target 108
  ]
  edge [
    source 12
    target 109
  ]
  edge [
    source 12
    target 110
  ]
  edge [
    source 12
    target 111
  ]
  edge [
    source 13
    target 112
  ]
  edge [
    source 13
    target 113
  ]
  edge [
    source 13
    target 114
  ]
  edge [
    source 13
    target 115
  ]
  edge [
    source 13
    target 116
  ]
  edge [
    source 13
    target 117
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 118
  ]
  edge [
    source 14
    target 119
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 120
  ]
  edge [
    source 15
    target 23
  ]
  edge [
    source 15
    target 121
  ]
  edge [
    source 15
    target 122
  ]
  edge [
    source 15
    target 123
  ]
  edge [
    source 15
    target 124
  ]
  edge [
    source 15
    target 125
  ]
  edge [
    source 15
    target 126
  ]
  edge [
    source 15
    target 127
  ]
  edge [
    source 15
    target 128
  ]
  edge [
    source 15
    target 129
  ]
  edge [
    source 15
    target 130
  ]
  edge [
    source 15
    target 131
  ]
  edge [
    source 15
    target 132
  ]
  edge [
    source 15
    target 133
  ]
  edge [
    source 15
    target 134
  ]
  edge [
    source 15
    target 135
  ]
  edge [
    source 15
    target 136
  ]
  edge [
    source 15
    target 137
  ]
  edge [
    source 15
    target 138
  ]
  edge [
    source 15
    target 139
  ]
  edge [
    source 15
    target 140
  ]
  edge [
    source 15
    target 141
  ]
  edge [
    source 15
    target 142
  ]
  edge [
    source 15
    target 143
  ]
  edge [
    source 15
    target 144
  ]
  edge [
    source 15
    target 145
  ]
  edge [
    source 15
    target 146
  ]
  edge [
    source 15
    target 147
  ]
  edge [
    source 15
    target 148
  ]
  edge [
    source 15
    target 149
  ]
  edge [
    source 15
    target 150
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 151
  ]
  edge [
    source 16
    target 152
  ]
  edge [
    source 16
    target 153
  ]
  edge [
    source 16
    target 58
  ]
  edge [
    source 16
    target 154
  ]
  edge [
    source 16
    target 155
  ]
  edge [
    source 16
    target 156
  ]
  edge [
    source 16
    target 157
  ]
  edge [
    source 16
    target 158
  ]
  edge [
    source 16
    target 159
  ]
  edge [
    source 16
    target 160
  ]
  edge [
    source 17
    target 161
  ]
  edge [
    source 17
    target 162
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 163
  ]
  edge [
    source 18
    target 164
  ]
  edge [
    source 18
    target 23
  ]
  edge [
    source 18
    target 165
  ]
  edge [
    source 18
    target 166
  ]
  edge [
    source 18
    target 167
  ]
  edge [
    source 18
    target 168
  ]
  edge [
    source 18
    target 169
  ]
  edge [
    source 18
    target 170
  ]
  edge [
    source 18
    target 171
  ]
  edge [
    source 18
    target 172
  ]
  edge [
    source 18
    target 173
  ]
  edge [
    source 18
    target 174
  ]
  edge [
    source 18
    target 175
  ]
  edge [
    source 18
    target 176
  ]
  edge [
    source 18
    target 177
  ]
  edge [
    source 18
    target 178
  ]
  edge [
    source 18
    target 179
  ]
  edge [
    source 18
    target 180
  ]
  edge [
    source 18
    target 181
  ]
  edge [
    source 18
    target 182
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 183
  ]
  edge [
    source 20
    target 184
  ]
  edge [
    source 20
    target 185
  ]
]
