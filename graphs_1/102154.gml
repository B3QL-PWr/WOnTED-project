graph [
  maxDegree 27
  minDegree 1
  meanDegree 2
  density 0.05
  graphCliqueNumber 2
  node [
    id 0
    label "paradygmat"
    origin "text"
  ]
  node [
    id 1
    label "obiektowy"
    origin "text"
  ]
  node [
    id 2
    label "opiera&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "cztery"
    origin "text"
  ]
  node [
    id 5
    label "zasada"
    origin "text"
  ]
  node [
    id 6
    label "fleksja"
  ]
  node [
    id 7
    label "ruch"
  ]
  node [
    id 8
    label "ideal"
  ]
  node [
    id 9
    label "mildew"
  ]
  node [
    id 10
    label "spos&#243;b"
  ]
  node [
    id 11
    label "osnowywa&#263;"
  ]
  node [
    id 12
    label "back"
  ]
  node [
    id 13
    label "czerpa&#263;"
  ]
  node [
    id 14
    label "digest"
  ]
  node [
    id 15
    label "stawia&#263;"
  ]
  node [
    id 16
    label "obserwacja"
  ]
  node [
    id 17
    label "moralno&#347;&#263;"
  ]
  node [
    id 18
    label "podstawa"
  ]
  node [
    id 19
    label "regu&#322;a_Ramseya"
  ]
  node [
    id 20
    label "umowa"
  ]
  node [
    id 21
    label "dominion"
  ]
  node [
    id 22
    label "qualification"
  ]
  node [
    id 23
    label "twierdzenie_Rybczy&#324;skiego"
  ]
  node [
    id 24
    label "opis"
  ]
  node [
    id 25
    label "regu&#322;a_Allena"
  ]
  node [
    id 26
    label "normalizacja"
  ]
  node [
    id 27
    label "regu&#322;a_Glogera"
  ]
  node [
    id 28
    label "regu&#322;a_Chargaffa"
  ]
  node [
    id 29
    label "standard"
  ]
  node [
    id 30
    label "base"
  ]
  node [
    id 31
    label "substancja"
  ]
  node [
    id 32
    label "prawid&#322;o"
  ]
  node [
    id 33
    label "prawo_Mendla"
  ]
  node [
    id 34
    label "prawid&#322;owo&#347;&#263;"
  ]
  node [
    id 35
    label "criterion"
  ]
  node [
    id 36
    label "twierdzenie"
  ]
  node [
    id 37
    label "regu&#322;a_Tinbergena"
  ]
  node [
    id 38
    label "prawo"
  ]
  node [
    id 39
    label "occupation"
  ]
  node [
    id 40
    label "zasada_d'Alemberta"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 6
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 0
    target 10
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 16
  ]
  edge [
    source 5
    target 17
  ]
  edge [
    source 5
    target 18
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 5
    target 21
  ]
  edge [
    source 5
    target 22
  ]
  edge [
    source 5
    target 23
  ]
  edge [
    source 5
    target 24
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 5
    target 31
  ]
  edge [
    source 5
    target 10
  ]
  edge [
    source 5
    target 32
  ]
  edge [
    source 5
    target 33
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 5
    target 35
  ]
  edge [
    source 5
    target 36
  ]
  edge [
    source 5
    target 37
  ]
  edge [
    source 5
    target 38
  ]
  edge [
    source 5
    target 39
  ]
  edge [
    source 5
    target 40
  ]
]
