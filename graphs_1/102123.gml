graph [
  maxDegree 24
  minDegree 1
  meanDegree 2.109090909090909
  density 0.009630552096305521
  graphCliqueNumber 3
  node [
    id 0
    label "kolejny"
    origin "text"
  ]
  node [
    id 1
    label "spotkanie"
    origin "text"
  ]
  node [
    id 2
    label "odby&#263;"
    origin "text"
  ]
  node [
    id 3
    label "si&#281;"
    origin "text"
  ]
  node [
    id 4
    label "wyj&#261;tkowo"
    origin "text"
  ]
  node [
    id 5
    label "liczny"
    origin "text"
  ]
  node [
    id 6
    label "grono"
    origin "text"
  ]
  node [
    id 7
    label "dzi&#281;ki"
    origin "text"
  ]
  node [
    id 8
    label "draxanowi"
    origin "text"
  ]
  node [
    id 9
    label "kt&#243;ry"
    origin "text"
  ]
  node [
    id 10
    label "sprowadzi&#263;"
    origin "text"
  ]
  node [
    id 11
    label "ekipa"
    origin "text"
  ]
  node [
    id 12
    label "posiadacz"
    origin "text"
  ]
  node [
    id 13
    label "monstera"
    origin "text"
  ]
  node [
    id 14
    label "truck"
    origin "text"
  ]
  node [
    id 15
    label "wprawdzie"
    origin "text"
  ]
  node [
    id 16
    label "nasz"
    origin "text"
  ]
  node [
    id 17
    label "ciasny"
    origin "text"
  ]
  node [
    id 18
    label "tora"
    origin "text"
  ]
  node [
    id 19
    label "pewno"
    origin "text"
  ]
  node [
    id 20
    label "by&#263;"
    origin "text"
  ]
  node [
    id 21
    label "optymalny"
    origin "text"
  ]
  node [
    id 22
    label "dla"
    origin "text"
  ]
  node [
    id 23
    label "monsterow"
    origin "text"
  ]
  node [
    id 24
    label "skala"
    origin "text"
  ]
  node [
    id 25
    label "przeszkodzi&#263;"
    origin "text"
  ]
  node [
    id 26
    label "dobry"
    origin "text"
  ]
  node [
    id 27
    label "zabawa"
    origin "text"
  ]
  node [
    id 28
    label "widok"
    origin "text"
  ]
  node [
    id 29
    label "elektryczny"
    origin "text"
  ]
  node [
    id 30
    label "bagusa"
    origin "text"
  ]
  node [
    id 31
    label "anda"
    origin "text"
  ]
  node [
    id 32
    label "&#347;miga&#263;"
    origin "text"
  ]
  node [
    id 33
    label "mi&#281;dzy"
    origin "text"
  ]
  node [
    id 34
    label "ogromny"
    origin "text"
  ]
  node [
    id 35
    label "prosty"
    origin "text"
  ]
  node [
    id 36
    label "niezapomniany"
    origin "text"
  ]
  node [
    id 37
    label "inny"
  ]
  node [
    id 38
    label "nast&#281;pnie"
  ]
  node [
    id 39
    label "kt&#243;ry&#347;"
  ]
  node [
    id 40
    label "kolejno"
  ]
  node [
    id 41
    label "nastopny"
  ]
  node [
    id 42
    label "zapoznawanie_si&#281;"
  ]
  node [
    id 43
    label "po&#380;egnanie"
  ]
  node [
    id 44
    label "spowodowanie"
  ]
  node [
    id 45
    label "znalezienie"
  ]
  node [
    id 46
    label "znajomy"
  ]
  node [
    id 47
    label "doznanie"
  ]
  node [
    id 48
    label "employment"
  ]
  node [
    id 49
    label "Og&#243;lnopolskie_Spotkanie_M&#322;odych_Lednica_2000"
  ]
  node [
    id 50
    label "gather"
  ]
  node [
    id 51
    label "powitanie"
  ]
  node [
    id 52
    label "spotykanie"
  ]
  node [
    id 53
    label "wydarzenie"
  ]
  node [
    id 54
    label "gathering"
  ]
  node [
    id 55
    label "spotkanie_si&#281;"
  ]
  node [
    id 56
    label "zdarzenie_si&#281;"
  ]
  node [
    id 57
    label "match"
  ]
  node [
    id 58
    label "zawarcie"
  ]
  node [
    id 59
    label "reserve"
  ]
  node [
    id 60
    label "przej&#347;&#263;"
  ]
  node [
    id 61
    label "wzi&#261;&#263;_udzia&#322;"
  ]
  node [
    id 62
    label "niestandardowo"
  ]
  node [
    id 63
    label "wyj&#261;tkowy"
  ]
  node [
    id 64
    label "niezwykle"
  ]
  node [
    id 65
    label "rojenie_si&#281;"
  ]
  node [
    id 66
    label "cz&#281;sty"
  ]
  node [
    id 67
    label "licznie"
  ]
  node [
    id 68
    label "jagoda"
  ]
  node [
    id 69
    label "ki&#347;&#263;"
  ]
  node [
    id 70
    label "mirycetyna"
  ]
  node [
    id 71
    label "grupa"
  ]
  node [
    id 72
    label "owoc"
  ]
  node [
    id 73
    label "zbi&#243;r"
  ]
  node [
    id 74
    label "powie&#347;&#263;"
  ]
  node [
    id 75
    label "get"
  ]
  node [
    id 76
    label "pom&#243;c"
  ]
  node [
    id 77
    label "ograniczy&#263;"
  ]
  node [
    id 78
    label "pos&#322;a&#263;"
  ]
  node [
    id 79
    label "spowodowa&#263;"
  ]
  node [
    id 80
    label "zwr&#243;ci&#263;"
  ]
  node [
    id 81
    label "become"
  ]
  node [
    id 82
    label "bring"
  ]
  node [
    id 83
    label "u&#322;amek"
  ]
  node [
    id 84
    label "zmieni&#263;"
  ]
  node [
    id 85
    label "carry"
  ]
  node [
    id 86
    label "wprowadzi&#263;"
  ]
  node [
    id 87
    label "upro&#347;ci&#263;"
  ]
  node [
    id 88
    label "zesp&#243;&#322;"
  ]
  node [
    id 89
    label "dublet"
  ]
  node [
    id 90
    label "force"
  ]
  node [
    id 91
    label "wykupienie"
  ]
  node [
    id 92
    label "bycie_w_posiadaniu"
  ]
  node [
    id 93
    label "wykupywanie"
  ]
  node [
    id 94
    label "podmiot"
  ]
  node [
    id 95
    label "obrazkowate"
  ]
  node [
    id 96
    label "pn&#261;cze"
  ]
  node [
    id 97
    label "ro&#347;lina"
  ]
  node [
    id 98
    label "ro&#347;lina_wieloletnia"
  ]
  node [
    id 99
    label "ro&#347;lina_alimentacyjna"
  ]
  node [
    id 100
    label "czyj&#347;"
  ]
  node [
    id 101
    label "niewygodny"
  ]
  node [
    id 102
    label "niedostateczny"
  ]
  node [
    id 103
    label "zwarty"
  ]
  node [
    id 104
    label "kr&#281;powanie"
  ]
  node [
    id 105
    label "nieelastyczny"
  ]
  node [
    id 106
    label "zw&#281;&#380;enie"
  ]
  node [
    id 107
    label "jajognioty"
  ]
  node [
    id 108
    label "duszny"
  ]
  node [
    id 109
    label "obcis&#322;y"
  ]
  node [
    id 110
    label "niewystarczaj&#261;cy"
  ]
  node [
    id 111
    label "ciasno"
  ]
  node [
    id 112
    label "ma&#322;y"
  ]
  node [
    id 113
    label "w&#261;ski"
  ]
  node [
    id 114
    label "zw&#243;j"
  ]
  node [
    id 115
    label "Tora"
  ]
  node [
    id 116
    label "si&#281;ga&#263;"
  ]
  node [
    id 117
    label "trwa&#263;"
  ]
  node [
    id 118
    label "obecno&#347;&#263;"
  ]
  node [
    id 119
    label "stan"
  ]
  node [
    id 120
    label "cechowa&#263;_si&#281;"
  ]
  node [
    id 121
    label "stand"
  ]
  node [
    id 122
    label "mie&#263;_miejsce"
  ]
  node [
    id 123
    label "uczestniczy&#263;"
  ]
  node [
    id 124
    label "chodzi&#263;"
  ]
  node [
    id 125
    label "znachodzi&#263;_si&#281;"
  ]
  node [
    id 126
    label "equal"
  ]
  node [
    id 127
    label "jedyny"
  ]
  node [
    id 128
    label "doskona&#322;y"
  ]
  node [
    id 129
    label "przedzia&#322;"
  ]
  node [
    id 130
    label "przymiar"
  ]
  node [
    id 131
    label "podzia&#322;ka"
  ]
  node [
    id 132
    label "proporcja"
  ]
  node [
    id 133
    label "tetrachord"
  ]
  node [
    id 134
    label "scale"
  ]
  node [
    id 135
    label "dominanta"
  ]
  node [
    id 136
    label "rejestr"
  ]
  node [
    id 137
    label "sfera"
  ]
  node [
    id 138
    label "struktura"
  ]
  node [
    id 139
    label "kreska"
  ]
  node [
    id 140
    label "zero"
  ]
  node [
    id 141
    label "interwa&#322;"
  ]
  node [
    id 142
    label "przyrz&#261;d_pomiarowy"
  ]
  node [
    id 143
    label "subdominanta"
  ]
  node [
    id 144
    label "dziedzina"
  ]
  node [
    id 145
    label "masztab"
  ]
  node [
    id 146
    label "part"
  ]
  node [
    id 147
    label "podzakres"
  ]
  node [
    id 148
    label "wielko&#347;&#263;"
  ]
  node [
    id 149
    label "jednostka"
  ]
  node [
    id 150
    label "intervene"
  ]
  node [
    id 151
    label "utrudni&#263;"
  ]
  node [
    id 152
    label "pomy&#347;lny"
  ]
  node [
    id 153
    label "skuteczny"
  ]
  node [
    id 154
    label "moralny"
  ]
  node [
    id 155
    label "korzystny"
  ]
  node [
    id 156
    label "odpowiedni"
  ]
  node [
    id 157
    label "zwrot"
  ]
  node [
    id 158
    label "dobrze"
  ]
  node [
    id 159
    label "pozytywny"
  ]
  node [
    id 160
    label "grzeczny"
  ]
  node [
    id 161
    label "mi&#322;y"
  ]
  node [
    id 162
    label "dobroczynny"
  ]
  node [
    id 163
    label "pos&#322;uszny"
  ]
  node [
    id 164
    label "ca&#322;y"
  ]
  node [
    id 165
    label "w&#322;a&#347;ciwy"
  ]
  node [
    id 166
    label "czw&#243;rka"
  ]
  node [
    id 167
    label "spokojny"
  ]
  node [
    id 168
    label "&#347;mieszny"
  ]
  node [
    id 169
    label "drogi"
  ]
  node [
    id 170
    label "wodzirej"
  ]
  node [
    id 171
    label "rozrywka"
  ]
  node [
    id 172
    label "nabawienie_si&#281;"
  ]
  node [
    id 173
    label "cecha"
  ]
  node [
    id 174
    label "gambling"
  ]
  node [
    id 175
    label "taniec"
  ]
  node [
    id 176
    label "impreza"
  ]
  node [
    id 177
    label "igraszka"
  ]
  node [
    id 178
    label "igra"
  ]
  node [
    id 179
    label "nabawi&#263;_si&#281;"
  ]
  node [
    id 180
    label "ta&#324;c&#243;wka"
  ]
  node [
    id 181
    label "game"
  ]
  node [
    id 182
    label "ubaw"
  ]
  node [
    id 183
    label "chwyt"
  ]
  node [
    id 184
    label "weso&#322;e_miasteczko"
  ]
  node [
    id 185
    label "wygl&#261;d"
  ]
  node [
    id 186
    label "obraz"
  ]
  node [
    id 187
    label "przestrze&#324;"
  ]
  node [
    id 188
    label "teren"
  ]
  node [
    id 189
    label "perspektywa"
  ]
  node [
    id 190
    label "elektrycznie"
  ]
  node [
    id 191
    label "run"
  ]
  node [
    id 192
    label "macha&#263;"
  ]
  node [
    id 193
    label "rosn&#261;&#263;"
  ]
  node [
    id 194
    label "p&#281;dzi&#263;"
  ]
  node [
    id 195
    label "olbrzymio"
  ]
  node [
    id 196
    label "ogromnie"
  ]
  node [
    id 197
    label "znaczny"
  ]
  node [
    id 198
    label "jebitny"
  ]
  node [
    id 199
    label "prawdziwy"
  ]
  node [
    id 200
    label "wa&#380;ny"
  ]
  node [
    id 201
    label "dono&#347;ny"
  ]
  node [
    id 202
    label "&#322;atwy"
  ]
  node [
    id 203
    label "prostowanie_si&#281;"
  ]
  node [
    id 204
    label "rozprostowanie_si&#281;"
  ]
  node [
    id 205
    label "rozprostowanie"
  ]
  node [
    id 206
    label "niez&#322;o&#380;ony"
  ]
  node [
    id 207
    label "prostoduszny"
  ]
  node [
    id 208
    label "naturalny"
  ]
  node [
    id 209
    label "naiwny"
  ]
  node [
    id 210
    label "cios"
  ]
  node [
    id 211
    label "prostowanie"
  ]
  node [
    id 212
    label "niepozorny"
  ]
  node [
    id 213
    label "zwyk&#322;y"
  ]
  node [
    id 214
    label "prosto"
  ]
  node [
    id 215
    label "po_prostu"
  ]
  node [
    id 216
    label "skromny"
  ]
  node [
    id 217
    label "wyprostowanie_si&#281;"
  ]
  node [
    id 218
    label "pami&#281;tny"
  ]
  node [
    id 219
    label "pami&#281;tnie"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 40
  ]
  edge [
    source 0
    target 41
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 42
  ]
  edge [
    source 1
    target 43
  ]
  edge [
    source 1
    target 44
  ]
  edge [
    source 1
    target 45
  ]
  edge [
    source 1
    target 46
  ]
  edge [
    source 1
    target 47
  ]
  edge [
    source 1
    target 48
  ]
  edge [
    source 1
    target 49
  ]
  edge [
    source 1
    target 50
  ]
  edge [
    source 1
    target 51
  ]
  edge [
    source 1
    target 52
  ]
  edge [
    source 1
    target 53
  ]
  edge [
    source 1
    target 54
  ]
  edge [
    source 1
    target 55
  ]
  edge [
    source 1
    target 56
  ]
  edge [
    source 1
    target 57
  ]
  edge [
    source 1
    target 58
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 59
  ]
  edge [
    source 2
    target 60
  ]
  edge [
    source 2
    target 61
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 65
  ]
  edge [
    source 5
    target 66
  ]
  edge [
    source 5
    target 67
  ]
  edge [
    source 5
    target 34
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 68
  ]
  edge [
    source 6
    target 69
  ]
  edge [
    source 6
    target 70
  ]
  edge [
    source 6
    target 71
  ]
  edge [
    source 6
    target 72
  ]
  edge [
    source 6
    target 73
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 30
  ]
  edge [
    source 9
    target 31
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 74
  ]
  edge [
    source 10
    target 75
  ]
  edge [
    source 10
    target 76
  ]
  edge [
    source 10
    target 77
  ]
  edge [
    source 10
    target 78
  ]
  edge [
    source 10
    target 79
  ]
  edge [
    source 10
    target 80
  ]
  edge [
    source 10
    target 81
  ]
  edge [
    source 10
    target 82
  ]
  edge [
    source 10
    target 83
  ]
  edge [
    source 10
    target 84
  ]
  edge [
    source 10
    target 85
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 35
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 71
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 11
    target 90
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 95
  ]
  edge [
    source 13
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 34
  ]
  edge [
    source 14
    target 20
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 100
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 101
  ]
  edge [
    source 17
    target 102
  ]
  edge [
    source 17
    target 103
  ]
  edge [
    source 17
    target 104
  ]
  edge [
    source 17
    target 105
  ]
  edge [
    source 17
    target 106
  ]
  edge [
    source 17
    target 107
  ]
  edge [
    source 17
    target 108
  ]
  edge [
    source 17
    target 109
  ]
  edge [
    source 17
    target 26
  ]
  edge [
    source 17
    target 110
  ]
  edge [
    source 17
    target 111
  ]
  edge [
    source 17
    target 112
  ]
  edge [
    source 17
    target 113
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 114
  ]
  edge [
    source 18
    target 115
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 20
    target 35
  ]
  edge [
    source 20
    target 116
  ]
  edge [
    source 20
    target 117
  ]
  edge [
    source 20
    target 118
  ]
  edge [
    source 20
    target 119
  ]
  edge [
    source 20
    target 120
  ]
  edge [
    source 20
    target 121
  ]
  edge [
    source 20
    target 122
  ]
  edge [
    source 20
    target 123
  ]
  edge [
    source 20
    target 124
  ]
  edge [
    source 20
    target 125
  ]
  edge [
    source 20
    target 126
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 127
  ]
  edge [
    source 21
    target 128
  ]
  edge [
    source 22
    target 23
  ]
  edge [
    source 23
    target 24
  ]
  edge [
    source 24
    target 25
  ]
  edge [
    source 24
    target 129
  ]
  edge [
    source 24
    target 130
  ]
  edge [
    source 24
    target 131
  ]
  edge [
    source 24
    target 132
  ]
  edge [
    source 24
    target 133
  ]
  edge [
    source 24
    target 134
  ]
  edge [
    source 24
    target 135
  ]
  edge [
    source 24
    target 136
  ]
  edge [
    source 24
    target 137
  ]
  edge [
    source 24
    target 138
  ]
  edge [
    source 24
    target 139
  ]
  edge [
    source 24
    target 140
  ]
  edge [
    source 24
    target 141
  ]
  edge [
    source 24
    target 142
  ]
  edge [
    source 24
    target 143
  ]
  edge [
    source 24
    target 144
  ]
  edge [
    source 24
    target 145
  ]
  edge [
    source 24
    target 146
  ]
  edge [
    source 24
    target 147
  ]
  edge [
    source 24
    target 73
  ]
  edge [
    source 24
    target 148
  ]
  edge [
    source 24
    target 149
  ]
  edge [
    source 25
    target 26
  ]
  edge [
    source 25
    target 150
  ]
  edge [
    source 25
    target 151
  ]
  edge [
    source 26
    target 27
  ]
  edge [
    source 26
    target 152
  ]
  edge [
    source 26
    target 153
  ]
  edge [
    source 26
    target 154
  ]
  edge [
    source 26
    target 155
  ]
  edge [
    source 26
    target 156
  ]
  edge [
    source 26
    target 157
  ]
  edge [
    source 26
    target 158
  ]
  edge [
    source 26
    target 159
  ]
  edge [
    source 26
    target 160
  ]
  edge [
    source 26
    target 51
  ]
  edge [
    source 26
    target 161
  ]
  edge [
    source 26
    target 162
  ]
  edge [
    source 26
    target 163
  ]
  edge [
    source 26
    target 164
  ]
  edge [
    source 26
    target 165
  ]
  edge [
    source 26
    target 166
  ]
  edge [
    source 26
    target 167
  ]
  edge [
    source 26
    target 168
  ]
  edge [
    source 26
    target 169
  ]
  edge [
    source 27
    target 28
  ]
  edge [
    source 27
    target 170
  ]
  edge [
    source 27
    target 171
  ]
  edge [
    source 27
    target 172
  ]
  edge [
    source 27
    target 173
  ]
  edge [
    source 27
    target 174
  ]
  edge [
    source 27
    target 175
  ]
  edge [
    source 27
    target 176
  ]
  edge [
    source 27
    target 177
  ]
  edge [
    source 27
    target 178
  ]
  edge [
    source 27
    target 179
  ]
  edge [
    source 27
    target 180
  ]
  edge [
    source 27
    target 181
  ]
  edge [
    source 27
    target 182
  ]
  edge [
    source 27
    target 183
  ]
  edge [
    source 27
    target 184
  ]
  edge [
    source 28
    target 29
  ]
  edge [
    source 28
    target 185
  ]
  edge [
    source 28
    target 186
  ]
  edge [
    source 28
    target 173
  ]
  edge [
    source 28
    target 187
  ]
  edge [
    source 28
    target 188
  ]
  edge [
    source 28
    target 189
  ]
  edge [
    source 29
    target 30
  ]
  edge [
    source 29
    target 190
  ]
  edge [
    source 31
    target 32
  ]
  edge [
    source 32
    target 33
  ]
  edge [
    source 32
    target 191
  ]
  edge [
    source 32
    target 192
  ]
  edge [
    source 32
    target 193
  ]
  edge [
    source 32
    target 194
  ]
  edge [
    source 32
    target 85
  ]
  edge [
    source 33
    target 34
  ]
  edge [
    source 34
    target 195
  ]
  edge [
    source 34
    target 63
  ]
  edge [
    source 34
    target 196
  ]
  edge [
    source 34
    target 197
  ]
  edge [
    source 34
    target 198
  ]
  edge [
    source 34
    target 199
  ]
  edge [
    source 34
    target 200
  ]
  edge [
    source 34
    target 201
  ]
  edge [
    source 35
    target 36
  ]
  edge [
    source 35
    target 202
  ]
  edge [
    source 35
    target 203
  ]
  edge [
    source 35
    target 204
  ]
  edge [
    source 35
    target 205
  ]
  edge [
    source 35
    target 206
  ]
  edge [
    source 35
    target 207
  ]
  edge [
    source 35
    target 208
  ]
  edge [
    source 35
    target 209
  ]
  edge [
    source 35
    target 210
  ]
  edge [
    source 35
    target 211
  ]
  edge [
    source 35
    target 212
  ]
  edge [
    source 35
    target 213
  ]
  edge [
    source 35
    target 214
  ]
  edge [
    source 35
    target 215
  ]
  edge [
    source 35
    target 216
  ]
  edge [
    source 35
    target 217
  ]
  edge [
    source 36
    target 218
  ]
  edge [
    source 36
    target 219
  ]
  edge [
    source 36
    target 200
  ]
]
