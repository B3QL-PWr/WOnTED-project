graph [
  maxDegree 13
  minDegree 1
  meanDegree 1.9534883720930232
  density 0.046511627906976744
  graphCliqueNumber 2
  node [
    id 0
    label "zmniejszy&#263;"
    origin "text"
  ]
  node [
    id 1
    label "si&#281;"
    origin "text"
  ]
  node [
    id 2
    label "liczba"
    origin "text"
  ]
  node [
    id 3
    label "sp&#243;r"
    origin "text"
  ]
  node [
    id 4
    label "podatkowy"
    origin "text"
  ]
  node [
    id 5
    label "z&#322;o"
    origin "text"
  ]
  node [
    id 6
    label "wiadomo&#347;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "zmieni&#263;"
  ]
  node [
    id 8
    label "soften"
  ]
  node [
    id 9
    label "kategoria"
  ]
  node [
    id 10
    label "kategoria_gramatyczna"
  ]
  node [
    id 11
    label "kwadrat_magiczny"
  ]
  node [
    id 12
    label "grupa"
  ]
  node [
    id 13
    label "cecha"
  ]
  node [
    id 14
    label "wyra&#380;enie"
  ]
  node [
    id 15
    label "pierwiastek"
  ]
  node [
    id 16
    label "rozmiar"
  ]
  node [
    id 17
    label "number"
  ]
  node [
    id 18
    label "poj&#281;cie"
  ]
  node [
    id 19
    label "koniugacja"
  ]
  node [
    id 20
    label "wsp&#243;r"
  ]
  node [
    id 21
    label "clash"
  ]
  node [
    id 22
    label "konflikt"
  ]
  node [
    id 23
    label "obrona"
  ]
  node [
    id 24
    label "podatkowo"
  ]
  node [
    id 25
    label "czyn"
  ]
  node [
    id 26
    label "cholerstwo"
  ]
  node [
    id 27
    label "negatywno&#347;&#263;"
  ]
  node [
    id 28
    label "ailment"
  ]
  node [
    id 29
    label "rzecz"
  ]
  node [
    id 30
    label "action"
  ]
  node [
    id 31
    label "zarys"
  ]
  node [
    id 32
    label "nap&#322;ywa&#263;"
  ]
  node [
    id 33
    label "zniesienie"
  ]
  node [
    id 34
    label "nap&#322;ywanie"
  ]
  node [
    id 35
    label "znosi&#263;"
  ]
  node [
    id 36
    label "informacja"
  ]
  node [
    id 37
    label "znie&#347;&#263;"
  ]
  node [
    id 38
    label "komunikat"
  ]
  node [
    id 39
    label "depesza_emska"
  ]
  node [
    id 40
    label "communication"
  ]
  node [
    id 41
    label "znoszenie"
  ]
  node [
    id 42
    label "signal"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 9
  ]
  edge [
    source 2
    target 10
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 2
    target 18
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 20
  ]
  edge [
    source 3
    target 21
  ]
  edge [
    source 3
    target 22
  ]
  edge [
    source 3
    target 23
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 24
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 25
  ]
  edge [
    source 5
    target 26
  ]
  edge [
    source 5
    target 27
  ]
  edge [
    source 5
    target 28
  ]
  edge [
    source 5
    target 29
  ]
  edge [
    source 5
    target 30
  ]
  edge [
    source 6
    target 31
  ]
  edge [
    source 6
    target 32
  ]
  edge [
    source 6
    target 33
  ]
  edge [
    source 6
    target 34
  ]
  edge [
    source 6
    target 35
  ]
  edge [
    source 6
    target 36
  ]
  edge [
    source 6
    target 37
  ]
  edge [
    source 6
    target 38
  ]
  edge [
    source 6
    target 39
  ]
  edge [
    source 6
    target 40
  ]
  edge [
    source 6
    target 41
  ]
  edge [
    source 6
    target 42
  ]
]
