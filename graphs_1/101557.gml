graph [
  maxDegree 21
  minDegree 1
  meanDegree 2
  density 0.013513513513513514
  graphCliqueNumber 2
  node [
    id 0
    label "zewn&#261;trz"
    origin "text"
  ]
  node [
    id 1
    label "panowa&#263;"
    origin "text"
  ]
  node [
    id 2
    label "ciemno&#347;&#263;"
    origin "text"
  ]
  node [
    id 3
    label "tylko"
    origin "text"
  ]
  node [
    id 4
    label "&#347;wiat&#322;a"
    origin "text"
  ]
  node [
    id 5
    label "lampa"
    origin "text"
  ]
  node [
    id 6
    label "wyziera&#263;"
    origin "text"
  ]
  node [
    id 7
    label "przestrze&#324;"
    origin "text"
  ]
  node [
    id 8
    label "czworok&#261;t"
    origin "text"
  ]
  node [
    id 9
    label "okno"
    origin "text"
  ]
  node [
    id 10
    label "przesuwa&#263;"
    origin "text"
  ]
  node [
    id 11
    label "si&#281;"
    origin "text"
  ]
  node [
    id 12
    label "szybko"
    origin "text"
  ]
  node [
    id 13
    label "zbocz"
    origin "text"
  ]
  node [
    id 14
    label "nasyp"
    origin "text"
  ]
  node [
    id 15
    label "przelotny"
    origin "text"
  ]
  node [
    id 16
    label "wywiad"
    origin "text"
  ]
  node [
    id 17
    label "poci&#261;g"
    origin "text"
  ]
  node [
    id 18
    label "przebiega&#263;"
    origin "text"
  ]
  node [
    id 19
    label "jaki&#347;"
    origin "text"
  ]
  node [
    id 20
    label "puste"
    origin "text"
  ]
  node [
    id 21
    label "&#322;&#261;ka"
    origin "text"
  ]
  node [
    id 22
    label "pastwisko"
    origin "text"
  ]
  node [
    id 23
    label "przewa&#380;a&#263;"
  ]
  node [
    id 24
    label "kontrolowa&#263;"
  ]
  node [
    id 25
    label "control"
  ]
  node [
    id 26
    label "podporz&#261;dkowywa&#263;"
  ]
  node [
    id 27
    label "kierowa&#263;"
  ]
  node [
    id 28
    label "manipulate"
  ]
  node [
    id 29
    label "dominowa&#263;"
  ]
  node [
    id 30
    label "rz&#261;dzi&#263;"
  ]
  node [
    id 31
    label "istnie&#263;"
  ]
  node [
    id 32
    label "ton"
  ]
  node [
    id 33
    label "ciemnota"
  ]
  node [
    id 34
    label "&#263;ma"
  ]
  node [
    id 35
    label "noktowizja"
  ]
  node [
    id 36
    label "Ereb"
  ]
  node [
    id 37
    label "pomrok"
  ]
  node [
    id 38
    label "sowie_oczy"
  ]
  node [
    id 39
    label "z&#322;o"
  ]
  node [
    id 40
    label "niewiedza"
  ]
  node [
    id 41
    label "zjawisko"
  ]
  node [
    id 42
    label "cloud"
  ]
  node [
    id 43
    label "sygnalizacja_&#347;wietlna"
  ]
  node [
    id 44
    label "miejsce"
  ]
  node [
    id 45
    label "skrzy&#380;owanie"
  ]
  node [
    id 46
    label "pasy"
  ]
  node [
    id 47
    label "iluminowa&#263;"
  ]
  node [
    id 48
    label "&#380;ar&#243;wka"
  ]
  node [
    id 49
    label "sztuczne_&#378;r&#243;d&#322;o_&#347;wiat&#322;a"
  ]
  node [
    id 50
    label "o&#347;wietla&#263;"
  ]
  node [
    id 51
    label "wygl&#261;da&#263;"
  ]
  node [
    id 52
    label "oktant"
  ]
  node [
    id 53
    label "niezmierzony"
  ]
  node [
    id 54
    label "bezbrze&#380;e"
  ]
  node [
    id 55
    label "przedzieli&#263;"
  ]
  node [
    id 56
    label "rozdzielanie"
  ]
  node [
    id 57
    label "rozdziela&#263;"
  ]
  node [
    id 58
    label "zbi&#243;r"
  ]
  node [
    id 59
    label "punkt"
  ]
  node [
    id 60
    label "przestw&#243;r"
  ]
  node [
    id 61
    label "przedzielenie"
  ]
  node [
    id 62
    label "nielito&#347;ciwy"
  ]
  node [
    id 63
    label "czasoprzestrze&#324;"
  ]
  node [
    id 64
    label "quadrilateral"
  ]
  node [
    id 65
    label "wielok&#261;t"
  ]
  node [
    id 66
    label "figura_geometryczna"
  ]
  node [
    id 67
    label "lufcik"
  ]
  node [
    id 68
    label "pasek_narz&#281;dzi"
  ]
  node [
    id 69
    label "parapet"
  ]
  node [
    id 70
    label "otw&#243;r"
  ]
  node [
    id 71
    label "skrzyd&#322;o"
  ]
  node [
    id 72
    label "kwatera_okienna"
  ]
  node [
    id 73
    label "szyba"
  ]
  node [
    id 74
    label "futryna"
  ]
  node [
    id 75
    label "nadokiennik"
  ]
  node [
    id 76
    label "interfejs"
  ]
  node [
    id 77
    label "inspekt"
  ]
  node [
    id 78
    label "program"
  ]
  node [
    id 79
    label "casement"
  ]
  node [
    id 80
    label "prze&#347;wit"
  ]
  node [
    id 81
    label "menad&#380;er_okien"
  ]
  node [
    id 82
    label "pulpit"
  ]
  node [
    id 83
    label "transenna"
  ]
  node [
    id 84
    label "nora"
  ]
  node [
    id 85
    label "okiennica"
  ]
  node [
    id 86
    label "zmienia&#263;"
  ]
  node [
    id 87
    label "postpone"
  ]
  node [
    id 88
    label "dostosowywa&#263;"
  ]
  node [
    id 89
    label "translate"
  ]
  node [
    id 90
    label "przenosi&#263;"
  ]
  node [
    id 91
    label "transfer"
  ]
  node [
    id 92
    label "estrange"
  ]
  node [
    id 93
    label "go"
  ]
  node [
    id 94
    label "przestawia&#263;"
  ]
  node [
    id 95
    label "rusza&#263;"
  ]
  node [
    id 96
    label "quicker"
  ]
  node [
    id 97
    label "promptly"
  ]
  node [
    id 98
    label "bezpo&#347;rednio"
  ]
  node [
    id 99
    label "quickest"
  ]
  node [
    id 100
    label "sprawnie"
  ]
  node [
    id 101
    label "dynamicznie"
  ]
  node [
    id 102
    label "szybciej"
  ]
  node [
    id 103
    label "prosto"
  ]
  node [
    id 104
    label "szybciochem"
  ]
  node [
    id 105
    label "szybki"
  ]
  node [
    id 106
    label "usypisko"
  ]
  node [
    id 107
    label "kr&#243;tki"
  ]
  node [
    id 108
    label "w&#281;drowny"
  ]
  node [
    id 109
    label "daleki"
  ]
  node [
    id 110
    label "przelotnie"
  ]
  node [
    id 111
    label "czasowy"
  ]
  node [
    id 112
    label "diagnostyka"
  ]
  node [
    id 113
    label "rozmowa"
  ]
  node [
    id 114
    label "sonda&#380;"
  ]
  node [
    id 115
    label "autoryzowanie"
  ]
  node [
    id 116
    label "autoryzowa&#263;"
  ]
  node [
    id 117
    label "s&#322;u&#380;by_specjalne"
  ]
  node [
    id 118
    label "dzia&#322;alno&#347;&#263;"
  ]
  node [
    id 119
    label "agent"
  ]
  node [
    id 120
    label "inquiry"
  ]
  node [
    id 121
    label "consultation"
  ]
  node [
    id 122
    label "s&#322;u&#380;ba"
  ]
  node [
    id 123
    label "lokomotywa"
  ]
  node [
    id 124
    label "pojazd_kolejowy"
  ]
  node [
    id 125
    label "kolej"
  ]
  node [
    id 126
    label "skomunikowa&#263;_si&#281;"
  ]
  node [
    id 127
    label "tender"
  ]
  node [
    id 128
    label "cug"
  ]
  node [
    id 129
    label "skomunikowanie_si&#281;"
  ]
  node [
    id 130
    label "cecha_osobowo&#347;ci"
  ]
  node [
    id 131
    label "wagon"
  ]
  node [
    id 132
    label "p&#322;yn&#261;&#263;"
  ]
  node [
    id 133
    label "ci&#261;gn&#261;&#263;_si&#281;"
  ]
  node [
    id 134
    label "pojawia&#263;_si&#281;"
  ]
  node [
    id 135
    label "przebywa&#263;"
  ]
  node [
    id 136
    label "draw"
  ]
  node [
    id 137
    label "biec"
  ]
  node [
    id 138
    label "carry"
  ]
  node [
    id 139
    label "jako&#347;"
  ]
  node [
    id 140
    label "charakterystyczny"
  ]
  node [
    id 141
    label "ciekawy"
  ]
  node [
    id 142
    label "jako_tako"
  ]
  node [
    id 143
    label "dziwny"
  ]
  node [
    id 144
    label "niez&#322;y"
  ]
  node [
    id 145
    label "przyzwoity"
  ]
  node [
    id 146
    label "trawa"
  ]
  node [
    id 147
    label "formacja_ro&#347;linna"
  ]
  node [
    id 148
    label "lejmoniada"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 23
  ]
  edge [
    source 1
    target 24
  ]
  edge [
    source 1
    target 25
  ]
  edge [
    source 1
    target 26
  ]
  edge [
    source 1
    target 27
  ]
  edge [
    source 1
    target 28
  ]
  edge [
    source 1
    target 29
  ]
  edge [
    source 1
    target 30
  ]
  edge [
    source 1
    target 31
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 32
  ]
  edge [
    source 2
    target 33
  ]
  edge [
    source 2
    target 34
  ]
  edge [
    source 2
    target 35
  ]
  edge [
    source 2
    target 36
  ]
  edge [
    source 2
    target 37
  ]
  edge [
    source 2
    target 38
  ]
  edge [
    source 2
    target 39
  ]
  edge [
    source 2
    target 40
  ]
  edge [
    source 2
    target 41
  ]
  edge [
    source 2
    target 42
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 4
    target 44
  ]
  edge [
    source 4
    target 45
  ]
  edge [
    source 4
    target 46
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 5
    target 50
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 52
  ]
  edge [
    source 7
    target 53
  ]
  edge [
    source 7
    target 44
  ]
  edge [
    source 7
    target 54
  ]
  edge [
    source 7
    target 55
  ]
  edge [
    source 7
    target 56
  ]
  edge [
    source 7
    target 57
  ]
  edge [
    source 7
    target 58
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 7
    target 63
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 67
  ]
  edge [
    source 9
    target 68
  ]
  edge [
    source 9
    target 69
  ]
  edge [
    source 9
    target 70
  ]
  edge [
    source 9
    target 71
  ]
  edge [
    source 9
    target 72
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 9
    target 81
  ]
  edge [
    source 9
    target 82
  ]
  edge [
    source 9
    target 83
  ]
  edge [
    source 9
    target 84
  ]
  edge [
    source 9
    target 85
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 86
  ]
  edge [
    source 10
    target 87
  ]
  edge [
    source 10
    target 88
  ]
  edge [
    source 10
    target 89
  ]
  edge [
    source 10
    target 90
  ]
  edge [
    source 10
    target 91
  ]
  edge [
    source 10
    target 92
  ]
  edge [
    source 10
    target 93
  ]
  edge [
    source 10
    target 94
  ]
  edge [
    source 10
    target 95
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 12
    target 97
  ]
  edge [
    source 12
    target 98
  ]
  edge [
    source 12
    target 99
  ]
  edge [
    source 12
    target 100
  ]
  edge [
    source 12
    target 101
  ]
  edge [
    source 12
    target 102
  ]
  edge [
    source 12
    target 103
  ]
  edge [
    source 12
    target 104
  ]
  edge [
    source 12
    target 105
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 106
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 107
  ]
  edge [
    source 15
    target 108
  ]
  edge [
    source 15
    target 109
  ]
  edge [
    source 15
    target 110
  ]
  edge [
    source 15
    target 111
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 112
  ]
  edge [
    source 16
    target 113
  ]
  edge [
    source 16
    target 114
  ]
  edge [
    source 16
    target 115
  ]
  edge [
    source 16
    target 116
  ]
  edge [
    source 16
    target 117
  ]
  edge [
    source 16
    target 118
  ]
  edge [
    source 16
    target 119
  ]
  edge [
    source 16
    target 120
  ]
  edge [
    source 16
    target 121
  ]
  edge [
    source 16
    target 122
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 123
  ]
  edge [
    source 17
    target 124
  ]
  edge [
    source 17
    target 125
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 127
  ]
  edge [
    source 17
    target 128
  ]
  edge [
    source 17
    target 129
  ]
  edge [
    source 17
    target 130
  ]
  edge [
    source 17
    target 131
  ]
  edge [
    source 18
    target 19
  ]
  edge [
    source 18
    target 132
  ]
  edge [
    source 18
    target 133
  ]
  edge [
    source 18
    target 134
  ]
  edge [
    source 18
    target 135
  ]
  edge [
    source 18
    target 136
  ]
  edge [
    source 18
    target 137
  ]
  edge [
    source 18
    target 138
  ]
  edge [
    source 19
    target 20
  ]
  edge [
    source 19
    target 139
  ]
  edge [
    source 19
    target 140
  ]
  edge [
    source 19
    target 141
  ]
  edge [
    source 19
    target 142
  ]
  edge [
    source 19
    target 143
  ]
  edge [
    source 19
    target 144
  ]
  edge [
    source 19
    target 145
  ]
  edge [
    source 20
    target 21
  ]
  edge [
    source 21
    target 22
  ]
  edge [
    source 21
    target 146
  ]
  edge [
    source 21
    target 147
  ]
  edge [
    source 21
    target 148
  ]
]
