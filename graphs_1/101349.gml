graph [
  maxDegree 5
  minDegree 1
  meanDegree 1.75
  density 0.25
  graphCliqueNumber 2
  node [
    id 0
    label "rudawka"
    origin "text"
  ]
  node [
    id 1
    label "powiat"
    origin "text"
  ]
  node [
    id 2
    label "sok&#243;lski"
    origin "text"
  ]
  node [
    id 3
    label "woda"
  ]
  node [
    id 4
    label "bagno"
  ]
  node [
    id 5
    label "gmina"
  ]
  node [
    id 6
    label "jednostka_administracyjna"
  ]
  node [
    id 7
    label "wojew&#243;dztwo"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 3
  ]
  edge [
    source 0
    target 4
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 5
  ]
  edge [
    source 1
    target 6
  ]
  edge [
    source 1
    target 7
  ]
]
