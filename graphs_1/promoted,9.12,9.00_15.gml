graph [
  maxDegree 39
  minDegree 1
  meanDegree 2.0105820105820107
  density 0.010694585162670269
  graphCliqueNumber 2
  node [
    id 0
    label "zwi&#261;zek"
    origin "text"
  ]
  node [
    id 1
    label "pojawia&#263;"
    origin "text"
  ]
  node [
    id 2
    label "si&#281;"
    origin "text"
  ]
  node [
    id 3
    label "ostatni"
    origin "text"
  ]
  node [
    id 4
    label "czas"
    origin "text"
  ]
  node [
    id 5
    label "okre&#347;lenie"
    origin "text"
  ]
  node [
    id 6
    label "dotyczy&#263;"
    origin "text"
  ]
  node [
    id 7
    label "niemiecki"
    origin "text"
  ]
  node [
    id 8
    label "nazistowski"
    origin "text"
  ]
  node [
    id 9
    label "ob&#243;z"
    origin "text"
  ]
  node [
    id 10
    label "zag&#322;ada"
    origin "text"
  ]
  node [
    id 11
    label "pro&#347;ba"
    origin "text"
  ]
  node [
    id 12
    label "powiela&#263;"
    origin "text"
  ]
  node [
    id 13
    label "ten"
    origin "text"
  ]
  node [
    id 14
    label "k&#322;amstwo"
    origin "text"
  ]
  node [
    id 15
    label "szczeg&#243;lnie"
    origin "text"
  ]
  node [
    id 16
    label "tytu&#322;"
    origin "text"
  ]
  node [
    id 17
    label "artyku&#322;"
    origin "text"
  ]
  node [
    id 18
    label "znalezisko"
    origin "text"
  ]
  node [
    id 19
    label "odwodnienie"
  ]
  node [
    id 20
    label "konstytucja"
  ]
  node [
    id 21
    label "zwi&#261;zywa&#263;_si&#281;"
  ]
  node [
    id 22
    label "substancja_chemiczna"
  ]
  node [
    id 23
    label "bratnia_dusza"
  ]
  node [
    id 24
    label "zwi&#261;zanie"
  ]
  node [
    id 25
    label "lokant"
  ]
  node [
    id 26
    label "zwi&#261;zywanie_si&#281;"
  ]
  node [
    id 27
    label "zwi&#261;za&#263;"
  ]
  node [
    id 28
    label "organizacja"
  ]
  node [
    id 29
    label "odwadnia&#263;"
  ]
  node [
    id 30
    label "marriage"
  ]
  node [
    id 31
    label "marketing_afiliacyjny"
  ]
  node [
    id 32
    label "bearing"
  ]
  node [
    id 33
    label "wi&#261;zanie"
  ]
  node [
    id 34
    label "odwadnianie"
  ]
  node [
    id 35
    label "koligacja"
  ]
  node [
    id 36
    label "zwi&#261;za&#263;_si&#281;"
  ]
  node [
    id 37
    label "odwodni&#263;"
  ]
  node [
    id 38
    label "azeotrop"
  ]
  node [
    id 39
    label "powi&#261;zanie"
  ]
  node [
    id 40
    label "cz&#322;owiek"
  ]
  node [
    id 41
    label "kolejny"
  ]
  node [
    id 42
    label "istota_&#380;ywa"
  ]
  node [
    id 43
    label "najgorszy"
  ]
  node [
    id 44
    label "aktualny"
  ]
  node [
    id 45
    label "ostatnio"
  ]
  node [
    id 46
    label "niedawno"
  ]
  node [
    id 47
    label "wi&#261;&#380;&#261;cy"
  ]
  node [
    id 48
    label "sko&#324;czony"
  ]
  node [
    id 49
    label "poprzedni"
  ]
  node [
    id 50
    label "pozosta&#322;y"
  ]
  node [
    id 51
    label "w&#261;tpliwy"
  ]
  node [
    id 52
    label "czasokres"
  ]
  node [
    id 53
    label "trawienie"
  ]
  node [
    id 54
    label "kategoria_gramatyczna"
  ]
  node [
    id 55
    label "period"
  ]
  node [
    id 56
    label "odczyt"
  ]
  node [
    id 57
    label "przep&#322;yni&#281;cie"
  ]
  node [
    id 58
    label "zbli&#380;y&#263;_si&#281;"
  ]
  node [
    id 59
    label "chwila"
  ]
  node [
    id 60
    label "ma&#322;a_stabilizacja"
  ]
  node [
    id 61
    label "poprzedzenie"
  ]
  node [
    id 62
    label "koniugacja"
  ]
  node [
    id 63
    label "dzieje"
  ]
  node [
    id 64
    label "poprzedzi&#263;"
  ]
  node [
    id 65
    label "przep&#322;ywanie"
  ]
  node [
    id 66
    label "przep&#322;ywa&#263;"
  ]
  node [
    id 67
    label "odwlekanie_si&#281;"
  ]
  node [
    id 68
    label "zbli&#380;a&#263;_si&#281;"
  ]
  node [
    id 69
    label "Zeitgeist"
  ]
  node [
    id 70
    label "przep&#322;yn&#261;&#263;"
  ]
  node [
    id 71
    label "okres_czasu"
  ]
  node [
    id 72
    label "zbli&#380;enie_si&#281;"
  ]
  node [
    id 73
    label "pochodzi&#263;"
  ]
  node [
    id 74
    label "schy&#322;ek"
  ]
  node [
    id 75
    label "czwarty_wymiar"
  ]
  node [
    id 76
    label "chronometria"
  ]
  node [
    id 77
    label "poprzedzanie"
  ]
  node [
    id 78
    label "odp&#322;yni&#281;cie"
  ]
  node [
    id 79
    label "pogoda"
  ]
  node [
    id 80
    label "zegar"
  ]
  node [
    id 81
    label "trawi&#263;"
  ]
  node [
    id 82
    label "pochodzenie"
  ]
  node [
    id 83
    label "poprzedza&#263;"
  ]
  node [
    id 84
    label "time_period"
  ]
  node [
    id 85
    label "rachuba_czasu"
  ]
  node [
    id 86
    label "zbli&#380;anie_si&#281;"
  ]
  node [
    id 87
    label "czasoprzestrze&#324;"
  ]
  node [
    id 88
    label "laba"
  ]
  node [
    id 89
    label "zdecydowanie"
  ]
  node [
    id 90
    label "follow-up"
  ]
  node [
    id 91
    label "appointment"
  ]
  node [
    id 92
    label "ustalenie"
  ]
  node [
    id 93
    label "localization"
  ]
  node [
    id 94
    label "denomination"
  ]
  node [
    id 95
    label "wyra&#380;enie"
  ]
  node [
    id 96
    label "ozdobnik"
  ]
  node [
    id 97
    label "przewidzenie"
  ]
  node [
    id 98
    label "term"
  ]
  node [
    id 99
    label "bargain"
  ]
  node [
    id 100
    label "&#322;&#261;czy&#263;_si&#281;"
  ]
  node [
    id 101
    label "tycze&#263;"
  ]
  node [
    id 102
    label "szwabski"
  ]
  node [
    id 103
    label "po_niemiecku"
  ]
  node [
    id 104
    label "niemiec"
  ]
  node [
    id 105
    label "cenar"
  ]
  node [
    id 106
    label "j&#281;zyk"
  ]
  node [
    id 107
    label "europejski"
  ]
  node [
    id 108
    label "German"
  ]
  node [
    id 109
    label "pionier"
  ]
  node [
    id 110
    label "niemiecko"
  ]
  node [
    id 111
    label "j&#281;zyk_zachodniogerma&#324;ski"
  ]
  node [
    id 112
    label "zachodnioeuropejski"
  ]
  node [
    id 113
    label "strudel"
  ]
  node [
    id 114
    label "junkers"
  ]
  node [
    id 115
    label "&#347;rodkowoeuropejski"
  ]
  node [
    id 116
    label "anty&#380;ydowski"
  ]
  node [
    id 117
    label "faszystowski"
  ]
  node [
    id 118
    label "hitlerowsko"
  ]
  node [
    id 119
    label "rasistowski"
  ]
  node [
    id 120
    label "antykomunistyczny"
  ]
  node [
    id 121
    label "po_nazistowsku"
  ]
  node [
    id 122
    label "namiot"
  ]
  node [
    id 123
    label "ONZ"
  ]
  node [
    id 124
    label "grupa"
  ]
  node [
    id 125
    label "podob&#243;z"
  ]
  node [
    id 126
    label "blok"
  ]
  node [
    id 127
    label "odpoczynek"
  ]
  node [
    id 128
    label "alianci"
  ]
  node [
    id 129
    label "Paneuropa"
  ]
  node [
    id 130
    label "NATO"
  ]
  node [
    id 131
    label "miejsce_odosobnienia"
  ]
  node [
    id 132
    label "schronienie"
  ]
  node [
    id 133
    label "confederation"
  ]
  node [
    id 134
    label "obozowisko"
  ]
  node [
    id 135
    label "apokalipsa"
  ]
  node [
    id 136
    label "negacjonizm"
  ]
  node [
    id 137
    label "zniszczenie"
  ]
  node [
    id 138
    label "pogr&#261;&#380;y&#263;"
  ]
  node [
    id 139
    label "ludob&#243;jstwo"
  ]
  node [
    id 140
    label "szmalcownik"
  ]
  node [
    id 141
    label "holocaust"
  ]
  node [
    id 142
    label "kl&#281;ska_&#380;ywio&#322;owa"
  ]
  node [
    id 143
    label "pogr&#261;&#380;enie"
  ]
  node [
    id 144
    label "crash"
  ]
  node [
    id 145
    label "solicitation"
  ]
  node [
    id 146
    label "wypowied&#378;"
  ]
  node [
    id 147
    label "powtarza&#263;"
  ]
  node [
    id 148
    label "kopiowa&#263;"
  ]
  node [
    id 149
    label "transcribe"
  ]
  node [
    id 150
    label "okre&#347;lony"
  ]
  node [
    id 151
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 152
    label "lipa"
  ]
  node [
    id 153
    label "oszustwo"
  ]
  node [
    id 154
    label "wymys&#322;"
  ]
  node [
    id 155
    label "lie"
  ]
  node [
    id 156
    label "blef"
  ]
  node [
    id 157
    label "nieprawda"
  ]
  node [
    id 158
    label "szczeg&#243;lny"
  ]
  node [
    id 159
    label "osobnie"
  ]
  node [
    id 160
    label "wyj&#261;tkowo"
  ]
  node [
    id 161
    label "specially"
  ]
  node [
    id 162
    label "podtytu&#322;"
  ]
  node [
    id 163
    label "debit"
  ]
  node [
    id 164
    label "szata_graficzna"
  ]
  node [
    id 165
    label "elevation"
  ]
  node [
    id 166
    label "wyda&#263;"
  ]
  node [
    id 167
    label "nadtytu&#322;"
  ]
  node [
    id 168
    label "tytulatura"
  ]
  node [
    id 169
    label "nazwa"
  ]
  node [
    id 170
    label "redaktor"
  ]
  node [
    id 171
    label "wydawa&#263;"
  ]
  node [
    id 172
    label "druk"
  ]
  node [
    id 173
    label "mianowaniec"
  ]
  node [
    id 174
    label "poster"
  ]
  node [
    id 175
    label "publikacja"
  ]
  node [
    id 176
    label "dokument"
  ]
  node [
    id 177
    label "towar"
  ]
  node [
    id 178
    label "nag&#322;&#243;wek"
  ]
  node [
    id 179
    label "znak_j&#281;zykowy"
  ]
  node [
    id 180
    label "wyr&#243;b"
  ]
  node [
    id 181
    label "line"
  ]
  node [
    id 182
    label "paragraf"
  ]
  node [
    id 183
    label "rodzajnik"
  ]
  node [
    id 184
    label "prawda"
  ]
  node [
    id 185
    label "szkic"
  ]
  node [
    id 186
    label "tekst"
  ]
  node [
    id 187
    label "fragment"
  ]
  node [
    id 188
    label "przedmiot"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 19
  ]
  edge [
    source 0
    target 20
  ]
  edge [
    source 0
    target 21
  ]
  edge [
    source 0
    target 22
  ]
  edge [
    source 0
    target 23
  ]
  edge [
    source 0
    target 24
  ]
  edge [
    source 0
    target 25
  ]
  edge [
    source 0
    target 26
  ]
  edge [
    source 0
    target 27
  ]
  edge [
    source 0
    target 28
  ]
  edge [
    source 0
    target 29
  ]
  edge [
    source 0
    target 30
  ]
  edge [
    source 0
    target 31
  ]
  edge [
    source 0
    target 32
  ]
  edge [
    source 0
    target 33
  ]
  edge [
    source 0
    target 34
  ]
  edge [
    source 0
    target 35
  ]
  edge [
    source 0
    target 36
  ]
  edge [
    source 0
    target 37
  ]
  edge [
    source 0
    target 38
  ]
  edge [
    source 0
    target 39
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 40
  ]
  edge [
    source 3
    target 41
  ]
  edge [
    source 3
    target 42
  ]
  edge [
    source 3
    target 43
  ]
  edge [
    source 3
    target 44
  ]
  edge [
    source 3
    target 45
  ]
  edge [
    source 3
    target 46
  ]
  edge [
    source 3
    target 47
  ]
  edge [
    source 3
    target 48
  ]
  edge [
    source 3
    target 49
  ]
  edge [
    source 3
    target 50
  ]
  edge [
    source 3
    target 51
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 52
  ]
  edge [
    source 4
    target 53
  ]
  edge [
    source 4
    target 54
  ]
  edge [
    source 4
    target 55
  ]
  edge [
    source 4
    target 56
  ]
  edge [
    source 4
    target 57
  ]
  edge [
    source 4
    target 58
  ]
  edge [
    source 4
    target 59
  ]
  edge [
    source 4
    target 60
  ]
  edge [
    source 4
    target 61
  ]
  edge [
    source 4
    target 62
  ]
  edge [
    source 4
    target 63
  ]
  edge [
    source 4
    target 64
  ]
  edge [
    source 4
    target 65
  ]
  edge [
    source 4
    target 66
  ]
  edge [
    source 4
    target 67
  ]
  edge [
    source 4
    target 68
  ]
  edge [
    source 4
    target 69
  ]
  edge [
    source 4
    target 70
  ]
  edge [
    source 4
    target 71
  ]
  edge [
    source 4
    target 72
  ]
  edge [
    source 4
    target 73
  ]
  edge [
    source 4
    target 74
  ]
  edge [
    source 4
    target 75
  ]
  edge [
    source 4
    target 76
  ]
  edge [
    source 4
    target 77
  ]
  edge [
    source 4
    target 78
  ]
  edge [
    source 4
    target 79
  ]
  edge [
    source 4
    target 80
  ]
  edge [
    source 4
    target 81
  ]
  edge [
    source 4
    target 82
  ]
  edge [
    source 4
    target 83
  ]
  edge [
    source 4
    target 84
  ]
  edge [
    source 4
    target 85
  ]
  edge [
    source 4
    target 86
  ]
  edge [
    source 4
    target 87
  ]
  edge [
    source 4
    target 88
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 89
  ]
  edge [
    source 5
    target 90
  ]
  edge [
    source 5
    target 91
  ]
  edge [
    source 5
    target 92
  ]
  edge [
    source 5
    target 93
  ]
  edge [
    source 5
    target 94
  ]
  edge [
    source 5
    target 95
  ]
  edge [
    source 5
    target 96
  ]
  edge [
    source 5
    target 97
  ]
  edge [
    source 5
    target 98
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 99
  ]
  edge [
    source 6
    target 100
  ]
  edge [
    source 6
    target 101
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 102
  ]
  edge [
    source 7
    target 103
  ]
  edge [
    source 7
    target 104
  ]
  edge [
    source 7
    target 105
  ]
  edge [
    source 7
    target 106
  ]
  edge [
    source 7
    target 107
  ]
  edge [
    source 7
    target 108
  ]
  edge [
    source 7
    target 109
  ]
  edge [
    source 7
    target 110
  ]
  edge [
    source 7
    target 111
  ]
  edge [
    source 7
    target 112
  ]
  edge [
    source 7
    target 113
  ]
  edge [
    source 7
    target 114
  ]
  edge [
    source 7
    target 115
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 116
  ]
  edge [
    source 8
    target 117
  ]
  edge [
    source 8
    target 118
  ]
  edge [
    source 8
    target 119
  ]
  edge [
    source 8
    target 120
  ]
  edge [
    source 8
    target 121
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 122
  ]
  edge [
    source 9
    target 123
  ]
  edge [
    source 9
    target 124
  ]
  edge [
    source 9
    target 125
  ]
  edge [
    source 9
    target 126
  ]
  edge [
    source 9
    target 127
  ]
  edge [
    source 9
    target 128
  ]
  edge [
    source 9
    target 129
  ]
  edge [
    source 9
    target 130
  ]
  edge [
    source 9
    target 131
  ]
  edge [
    source 9
    target 132
  ]
  edge [
    source 9
    target 133
  ]
  edge [
    source 9
    target 134
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 10
    target 135
  ]
  edge [
    source 10
    target 136
  ]
  edge [
    source 10
    target 137
  ]
  edge [
    source 10
    target 138
  ]
  edge [
    source 10
    target 139
  ]
  edge [
    source 10
    target 140
  ]
  edge [
    source 10
    target 141
  ]
  edge [
    source 10
    target 142
  ]
  edge [
    source 10
    target 143
  ]
  edge [
    source 10
    target 144
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 145
  ]
  edge [
    source 11
    target 146
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 147
  ]
  edge [
    source 12
    target 148
  ]
  edge [
    source 12
    target 149
  ]
  edge [
    source 13
    target 14
  ]
  edge [
    source 13
    target 150
  ]
  edge [
    source 13
    target 151
  ]
  edge [
    source 14
    target 15
  ]
  edge [
    source 14
    target 152
  ]
  edge [
    source 14
    target 153
  ]
  edge [
    source 14
    target 154
  ]
  edge [
    source 14
    target 155
  ]
  edge [
    source 14
    target 156
  ]
  edge [
    source 14
    target 157
  ]
  edge [
    source 15
    target 16
  ]
  edge [
    source 15
    target 158
  ]
  edge [
    source 15
    target 159
  ]
  edge [
    source 15
    target 160
  ]
  edge [
    source 15
    target 161
  ]
  edge [
    source 16
    target 17
  ]
  edge [
    source 16
    target 162
  ]
  edge [
    source 16
    target 163
  ]
  edge [
    source 16
    target 164
  ]
  edge [
    source 16
    target 165
  ]
  edge [
    source 16
    target 166
  ]
  edge [
    source 16
    target 167
  ]
  edge [
    source 16
    target 168
  ]
  edge [
    source 16
    target 169
  ]
  edge [
    source 16
    target 170
  ]
  edge [
    source 16
    target 171
  ]
  edge [
    source 16
    target 172
  ]
  edge [
    source 16
    target 173
  ]
  edge [
    source 16
    target 174
  ]
  edge [
    source 16
    target 175
  ]
  edge [
    source 17
    target 18
  ]
  edge [
    source 17
    target 176
  ]
  edge [
    source 17
    target 177
  ]
  edge [
    source 17
    target 178
  ]
  edge [
    source 17
    target 179
  ]
  edge [
    source 17
    target 180
  ]
  edge [
    source 17
    target 126
  ]
  edge [
    source 17
    target 181
  ]
  edge [
    source 17
    target 182
  ]
  edge [
    source 17
    target 183
  ]
  edge [
    source 17
    target 184
  ]
  edge [
    source 17
    target 185
  ]
  edge [
    source 17
    target 186
  ]
  edge [
    source 17
    target 187
  ]
  edge [
    source 18
    target 188
  ]
]
