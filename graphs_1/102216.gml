graph [
  maxDegree 9
  minDegree 1
  meanDegree 1.92
  density 0.08
  graphCliqueNumber 2
  node [
    id 0
    label "u&#380;ytek"
    origin "text"
  ]
  node [
    id 1
    label "niniejszy"
    origin "text"
  ]
  node [
    id 2
    label "rozporz&#261;dzenie"
    origin "text"
  ]
  node [
    id 3
    label "stosowa&#263;"
    origin "text"
  ]
  node [
    id 4
    label "si&#281;"
    origin "text"
  ]
  node [
    id 5
    label "nast&#281;puj&#261;cy"
    origin "text"
  ]
  node [
    id 6
    label "definicja"
    origin "text"
  ]
  node [
    id 7
    label "cel"
  ]
  node [
    id 8
    label "kopia"
  ]
  node [
    id 9
    label "function"
  ]
  node [
    id 10
    label "ten"
  ]
  node [
    id 11
    label "rule"
  ]
  node [
    id 12
    label "polecenie"
  ]
  node [
    id 13
    label "akt"
  ]
  node [
    id 14
    label "arrangement"
  ]
  node [
    id 15
    label "zarz&#261;dzenie"
  ]
  node [
    id 16
    label "commission"
  ]
  node [
    id 17
    label "ordonans"
  ]
  node [
    id 18
    label "u&#380;ywa&#263;"
  ]
  node [
    id 19
    label "okre&#347;lony"
  ]
  node [
    id 20
    label "nast&#281;puj&#261;co"
  ]
  node [
    id 21
    label "definiens"
  ]
  node [
    id 22
    label "definiendum"
  ]
  node [
    id 23
    label "obja&#347;nienie"
  ]
  node [
    id 24
    label "definition"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 7
  ]
  edge [
    source 0
    target 8
  ]
  edge [
    source 0
    target 9
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 1
    target 10
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 11
  ]
  edge [
    source 2
    target 12
  ]
  edge [
    source 2
    target 13
  ]
  edge [
    source 2
    target 14
  ]
  edge [
    source 2
    target 15
  ]
  edge [
    source 2
    target 16
  ]
  edge [
    source 2
    target 17
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 18
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 19
  ]
  edge [
    source 5
    target 20
  ]
  edge [
    source 6
    target 21
  ]
  edge [
    source 6
    target 22
  ]
  edge [
    source 6
    target 23
  ]
  edge [
    source 6
    target 24
  ]
]
