graph [
  maxDegree 12
  minDegree 1
  meanDegree 2.019230769230769
  density 0.019604182225541448
  graphCliqueNumber 3
  node [
    id 0
    label "dzisiaj"
    origin "text"
  ]
  node [
    id 1
    label "oko&#322;o"
    origin "text"
  ]
  node [
    id 2
    label "po&#322;udnie"
    origin "text"
  ]
  node [
    id 3
    label "problem"
    origin "text"
  ]
  node [
    id 4
    label "dost&#281;p"
    origin "text"
  ]
  node [
    id 5
    label "us&#322;uga"
    origin "text"
  ]
  node [
    id 6
    label "zacz&#261;&#263;"
    origin "text"
  ]
  node [
    id 7
    label "zg&#322;asza&#263;"
    origin "text"
  ]
  node [
    id 8
    label "klient"
    origin "text"
  ]
  node [
    id 9
    label "ma&#322;o"
    origin "text"
  ]
  node [
    id 10
    label "pi&#261;&#263;"
    origin "text"
  ]
  node [
    id 11
    label "du&#380;y"
    origin "text"
  ]
  node [
    id 12
    label "bank"
    origin "text"
  ]
  node [
    id 13
    label "naraz"
    origin "text"
  ]
  node [
    id 14
    label "doba"
  ]
  node [
    id 15
    label "dzie&#324;_dzisiejszy"
  ]
  node [
    id 16
    label "dzi&#347;"
  ]
  node [
    id 17
    label "tera&#378;niejszo&#347;&#263;"
  ]
  node [
    id 18
    label "wsp&#243;&#322;cze&#347;nie"
  ]
  node [
    id 19
    label "dwunasta"
  ]
  node [
    id 20
    label "obszar"
  ]
  node [
    id 21
    label "Ziemia"
  ]
  node [
    id 22
    label "godzina"
  ]
  node [
    id 23
    label "strona_&#347;wiata"
  ]
  node [
    id 24
    label "cz&#281;&#347;&#263;"
  ]
  node [
    id 25
    label "&#347;rodek"
  ]
  node [
    id 26
    label "pora"
  ]
  node [
    id 27
    label "dzie&#324;"
  ]
  node [
    id 28
    label "trudno&#347;&#263;"
  ]
  node [
    id 29
    label "sprawa"
  ]
  node [
    id 30
    label "ambaras"
  ]
  node [
    id 31
    label "problemat"
  ]
  node [
    id 32
    label "pierepa&#322;ka"
  ]
  node [
    id 33
    label "obstruction"
  ]
  node [
    id 34
    label "problematyka"
  ]
  node [
    id 35
    label "jajko_Kolumba"
  ]
  node [
    id 36
    label "subiekcja"
  ]
  node [
    id 37
    label "trudny_orzech_do_zgryzienia"
  ]
  node [
    id 38
    label "miejsce"
  ]
  node [
    id 39
    label "konto"
  ]
  node [
    id 40
    label "informatyka"
  ]
  node [
    id 41
    label "has&#322;o"
  ]
  node [
    id 42
    label "mo&#380;liwo&#347;&#263;"
  ]
  node [
    id 43
    label "operacja"
  ]
  node [
    id 44
    label "produkt_gotowy"
  ]
  node [
    id 45
    label "service"
  ]
  node [
    id 46
    label "czynno&#347;&#263;"
  ]
  node [
    id 47
    label "&#347;wiadczy&#263;"
  ]
  node [
    id 48
    label "&#347;wiadczenie"
  ]
  node [
    id 49
    label "asortyment"
  ]
  node [
    id 50
    label "cause"
  ]
  node [
    id 51
    label "introduce"
  ]
  node [
    id 52
    label "begin"
  ]
  node [
    id 53
    label "odj&#261;&#263;"
  ]
  node [
    id 54
    label "post&#261;pi&#263;"
  ]
  node [
    id 55
    label "zacz&#261;&#263;_si&#281;"
  ]
  node [
    id 56
    label "do"
  ]
  node [
    id 57
    label "zrobi&#263;_pierwszy_krok"
  ]
  node [
    id 58
    label "zrobi&#263;"
  ]
  node [
    id 59
    label "report"
  ]
  node [
    id 60
    label "write"
  ]
  node [
    id 61
    label "w&#322;&#261;cza&#263;"
  ]
  node [
    id 62
    label "informowa&#263;"
  ]
  node [
    id 63
    label "cz&#322;owiek"
  ]
  node [
    id 64
    label "bratek"
  ]
  node [
    id 65
    label "klientela"
  ]
  node [
    id 66
    label "szlachcic"
  ]
  node [
    id 67
    label "agent_rozliczeniowy"
  ]
  node [
    id 68
    label "komputer_cyfrowy"
  ]
  node [
    id 69
    label "program"
  ]
  node [
    id 70
    label "us&#322;ugobiorca"
  ]
  node [
    id 71
    label "Rzymianin"
  ]
  node [
    id 72
    label "obywatel"
  ]
  node [
    id 73
    label "mo&#380;liwie"
  ]
  node [
    id 74
    label "nieznaczny"
  ]
  node [
    id 75
    label "kr&#243;tko"
  ]
  node [
    id 76
    label "nieistotnie"
  ]
  node [
    id 77
    label "nieliczny"
  ]
  node [
    id 78
    label "mikroskopijnie"
  ]
  node [
    id 79
    label "pomiernie"
  ]
  node [
    id 80
    label "ma&#322;y"
  ]
  node [
    id 81
    label "doros&#322;y"
  ]
  node [
    id 82
    label "wiele"
  ]
  node [
    id 83
    label "dorodny"
  ]
  node [
    id 84
    label "znaczny"
  ]
  node [
    id 85
    label "du&#380;o"
  ]
  node [
    id 86
    label "prawdziwy"
  ]
  node [
    id 87
    label "niema&#322;o"
  ]
  node [
    id 88
    label "wa&#380;ny"
  ]
  node [
    id 89
    label "rozwini&#281;ty"
  ]
  node [
    id 90
    label "wk&#322;adca"
  ]
  node [
    id 91
    label "agencja"
  ]
  node [
    id 92
    label "eurorynek"
  ]
  node [
    id 93
    label "zbi&#243;r"
  ]
  node [
    id 94
    label "instytucja"
  ]
  node [
    id 95
    label "siedziba"
  ]
  node [
    id 96
    label "kwota"
  ]
  node [
    id 97
    label "raptownie"
  ]
  node [
    id 98
    label "niespodziewanie"
  ]
  node [
    id 99
    label "jednocze&#347;nie"
  ]
  node [
    id 100
    label "&#322;&#261;cznie"
  ]
  node [
    id 101
    label "Pekao"
  ]
  node [
    id 102
    label "sekunda"
  ]
  node [
    id 103
    label "a"
  ]
  edge [
    source 0
    target 1
  ]
  edge [
    source 0
    target 14
  ]
  edge [
    source 0
    target 15
  ]
  edge [
    source 0
    target 16
  ]
  edge [
    source 0
    target 17
  ]
  edge [
    source 0
    target 18
  ]
  edge [
    source 1
    target 2
  ]
  edge [
    source 2
    target 3
  ]
  edge [
    source 2
    target 19
  ]
  edge [
    source 2
    target 20
  ]
  edge [
    source 2
    target 21
  ]
  edge [
    source 2
    target 22
  ]
  edge [
    source 2
    target 23
  ]
  edge [
    source 2
    target 24
  ]
  edge [
    source 2
    target 25
  ]
  edge [
    source 2
    target 26
  ]
  edge [
    source 2
    target 27
  ]
  edge [
    source 3
    target 4
  ]
  edge [
    source 3
    target 28
  ]
  edge [
    source 3
    target 29
  ]
  edge [
    source 3
    target 30
  ]
  edge [
    source 3
    target 31
  ]
  edge [
    source 3
    target 32
  ]
  edge [
    source 3
    target 33
  ]
  edge [
    source 3
    target 34
  ]
  edge [
    source 3
    target 35
  ]
  edge [
    source 3
    target 36
  ]
  edge [
    source 3
    target 37
  ]
  edge [
    source 4
    target 5
  ]
  edge [
    source 4
    target 38
  ]
  edge [
    source 4
    target 39
  ]
  edge [
    source 4
    target 40
  ]
  edge [
    source 4
    target 41
  ]
  edge [
    source 4
    target 42
  ]
  edge [
    source 4
    target 43
  ]
  edge [
    source 5
    target 6
  ]
  edge [
    source 5
    target 44
  ]
  edge [
    source 5
    target 45
  ]
  edge [
    source 5
    target 46
  ]
  edge [
    source 5
    target 47
  ]
  edge [
    source 5
    target 48
  ]
  edge [
    source 5
    target 49
  ]
  edge [
    source 6
    target 7
  ]
  edge [
    source 6
    target 50
  ]
  edge [
    source 6
    target 51
  ]
  edge [
    source 6
    target 52
  ]
  edge [
    source 6
    target 53
  ]
  edge [
    source 6
    target 54
  ]
  edge [
    source 6
    target 55
  ]
  edge [
    source 6
    target 56
  ]
  edge [
    source 6
    target 57
  ]
  edge [
    source 6
    target 58
  ]
  edge [
    source 7
    target 8
  ]
  edge [
    source 7
    target 59
  ]
  edge [
    source 7
    target 60
  ]
  edge [
    source 7
    target 61
  ]
  edge [
    source 7
    target 62
  ]
  edge [
    source 8
    target 9
  ]
  edge [
    source 8
    target 63
  ]
  edge [
    source 8
    target 64
  ]
  edge [
    source 8
    target 65
  ]
  edge [
    source 8
    target 66
  ]
  edge [
    source 8
    target 67
  ]
  edge [
    source 8
    target 68
  ]
  edge [
    source 8
    target 69
  ]
  edge [
    source 8
    target 70
  ]
  edge [
    source 8
    target 71
  ]
  edge [
    source 8
    target 72
  ]
  edge [
    source 9
    target 10
  ]
  edge [
    source 9
    target 73
  ]
  edge [
    source 9
    target 74
  ]
  edge [
    source 9
    target 75
  ]
  edge [
    source 9
    target 76
  ]
  edge [
    source 9
    target 77
  ]
  edge [
    source 9
    target 78
  ]
  edge [
    source 9
    target 79
  ]
  edge [
    source 9
    target 80
  ]
  edge [
    source 10
    target 11
  ]
  edge [
    source 11
    target 12
  ]
  edge [
    source 11
    target 81
  ]
  edge [
    source 11
    target 82
  ]
  edge [
    source 11
    target 83
  ]
  edge [
    source 11
    target 84
  ]
  edge [
    source 11
    target 85
  ]
  edge [
    source 11
    target 86
  ]
  edge [
    source 11
    target 87
  ]
  edge [
    source 11
    target 88
  ]
  edge [
    source 11
    target 89
  ]
  edge [
    source 12
    target 13
  ]
  edge [
    source 12
    target 90
  ]
  edge [
    source 12
    target 91
  ]
  edge [
    source 12
    target 39
  ]
  edge [
    source 12
    target 67
  ]
  edge [
    source 12
    target 92
  ]
  edge [
    source 12
    target 93
  ]
  edge [
    source 12
    target 94
  ]
  edge [
    source 12
    target 95
  ]
  edge [
    source 12
    target 96
  ]
  edge [
    source 13
    target 97
  ]
  edge [
    source 13
    target 98
  ]
  edge [
    source 13
    target 99
  ]
  edge [
    source 13
    target 100
  ]
  edge [
    source 101
    target 102
  ]
  edge [
    source 101
    target 103
  ]
  edge [
    source 102
    target 103
  ]
]
