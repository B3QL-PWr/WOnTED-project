from string import punctuation
import xml.etree.ElementTree as ET
from typing import List, Tuple, Dict
import re


def _duplicates(lst, item):
    return [i for i, x in enumerate(lst) if x == item]


def _merge_multiword(words, lexs, names):
    multiwords = {}
    for keys, values in names.items():
        dupls = [_duplicates(values, x) for x in set(values) - set('0') if values.count(x) > 1]
        mw_list = [[words[i] for i in lst] for lst in dupls]
        lx_list = [[lexs[i] for i in lst] for lst in dupls]
        for mw, lx in zip(mw_list, lx_list):
            if len(mw) > 0:
                multiwords[' '.join(mw)] = lx
    return multiwords


def get_proper(file: str) -> Tuple[List[str], Dict[str, List[str]]]:
    single = []
    multi = {}
    tree = ET.parse(file)
    root = tree.getroot()
    for child in root:
        for sentence in child:
            words = []
            lexs = []
            names = {}
            for token in sentence:
                if token.tag != 'ns':
                    names_list = {}
                    word = ''
                    lex = ''
                    for attr in token:
                        if attr.tag == 'orth':
                            word = re.sub(f'[{punctuation}]', '', attr.text)
                        if attr.tag == 'lex':
                            lex = re.sub(f'[{punctuation}]', '', attr[0].text)
                        if word and lex:
                            if attr.tag == 'ann' and 'nam' in attr.attrib['chan']:
                                names_list[attr.attrib['chan']] = attr.text
                    if any(nam != '0' for nam in names_list.values()):
                        if word:
                            words.append(word)
                        if lex:
                            lexs.append(lex)
                        for key, value in names_list.items():
                            if key in names:
                                names[key].append(value)
                            else:
                                names[key] = [value]
            if len(words) > 0:
                single += [lex.lower() for lex in lexs if lex.lower() not in single]
                multi.update(_merge_multiword(words, lexs, names))
    return single, multi
